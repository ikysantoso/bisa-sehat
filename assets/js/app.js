$(function(){
    
    if($('.section-parallax').length){
        $(window).on('scroll',function(){
	        var y = $(window).scrollTop() /5;
	        $('.section-parallax .bg').css({'transform':'translateY('+y+'px)'})
	    });
    }
    $('[data-toggle=scroll]').on('click',function(){
        var offset = $(this).data('offset') || 0;
        var href = $(this).attr('href');
        var target = $(href).offset().top - offset;
        $('html,body').animate({
            scrollTop:target
        },500)
    });

    if($('.section-course-sidebar').length){
        var sidebarWidth = $('.section-course-sidebar').innerWidth();
        var sidebarPos = $('.section-course-sidebar').offset();
        if($(window).width() > 768){
            floatSidebarCourse();
            $(window).on('scroll',function(){
                floatSidebarCourse();
            });
        }

        function floatSidebarCourse(){
            var trigger = $('.section-course').next().offset().top - $('.section-course').height()/2 - 100;
            console.log(trigger);
            console.log($(window).scrollTop());
            if($(window).scrollTop() > sidebarPos.top - 100 && $(window).scrollTop() < trigger){
                $('.section-course-sidebar').css({
                    'position':'fixed',
                    'top':100,
                    'width':sidebarWidth+'px'
                });
            } else if($(window).scrollTop() > trigger){
                $('.section-course-sidebar').css({
                    'position':'absolute',
                    'top':'auto',
                    'bottom':30,
                    'width':sidebarWidth+'px'
                });
            } else{
                $('.section-course-sidebar').css({
                    'position':'relative',
                    'top':'auto',
                    'bottom':'auto',
                    'width':'100%'
                });
            }
        }
    }
    
});