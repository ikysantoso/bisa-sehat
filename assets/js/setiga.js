var $ = jQuery;

(function($) {
  $.fn.inputFilter = function(inputFilter) {
    return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
      if (inputFilter(this.value)) {
        this.oldValue = this.value;
        this.oldSelectionStart = this.selectionStart;
        this.oldSelectionEnd = this.selectionEnd;
      } else if (this.hasOwnProperty("oldValue")) {
        this.value = this.oldValue;
        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
      } else {
        this.value = "";
      }
    });
  };
}(jQuery));

var contact_us_form = (function(){
	'use strict';

	function check_empty(){
		var empty = 0;
		if($('input.required').length > 0){
			$('input.required').each(function(index,el){
				if($(el).val().trim() == ''){
					empty = empty+1;
					$(el).addClass('error');
				}else{
					$(el).removeClass('error');
				}
			})
		}

		console.log(check_email($('#email').val().trim()));return false;

		// return empty;
		// console.log($('input.required').length);
		// console.log(empty);
	}

	function check_email(email){
		// var emailRegex = /^[A-Z0-9_'%=+!`#~$*?^{}&|-]+([\.][A-Z0-9_'%=+!`#~$*?^{}&|-]+)*@[A-Z0-9-]+(\.[A-Z0-9-]+)+$/i;
		var emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
		if(emailRegex.test('email')){
			return 'valid';
		}
		else{
			return 'invalid';
		}
	}

	function send_email(){
		$('.send_email').on('click', function(){
			if(check_empty() == 0){
				// console.log('diisi kabeh');
				// $('#contact').submit();
			}else{
				// console.log('ono sing jik kosong');
			}
		})
	}

	var contact_us_form = {
		init: function() {
			send_email();
		}
	};

	return contact_us_form;
}());

var buylink = (function(){
	'use strict';

	function click_control(){
		$('.buy-button').on('click', function(){

			if(window.screen.width < 768){
				// open mobile
				if($('.buylink.mobile').hasClass('show')){
					return false;
				}else{
					$('.buylink.mobile').removeClass('d-none');
					$('.buylink.mobile').addClass('show');
					$('.buylink.desktop').addClass('d-none');
				}
			}else{
				// open desktop
				if($('.buylink.desktop').hasClass('show')){
					return false;
				}else{
					$('.buylink.desktop').removeClass('d-none');
					$('.buylink.desktop').addClass('show');
					$('.buylink.mobile').addClass('d-none');
				}
			}
		})

		$('.close-button').on('click', function(){
			if($('.buylink').hasClass('show')){
				$('.buylink').addClass('d-none');
				$('.buylink').removeClass('show');	
			}else{
				return false;							
			}
		})
	}

	var buylink = {
		init: function() {
			click_control();
		}
	};

	return buylink;
}());

var member_form = (function(){
	'use strict';

	function get_city(){
		$('select#provinsi').on('change', function(){
			var level = 'provinsi';
			var provinsi =[$('select#provinsi').find('option:selected').val()];
			$.ajax(
                {
                    type:"post",
                    url: base_url + "ajax-alamat",
                    data:{ level: level, selected_data: provinsi},
                    success:function(response)
                    {
                        // console.log(response);
                        var kab = jQuery.parseJSON(response);
                        var option = '<option val="">Pilih Kabupaten/Kota</option>'
                        for(var i = 0; i < kab.length; i++) {
                            var obj = kab[i];
                        	var new_option = '<option value="'+obj.type+'+'+obj.kabupaten+'">'+obj.type+' ' +obj.kabupaten+'</option>'

                            option = option + new_option;
                        }
                        $('select#kabupaten').empty();
                        $('select#kabupaten').html(option);
                        // console.log(option);

                        $('select#kecamatan').empty();
                        $('select#kecamatan').html('<option val="">Pilih Kecamatan</option>');

                        $('select#desa').empty();
                        $('select#desa').html('<option val="">Pilih Desa/Kelurahan</option>');
                    }
                }
            );
		})
	}

	function get_district(){
		$('select#kabupaten').on('change', function(){
			var level = 'kabupaten';
			var prov = $('select#provinsi').find('option:selected').val();
			var kab = $('select#kabupaten').find('option:selected').val();
			var all_data =[prov,kab];
			$.ajax(
                {
                    type:"post",
                    url: base_url + "ajax-alamat",
                    data:{ level: level, selected_data: all_data},
                    success:function(response)
                    {
                        console.log(response);
                        var kec = jQuery.parseJSON(response);
                        var option = '<option val="">Pilih Kecamatan</option>'
                        for(var i = 0; i < kec.length; i++) {
                            var obj = kec[i];
                        	var new_option = '<option value="'+obj.kecamatan+'">' +obj.kecamatan+'</option>'

                            option = option + new_option;
                        }
                        $('select#kecamatan').empty();
                        $('select#kecamatan').html(option);
                        // console.log(option);

                        $('select#desa').empty();
                        $('select#desa').html('<option val="">Pilih Desa/Kelurahan</option>');
                    }
                }
            );
		})
	}

	function get_village(){
		$('select#kecamatan').on('change', function(){
			var level = 'kecamatan';
			var prov = $('select#provinsi').find('option:selected').val();
			var kab = $('select#kabupaten').find('option:selected').val();
			var kec = $('select#kecamatan').find('option:selected').val();
			var all_data =[prov,kab,kec];
			$.ajax(
                {
                    type:"post",
                    url: base_url + "ajax-alamat",
                    data:{ level: level, selected_data: all_data},
                    success:function(response)
                    {
                        console.log(response);
                        var desa = jQuery.parseJSON(response);
                        var option = '<option val="">Pilih Desa/Kelurahan</option>'
                        for(var i = 0; i < desa.length; i++) {
                            var obj = desa[i];
                        	var new_option = '<option value="'+obj.id+'+'+obj.desa+'">' +obj.desa+'</option>'

                            option = option + new_option;
                        }
                        $('select#desa').empty();
                        $('select#desa').html(option);
                        // console.log(option);
                    }
                }
            );
		})
	}

	var member_form = {
		init: function() {
			get_city();
			get_district();
			get_village();
		}
	};

	return member_form;
}());

jQuery(document).ready(function($) {
	if($('form#contact').length > 0){
		//contact_us_form.init();
	}

	if($('.buylink').length > 0){
		buylink.init();
	}

	member_form.init();
	$(".numeric").inputFilter(function(value) {
	    return /^\d*$/.test(value);    // Allow digits only, using a RegExp
	});

	$(".decimal").inputFilter(function(value) {
	    return /^\d*\.?(?:\d{1,2})?$/.test(value);    // Allow digits only, using a RegExp
	});
});