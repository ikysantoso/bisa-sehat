	function load_fast_select(reinit=false, selected_data=null){

		if (reinit) {

			if ( $('input[name="province"]').data('fastselect') ) {
				$('input[name="province"]').data('fastselect').destroy();
			}
			if ( $('input[name="district"]').data('fastselect') ) {
				$('input[name="district"]').data('fastselect').destroy();
				$('input[name="district"]').addClass('form-control').attr('disabled', 'disabled')
			}

			if ( $('input[name="sub-district"]').data('fastselect') ) {
				$('input[name="sub-district"]').data('fastselect').destroy();
				$('input[name="sub-district"]').addClass('form-control').attr('disabled', 'disabled')
			}

			if ( $('input[name="village"]').data('fastselect') ) {
				$('input[name="village"]').data('fastselect').destroy();
				$('input[name="village"]').addClass('form-control').attr('disabled', 'disabled')
			}
		}


		var initialValue_data = (selected_data && selected_data.province) ? [{"text": selected_data.province.text, "value":selected_data.province.value}] : null;

		$('input[name="province"]').fastselect({
			userOptionAllowed: true,
			initialValue: initialValue_data,
			onItemSelect: function($item, itemModel){
				trigger_load_district( itemModel.value );
			}
		});

		if ( initialValue_data ) {
			$('input[name="province"]').val( selected_data.province.value );
			trigger_load_district( selected_data.province.value, selected_data );
		}
	}

	function trigger_load_district( district_value, selected_data=null ){
				// clear data
				if ( $("input[name='district']").data('fastselect') ) {
					$("input[name='district']").data('fastselect').destroy();
					$("input[name='district']").addClass('form-control').val('').attr('disabled','disabled');
				}
				if ( $("input[name='sub-district']").data('fastselect') ) {
					$("input[name='sub-district']").data('fastselect').destroy();
					$("input[name='sub-district']").addClass('form-control').val('').attr('disabled','disabled');
				}

				if ( $("input[name='village']").data('fastselect') ) {
					$("input[name='village']").data('fastselect').destroy();
					$("input[name='village']").addClass('form-control').val('').attr('disabled','disabled');
				}
				if (!selected_data) {
					
					$("input[name='zipcode']").val('').attr('disabled','disabled');
					$("textarea[name='address']").val('').attr('disabled','disabled');
				}
				
				load_districts( district_value, selected_data );
				$("input[name='province']").valid();
	}

	
	// load districts
	function load_districts( id, selected_data=null ){

		var selector = $("input[name='district']");
		selector.removeClass('form-control').removeAttr('disabled');

		if ( selector.data('fastselect') ) {
			selector.data('fastselect').destroy();
		}

		var initialValue_data = (selected_data && selected_data.district) ? [{"text": selected_data.district.text, "value":selected_data.district.value}] : null;

		selector.fastselect({
			url: base_url+'api/wilayah/city/'+id,
			userOptionAllowed: true,
			initialValue: initialValue_data,
			onItemSelect: function($item, itemModel){
				trigger_load_sub_district( itemModel.value, selected_data );
				$("input[name='district']").valid();
			}
		});

		if ( initialValue_data ) {
			selector.val( selected_data.district.value );
			trigger_load_sub_district( selected_data.district.value, selected_data );
		}
	}

	function trigger_load_sub_district( sub_district_value, selected_data=null ){
				if ( $("input[name='sub-district']").data('fastselect') ) {
					$("input[name='sub-district']").data('fastselect').destroy();
					$("input[name='sub-district']").addClass('form-control').val('').attr('disabled','disabled');
				}
				if ( $("input[name='village']").data('fastselect') ) {
					$("input[name='village']").data('fastselect').destroy();
					$("input[name='village']").addClass('form-control').val('').attr('disabled','disabled');
				}

				if (!selected_data) {
					
					$("input[name='zipcode']").val('').attr('disabled','disabled');
					$("textarea[name='address']").val('').attr('disabled','disabled');
				}
				
				load_sub_districts( sub_district_value, selected_data );
				$("input[name='district']").valid();
	}

	// load districts
	function load_sub_districts( id, selected_data=null ){
		var selector = $("input[name='sub-district']");
		selector.removeClass('form-control').removeAttr('disabled');

		if ( selector.data('fastselect') ) {
			selector.data('fastselect').destroy();
		}

		var initialValue_data = (selected_data && selected_data.subdistrict) ? [{"text": selected_data.subdistrict.text, "value":selected_data.subdistrict.value}] : null;

		selector.fastselect({
			url: base_url+'api/wilayah/subdistrict/'+id,
			userOptionAllowed: true,
			initialValue: initialValue_data,
			onItemSelect: function($item, itemModel){
				trigger_load_village( itemModel.value );
				$("input[name='sub-district']").valid();
			}
		});

		if ( initialValue_data ) {
			selector.val( selected_data.subdistrict.value );
			trigger_load_village( selected_data.subdistrict.value, selected_data );
		}

	}

	function trigger_load_village( village_value, selected_data=null ){
				if ( $("input[name='village']").data('fastselect') ) {
					$("input[name='village']").data('fastselect').destroy();
					$("input[name='village']").addClass('form-control').val('').attr('disabled','disabled');
				}

				if (!selected_data) {
					
					$("input[name='zipcode']").val('').attr('disabled','disabled');
					$("textarea[name='address']").val('').attr('disabled','disabled');
				}
				
				load_villages( village_value, selected_data );
				$("input[name='sub-district']").valid();
	}

	// load districts
	function load_villages( id, selected_data=null ){
		var selector = $("input[name='village']");
		selector.removeClass('form-control').removeAttr('disabled');

		if ( selector.data('fastselect') ) {
			selector.data('fastselect').destroy();
		}

		var initialValue_data = (selected_data && selected_data.village) ? [{"text": selected_data.village.text, "value":selected_data.village.value}] : null;

		selector.fastselect({
			url: base_url+'api/wilayah/village/'+id,
			userOptionAllowed: true,
			initialValue: initialValue_data,
			onItemSelect: function($item, itemModel){
				load_address( itemModel.value );
				$("input[name='village']").valid();
			}
		});

		if ( selected_data ) {
			selector.val( selected_data.village.value );
			load_address( id, selected_data );
		}

	}

	// load villages
	function load_address( id, selected_data=null ){

		$("textarea[name='address'], input[name='zipcode']").removeAttr('disabled');
	}