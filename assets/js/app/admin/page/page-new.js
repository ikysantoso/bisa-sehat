$(document).ready(function(){
    var editor = CKEDITOR.replace( 'caption' );

    editor.on('text-change', function() {
        $("input[name='description']").val( $('.editor').html() );
    });

    // set initial value
    $("input[name='description']").val( $('.editor').html() )

    $('.singleInputDynamic').fastselect({
        onItemSelect: function($item, itemModel){
            $('.singleInputDynamic').valid();
        }
    });

    // add product
    var form_element = $('#form-page');

    form_element.on( 'change', '.input-img', function(){
        $(this).parent().css('background-image','url('+window.URL.createObjectURL(this.files[0])+')');
        $(this).parent().find('button').hide();
        $(this).parent().find('.btn-remove-img').show();
    });

    // remove preview image
    form_element.on('click','.btn-remove-img',function(){
        $(this).parent().find('input[type="file"]').val('');
        $(this).parent().css('background-image','none');
        $(this).parent().find('button').show();
        $(this).parent().find('.btn-remove-img').hide();

        form_element.find('input[name="current_image"]').val('');
    });

    form_element.validate({
		ignore: ".ql-container *",
        rules: {},
        highlight: function(element) {
        	$(element).closest('.form-group').removeClass('success').addClass('error');
        },
       	success: function(element) {
            $(element).closest('.form-group').removeClass('error');
        	element.remove();
		},
        submitHandler: function(form) {
            $('body').addClass('loading');

            var submit_url = (form_element.find('input[name="id"]').val()) ? base_url+'admin/page/update' : base_url+'admin/page/save';

            $.ajax({
                type: 'post',
                url: submit_url,
                enctype: 'multipart/form-data',
                data: new FormData(form_element[0]),
                cache:false,
                contentType: false,
                processData: false,
                traditional: true,
                success: function(response) {
                	$('body').removeClass('loading');
                    if ( response ) {

                        if ( response.status == 'success' ) {
							location.href=base_url+'admin/page/list';
						} else {
							// get error field
                            var error_field = '';
                            $.each( response.data, function(index, item){
                            	error_field += '<p>'+item+'</p>';
                            });

                            Swal.fire({
                                title: 'Form belum lengkap',
                                html: error_field,
                            	type: 'warning',
                            });
                                            
						}

					} else {
                        Swal.fire({
                        	title: 'Error',
                            text: 'Kesalahan pada server',
                            icon: 'warning',
                       	});
                    }
				}
        	});
        }
    });
});