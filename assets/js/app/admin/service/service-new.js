$(document).ready(function(){
	// quill editor init
    var quill = new Quill('#editor', {
        theme: 'snow'
    });
    quill.on('text-change', function() {
        $("input[name='description']").val( $('.ql-editor').html() );
    });

    // set initial value
    $("input[name='description']").val( $('.ql-editor').html() )

    $('.singleInputDynamic').fastselect({
        onItemSelect: function($item, itemModel){
            $('.singleInputDynamic').valid();
        }
    });

    // add product
    var form_element = $('#form-service');
    form_element.validate({
		ignore: ".ql-container *",
        rules: {},
        highlight: function(element) {
        	$(element).closest('.form-group').removeClass('success').addClass('error');
        },
       	success: function(element) {
            $(element).closest('.form-group').removeClass('error');
        	element.remove();
		},
        submitHandler: function(form) {
            $('body').addClass('loading');

            var submit_url = (form_element.find('input[name="id"]').val()) ? base_url+'admin/service/update' : base_url+'admin/service/save';

            $.ajax({
                type: 'post',
                url: submit_url,
                enctype: 'multipart/form-data',
                data: new FormData(form_element[0]),
                cache:false,
                contentType: false,
                processData: false,
                traditional: true,
                success: function(response) {
                	$('body').removeClass('loading');
                    if ( response ) {

                        if ( response.status == 'success' ) {
							location.href=base_url+'admin/service/list';
						} else {
							// get error field
                            var error_field = '';
                            $.each( response.data, function(index, item){
                            	error_field += '<p>'+item+'</p>';
                            });

                            Swal.fire({
                                title: 'Form belum lengkap',
                                html: error_field,
                            	type: 'warning',
                            });
                                            
						}

					} else {
                        Swal.fire({
                        	title: 'Error',
                            text: 'Kesalahan pada server',
                            icon: 'warning',
                       	});
                    }
				}
        	});
        }
    });

    includeJs( base_url+'assets/js/app/admin/service/service-image-field.js' );

});