$(document).ready(function(){
	var tableData = $('#table-service-list');
	tableData.DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": {
        	'url': base_url+'admin/service/list',
        	'type': 'POST'
        },
        "columnDefs": [ 
            { "targets": [ 4 ], "visible": false },
        ],
        "createdRow": function( row, data, dataIndex ) {
            $(row).find('.btn-delete').attr( 'data-id', data[4] );
            $(row).find('.btn-edit').attr( 'href', base_url+'admin/service/edit/'+data[4] );
        },
        "language": {
            "paginate": {
                "previous": "<i class='fas fa-angle-left'></i>",
                "next": "<i class='fas fa-angle-right'></i>"
            }
        }
    });

    tableData.on('click','.btn-delete', function(){
        var id = $(this).attr('data-id');

        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                $('body').addClass('loading');

                $.ajax({
                    type: 'post',
                    url: base_url+'admin/service/delete',
                    data: {
                        id: id
                    },
                    success: function(response) {
                        $('body').removeClass('loading');
                        if ( response ) {
                            tableData.DataTable().ajax.reload();
                        }
                    }
                });
            }
        });
    })
});