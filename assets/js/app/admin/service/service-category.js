$(document).ready(function(){
	var tableData = $('#table-service-category-list');
	tableData.DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": {
        	'url': base_url+'admin/service/category',
        	'type': 'POST'
        },
        "columnDefs": [ 
            { "targets": [ 3 ], "visible": false },
        ],
        "createdRow": function( row, data, dataIndex ) {
            $(row).find('.btn-delete').attr( 'data-id', data[3] );
            $(row).find('.btn-edit').attr( 'data-id', data[3] );
        },
        "language": {
            "paginate": {
                "previous": "<i class='fas fa-angle-left'></i>",
                "next": "<i class='fas fa-angle-right'></i>"
            }
        }
    });

	var form_element = $("#form-service-category");
	form_element.validate({
		rules: {},
        highlight: function(element) {
        	$(element).closest('.form-group').removeClass('success').addClass('error');
        },
		success: function(element) {
            $(element).closest('.form-group').removeClass('error');
        	element.remove();
        },
       	submitHandler: function(form) {
            
            var submit_url = (form_element.find('input[name="id"]').val() != '') ? base_url+'admin/service/category_update' : base_url+'admin/service/category_save';
            
            $('body').addClass('loading');
            $.ajax({
            	type: 'post',
                url: submit_url,
                data: form_element.serialize(),
                success: function(response) {
                    $('body').removeClass('loading');
                    if ( response ) {
                    	$("#modal-service-category").modal('hide');
                        tableData.DataTable().ajax.reload();
                	}
                }
        	});
        }
	});

    // delete product category
    tableData.on('click', '.btn-delete', function(){
        var id = $(this).attr( 'data-id' );

        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                $('body').addClass('loading');

                $.ajax({
                    type: 'post',
                    url: base_url+'admin/service/category_delete',
                    data: {
                        id: id
                    },
                    success: function(response) {
                        $('body').removeClass('loading');
                        if ( response ) {
                            tableData.DataTable().ajax.reload();
                        }
                    }
                });
            }
        });
    });

    tableData.on('click', '.btn-edit', function(){
        var id = $(this).attr('data-id');
        $('body').addClass('loading');

        $.ajax({
            type: 'post',
            url: base_url+'admin/service/category_detail',
            data: {
                id: id
            },
            success: function(response) {
                $('body').removeClass('loading');
                if ( response ) {
                    $("#form-service-category").find("input[name='id']").val( response.id );
                    $("#form-service-category").find("input[name='name']").val( response.name );
                    $("#modal-service-category").modal('show');
                }
            }
        });
    });

    $("#modal-service-category").on('hidden.bs.modal', function (e) {
        $("#form-service-category").find("input[name='id']").val( '' );
        $("#form-service-category").trigger('reset');
    });
});