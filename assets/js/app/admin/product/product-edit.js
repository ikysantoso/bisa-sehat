$(document).ready(function() {
    $('.singleInputDynamic').fastselect();

    // quill editor init
    var quill = new Quill('#editor', {
        theme: 'snow'
    });
    quill.on('text-change', function() {
        $("input[name='description']").val( $('.ql-editor').html() );
    });
    // set initial value
    $("input[name='description']").val( $('.ql-editor').html() )

    $("input[name='is_preorder']").change(function(){
        if ( $(this).is(':checked') ) {
            $("#preorder-time").removeClass('d-none');
            $("#preorder-time input[name='preorder-time']").attr('required', 'required');
            $("#form-stock input[name='stock']").val('0').attr('readonly','readonly');
        } else {
            $("#preorder-time").addClass('d-none');
            $("#preorder-time input[name='preorder-time']").removeAttr('required');
            $("#form-stock input[name='stock']").val('').removeAttr('readonly');
        }
    });
    
    includeJs( base_url+'assets/js/app/admin/product/product-image-field.js' );
    includeJs( base_url+'assets/js/app/admin/product/product-stock-field.js' );
    generate_stock_list();

    // edit product
    var form_element = $('#form-add-product');
    form_element.validate({
                        ignore: ".ql-container *",
                        rules: {},
                        highlight: function(element) {
                            $(element).closest('.form-group').removeClass('success').addClass('error');
                        },
                        success: function(element) {
                            $(element).closest('.form-group').removeClass('error');
                            element.remove();
                        },
                        submitHandler: function(form) {
                            $('body').addClass('loading');
                            $.ajax({
                                type: 'post',
                                url: base_url+'admin/product/update',
                                enctype: 'multipart/form-data',
                                data: new FormData(form_element[0]),
                                cache:false,
                                contentType: false,
                                processData: false,
                                traditional: true,
                                success: function(response) {
                                    $('body').removeClass('loading');
                                    if ( response ) {
                                        Swal.fire({
                                            title: 'Update produk berhasil',
                                            text: '',
                                            type: 'success',
                                        }).then((result) => {
                                            $('body').addClass('loading');
                                            location.href=base_url+'admin/product/list';
                                        });
                                    } else {
                                        Swal.fire({
                                            title: 'Error',
                                            text: 'Kesalahan pada server',
                                            icon: 'warning',
                                        });
                                    }
                                }
                            });
                        }
    });
});