$(document).ready(function(){
	$("#btn-add-new").click(function(){
		$("#modal-warehouse").modal('show');
	});

	var tableData = $('#table-warehouse-list');
	var form_element = $('#form-warehouse');

	includeJs( base_url+'assets/js/app/components/addressForm.js' );
	load_fast_select();


    form_element.validate({
		rules: {},
        highlight: function(element) {
        	$(element).closest('.form-group').removeClass('success').addClass('error');
        },
		success: function(element) {
            $(element).closest('.form-group').removeClass('error');
        	element.remove();
        },
		submitHandler: function(form) {
			$('body').addClass('loading');

            var id = form_element.find('input[name="id"]').val();

			var submit_url = (id) ? base_url+'admin/product/warehouse_update' : base_url+'admin/product/warehouse_save';
            var shippingMethodElement = $('input[class=shipping-method-checkbox]:checked');

            var shippingMethodValue = [];
            $.each(shippingMethodElement, function(){
                shippingMethodValue.push($(this).val());
            });

            if (shippingMethodValue.length > 1 && shippingMethodValue.includes("Close")) {
                $('body').removeClass('loading');

                var alertTitle = "Error";
                var alertBody = "Jika ingin close warehouse, harap tidak memilih opsi pengiriman yang lain";
                var alertIcon = "error";

                Swal.fire(alertTitle, alertBody, alertIcon);
            } else {
                var formElementData = {};

                formElementData.id = id;

                $.each(form_element.serializeArray(), function() {
                    formElementData[this.name] = this.value;

                    if (this.name == "sub-district") {
                        formElementData['subDistrict'] = this.value;
                    }
                });

                formElementData.shipping = shippingMethodValue;

                $.ajax({
                    type: 'post',
                    url: submit_url,
                    data: {
                        formData: formElementData
                    },
                    success: function(response) {
                        $('body').removeClass('loading');
                        $("#modal-warehouse").modal('hide');
                        tableData.DataTable().ajax.reload();
                    },
                    error: function(xhr){
                        if (xhr.status==500) {
                            Swal.fire( 'Error '+xhr.status, 'warehouse tidak ditemukan', 'warning' );
                            $('body').removeClass('loading');
                        }
                    }
                });
            }
        }
    });

    tableData.DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": {
        	'url': base_url+'admin/product/warehouse',
        	'type': 'POST'
        },
        "columnDefs": [
            { "targets": [ 3 ], "visible": false },
        ],
        "createdRow": function( row, data, dataIndex ) {
            $(row).find('.btn-delete').attr( 'data-id', data[3] );
            $(row).find('.btn-edit').attr( 'data-id', data[3] );
        },
        "language": {
            "paginate": {
                "previous": "<i class='fas fa-angle-left'></i>",
                "next": "<i class='fas fa-angle-right'></i>"
            }
        }
    });

    // delete warehouse
    tableData.on('click', '.btn-delete', function(){
        var id = $(this).attr( 'data-id' );

        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: 'post',
                    url: base_url+'admin/product/warehouse_delete',
                    data: {
                        id: id
                    },
                    success: function(response) {
                        $('body').removeClass('loading');
                        if ( response ) {
                            Swal.fire(
                                'Deleted!',
                                'Your warehouse has been deleted.',
                                'success'
                            );
                            tableData.DataTable().ajax.reload();
                        }
                    }
                });
            }
        });
    });

    tableData.on('click', '.btn-edit', function(){
        $('body').addClass('loading');
    	var id = $(this).attr('data-id');
    	$.ajax({
			type: 'post',
            url: base_url+'admin/product/warehouse_detail',
            data: {
            	id: id
            },
           	success: function(response) {
                $('body').removeClass('loading');
                if ( response ) {
                	form_element.find('input[name="id"]').val(response.id);
                	form_element.find('input[name="name"]').val(response.name);
                	form_element.find('input[name="village"]').val(response.village_text);
                	form_element.find('textarea[name="address"]').val(response.address);
                	form_element.find('input[name="zipcode"]').val(response.zipcode);

                    if (response.shipping != null) {
                        let shippingMethod = JSON.parse(response.shipping);

                        for (let i = 0; i < shippingMethod.length; i++) {
                            $("input[value='"+shippingMethod[i]+"']").prop('checked', true);
                        }
                    }

                	load_fast_select(
						true,
						{
							'province': response.province,
							'district': response.district,
							'subdistrict': response.subdistrict,
						}
					);

					$("#modal-warehouse").modal('show');
                    $(".modal-title").html("Edit warehouse");
            	}
            }
		});
    });

    $('#modal-warehouse').on('hidden.bs.modal', function (e) {
    	form_element.trigger('reset');
    	$("input[name='id']").val('');
    	$(".fstResultItem").first().trigger('click');
    	load_fast_select( true );
	})

});