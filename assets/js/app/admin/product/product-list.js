$(document).ready(function() {
    var table_product_list = $('#table-product-list');

    table_product_list.DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": {
        	'url': base_url+'admin/product/list',
        	'type': 'POST'
        },
        "columnDefs": [
            { "targets": [ 4 ], "visible": false },
            { "targets": [ 3,6,7,8,9 ], "className": "text-center align-middle" },
            {
                "targets": [2],
                "className": "text-right align-middle",
                "render": function ( data, type, row, meta ) {
                    if (data) {
                        return 'Rp '+data.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
                    } else {
                        return '-';
                    }
                },
            },
            {
                "targets": [5],
                "render": function ( data, type, row, meta ) {
                    if (data) {
                        return 'Rp '+data.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
                    } else {
                        return '-';
                    }
                },
            }

        ],
        "createdRow": function( row, data, dataIndex ) {
            $(row).find('.btn-delete').attr( 'data-id', data[4] );
            $(row).find('.btn-edit').attr( 'href', base_url+'admin/product/edit/'+data[4] );
        },
        "language": {
            "paginate": {
                "previous": "<i class='fas fa-angle-left'></i>",
                "next": "<i class='fas fa-angle-right'></i>"
            }
        }
    });

    table_product_list.on('click', '.btn-delete', function(){
        var id = $(this).attr( 'data-id' );

        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {

                $.ajax({
                    type: 'post',
                    url: base_url+'admin/product/delete',
                    data: {
                        id: id
                    },
                    success: function(response) {
                        $('body').removeClass('loading');
                        if ( response ) {
                            Swal.fire(
                                'Deleted!',
                                'Your product has been deleted.',
                                'success'
                            );
                            table_product_list.DataTable().ajax.reload();
                        }
                    }
                });
            }
        });
    });
});