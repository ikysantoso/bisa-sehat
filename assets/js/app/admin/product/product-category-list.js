$(document).ready(function() {
    $('#table-product-category-list').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": {
        	'url': base_url+'admin/product/category',
        	'type': 'POST'
        },
        "columnDefs": [ 
            { "targets": [ 2 ], "visible": false },
        ],
        "createdRow": function( row, data, dataIndex ) {
            $(row).find('.btn-delete').attr( 'data-id', data[2] );
            $(row).find('.btn-edit').attr( 'data-id', data[2] );
        },
        "language": {
            "paginate": {
                "previous": "<i class='fas fa-angle-left'></i>",
                "next": "<i class='fas fa-angle-right'></i>"
            }
        }
    });

    // product category add
    $("#btn-add-new").click(function(){
        $("#modal-title-default").html('<i class="fas fa-plus"></i> Add new product category');
        $("#modal-add-new").modal('show');
    });

    var form_element = $('#form-add-product-category');
    form_element.validate({
                        
                        rules: {},
                        highlight: function(element) {
                            $(element).closest('.form-group').removeClass('success').addClass('error');
                        },
                        success: function(element) {
                            $(element).closest('.form-group').removeClass('error');
                            element.remove();
                        },
                        submitHandler: function(form) {
                            $('body').addClass('loading');

                            var url_submit = (form_element.find( 'input[name="id"]' ).val()) ? base_url+'admin/product/update_category' : base_url+'admin/product/save_category';

                            $.ajax({
                                type: 'post',
                                url: url_submit,
                                enctype: 'multipart/form-data',
                                data: new FormData(form_element[0]),
                                cache:false,
                                contentType: false,
                                processData: false,
                                traditional: true,
                                success: function(response) {
                                    $('body').removeClass('loading');
                                    if ( response ) {
                                        form_element.trigger('reset');
                                        $(".btn-remove-img").trigger('click');
                                        $("#modal-add-new").modal('hide');
                                        $('#table-product-category-list').DataTable().ajax.reload();
                                    } else {
                                        Swal.fire({
                                            title: 'Error',
                                            text: 'Kesalahan pada server',
                                            icon: 'warning',
                                        });
                                    }
                                }
                            });
                        }
    });

    // delete product category
    $('#table-product-category-list').on('click', '.btn-delete', function(){
        var id = $(this).attr( 'data-id' );

        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {

                $.ajax({
                    type: 'post',
                    url: base_url+'admin/product/delete_category',
                    data: {
                        id: id
                    },
                    success: function(response) {
                        $('body').removeClass('loading');
                        if ( response ) {
                            Swal.fire(
                                'Deleted!',
                                'Your product category has been deleted.',
                                'success'
                            );
                            $('#table-product-category-list').DataTable().ajax.reload();
                        }
                    }
                });
            }
        });
    });

    // edit product category
    $('#table-product-category-list').on('click', '.btn-edit', function(){
        var id = $(this).attr( 'data-id' );
        $("#modal-title-default").html('<i class="fas fa-edit"></i> Edit product category');
        // load data
        $.ajax({
            type: 'post',
            url: base_url+'admin/product/product_category_detail',
            data: {
                id: id
            },
            success: function(response) {
                $('body').removeClass('loading');
                if ( response ) {
                    form_element.find('input[name="name"]').val( response.name );
                    form_element.find('input[name="id"]').val( response.id );
                    if ( response.image ) {
                        form_element.find('.embed-responsive').css('background-image', 'url("'+base_url+'/assets/images/upload/product_category/'+response.image+'")');
                        form_element.find('.input-img').parent().find('button').hide();
                        form_element.find('.input-img').parent().find('.btn-remove-img').show();
                    } else {
                        $(".btn-remove-img").trigger('click');
                    }
                    $("#modal-add-new").modal('show');
                }
            }
        });
    });

    // preview image
    var column_html = ($("input[name='product_id']").val()) ? '<div class="input-img-item mb-3 col">'+$('#field-image').html()+'</div>' : $('.input-img-container').html();
    form_element.on( 'change', '.input-img', function(){
        $(this).parent().css('background-image','url('+window.URL.createObjectURL(this.files[0])+')');
        $(this).parent().find('button').hide();
        $(this).parent().find('.btn-remove-img').show();
    });

    // remove preview image
    form_element.on('click','.btn-remove-img',function(){
        $(this).parent().find('input[type="file"]').val('');
        $(this).parent().css('background-image','none');
        $(this).parent().find('button').show();
        $(this).parent().find('.btn-remove-img').hide();
    });
    
});