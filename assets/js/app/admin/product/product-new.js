$(document).ready(function() {
	$("#btn-add-new").click(function(){
    	$("#modal-add-new").modal('show');
    });

    $('.singleInputDynamic').fastselect({
        onItemSelect: function($item, itemModel){
            $('.singleInputDynamic').valid();
        }
    });

    var quill = new Quill('#editor', {
        theme: 'snow'
    });

    quill.on('text-change', function() {
        $("input[name='description']").val( $('.ql-editor').html() );
    });

    $("input[name='is_preorder']").change(function(){
        if ( $(this).is(':checked') ) {
            $("#preorder-time").removeClass('d-none');
            $("#preorder-time input[name='preorder-time']").attr('required', 'required');
            $("#form-stock input[name='stock']").val('0').attr('readonly','readonly');
        } else {
            $("#preorder-time").addClass('d-none');
            $("#preorder-time input[name='preorder-time']").removeAttr('required');
            $("#form-stock input[name='stock']").val('').removeAttr('readonly');
        }
    });

    // add product
    var form_element = $('#form-add-product');
    form_element.validate({
                        ignore: ".ql-container *",
                        rules: {},
                        highlight: function(element) {
                            $(element).closest('.form-group').removeClass('success').addClass('error');
                        },
                        success: function(element) {
                            $(element).closest('.form-group').removeClass('error');
                            element.remove();
                        },
                        submitHandler: function(form) {
                            $('body').addClass('loading');
                            $.ajax({
                                type: 'post',
                                url: base_url+'admin/product/save',
                                enctype: 'multipart/form-data',
                                data: new FormData(form_element[0]),
                                cache:false,
                                contentType: false,
                                processData: false,
                                traditional: true,
                                success: function(response) {
                                    $('body').removeClass('loading');
                                    if ( response ) {
                                        if ( response.status == 'success' ) {
                                            // reset form
                                            form_element.trigger( 'reset' );
                                            $(".btn-remove-img").trigger('click');
                                            quill.setContents([
                                                { insert: '\n' }
                                            ]);

                                            // reinit fastselect
                                            $('.singleInputDynamic').data('fastselect').destroy();
                                            $('.singleInputDynamic').fastselect();

                                            // hide modal
                                            $("#modal-add-new").modal('hide');

                                            // refresh table
                                            $('body').addClass('loading');
                                            if ( response.redirect ) {
                                                location.href=response.redirect;
                                            } else {
                                                location.href=base_url+'admin/product/list';
                                            }
                                        } else {

                                            // get error field
                                            var error_field = '';
                                            $.each( response.data, function(index, item){
                                                error_field += '<p>'+item+'</p>';
                                            });

                                            Swal.fire({
                                                title: 'Form belum lengkap',
                                                html: error_field,
                                                type: 'warning',
                                            });

                                        }

                                    } else {
                                        Swal.fire({
                                            title: 'Error',
                                            text: 'Kesalahan pada server',
                                            icon: 'warning',
                                        });
                                    }
                                }
                            });
                        }
    });

    includeJs( base_url+'assets/js/app/admin/product/product-image-field.js' );
    includeJs( base_url+'assets/js/app/admin/product/product-stock-field.js' );
});