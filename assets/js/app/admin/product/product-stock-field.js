    $("#btn-add-stock").click(function(){
        $("#modal-stock").modal('show');
    });

    $("#form-stock").validate({
        rules: {},
        highlight: function(element) {
            $(element).closest('.form-group').removeClass('success').addClass('error');
        },
        success: function(element) {
            $(element).closest('.form-group').removeClass('error');
            element.remove();
        },
        submitHandler: function(form) {
            var selected_stocks = ($("input[name='stocks']").val()) ? JSON.parse($("input[name='stocks']").val()) : [];
            var warehouse = $("#form-stock select[name='warehouse'] option:selected");
            var stock = $("#form-stock input[name='stock']").val();

            selected_stocks.push(
                {
                    "warehouse_id": warehouse.val(),
                    "warehouse_name": warehouse.text(),
                    "stock": stock   
                }
            );

            $("input[name='stocks']").val( JSON.stringify( selected_stocks ) );
            $("#modal-stock").modal('hide');

            generate_stock_list();
        }
    });

    function generate_stock_list(){
        $("#stock-list").html('');

        var selected_stocks = ($("input[name='stocks']").val()) ? JSON.parse($("input[name='stocks']").val()) : [];
            
        $.each(selected_stocks, function(index, item){

            var stock = (item.stock < 0) ? 0 : item.stock; 

            $("#stock-list").append(`
                <li data-id="`+item.warehouse_id+`" class="list-group-item d-flex justify-content-between align-items-center">
                    <div>`+item.warehouse_name+`</div>
                    <div>
                        <div class="input-group" style="width:150px">
                            <div class="input-group-prepend btn-decrease">
                                <span class="input-group-text">-</span>
                            </div>
                            <input type="text" value="`+stock+`" class="h-auto text-center form-control w-auto stock-field">
                            <div class="input-group-append btn-increase">
                                <span class="input-group-text">+</span>
                            </div>
                            <button type="button" class="ml-3 btn btn-sm btn-danger btn-delete"><i class="fas fa-trash"></i></button>
                        </div>

                    </div>
                </li>
            `);
        });
    }

    $('#modal-stock').on('hidden.bs.modal', function (e) {
        $("#form-stock").trigger('reset');
    });

    $('#modal-stock').on('shown.bs.modal', function (e) {
        // check selected_warehouse
        var selected_warehouse = [];
        var warehouse_data = ($("input[name='stocks']").val()) ? JSON.parse($("input[name='stocks']").val()): [];
        $.each(warehouse_data, function(index, item){
            selected_warehouse.push( parseInt(item.warehouse_id) );
        });

        console.log(selected_warehouse);

        var items = $("#form-stock select[name='warehouse']").find('option');
        $.each(items, function(index,item){
            if ( index > 0 ) {
                $(this).prop('disabled', false);
                $(this).removeClass('bg-light');
                if ( selected_warehouse.includes( parseInt( $(this).val() ) ) ) {
                    $(this).prop('disabled', true);
                    $(this).addClass('bg-light');
                }
            }
        });
    });

    // remove stocklist
    $("#stock-list").on('click','.btn-delete',function(){

        Swal.fire({
            title: 'Are you sure?',
            text: "Delete product from this warehouse?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes'
        }).then((result) => {
            if (result.value) {
            
                var current_id = $(this).parent().parent().parent().attr('data-id');

                var selected_stocks = [];
                $.each(JSON.parse($("input[name='stocks']").val()), function(index, item){

                    if (item.warehouse_id != current_id) {
                        selected_stocks.push(
                            {
                                "warehouse_id": item.warehouse_id,
                                "warehouse_name": item.warehouse_name,
                                "stock": item.stock   
                            }
                        );
                    }

                });

                $("input[name='stocks']").val( JSON.stringify( selected_stocks ) );
                $("#stock-list").find("li[data-id='"+current_id+"']").remove();
            }
        });

    });

    $("#stock-list").on('click','.btn-increase',function(){
        var current_id = $(this).parent().parent().parent().attr('data-id');
        var current_stock = $(this).parent().find('.stock-field').val();

        update_stock(current_id, parseInt(current_stock) + 1)
    });

    $("#stock-list").on('click','.btn-decrease',function(){
        var current_id = $(this).parent().parent().parent().attr('data-id');
        var current_stock = parseInt($(this).parent().find('.stock-field').val());
        var btn_delete = $(this).parent().find('.btn-delete');

        if ( current_stock == "0" ) {

            btn_delete.trigger('click');
            
        } else {
            update_stock(current_id, current_stock - 1)
        }
    });

    // prevent submit by enter
    $(document).on('keyup keypress', '.stock-field', function(e) {
        if(e.keyCode == 13) {
            e.preventDefault();
            return false;
        }
    });

    $("#stock-list").on('change','.stock-field',function(e){
        var current_id = $(this).parent().parent().parent().attr('data-id');
        var new_value = $(this).val();
        if ( new_value < 0 ) {
            new_value = 0;

        }
        update_stock(current_id, new_value);
    });

    function update_stock(current_id, new_value){
        $("#stock-list").find('li[data-id="'+current_id+'"]').find(".stock-field").val( new_value );

        var selected_stocks = [];
        $.each(JSON.parse($("input[name='stocks']").val()), function(index, item){

            selected_stocks.push(
                {
                    "warehouse_id": item.warehouse_id,
                    "warehouse_name": item.warehouse_name,
                    "stock": (item.warehouse_id == current_id) ? new_value : item.stock   
                }
            );

        });

        $("input[name='stocks']").val( JSON.stringify( selected_stocks ) );
    }