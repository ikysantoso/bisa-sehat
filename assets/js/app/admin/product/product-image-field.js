    // preview image
	var column_html = ($("input[name='product_id']").val()) ? '<div class="input-img-item mb-3 col">'+$('#field-image').html()+'</div>' : $('.input-img-container').html();
    $("#form-add-product").on( 'change', '.input-img', function(){
        $(this).parent().css('background-image','url('+window.URL.createObjectURL(this.files[0])+')');
        $(this).parent().find('button').hide();
        $(this).parent().find('.btn-remove-img').show();

        $(this).parent().parent().parent().append( column_html );
		
		image_grid_generator();
        
    });

    // remove preview image
    $("#form-add-product").on('click','.btn-remove-img',function(){
        $(this).parent().find('input[type="file"]').val('');
        $(this).parent().css('background-image','none');
        $(this).parent().find('button').show();
        $(this).parent().find('.btn-remove-img').hide();

        var current_image_id = $(this).attr('data-id');

        var items = $('.input-img-container').find('.input-img-item');
        if ( items.length > 1 ) {
        	$(this).parent().parent().remove();

            // if product edit mode -> remove current image list
            var current_product_images = $("input[name='current_images']").val();
            if ( current_product_images ) {
                var new_image_id = [];
                $.each(current_product_images.split(','), function(index, image_id){
                    if ( image_id != current_image_id ) {
                        new_image_id.push( image_id );
                    }
                });
                $("input[name='current_images']").val( new_image_id.join(",") );
            }
        }

        image_grid_generator();
    });

    // image grid generator
    function image_grid_generator(){
    	var items = $('.input-img-container').find('.input-img-item');
		var item_class = '';
		if ( items.length == 1) {
			item_class = 'col';
		} else if ( items.length > 1 && items.length < 5 ) {
			item_class = 'col-6';
		} else if ( items.length > 4 && items.length < 10 ) {
			item_class = 'col-4';
		} else {
			item_class = 'col-3';
		}
		$.each( items, function(index, item){
			$(item).removeAttr('class');
			$(item).addClass('input-img-item mb-3 '+item_class);
		});
    }
