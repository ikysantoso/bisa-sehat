$(document).ready(function() {
	var form_element = $("#form-medical-checkup");

	form_element.validate({
        rules: {},
        highlight: function(element) {
        	$(element).closest('.form-group').removeClass('success').addClass('error');
		},
	    success: function(element) {
	    	$(element).closest('.form-group').removeClass('error');
	    	element.remove();
	    },
		submitHandler: function(form) {
            $('body').addClass('loading');
			$.ajax({
                type: 'post',
                url: base_url+'admin/medical_checkup/save',
                data: form_element.serialize(),
                success: function(response) {
                    $('body').removeClass('loading');
                    if ( response ) {
                    	location.href = base_url+'admin/medical_checkup/list';
                    } else {
                        Swal.fire({
                            title: 'Error',
                            text: 'Kesalahan pada server',
                            type: 'warning',
                        });
                    }
                }
            });
		}
	});
});