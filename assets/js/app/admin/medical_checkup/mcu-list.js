$(document).ready(function() {
    var tableData = $('#table-user-list');
    tableData.DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": {
        	'url': base_url+'admin/medical_checkup/list',
        	'type': 'POST'
        },
        "columnDefs": [ 
            { "targets": [ 5,6,7,8,9 ], "visible": false },
            { "targets": [ 0,10 ], "orderable": false },
            { "targets": [ 3,4 ], "className": 'text-center' },
        ],
        "createdRow": function( row, data, dataIndex ) {
            $(row).find('.btn-new').attr( 'href', base_url+'admin/medical_checkup/new/'+data[5] );
            $(row).find('.btn-detail').attr( 'href', base_url+'admin/medical_checkup/detail/'+data[5] );
        },
        "language": {
            "paginate": {
                "previous": "<i class='fas fa-angle-left'></i>",
                "next": "<i class='fas fa-angle-right'></i>"
            }
        }
    });
});