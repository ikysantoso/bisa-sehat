$(document).ready(function(){
    $(".active_at_option").change(function(){
        active_option_trigger( $(this).val() );
    });

    active_option_trigger( $(".active_at_option:checked").val() );

    function active_option_trigger( option_value ){
        console.log(option_value);
        if ( option_value == 'start-date' ) {
            $('#daterange').hide();
            $('#datepicker').show();
            $("input[name='datepicker']").datepicker({
                uiLibrary: 'bootstrap4',
                format: 'dd-mm-yyyy'
            });
        } else if ( option_value == 'periode' ) {
            $('#daterange').show();
            $('#datepicker').hide();
            $('input[name="daterange"]').daterangepicker(
                {
                    locale: {
                      format: 'DD/MM/YYYY'
                    }
                }
            );
        } else if ( option_value == 'forever' ) {
            $('#daterange').hide();
            $('#datepicker').hide();
        }
    }

	var form_element = $("#form-slider");

    form_element.on( 'change', '.input-img', function(){
        $(this).parent().css('background-image','url('+window.URL.createObjectURL(this.files[0])+')');
        $(this).parent().find('button').hide();
        $(this).parent().find('.btn-remove-img').show();
    });

    // remove preview image
    form_element.on('click','.btn-remove-img',function(){
        $(this).parent().find('input[type="file"]').val('');
        $(this).parent().css('background-image','none');
        $(this).parent().find('button').show();
        $(this).parent().find('.btn-remove-img').hide();

        form_element.find('input[name="current_image"]').val('');
    });

    form_element.validate({
        rules: {},
        highlight: function(element) {
        	$(element).closest('.form-group').removeClass('success').addClass('error');
        },
       	success: function(element) {
            $(element).closest('.form-group').removeClass('error');
        	element.remove();
		},
        submitHandler: function(form) {
            $('body').addClass('loading');

            var submit_url = (form_element.find('input[name="id"]').val()) ? base_url+'admin/slider/update' : base_url+'admin/slider/save';

            $.ajax({
                type: 'post',
                url: submit_url,
                enctype: 'multipart/form-data',
                data: new FormData(form_element[0]),
                cache:false,
                contentType: false,
                processData: false,
                traditional: true,
                success: function(response) {
                	$('body').removeClass('loading');
                    if ( response ) {

                        if ( response.status == 'success' ) {
							location.href=base_url+'admin/slider/list';
						} else {
							// get error field
                            var error_field = '';
                            $.each( response.data, function(index, item){
                            	error_field += '<p>'+item+'</p>';
                            });

                            Swal.fire({
                                title: 'Form belum lengkap',
                                html: error_field,
                            	type: 'warning',
                            });
                                            
						}

					} else {
                        Swal.fire({
                        	title: 'Error',
                            text: 'Kesalahan pada server',
                            icon: 'warning',
                       	});
                    }
				}
        	});
        }
    });
});