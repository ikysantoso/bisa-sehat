$(document).ready(function(){
    
    var tableData = $("#table-wishorder");
    var form_element = $("#form-link-to-product");
    $('.singleInputDynamic').fastselect();

	tableData.DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": {
            'url': base_url+'admin/order/wishorder',
            'type': 'POST'
        },
        "columnDefs": [ 
            { "targets": [ 7,8 ], "visible": false },
            // { "targets": [ 2,5 ], "className": "align-middle" },
            // { "targets": [ 0,1,6 ], "className": "text-center align-middle" },
            // { "targets": [ 4 ], "className": "text-right align-middle" },
            // {
            //     "targets": [3],
            //     "className": "text-right align-middle",
            //     "render": function ( data, type, row, meta ) {
            //         if (data) {
            //             return 'Rp '+data.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
            //         } else {
            //             return '-';
            //         }
            //     },
            // },
            {
                "targets": [7],
                "render": function ( data, type, row, meta ) {
                    if (data) {
                        return '<a href="'+base_url+'/member/order/wishorder_buy/'+data+'" class="btn filled-button">Beli Sekarang</a>';
                    } else {
                        return 'Pengajuan';
                    }
                },
            },

            {
                "targets": [9],
                "render": function ( data, type, row, meta ) {
                    if ( row[7] ) {
                        return 'Linked Product: <a class="btn-link-product" href="#">#'+row[7]+'</a>';
                    } else {
                        return data;
                    }
                },
            },
        ],
        "createdRow": function( row, data, dataIndex ) {
            $(row).find('.btn-link-product').attr( 'data-id', data[8] );
            // $(row).find('.btn-edit').attr( 'data-id', data[3] );
        },
        "language": {
            "paginate": {
                "previous": "<i class='fas fa-angle-left'></i>",
                "next": "<i class='fas fa-angle-right'></i>"
            }
        },
        "initComplete": function(settings, json) {
            auto_load_modal();
        }
    });

    var current_product_id = null;
    tableData.on('click', '.btn-link-product', function(){
        var id = $(this).attr('data-id');
        current_product_id = ($(this).text()).replace(/\D/g,'');
        form_element.find( 'input[name="id"]' ).val( id );

        $("#btn-link-to-new-product").attr('data-id', id)
        $("#modal-link-to-product").modal("show");
    });

    form_element.validate({
        rules: {},
        highlight: function(element) {
            $(element).closest('.form-group').removeClass('success').addClass('error');
        },
        success: function(element) {
            $(element).closest('.form-group').removeClass('error');
            element.remove();
        },
        submitHandler: function(form) {
            $('body').addClass('loading');

            $.ajax({
                type: 'post',
                url: base_url+'admin/order/wishorder/link_to_product',
                data: form_element.serialize(),
                success: function(response) {
                    $('body').removeClass('loading');
                    $("#modal-link-to-product").modal('hide');
                    tableData.DataTable().ajax.reload();
                },
                error: function(xhr){
                    if (xhr.status==500) {
                        Swal.fire( 'Error '+xhr.status, 'warehouse tidak ditemukan', 'warning' );
                        $('body').removeClass('loading');
                    }
                }
            });
        }
    });

    $('#modal-link-to-product').on('hidden.bs.modal', function (e) {
        form_element.trigger('reset');
        form_element.find( 'input[name="id"]' ).val('');

        // reinit fastselect
        if ( $('.singleInputDynamic').data('fastselect') ) {
            $('.singleInputDynamic').data('fastselect').destroy();
            $('.singleInputDynamic').fastselect();
        }
    })

    $('#modal-link-to-product').on('shown.bs.modal', function (e) {
        form_element.find('input[name="product_id"]').val(current_product_id)

        $.ajax({
            type: 'post',
            url: base_url+'admin/product/product_fastselect/'+current_product_id,
            data: form_element.serialize(),
            success: function(response) {
                // reinit fastselect
                if ( $('.singleInputDynamic').data('fastselect') ) {
                    $('.singleInputDynamic').data('fastselect').destroy();
                    $('.singleInputDynamic').fastselect({
                        userOptionAllowed: true,
                        initialValue: response,
                    });
                }
            }
        });

        
    });


    $("#btn-link-to-new-product").click(function(){
        $('body').addClass('loading');
        location.href = base_url+'admin/product/new?wishorder='+$(this).attr('data-id');
    });

    // auto load modal by id ?id=
    $.urlParam = function(name){
        var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
        if (results) {
            return results[1] || 0;
        }
    }

    if ( $.urlParam('id') && $.urlParam('name') ) {
        tableData.DataTable().ajax.reload();
        $('#table-wishorder_filter input').val( decodeURIComponent($.urlParam('name')) ).trigger($.Event("keyup", { keyCode: 13 }));
    }
    function auto_load_modal(){
        tableData.find('.btn-link-product[data-id="'+$.urlParam('id')+'"]').trigger('click');
    }
});