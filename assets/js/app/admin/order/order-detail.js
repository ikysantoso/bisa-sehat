$(document).ready(function() {
	var form_element = $("#form-tracking-code");

	form_element.validate({
        rules: {},
        highlight: function(element) {
        	$(element).closest('.form-group').removeClass('success').addClass('error');
		},
	    success: function(element) {
	    	$(element).closest('.form-group').removeClass('error');
	    	element.remove();
	    },
		submitHandler: function(form) {
			$.ajax({
                type: 'post',
                url: base_url+'admin/order/update_tracking_code',
                data: form_element.serialize(),
                success: function(response) {
                    $('body').removeClass('loading');
                    if ( response ) {
                        $('body').addClass('loading');
                    	location.href = base_url+'/admin/order/detail/'+form_element.find('input[name="id"]').val();
                    } else {
                        Swal.fire({
                            title: 'Error',
                            text: 'Kesalahan pada server',
                            type: 'warning',
                        });
                    }
                }
            });
		}
	});

    $(".hint-non-jabodetabek").on('click', function() {
        Swal.fire(
			'Pengiriman Non Jabodetabek',
			'Untuk metode pengiriman ini, silahkan konfirmasi ongkos kirim kepada user',
			'warning'
		);
    });
});

function approvalOrder(id, type) {
    $.ajax({
        type: 'post',
        url: base_url+'admin/order/approval',
        data: {
            id: id,
            type: type
        },
        success: function(response) {
            $('body').removeClass('loading');
            if (response) {
                Swal.fire({
                    title: 'Success',
                    text: 'Order approved',
                    type: 'success',
                })
                .then((result) => {
                    location.reload();
                });
            } else {
                Swal.fire({
                    title: 'Error',
                    text: 'Kesalahan pada server',
                    type: 'warning',
                });
            }
        }
    });
}