$(document).ready(function(){
    
    var tableData = $("#table-order-waiting-confirmation");

	tableData.DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": {
            'url': base_url+'admin/order/waiting_confirmation',
            'type': 'POST'
        },
        "columnDefs": [ 
            {
                "targets": [5],
                "visible": false,
            },
            {
                "targets": [3],
                "className": "text-right align-middle",
                "render": function ( data, type, row, meta ) {
                    if (data) {
                        return 'Rp '+data.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
                    } else {
                        return '-';
                    }
                },
            },
        ],
        "createdRow": function( row, data, dataIndex ) {
            $(row).find('.btn-action').attr( 'data-id', data[5] );
            $(row).find('.btn-detail').attr( 'href', base_url+'admin/order/detail/'+data[5] );
        },
        "language": {
            "paginate": {
                "previous": "<i class='fas fa-angle-left'></i>",
                "next": "<i class='fas fa-angle-right'></i>"
            }
        },
    });

    tableData.on('click','.btn-action',function(){
        var text_label = $(this).text();
        var row_element = $(this).parent().parent();
        var action = $(this).attr('data-action');
        var id = $(this).attr('data-id');

        Swal.fire({
            title: text_label+'?',
            text: "",
            type: 'question',
            showCancelButton: true,
            onOpen: function(){
                row_element.addClass('bg-selected-row');
            },
            onClose: function(){
                row_element.removeClass('bg-selected-row');
            }
        }).then((result) => {
            if (result.value) {
                $('body').addClass('loading');
                $.ajax({
                    type: 'post',
                    url: base_url+'admin/order/approval_payment',
                    data: {
                        id: id,
                        action: action
                    },
                    success: function(response) {
                        $('body').removeClass('loading');
                        if ( response ) {
                            tableData.DataTable().ajax.reload();
                        } else {
                            Swal.fire('Error', 'Transaksi tidak ditemukan', 'warning');
                        }
                    }
                });
            }
        })
    })
});