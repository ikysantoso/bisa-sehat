$(document).ready(function(){
    
    var tableData = $("#table-order");

    tableData.DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": {
            'url': base_url+'admin/order/preorder',
            'type': 'POST'
        },
        "columnDefs": [ 
            {
                "targets": [4],
                "visible": false,
            },
            {
                "targets": [2],
                "className": "text-center align-middle",
                "render": function ( data, type, row, meta ) {
                    var status = {
                        'paid'      : '<span class="badge badge-success">paid</span>',
                        'unpaid'    : '<span class="badge badge-warning">unpaid</span>',
                        'waiting'   : '<span class="badge badge-info">waiting</span>',
                        'failed'    : '<span class="badge badge-danger">failed</span>',
                    };
                    return status[data];
                },
            },
        ],
        "createdRow": function( row, data, dataIndex ) {
            // $(row).find('.btn-link-product').attr( 'data-id', data[8] );
            // $(row).find('.btn-edit').attr( 'data-id', data[3] );
            $(row).find('.btn-detail').attr( 'href', base_url+'admin/order/detail/'+data[4] );
        },
        "language": {
            "paginate": {
                "previous": "<i class='fas fa-angle-left'></i>",
                "next": "<i class='fas fa-angle-right'></i>"
            }
        },
    });
});