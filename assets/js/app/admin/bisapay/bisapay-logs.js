$(document).ready(function(){
    var tableData = $('#table-bisapay-list');
	tableData.DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": {
        	'url': base_url+'admin/bisapay/logs',
        	'type': 'POST'
        },
        "columnDefs": [ 
            { "targets": [ 7 ], "visible": false },
            {
                "targets": [2],
                "className": "text-right align-middle",
                "render": function ( data, type, row, meta ) {
                    if (data) {
                        return 'Rp '+(parseInt(data)).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
                    } else {
                        return '-';
                    }
                },
            },
        ],
        "createdRow": function( row, data, dataIndex ) {
            // $(row).find('.btn-delete').attr( 'data-id', data[3] );
            // $(row).find('.btn-edit').attr( 'data-id', data[3] );
        },
        "language": {
            "paginate": {
                "previous": "<i class='fas fa-angle-left'></i>",
                "next": "<i class='fas fa-angle-right'></i>"
            }
        }
    });
});