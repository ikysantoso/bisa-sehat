$(document).ready(function(){
    var tableData = $('#table-bisapay-withdraw-list');

    load_table();

    $("input[name='datatable_filter']").change(function(){
        load_table( $(this).val(), true );
    });

    function load_table( filter='waiting', reinit=false ){

        if ( reinit ) {
            tableData.DataTable().destroy();
        }

    	tableData.DataTable( {
            "processing": true,
            "serverSide": true,
            "ajax": {
            	'url': base_url+'admin/bisapay/withdraw',
            	'type': 'POST',
                'data': {
                    'status': filter
                }
            },
            "columnDefs": [ 
                { "targets": [ 6 ], "visible": false },
                {
                    "targets": [2],
                    "className": "text-right align-middle",
                    "render": function ( data, type, row, meta ) {
                        if (data) {
                            return 'Rp '+(parseInt(data)).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
                        } else {
                            return '-';
                        }
                    },
                },
                {
                    "targets": [5],
                    "className": "text-center align-middle",
                    "render": function ( data, type, row, meta ) {
                        if (data=='waiting') {
                        	return `<button data-id="`+row[6]+`" class="btn btn-sm btn-info btn-action"><i class="far fa-clock"></i> `+data+`</button>`;
                        } else if (data=='success') {
                        	return `<button disabled="disabled" class="btn btn-sm btn-success"><i class="fas fa-check"></i> `+data+`</button>`;
                        } else if (data=='failed') {
                        	return `<button disabled="disabled" class="btn btn-sm btn-danger"><i class="fas fa-exclamation-circle"></i> `+data+`</button>`;
                        }
                    },
                },
            ],
            "language": {
                "paginate": {
                    "previous": "<i class='fas fa-angle-left'></i>",
                    "next": "<i class='fas fa-angle-right'></i>"
                }
            }
        });
    }

    tableData.on('click', '.btn-action', function(){

        var id = $(this).attr('data-id');
        var total = $(this).parent().parent().find('td:nth-child('+3+')').html();
        var destination = $(this).parent().parent().find('td:nth-child('+4+')').html();
        
        Swal.fire({
            title: 'Yakin sudah ditransfer ke rekening user?',
            html: `
              <div class="p-3 rounded border" style="background-color:rgba(17, 205, 239, 0.1)">
                  <h2>`+destination+`</h2>
                  <h1>`+total+`</h1>
              </div>
            `,
            type: 'question',
            showCancelButton: true,
        }).then((result) => {
            if (result.value) {
                $('body').addClass('loading');
                $.ajax({
                    type: 'post',
                    url: base_url+'admin/bisapay/withdraw_approval',
                    data: {
                        id: id,
                        status: 'success',
                    },
                    success: function(response) {
                        $('body').removeClass('loading');
                        if ( response ) {
                            tableData.DataTable().ajax.reload();
                        } else {
                            Swal.fire(
                              'Error',
                              'Terdapat kesalahan pada server',
                              'warning'
                            )
                        }
                    }
                });
            }
        })
    });

});