$(document).ready(function(){
    var tableData = $('#table-bisapay-list');
	tableData.DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": {
        	'url': base_url+'admin/bisapay/lock',
        	'type': 'POST'
        },
        "columnDefs": [ 
            // { "targets": [ 5 ], "visible": false },
            {
                "targets": [2],
                "className": "text-right align-middle",
                "render": function ( data, type, row, meta ) {
                    if (data) {
                        return 'Rp '+(parseInt(data)).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
                    } else {
                        return '-';
                    }
                },
            },
        ],
        "createdRow": function( row, data, dataIndex ) {
            // $(row).find('.btn-delete').attr( 'data-id', data[3] );
            // $(row).find('.btn-edit').attr( 'data-id', data[3] );
        },
        "language": {
            "paginate": {
                "previous": "<i class='fas fa-angle-left'></i>",
                "next": "<i class='fas fa-angle-right'></i>"
            }
        }
    });

    $('.singleInputDynamic').fastselect({
        onItemSelect: function($item, itemModel){
            $('.singleInputDynamic').valid();
        }
    });

    $("#lock-saldo-btn").click(function(){
        $("#modal-lock-saldo").modal('show');
    });

    // cek bisapay saldo
    $("#form-lock-saldo input[name='user']").change(function(){
        var id = $(this).val();

        $.ajax({
            type: 'post',
            url: base_url+'admin/bisapay/ceksaldo',
            data: {
                id: id,
            },
            success: function(saldo) {
                $("#form-lock-saldo input[name='nominal']").attr('max', saldo);
                $("#form-lock-saldo input[name='saldo']").val('Rp '+(parseInt(saldo)).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'));
                $("#form-lock-saldo input[name='nominal']").attr('placeholder', 'Lock saldo maksimal: Rp '+(parseInt(saldo)).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'));
                $("#form-lock-saldo input[name='nominal']").removeAttr('disabled');
            }
        });
    });

    $("#form-lock-saldo").validate({
        rules: {},
        highlight: function(element) {
            $(element).closest('.form-group').removeClass('success').addClass('error');
        },
        success: function(element) {
            $(element).closest('.form-group').removeClass('error');
            element.remove();
        },
        submitHandler: function(form) {
            $("body").addClass('loading');
            $.ajax({
                type: 'post',
                url: base_url+'admin/bisapay/lock_saldo',
                data: {
                    id: $("#form-lock-saldo input[name='user']").val(),
                    nominal: $("#form-lock-saldo input[name='nominal']").val()
                },
                success: function(response) {
                    if ( response ) {
                        $("body").removeClass('loading');

                        if ( response.status == 'success' ) {
                            Swal.fire({
                                title: response.title,
                                text: response.message,
                                type: response.status,
                            }).then((result) => {
                                $("#form-lock-saldo").trigger('reset');
                                $("#modal-lock-saldo").modal('hide');

                                // reinit fastselect
                                if ( $('.singleInputDynamic').data('fastselect') ) {
                                    $('.singleInputDynamic').data('fastselect').destroy();
                                    $('.singleInputDynamic').fastselect();
                                }
                                tableData.DataTable().ajax.reload();
                            });
                        } else {
                            Swal.fire({
                                title: response.title,
                                text: response.message,
                                type: response.status,
                            });
                        }
                    }
                }
            });
        }
    });
});