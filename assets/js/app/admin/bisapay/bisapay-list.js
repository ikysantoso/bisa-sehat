$(document).ready(function(){
    var tableData = $('#table-bisapay-list');

    if ( location.search.split('status=')[1] ) {
        var selected_status = location.search.split('status=')[1];
        $("input[name='datatable_filter'][value='"+selected_status+"']").prop('checked', true);
        load_table( selected_status );
    } else {
        load_table();
    }

    $("input[name='datatable_filter']").change(function(){
        load_table( $(this).val(), true );
    });

    function load_table( filter='all', reinit=false ){

        if ( reinit ) {
            tableData.DataTable().destroy();
        }

    	tableData.DataTable( {
            "searching": false,
            "processing": true,
            "serverSide": true,
            "ajax": {
            	'url': base_url+'admin/bisapay/list',
            	'type': 'POST',
                'data': {
                    'status': filter
                }
            },
            "columnDefs": [
                { "targets": [ 3,7 ], "visible": false },
                {
                    "targets": [2],
                    "className": "text-right align-middle",
                    "render": function ( data, type, row, meta ) {
                        if (data) {
                        	var kode_unik = parseInt((row[3]) ? row[3] : 0);
                            return 'Rp '+(parseInt(data) + kode_unik).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
                        } else {
                            return '-';
                        }
                    },
                },
                {
                    "targets": [6],
                    "className": "text-center align-middle",
                    "render": function ( data, type, row, meta ) {
                        if (data=='waiting') {
                        	return `<button data-id="`+row[7]+`" class="btn btn-sm btn-info btn-approve"><i class="far fa-clock"></i> `+data+`</button>`;
                        } else if (data=='success') {
                        	return `<button disabled="disabled" class="btn btn-sm btn-success"><i class="fas fa-check"></i> `+data+`</button>`;
                        } else if (data=='failed') {
                        	return `<button disabled="disabled" class="btn btn-sm btn-danger"><i class="fas fa-exclamation-circle"></i> `+data+`</button>`;
                        }
                    },
                },
            ],
            "createdRow": function( row, data, dataIndex ) {
                // $(row).find('.btn-delete').attr( 'data-id', data[3] );
                // $(row).find('.btn-edit').attr( 'data-id', data[3] );
            },
            "language": {
                "paginate": {
                    "previous": "<i class='fas fa-angle-left'></i>",
                    "next": "<i class='fas fa-angle-right'></i>"
                }
            }
        });
    }

    $('#modal-bisapay').on('hidden.bs.modal', function (e) {
    	form_element.trigger('reset');
	});

    var form_element = $("#form-approval");
    tableData.on('click','.btn-approve',function(){
        form_element.trigger('reset');
        var id = $(this).attr('data-id');
        $.ajax({
            type: 'post',
            url: base_url+'admin/bisapay/topup_detail',
            data: {
                id: id
            },
            success: function(response) {
                $('body').removeClass('loading');
                if ( response ) {
                    $("#id").val(response.id);
                    $("#user-name").text( response.log.bisapay.user.first_name+' '+response.log.bisapay.user.last_name );
                    $("#topup-date").text( response.created_at );
                    $("#bank-name").text( response.bank.name );
                    $("#account-name").text( response.bank.account_name );
                    $("#account-number").text( response.bank.account_number );
                    $("#nominal").text( 'Rp '+(parseInt(response.log.nominal) + parseInt(response.unique_number)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") );

                    if (response.log.payment_image != null) {
                        if ($("#payment-image-container").hasClass('d-flex justify-content-between')) {
                            $("#payment-image-container").removeClass('d-flex justify-content-between');
                        }
                        $("#payment-image-container").html(
                            '<span style="font-weight: bold;">Payment Confirmation</span>' +
                            `<img src="../../assets/images/transactions/bisapay/${response.log.payment_image}" style="margin-top: 10px; width: 100%;"/>`
                        );
                    } else {
                        $("#payment-image-container").addClass('d-flex justify-content-between');
                        $("#payment-image-container").html(
                            '<span>Payment Confirmation</span>' +
                            '<div class="flex-fill border-bottom mx-3" style="border-style: dashed;border-width: 1px;opacity: 0.2"></div>' +
                            '<b>No Confirmation</b>'
                        );
                    }
                }
            }
        });
        $("#modal-topup").modal('show');
    });

    form_element.validate({
        rules: {},
        highlight: function(element) {
            $(element).closest('.form-group').removeClass('success').addClass('error');
        },
        success: function(element) {
            $(element).closest('.form-group').removeClass('error');
            element.remove();
        },
        submitHandler: function(form) {
            $('body').addClass('loading');
            $.ajax({
                type: 'post',
                url: base_url+'admin/bisapay/topup_approval',
                data: form_element.serialize(),
                success: function(response) {
                    $('body').removeClass('loading');
                    if ( response ) {
                        $("#modal-topup").modal('hide');
                        tableData.DataTable().ajax.reload();
                    }
                }
            });
        }
    });
});