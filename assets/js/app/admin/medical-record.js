	function approve(id){
		var id = +id;

		Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, approve it!'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: 'post',
                    url: base_url+'admin/dashboard/medical_record_approve',
                    data: {
                        id: id
                    },
                    success: function(response) {
                        $('body').removeClass('loading');
                        if ( response ) {
                            location.reload();
                        }
                    }
                });
            }
    	});
    }

