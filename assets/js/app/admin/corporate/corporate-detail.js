$(document).ready(function(){
	var tableData = $('#table-coporate-member');
	tableData.DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": {
        	'url': base_url+'admin/corporate/member/'+$("#corporate_id").attr('data-id'),
        	'type': 'POST'
        },
        "columnDefs": [ 
            { "targets": [ 5 ], "visible": false },
            
        ],
        "createdRow": function( row, data, dataIndex ) {
            $(row).find('.btn-delete').attr( 'data-id', data[3] );
            // $(row).find('.btn-edit').attr( 'href', base_url+'admin/slider/edit/'+data[4] );
        },
        "language": {
            "paginate": {
                "previous": "<i class='fas fa-angle-left'></i>",
                "next": "<i class='fas fa-angle-right'></i>"
            }
        }
    });

    tableData.on('click', '.btn-delete', function(){
        var id = $(this).attr('data-id');
        Swal.fire({
            title: 'Are you sure?',
            text: "Remove user from current corporate?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, remove it!'
        }).then((result) => {
            if (result.value) {
                $('body').addClass('loading');

                $.ajax({
                    type: 'post',
                    url: base_url+'admin/corporate/remove_member',
                    data: {
                        id: id
                    },
                    success: function(response) {
                        $('body').removeClass('loading');
                        if ( response ) {
                            tableData.DataTable().ajax.reload();
                        }
                    }
                });
            }
        }); 
    });

    $('.multipleSelect').fastselect({
        onItemSelect: function($item, itemModel){
            $('.multipleSelect').valid();
        }
    });

    $(".btn-add-member").click(function(){
        $("#modal-add-employee").modal('show');
    });

    var form_element = $("#form-add-corporate-member");
    form_element.validate({
        rules: {},
        highlight: function(element) {
            $(element).closest('.form-group').removeClass('success').addClass('error');
        },
        success: function(element) {
            $(element).closest('.form-group').removeClass('error');
            element.remove();
        },
        submitHandler: function(form) {
            $('body').addClass('loading');
            $.ajax({
                type: 'post',
                url: base_url+'admin/corporate/add_member',
                data: form_element.serialize(),
                success: function(response) {
                    $('body').removeClass('loading');
                    if ( response ) {
                        $("#modal-add-employee").modal('hide');
                        tableData.DataTable().ajax.reload();
                    }
                }
            });
        }
    });
});