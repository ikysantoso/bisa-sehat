$(document).ready(function(){
    var form_element = $("#form-new-corporate");
    form_element.validate({
        rules: {},
        highlight: function(element) {
            $(element).closest('.form-group').removeClass('success').addClass('error');
        },
        success: function(element) {
            $(element).closest('.form-group').removeClass('error');
            element.remove();
        },
        submitHandler: function(form) {
            $('body').addClass('loading');

            var submit_url = (form_element.find('input[name="id"]').val()) ?  base_url+'admin/corporate/update' : base_url+'admin/corporate/save';
            
            $.ajax({
                type: 'post',
                url: submit_url,
                data: form_element.serialize(),
                success: function(response) {
                    $('body').removeClass('loading');
                    if ( response && response.id ) {
                        $('body').addClass('loading');
                        location.href = base_url+'admin/corporate/detail/'+response.id;
                    } else {
                        Swal.fire(
                            'Error',
                            'Gagal disimpan',
                            'warning'
                        )
                    }
                }
            });
        }
    });
});