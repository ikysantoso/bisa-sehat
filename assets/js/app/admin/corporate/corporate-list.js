$(document).ready(function(){
	var tableData = $('#table-slider-list');
	tableData.DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": {
        	'url': base_url+'admin/corporate/list',
        	'type': 'POST'
        },
        "columnDefs": [ 
            { "targets": [ 5 ], "visible": false },
            { "targets": [ 2 ], "className": 'text-center' },
            {
                "targets": [1],
                "render": function ( data, type, row, meta ) {
                    return '<a href="'+base_url+'admin/corporate/detail/'+row[5]+'"><b>'+data+'</b></a>';
                },
            },
            {
                "targets": [3],
                "className": 'text-center',
                "render": function ( data, type, row, meta ) {
                    if ( data == 'waiting' ) {
                        var button_type = 'secondary';
                    } else if ( data == 'approved' ) {
                        var button_type = 'success';
                    } else if ( data == 'rejected' ) {
                        var button_type = 'danger';
                    }
                    return '<button data-id="'+row[5]+'" class="btn btn-'+button_type+' btn-sm corporate-approval-btn">'+data+'</button>';
                },
            },
        ],
        "createdRow": function( row, data, dataIndex ) {
            $(row).find('.btn-delete').attr( 'data-id', data[5] );
            $(row).find('.btn-edit').attr( 'href', base_url+'admin/corporate/edit/'+data[5] );
        },
        "language": {
            "paginate": {
                "previous": "<i class='fas fa-angle-left'></i>",
                "next": "<i class='fas fa-angle-right'></i>"
            }
        }
    });

    tableData.on('click', '.btn-delete', function(){
        var id = $(this).attr('data-id');
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                $('body').addClass('loading');

                $.ajax({
                    type: 'post',
                    url: base_url+'admin/corporate/delete',
                    data: {
                        id: id
                    },
                    success: function(response) {
                        $('body').removeClass('loading');
                        if ( response ) {
                            tableData.DataTable().ajax.reload();
                        }
                    }
                });
            }
        }); 
    });

    var corporate_approval_modal = $("#corporate-approval-modal");
    var form_element = $("#corporate-approval-form");

    tableData.on('click', '.corporate-approval-btn', function(){
        var id = $(this).attr('data-id');
        $.ajax({
            type: 'post',
            url: base_url+'admin/corporate/detail',
            data: {
                id: id
            },
            success: function(response) {
                $('body').removeClass('loading');
                if ( response ) {
                    form_element.find("input[name='id']").val(response.id);
                    form_element.find("input[name='name']").val(response.name);
                    form_element.find("input[name='status'][value='"+response.status+"']").prop('checked', true);
                    corporate_approval_modal.modal('show');
                }
            }
        });
    });

    form_element.validate({
        rules: {},
        highlight: function(element) {
            $(element).closest('.form-group').removeClass('success').addClass('error');
        },
        success: function(element) {
            $(element).closest('.form-group').removeClass('error');
            element.remove();
        },
        submitHandler: function(form) {
            $('body').addClass('loading');
            $.ajax({
                type: 'post',
                url: base_url+'admin/corporate/approval',
                data: form_element.serialize(),
                success: function(response) {
                    $('body').removeClass('loading');
                    if ( response ) {
                        corporate_approval_modal.modal('hide');
                        tableData.DataTable().ajax.reload();
                    }
                }
            });
        }
    });
});