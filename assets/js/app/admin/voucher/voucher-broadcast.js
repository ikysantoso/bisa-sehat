$('.type_broadcast').click(function() {
    $('.content-type-broadcast').hide();
    if ($(this).val() === '1') {
        
    } else if ($(this).val() === '2') {
        $(this).siblings('.content-type-broadcast').show();
    } else {
        $(this).siblings('.content-type-broadcast').show();
    }
});

$(document).ready(function() {
    $('.js-example-basic-multiple').select2();
    $('.js-example-basic-multiple2').select2();
    
	var form_element = $("#form-voucher-broadcast");

	form_element.validate({
        rules: {},
        highlight: function(element) {
        	$(element).closest('.form-group').removeClass('success').addClass('error');
		},
	    success: function(element) {
	    	$(element).closest('.form-group').removeClass('error');
	    	element.remove();
	    },
		submitHandler: function(form) {
			$.ajax({
                type: 'post',
                url: base_url+'admin/voucher/broadcastSave',
                data: form_element.serialize(),
                success: function(response) {
                    $('body').removeClass('loading');
                    if ( response ) {
                    	location.href = base_url+'admin/voucher';
                    } else {
                        Swal.fire({
                            title: 'Error',
                            text: 'Kesalahan pada server',
                            type: 'warning',
                        });
                    }
                }
            });
		}
	});
});