$(document).ready(function() {
	var form_element = $("#form-edit-voucher");

	form_element.validate({
        rules: {},
        highlight: function(element) {
        	$(element).closest('.form-group').removeClass('success').addClass('error');
		},
	    success: function(element) {
	    	$(element).closest('.form-group').removeClass('error');
	    	element.remove();
	    },
		submitHandler: function(form) {
			$.ajax({
                type: 'post',
                url: base_url+'admin/voucher/update',
                data: form_element.serialize(),
                success: function(response) {
                    $('body').removeClass('loading');
                    if ( response ) {
                    	location.href = base_url+'admin/voucher';
                    } else {
                        Swal.fire({
                            title: 'Error',
                            text: 'Kesalahan pada server',
                            type: 'warning',
                        });
                    }
                }
            });
		}
	});
});