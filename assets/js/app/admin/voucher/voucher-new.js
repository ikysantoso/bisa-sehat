$(document).ready(function() {
    // $('#datetimepicker1').datetimepicker();
    var form_element = $("#form-new-voucher");
	form_element.validate({
        rules: {},
        highlight: function(element) {
        	$(element).closest('.form-group').removeClass('success').addClass('error');
		},
	    success: function(element) {
	    	$(element).closest('.form-group').removeClass('error');
	    	element.remove();
	    },
		submitHandler: function(form) {
            var userSelected = [];
            $('#userSelectedContainer li').each(function() {
                userSelected.push($(this).attr('data-id'));
            });

            var name = $("#voucher-name").val();
            var nominal = $("#voucher-nominal").val();
            var quantity = $("#voucher-quantity").val();
            var startDate = $("#datepicker").val();
            var expireDate = $("#datepicker2").val();

			$.ajax({
                type: 'post',
                url: base_url+'admin/voucher/save',
                data: {
                    name: name,
                    nominal: nominal,
                    quantity: quantity,
                    start_date: startDate,
                    expire_date: expireDate,
                    selected_user: userSelected
                },
                success: function(response) {
                    $('body').removeClass('loading');
                    if ( response ) {
                    	location.href = base_url+'admin/voucher';
                    } else {
                        Swal.fire({
                            title: 'Error',
                            text: 'Kesalahan pada server',
                            type: 'warning',
                        });
                    }
                }
            });
		}
	});
});

function doSearchUserForVoucher() {
    var user = document.getElementById("voucherUserSelect").value;

    if (user !== "") {
        $.ajax({
            type: 'post',
            url: base_url+'admin/voucher/search_user',
            data: {
                user: user
            },
            success: function(response) {
                var htmlElement = '';

                var userSelected = [];
                $('#userSelectedContainer li').each(function() {
                    userSelected.push($(this).attr('data-id'));
                });

                for (i = 0; i < response.length; i++) {

                    if (!userSelected.includes(response[i].id)) {
                        htmlElement += `<input type="checkbox" name="voucher_user_id" data-id="${response[i].id}" value="${response[i].first_name} ${response[i].last_name}"> `
                        htmlElement += `<label>${response[i].first_name} ${response[i].last_name}</label> <br>`;
                    }
                }

                $('#modal-voucher-body').html(htmlElement);
                $("#modal-voucher").modal('show');
            }
        });
    } else {
        Swal.fire({
            title: 'Error',
            text: 'Masukkan nama user yang ingin dipiih',
            type: 'warning',
        });
    }
}

function doConfirmUserVoucherSelected() {
    var value = [];

    $("input:checkbox[name=voucher_user_id]:checked").each(function(){
        var selectedObj = {
            id: $(this).attr('data-id'),
            value:  $(this).val()
        };

        value.push(selectedObj);
    });

    if (value.length > 0) {
        for(var i = 0; i < value.length; i++) {
            $('#userSelectedContainer ul').append(`<li data-id="${value[i].id}">${value[i].value}<span class="close" onclick="doRemoveUserSelected(${value[i].id})">&times;</span></li>`);
        }

        $("#hint-voucher-distribution").hide();
    }
}

function doRemoveUserSelected(id) {
    var element = document.querySelector(`li[data-id='${id}']`);

    if ($('#userSelectedContainer ul li').length == 1) {
        element.remove();
        $("#hint-voucher-distribution").show();
    } else {
        element.remove();
    }
}