$(document).ready(function(){
	var tableData = $('#table-voucher-list');
	tableData.DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": {
        	'url': base_url+'admin/voucher/list',
        	'type': 'POST'
        },
        "columnDefs": [
            { "targets": [ 6 ], "visible": false },
            {
                "targets": [2,3],
                "className": "text-center align-middle",
                "render": function ( data, type, row, meta ) {
                    if (data) {
                        var options = {  year: 'numeric', month: 'long', day: 'numeric' };
                        var tanggal  = new Date(data);
                        return (tanggal.toLocaleDateString("id-id", options) +' | '+ tanggal.toLocaleTimeString());
                    } else {
                        return '-';
                    }
                },
            },
            { "targets": [ 5,7 ], "className": "text-center align-middle" },
            {
                "targets": [4],
                "className": "text-center align-middle",
                "render": function ( data, type, row, meta ) {
                    if (data) {
                        return 'Rp '+data.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
                    } else {
                        return '-';
                    }
                },
            },
        ],
        "createdRow": function( row, data, dataIndex ) {
            $(row).find('.btn-delete').attr( 'data-id', data[6] );
            $(row).find('.btn-edit.voucher').attr( 'href', base_url+'admin/voucher/edit/'+data[6] );
        },
        "language": {
            "paginate": {
                "previous": "<i class='fas fa-angle-left'></i>",
                "next": "<i class='fas fa-angle-right'></i>"
            }
        }
    });

    tableData.on('click','.btn-delete', function(){
        var id = $(this).attr('data-id');

        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                $('body').addClass('loading');

                $.ajax({
                    type: 'post',
                    url: base_url+'admin/voucher/delete',
                    data: {
                        id: id
                    },
                    success: function(response) {
                        $('body').removeClass('loading');
                        if ( response ) {
                            tableData.DataTable().ajax.reload();
                        }
                    }
                });
            }
        });
    })
});