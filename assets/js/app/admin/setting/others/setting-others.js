$(document).ready(function(){
	// unique code digit setting
	var form_element = $("#form-unique-code-digit");
    form_element.validate({
            rules: {},
            highlight: function(element) {
                $(element).closest('.form-group').removeClass('success').addClass('error');
            },
            success: function(element) {
                $(element).closest('.form-group').removeClass('error');
                element.remove();
            },
            submitHandler: function(form) {
                $('body').addClass('loading');
                $.ajax({
                    type: 'post',
                    url: base_url+'admin/setting/others/unique_code',
                    data: form_element.serialize(),
                    success: function(response) {
                        $('body').removeClass('loading');
                        if ( response.status == 'success' ) {
                            Swal.fire('Berhasil','Pengaturan berhasil disimpan', 'success');
                        	$('#myTab a[href="#unique_code"]').tab('show')
                        } else {
                        	Swal.fire('Gagal disimpan','Kesalahan pada sistem', 'warning');
                        }
                    }
                });
            }
	});

    // Whatsapp number setting
    var form_element_contact_info = $("#form-contact-info");
    form_element_contact_info.validate({
            rules: {},
            highlight: function(element) {
                $(element).closest('.form-group').removeClass('success').addClass('error');
            },
            success: function(element) {
                $(element).closest('.form-group').removeClass('error');
                element.remove();
            },
            submitHandler: function(form) {
                $('body').addClass('loading');
                $.ajax({
                    type: 'post',
                    url: base_url+'admin/setting/others/contact_info',
                    data: form_element_contact_info.serialize(),
                    success: function(response) {
                        $('body').removeClass('loading');
                        if ( response.status == 'success' ) {
                            // location.reload();
                            Swal.fire('Berhasil','Pengaturan berhasil disimpan', 'success');
                            $('#myTab a[href="#contact_info"]').tab('show')
                        } else {
                            Swal.fire('Gagal disimpan','Kesalahan pada sistem', 'warning');
                        }
                    }
                });
            }
    });

    // facebook setting
    // var form_element_facebook_ = $("#form-facebook");
    // form_element_facebook_.validate({
    //         rules: {},
    //         highlight: function(element) {
    //             $(element).closest('.form-group').removeClass('success').addClass('error');
    //         },
    //         success: function(element) {
    //             $(element).closest('.form-group').removeClass('error');
    //             element.remove();
    //         },
    //         submitHandler: function(form) {
    //             $('body').addClass('loading');
    //             $.ajax({
    //                 type: 'post',
    //                 url: base_url+'admin/setting/others/facebook',
    //                 data: form_element_facebook_.serialize(),
    //                 success: function(response) {
    //                     $('body').removeClass('loading');
    //                     if ( response.status == 'success' ) {
    //                         // location.reload();
    //                         Swal.fire('Berhasil','Pengaturan berhasil disimpan', 'success');
    //                         $('#myTab a[href="#contact_info"]').tab('show')
    //                     } else {
    //                         Swal.fire('Gagal disimpan','Kesalahan pada sistem', 'warning');
    //                     }
    //                 }
    //             });
    //         }
    // });

    // // instagram setting
    // var form_element_instagram = $("#form-instagram");
    // form_element_instagram.validate({
    //         rules: {},
    //         highlight: function(element) {
    //             $(element).closest('.form-group').removeClass('success').addClass('error');
    //         },
    //         success: function(element) {
    //             $(element).closest('.form-group').removeClass('error');
    //             element.remove();
    //         },
    //         submitHandler: function(form) {
    //             $('body').addClass('loading');
    //             $.ajax({
    //                 type: 'post',
    //                 url: base_url+'admin/setting/others/instagram',
    //                 data: form_element_instagram.serialize(),
    //                 success: function(response) {
    //                     $('body').removeClass('loading');
    //                     if ( response.status == 'success' ) {
    //                         // location.reload();
    //                         Swal.fire('Berhasil','Pengaturan berhasil disimpan', 'success');
    //                         $('#myTab a[href="#contact_info"]').tab('show')
    //                     } else {
    //                         Swal.fire('Gagal disimpan','Kesalahan pada sistem', 'warning');
    //                     }
    //                 }
    //             });
    //         }
    // });
});