$(document).ready(function() {
    var table_element = $('#table-bank-account-list');
    table_element.DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": {
        	'url': base_url+'admin/setting/bank_accounts',
        	'type': 'POST'
        },
        "columnDefs": [ 
            { "targets": [ 5 ], "visible": false },
            {
                "targets": [4],
                "className": "text-center align-middle",
                "render": function ( data, type, row, meta ) {
                    var status = (data=='active') ? 'checked' : '';
                    return `
                    <label class="custom-toggle custom-toggle-success mx-auto">
                        <input data-id="`+row[5]+`" class="status-toggle" type="checkbox" `+status+`>
                        <span class="custom-toggle-slider rounded-circle" data-label-off="No" data-label-on="Yes"></span>
                    </label>
                    `;
                },
            },

        ],
        "createdRow": function( row, data, dataIndex ) {
            $(row).find('.btn-delete').attr( 'data-id', data[5] );
            $(row).find('.btn-edit').attr( 'data-id', data[5] );
        },
        "language": {
            "paginate": {
                "previous": "<i class='fas fa-angle-left'></i>",
                "next": "<i class='fas fa-angle-right'></i>"
            }
        }
    });

    // save or update
    var form_element = $('#form-bank-acccount');
    form_element.validate({
        rules: {},
        highlight: function(element) {
            $(element).closest('.form-group').removeClass('success').addClass('error');
        },
        success: function(element) {
            $(element).closest('.form-group').removeClass('error');
            element.remove();
        },
        submitHandler: function(form) {
            $('body').addClass('loading');
            var url_submit = (form_element.find( 'input[name="id"]' ).val()) ? base_url+'admin/setting/bank_accounts/update' : base_url+'admin/setting/bank_accounts/save';

            $.ajax({
                type: 'post',
                url: url_submit,
                data: form_element.serialize(),
                success: function(response) {
                    $('body').removeClass('loading');
                    if ( response ) {
                        $("#modal-bank-accounts").modal('hide');
                        table_element.DataTable().ajax.reload();
                    } else {
                        Swal.fire({
                            title: 'Error',
                            text: 'Kesalahan pada server',
                            icon: 'warning',
                        });
                    }
                }
            });
        }
    });

    // edit trigger form
    table_element.on('click', '.btn-edit', function(){
        $('body').addClass('loading');
        var id = $(this).attr('data-id');
        $.ajax({
            type: 'post',
            url: base_url+'admin/setting/bank_accounts/detail',
            data: {
                id: id
            },
            success: function(response) {
                $('body').removeClass('loading');
                if ( response ) {
                    form_element.find('input[name="id"]').val( response.id );
                    form_element.find('input[name="name"]').val( response.name );
                    form_element.find('input[name="account_name"]').val( response.account_name );
                    form_element.find('input[name="account_number"]').val( response.account_number );
                    form_element.find('input[name="account_number"]').val( response.account_number );
                    form_element.find("input[name=status][value='"+response.status+"']").prop("checked",true);

                    $("#modal-bank-accounts").modal('show');
                } else {
                    Swal.fire({
                        title: 'Error',
                        text: 'Kesalahan pada server',
                        icon: 'warning',
                    });
                }
            }
        });
    });

    // update status active/inactive
    table_element.on('change', '.status-toggle', function(){
        $('body').addClass('loading');
        var status = ($(this).is(':checked')) ? 'active' : 'inactive';
        var id = $(this).attr('data-id');
        $.ajax({
            type: 'post',
            url: base_url+'admin/setting/bank_accounts/update_status',
            data: {
                id: id,
                status: status
            },
            success: function(response) {
                $('body').removeClass('loading');
                if ( response ) {
                    table_element.DataTable().ajax.reload();
                } else {
                    Swal.fire({
                        title: 'Error',
                        text: 'Kesalahan pada server',
                        icon: 'warning',
                    });
                }
            }
        });
    });

    // delete product
    table_element.on('click', '.btn-delete', function(){
        var id = $(this).attr( 'data-id' );

        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {

                $.ajax({
                    type: 'post',
                    url: base_url+'admin/setting/bank_accounts/delete',
                    data: {
                        id: id
                    },
                    success: function(response) {
                        $('body').removeClass('loading');
                        if ( response ) {
                            Swal.fire(
                                'Deleted!',
                                'Your bank account has been deleted.',
                                'success'
                            );
                            table_element.DataTable().ajax.reload();
                        }
                    }
                });
            }
        });
    });

    $('#modal-bank-accounts').on('hidden.bs.modal', function (e) {
        form_element.trigger('reset');
        form_element.find('input[name="id"]').val('');
    })

    $('#modal-bank-accounts').on('shown.bs.modal', function (e) {
        if ( form_element.find('input[name="id"]').val() ) {
            $(this).find('.modal-title').text('Edit Bank Account');
        } else {
            $(this).find('.modal-title').text('Add New Bank Account');
        }
    })
});