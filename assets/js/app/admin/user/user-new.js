$(document).ready(function() {
	var form_element = $("#form-user");

	form_element.validate({
        rules: {},
        highlight: function(element) {
        	$(element).closest('.form-group').removeClass('success').addClass('error');
		},
	    success: function(element) {
	    	$(element).closest('.form-group').removeClass('error');
	    	element.remove();
	    },
		submitHandler: function(form) {
			$.ajax({
                type: 'post',
                url: base_url+'admin/user/save',
                data: form_element.serialize(),
                success: function(response) {
                    $('body').removeClass('loading');
                    if ( response ) {
                    	location.href = base_url+'admin/user';
                    } else {
                        Swal.fire({
                            title: 'Error',
                            text: 'Email sudah digunakan',
                            type: 'warning',
                        });
                    }
                }
            });
		}
	});
});