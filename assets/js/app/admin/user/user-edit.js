$(document).ready(function() {
	var form_element = $("#form-user");

	form_element.validate({
        rules: {},
        highlight: function(element) {
        	$(element).closest('.form-group').removeClass('success').addClass('error');
		},
	    success: function(element) {
	    	$(element).closest('.form-group').removeClass('error');
	    	element.remove();
	    },
		submitHandler: function(form) {
			$.ajax({
                type: 'post',
                url: base_url+'admin/user/update',
                data: form_element.serialize(),
                success: function(response) {
                    $('body').removeClass('loading');
                    if ( response ) {
                        $('body').addClass('loading');
                    	location.href = base_url+'admin/user';
                    } else {
                        Swal.fire({
                            title: 'Error',
                            text: 'Email sudah digunakan',
                            type: 'warning',
                        });
                    }
                }
            });
		}
	});

    var form_element_password = $("#form-password");

    form_element_password.validate({
        rules: {},
        highlight: function(element) {
            $(element).closest('.form-group').removeClass('success').addClass('error');
        },
        success: function(element) {
            $(element).closest('.form-group').removeClass('error');
            element.remove();
        },
        submitHandler: function(form) {
            $('body').addClass('loading');
            $.ajax({
                type: 'post',
                url: base_url+'admin/user/update_password',
                data: form_element_password.serialize(),
                success: function(response) {
                    $('body').removeClass('loading');
                    if ( response ) {
                        $('body').addClass('loading');
                        Swal.fire({
                            title: 'Password berhasil diubah',
                            text: '',
                            type: 'success',
                        });
                        location.href = base_url+'admin/user';
                    } else {
                        Swal.fire({
                            title: 'Error',
                            text: 'Kesalahan pada server',
                            type: 'warning',
                        });
                    }
                }
            });
        }
    });

});