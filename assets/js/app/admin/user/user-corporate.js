$(document).ready(function() {
    var tableData = $('#table-user-list');
    tableData.DataTable( {
        "searching": false,
        "processing": true,
        "serverSide": true,
        "ajax": {
        	'url': base_url+'admin/user/corporate',
        	'type': 'POST'
        },
        "columnDefs": [ 
            { "targets": [ 4 ], "visible": false },
        ],
        "createdRow": function( row, data, dataIndex ) {
            $(row).find('.btn-detail').attr( 'href', base_url+'admin/user/corporate_detail/'+data[4] );
        },
        "language": {
            "paginate": {
                "previous": "<i class='fas fa-angle-left'></i>",
                "next": "<i class='fas fa-angle-right'></i>"
            }
        }
    });

});