$(document).ready(function() {
    var tableData = $('#table-user-list');
    tableData.DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": {
        	'url': base_url+'admin/user/list',
        	'type': 'POST'
        },
        "columnDefs": [ 
            { "targets": [ 6 ], "visible": false },
            {
                "targets": [5],
                "className": "text-center align-middle",
                "render": function ( data, type, row, meta ) {
                    var status = (data=='1') ? 'checked' : '';
                    return `
                    <label class="custom-toggle custom-toggle-success mx-auto">
                        <input data-id="`+row[6]+`" class="status-toggle" type="checkbox" `+status+`>
                        <span class="custom-toggle-slider rounded-circle" data-label-off="No" data-label-on="Yes"></span>
                    </label>
                    `;
                },
            },
        ],
        "createdRow": function( row, data, dataIndex ) {
            $(row).find('.btn-delete').attr( 'data-id', data[6] );
            $(row).find('.btn-edit').attr( 'href', base_url+'admin/user/edit/'+data[6] );
        },
        "language": {
            "paginate": {
                "previous": "<i class='fas fa-angle-left'></i>",
                "next": "<i class='fas fa-angle-right'></i>"
            }
        }
    });

    tableData.on('change','.status-toggle',function(){
        var id = $(this).attr('data-id');
        var status = ($(this).is(':checked')) ? '1' : '0';
        $.ajax({
            type: 'post',
            url: base_url+'admin/user/activation',
            data: {
                id: id,
                status: status
            },
            success: function(response) {
                $('body').removeClass('loading');
                if ( response ) {
                    tableData.DataTable().ajax.reload();
                } else {
                    Swal.fire({
                        title: 'Error',
                        text: 'Kesalahan pada server',
                        type: 'warning',
                    });
                }
            }
        });
    });

    tableData.on('click', '.btn-delete', function(){
        var id = $(this).attr( 'data-id' );

        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {

                $.ajax({
                    type: 'post',
                    url: base_url+'admin/user/delete',
                    data: {
                        id: id
                    },
                    success: function(response) {
                        $('body').removeClass('loading');
                        if ( response ) {
                            Swal.fire(
                                'Deleted!',
                                'User has been deleted.',
                                'success'
                            );
                            tableData.DataTable().ajax.reload();
                        }
                    }
                });
            }
        });
    });
});