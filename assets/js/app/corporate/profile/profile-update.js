$(document).ready(function(){
    var form_element = $("#form-corporate");
    form_element.validate({
        rules: {},
        highlight: function(element) {
            $(element).closest('.form-group').removeClass('success').addClass('error');
        },
        success: function(element) {
            $(element).closest('.form-group').removeClass('error');
            element.remove();
        },
        submitHandler: function(form) {
            $('body').addClass('loading');
            $.ajax({
                type: 'post',
                url: base_url+'corporate/profile/save',
                data: form_element.serialize(),
                success: function(response) {
                    $('body').removeClass('loading');
                    if ( response ) {
                        Swal.fire({
                            title: 'Berhasil',
                            text: "Corporate Profile Disimpan",
                            type: 'success',
                        }).then(() => {
                            location.reload();
                        })
                    }
                }
            });
        }
    });
});