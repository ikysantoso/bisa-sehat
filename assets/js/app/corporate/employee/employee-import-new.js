$(document).ready(function() {
    var form_element = $('#form-import');
    form_element.validate({
        rules: {},
        highlight: function(element) {
            $(element).closest('.form-group').removeClass('success').addClass('error');
        },
        success: function(element) {
            $(element).closest('.form-group').removeClass('error');
            element.remove();
        },
        submitHandler: function(form) {
            $('body').addClass('loading');
            $.ajax({
                type: 'post',
                url: base_url+'corporate/employee/import_upload',
                enctype: 'multipart/form-data',
                data: new FormData(form_element[0]),
                cache:false,
                contentType: false,
                processData: false,
                traditional: true,
                success: function(response) {
                    $('body').removeClass('loading');


                    var column = ["Firstname", "Lastname", "Email", "Username", "Password"];
                    var valid_column = [];
                    var empty_required_column = [];

                    if ( response && response.data ) {

                        $("#form-import").hide();
                        $("#reimport-btn").show();

                        // reset data
                        $("#table-import-preview thead tr").html('');
                        $("#table-import-preview tbody").html('');

                        $.each(response.data, function(index, item){
                            if ( index == 1 ) {
                                

                                $.each(item, function(_index, _item){
                                    if ( column.includes( _item ) ) {
                                        valid_column.push({
                                            'column': _index,
                                            'name': _item
                                        });
                                    }
                                });

                                $.each(valid_column, function(th_index, th_data){
                                    $("#table-import-preview thead tr").append('<th>'+th_data.name+'</th>');
                                });
                            } else {
                                var tbody_td_data = '';
                                $.each(valid_column, function(tbody_index, tbody_data){

                                    if ( item[tbody_data.column] ) {
                                        tbody_td_data += '<td>'+item[tbody_data.column]+'</td>';
                                    } else {
                                        if ( tbody_data.name == 'Lastname' ) {
                                            tbody_td_data += '<td></td>';
                                        } else {
                                            empty_required_column.push({
                                                index: index,
                                                column: tbody_data.column
                                            });
                                            tbody_td_data += '<td class="border border-danger text-center"><small><i>*required</i></small></td>';
                                        }
                                    }
                                });
                                $("#table-import-preview tbody").append('<tr>'+tbody_td_data+'</tr>');
                            }
                            
                        });

                        $("#table-import-preview").show();

                        // count valid data
                        var invalid_data = [];
                        var total_data = 0;
                        $.each(response.data, function(index, item){
                            $.each(empty_required_column, function(_index, _item){
                                if ( !invalid_data.includes( _item.index ) ) {
                                    invalid_data.push( _item.index );
                                }
                            });
                            total_data++;
                        });

                        var total_valid_data = total_data - 1 - invalid_data.length;

                        if ( empty_required_column.length == 0 ) {
                            
                            if ( total_valid_data == 0 ) {
                                show_import_error('Data masih kosong');
                            } else {
                                $("#import-submit-btn").attr('data-file',response.file).show();
                                $("#import-alert").remove();
                            }
                        } else{
                            show_import_error('Ada field wajib yang belum terisi!');
                        }

                    }
                }
            });
        }
    });

    function show_import_error( message ){
        
        $("#import-alert").remove();
        $("#import-submit-btn").hide();
        $("#import-submit-btn").after(`<div class="alert alert-warning d-inline-block mb-0">`+message+`</div>`);
            
    }

    $("body").on("click","#reimport-btn", function(){
        $('body').addClass('loading');
        location.reload();
    });

    $("#import-submit-btn").click(function(){
        var file = $(this).attr('data-file');
        $.ajax({
            type: 'post',
            url: base_url+'corporate/employee/import_save',
            data: {
                file: file
            },
            success: function(response) {
                $('body').removeClass('loading');
                if ( response ) {
                    location.href = base_url+'corporate/employee/import';
                } else {
                    Swal.fire({
                        title: 'Error',
                        text: 'Kesalahan pada server',
                        type: 'warning',
                    });
                }
            }
        });
    });
});