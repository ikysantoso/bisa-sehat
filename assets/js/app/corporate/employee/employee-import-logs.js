$(document).ready(function() {
    var tableData = $('#table-user-list');
    tableData.DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": {
        	'url': base_url+'corporate/employee/import',
        	'type': 'POST'
        },
        "columnDefs": [ 
            { "targets": [ 3 ], "visible": false },
        ],
        "createdRow": function( row, data, dataIndex ) {
            $(row).find('.btn-detail').attr( 'href', base_url+'corporate/employee/import_detail/'+data[3] );
        },
        "language": {
            "paginate": {
                "previous": "<i class='fas fa-angle-left'></i>",
                "next": "<i class='fas fa-angle-right'></i>"
            }
        }
    });
});