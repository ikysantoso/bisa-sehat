$(document).ready(function() {
    var tableData = $('#table-user-list');
    tableData.DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": {
        	'url': base_url+'corporate/employee/list',
        	'type': 'POST'
        },
        "columnDefs": [ 
            { "targets": [ 6 ], "visible": false },
            {
                "targets": [5],
                "className": 'text-center',
                "render": function ( data, type, row, meta ) {
                    if ( data == 'waiting' ) {
                        var button_type = 'secondary';
                    } else if ( data == 'approved' ) {
                        var button_type = 'success';
                    } 
                    if ( row[6] != current_user_id ) {
                        var disabled_action = '';
                    } else {
                        var disabled_action = 'disabled="disabled"';
                    }
                    return '<button '+disabled_action+' data-id="'+row[6]+'" class="btn btn-'+button_type+' btn-sm corporate-approval-btn">'+data+'</button>';
                },
            },
        ],
        "createdRow": function( row, data, dataIndex ) {
            if ( data[6] != current_user_id ) {
                $(row).find('.btn-delete').attr( 'data-id', data[6] );
            } else {
                $(row).find('.btn-delete').attr('disabled','disabled');
            }
        },
        "language": {
            "paginate": {
                "previous": "<i class='fas fa-angle-left'></i>",
                "next": "<i class='fas fa-angle-right'></i>"
            },
            "processing": "<img src='"+base_url+"assets/images/loader.svg' />"
        }
    });

    tableData.on('click', '.btn-delete', function(){
        var id = $(this).attr( 'data-id' );

        Swal.fire({
            title: 'Remove from Corporate',
            text: "",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        }).then((result) => {
            if (result.value) {

                $.ajax({
                    type: 'post',
                    url: base_url+'corporate/employee/remove',
                    data: {
                        id: id
                    },
                    success: function(response) {
                        $('body').removeClass('loading');
                        if ( response ) {
                            Swal.fire(
                                'Deleted!',
                                'User has been deleted.',
                                'success'
                            );
                            tableData.DataTable().ajax.reload();
                        }
                    }
                });
            }
        });
    });

    var form_element = $("#approval-member-form");

    tableData.on('click', '.corporate-approval-btn', function(){

        form_element.trigger('reset');

        var id = $(this).attr('data-id');
        var selected_row = $(this).parent().parent();
        var current_status = selected_row.find('td:nth-child(6)').text();

        form_element.find( 'input[name="id"]' ).val( id );
        form_element.find( 'input[name="name"]' ).val( selected_row.find('td:nth-child(2)').text() );
        form_element.find( 'input[name="username"]' ).val( selected_row.find('td:nth-child(3)').text() );
        form_element.find( 'input[name="email"]' ).val( selected_row.find('td:nth-child(4)').text() );
        form_element.find( 'input[name="role"]' ).val( selected_row.find('td:nth-child(5)').text() );
        form_element.find( 'input[name="status"][value="'+current_status+'"]' ).prop('checked', true);
        $("#approval-member-modal").modal('show');
    });

    form_element.validate({
        rules: {},
        highlight: function(element) {
            $(element).closest('.form-group').removeClass('success').addClass('error');
        },
        success: function(element) {
            $(element).closest('.form-group').removeClass('error');
            element.remove();
        },
        submitHandler: function(form) {
            $('body').addClass('loading');
            $.ajax({
                type: 'post',
                url: base_url+'corporate/employee/approval_member',
                data: form_element.serialize(),
                success: function(response) {
                    $('body').removeClass('loading');
                    if ( response ) {
                        $("#approval-member-modal").modal('hide');
                        tableData.DataTable().ajax.reload();
                    }
                }
            });
        }
    });
});