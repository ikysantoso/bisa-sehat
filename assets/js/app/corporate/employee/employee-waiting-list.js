$(document).ready(function() {
    var tableData = $('#table-user-list');
    tableData.DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": {
        	'url': base_url+'corporate/employee/waiting_list',
        	'type': 'POST'
        },
        "columnDefs": [ 
            { "targets": [ 6 ], "visible": false },
            {
                "targets": [5],
                "className": 'text-center',
                "render": function ( data, type, row, meta ) {
                    if ( data == 'waiting' ) {
                        var button_type = 'secondary';
                    } else if ( data == 'approved' ) {
                        var button_type = 'success';
                    } 
                    return '<button data-id="'+row[6]+'" class="btn btn-'+button_type+' btn-sm corporate-approval-btn">'+data+'</button>';
                },
            },
        ],
        "createdRow": function( row, data, dataIndex ) {
            // $(row).find('.btn-delete').attr( 'data-id', data[5] );
        },
        "language": {
            "paginate": {
                "previous": "<i class='fas fa-angle-left'></i>",
                "next": "<i class='fas fa-angle-right'></i>"
            }
        }
    });

    var form_element = $("#approval-member-form");

    tableData.on('click', '.corporate-approval-btn', function(){
        var id = $(this).attr('data-id');
        var selected_row = $(this).parent().parent();

        form_element.find( 'input[name="id"]' ).val( id );
        form_element.find( 'input[name="name"]' ).val( selected_row.find('td:nth-child(2)').text() );
        form_element.find( 'input[name="username"]' ).val( selected_row.find('td:nth-child(3)').text() );
        form_element.find( 'input[name="email"]' ).val( selected_row.find('td:nth-child(4)').text() );
        form_element.find( 'input[name="role"]' ).val( selected_row.find('td:nth-child(5)').text() );

        $("#approval-member-modal").modal('show');
    });

    form_element.validate({
        rules: {},
        highlight: function(element) {
            $(element).closest('.form-group').removeClass('success').addClass('error');
        },
        success: function(element) {
            $(element).closest('.form-group').removeClass('error');
            element.remove();
        },
        submitHandler: function(form) {
            $('body').addClass('loading');
            $.ajax({
                type: 'post',
                url: base_url+'corporate/employee/approval_member',
                data: form_element.serialize(),
                success: function(response) {
                    $('body').removeClass('loading');
                    if ( response ) {
                        $("#approval-member-modal").modal('hide');
                        tableData.DataTable().ajax.reload();
                    }
                }
            });
        }
    });
});