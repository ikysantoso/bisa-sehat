$(document).ready(function() {
    var tableData = $('#table-status');
    tableData.DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": {
        	'url': base_url+'corporate/healthy/status',
        	'type': 'POST'
        },
        "columnDefs": [ 
            { "targets": [ 2,3,6 ], "visible": false },
            { "targets": [ 0,7 ], "orderable": false },
        ],
        "createdRow": function( row, data, dataIndex ) {
            if ( data[4] > 0 && data[5] > 0 ) {
                $(row).find('.btn-download').attr( 'href', base_url+'corporate/healthy/status/download/'+data[6] ).attr('target', '_blank');
                $(row).find('.btn-detail').attr( 'href', base_url+'corporate/healthy/status/detail/'+data[6] );
            } else {
                $(row).find('.btn').addClass('disabled');
            }
        },
        "language": {
            "paginate": {
                "previous": "<i class='fas fa-angle-left'></i>",
                "next": "<i class='fas fa-angle-right'></i>"
            },
            "processing": "<img src='"+base_url+"assets/images/loader.svg' />"
        }
    });
});