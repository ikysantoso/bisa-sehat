$(document).ready(function(){
	var form_element = $('#form-update-password');
	form_element.validate({
		rules: {
		    password: {
				required: true,
				minlength: 8
			},
			password_confirm: {
				required: true,
				minlength: 8,
				equalTo: "#new_password"
			},
		},
	   	highlight: function(element) {
	    	$(element).closest('.form-group').removeClass('success').addClass('error');
	    },
	   	success: function(element) {
	   		$(element).closest('.form-group').removeClass('error');
	    	element.remove();
	    },
	   	submitHandler: function(form) {
	        $('body').addClass('loading');
	        $.ajax({
			    type: 'post',
			    url: base_url+'auth/update_password',
			    data: form_element.serialize(),
			    success: function(response) {
					$('body').removeClass('loading');
			        if ( response ) {
			        	$('body').addClass('loading');
			        	location.href = base_url+'/auth/login';
			        }
			    }
			});
		}
	});
});

$(document).on('click', '.toggle-update-password', function() {

    $(this).toggleClass("fa-eye fa-eye-slash");
    
    var input = $("#new_password");
    input.attr('type') === 'password' ? input.attr('type','text') : input.attr('type','password')
});

$(document).on('click', '.toggle-reset-password', function() {

    $(this).toggleClass("fa-eye fa-eye-slash");
    
    var input = $("#password_confirm");
    input.attr('type') === 'password' ? input.attr('type','text') : input.attr('type','password')
});

