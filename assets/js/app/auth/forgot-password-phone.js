$(document).ready(function(){
	var form_element = $('#form-forgot-password');
	form_element.validate({
		rules: {},
	    highlight: function(element) {
	    	$(element).closest('.form-group').removeClass('success').addClass('error');
	    },
	   	success: function(element) {
	   		$(element).closest('.form-group').removeClass('error');
	    	element.remove();
	    },
	   	submitHandler: function(form) {
	        $('body').addClass('loading');
	        $.ajax({
				type: 'post',
			    url: base_url+'auth/forgot_password_p',
			    data: form_element.serialize(),
			    success: function(response) {
			    	if ( response ) {
			    		location.reload();
			    	}
			    }
			});
	    }
	});
});
