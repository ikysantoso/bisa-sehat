$(document).ready(function(){
	var form_element = $('#form-register');

	$.validator.addMethod("loginRegex", function(value, element) {
			return this.optional(element) || /^[a-z0-9]+$/i.test(value);
	}, "Username must contain only letters or numbers");

	var validator = form_element.validate({
	rules: {
		"username": {
			required: true,
			loginRegex: true,
		},
		password: {
			required: true,
			minlength: 8
		},
		password_confirm: {
			required: true,
			minlength: 8,
			equalTo: "#new_password"
		},
	},
	highlight: function(element) {
		$(element).closest('.form-group').removeClass('success').addClass('error');
		},
		success: function(element) {
				$(element).closest('.form-group').removeClass('error');
			element.remove();
		},
		submitHandler: function(form) {
			$('body').addClass('loading');
			$.ajax({
				type: 'post',
				url: base_url+'auth/register',
				data: form_element.serialize(),
				success: function(response) {
					$('body').removeClass('loading');

					if (response) {
						if (response.type=='success') {
								$('body').addClass('loading');
								location.href=base_url+'auth/register';
						} else {
							Swal.fire({
								title: 'Gagal',
								text: 'Registrasi Gagal, Silahkan hubungi Administrator',
								icon: 'warning',
							});
						}
					}
				},
				error: function(xhr){
					if ( xhr.status == 409 ) {
						$('body').removeClass('loading');
						validator.showErrors({
							[xhr.responseJSON.field]: xhr.responseJSON.text
						});
					}
					}
			});
		}
	});
});

$(document).on('click', '.toggle-password', function() {

    $(this).toggleClass("fa-eye fa-eye-slash");
    
    var input = $("#new_password");
    input.attr('type') === 'password' ? input.attr('type','text') : input.attr('type','password')
});

$(document).on('click', '.toggle-confirm-password', function() {

    $(this).toggleClass("fa-eye fa-eye-slash");
    
    var input = $("#password_confirm");
    input.attr('type') === 'password' ? input.attr('type','text') : input.attr('type','password')
});

