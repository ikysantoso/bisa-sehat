$(document).ready(function(){
	var form_element = $('#form-login');
	form_element.validate({
		rules: {},
		highlight: function(element) {
				$(element).closest('.form-group').removeClass('success').addClass('error');
		},
		success: function(element) {
			$(element).closest('.form-group').removeClass('error');
			element.remove();
		},
		submitHandler: function(form) {
			$('body').addClass('loading');
			$.ajax({
				type: 'post',
				url: base_url+'auth/login',
				data: form_element.serialize(),
				success: function(response) {
					$('body').removeClass('loading');
					var parsedResponse = $.parseJSON(response);

					if (parsedResponse) {
						if (parsedResponse.type == 'success') {
							$('body').addClass('loading');
								location.href = parsedResponse.redirect;
						} else {
							$("#form-login").find( '.alert' ).remove();
							$("#form-login").prepend( '<div class="alert alert-warning">'+parsedResponse.message+'</div>' );
						}
					}
				}
			});
		}
	});
});

$(document).on('click', '.toggle-login-password', function() {

    $(this).toggleClass("fa-eye fa-eye-slash");
    
    var input = $("#password");
    input.attr('type') === 'password' ? input.attr('type','text') : input.attr('type','password')
});
