$(document).ready(function() {
	$('.js-example-basic-single').select2().next().hide();

	grandTotal();

	$("#address-list .btn-check-address").click(function() {
		$(this).parent().parent().find('.form-check-input').prop('checked', true);
	});

	$('#change-address-modal').on('shown.bs.modal', function () {
		var address = $("#selected-address").attr('data-id');

		$("#address-list").find('.form-check-input[value="'+address+'"]').prop('checked', true);
	});

	$("input[name='payment-method']").change(function() {
		var paymentMethodDOM =  $(this);

		changePaymentMethod(paymentMethodDOM);
	});

	$("#voucher-select").on('change', function() {
		var voucherDOM = $(this);

		changeVoucher(voucherDOM);
	});

	$(".shipping-fee-list").on('change', function() {
		var shippingFeeDOM = $(this);

		changeShippingFee(shippingFeeDOM);
	});
});

function changeAddress() {
	var address = $("input[name='address']:checked");

	var addressTitle = address.parent().next().text();
	var addressContent = address.parent().parent().next().find('.card-body').html();
	var id = address.val();

	$("#selected-address").attr('data-id', id);
	$("#selected-address").attr('data-city', address.attr('data-city'));
	$("#selected-address .card-header").text( addressTitle );
	$("#selected-address .address").html( addressContent );
	$('#change-address-modal').modal('hide');

	$.ajax({
		type: 'post',
		url: `${base_url}checkout/set_shipping_address`,
		data: {
			id: id
		},
		success: function(result) {
			var response = JSON.parse(result);

			if (response === "success") {
				location.reload();
			}
		}
	})
}

function grandTotal() {
	var shippingMethod = $(".shipping-fee-list").find('option:selected').val();
	// var shippingFee = parseInt(shippingMethod.split(".")[1]);
	var shippingFeeTotal = parseInt(shippingMethod.split(".")[2]);

	var productFeeTotal = 0;
	var shippingFeeTotal = 0;

	$.each( $(".cart-item"), function(index, item){
		productFeeTotal += parseInt( $(item).find('.sub-total').attr('data-subtotal') );
		var current_shipping_fee = parseInt( $(item).find(".shipping-fee-list").val() );
		shippingFeeTotal += current_shipping_fee;

		if ( current_shipping_fee ) {
			$(item).find('.shipping-fee').html( '<span style="font-weight:lighter">('+$(item).find(".shipping-fee-list").attr('data-weight')+' g)</span> Rp '+current_shipping_fee.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") );
		}
	});

	var grandFeeTotal = shippingFeeTotal + productFeeTotal;

	// $(".shipping-fee").text( 'Rp '+((( shippingFee ) > 0) ? shippingFee : 0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") );
	$("#product-total").text( 'Rp '+((( productFeeTotal ) > 0) ? productFeeTotal : 0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") );
	$("#shipping-fee-total").text( 'Rp '+((( shippingFeeTotal ) > 0) ? shippingFeeTotal : 0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") );
	$("#grand-total").text( 'Rp '+((( grandFeeTotal ) > 0) ? grandFeeTotal : 0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") );

	if ( grandFeeTotal > 0 ) {
		$("#btn-checkout").removeAttr('disabled');
	}
}

function checkoutOrder() {
	$('body').addClass('loading');

	var shippingMethod = $(".shipping-fee-list").find('option:selected').val();
	var shippingName = shippingMethod.split(".")[0];
	var shippingFee = parseInt(shippingMethod.split(".")[2]);

	var weight = $(".weight-total").attr("data-weight-total");

	var cartItems = [];

	$.each($(".cart-item"), function(index, item){
		cartItems.push({
			cart_id: $(item).find(".shipping-fee-list").attr('data-cart'),
			courier_code: $(item).find("input[name='courier-code']").val(),
			courier_service: $(item).find("input[name='courier-service']").val(),
		});
	});

	var params = {
		'customer_address': $("#selected-address").attr('data-id'),
		'cart_items' : cartItems,
		'shipping_name' : shippingName,
		'shipping_fee' : shippingFee,
		'weight': weight
	};

	$.ajax({
		type: 'post',
		url: `${base_url}checkout/submit`,
		data: params,
		success: function(response) {
			if (response.status === 'success') {
				window.snap.pay(response.payment_token);
			} else {
				$('body').removeClass('loading');

				Swal.fire(
					response.title,
					response.message,
					response.status
				);
			}
		}
	});
}

function changePaymentMethod(paymentMethodDOM) {
	$("#modal-bank").find('.list-group-item').removeClass('bg-light');
	$("#modal-bank").find("input[name='payment-method']:checked").parent().parent().addClass('bg-light');

	var paymentOption = '';

	if (paymentMethodDOM.val() === 'bisapay') {
		paymentOption = `<h6 class="font-weight-bold">${paymentMethodDOM.next().html()}</h6>`;
	} else {
		var paymentOptionData = JSON.parse(paymentMethodDOM.attr('data-bank') );

		paymentOption = `<h6 class="font-weight-bold">${paymentOptionData.name}</h6>
		<div class="norek">${paymentOptionData.account_number}</div>
		<div class="name">a.n <strong>${paymentOptionData.account_name}</strong></div>`;
	}

	$("#selected-payment").html(paymentOption);
	$("#selected-payment").attr('data-id', paymentMethodDOM.val());

	$("#modal-bank").modal('hide');
}

function changeVoucher(voucherDOM) {
	var voucherNominal = parseInt(voucherDOM.find('option:selected').attr('data-nominal'));
	var voucherName = voucherDOM.find('option:selected').attr('data-name');

	if (voucherNominal > 0) {
		var replacedVoucherNominal = voucherNominal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

		$("#voucher-user").html(`<span style="font-weight:lighter">(${voucherName})</span> - Rp ${replacedVoucherNominal}`);
	} else {
		$("#voucher-user").html('0');
	}

	grandTotal();
}

function changeShippingFee(shippingFeeDOM) {
	grandTotal();

	var value = shippingFeeDOM.find('option:selected').val();

	if (value.split(".")[0] === ("Instan" || "Pickup")) {
		$('.shipping-fee').attr('hidden', 'true');
		$('#shipping-fee-total').attr('hidden', 'true');
		$('.hint-fee').removeAttr('hidden');
		$('.hint-fee-total').removeAttr('hidden')
	} else {
		$('.shipping-fee').removeAttr('hidden');
		$('#shipping-fee-total').removeAttr('hidden');
		$('.hint-fee').attr('hidden', 'true');
		$('.hint-fee-total').attr('hidden', 'true');
	}
}

function showHintFee() {
	Swal.fire(
		'Pengiriman Non Jabodetabek',
		'Untuk metode pengiriman ini, biaya akan dikonfirmasi oleh admin',
		'warning'
	);
}

function showHintFeeTotal() {
	Swal.fire(
		'Pengiriman Non Jabodetabek',
		'Untuk metode pengiriman ini, biaya akan dikonfirmasi oleh admin',
		'warning'
	);
}

function showPaymentTokenSnap($id) {
	$.ajax({
		type: 'post',
		url: `${base_url}checkout/show_payment_token`,
		data: {
			'id': $id
		},
		success: function(response) {
			if (response.status === 'success') {
				window.snap.pay(response.payment_token);
			} else {
				$('body').removeClass('loading');

				Swal.fire(
					response.title,
					response.message,
					response.status
				);
			}
		}
	});
}

load_ongkir();

	function load_ongkir(){

		$("#btn-checkout").attr('disabled','disabled');

		$.each( $(".shipping-fee-list"), function(){

			var current_shipping_selector = $(this);

			current_shipping_selector.html('');
			var shipping_content = current_shipping_selector.parent();
			shipping_content.before('<div class="loader-box"></div>');

			var destination = $("#selected-address").attr('data-city');
			$.ajax({
		    	type: 'post',
		        url: base_url+"api/ongkir",
		        data: {
		        	'cart_id': current_shipping_selector.attr('data-cart'),
		        	'destination': destination,
		        },
		        success: function(response) {
		            shipping_content.parent().find('.loader-box').remove();

		            if ( response ) {
		            	
		            	current_shipping_selector.attr( 'data-weight', response.weight );

						$.each(response.data, function(index,item){

			            		if ( item.rajaongkir.status.code == 200 ) {
				            		var item = item.rajaongkir.results[0];

				            		var costs = '';
				            		$.each(item.costs, function(index_cost, cost){
				            			costs += '<option data-courier='+"'"+JSON.stringify({'code': item.code, 'service':cost.service})+"'"+' value="'+cost.cost[0].value+'">'+item.code.toUpperCase()+' '+cost.service+' - '+cost.cost[0].etd+' Hari (Rp '+cost.cost[0].value+')</option>'
				            		});
				            		if ( costs != '' ) {
						            	current_shipping_selector.append(`
						            		<optgroup label="`+item.name+`">`+costs+`</optgroup>
						            	`);
				            		}

				            		if (index==0) {
				            			$("input[name='courier-code']").val( item.code );
										$("input[name='courier-service']").val( item.costs[0].service );
				            		}

				            		
				            	} else if ( item.rajaongkir.status.code == 400 ) {
									var message = (item.rajaongkir.status.description).replace("Bad request. Weight", "Berat");

				            		Swal.fire(
		                                'Warning!',
		                                message,
		                                'warning'
		                            );
				            		$("#btn-checkout").attr('disabled','disabled');
									$("tr[data-weight-total='"+response.weight+"']").addClass('bg-warning');
		                            return false;
				            	}
			            	})
			            
		            }
		            grandTotal();
		        }
			});

		});

		
	}

	$(".shipping-fee-list").on('change', function(){
		var data_courier = JSON.parse( $(this).find('option:selected').attr('data-courier') );
		$(this).parent().find("input[name='courier-code']").val( data_courier.code );
		$(this).parent().find("input[name='courier-service']").val( data_courier.service );
		grandTotal( parseInt($(this).val()) );	
	});