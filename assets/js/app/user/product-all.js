$(document).ready(function(){
	$(".buy-btn").click(function(){
		var id = $(this).attr('data-id');

		$.ajax({
				type: 'post',
				url: base_url+'product/stock',
				data: {
					id: id
				},
				success: function(response) {
					$('body').removeClass('loading');

					$("#product-image").attr('src', (response.image) ? base_url+'assets/images/upload/product/'+response.image : 'https://via.placeholder.com/150x150?text=No%20Image');

					let name = response.name;
					let price = `Rp ${(response.price).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}`;

					if (response.discount === null) {
						$("#modal-product-container").html(`<h5 id="product-name" class="mt-0 mb-2">${name}</h5>`);
						$("#modal-product-container").append(`<h5 id="product-price" class="mt-2 after-discount">${price}</h5>`);
					} else {
						let discount = `Rp ${(response.discount).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}`;

						$("#modal-product-container").html(`<h5 id="product-name" class="mt-0 mb-2">${name}</h5>`);
						$("#modal-product-container").append(`<strike id="product-price" class="mt-3 before-discount">${price}</strike>`);
						$("#modal-product-container").append(`<h5 id="product-discount" class="after-discount">${discount}</h5>`);
					}

					$("#btn-payment, #btn-add-to-cart").attr('disabled','disabled').attr('data-id', id);
					$("#product-stock-list").html('<option disabled="disabled" selected="selected">--Pilih Stock--</option>');

					$.each(response.stocks, function(index, item){
						var stock_info = (response.is_preorder == 'no') ? '('+item.stock+' stok)' : '';
						$("#product-stock-list").append('<option data-preorder="'+response.is_preorder+'" data-stock="'+item.stock+'" value="'+item.id+'">'+item.warehouse+' '+stock_info+'</option>');
					});

					$("#modal-buy").modal('show');
				}
		});
	});

	$("#product-stock-list").change(function(){
		var selected_stock = $(this).find('option:selected');
		if ( selected_stock.val() ) {
			if ( selected_stock.attr('data-stock') == '0' ) {
				$("#btn-add-to-cart, #btn-buy-now").removeAttr('disabled');
				$("#btn-buy-now span").text('Preorder Sekarang')
			} else {
				$("#btn-add-to-cart, #btn-buy-now").removeAttr('disabled');
				$("#btn-buy-now span").text('Beli Sekarang')
			}
		} else {
			$("#btn-add-to-cart,#btn-buy-now").removeAttr('disabled');
		}
	});

	$('#modal-buy').on('shown.bs.modal', function() {
        $(document).off('focusin.modal');
    });

});