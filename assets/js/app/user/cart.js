$(document).ready(function(){
	var cart_table = $("#table-cart tbody");

	$("input[name='check-all']").prop('checked', false);
	cart_table.find('.product-check').prop('checked', false);

	// check / uncheck all
	$("input[name='check-all']").change(function(){
		cart_table.find('.product-check').prop('checked', $(this).is(':checked'));
		generate_sub_total();
	});

	// selected check
	var product_checkboxs = cart_table.find('.product-check');
	$.each(product_checkboxs, function(index, item){

		$(item).change(function(){

			var checked_total = cart_table.find('.product-check:checked').length;

			generate_sub_total();

			// is check/unchecked all or not
			if ( checked_total == product_checkboxs.length ) {
				$("input[name='check-all']").prop('checked', true);
			} else {
				$("input[name='check-all']").prop('checked', false);
			}

		});
	});

	// change quantity
	$(".quantity-update-btn").click(function(){
		var current_value = parseInt($(this).parent().find('.quantity').val());
		var new_value = 0;
		var id = $(this).parent().parent().parent().find('.product-check').val();

		if ( $(this).hasClass('increase-value') ) {
			new_value = current_value + 1;
		}

		if ( $(this).hasClass('decrease-value') ) {
			new_value = current_value - 1;
		}

		update_quantity_cart( id, new_value );

	});

	function generate_sub_total(){
		var sub_total = 0;
		var total_checked = 0;
		$.each(cart_table.find('tr'), function(index, item){
			if ( $(item).find('.product-check').is(':checked') ) {
				sub_total += $(item).find('.price').attr('data-price') * $(item).find('.quantity').val();
				total_checked++;
			}
		});

		$("#sub-total").text( 'Rp '+sub_total.toLocaleString('en')  );

		if ( total_checked > 0 ) {
			$("#checkout-btn").removeAttr('disabled');
		} else {
			$("#checkout-btn").attr('disabled','disabled');
		}
	}

	function update_quantity_cart( id, quantity ){
		if ( quantity == 0 ) {
			delete_cart( id );
		} else {
			$('body').addClass('loading');
			$.ajax({
				type: 'post',
			    url: base_url+'cart/update_cart_item',
			    data: {
			    	cart_id: id,
			    	quantity: quantity,
			    },
			    success: function(response) {
			        $('body').removeClass('loading');
			        if ( response ) {
						$("#quantity-"+id).val( response );
						$("#sub_total-"+id).text( 'Rp '+(response * $("#price-"+id).attr('data-price') ).toLocaleString('en') );
						generate_sub_total();
			    	}
			    }
			});
		}
	}

	function delete_cart( id ){
		Swal.fire({
		            title: 'Hapus produk ini dari keranjang?',
		            text: "",
		            type: 'warning',
		            showCancelButton: true,
		            confirmButtonColor: '#3085d6',
		            cancelButtonColor: '#d33',
		            confirmButtonText: 'Ya'
		        }).then((result) => {
		            if (result.value) {

		                $.ajax({
		                    type: 'post',
		                    url: base_url+'cart/delete_cart_item',
		                    data: {
		                        cart_id: id
		                    },
		                    success: function(response) {
		                        $('body').removeClass('loading');
		                        if ( response ) {
		                        	$('body').addClass('loading');
		                        	location.reload();
		                        }
		                    }
		                });
		            }
		});
	}

	// submit checkout
	$("#checkout-btn").click(function(){

		$('body').addClass('loading');

		var cart_ids = [];
		$.each(cart_table.find('tr'), function(index, item){
			if ( $(item).find('.product-check').is(':checked') ) {
				cart_ids.push($(item).find('.product-check').val());
			}
		});

		if (cart_ids.length > 0) {
			$.ajax({
				type: 'post',
		        url: base_url+'cart/pre_checkout',
		        data: {
		        	cart_id: cart_ids.toString()
		        },
		        success: function(response) {
		            if ( response.status == 'success' ) {
		            	var redirect = base_url+'checkout';
						$.redirectPost(redirect, {cart_id: cart_ids.toString()});
		            } else {
		            	$('body').removeClass('loading');
		        		Swal.fire(
		                    response.title,
		                    response.message,
		                	response.status
						);
		            }
		    	}
			});
		}
	})

});