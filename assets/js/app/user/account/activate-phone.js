$(document).ready(function() {
	var district_loaded = false;
	var sub_district_loaded = false;
	var village_loaded = false;

	load_fast_select();

    $("input[name='date-birth']").on('click', function () {
        datepicker({uiLibrary: 'bootstrap4',
        format: 'mm/dd/yyyy',})
                    });
	$("input[name='date-birth']").datepicker({
		uiLibrary: 'bootstrap4'
	});

	function load_fast_select(reinit = false, selected_data = null) {
		if (reinit) {
			$('input[name="province"]').data('fastselect').destroy();
		}

		var initialValue_data = (selected_data && selected_data.province) ? [{"text": selected_data.province.text, "value":selected_data.province.value}] : null;

		$('input[name="province"]').fastselect({
			userOptionAllowed: true,
			initialValue: initialValue_data,
			onItemSelect: function($item, itemModel) {
				trigger_load_district(itemModel.value);
			}
		});

		if (initialValue_data) {
			$('input[name="province"]').val(selected_data.province.value);
			trigger_load_district(selected_data.province.value, selected_data);
		}
	}

	function trigger_load_district(district_value, selected_data = null) {
		if (district_loaded) {
			$("input[name='district']").data('fastselect').destroy();
			$("input[name='district']").addClass('form-control').val('').attr('disabled','disabled');
			district_loaded = false;
		}

		if (sub_district_loaded) {
			$("input[name='sub-district']").data('fastselect').destroy();
			$("input[name='sub-district']").addClass('form-control').val('').attr('disabled','disabled');
			sub_district_loaded = false;
		}

		if (village_loaded) {
			$("input[name='village']").data('fastselect').destroy();
			$("input[name='village']").addClass('form-control').val('').attr('disabled','disabled');
			village_loaded = false;
		}

		if (!selected_data) {
			$("textarea[name='address']").val('').attr('disabled','disabled');
		}

		load_districts(district_value, selected_data);

		$("#account-activation-form input[name='province']").valid();
	}

	function load_districts(id, selected_data = null){
		var selector = $("input[name='district']");
		selector.removeClass('form-control').removeAttr('disabled');

		if (district_loaded) {
			selector.data('fastselect').destroy();
		}

		var initialValue_data = (selected_data && selected_data.district) ? [{"text": selected_data.district.text, "value":selected_data.district.value}] : null;

		district_loaded = selector.fastselect({
			url: base_url+'api/wilayah/city/'+id,
			userOptionAllowed: true,
			initialValue: initialValue_data,
			onItemSelect: function($item, itemModel){
				load_sub_districts( itemModel.value, selected_data );
				$("#account-activation-form input[name='district']").valid();
			}
		});

		if (initialValue_data) {
			selector.val(selected_data.district.value);
			load_sub_districts(selected_data.district.value, selected_data);
		}
	}

	function load_sub_districts(id, selected_data = null) {
		var selector = $("input[name='sub-district']");
		selector.removeClass('form-control').removeAttr('disabled');

		if (sub_district_loaded) {
			selector.data('fastselect').destroy();
		}

		var initialValue_data = (selected_data && selected_data.subdistrict) ? [{"text": selected_data.subdistrict.text, "value":selected_data.subdistrict.value}] : null;

		sub_district_loaded = selector.fastselect({
			url: base_url+'api/wilayah/subdistrict/'+id,
			userOptionAllowed: true,
			initialValue: initialValue_data,
			onItemSelect: function($item, itemModel){
				load_villages( itemModel.value );
				$("#account-activation-form input[name='sub-district']").valid();
			}
		});

		if (selected_data) {
			selector.val(selected_data.subdistrict.value);
			load_villages(id, selected_data);
		}
	}

	function load_villages(id, selected_data = null) {
		var selector = $("input[name='village']");
		selector.removeClass('form-control').removeAttr('disabled');

		if (village_loaded) {
			selector.data('fastselect').destroy();
		}

		var initialValue_data = (selected_data && selected_data.village) ? [{"text": selected_data.village.text, "value":selected_data.village.value}] : null;

		village_loaded = selector.fastselect({
			url: base_url+'api/wilayah/village/'+id,
			userOptionAllowed: true,
			initialValue: initialValue_data,
			onItemSelect: function($item, itemModel){
				load_address( itemModel.value );
				$("#account-activation-form input[name='village']").valid();
			}
		});

		if (selected_data) {
			selector.val(selected_data.village.value);
			load_address(id, selected_data);
		}
	}

	function load_address(){
		$("textarea[name='address']").removeAttr('disabled');
	}

	var form_element = $("#account-activation-form");
	var validator = form_element.validate({
	rules: {},
		highlight: function(element) {
			$(element).closest('.form-group').removeClass('success').addClass('error');
		},
		success: function(element) {
			$(element).closest('.form-group').removeClass('error');
			element.remove();
		},
		submitHandler: function(form) {
			$('body').addClass('loading');
				$.ajax({
					type: 'post',
					url: base_url+'auth/finish_registration_p',
					data: form_element.serialize(),
					success: function(response) {
						$('body').removeClass('loading');
							if (response.redirect) {
								location.href = response.redirect;
							} else {
									Swal.fire({
											title: 'Error',
											text: 'Kesalahan pada server',
											type: 'warning',
									});
						}
					},
					error: function(xhr){
						if ( xhr.status == 409 ) {
							$('body').removeClass('loading');
								validator.showErrors({
									[xhr.responseJSON.field]: xhr.responseJSON.text
								});

								Swal.fire({
													title: 'Kesalahan pada form',
													text: xhr.responseJSON.text,
												type: 'warning',
											});
						}
					}
				});
			}
    });

    // $('#toggle-event').change(function() {
	// 		if ($(this).is(":checked")) {
	// 		$("#corporate-field").show();
	// 		$("#corporate-option").hide();
	// 		form_element.find('input[name="is_new_corporate"]').val('yes');
	// 	} else {
	// 		$("#corporate-field").hide();
	// 		$("#corporate-option").show();
	// 		form_element.find('input[name="is_new_corporate"]').val('no');
	// 	}
	// });
});