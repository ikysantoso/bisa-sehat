$(document).ready(function(){


    var form_element = $("#form-update-profile");
    var validator = form_element.validate({
		rules: {},
        highlight: function(element) {
        	$(element).closest('.form-group').removeClass('success').addClass('error');
        },
       	success: function(element) {
        	$(element).closest('.form-group').removeClass('error');
        	element.remove();
        },
       	submitHandler: function(form) {
        	$('body').addClass('loading');
            $.ajax({
            	type: 'post',
                url: base_url+'member/profile/update',
                data: form_element.serialize(),
                success: function(response) {
                	$('body').removeClass('loading');
                    if ( response ) {
                    	location.href = base_url+'member/dashboard';
                    } else {
                        Swal.fire({
                            title: 'Error',
                            text: 'Kesalahan pada server',
                        	icon: 'warning',
                        });
                	}
              	},
                error: function(xhr){
                    if ( xhr.status == 409 ) {
                        $('body').removeClass('loading');
                        validator.showErrors({
                            [xhr.responseJSON.field]: xhr.responseJSON.text
                        });

                        Swal.fire({
                            title: 'Kesalahan pada form',
                            text: xhr.responseJSON.text,
                            type: 'warning',
                        });
                    }
                }
        	});
        }
    });
});