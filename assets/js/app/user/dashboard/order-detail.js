$(document).ready(function(){
	$("#payment-confirmation").click(function(){
		$("#modal-order-confirmation").modal('show');
	});

	form_element_payment = $("#form-order-confirmation");
    form_element_payment.validate({
        rules: {},
        messages: {
        },
        highlight: function(element) {
            $(element).closest('.form-group').removeClass('success').addClass('error');
        },
        success: function(element) {
            element.remove();
        },
        submitHandler: function(form) {
			var id = $(".submit-order-confirmation").attr('data-id');
            var image = $('#order-confirmation-file').prop('files')[0];

			var form_data = new FormData();
			form_data.append('id', id);
			form_data.append('file', image);

            if (image === undefined) {
                Swal.fire({
                    title: 'Pengiriman tidak di proses',
                    text: 'Anda belum mengirim bukti pembayaran',
                    type: 'error'
                })
            } else {
                $('body').addClass('loading');

				$.ajax({
					type: 'post',
					url: base_url+'member/order/payment_confirmation',
					data: form_data,
                    dataType: 'json',
                    cache: false,
                    processData: false,
                    contentType: false,
					success: function(response) {
						$('body').removeClass('loading');

                        if (response.code === "230") {
                            $("#modal-order-confirmation").modal('hide');

                            Swal.fire({
                                title: response.title,
                                text: response.message,
                                type: response.status
                            })
                            .then(() => {
                                location.reload();
                            })
                        } else {
                            Swal.fire({
                                title: response.title,
                                text: response.message,
                                type: response.status,
                                confirmButtonText: 'Kirim ulang',
                                showCancelButton: true
                            })
                            .then(() => {
                                console.log('kirim ulang');
                            })
                        }
					}
				});
            }
        }
    });

    $("#phone_activation_button").on('click', function () {
        $.ajax({
            type: 'post',
            url: base_url+'member/order/activation_phone',
            data: {
                activation_phone_code: $("#activation_phone_code"),
                username_phone_activation: $("#username_phone_activation"),
            },
            success: function(response) {
                window.location.reload();
            }
        });
    });

    $("#request_phone_activation_button").on('click', function () {
        $.ajax({
            type: 'post',
            url: base_url+'member/order/request_phone_activation',
            data: {
                username: "testusername"
            }
        });
    })
});

function transactionConfirmation() {
	var id = $('.btn-transaction-confirmation').attr('data-id');

	Swal.fire({
			title: 'Sudah terima barang?',
			text: "",
			type: 'question',
			showCancelButton: true,
	}).then((result) => {
			if (result.value) {
				$('body').addClass('loading');
				$.ajax({
					type: 'post',
					url: base_url+'member/order/transaction_confirmation',
					data: {
						'id': id
					},
					success: function(response) {
						if (response) {
							location.reload();
						} else {
							$('body').removeClass('loading');
							Swal.fire('Error', 'Transaksi tidak ditemukan', 'warning');
						}
					}
				});
			}
	});
}

function showHint() {
	Swal.fire(
		'Pengiriman Non Jabodetabek',
		'Untuk metode pengiriman ini, biaya akan dikonfirmasi oleh admin',
		'warning'
	);
}