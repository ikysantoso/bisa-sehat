$(document).ready(function() {
    $("#product-stock-list").change(function(){

		var selected_stock = $(this).find('option:selected');

		if (selected_stock.val()) {
			$("#btn-payment").removeAttr('disabled');
		}
	});
});

function continuePayment() {
    var quantity = $('#product-stock-list').find('option:selected');
    var warehouse = $("#product-stock-list").val();
    var id = document.getElementById("btn-payment").getAttribute('data-id');

    Swal.fire({
        type: 'question',
        title: 'Quantity',
        input: 'number',
        confirmButtonText: '<i class="fas fa-shopping-bag"></i> Beli Sekarang',
        confirmButtonColor: '#f33f3f',
        showLoaderOnConfirm: true,
        inputAttributes: {
            min: 1,
            max: quantity,
            step: 1,
            pattern: `[0-9]{${quantity}}`
        },
        inputValidator: (value) => {
            if (value > quantity) {
                return 'Jumlah tidak boleh lebih besar dari stok yang tersedia'
            }
            if (value < 0) {
                return 'Masukan minimal 1'
            }
            if (value == 0) {
                return 'Masukan minimal 1'
            }
        },
        preConfirm: (quantity) => {
            return $.ajax({
                type: 'post',
                url: base_url+'product/add_to_cart',
                data: {
                    product_id: id,
                    warehouse_id: warehouse,
                    quantity: quantity,
                    transaction_type: 'checkout'
                },
                success: function(response) {
                    $('body').removeClass('loading');
                    if ( response.type == 'success' ) {
                        return response.data.id;
                    } else {
                        Swal.fire({
                            title: response.title,
                            text: response.message,
                            type: response.type,
                        }).then((result) => {
                            $('body').addClass('loading');
                            if ( response.redirect != '' ) {
                                location.href =  response.redirect;
                            } else {
                                location.reload();
                            }
                        });
                    }
                }
            });

        },
        allowOutsideClick: () => !Swal.isLoading()
    }).then((result) => {
        if (result.value) {
            if ( result.value.type == 'warning' ) {
                var redirect_url = (result.value.redirect) ? result.value.redirect : null;
                Swal.fire({
                    title: result.value.title,
                    text: result.value.message,
                    type: result.value.type,
                }).then((result) => {
                    if ( redirect_url ) {
                        location.href =  redirect_url;
                    }
                });
            } else {
                var redirect = base_url+'checkout';
                $.redirectPost(redirect, {cart_id: result.value.data.id});
            }
        }
    });
}

function addProductToCart() {
    var quantity = $('#product-stock-list').find('option:selected');
    var id = document.getElementById("btn-add-to-cart").getAttribute('data-id');
    var warehouse = $("#product-stock-list").val();

    Swal.fire({
        type: 'question',
        title: 'Quantity',
        input: 'number',
        confirmButtonText: 'Masukan Keranjang Sekarang',
        confirmButtonColor: '#87d7bf',
        showLoaderOnConfirm: true,
        inputAttributes: {
            min: 1,
            max: quantity,
            step: 1,
            pattern: `[0-9]{${quantity}}`
        },
        inputValidator: (value) => {
            if (value > quantity) {
                return 'Jumlah tidak boleh lebih besar dari stok yang tersedia'
            }
            if (value < 0) {
                return 'Masukan minimal 1'
            }
            if (value == 0) {
                return 'Masukan minimal 1'
            }
        },
    }).then(function(quantity) {
        let data = {
            product_id: id,
            warehouse_id: warehouse,
            quantity: quantity.value,
            transaction_type: 'cart'
        };

        $.ajax({
            type: 'post',
            url: base_url+'product/add_to_cart',
            data: data,
            success: function(response) {
                $('body').removeClass('loading');

                if (response.type === 'success') {
                    $('body').addClass('loading');
                     location.reload();
                } else {
                    Swal.fire({
                        title: response.title,
                        text: response.message,
                        type: response.type,
                    }).then(() => {
                        $('body').addClass('loading');
                        if (response.redirect !== '') {
                            location.href =  response.redirect;
                        } else {
                            location.reload();
                        }
                    });
                }
            }
        });
    });
}