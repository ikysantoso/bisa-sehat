function doClaimVoucher() {
    var voucherId = document.getElementById("card-voucher").getAttribute('data-id');

    $.ajax({
       type: 'post',
       url: base_url + "member/voucher/claim",
       data: {
           voucherId: voucherId
       },
       success: function(response) {
        for (i = 0; i < response.length; i++) {
            Swal.fire({
                title: 'Voucher Berhasil di Klaim',
                text: 'Sisa Kuota Voucher ' + response[i].quantity,
                type: 'success'
            });
        }
       }
    });
}