$.ajax({
	type: 'get',
	url: base_url + 'home/getDiagnoseTotal',
	success: function(response) {
		new Chart($("#chart-blood-pressure"), {
			type: 'line',
			data: {
				labels: response.date,
				datasets: [
					{
						label: "Tekanan Darah Atas",
						data: response.sistole,
						borderColor: "#008081"
					},
					{
						label: "Tekanan Darah Bawah",
						data: response.diastole,
						borderColor: "#008080"
					}
				]
			},
			options: {
      annotation: {
        annotations: [
          {
            drawTime: "afterDatasetsDraw",
            type: "line",
            mode: "horizontal",
            scaleID: "y-axis-0",
            value: 90,
            borderWidth: 2,
            borderColor: "red",
            label: {
              content: "",
              enabled: true,
              position: "top"
            }
          },
          {
            drawTime: "afterDatasetsDraw",
            type: "line",
            mode: "horizontal",
            scaleID: "y-axis-0",
            value: 140,
            borderWidth: 2,
            borderColor: "red",
            label: {
              content: "",
              enabled: true,
              position: "top"
            }
          }
        ]
      },
				scales: {
					yAxes: [{
						ticks: {
							min: 50,
							max: 180,
          maxTicksLimit: 10,
							beginAtZero:false,
          callback: function (value, index, values) {
          	if(value==50){return '50/kurang';}else if(value==180){return '180/lebih';}else{return value;}
          }
						}
					}],
				},
				maintainAspectRatio: false,
				plugins: {
					title: {
						display: true,
						text: ["Tekanan Darah"],
						align: "center",
						position: "bottom",
						font: {
							size: 18
						},
					},
				}
			}
		});

		new Chart($("#chart-blood-sugar"), {
			type: 'line',
			data: {
				labels: response.date,
				datasets: [
					{
						label: "Gula Darah",
						data: response.bloodSugar,
						borderColor: "#008080"
					}
				]
			},
			options: {
      annotation: {
        annotations: [
          {
            drawTime: "afterDatasetsDraw",
            type: "line",
            mode: "horizontal",
            scaleID: "y-axis-0",
            value: 70,
            borderWidth: 3,
            borderColor: "yellow",
            label: {
              content: "",
              enabled: true,
              position: "top"
            }
          },
          {
            drawTime: "afterDatasetsDraw",
            type: "line",
            mode: "horizontal",
            scaleID: "y-axis-0",
            value: 140,
            borderWidth: 3,
            borderColor: "red",
            label: {
              content: "",
              enabled: true,
              position: "top"
            }
          }
        ]
      },
				scales: {
					yAxes: [{
						ticks: {
							min: 55,
							max: 300,
          maxTicksLimit: 10,
							beginAtZero:false,
          callback: function (value, index, values) {
          	if(value==55){return '55/kurang';}else if(value==300){return '300/lebih';}else{return value;}
          }
						}
					}],
				},
				maintainAspectRatio: false,
				plugins: {
					title: {
						display: true,
						text: ["Gula Darah"],
						align: "center",
						position: "bottom",
						font: {
							size: 18
						}
					}
				}
			}
		})

		new Chart($("#chart-cholesterol"), {
			type: 'line',
			data: {
				labels: response.date,
				datasets: [
					{
						label: "Kolesterol",
						data: response.cholesterol,
						borderColor: "#008080"
					}
				]
			},
			options: {
      annotation: {
        annotations: [
          {
            drawTime: "afterDatasetsDraw",
            type: "line",
            mode: "horizontal",
            scaleID: "y-axis-0",
            value: 240,
            borderWidth: 3,
            borderColor: "red",
            label: {
              content: "",
              enabled: true,
              position: "top"
            }
          }
        ]
      },
				scales: {
					yAxes: [{
						ticks: {
							min: 120,
							max: 300,
          maxTicksLimit: 10,
							beginAtZero:false,
          callback: function (value, index, values) {
          	if(value==120){return '120/kurang';}else if(value==300){return '300/lebih';}else{return value;}
          }
						}
					}],
				},
				maintainAspectRatio: false,
				plugins: {
					title: {
						display: true,
						text: ["Kolesterol"],
						align: "center",
						position: "bottom",
						font: {
							size: 18
						}
					}
				}
			}
		})

		new Chart($("#chart-uric-acid"), {
			type: 'line',
			data: {
				labels: response.date,
				datasets: [
					{
						label: "Asam Urat",
						data: response.uricAcid,
						borderColor: "#008080"
					}
				]
			},
			options: {
				annotation: {
        annotations: [
          {
            drawTime: "afterDatasetsDraw",
            type: "line",
            mode: "horizontal",
            scaleID: "y-axis-0",
            value: $("#line-uric-acid").val(),
            borderWidth: 2,
            borderColor: "red",
            label: {
              content: "",
              enabled: true,
              position: "top"
            }
          }
        ]
      },

				scales: {
					yAxes: [{
						ticks: {
							min: 1.5,
							max: 10,
          maxTicksLimit: 10,
							beginAtZero:false,
          callback: function (value, index, values) {
          	if(value==1.5){return '1.5/kurang';}else if(value==10){return '10/lebih';}else{return value;}
          }
						}
					}],
				},
				maintainAspectRatio: false,
				plugins: {
					title: {
						display: true,
						text: ["Asam Urat"],
						align: "center",
						position: "bottom",
						font: {
							size: 18
						}
					}
				}
			}
		})

		new Chart($("#chart-bmi"), {
			type: 'line',
			data: {
				labels: response.date,
				datasets: [
					{
						label: "Body Mass Index",
						data: response.bodyMassIndex,
						borderColor: "#008080"
					}
				]
			},
			options: {
      annotation: {
        annotations: [
          {
            drawTime: "afterDatasetsDraw",
            type: "line",
            mode: "horizontal",
            scaleID: "y-axis-0",
            value: 23,
            borderWidth: 2,
            borderColor: "yellow",
            label: {
              content: "",
              enabled: true,
              position: "top"
            }
          },
          {
            drawTime: "afterDatasetsDraw",
            type: "line",
            mode: "horizontal",
            scaleID: "y-axis-0",
            value: 25,
            borderWidth: 2,
            borderColor: "red",
            label: {
              content: "",
              enabled: true,
              position: "top"
            }
          }
        ]
      },
				scales: {
					yAxes: [{
						ticks: {
							min: 12,
							max: 35,
          maxTicksLimit: 10,
							beginAtZero:false,
          callback: function (value, index, values) {
          	if(value==12){return '12/kurang';}else if(value==35){return '35/lebih';}else{return value;}
          }
						}
					}],
				},
				maintainAspectRatio: false,
				plugins: {
					title: {
						display: true,
						text: ["Body Mass Index"],
						align: "center",
						position: "bottom",
						font: {
							size: 18
						}
					}
				}
			}
		})
	},
	error: function(error) {
		console.log(error);
	}
});

$.ajax({
	type: 'get',
	url: base_url + 'home/getFitnessResults',
	success: function(response) {
		new Chart($("#chart-fitness"), {
			type: 'line',
			data: {
				labels: response.date,
				datasets: [
					{
						label: "Status Kebugaran",
						data: response.no,
						borderColor: "#008080"
					}
				]
			},
			options: {
				scales: {
					yAxes: [{
						ticks: {
							min: 0,
							max: 1,
          maxTicksLimit: 2,
							beginAtZero:false,
          callback: function (value, index, values) {
          	if(value==0){return 'Tidak Bugar';}else{return 'Bugar';}
          }
						}
					}],
				},
				maintainAspectRatio: false,
				plugins: {
					title: {
						display: true,
						text: ["Status Kebugaran","* 0 = Tidak Bugar","* 1 = Bugar"],
						align: "center",
						position: "bottom",
						font: {
							size: 18
						},
					}
				}
			},
		})
	},
	error: function(error) {
		console.log(error);
	}
});

$.ajax({
	type: 'get',
	url: base_url + 'home/getDiagnose',
	success: function(response) {
		new Chart($("#chart-blood-pressure-mobile"), {
			type: 'line',
			data: {
				labels: response.date,
				datasets: [
					{
						label: "Tekanan Darah Atas",
						data: response.sistole,
						borderColor: "#008080"
					},
					{
						label: "Tekanan Darah Bawah",
						data: response.diastole,
						borderColor: "#008080"
					}
				]
			},
			options: {
      annotation: {
        annotations: [
          {
            drawTime: "afterDatasetsDraw",
            type: "line",
            mode: "horizontal",
            scaleID: "y-axis-0",
            value: 90,
            borderWidth: 2,
            borderColor: "red",
            label: {
              content: "",
              enabled: true,
              position: "top"
            }
          },
          {
            drawTime: "afterDatasetsDraw",
            type: "line",
            mode: "horizontal",
            scaleID: "y-axis-0",
            value: 140,
            borderWidth: 2,
            borderColor: "red",
            label: {
              content: "",
              enabled: true,
              position: "top"
            }
          }
        ]
      },
				scales: {
					yAxes: [{
						ticks: {
							min: 50,
							max: 180,
          maxTicksLimit: 10,
							beginAtZero:false,
          callback: function (value, index, values) {
          	if(value==50){return '50/kurang';}else if(value==180){return '180/lebih';}else{return value;}
          }
						}
					}],
				},
				maintainAspectRatio: false,
				plugins: {
					title: {
						display: true,
						text: ["Tekanan Darah"],
						align: "center",
						position: "bottom",
						font: {
							size: 18
						},
					}
				}
			}
		})

		new Chart($("#chart-blood-sugar-mobile"), {
			type: 'line',
			data: {
				labels: response.date,
				datasets: [
					{
						label: "Gula Darah",
						data: response.bloodSugar,
						borderColor: "#008080"
					}
				]
			},
			options: {
      annotation: {
        annotations: [
          {
            drawTime: "afterDatasetsDraw",
            type: "line",
            mode: "horizontal",
            scaleID: "y-axis-0",
            value: 70,
            borderWidth: 3,
            borderColor: "yellow",
            label: {
              content: "",
              enabled: true,
              position: "top"
            }
          },
          {
            drawTime: "afterDatasetsDraw",
            type: "line",
            mode: "horizontal",
            scaleID: "y-axis-0",
            value: 140,
            borderWidth: 3,
            borderColor: "red",
            label: {
              content: "",
              enabled: true,
              position: "top"
            }
          }
        ]
      },
				scales: {
					yAxes: [{
						ticks: {
							min: 55,
							max: 300,
          maxTicksLimit: 10,
							beginAtZero:false,
          callback: function (value, index, values) {
          	if(value==55){return '55/kurang';}else if(value==300){return '300/lebih';}else{return value;}
          }
						}
					}],
				},
				maintainAspectRatio: false,
				plugins: {
					title: {
						display: true,
						text: ["Gula Darah"],
						align: "center",
						position: "bottom",
						font: {
							size: 18
						}
					}
				}
			}
		})

		new Chart($("#chart-cholesterol-mobile"), {
			type: 'line',
			data: {
				labels: response.date,
				datasets: [
					{
						label: "Kolesterol",
						data: response.cholesterol,
						borderColor: "#008080"
					}
				]
			},
			options: {
      annotation: {
        annotations: [
          {
            drawTime: "afterDatasetsDraw",
            type: "line",
            mode: "horizontal",
            scaleID: "y-axis-0",
            value: 240,
            borderWidth: 3,
            borderColor: "red",
            label: {
              content: "",
              enabled: true,
              position: "top"
            }
          }
        ]
      },
				scales: {
					yAxes: [{
						ticks: {
							min: 120,
							max: 300,
          maxTicksLimit: 10,
							beginAtZero:false,
          callback: function (value, index, values) {
          	if(value==120){return '120/kurang';}else if(value==300){return '300/lebih';}else{return value;}
          }
						}
					}],
				},
				maintainAspectRatio: false,
				plugins: {
					title: {
						display: true,
						text: ["Kolesterol"],
						align: "center",
						position: "bottom",
						font: {
							size: 18
						}
					}
				}
			}
		})

		new Chart($("#chart-uric-acid-mobile"), {
			type: 'line',
			data: {
				labels: response.date,
				datasets: [
					{
						label: "Asam Urat",
						data: response.uricAcid,
						borderColor: "#008080"
					}
				]
			},
			options: {
      annotation: {
        annotations: [
          {
            drawTime: "afterDatasetsDraw",
            type: "line",
            mode: "horizontal",
            scaleID: "y-axis-0",
            value: $("#line-uric-acid").val(),
            borderWidth: 2,
            borderColor: "red",
            label: {
              content: "",
              enabled: true,
              position: "top"
            }
          }
        ]
      },

				scales: {
					yAxes: [{
						ticks: {
							min: 1.5,
							max: 10,
          maxTicksLimit: 10,
							beginAtZero:false,
          callback: function (value, index, values) {
          	if(value==1.5){return '1.5/kurang';}else if(value==10){return '10/lebih';}else{return value;}
          }
						}
					}],
				},
				maintainAspectRatio: false,
				plugins: {
					title: {
						display: true,
						text: ["Asam Urat"],
						align: "center",
						position: "bottom",
						font: {
							size: 18
						}
					}
				}
			}
		})

		new Chart($("#chart-bmi-mobile"), {
			type: 'line',
			data: {
				labels: response.date,
				datasets: [
					{
						label: "Body Mass Index",
						data: response.bodyMassIndex,
						borderColor: "#008080"
					}
				]
			},
			options: {
      annotation: {
        annotations: [
          {
            drawTime: "afterDatasetsDraw",
            type: "line",
            mode: "horizontal",
            scaleID: "y-axis-0",
            value: 23,
            borderWidth: 2,
            borderColor: "yellow",
            label: {
              content: "",
              enabled: true,
              position: "top"
            }
          },
          {
            drawTime: "afterDatasetsDraw",
            type: "line",
            mode: "horizontal",
            scaleID: "y-axis-0",
            value: 25,
            borderWidth: 2,
            borderColor: "red",
            label: {
              content: "",
              enabled: true,
              position: "top"
            }
          }
        ]
      },
				scales: {
					yAxes: [{
						ticks: {
							min: 12,
							max: 35,
          maxTicksLimit: 10,
							beginAtZero:false,
          callback: function (value, index, values) {
          	if(value==12){return '12/kurang';}else if(value==35){return '35/lebih';}else{return value;}
          }
						}
					}],
				},
				maintainAspectRatio: false,
				plugins: {
					title: {
						display: true,
						text: ["Body Mass Index"],
						align: "center",
						position: "bottom",
						font: {
							size: 18
						}
					}
				}
			}
		})
	},
	error: function(error) {
		console.log(error);
	}
});

$.ajax({
	type: 'get',
	url: base_url + 'home/getFitness',
	success: function(response) {
		new Chart($("#chart-fitness-mobile"), {
			type: 'line',
			data: {
				labels: response.date,
				datasets: [
					{
						label: "Status Kebugaran",
						data: response.no,
						borderColor: "#FFDAB9"
					}
				]
			},
			options: {
				scales: {
					yAxes: [{
						ticks: {
							min: 0,
							max: 1,
          maxTicksLimit: 2,
							beginAtZero:false,
          callback: function (value, index, values) {
          	if(value==0){return 'Tidak Bugar';}else{return 'Bugar';}
          }
						}
					}],
				},
				maintainAspectRatio: false,
				plugins: {
					title: {
						display: true,
						text: ["Status Kebugaran","* 0 = Tidak Bugar","* 1 = Bugar"],
						align: "center",
						position: "bottom",
						font: {
							size: 18
						},
					}
				}
			}
		})
	},
	error: function(error) {
		console.log(error);
	}
});

let Buttons = document.querySelectorAll(".owl-slide button");

for (let button of Buttons) {
  button.addEventListener('click', (e) => {
    const et = e.target;
    const active = document.querySelector(".active");

    if (active) {
      active.classList.remove("active");
    }
    et.classList.add("active");
    
    let allContent = document.querySelectorAll('.content');

    for (let content of allContent) {
      if(content.getAttribute('data-number') === button.getAttribute('data-number')) {
        content.style.display = "block";
       }
      else {
        content.style.display = "none";
       }
     }
  });
}
