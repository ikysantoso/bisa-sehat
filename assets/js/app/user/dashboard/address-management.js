$(document).ready(function(){
    
    includeJs( base_url+'assets/js/app/components/addressForm.js' );
    load_fast_select();

    // save address
    var form_element = $('#form-address');
    form_element.validate({
        rules: {},
        highlight: function(element) {
            $(element).closest('.form-group').removeClass('success').addClass('error');
        },
        success: function(element) {
            $(element).closest('.form-group').removeClass('error');
            element.remove();
        },
        submitHandler: function(form) {
            $('body').addClass('loading');

            var url_submit = (form_element.find('input[name="id"]').val()) ? base_url+"member/setting/update_address" : base_url+"member/setting/save_address";

            $.ajax({
                type: 'post',
                url: url_submit,
                data: form_element.serialize(),
                success: function(response) {
                    $('body').removeClass('loading');
                    if ( response ) {
                        $("#modal-address").modal('hide');
                        $('body').addClass('loading');
                        location.reload();
                    } else {
                        Swal.fire({
                            title: 'Error',
                            text: 'Kesalahan pada server',
                            icon: 'warning',
                        });
                    }
                },
                error: function(){
                    $('body').removeClass('loading');
                }
            });
        }
    });

    $('#modal-address').on('hidden.bs.modal', function (e) {
        form_element.trigger('reset');
        $("input[name='id']").val('');
        $(".fstResultItem").first().trigger('click');
        load_fast_select( true );
    })

    $("#address-list").on('click', '.address-item', function(){
        $("body").addClass('loading');
        var id = $(this).attr('data-id');
        $.ajax({
            type: 'post',
            url: base_url+"member/setting/address_detail",
            data: {
                id: id,
            },
            success: function(response) {
                $("body").removeClass('loading');
                if ( response ) {
                    $("input[name='id']").val(response.id);
                    $("input[name='name']").val(response.name);
                    $("input[name='receiver_name']").val(response.receiver_name);
                    $("input[name='phone']").val(response.phone);
                    $("input[name='zipcode']").val(response.zipcode);
                    $("input[name='is_primary_address'][value='"+response.is_primary_address+"']").prop('checked', true);
                    $("textarea[name='address']").val(response.address);

                    load_fast_select(
                        true,
                        {
                            'province': response.province,
                            'district': response.district,
                            'subdistrict': response.subdistrict,
                            'village': response.village,
                        }
                    );
                    console.log(response.province);
                    console.log(response.district);
                    console.log(response.subdistrict);
                    console.log(response.village);
                    $("#modal-address").modal('show');
                }
            }
        });
    });

    $("#address-list").on('click', '.btn-delete', function(){
        var id = $(this).attr('data-id');

        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                $("body").addClass('loading');

                $.ajax({
                    type: 'post',
                    url: base_url+'member/setting/address_delete',
                    data: {
                        id: id
                    },
                    success: function(response) {
                        $('body').removeClass('loading');
                        if ( response ) {
                            $("body").addClass('loading');
                            location.reload();
                        }
                    }
                });
            }
        });
    });
});
