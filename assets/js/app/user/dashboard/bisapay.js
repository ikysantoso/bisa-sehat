$(document).ready(function(){
	$(".bisapay-activate-btn").click(function(){
		Swal.fire({
		  	title: 'Aktifkan Bisapay Sekarang?',
		  	text: "",
		  	type: 'question',
		  	showCancelButton: true,
		  	// confirmButtonColor: '#3085d6',
		  	// cancelButtonColor: '#d33',
		  	confirmButtonText: 'Ya, aktifkan!'
		}).then((result) => {
		  	if (result.value) {
		  		$("body").addClass('loading');
		    	location.href=base_url+'member/bisapay/activate';
		  	}
		})
	});

	form_element = $("#form-bisapay");
	form_element.validate({
		rules: {},
        highlight: function(element) {
        	$(element).closest('.form-group').removeClass('success').addClass('error');
        },
		success: function(element) {
            $(element).closest('.form-group').removeClass('error');
        	element.remove();
        },
		submitHandler: function(form) {
			$('body').addClass('loading');
			$.ajax({
                type: 'post',
                url: base_url+'member/bisapay/topup',
				data: form_element.serialize(),
				success: function(response) {
                    var convertedJSONResponse = JSON.stringify(response);
                    var parsedJSONResponse = JSON.parse(convertedJSONResponse);

                    var bankAccount = parsedJSONResponse.response.bankAccount;

                    var nominalLabel = parsedJSONResponse.response.nominal;
                    var paymentAmount = $('input[name=nominal]').val();
                    var manualPaymentHeadline = "<h5 id='manualPaymentHeadline' style='margin-top: 40px; font-weight: bold;'>Pilih Metode Pembayaran</h5>";
                    var selectManualMethod = $('<div style="margin-left: 5px; margin-top: 20px;"></div>');
                    $.each(bankAccount, function(idx, val) {
                        selectManualMethod.append(`<h5 style="margin-top: 5px;"><input type='radio' name='bankAcc' id='bankAcc' value=${val.id} style='margin-right: 10px;'>${val.name} <br /> ${val.account_number} / ${val.account_name}</h5>`);
                    })

                    $('#payment-method-modal-body').append(`<h3 style="font-weight: bold;"> SETOR ${nominalLabel}</h3>`);
                    $('#payment-method-modal-body').append(manualPaymentHeadline);
                    $('#payment-method-modal-body').append(selectManualMethod);
                    $('#payNowBtn').click(function() {
                        var bankAccId = document.querySelector('input[name="bankAcc"]:checked').value;

                        doRequestTransaction(paymentAmount, null, bankAccId);
                    });

					$('body').removeClass('loading');
					$("#modal-bisapay").modal('hide');
                    $('#payment-method-modal').modal('show');
					$('#table-bisapay-logs').DataTable().ajax.reload();
                }
            });
        }
    });

    $('#table-bisapay-logs').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": {
        	'url': base_url+'member/bisapay/logs',
        	'type': 'POST'
        },
        "columnDefs": [
            { "targets": [ 5,6 ], "visible": false },
            {
                "targets": [4],
                "className": "text-right align-middle",
                "render": function ( data, type, row, meta ) {
                    if (data) {
                    	var kode_unik = parseInt((row[5]) ? row[5] : 0);
                        return 'Rp '+(parseInt(data) + kode_unik).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
                    } else {
                        return '-';
                    }
                },
            },
            {
                "targets": [3],
                "className": "text-center align-middle",
            },
            {
                "targets": [2],
                "render": function ( data, type, row, meta ) {
                    if ( data == 'in (topup)' ) {
                        return '<div class="d-flex justify-content-between"><span class="text-success"><i class="fas fa-arrow-circle-down"></i> in</span> <span>(topup #<a href="'+base_url+'/member/bisapay/topup_detail/'+row[6]+'"><b>'+row[6]+'</b></a>)</span></div>';
                    } else {

                        if ( data.includes("out (transaction #") ) {
                            var transaction_id = data.split('#').pop().split(')')[0];
                            return '<div class="d-flex justify-content-between"><span class="text-danger"><i class="fas fa-arrow-circle-up"></i> out</span> <span>(transaction <a href="'+base_url+'member/order/detail/'+transaction_id+'"><b>#'+transaction_id+'</b></a>)</span></div>';
                        }

                        else if ( data.includes("in (refund preorder #") ) {
                            var transaction_id = data.split('#').pop().split(')')[0];
                            return '<div class="d-flex justify-content-between"><span class="text-success"><i class="fas fa-arrow-circle-down"></i> in</span> <span>(refund preorder <a href="'+base_url+'member/order/detail/'+transaction_id+'"><b>#'+transaction_id+'</b></a>)</span></div>';
                        }

                        else if ( data.includes("lock (transaction #") ) {
                            var transaction_id = data.split('#').pop().split(')')[0];
                            return '<div class="d-flex justify-content-between"><span class="text-dark"><i class="fas fa-lock"></i> lock</span> <span>(transaction <a href="'+base_url+'member/order/detail/'+transaction_id+'"><b>#'+transaction_id+'</b></a>)</span></div>';
                        }

                        else if ( data.includes("out (withdraw)") ) {
                            return '<div class="d-flex justify-content-between"><span class="text-danger"><i class="fas fa-arrow-circle-up"></i> out</span> <span>(withdraw #<a class="withdraw-info" href="'+base_url+'/member/bisapay/withdraw_detail/'+row[6]+'"><b>'+row[6]+'</b></a>)</span></div>';
                        }

                        else {
                            return data;
                        }
                    }
                },
            },
        ],
        "createdRow": function( row, data, dataIndex ) {
            // $(row).find('.btn-delete').attr( 'data-id', data[3] );
            // $(row).find('.btn-edit').attr( 'data-id', data[3] );
        },
        "language": {
            "paginate": {
                "previous": "<i class='fas fa-angle-left'></i>",
                "next": "<i class='fas fa-angle-right'></i>"
            }
        }
    });

    getDoneTransactionLogs();

    $('#done-transaction').click(function() {
        $('#inprogress-transaction').removeClass('active');
        $(this).addClass('active');
        getDoneTransactionLogs();
    });

    $('#inprogress-transaction').click(function() {
        $('#done-transaction').removeClass('active');
        $(this).addClass('active');
        getInProgressTransactionLogs();
    });

    $('#modal-bisapay').on('hidden.bs.modal', function (e) {
    	form_element.trigger('reset');
	})

    $('#payment-method-modal').on('hidden.bs.modal', function (e) {
    	$('#payment-method-modal-body').html('');
	})

    form_element_bisapay_withdaw = $("#form-bisapay-withdraw");
    form_element_bisapay_withdaw.validate({
        rules: {},
        messages: {
            nominal: {
                max: jQuery.validator.format("Nominal tidak boleh melebihi saldo: {0}."),
                min: jQuery.validator.format("Nominal tidak boleh kurang dari: {0}."),
            }
        },
        highlight: function(element) {
            $(element).closest('.form-group').removeClass('success').addClass('error');
        },
        success: function(element) {
            element.remove();
        },
        submitHandler: function(form) {
            $('body').addClass('loading');
            $.ajax({
                type: 'post',
                url: base_url+'member/bisapay/withdraw',
                data: form_element_bisapay_withdaw.serialize(),
                success: function(response) {

                    if ( response.status == 'success' ) {
                        location.reload();
                    } else {
                        $('body').removeClass('loading');
                        Swal.fire({
                            title: response.title,
                            text: response.message,
                            type: response.status
                        });
                    }
                }
            });
        }
    });

    $('#modal-bisapay-withdraw').on('show.bs.modal', function (e) {
        if ( $("#form-bisapay-withdraw").find("select[name='destination']").attr('data-bank-total') == 0 ) {
            Swal.fire('Belum ada data bank', 'Silahkan tambahkan akun bank Anda terlebih dahulu', 'warning').then(()=>{
                location.href = base_url+'member/setting';
            });
            return false;
        }
    })

    $('#withdraw-btn').click(function(){
        var saldo = $("#form-bisapay-withdraw").find('input[name="nominal"]').attr('max');
        if ( saldo == 0 ) {
            Swal.fire({
                title: 'Saldo 0',
                text: 'Saldo masih 0, setor dulu sebelum tarik dana',
                type: 'warning'
            });
        } else {
            $("#modal-bisapay-withdraw").modal('show');
        }
    });

    $(".payment-confirmation-btn").click(function() {
        $('input[name="confirmation_status"]').attr('value', 'confirm');
        $("#modal-payment-confirmation").modal('show');
    });

    $(".reconfirm-payment-btn").click(function() {
        $('input[name="confirmation_status"]').attr('value', 'reconfirm');
        $("#modal-payment-confirmation").modal('show');
    });

    form_element_payment = $("#form-payment-confirmation");
    form_element_payment.validate({
        rules: {},
        messages: {
        },
        highlight: function(element) {
            $(element).closest('.form-group').removeClass('success').addClass('error');
        },
        success: function(element) {
            element.remove();
        },
        submitHandler: function(form) {
            var log_id = $('#topup_log_id').val();
            var nominal = $('#topup_nominal').val();
            var image = $('#payment-confirmation-file').prop('files')[0];
            console.log(image);
            var exist_image_name = $('#exist_image_name').val();

            var form_data = new FormData();
            form_data.append('log_id', log_id);
            form_data.append('nominal', nominal);
            form_data.append('file', image);
            form_data.append('exist_image_name', exist_image_name);

            if (image === undefined) {
                Swal.fire({
                    title: 'Pengiriman tidak di proses',
                    text: 'Anda belum mengirim bukti pembayaran',
                    type: 'error'
                })
            } else {
                $('body').addClass('loading');

                $.ajax({
                    type: 'post',
                    url: base_url+'member/bisapay/payment_confirmation',
                    data: form_data,
                    dataType: 'json',
                    cache: false,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        $('body').removeClass('loading');

                        if (response.code === "230") {
                            $("#modal-payment-confirmation").modal('hide');

                            Swal.fire({
                                title: response.title,
                                text: response.message,
                                type: response.status
                            })
                            .then(() => {
                                location.reload();
                            })
                        } else {
                            Swal.fire({
                                title: response.title,
                                text: response.message,
                                type: response.status,
                                confirmButtonText: 'Kirim ulang',
                                showCancelButton: true
                            })
                            .then(() => {
                                console.log('kirim ulang');
                            })
                        }
                    }
                });
            }
        }
    });
});

function formatDate(datetime) {
    var newDateTime = new Date(datetime);
    var monthNames=["Januari", "Februari", "Maret", "April", "Mei", "Juni",
            "Juli", "Agustus", "September", "Oktober", "November", "Desember"];

    var date = newDateTime.getDate().toString();
    var year = newDateTime.getFullYear().toString();

    var hours = newDateTime.getHours() < 10 ? "0" + newDateTime.getHours() : newDateTime.getHours();
    var minutes = newDateTime.getMinutes() < 10 ? "0" + newDateTime.getMinutes() : newDateTime.getMinutes();
    var seconds = newDateTime.getSeconds() < 10 ? "0" + newDateTime.getSeconds() : newDateTime.getSeconds();
    var time = hours + ":" + minutes + ":" + seconds;

    var result = date + " " + monthNames[newDateTime.getMonth()] + " " + year + ", " + time;

    return result;
}

function getDoneTransactionLogs() {
    $.ajax({
        type: "POST",
        url: base_url+'member/bisapay/transaction_logs',
        dataType: 'JSON',
        success: function(data) {
            var row = '';
            for (var i = 0; i < Object.keys(data).length; i++) {
                if (data[i].status == "success") {
                    $formattedDate = formatDate(data[i].created_at);

                    $transaction_type = data[i].log_type_description == "topup" ? '<a href="'+base_url+'member/bisapay/topup_detail/'+data[i].id+'" style="font-size: 13pt;">'+ '<b style="color: #008080;">'+ "Setor" + '</b>' + '<br>' + '<p>'+ $formattedDate +'</p>' +'</a>'
                                        : data[i].log_type_description == "withdraw" ? '<a href="'+base_url+'member/bisapay/topup_detail/'+data[i].id+'" style="font-size: 13pt;">'+ '<b style="color: #A86561;">'+ "Tarik" + '</b>' + '<br>' + '<p>'+ $formattedDate +'</p>' +'</a>'
                                        : '<a href="'+base_url+'member/bisapay/topup_detail/'+data[i].id+'" style="font-size: 13pt;">'+ '<b style="color: #A86561;">'+ "Transaksi" + '</b>' + '<br>' + '<p>'+ $formattedDate +'</p>' +'</a>';

                    $nominal = data[i].log_type_description == "topup" ? '<td style="border-top: none !important; color: #00A5A5;">'+ 'Rp ' + data[i].nominal +'</td>': '<td style="border-top: none !important; color: #A86561;">'+ 'Rp ' + data[i].nominal +'</td>';

                    row += '<tr>'+ '<td style="border-top: none !important;">'+ $transaction_type +'</td>'+ $nominal +'</tr>';
                }
            }

            $('#bisapay-logs-table-body').html(row);
        }
    });
}

function getInProgressTransactionLogs() {
    $.ajax({
        type: "POST",
        url: base_url+'member/bisapay/transaction_logs',
        dataType: 'JSON',
        success: function(data) {
            var row = '';
            for (var i = 0; i < Object.keys(data).length; i++) {
                if (data[i].status == "waiting") {
                    $formattedDate = formatDate(data[i].created_at);

                    $transaction_type = data[i].log_type_description == "topup" ? '<a href="'+base_url+'member/bisapay/topup_detail/'+data[i].id+'" style="font-size: 13pt;">'+ '<b style="color: #008080;">'+ "Setor" + '</b>' + '<br>' + '<p>'+ $formattedDate +'</p>' +'</a>'
                                        : data[i].log_type_description == "withdraw" ? '<a href="'+base_url+'member/bisapay/topup_detail/'+data[i].id+'" style="font-size: 13pt;">'+ '<b style="color: #A86561;">'+ "Tarik" + '</b>' + '<br>' + '<p>'+ $formattedDate +'</p>' +'</a>'
                                        : '<a href="'+base_url+'member/bisapay/topup_detail/'+data[i].id+'" style="font-size: 13pt;">'+ '<b style="color: #A86561;">'+ "Transaksi" + '</b>' + '<br>' + '<p>'+ $formattedDate +'</p>' +'</a>';

                    $nominal = data[i].log_type_description == "topup" ? '<td style="border-top: none !important; color: #00A5A5;">'+ 'Rp ' + data[i].nominal +'</td>': '<td style="border-top: none !important; color: #A86561;">'+ 'Rp ' + data[i].nominal +'</td>';

                    row += '<tr>'+ '<td style="border-top: none !important;">'+ $transaction_type +'</td>'+ $nominal +'</tr>';
                }
            }

            $('#bisapay-logs-table-body').html(row);
        }
    })
}

function doRequestTransaction(paymentAmount, paymentMethod, bankId) {
    // switch(paymentOption) {
        // case "onlinePayment":
        //     var requestBody = {
        //         "paymentAmount": paymentAmount,
        //         "paymentMethod": paymentMethod
        //     }

        //     $.ajax({
        //         type: "POST",
        //         url: base_url+'member/bisapay/online_transaction',
        //         data: requestBody,
        //         dataType: 'JSON',
        //         success: function(response) {
        //             var parsedResponse = JSON.parse(response);
        //             window.location.href = parsedResponse.paymentUrl;
        //         },
        //         error : function(err) {
        //             console.log(err);
        //         }
        //     })
        //     break;

        // case "manualPayment":
            var requestBodyManual = {
                "paymentAmount": paymentAmount,
                "bankId": bankId
            }

            $.ajax({
                type: "POST",
                url: base_url+'member/bisapay/manual_transaction',
                data: requestBodyManual,
                dataType: 'JSON',
                success: function(response) {
                    location.href = base_url+'member/bisapay/topup_detail/'+response.bisapay_log_id;
                },
                error : function(err) {
                    console.log(err);
                }
            });

            // break;
    // }
}