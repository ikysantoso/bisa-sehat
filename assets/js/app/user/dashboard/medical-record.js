$(document).ready(function(){
	var form_element = $("#form-medical-record");
	form_element.validate({
		rules: {},
        highlight: function(element) {
        	$(element).closest('.form-group').removeClass('success').addClass('error');
        },
       	success: function(element) {
        	$(element).closest('.form-group').removeClass('error');
        	element.remove();
        },
		submitHandler: function(form) {
			$('body').addClass('loading');
			var url_submit = (form_element.find('input[name="id"]').val()) ? base_url+'member/medical_record/update' : base_url+'member/medical_record/save';
			$.ajax({
                type: 'post',
                url: url_submit,
				data: form_element.serialize(),
				success: function(response) {
					if ( response ) {
						location.href = base_url+'member/medical_record';
					}
                }
            });
        }
	});

	$("#table-medical-record").on('click', '.btn-delete', function(){
		var id = $(this).attr('data-id');

        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {

                location.href=base_url+'member/medical_record/delete/'+id;
            }
        });
	});

	$("#table-medical-record").on('click', '.btn-edit', function(){
		var id = $(this).attr('data-id');
		$.ajax({
			type: 'post',
            url: base_url+'member/medical_record/detail',
			data: {
				id: id
			},
			success: function(response) {
				if ( response ) {

					form_element.find('input[name="id"]').val(response.id);
					form_element.find('.form-control[name="height"]').val(response.height);
					form_element.find('.form-control[name="weight"]').val(response.weight);

					form_element.find('.form-control[name="sistole"]').val(response.sistole).attr('readonly','readonly');
					form_element.find('.form-control[name="diastole"]').val(response.diastole).attr('readonly','readonly');
					form_element.find('.form-control[name="blood_sugar_level"]').val(response.blood_sugar_level).attr('readonly','readonly');
					form_element.find('.form-control[name="cholesterol_level"]').val(response.cholesterol_level).attr('readonly','readonly');
					form_element.find('.form-control[name="uric_acid_level"]').val(response.uric_acid_level).attr('readonly','readonly');
					$("#modal-medical-record").modal('show');
				}
        	}
		});
	});

	$('#modal-medical-record').on('hidden.bs.modal', function (e) {
		form_element.find('input[name="id"]').val('');
		form_element.find('.form-control[name="height"]').val('');
		form_element.find('.form-control[name="weight"]').val('');

		form_element.find('.form-control[name="sistole"]').val('').removeAttr('readonly');
		form_element.find('.form-control[name="diastole"]').val('').removeAttr('readonly');
		form_element.find('.form-control[name="blood_sugar_level"]').val('').removeAttr('readonly');
		form_element.find('.form-control[name="cholesterol_level"]').val('').removeAttr('readonly');
		form_element.find('.form-control[name="uric_acid_level"]').val('').removeAttr('readonly');
	});
});