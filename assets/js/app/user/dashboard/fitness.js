$(document).ready(function(){
	var form_element = $("#form-fitness");
	form_element.validate({
		rules: {},
        highlight: function(element) {
        	$(element).closest('.form-group').removeClass('success').addClass('error');
        },
       	success: function(element) {
        	$(element).closest('.form-group').removeClass('error');
        	element.remove();
        },
		submitHandler: function(form) {
			$('body').addClass('loading');
            var url_submit = (form_element.find('input[name="id"]').val()) ? base_url+'member/fitness/update' : base_url+'member/fitness/save';
			$.ajax({
                type: 'post',
                url: url_submit,
				data: form_element.serialize(),
				success: function(response) {
					if ( response ) {
						// location.reload()
                        location.href = base_url+'member/fitness';

					}
                }
            });
        }
	});

    var height_field = form_element.find("input[name='height']");
    var weight_field = form_element.find("input[name='weight']");
    var mileage_field = form_element.find("input[name='mileage']");
    
    height_field.keyup(function(){
        mileage_field_callback();
    });

    weight_field.keyup(function(){
        mileage_field_callback();
    });

    function mileage_field_callback(){
        if ( weight_field.val() && height_field.val() ) {
            mileage_field.removeAttr('disabled');   
        } else {
            mileage_field.attr('disabled','disabled');
        }
    }

	$("#table-fitness").on('click', '.btn-delete', function(){
		var id = $(this).attr('data-id');

        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                location.href=base_url+'member/fitness/delete/'+id;
            }
        });
	});

    $("#table-fitness").on('click', '.btn-edit', function(){
        var id = $(this).attr('data-id');
        $.ajax({
            type: 'post',
            url: base_url+'member/fitness/detail',
            data: {
                id: id
            },
            success: function(response) {
                if ( response ) {
                    console.log(response);

                    form_element.find('input[name="id"]').val(response.id);
                    form_element.find('.form-control[name="height"]').val(response.height);
                    form_element.find('.form-control[name="weight"]').val(response.weight);

                    form_element.find('.form-control[name="mileage"]').val(response.mileage).attr('readonly','readonly');
                    $("#modal-fitness").modal('show');
                }
            }
        });
    });

    $('#modal-fitness').on('hidden.bs.modal', function (e) {
        form_element.find('input[name="id"]').val('');
        form_element.find('.form-control[name="height"]').val('');
        form_element.find('.form-control[name="weight"]').val('');

        form_element.find('.form-control[name="mileage"]').val('').removeAttr('readonly');
    });
});