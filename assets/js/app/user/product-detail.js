$(document).ready(function(){
	var form_element = $("#form-add-to-cart");

	var submit_type = '';

	$(".submit-btn").click(function(){
		submit_type = $(this).attr('data-button')
	});

	form_element.validate({
		rules: {},
        highlight: function(element) {
        	$(element).closest('.form-group').removeClass('success').addClass('error');
        },
       	success: function(element) {
       		$(element).closest('.form-group').removeClass('error');
        	element.remove();
        },
		submitHandler: function(form) {


			if ( submit_type == 'cart' ) {
				$('body').addClass('loading');

				$.ajax({
	                type: 'post',
	                url: base_url+'product/add_to_cart',
					data: form_element.serialize(),
					success: function(response) {
						$('body').removeClass('loading');
						if ( response.type == 'success' ) {
							$('body').addClass('loading');
							location.href=base_url+'cart';
						} else {
							Swal.fire({
					            title: response.title,
					            text: response.message,
					            type: response.type,
					        }).then((result) => {
								$('body').addClass('loading');
								if ( response.redirect != '' ) {
									location.href =  response.redirect;
								} else {
						            location.reload();
								}
							});
						}
	                }
	            });
			} else {
				Swal.fire({
					type: 'question',
				  	title: 'Quantity',
				  	input: 'number',
					  inputAttributes: {
						min: 1,
						max: 100000
					},
				  	confirmButtonText: '<i class="fas fa-shopping-bag"></i> Beli Sekarang',
				  	showLoaderOnConfirm: true,
				  	confirmButtonColor: '#f33f3f',
				  	preConfirm: (quantity) => {
				  		return $.ajax({
			                type: 'post',
			                url: base_url+'product/add_to_cart',
							data: form_element.serialize()+'&quantity='+quantity,
							success: function(response) {
								$('body').removeClass('loading');
								if ( response.type == 'success' ) {
									return response.data.id;
								} else {
									Swal.fire({
							            title: response.title,
							            text: response.message,
							            type: response.type,
							        }).then((result) => {
										$('body').addClass('loading');
										if ( response.redirect != '' ) {
											location.href =  response.redirect;
										} else {
								            location.reload();
										}
									});
								}
			                }
			            });

				  	},
				  	allowOutsideClick: () => !Swal.isLoading()
				}).then((result) => {
				  	if (result.value) {
					    var redirect = base_url+'checkout';
					    $.redirectPost(redirect, {cart_id: result.value.data.id});
				  	}
				})
			}

        }
	});

	$("select[name='warehouse_id']").change(function(){

		var stock = $(this).find('option:selected').attr('data-stock');

		if ( stock == '0' ) {
			$(".buy-now-btn span").text('Preorder Sekarang');
		} else {
			$(".buy-now-btn span").text('Beli Sekarang');
		}

	});
});