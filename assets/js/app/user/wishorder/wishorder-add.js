$(document).ready(function(){
	var form_element = $("#form-wishorder");
	form_element.validate({
		rules: {},
        highlight: function(element) {
        	$(element).closest('.form-group').removeClass('success').addClass('error');
        },
		success: function(element) {
			$(element).closest('.form-group').removeClass('error');
        	element.remove();
        },
		submitHandler: function(form) {

			$('body').addClass('loading');

			$.ajax({
                type: 'post',
                url: base_url+'member/order/wishorder_add',
				data: form_element.serialize(),
				success: function(response) {
					$('body').removeClass('loading');
					if ( response ) {
						Swal.fire({
                            title: 'Success',
                            text: 'Wish order berhasil dibuat',
                        	type: 'success',
						}).then((result) => {
							$('body').addClass('loading');
							location.href = base_url+'member/order/wishorder';			
						});
					}
                },
            });
        }
    });
});