$(document).ready(function(){
    var tableData = $("#table-wishorder");
    tableData.DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": {
        	'url': base_url+'member/order/wishorder',
        	'type': 'POST',
        },
        "drawCallback": function(settings) {
           if (settings.json.data.length == 0 && !settings.oPreviousSearch.sSearch) {
                $("#table-wishorder_wrapper").hide();
                $("#table-wishorder_wrapper").after('<div id="alert-empty-wishorder" class="alert alert-info" style="margin-bottom:100px;">Wishorder masih kosong</div>')
           }
        },
        "columnDefs": [ 
            { "targets": [ 7 ], "visible": false },
            { "targets": [ 2,5 ], "className": "align-middle" },
            { "targets": [ 0,1,6 ], "className": "text-center align-middle" },
            { "targets": [ 4 ], "className": "text-right align-middle" },
            {
                "targets": [3],
                "className": "text-right align-middle",
                "render": function ( data, type, row, meta ) {
                    if (data) {
                        return 'Rp '+data.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
                    } else {
                        return '-';
                    }
                },
            },
            {
                "targets": [6],
                "render": function ( data, type, row, meta ) {
                    if (data) {
                        return '<a href="'+base_url+'/product/detail/'+data+'" class="btn filled-button">Beli Sekarang</a>';
                    } else {
                        return 'Pengajuan';
                    }
                },
            },
        ],
        "createdRow": function( row, data, dataIndex ) {
            $(row).find('.btn-delete').attr( 'data-id', data[7] );
            // $(row).find('.btn-edit').attr( 'data-id', data[3] );
        },
        "language": {
            "paginate": {
                "previous": "<i class='fas fa-angle-left'></i>",
                "next": "<i class='fas fa-angle-right'></i>"
            }
        }
    });

    tableData.on('click','.btn-delete',function(){
        var id = $(this).attr('data-id');

        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {

                $.ajax({
                    type: 'post',
                    url: base_url+'member/order/wishorder_delete',
                    data: {
                        id: id
                    },
                    success: function(response) {
                        $('body').removeClass('loading');
                        if ( response ) {
                            Swal.fire(
                                'Deleted!',
                                'Your product has been deleted.',
                                'success'
                            );
                            tableData.DataTable().ajax.reload();
                        }
                    }
                });
            }
        });
    });

});