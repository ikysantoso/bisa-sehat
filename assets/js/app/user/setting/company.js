$(document).ready(function(){
    $('#nik_null').on('click', function () {
                        Swal.fire({
                            title: 'Error',
                            text: 'Silahkan isi NIK terlebih dulu.',
                            icon: 'warning',
                        });
                    });
    $('#confirmation-request-company-btn').on('click', function () {
        var data = {
            companyId: document.getElementById('company').value,
            nik : document.getElementById('nik').value,
            fullname: document.getElementById('fullname').value,
            birthdate: document.getElementById('birthdate').value,
            division: document.getElementById('division').value
        }

        $.ajax({
            type: 'POST',
            url: 'https://api.bisa-inco.com/api/v1/employee/verification/request',
            data: data,
            success: function(response) {
                $('body').removeClass('loading');

                location.reload();
            },
            error: function(err){
                $('body').removeClass('loading');

                Swal.fire({
                    title: 'Error',
                    text: 'Terjadi Kesalahan, periksa kembali data anda.',
                    type: 'warning',
                });
            }
        });
    });
    $('#delete-request-company-btn').on('click', function () {
        var data = {
            companyId: document.getElementById('already_company').value,
            nik : document.getElementById('nik').value,
            fullname: document.getElementById('fullname').value,
            birthdate: document.getElementById('birthdate').value,
            division: document.getElementById('already_division').value
        }
        $.ajax({
            type: 'POST',
            url: 'https://api.bisa-inco.com/api/v1/employee/delete/request',
            data: data,
            success: function(response) {
                $('body').removeClass('loading');

                location.reload();
            },
            error: function(err){
                $('body').removeClass('loading');

                Swal.fire({
                    title: 'Error',
                    text: 'Kesalahan pada server',
                    type: 'warning',
                });
            }
        });
    });
});