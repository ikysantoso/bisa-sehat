$(document).ready(function(){
	var form_element = $('#form-bank');
    form_element.validate({
        rules: {},
        highlight: function(element) {
            $(element).closest('.form-group').removeClass('success').addClass('error');
        },
        success: function(element) {
            $(element).closest('.form-group').removeClass('error');
            element.remove();
        },
        submitHandler: function(form) {
            $('body').addClass('loading');

            $.ajax({
                type: 'post',
                url: base_url+'member/setting/savebank',
                data: form_element.serialize(),
                success: function(response) {
                    $('body').removeClass('loading');
                    if ( response ) {
                        $("#modal-bank").modal('hide');
                    	$('body').addClass('loading');
                        location.reload();
                    } else {
                        Swal.fire({
                            title: 'Error',
                            text: 'Kesalahan pada server',
                            icon: 'warning',
                        });
                    }
                },
                error: function(){
                	$('body').removeClass('loading');
                    Swal.fire({
                        title: 'Error',
                        text: 'Kesalahan pada server',
                        type: 'warning',
                    });
                }
            });
        }
    });

    $("#bank-accounts").on('click', '.btn-delete', function(){
        
        var bank_id = $(this).attr('data-id');
        
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {

                $('body').addClass('loading');

                $.ajax({
                    type: 'post',
                    url: base_url+'member/setting/deletebank',
                    data: {
                        id: bank_id
                    },
                    success: function(response) {
                        if ( response ) {
                            location.reload();
                        } else {
                            $('body').removeClass('loading');
                            Swal.fire({
                                title: 'Error',
                                text: 'Kesalahan pada server',
                                icon: 'warning',
                            });
                        }
                    },
                    error: function(xhr){
                        $('body').removeClass('loading');
                        Swal.fire({
                            title: 'Error'+xhr.status,
                            text: xhr.responseJSON.message,
                            type: 'warning',
                        });
                    }
                });
            }
        })
    })
});