$(document).ready(function() {
    // save or update
    var form_element = $('#form-contact');
    form_element.validate({
        rules: {},
        highlight: function(element) {
            $(element).closest('.form-group').removeClass('success').addClass('error');
        },
        success: function(element) {
            $(element).closest('.form-group').removeClass('error');
            element.remove();
        },
        submitHandler: function(form) {
            $('body').addClass('loading');

            $.ajax({
                type: 'post',
                url: base_url+'contact_us/submit',
                data: form_element.serialize(),
                success: function(response) {
                    $('body').removeClass('loading');
                    if ( response.status == 'success' ) {
                        Swal.fire('Berhasil','Email berhasil dikirim', 'success');
                    } else {
                        Swal.fire('Gagal','Email gagal dikirim: '+response.message, 'warning');
                    }
                }
            });
        }
    });
});