<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		if (!$this->ion_auth->logged_in()) {
			redirect(base_url('auth/login'), 'refresh');
		} else if ($this->ion_auth->is_admin()) {
			redirect(base_url('admin/dashboard'), 'refresh');
		}
	}

	public function index($_warehouse_id = null)
	{
		$this->load->model('CartModel');

		$warehouse_list = CartModel::where('user_id', $this->ion_auth->user()->row()->id)->select('warehouse_id')->distinct()->get();

		$warehouse_id = ($_warehouse_id) ? $_warehouse_id : (($warehouse_list->count() > 0) ? $warehouse_list[0]->warehouse_id : null);

		$carts = CartModel::where(['user_id' => $this->ion_auth->user()->row()->id, 'type' => 'cart'])->orderBy('warehouse_id')->get();

		$data = [
			'page'				=> 'cart',
			'isBackDetail'      => 'true',
			'carts'				=> $carts,
			'warehouses'		=> $warehouse_list,
			'current_warehouse'	=> $warehouse_id,
			'scripts'			=> [
				base_url('node_modules/sweetalert2/dist/sweetalert2.all.min.js'),
				base_url('assets/js/app/user/cart.js'),
			]
		];

		$this->load->view('template-part/header', $data);
		$this->load->view('template-part/navbar', $data);
		$this->load->view('app/frontpage/cart', $data);
		$this->load->view('template-part/footer', $data);
	}

	public function delete_cart_item()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('cart_id', 'cart_id', 'required');

		if ($this->form_validation->run() == TRUE) {
			$this->load->model('CartModel');

			$result = CartModel::where([
				'id'		=> $this->input->post('cart_id'),
				'user_id'	=> $this->ion_auth->user()->row()->id
			])->forceDelete();

			json_generator( $result );
		}
	}

	public function update_cart_item()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('cart_id', 'cart_id', 'required');
		$this->form_validation->set_rules('quantity', 'quantity', 'required');

		if ($this->form_validation->run() == TRUE) {
			$this->load->model('CartModel');

			$result = CartModel::where([
				'id'		=> $this->input->post('cart_id'),
				'user_id'	=> $this->ion_auth->user()->row()->id
			])->update([
				'quantity'	=> $this->input->post('quantity')
			]);

			json_generator(($result) ? $this->input->post('quantity') : null);
		}
	}

	public function pre_checkout()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('cart_id', 'cart_id', 'required');

		if ( $this->form_validation->run() == TRUE ) {
			$this->load->model('CartModel');

			$carts = CartModel::whereIn( 'id', explode(',', $this->input->post('cart_id')) )->get();
			$product_type = [];

			foreach ($carts as $key => $cart) {
				if (!in_array($cart->product->is_preorder, $product_type)) {
					$product_type[] = $cart->product->is_preorder;
				}
			}

			$data = [
				'status' => 'success',
			];

			json_generator( $data );
		}
	}
}