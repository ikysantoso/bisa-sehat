<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Preorder extends CI_Controller {
	public function __construct(){
		parent::__construct();		
		$this->load->model( ['TransactionModel','BisapayModel','BisapayLogsModel'] );
	}

	public function index(){
		$transactions = TransactionModel::where(['is_preorder' => 'yes', 'status' => 'waiting'])->get();
		foreach ($transactions as $key => $transaction) {
			$preorder_time = $transaction->transaction_items[0]->product->preorder_time;

			$due_date = date('Y-m-d H:i:s', strtotime('+'.$preorder_time.' day', strtotime($transaction->created_at)));
			if (new DateTime() > new DateTime( $due_date )) {

				$bisapay = BisapayModel::where( 'user_id', $transaction->user_id )->first();

				$nominal = $transaction->total() + $transaction->shipping_fee + $transaction->payment_unique_code - ((isset($transaction->voucher->nominal)) ? $transaction->voucher->nominal : 0);

				$bisapay_log = BisapayLogsModel::create([
					'bisapay_id'			=> $bisapay->id,
					'nominal'				=> $nominal,
					'log_type'				=> 'in',
					'log_type_description'	=> 'refund preorder #'.$transaction->id,
					'status'				=> 'success',
				]);

				if ( $bisapay_log ) {
					$transaction->update( ['status' => 'cancelled'] );
				}
			}
		}
	}
}