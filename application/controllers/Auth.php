<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller
{
	public $data = [];

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(['ion_auth', 'form_validation']);
		$this->load->helper(['url', 'language']);

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('auth');
	}

	public function index()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect(base_url('auth/login'), 'refresh');
		}
	}

	public function register()
	{
		if ($this->ion_auth->logged_in()) {
			redirect(base_url('home'), 'refresh');
		} else {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('username', 'username', 'required');
			$this->form_validation->set_rules('email', 'email', 'required');
			$this->form_validation->set_rules('password', 'Password', 'required');
			$this->form_validation->set_rules('password_confirm', 'Confirm Password', 'required|matches[password]');

			if ($this->form_validation->run() == TRUE) {
				$this->load->model('UserModel');

				$username_exist = UserModel::where([
					'username' => $this->input->post('username')
				])->first();

				$email_exist = UserModel::where([
					'email' => $this->input->post('email')
				])->first();

				if ($username_exist) {
					json_generator(
						[
							'text' 	=> 'Username sudah digunakan',
							'field'	=> 'username'
						],
						409
					);
				} else if ($email_exist) {
					json_generator(
						[
							'text' 	=> 'Email sudah digunakan',
							'field'	=> 'email'
						],
						409
					);
				} else {
					if ($this->input->post('password') != $this->input->post('password_confirm')) {
						json_generator(
							[
								'text' 	=> 'Password and confirm password does not match',
								'field'	=> 'password_confirm'
							],
							409
						);
					}

					$identity_column = $this->config->item('identity', 'ion_auth');
					$identity =  ($identity_column === 'email') ? $this->input->post('email') : $this->input->post('username');

					$additional_data = [
						'activation_code'	=> md5( date('Y-m-d H:i:s')."#".rand(1,99999999) )
					];

					if ($identity_column == 'email') {
						$additional_data['username'] = $this->input->post('username');
					}

					if ($this->ion_auth->register($identity, $this->input->post('password'), $this->input->post('email'), $additional_data)) {
						$this->load->library('my_email');

						$data = [
							'username' => $this->input->post('username'),
							'email'		 => $this->input->post('email'),
							'code'		 => $additional_data['activation_code'],
						];

						$this->my_email->registration_email($data);

						$this->session->set_flashdata('message', 'Registrasi berhasil, silakan cek Email dan lakukan Aktivasi akun');

						json_generator(
							[
								'message' => 'Registrasi Sukses',
								'type'		=> 'success',
								'title'		=> 'Success',
							]
						);
					}
				}
			} else {
				$data = [
					'styles'	=> [
						'https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css'
					],
					'scripts'	=> [
						'https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/jquery.validate.min.js',
						'https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.12.7/sweetalert2.all.min.js',
						'https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js',
						base_url('assets/js/app/auth/register.js'),
					]
				];

				$this->load->view('template-part/header', $data);
				$this->load->view('template-part/navbar', $data);
				$this->load->view('app/auth/register', $data);
				$this->load->view('template-part/footer', $data);
			}
		}
	}

	public function finish_registration($code = null)
	{
		if ($code) {
			$this->load->model(['UserModel','CorporateModel']);

			$this->db->from('corporate');
			$this->db->where('status', 'approved');
			$this->db->order_by('name', 'asc');
			$corporateResults = $this->db->get();

			$user = UserModel::where('activation_code', $code)->first();
			if ($user) {
				$data = [
					'user_info'	=> $user,
					'scripts'		=> [
						'https://cdnjs.cloudflare.com/ajax/libs/fastselect/0.7.3/fastselect.standalone.min.js',
						'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js',
						base_url('node_modules/sweetalert2/dist/sweetalert2.all.min.js'),
						base_url('node_modules/gijgo/js/gijgo.min.js'),
						'https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js',
						base_url( 'assets/js/app/user/account/activate.js' ),
					],
					'styles'		=> [
						'https://cdnjs.cloudflare.com/ajax/libs/fastselect/0.7.3/fastselect.min.css',
						base_url('node_modules/gijgo/css/gijgo.min.css'),
						'https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css',
					],
					'corporates'	=> $corporateResults->result()
				];

				$this->load->view('template-part/header', $data);
				$this->load->view('template-part/navbar', $data);
				$this->load->view('app/auth/activate', $data);
				$this->load->view('template-part/footer', $data);
			} else {
				$this->session->set_flashdata('message', 'Link aktivasi tidak ditemukan atau sudah dipakai');
				redirect(base_url('auth/login'), 'refresh');
			}
		}
	}

	public function submit_activation()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('activation_code', 'activation_code', 'required');
		$this->form_validation->set_rules('fullname', 'fullname', 'required');
		$this->form_validation->set_rules('gender', 'gender', 'required');
		$this->form_validation->set_rules('date-birth', 'date-birth', 'required');

		if ($this->form_validation->run() == TRUE) {
			$this->load->model(['UserModel','CustomerAddressModel']);

			$user = UserModel::where('activation_code', $this->input->post('activation_code'))->first();

			if ($user) {
				$address_data = [
                    'name'                      => 'Alamat Utama',
					'receiver_name'				=> $this->input->post('fullname'),
					'phone'						=> '',
					'user_id'					=> $user->id,
					'is_primary_address'	    => 'yes',
				];

				CustomerAddressModel::create($address_data);

				$first_name = explode(" ", $this->input->post('fullname'));
				$last_name = explode($first_name[0], $this->input->post('fullname'));

				$is_updated = $user->update([
					'first_name'		=> $first_name[0],
					'last_name'			=> $last_name[1],
					'phone'				=> '',
					'activation_code'	=> null,
				]);

				$birth= date('m/d/Y', strtotime($this->input->post('date-birth')));
				$user_meta = [
					[
						'meta_key'		=> 'nik',
						'meta_value'	=> '',
						'user_id'			=> $user->id,
					],
					[
						'meta_key'		=> 'gender',
						'meta_value'	=> $this->input->post('gender'),
						'user_id'			=> $user->id,
					],
					[
						'meta_key'		=> 'date-birth',
						'meta_value'	=> $birth,
						'user_id'			=> $user->id,
					],
					[
						'meta_key'		=> 'blood-type',
						'meta_value'	=> 'None',
						'user_id'			=> $user->id,
					],
					[
						'meta_key'		=> 'religion',
						'meta_value'	=> '-',
						'user_id'			=> $user->id,
					],
					[
						'meta_key'		=> 'corporate',
						'meta_value'	=> "-",
						'user_id'		=> $user->id,
					],
					[
						'meta_key'		=> 'job',
						'meta_value'	=> '',
						'user_id'			=> $user->id,
					],
				];

				$user->user_metas()->insert($user_meta);

				if ($is_updated) {
					$this->session->set_flashdata('message', 'Aktivasi akun berhasil, silakan login dengan data yang didaftarkan');
					json_generator( ['redirect' => base_url('auth/login') ] );
				}
			}
		}
	}

	public function register_p()
	{
		if ($this->ion_auth->logged_in()) {
			redirect(base_url('home'), 'refresh');
		} else {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('username', 'username', 'required');
			$this->form_validation->set_rules('phone', 'phone', 'required');
			$this->form_validation->set_rules('password', 'Password', 'required');
			$this->form_validation->set_rules('password_confirm', 'Confirm Password', 'required|matches[password]');

			if ($this->form_validation->run() == TRUE) {
				$this->load->model('UserModel');

				$username_exist = UserModel::where([
					'username' => $this->input->post('username')
				])->first();

				$phone_exist = UserModel::where([
					'phone' => $this->input->post('phone')
				])->first();

				if ($username_exist) {
					json_generator(
						[
							'text' 	=> 'Username sudah digunakan',
							'field'	=> 'username'
						],
						409
					);
				} else if ($phone_exist) {
					json_generator(
						[
							'text' 	=> 'Nomor HP sudah digunakan',
							'field'	=> 'phone'
						],
						409
					);
				} else {
					if ($this->input->post('password') != $this->input->post('password_confirm')) {
						json_generator(
							[
								'text' 	=> 'Password and confirm password does not match',
								'field'	=> 'password_confirm'
							],
							409
						);
					}

					$identity_column = $this->config->item('identity', 'ion_auth');
					$identity =  ($identity_column === 'phone') ? $this->input->post('phone') : $this->input->post('username');
					
					$additional_data = [
						'activation_code'	=> md5( date('Y-m-d H:i:s')."#".rand(1,99999999) )
					];

					if ($identity_column == 'phone') {
						$additional_data['username'] = $this->input->post('username');
					}

					if ($this->ion_auth->register_phone($identity, $this->input->post('username'), $this->input->post('password'), $this->input->post('phone'),  $additional_data)) {

						$this->session->set_flashdata('message', 'Registrasi berhasil, silakan lakukan <a href="'.base_url('auth/finish_registration_p').'"">Aktivasi akun</a>');
						$this->session->set_userdata('phone', $this->input->post('phone'));
						redirect(base_url('auth/finish_registration_p'), 'refresh');
						// $this->session->set_userdata('phone', $this->input->post('phone'));
						// redirect(base_url('auth/finish_registration_p'), 'refresh');
						json_generator(
							[
								'message' => 'Registrasi Sukses',
								'type'		=> 'success',
								'title'		=> 'Success',
							]
						);
					}
				}
			} else {
				$data = [
					'styles'	=> [
						'https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css'
					],
					'scripts'	=> [
						'https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/jquery.validate.min.js',
						'https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.12.7/sweetalert2.all.min.js',
						'https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js',
						base_url('assets/js/app/auth/register-phone.js'),
					]
				];

				$this->load->view('template-part/header', $data);
				$this->load->view('template-part/navbar', $data);
				$this->load->view('app/auth/register-phone', $data);
				$this->load->view('template-part/footer', $data);
			}
		}
	}

	public function finish_registration_p()
	{
		$code=$this->session->userdata('phone');
		if ($code) {
			$this->load->model(['UserModel','CorporateModel']);

			$this->db->from('corporate');
			$this->db->where('status', 'approved');
			$this->db->order_by('name', 'asc');
			$corporateResults = $this->db->get();
			$user = UserModel::where('phone', $code)->first();
			if ($user) {
		$this->load->library('form_validation');
		$this->form_validation->set_rules('activation_code', 'activation_code', 'required');
		$this->form_validation->set_rules('fullname', 'fullname', 'required');
		$this->form_validation->set_rules('gender', 'gender', 'required');
		$this->form_validation->set_rules('date-birth', 'date-birth', 'required');
		if ($this->form_validation->run() == TRUE) {
			$this->load->model(['UserModel','CustomerAddressModel']);

			$user = UserModel::where('activation_code', $this->input->post('activation_code'))->first();

			if ($user) {
				$address_data = [
                    'name'                      => 'Alamat Utama',
					'receiver_name'				=> $this->input->post('fullname'),
					'phone'						=> $this->input->post('phone'),
					'user_id'					=> $user->id,
					'is_primary_address'	    => 'yes',
				];

				CustomerAddressModel::create($address_data);

				$first_name = explode(" ", $this->input->post('fullname'));
				$last_name = explode($first_name[0], $this->input->post('fullname'));

				$is_updated = $user->update([
					'first_name'		=> $first_name[0],
					'last_name'			=> $last_name[1],
					'phone'				=> $this->input->post('phone'),
					'activation_code'	=> null,
				]);
				
				$birth= date('m/d/Y', strtotime($this->input->post('date-birth')));
				$user_meta = [
					[
						'meta_key'		=> 'nik',
						'meta_value'	=> '',
						'user_id'			=> $user->id,
					],
					[
						'meta_key'		=> 'gender',
						'meta_value'	=> $this->input->post('gender'),
						'user_id'			=> $user->id,
					],
					[
						'meta_key'		=> 'date-birth',
						'meta_value'	=> $birth,
						'user_id'			=> $user->id,
					],
					[
						'meta_key'		=> 'blood-type',
						'meta_value'	=> "None",
						'user_id'			=> $user->id,
					],
					[
						'meta_key'		=> 'religion',
						'meta_value'	=> "-",
						'user_id'			=> $user->id,
					],
					[
						'meta_key'		=> 'corporate',
						'meta_value'	=> "-",
						'user_id'		=> $user->id,
					],
					[
						'meta_key'		=> 'job',
						'meta_value'	=> "-",
						'user_id'			=> $user->id,
					],
				];

				$user->user_metas()->insert($user_meta);

				if ($is_updated) {
					$this->session->set_flashdata('message', 'Aktivasi akun berhasil, silakan login dengan data yang didaftarkan');
					json_generator( ['redirect' => base_url('auth/login') ] );
				}
			}

			} else {
				$data = [
					'user_info'	=> $user,
					'scripts'		=> [
						'https://cdnjs.cloudflare.com/ajax/libs/fastselect/0.7.3/fastselect.standalone.min.js',
						'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js',
						base_url('node_modules/sweetalert2/dist/sweetalert2.all.min.js'),
						base_url('node_modules/gijgo/js/gijgo.min.js'),
						'https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js',
						base_url( 'assets/js/app/user/account/activate-phone.js' ),
					],
					'styles'		=> [
						'https://cdnjs.cloudflare.com/ajax/libs/fastselect/0.7.3/fastselect.min.css',
						base_url('node_modules/gijgo/css/gijgo.min.css'),
						'https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css',
					],
					'corporates'	=> $corporateResults->result()
				];
				$this->load->view('template-part/header', $data);
				$this->load->view('template-part/navbar', $data);
				$this->load->view('app/auth/activate-phone', $data);
				$this->load->view('template-part/footer', $data);
			}
			} else {
				$this->session->set_flashdata('message', 'Link aktivasi tidak ditemukan atau sudah dipakai');
				redirect(base_url('auth/login'), 'refresh');
			}
		}
	}

	public function login()
	{
		if (!$this->ion_auth->logged_in()) {
			$this->data['title'] = $this->lang->line('login_heading');

			$this->form_validation->set_rules('identity', str_replace(':', '', $this->lang->line('login_identity_label')), 'required');
			$this->form_validation->set_rules('password', str_replace(':', '', $this->lang->line('login_password_label')), 'required');

			if ($this->form_validation->run() == TRUE) {
				$remember = (bool)$this->input->post('remember');

				if ($this->ion_auth->login($this->input->post('identity'), $this->input->post('password'), $remember)) {
					if ($this->ion_auth->in_group('admin') || $this->ion_auth->in_group('co_admin')) {
						$redirect = base_url('admin/dashboard');
					} else if ($this->ion_auth->in_group('corporate')) {
						$redirect = base_url('corporate/dashboard');
					} else {
						$redirect = base_url('home');
					}

					$data_json = array(
						'type'		  => 'success',
						'redirect'	=> $redirect,
					);
				} else {
					$data_json = array(
						'type'		=> 'warning',
						'title'		=> 'Login Gagal',
						'message'	=> $this->ion_auth->errors(),
					);
				}

				echo json_encode($data_json);
			} else {
				$this->data['identity'] = [
					'name' 	=> 'identity',
					'id'   	=> 'identity',
					'type' 	=> 'text',
					'value' => $this->form_validation->set_value('identity'),
				];

				$this->data['password'] = [
					'name' => 'password',
					'id' 	 => 'password',
					'type' => 'password',
				];

				$this->data['styles'] = [];
				$this->data['scripts'] 	= [
					'https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/jquery.validate.min.js',
					'https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.12.7/sweetalert2.all.min.js',
					base_url('assets/js/app/auth/login.js'),
				];

				$this->data['page'] = 'login';

				$this->load->view('template-part/header', $this->data);
				$this->load->view('template-part/navbar', $this->data);
				$this->load->view('app/auth/login', $this->data);
				$this->load->view('template-part/footer', $this->data);
			}
		} else {
			redirect(base_url('home'), 'refresh');
		}
	}

	public function logout()
	{
		$this->data['title'] = "Logout";

		$this->ion_auth->logout();

		redirect(base_url('auth/login'), 'refresh');
	}

	public function forgot_password()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('email', 'email', 'required');

		if ( $this->form_validation->run() == TRUE ){
			$this->load->model('UserModel');

			$result = [];

			$user = UserModel::where('email', $this->input->post('email'))->first();

			if ($user) {
				$this->load->library('my_email');

				$forgot_password_code = md5(date('Y-m-d H:i:s')."#".$user->id);

				$this->load->model('UserMetaModel');

				UserMetaModel::updateOrCreate(
					[
						'user_id'			=> $user->id,
						'meta_key'		=> 'forgot_password_code'
					],
					[
						'meta_value'	=> $forgot_password_code
					]
				);

				$data = [
					'username'	=> $user->username,
					'email'			=> $user->email,
					'code'			=> $forgot_password_code,
				];

				$is_email_sent = $this->my_email->forgot_password($data);

				if ($is_email_sent['status'] == 'success') {
					$this->session->set_flashdata('message', 'Silahkan cek email Anda untuk melanjutkan reset password');
					$this->session->set_flashdata('status', 'success');

					$result = [
						'status'	=> 'success',
					];
				} else {
					$this->session->set_flashdata('status', 'warning');
					$this->session->set_flashdata('message', 'Reset password gagal, email tidak terkirim! ['.$is_email_sent['message'].']');

					$result = [
						'status'	=> 'error',
					];
				}
			} else {
				$this->session->set_flashdata('status', 'warning');
				$this->session->set_flashdata('message', 'Email tidak terdaftar!');

				$result = [
					'status'	=> 'error',
				];
			}

			json_generator($result);
		} else {
			$data = [
				'scripts'	=> [
					'https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/jquery.validate.min.js',
					base_url('assets/js/app/auth/forgot-password.js'),
				]
			];

			$this->load->view('template-part/header', $data);
			$this->load->view('template-part/navbar', $data);
			$this->load->view('app/auth/forgot-password', $data);
			$this->load->view('template-part/footer', $data);
		}
	}

	public function update_password($code = null)
	{
		if ($code) {
			$this->load->model('UserMetaModel');

			$user_meta = UserMetaModel::where([
				'meta_key'		=> 'forgot_password_code',
				'meta_value'	=> $code,
			])->first();

			if ($user_meta) {
				$date_diff = strtotime(date('Y-m-d H:i:s')) - strtotime($user_meta->updated_at);

				if (($date_diff / 60) < 60) {
					$data = [
						'scripts'	=> [
							'https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/jquery.validate.min.js',
							base_url('assets/js/app/auth/update-password.js'),
						],
						'forgot_password_code'	=> $code,
					];

					$this->load->view('template-part/header', $data);
					$this->load->view('template-part/navbar', $data);
					$this->load->view('app/auth/update-password', $data);
					$this->load->view('template-part/footer', $data);
				} else {
					$user_meta->update(['meta_value' => '']);

					$this->session->set_flashdata('message', 'Reset password is expired');

					redirect(base_url('auth/forgot_password'), 'refresh');
				}
			} else {
				$this->session->set_flashdata('message', 'Reset password is expired');
				redirect(base_url('auth/forgot_password'), 'refresh');
			}
		} else {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('forgot_password_code', 'forgot_password_code', 'required');
			$this->form_validation->set_rules('password', 'Password', 'required');
			$this->form_validation->set_rules('password_confirm', 'Confirm Password', 'required|matches[password]');

			if ($this->form_validation->run() == TRUE) {
				$this->load->model('UserMetaModel');

				$user_meta = UserMetaModel::where([
					'meta_key'		=> 'forgot_password_code',
					'meta_value'	=> $this->input->post('forgot_password_code')
				])->first();

				$update_password = $user_meta->user->update([
					'password'	=> $this->ion_auth->hash_password( $this->input->post('password') )
				]);

				$user_meta->update([
					'meta_value'	=> ''
				]);

				$this->session->set_flashdata('message', 'Reset password berhasil! Silahkan login dengan passsword baru Anda');

				json_generator($update_password);
			}
		}
	}

	
	public function forgot_password_p()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('phone', 'phone', 'required');

		if ( $this->form_validation->run() == TRUE ){
			$this->load->model('UserModel');

			$result = [];

			$user = UserModel::where('phone', $this->input->post('phone'))->first();

			if ($user) {

				$forgot_password_code = md5(date('Y-m-d H:i:s')."#".$user->id);

				$this->load->model('UserMetaModel');

				UserMetaModel::updateOrCreate(
					[
						'user_id'			=> $user->id,
						'meta_key'		=> 'forgot_password_code'
					],
					[
						'meta_value'	=> $forgot_password_code
					]
				);
				$data = [
					'username'	=> $user->username,
					'phone'			=> $user->phone,
					'code'			=> $forgot_password_code,
				];

				$result = [
					'code'	=> $forgot_password_code,
				];
				$this->session->set_flashdata('status', 'warning');
				$this->session->set_flashdata('message', $forgot_password_code);
				redirect('auth/update_password_p/'.$forgot_password_code, 'refresh');

			} else {
				$this->session->set_flashdata('status', 'warning');
				$this->session->set_flashdata('message', 'Nomor Handphone tidak terdaftar!');
				$forgot_password_code=null;
				$result = [
					'status'	=> 'error',
				];
			}

			json_generator($result);
		} else {
			$data = [
				'scripts'	=> [
					'https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/jquery.validate.min.js',
					base_url('assets/js/app/auth/forgot-password-phone.js'),
				]
			];

			$this->load->view('template-part/header', $data);
			$this->load->view('template-part/navbar', $data);
			$this->load->view('app/auth/forgot-password-phone', $data);
			$this->load->view('template-part/footer', $data);
		}
	}

	public function update_password_p($code = null)
	{
		$code=$this->session->flashdata('message');
		if ($code) {
			$this->load->model('UserMetaModel');

			$user_meta = UserMetaModel::where([
				'meta_key'		=> 'forgot_password_code',
				'meta_value'	=> $code,
			])->first();

			if ($user_meta) {
				$date_diff = strtotime(date('Y-m-d H:i:s')) - strtotime($user_meta->updated_at);

				if (($date_diff / 60) < 60) {
					$data = [
						'scripts'	=> [
							'https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/jquery.validate.min.js',
							base_url('assets/js/app/auth/update-password.js'),
						],
						'forgot_password_code'	=> $code,
					];
					$data['code'] = $code;
					$this->load->view('template-part/header', $data);
					$this->load->view('template-part/navbar', $data);
					$this->load->view('app/auth/update-password-phone', $data);
					$this->load->view('template-part/footer', $data);
				} else {
					$user_meta->update(['meta_value' => '']);

					$this->session->set_flashdata('message', 'Reset password is expired');

					redirect(base_url('auth/forgot_password'), 'refresh');
				}
			} else {
				$this->session->set_flashdata('message', 'Reset password is expired');
				redirect(base_url('auth/forgot_password'), 'refresh');
			}
		} else {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('forgot_password_code', 'forgot_password_code', 'required');
			$this->form_validation->set_rules('password', 'Password', 'required');
			$this->form_validation->set_rules('password_confirm', 'Confirm Password', 'required|matches[password]');

			if ($this->form_validation->run() == TRUE) {
				$this->load->model('UserMetaModel');

				$user_meta = UserMetaModel::where([
					'meta_key'		=> 'forgot_password_code',
					'meta_value'	=> $this->input->post('forgot_password_code')
				])->first();

				$update_password = $user_meta->user->update([
					'password'	=> $this->ion_auth->hash_password( $this->input->post('password') )
				]);

				$user_meta->update([
					'meta_value'	=> ''
				]);

				$this->session->set_flashdata('message', 'Reset password berhasil! Silahkan login dengan passsword baru Anda');

				json_generator($update_password);
			}
		}
	}
}
