<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		if (!$this->ion_auth->is_admin()) {
			redirect(base_url('auth/login'), 'refresh');
		}
	}

	public function list()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('draw', 'draw', 'required');

		if ($this->form_validation->run() == TRUE) {
			$tablename = 'product';

			$this->load->model('datatables');
			$selected_column = array(
                "name"			=> 'name',
				"price"			=> 'price',
				"category"	    => "(SELECT name from product_category WHERE id=".$tablename.".category_id)",
				"id"			=> 'id',
				"discount"      => 'discount',
                "first_stock"   => "(SELECT SUM(first_stock) FROM product_stock WHERE product_id=product.id)",
                "last_stock"    => "(SELECT SUM(last_stock) FROM product_stock WHERE product_id=product.id)",
                "in_stock"      => "(SELECT SUM(in_stock) FROM product_stock WHERE product_id=product.id)",
                "out_stock"     => "(SELECT SUM(out_stock) FROM product_stock WHERE product_id=product.id)"
			);

			$column_order = array(null, 'name', 'price');
			$column_search = array('name', 'price');
			$order = array('id' => 'desc');

			$custom_column = '<a class="btn btn-warning btn-sm btn-edit"><i class="fas fa-edit"></i> Edit</a>';

			$this->datatables->get_datatables( $tablename, $selected_column, $column_order, $column_search, $order, null, $custom_column );
		} else {
			$data = [
				'page'			=> 'admin-product-list',
				'page_title'=> 'Product List',
				'scripts'		=> [
					'https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js',
					'https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js',
					'https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js',
					'https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js',
					'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js',
					'https://cdnjs.cloudflare.com/ajax/libs/fastselect/0.7.3/fastselect.standalone.min.js',
					'https://cdn.quilljs.com/1.3.6/quill.js',
					base_url('node_modules/sweetalert2/dist/sweetalert2.all.min.js'),
					base_url('assets/js/app/admin/product/product-list.js')
				],
				'styles'		=> [
					'https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css',
					'https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap4.min.css',
					'https://cdnjs.cloudflare.com/ajax/libs/fastselect/0.7.3/fastselect.min.css',
					'https://cdn.quilljs.com/1.3.6/quill.snow.css',
				]
			];

			$this->load->view('template-part/admin/header', $data);
			$this->load->view('template-part/admin/sidebar', $data);
			$this->load->view('app/admin/product/product-list', $data);
			$this->load->view('template-part/admin/footer', $data);
		}
	}

	public function new()
	{
		$this->load->model('WarehouseModel');

		$wishorder = null;

		if (isset($_GET['wishorder'])) {
			$this->load->model('WishOrderModel');
			$wishorder =  WishOrderModel::find($_GET['wishorder']);
		}

		$data = [
				'page'			=> 'admin-product-list',
				'page_title'=> 'Product New',
				'warehouses'=> WarehouseModel::all(),
				'scripts'		=> [
					'https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js',
					'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js',
					'https://cdnjs.cloudflare.com/ajax/libs/fastselect/0.7.3/fastselect.standalone.min.js',
					'https://cdn.quilljs.com/1.3.6/quill.js',
					base_url('node_modules/sweetalert2/dist/sweetalert2.all.min.js'),
					base_url('assets/js/app/admin/product/product-new.js')
				],
				'styles'		=> [
					'https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css',
					'https://cdnjs.cloudflare.com/ajax/libs/fastselect/0.7.3/fastselect.min.css',
					'https://cdn.quilljs.com/1.3.6/quill.snow.css',
				],
				'wishorder'	=> $wishorder,
		];

		$this->load->view('template-part/admin/header', $data);
		$this->load->view('template-part/admin/sidebar', $data);
		$this->load->view('app/admin/product/product-new', $data);
		$this->load->view('template-part/admin/footer', $data);
	}

	public function edit($id = null)
	{
		$this->load->model(['ProductModel','WarehouseModel']);

		if (!$id) {
			redirect(base_url('admin/product/list'), 'refresh');
		}

		$product = ProductModel::find($id);

		if ($product) {

			$data = [
					'page'			=> 'admin-product-list',
					'page_title'=> 'Product',
					'warehouses'=> WarehouseModel::all(),
					'scripts'		=> [
						'https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js',
						'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js',
						'https://cdnjs.cloudflare.com/ajax/libs/fastselect/0.7.3/fastselect.standalone.min.js',
						'https://cdn.quilljs.com/1.3.6/quill.js',
						base_url('node_modules/sweetalert2/dist/sweetalert2.all.min.js'),
						base_url( 'assets/js/app/admin/product/product-edit.js' )
					],
					'styles'		=> [
						'https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css',
						'https://cdnjs.cloudflare.com/ajax/libs/fastselect/0.7.3/fastselect.min.css',
						'https://cdn.quilljs.com/1.3.6/quill.snow.css'
					],
					'product'	=> $product
			];

			$this->load->view('template-part/admin/header', $data);
			$this->load->view('template-part/admin/sidebar', $data);
			$this->load->view('app/admin/product/product-edit', $data);
			$this->load->view('template-part/admin/footer', $data);
		} else {
			redirect(base_url('admin/product/list'), 'refresh');
		}
	}

	public function product_fastselect($id = null)
	{
		$this->load->model('ProductModel');

		$products = ProductModel::select('id as value','name as text');

		if ($id) {
			$result = $products->find($id);
		} else {
			$search_query = (isset($_GET['query'])) ? "name LIKE '%".$_GET['query']."%'" : null;

			if ($search_query) {
				$products->where('name', 'like', '%'.$_GET['query'].'%')->limit(5);
			}

			$products->limit(5);
			$result = $products->get();
		}

		json_generator($result);
	}

	public function category_fastselect()
	{
		$this->load->model('ProductCategoryModel');

		$search_query = (isset($_GET['query'])) ? "name LIKE '%".$_GET['query']."%'" : null;

		$product_category = ProductCategoryModel::select('id as value','name as text');

		if ($search_query) {
			$product_category->where('name', 'like', '%'.$_GET['query'].'%')->limit(5);
		}

		$product_category->limit(5);

		json_generator($product_category->get());
	}

	public function save()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules( 'name', 'name', 'required' );
		$this->form_validation->set_rules( 'price', 'price', 'required' );
		$this->form_validation->set_rules( 'category', 'category', 'required' );
		$this->form_validation->set_rules( 'stocks', 'stocks', 'required' );
		$this->form_validation->set_rules( 'weight', 'weight', 'required' );

		if ($this->form_validation->run() == TRUE){
			$this->load->model('ProductModel');

			$redirect_url = null;

			$data_insert = [
				'name'				=> $this->input->post('name'),
				'price'				=> $this->input->post('price'),
				'category_id'	    => $this->input->post('category'),
				'weight'			=> $this->input->post('weight'),
				'discount'		    => $this->input->post('discount') == '' ? null : $this->input->post('discount'),
                'buyer_count'       => 0,
                'ppn'               => 0,
                'margin'            => 0,
                'hna'               => 0,
                'suggestion_for'    => $this->input->post('suggestion_for')
			];

			if ($this->input->post('description')) {
				$data_insert['description'] = $this->input->post('description');
			}

			if ($this->input->post('preorder-time')) {
				$data_insert['preorder_time'] = $this->input->post('preorder-time');
			}

			$product = ProductModel::create($data_insert);

			$stock_data = [];

			foreach(json_decode($this->input->post('stocks')) as $key => $stock_list) {
				$stock_data[] = [
					'product_id'		=> $product->id,
					'warehouse_id'		=> $stock_list->warehouse_id,
					'first_stock'		=> $stock_list->stock,
					'last_stock'		=> $stock_list->stock,
                    'in_stock'          => 0,
                    'out_stock'         => 0,
                    'unit'              => 0
				];
			}

			if (count($stock_data) > 0) {
				$this->load->model('ProductStockModel');

				ProductStockModel::insert($stock_data);
			}

			$images = [];

			for($i = 0; $i < count($_FILES['image']['name']); $i++) {
				if ($_FILES['image']['name'][$i]) {
					$upload = image_uploader('product', md5(date('y-m-d-h-i-s')), 'image', $i+1);

					if ($upload) {
						$images[] = [
							'product_id'	=> $product->id,
							'image'			=> $upload,
						];
					}
				}
			}

			if (count($images) > 0) {
				$product->images()->insert($images);
			}

			if ($this->input->post('wishorder_id')) {
				$this->load->model('WishOrderModel');

				$wishorder_data = WishOrderModel::find($this->input->post('wishorder_id'));
				$wishorder_data->update([
					'product_id' => $product->id
				]);

				$redirect_url = base_url('admin/order/wishorder?id='.$wishorder_data->id.'&name='.$wishorder_data->name);
			}

			json_generator(
				($product) ?
					[
						'status'		=> 'success',
						'redirect'	=> $redirect_url
					]
					: null
			);
		} else {
			json_generator(
				[
					'status'	=> 'error',
					'data'		=> $this->form_validation->error_array()
				]
			);
		}
	}

	public function update()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('product_id', 'product_id', 'required');
		$this->form_validation->set_rules('name', 'name', 'required');
		$this->form_validation->set_rules('price', 'price', 'required');
		$this->form_validation->set_rules('category', 'category', 'required');
		$this->form_validation->set_rules('stocks', 'stocks', 'required');
		$this->form_validation->set_rules('weight', 'weight', 'required');

		if ($this->form_validation->run() == TRUE) {
			$this->load->model(['ProductModel','ProductImageModel','ProductStockModel']);

			$product = ProductModel::find( $this->input->post('product_id') );

			if ($this->input->post('current_images')) {
				$current_images = explode(",", $this->input->post('current_images'));

				foreach ($product->images as $key => $product_image) {
					if (!in_array($product_image->id, $current_images)) {
						delete_image_file('product', $product_image->image);

						ProductImageModel::find($product_image->id)->forceDelete();
					}
				}
			}

			$images = [];

			for($i = 0; $i < count($_FILES['image']['name']); $i++){
				if ($_FILES['image']['name'][$i]) {
					$upload = image_uploader( 'product', md5( date('y-m-d-h-i-s') ), 'image', $i+1 );

					if ($upload) {
						$images[] = [
							'product_id'	=> $product->id,
							'image'				=> $upload,
						];
					}
				}
			}

			if (count($images) > 0) {
				$product->images()->insert($images);
			}

			$current_stocks = [];
			foreach (json_decode($this->input->post('stocks')) as $key => $stock) {
				$current_stocks[] = $stock->warehouse_id;
			}

			foreach ($product->stocks as $key => $stock) {
				if (!in_array($stock->warehouse_id, $current_stocks)) {
					ProductStockModel::where([
						'product_id'	=> $product->id,
						'warehouse_id'	=> $stock->warehouse_id
					])->forceDelete();
				}
			}

			foreach (json_decode($this->input->post('stocks')) as $key => $stock_list) {
				$product_stock = ProductStockModel::where('product_id', $product->id)->first();

				$current_stock = $stock_list->stock;

				if ($product_stock->last_stock > $current_stock) {
					$product_stock->out_stock = $product_stock->last_stock - $current_stock;
				} else if ($product_stock->last_stock < $current_stock) {
					$product_stock->in_stock = $current_stock - $product_stock->last_stock;
				}
				
				$product_stock->last_stock = $current_stock;
				$product_stock->save();
			}

			$data_update = [
				'name'					=> $this->input->post('name'),
				'price'					=> $this->input->post('price'),
				'category_id'		=> $this->input->post('category'),
				'weight'				=> $this->input->post('weight'),
				'description'		=> $this->input->post('description'),
				'is_preorder'		=> ($this->input->post('is_preorder')) ? 'yes' : 'no',
				'preorder_time'	=> ($this->input->post('preorder-time')) ? $this->input->post('preorder-time') : null,
				'discount'			=> $this->input->post('discount') === '' ? null : $this->input->post('discount'),
                'suggestion_for'    => $this->input->post('suggestion_for')
			];

			json_generator($product->update($data_update));
		}
	}

	public function delete()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('id', 'id', 'required');

		if ($this->form_validation->run() == TRUE) {
			$this->load->model('ProductModel');

			$product = ProductModel::find($this->input->post('id'));

			foreach ($product->images as $key => $product_image) {
				delete_image_file('product', $product_image->image);
			}

			json_generator($product->forceDelete());
		}
	}

	public function category()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('draw', 'draw', 'required');

		if ($this->form_validation->run() == TRUE) {
			$this->load->model('datatables');

			$selected_column = array(
				"name"	=> 'name',
				"id"		=> 'id',
			);

			$column_order = array(null, 'name');
			$column_search = array('name');
			$order = array('id' => 'desc');

			$custom_column = '<button type="button" class="btn btn-warning btn-sm btn-edit"><i class="fas fa-edit"></i> Edit</button>';

			$this->datatables->get_datatables( 'product_category', $selected_column, $column_order, $column_search, $order, null, $custom_column );
		} else {
			$data = [
				'page'			=> 'admin-product-category',
				'page_title'=> 'Product Category List',
				'scripts'		=> [
					'https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js',
					'https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js',
					'https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js',
					'https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js',
					'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js',
					'https://cdnjs.cloudflare.com/ajax/libs/fastselect/0.7.3/fastselect.standalone.min.js',
					base_url('node_modules/sweetalert2/dist/sweetalert2.all.min.js'),
					base_url('assets/js/app/admin/product/product-category-list.js')
				],
				'styles'		=> [
					'https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css',
					'https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap4.min.css',
					'https://cdnjs.cloudflare.com/ajax/libs/fastselect/0.7.3/fastselect.min.css'
				]
			];

			$this->load->view('template-part/admin/header', $data);
			$this->load->view('template-part/admin/sidebar', $data);
			$this->load->view('app/admin/product/product-category-list', $data);
			$this->load->view('template-part/admin/footer', $data);
		}
	}

	public function save_category()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'name', 'required');

		if ($this->form_validation->run() == TRUE) {
			$this->load->model('ProductCategoryModel');

			$data_insert = [
				'name' => $this->input->post('name')
			];

			if ($_FILES['image']['name']) {
				$upload = image_uploader('product_category', md5( date('y-m-d-h-i-s')), 'image');

				if ($upload) {
					$data_insert['image'] = $upload;
				}
			}

			$result = ProductCategoryModel::create($data_insert);

			json_generator($result);
		}
	}

	public function delete_category()
	{
		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules('id', 'id', 'required');

		if ($this->form_validation->run() == TRUE) {
			$this->load->model('ProductCategoryModel');

			$product_category = ProductCategoryModel::find($this->input->post('id'));

			json_generator($product_category->forceDelete());
		}
	}

	public function product_category_detail()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('id', 'id', 'required');

		if ($this->form_validation->run() == TRUE) {
			$this->load->model('ProductCategoryModel');

			$result = ProductCategoryModel::find($this->input->post('id'));
			json_generator($result);
		}
	}

	public function update_category()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('id', 'id', 'required');
		$this->form_validation->set_rules('name', 'name', 'required');

		$product_category = ProductCategoryModel::find($this->input->post('id'));

		if ($this->form_validation->run() == TRUE && $product_category) {
			$data_update = [
				'name' => $this->input->post('name')
			];

			if ($_FILES['image']['name']) {
				delete_image_file('product_category', $product_category->image);

				$upload = image_uploader('product_category', md5(date('y-m-d-h-i-s')), 'image');

				if ($upload) {
					$data_update['image'] = $upload;
				}
			}

			$result = $product_category->update($data_update);

			json_generator($result);
		}
	}

	public function warehouse()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('draw', 'draw', 'required');

		if ($this->form_validation->run() == TRUE){
			$this->load->model('datatables');

			$selected_column = array(
				"name"			=> 'name',
				"address"		=> "CONCAT(
										address,
										' - ',
										village_text,
										' - ',
										(SELECT subdistrict_name FROM wilayah_subdistrict WHERE subdistrict_id=warehouse.subdistrict_id),
										'<br/>',
										(SELECT city_name FROM wilayah_city WHERE city_id=(SELECT city_id FROM wilayah_subdistrict WHERE subdistrict_id=warehouse.subdistrict_id)),
										'- ',
										(SELECT province FROM wilayah_province WHERE province_id=(SELECT province_id FROM wilayah_city WHERE city_id=(SELECT city_id FROM wilayah_subdistrict WHERE subdistrict_id=warehouse.subdistrict_id))),
										' - ',
										zipcode
									)",
				"id"			=> 'id',
			);

			$column_order = array(null, 'name');
			$column_search = array('name');
			$order = array('id' => 'desc');
			$custom_column = '<button type="button" class="btn btn-warning btn-sm btn-edit"><i class="fas fa-edit"></i> Edit</button>';

			$this->datatables->get_datatables('warehouse', $selected_column, $column_order, $column_search, $order, null, $custom_column);
		} else {
			$data = [
				'page'			=> 'admin-product-warehouse',
				'page_title'=> 'Product Warehouse List',
				'scripts'		=> [
					'https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js',
					'https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js',
					'https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js',
					'https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js',
					'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js',
					'https://cdnjs.cloudflare.com/ajax/libs/fastselect/0.7.3/fastselect.standalone.min.js',
					base_url('node_modules/sweetalert2/dist/sweetalert2.all.min.js'),
					base_url('assets/js/app/admin/product/warehouse/warehouse-list.js')
				],
				'styles'		=> [
					'https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css',
					'https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap4.min.css',
					'https://cdnjs.cloudflare.com/ajax/libs/fastselect/0.7.3/fastselect.min.css'
				]
			];

			$this->load->view('template-part/admin/header', $data);
			$this->load->view('template-part/admin/sidebar', $data);
			$this->load->view('app/admin/product/warehouse/warehouse-list', $data);
			$this->load->view('template-part/admin/footer', $data);
		}
	}

	public function warehouse_save()
	{
		$this->load->model('WarehouseModel');

		$_formdata = json_decode(json_encode($this->input->post('formData')), false);

		$data_insert = [
			'name'						=> $_formdata->name,
			'village_text'		=> $_formdata->village,
			'subdistrict_id'	=> $_formdata->subDistrict,
			'address'					=> $_formdata->address,
			'zipcode'					=> $_formdata->zipcode,
			'shipping'				=> (isset($_formdata->shipping)) ? json_encode($_formdata->shipping) : null
		];

		$result = WarehouseModel::create($data_insert);

		json_generator($result);
	}

	public function warehouse_update()
	{
		$this->load->model('WarehouseModel');

		$_formdata = json_decode(json_encode($this->input->post('formData')), false);

		$warehouse = WarehouseModel::findOrFail($_formdata->id);

		$data_update = [
			'name'						=> $_formdata->name,
			'village_text'		=> $_formdata->village,
			'subdistrict_id'	=> $_formdata->subDistrict,
			'address'					=> $_formdata->address,
			'zipcode'					=> $_formdata->zipcode,
			'shipping'				=> (isset($_formdata->shipping)) ? json_encode($_formdata->shipping) : null
		];

		$result = $warehouse->update($data_update);

		json_generator($result);
	}

	public function warehouse_delete()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('id', 'id', 'required');

		if ($this->form_validation->run() == TRUE) {
			$warehouse = WarehouseModel::find($this->input->post('id'));

			json_generator($warehouse->forceDelete());
		}
	}

	public function warehouse_detail()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('id', 'id', 'required');

		if ($this->form_validation->run() == TRUE) {
			$_warehouse = WarehouseModel::find($this->input->post('id'));

			$warehouse = [
				'id'						=> $_warehouse->id,
				'name'					=> $_warehouse->name,
				'village_text'	=> $_warehouse->village_text,
				'address'				=> $_warehouse->address,
				'zipcode'				=> $_warehouse->zipcode,
				'province'			=> ['value'	=> $_warehouse->provinsi->province_id, 'text' => $_warehouse->provinsi->province],
				'district'			=> ['value'	=> $_warehouse->kabupaten->city_id, 'text' => $_warehouse->kabupaten->city_name],
				'subdistrict'		=> ['value'	=> $_warehouse->kecamatan->subdistrict_id, 'text' => $_warehouse->kecamatan->subdistrict_name],
				'shipping'			=> $_warehouse->shipping
			];

			json_generator($warehouse);
		}
	}
}