<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Voucher extends CI_Controller {
	public function __construct(){
		parent::__construct();
		if ( !$this->ion_auth->is_admin() ) {
			redirect(base_url('auth/login'), 'refresh');
		} else {
			$this->load->model( 'VoucherModel' );
			$this->load->model( 'SharedVoucher' );
		}
	}

	public function index(){
		redirect(base_url('admin/voucher/list'), 'refresh');
	}

	public function list(){
		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'draw', 'draw', 'required' );
		if ($this->form_validation->run() === TRUE){
			$this->load->model( 'datatables' );

			$selected_column = array(
				"name"			=> 'name',
				"start_date"	=> 'start_date',
				"expired"		=> 'expired',
				"nominal"		=> 'nominal',
				"quantity"	    => 'quantity',
				"id"			=> 'id',
			);

			$column_order = array( null, 'name', 'start_date', 'expired', 'nominal', 'quantity');
			$column_search = array( 'name','expired' );
			$order = array( 'id' => 'desc' );

			$where = null;

		   	$custom_column 	= 	'
		   						<button type="button" class="btn btn-danger btn-sm btn-delete"><i class="fas fa-trash"></i> Delete</button>
	              				<a class="btn btn-warning btn-sm btn-edit voucher"><i class="fas fa-edit"></i> Edit</a>
	              				';

			$this->datatables->get_datatables( 'voucher', $selected_column, $column_order, $column_search, $order, $where, $custom_column );

		} else {
			$data = [
				'page'			=> 'admin-voucher-list',
				'page_title'	=> 'Voucher List',
				'users'				 => $this->db->query("SELECT u.id , u.username from users u left join users_groups ug on u.id = ug.user_id where group_id='2'")->result(),
				'scripts'		=> [
					// datatable
					'https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js',
					'https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js',
					'https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js',
					'https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js',

					// jquery validate
					'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js',

					// sweetalert2
					base_url('node_modules/sweetalert2/dist/sweetalert2.all.min.js'),

					// app
					base_url( 'assets/js/app/admin/voucher/voucher-list.js' ),
				],
				'styles'		=> [
					// datatable
					'https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css',
					'https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap4.min.css',
				]
			];

			$this->load->view('template-part/admin/header', $data);
			$this->load->view('template-part/admin/sidebar', $data);
			$this->load->view('app/admin/voucher/voucher-list', $data);
			$this->load->view('template-part/admin/footer', $data);
		}
	}

	public function new(){
		$data = [
			'page'			=> 'admin-voucher-new',
			'page_title'	=> 'voucher New',
			'scripts'	=> [
				'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js',
				base_url('node_modules/sweetalert2/dist/sweetalert2.all.min.js'),
				base_url( 'assets/js/app/admin/voucher/voucher-new.js' ),
			],
			'styles'	=> [
				'https://cdnjs.cloudflare.com/ajax/libs/fastselect/0.7.3/fastselect.min.css'
			]
		];

		$this->load->view('template-part/admin/header', $data);
		$this->load->view('template-part/admin/sidebar', $data);
		$this->load->view('app/admin/voucher/voucher-new', $data);
		$this->load->view('template-part/admin/footer', $data);
	}

	public function save(){
		$data_insert = [
			'name'			=> $this->input->post('name'),
			'nominal'		=> $this->input->post('nominal'),
			'quantity'		=> $this->input->post('quantity'),
			'start_date'	=> date("Y-m-d H:i:s", strtotime($this->input->post('start_date'))),
			'expired'		=> date("Y-m-d H:i:s", strtotime($this->input->post('expire_date')))
		];

		$inserted_voucher = VoucherModel::create( $data_insert );

		if ($inserted_voucher) {
			$selected_user = $this->input->post('selected_user');

			if (isset($selected_user) && count($selected_user) > 0) {
				foreach($selected_user as $id) {
					$data_insert_to_shared = [
						'voucher_id' => $inserted_voucher->id,
						'user_id'	 => $id,
						'quantity'	 => $inserted_voucher->quantity
					];

					SharedVoucher::create($data_insert_to_shared);
				}
			} else {
				$users = $this->db->select()->from('users')->get()->result_array();

				foreach($users as $user) {
					$data_insert_to_shared = [
						'voucher_id' => $inserted_voucher->id,
						'user_id'	 => $user['id'],
						'quantity'	 => $inserted_voucher->quantity
					];

					SharedVoucher::create($data_insert_to_shared);
				}
			}

			json_generator(['status' => 'success']);
		}
	}

	public function broadcastSave(){
		$this->load->library( 'form_validation' );
		$this->load->model( 'UserModel' );
		$this->form_validation->set_rules( 'voucher_id', 'voucher_id', 'required' );
		$this->form_validation->set_rules( 'type_broadcast', 'type_broadcast', 'required' );

		if ($this->form_validation->run() === TRUE){
			$voucher_id = $this->input->post('voucher_id');
			$type_broadcast = $this->input->post('type_broadcast');
			if($type_broadcast == 1){
				$allusers = $this->db->query("SELECT u.id from users u left join users_groups ug on u.id = ug.user_id where group_id='2'")->result();
				$asignusers = [];
				foreach ($allusers as $alluser){
					array_push($asignusers, $alluser->id);
				}
			}elseif($type_broadcast == 2){
				$asignusers = $this->input->post('users');
				($asignusers) ? $asignusers : $asignusers = [];
			}elseif($type_broadcast == 3){
				$asigncorporates = $this->input->post('corporates');
				if($asigncorporates) {
					$allusers = $this->db->select("user_id")
											->where_in("corporate_id", $asigncorporates)
											->where("status", 'approved')
											->get("corporate_member")
											->result();
				}else {
					$allusers = [];
				}
				$asignusers = [];
				foreach ($allusers as $alluser){
					array_push($asignusers, $alluser->user_id);
				}
			}
			$this->db->query("DELETE FROM voucher_user where voucher_id = '$voucher_id'");
			foreach ($asignusers as $asignuser) {
				$voucher_code = $this->generateCoupon(7);
				$user_id = $asignuser;
				$insertdata= $this->db->query("INSERT INTO voucher_user set voucher_id = '$voucher_id',
								  user_id = '$user_id',
								  voucher_code = '$voucher_code',
								  type_broadcast = '$type_broadcast'
								");

				// if($insertdata) {
				// 	$userdata = UserModel::find( $user_id );
				// 	$this->load->library( 'my_email' );
				// 	$data = [
				// 		'nama'	=> $userdata->first_name.' '.$userdata->last_name,
				// 		'email'	=> $userdata->email,
				// 	];
				// 	$this->my_email->voucher_mail( $data );
				// }
			}
			json_generator( ['status' => 'success']);
		}
	}

	public function delete(){
		$this->load->library( 'form_validation' );

		$this->form_validation->set_rules( 'id', 'id', 'required' );
		if ($this->form_validation->run() === TRUE && $voucher = voucherModel::find( $this->input->post( 'id' ) )){
			delete_image_file( 'voucher', $voucher->image );
			json_generator( $voucher->forceDelete() );
		}
	}

	public function broadcast( $id = null ){
		if ( $id && $voucher = voucherModel::find($id) ) {

			$data = [
				'page'				 => 'admin-voucher-broadcast',
				'page_title'		 => 'voucher broadcast',
				'voucher'			 => $voucher,
				'usersselected'		 => $this->db->query("SELECT user_id from voucher_user where voucher_id = '$id'  and type_broadcast = '2'")->result(),
				'corporatedselected' => $this->db->query("SELECT DISTINCT(cm.corporate_id)
														 from voucher_user vu
														 inner join corporate_member cm on vu.user_id= cm.user_id
														 where voucher_id = '$id'
														 and cm.status = 'approved'
														 and vu.type_broadcast = '3'
														 ")->result(),
				'type_broadcast'	 => $this->db->query("SELECT type_broadcast from voucher_user where voucher_id = '$id'")->row(),
				'users'				 => $this->db->query("SELECT u.id , u.username from users u left join users_groups ug on u.id = ug.user_id where group_id='2'")->result(),
				'corporates'		 => $this->db->query("SELECT c.id, c.name
															from corporate c
															left join corporate_member cm on cm.corporate_id=c.id
															where cm.status = 'approved'
														")->result(),
				//  'corporates'		 => $this->db->query("SELECT u.id , u.username
													// from users u
													// left join users_groups ug on u.id = ug.user_id
													// where group_id='2'
													// and u.id in (select cm.user_id from corporate_member cm)
													// ")->result(),
				'voucher_id'	=> $id,
				'scripts'		=> [
					'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js',
					base_url('node_modules/sweetalert2/dist/sweetalert2.all.min.js'),
					base_url( 'assets/js/app/admin/voucher/voucher-broadcast.js' ),
				],
				'styles'		=> [
					'https://cdnjs.cloudflare.com/ajax/libs/fastselect/0.7.3/fastselect.min.css',
					base_url( 'assets/admin/vendor/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css' ),
				]
			];

			$this->load->view('template-part/admin/header', $data);
			$this->load->view('template-part/admin/sidebar', $data);
			$this->load->view('app/admin/voucher/voucher-broadcast', $data);
			$this->load->view('template-part/admin/footer', $data);
		}
	}

	public function edit( $id = null ){
		if ( $id && $voucher = voucherModel::find($id) ) {
			$data = [
				'page'			=> 'admin-voucher-edit',
				'page_title'	=> 'voucher Edit',
				'voucher'		=> $voucher,
				'scripts'		=> [
					'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js',
					base_url('node_modules/sweetalert2/dist/sweetalert2.all.min.js'),
					base_url( 'assets/js/app/admin/voucher/voucher-edit.js' ),
				],
				'styles'		=> [
					'https://cdnjs.cloudflare.com/ajax/libs/fastselect/0.7.3/fastselect.min.css',
				]
			];

			$this->load->view('template-part/admin/header', $data);
			$this->load->view('template-part/admin/sidebar', $data);
			$this->load->view('app/admin/voucher/voucher-edit', $data);
			$this->load->view('template-part/admin/footer', $data);
		}
	}

	public function update(){
		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'id', 'id', 'required' );
		$this->form_validation->set_rules( 'name', 'name', 'required' );
		$this->form_validation->set_rules( 'nominal', 'nominal', 'required' );
		$this->form_validation->set_rules( 'start_date', 'expired', 'required' );
		$this->form_validation->set_rules( 'expired', 'expired', 'required' );
		if ($this->form_validation->run() === TRUE && $voucher = voucherModel::find($this->input->post('id'))){

			$data_update = [
				'name'			=> $this->input->post('name'),
				'nominal'		=> $this->input->post('nominal'),
				'start_date'	=> date("Y-m-d H:i:s", strtotime($this->input->post('start_date'))),
				'expired'		=> date("Y-m-d H:i:s", strtotime($this->input->post('expired'))),
			];

			if($this->input->post('quantity')){
				$data_update['quantity'] = $this->input->post('quantity');
			} else{
				$data_update['quantity'] = null;
			}

			json_generator( ($voucher->update( $data_update )) ? ['status' => 'success'] : [] );
		}
	}

	function generateCoupon(
		int $length = 64,
		string $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
	): string {
		if ($length < 1) {
			throw new \RangeException("Length must be a positive integer");
		}
		$pieces = [];
		$max = mb_strlen($keyspace, '8bit') - 1;
		for ($i = 0; $i < $length; ++$i) {
			$pieces []= $keyspace[random_int(0, $max)];
		}
		return implode('', $pieces);
	}

	public function search_user()
	{
		$user = $this->input->post('user');

		$users = $this->db->query("SELECT u.id, u.first_name, u.last_name, u.phone
								from users u left join users_groups ug on u.id = ug.user_id
								where group_id='2' and (first_name LIKE '%".$user."%' OR last_name LIKE '%".$user."%')")->result();

		json_generator($users);
	}

}