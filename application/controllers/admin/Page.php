<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends CI_Controller {
	public function __construct(){
		parent::__construct();
		if ( !$this->ion_auth->is_admin() ) {
			redirect(base_url('auth/login'), 'refresh');
		} else {
			$this->load->model('PageModel');
		}
	}

	public function index(){
		redirect(base_url('admin/page/list'), 'refresh');
	}

	public function list(){
		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'draw', 'draw', 'required' );
		if ($this->form_validation->run() === TRUE){
			$this->load->model( 'datatables' );
			
			$selected_column = array(
				"title"			=> 'title',
				"created_at"	=> 'created_at',
				"id"			=> 'id',
			);

			$column_order = array( null, 'title', 'created_at' );
			$column_search = array( 'title' );
			$order = array( 'id' => 'desc' );

			$where = null;

		   	$custom_column 	= '<a class="btn btn-warning btn-sm btn-edit"><i class="fas fa-edit"></i> Edit</a>';
			$this->datatables->get_datatables( 'page', $selected_column, $column_order, $column_search, $order, $where, $custom_column );

		} else {
			$data = [
				'page'			=> 'admin-page-list',
				'page_title'	=> 'Page List',
				'scripts'		=> [
					// datatable
					'https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js',
					'https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js',
					'https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js',
					'https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js',

					// jquery validate
					'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js',

					// sweetalert2
					base_url('node_modules/sweetalert2/dist/sweetalert2.all.min.js'),

					// app
					base_url( 'assets/js/app/admin/page/page-list.js' ),
				],
				'styles'		=> [
					// datatable
					'https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css',
					'https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap4.min.css',
				]
			];

			$this->load->view('template-part/admin/header', $data);
			$this->load->view('template-part/admin/sidebar', $data);
			$this->load->view('app/admin/page/page-list', $data);
			$this->load->view('template-part/admin/footer', $data);
		}
	}

	public function new(){
		$data = [
			'page'			=> 'admin-page-new',
			'page_title'	=> 'Page New',
			'scripts'	=> [
				// 'https://cdn.quilljs.com/1.3.6/quill.js',
				'https://cdn.ckeditor.com/4.13.0/full/ckeditor.js',
				'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js',
				base_url('node_modules/sweetalert2/dist/sweetalert2.all.min.js'),
				'https://cdnjs.cloudflare.com/ajax/libs/fastselect/0.7.3/fastselect.standalone.min.js',
				base_url( 'assets/js/app/admin/page/page-new.js' ),
			],
			'styles'	=> [
				'https://cdn.quilljs.com/1.3.6/quill.snow.css',
				'https://cdnjs.cloudflare.com/ajax/libs/fastselect/0.7.3/fastselect.min.css',
			]
		];

		$this->load->view('template-part/admin/header', $data);
		$this->load->view('template-part/admin/sidebar', $data);
		$this->load->view('app/admin/page/page-new', $data);
		$this->load->view('template-part/admin/footer', $data);
	}

	public function edit( $id = null ){

		if ( $id && $page = PageModel::find( $id ) ) {
			$data = [
				'page'			=> 'admin-page-edit',
				'page_title'	=> 'Page Edit',
				'page'      	=> $page,
				'scripts'	=> [
					// 'https://cdn.quilljs.com/1.3.6/quill.js',
					'https://cdn.ckeditor.com/4.13.0/full/ckeditor.js',
					'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js',
					base_url('node_modules/sweetalert2/dist/sweetalert2.all.min.js'),
					'https://cdnjs.cloudflare.com/ajax/libs/fastselect/0.7.3/fastselect.standalone.min.js',
					base_url( 'assets/js/app/admin/page/page-new.js' ),
				],
				'styles'	=> [
					'https://cdn.quilljs.com/1.3.6/quill.snow.css',
					'https://cdnjs.cloudflare.com/ajax/libs/fastselect/0.7.3/fastselect.min.css',
				]
			];

			$this->load->view('template-part/admin/header', $data);
			$this->load->view('template-part/admin/sidebar', $data);
			$this->load->view('app/admin/page/page-edit', $data);
			$this->load->view('template-part/admin/footer', $data);
		}
	}

	public function save(){
		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'title', 'title', 'required' );
		if ($this->form_validation->run() === TRUE){

			// upload images
			if ( $_FILES['image']['name'] ) {
				$uploaded_image = image_uploader( 'page', md5( date('y-m-d-h-i-s') ), 'image' );
			} else {
				$uploaded_image = null;
			}

			$data_insert = [
				'title'			=> $this->input->post('title'),
				'description'	=> $this->input->post('description'),
				'image'		    => $uploaded_image,
			];

			json_generator( (PageModel::create($data_insert)) ? ['status' => 'success'] : null );
		}
	}

	public function update(){
		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'id', 'id', 'required' );
		$this->form_validation->set_rules( 'title', 'title', 'required' );
		if ($this->form_validation->run() === TRUE && $page = PageModel::find( $this->input->post('id') ) ){
			$data_update = [
				'title'			=> $this->input->post('title'),
				'description'	=> $this->input->post('description'),
			];

			if ( !$this->input->post('current_image') ) {
				delete_image_file( 'page', $page->image );
				
				if ( $_FILES['image']['name'] ) {
					$data_update['image'] = image_uploader( 'page', md5( date('y-m-d-h-i-s') ), 'image' );
				} else {
					$data_update['image'] = null;
				}
			}

			json_generator( ($page->update($data_update)) ? ['status' => 'success'] : null );
		}
	}

	public function delete(){
		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'id', 'id', 'required' );
		if ($this->form_validation->run() === TRUE && $page = PageModel::find($this->input->post('id'))){
			delete_image_file( 'page', $page->image );
			json_generator( $page->forceDelete() );	
		}
	}
}