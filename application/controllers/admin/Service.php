<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Service extends CI_Controller {
	public function __construct(){
		parent::__construct();
		if ( !$this->ion_auth->is_admin() ) {
			redirect(base_url('auth/login'), 'refresh');
		}
	}

	public function index(){
		redirect(base_url('admin/service/list'), 'refresh');
	}

	public function list(){
		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'draw', 'draw', 'required' );
		if ($this->form_validation->run() === TRUE){

			$this->load->model( 'datatables' );
			
			$selected_column = array(
				"title"			=> 'title',
				'category'		=> '(SELECT name FROM service_category WHERE id=service.category_id)',
				"created_at"	=> 'created_at',
				"id"			=> 'id',
			);

			$column_order = array( null, 'title', 'created_at' );
			$column_search = array( 'title' );
			$order = array( 'id' => 'desc' );

			$where = null;

		   	$custom_column 	= 	'
		   						<button type="button" class="btn btn-danger btn-sm btn-delete"><i class="fas fa-trash"></i> Delete</button>
	              				<a class="btn btn-warning btn-sm btn-edit"><i class="fas fa-edit"></i> Edit</a>
	              				';

			$this->datatables->get_datatables( 'service', $selected_column, $column_order, $column_search, $order, $where, $custom_column );
		

		} else {
			$data = [
				'page'			=> 'admin-service-list',
				'page_title'	=> 'Service List',
				'scripts'		=> [
					// datatable
					'https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js',
					'https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js',
					'https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js',
					'https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js',

					// jquery validate
					'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js',

					// sweetalert2
					base_url('node_modules/sweetalert2/dist/sweetalert2.all.min.js'),

					// app
					base_url( 'assets/js/app/admin/service/service-list.js' ),
				],
				'styles'		=> [
					// datatable
					'https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css',
					'https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap4.min.css',


				]
			];

			$this->load->view('template-part/admin/header', $data);
			$this->load->view('template-part/admin/sidebar', $data);
			$this->load->view('app/admin/service/service-list', $data);
			$this->load->view('template-part/admin/footer', $data);
		}
	}

	public function delete(){
		$this->load->library( 'form_validation' );
		$this->load->model( 'ServiceModel' );

		$this->form_validation->set_rules( 'id', 'id', 'required' );
		if ($this->form_validation->run() === TRUE && $service = ServiceModel::find( $this->input->post( 'id' ) )){

			// delete images
			foreach ($service->images as $key => $service_image) {
				delete_image_file( 'service', $service_image->image );
			}

			json_generator( $service->forceDelete() );
		}
	}

	public function save(){

		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'title', 'title', 'required' );
		if ($this->form_validation->run() === TRUE){
			$data_insert = [
				'title'			=> $this->input->post('title'),
				'description'	=> $this->input->post('description'),
				'category_id'	=> $this->input->post('category_id'),
			];

			$this->load->model( 'ServiceModel' );
			$service = ServiceModel::create( $data_insert );

			// upload images
			$images = [];
			for($i = 0; $i < count($_FILES['image']['name']); $i++){
				if ( $_FILES['image']['name'][$i] ) {
					$upload = image_uploader( 'service', md5( date('y-m-d-h-i-s') ), 'image', $i+1 );
					if ( $upload ) {
						$images[] = [
							'service_id'	=> $service->id,
							'image'			=> $upload,
						];
					}
				}
			}

			if ( count($images) > 0 ) {
				$service->images()->insert( $images );
			}

			json_generator( ($service) ? ['status' => 'success'] : null );

		} else {
			json_generator(
				[
					'status'	=> 'error',
					'data'		=> $this->form_validation->error_array()
				]
			);
		}
	}

	public function category(){
		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'draw', 'draw', 'required' );
		if ($this->form_validation->run() === TRUE){

			$this->load->model( 'datatables' );
			
			$selected_column = array(
				"name"			=> 'name',
				"service_total"	=> '(SELECT COUNT(*) FROM service WHERE category_id=service_category.id)',
				"id"			=> 'id',
			);

			$column_order = array( null, 'name', 'service_total' );
			$column_search = array( 'name' );
			$order = array( 'id' => 'desc' );

			$where = null;

		   	$custom_column 	= 	'
		   						<button type="button" class="btn btn-danger btn-sm btn-delete"><i class="fas fa-trash"></i> Delete</button>
	              				<button type="button" class="btn btn-warning btn-sm btn-edit"><i class="fas fa-edit"></i> Edit</button>
	              				';

			$this->datatables->get_datatables( 'service_category', $selected_column, $column_order, $column_search, $order, $where, $custom_column );
		

		} else {
			$data = [
				'page'			=> 'admin-service-category',
				'page_title'	=> 'Service List',
				'scripts'		=> [
					// datatable
					'https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js',
					'https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js',
					'https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js',
					'https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js',

					// jquery validate
					'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js',

					// sweetalert2
					base_url('node_modules/sweetalert2/dist/sweetalert2.all.min.js'),

					// app
					base_url( 'assets/js/app/admin/service/service-category.js' ),
				],
				'styles'		=> [
					// datatable
					'https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css',
					'https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap4.min.css',
				]
			];

			$this->load->view('template-part/admin/header', $data);
			$this->load->view('template-part/admin/sidebar', $data);
			$this->load->view('app/admin/service/service-category', $data);
			$this->load->view('template-part/admin/footer', $data);
		}
	}

	public function category_save(){
		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'name', 'name', 'required' );
		if ($this->form_validation->run() === TRUE){
			$this->load->model('ServiceCategoryModel');
			$result = ServiceCategoryModel::create(
				[
					'name'	=> $this->input->post('name')
				]
			);

			json_generator( $result );
		}
	}

	public function category_update(){
		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'name', 'name', 'required' );
		$this->form_validation->set_rules( 'id', 'id', 'required' );
		if ($this->form_validation->run() === TRUE && $category = ServiceCategoryModel::find($this->input->post('id'))){
			json_generator( $category->update( ['name' => $this->input->post('name')] ) );
		}
	}

	public function category_delete(){
		$this->load->library( 'form_validation' );
		$this->load->model('ServiceCategoryModel');

		$this->form_validation->set_rules( 'id', 'id', 'required' );
		if ($this->form_validation->run() === TRUE && $category = ServiceCategoryModel::find($this->input->post('id'))){
			json_generator( $category->forceDelete() );
		}
	}

	public function category_detail(){
		$this->load->library( 'form_validation' );
		$this->load->model('ServiceCategoryModel');

		$this->form_validation->set_rules( 'id', 'id', 'required' );
		if ($this->form_validation->run() === TRUE && $category = ServiceCategoryModel::find($this->input->post('id'))){
			json_generator( $category );
		}
	}

	public function new(){
		$data = [
			'page'			=> 'admin-service-new',
			'page_title'	=> 'Service New',
			'scripts'	=> [
				'https://cdn.quilljs.com/1.3.6/quill.js',
				'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js',
				base_url('node_modules/sweetalert2/dist/sweetalert2.all.min.js'),
				'https://cdnjs.cloudflare.com/ajax/libs/fastselect/0.7.3/fastselect.standalone.min.js',
				base_url( 'assets/js/app/admin/service/service-new.js' ),
			],
			'styles'	=> [
				'https://cdn.quilljs.com/1.3.6/quill.snow.css',
				'https://cdnjs.cloudflare.com/ajax/libs/fastselect/0.7.3/fastselect.min.css',
			]
		];

		$this->load->view('template-part/admin/header', $data);
		$this->load->view('template-part/admin/sidebar', $data);
		$this->load->view('app/admin/service/service-new', $data);
		$this->load->view('template-part/admin/footer', $data);
	}

	public function edit( $id = null ){

		$this->load->model('ServiceModel');

		if ( $id && $service = ServiceModel::find($id) ) {
			$data = [
				'page'			=> 'admin-service-edit',
				'page_title'	=> 'Service Edit',
				'service'		=> $service,
				'scripts'	=> [
					'https://cdn.quilljs.com/1.3.6/quill.js',
					'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js',
					base_url('node_modules/sweetalert2/dist/sweetalert2.all.min.js'),
					'https://cdnjs.cloudflare.com/ajax/libs/fastselect/0.7.3/fastselect.standalone.min.js',
					base_url( 'assets/js/app/admin/service/service-new.js' ),
				],
				'styles'	=> [
					'https://cdn.quilljs.com/1.3.6/quill.snow.css',
					'https://cdnjs.cloudflare.com/ajax/libs/fastselect/0.7.3/fastselect.min.css',
				]
			];

			$this->load->view('template-part/admin/header', $data);
			$this->load->view('template-part/admin/sidebar', $data);
			$this->load->view('app/admin/service/service-edit', $data);
			$this->load->view('template-part/admin/footer', $data);
		}
	}

	public function category_fastselect(){
		$this->load->model( 'ServiceCategoryModel' );

		$search_query = ( isset( $_GET['query'] ) ) ? "name LIKE '%".$_GET['query']."%'" : null;

		$category = ServiceCategoryModel::select('id as value','name as text');
		if ( $search_query ) {
			$category->where('name', 'like', '%'.$_GET['query'].'%')->limit(5);
		} 
		$category->limit(5);

		json_generator($category->get());
	}

	public function update(){
		$this->load->library( 'form_validation' );

		$this->form_validation->set_rules( 'id', 'id', 'required' );
		$this->form_validation->set_rules( 'title', 'title', 'required' );
		$this->form_validation->set_rules( 'category_id', 'category_id', 'required' );

		if ($this->form_validation->run() === TRUE){
			
			$this->load->model(['ServiceModel','ServiceImageModel']);
			
			$service = ServiceModel::find( $this->input->post('id') );

			// check current image
			if ( $this->input->post('current_images') ) {
				
				$current_images = explode( ",", $this->input->post('current_images') );
				
				foreach ($service->images as $key => $service_image) {
				
					if ( !in_array($service_image->id, $current_images) ) {
						// remove unused images
						delete_image_file( 'service', $service_image->image );
						ServiceImageModel::find( $service_image->id )->forceDelete();
					} 
				}
			}

			// upload new images
			$images = [];
			for($i = 0; $i < count($_FILES['image']['name']); $i++){
				if ( $_FILES['image']['name'][$i] ) {
					$upload = image_uploader( 'service', md5( date('y-m-d-h-i-s') ), 'image', $i+1 );
					if ( $upload ) {
						$images[] = [
							'service_id'	=> $service->id,
							'image'			=> $upload,
						];
					}
				}
			}
			if ( count($images) > 0 ) {
				$service->images()->insert( $images );
			}

			
			// main data
			$data_update = [
				'title'			=> $this->input->post( 'title' ),
				'category_id'	=> $this->input->post( 'category_id' ),
				'description'	=> $this->input->post( 'description' ),
			];

			json_generator( ($service->update( $data_update )) ? ['status' => 'success'] : null );
		}
	}
}