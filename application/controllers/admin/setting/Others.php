<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class  Others extends CI_Controller {
	public function __construct(){
		parent::__construct();
		if ( !$this->ion_auth->is_admin() ) {
			redirect(base_url('auth/login'), 'refresh');
		}
	}

	public function index(){

		$settings = [];
		foreach (SettingModel::all() as $key => $setting) {
			$settings[ $setting->meta_key ] = $setting->meta_value;
		}

			$data = [
				'page'			=> 'admin-setting-others',
				'page_title'	=> 'Other Settings',
				'settings'		=> $settings,
				'scripts'		=> [
					// datatable
					'https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js',
					'https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js',
					'https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js',
					'https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js',

					// jquery validate
					'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js',

					// sweetalert2
					base_url('node_modules/sweetalert2/dist/sweetalert2.all.min.js'),

					// app
					base_url( 'assets/js/app/admin/setting/others/setting-others.js' ),
				],
				'styles'		=> [
					// datatable
					'https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css',
					'https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap4.min.css',
				]
			];

			$this->load->view('template-part/admin/header', $data);
			$this->load->view('template-part/admin/sidebar', $data);
			$this->load->view('app/admin/setting/others/others-index', $data);
			$this->load->view('template-part/admin/footer', $data);
	}

	public function unique_code(){
		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'unique_code_digit', 'unique_code_digit', 'required' );
		if ($this->form_validation->run() === TRUE){

			$result = SettingModel::updateOrCreate(
				['meta_key'		=> 'unique_code_digit'],
				['meta_value'	=> $this->input->post('unique_code_digit')]
			);

			$status = ($result) ? ['status' => 'success'] : ['status' => 'error']; 
			json_generator( $status );
		}
	}

	public function contact_info(){
		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'whatsapp_number', 'whatsapp_number', 'required' );
		$this->form_validation->set_rules( 'facebook', 'facebook', 'required' );
		$this->form_validation->set_rules( 'instagram', 'instagram', 'required' );
		$this->form_validation->set_rules( 'our_office', 'our_office', 'required' );
		$this->form_validation->set_rules( 'map', 'map', 'required' );
		if ($this->form_validation->run() === TRUE){
			$data = ['whatsapp_number','facebook','instagram','our_office','map'];
			$error = [];
			foreach ($data as $key => $item) {
				$result = SettingModel::updateOrCreate(
					['meta_key'		=> $item],
					['meta_value'	=> $this->input->post($item)]
				);

				if (!$result) {
					$error[] = $key;
				}
			}

			$status = (count($error) == 0) ? ['status' => 'success'] : ['status' => 'error']; 
			json_generator( $status );
		}
	}

	// public function whatsapp_number(){
	// 	$this->load->library( 'form_validation' );
	// 	$this->form_validation->set_rules( 'whatsapp_number', 'whatsapp_number', 'required' );
	// 	if ($this->form_validation->run() === TRUE){
	// 		$result = SettingModel::updateOrCreate(
	// 			['meta_key'		=> 'whatsapp_number'],
	// 			['meta_value'	=> $this->input->post('whatsapp_number')]
	// 		);

	// 		$status = ($result) ? ['status' => 'success'] : ['status' => 'error']; 
	// 		json_generator( $status );
	// 	}
	// }

	// public function facebook(){
	// 	$this->load->library( 'form_validation' );
	// 	$this->form_validation->set_rules( 'facebook', 'facebook', 'required' );
	// 	if ($this->form_validation->run() === TRUE){
	// 		$result = SettingModel::updateOrCreate(
	// 			['meta_key'		=> 'facebook'],
	// 			['meta_value'	=> $this->input->post('facebook')]
	// 		);

	// 		$status = ($result) ? ['status' => 'success'] : ['status' => 'error']; 
	// 		json_generator( $status );
	// 	}
	// }

	// public function instagram(){
	// 	$this->load->library( 'form_validation' );
	// 	$this->form_validation->set_rules( 'instagram', 'instagram', 'required' );
	// 	if ($this->form_validation->run() === TRUE){
	// 		$result = SettingModel::updateOrCreate(
	// 			['meta_key'		=> 'instagram'],
	// 			['meta_value'	=> $this->input->post('instagram')]
	// 		);

	// 		$status = ($result) ? ['status' => 'success'] : ['status' => 'error']; 
	// 		json_generator( $status );
	// 	}
	// }
}