<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bank_accounts extends CI_Controller {
	public function __construct(){
		parent::__construct();
		if ( !$this->ion_auth->is_admin() ) {
			redirect(base_url('auth/login'), 'refresh');
		}
	}

	public function index(){
		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'draw', 'draw', 'required' );
		if ($this->form_validation->run() === TRUE){

			$tablename = 'bank';
			$this->load->model( 'datatables' );
			$selected_column = array(
				"name"			=> 'name',
				"account_name"	=> 'account_name',
				"account_number"=> 'account_number',
				"status"		=> "status",
				"id"			=> 'id',
			);

			$column_order = array( null, 'name', 'account_name', 'account_number', 'status' );
			$column_search = array( 'name', 'account_name', 'account_number', 'status' );
			$order = array( 'id' => 'desc' );

			$where = null;

		   	$custom_column 	= '<button type="button" class="btn btn-warning btn-sm btn-edit"><i class="fas fa-edit"></i> Edit</button>';
			$this->datatables->get_datatables( $tablename, $selected_column, $column_order, $column_search, $order, $where, $custom_column );

		} else {

			$data = [
				'page'			=> 'admin-setting-bank_accounts',
				'page_title'	=> 'Bank Accounts',
				'scripts'		=> [
					// datatable
					'https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js',
					'https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js',
					'https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js',
					'https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js',

					// jquery validate
					'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js',

					// sweetalert2
					base_url('node_modules/sweetalert2/dist/sweetalert2.all.min.js'),

					// app
					base_url( 'assets/js/app/admin/setting/bank-account/bank-account-list.js' ),
				],
				'styles'		=> [
					// datatable
					'https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css',
					'https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap4.min.css',
				]
			];

			$this->load->view('template-part/admin/header', $data);
			$this->load->view('template-part/admin/sidebar', $data);
			$this->load->view('app/admin/setting/bank-account/bank-account-list', $data);
			$this->load->view('template-part/admin/footer', $data);
		}
	}

	public function save(){
		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'name', 'name', 'required' );
		$this->form_validation->set_rules( 'account_name', 'account_name', 'required' );
		$this->form_validation->set_rules( 'account_number', 'account_number', 'required' );
		$this->form_validation->set_rules( 'status', 'status', 'required' );
		if ($this->form_validation->run() === TRUE){

			$data_insert = [
				'name'			=> $this->input->post( 'name' ),
				'account_name'	=> $this->input->post( 'account_name' ),
				'account_number'=> $this->input->post( 'account_number' ),
				'status'		=> $this->input->post( 'status' ),
			];

			$this->load->model('BankModel');

			$result = BankModel::create( $data_insert );
			json_generator( $result );
		}
	}

	public function update(){
		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'name', 'name', 'required' );
		$this->form_validation->set_rules( 'account_name', 'account_name', 'required' );
		$this->form_validation->set_rules( 'account_number', 'account_number', 'required' );
		$this->form_validation->set_rules( 'status', 'status', 'required' );
		$this->form_validation->set_rules( 'id', 'id', 'required' );
		if ($this->form_validation->run() === TRUE){

			$data_update = [
				'name'			=> $this->input->post( 'name' ),
				'account_name'	=> $this->input->post( 'account_name' ),
				'account_number'=> $this->input->post( 'account_number' ),
				'status'		=> $this->input->post( 'status' ),
			];

			$this->load->model('BankModel');

			$result = BankModel::find( $this->input->post('id') )->update( $data_update );

			json_generator( $result );
		}
	}

	public function update_status(){
		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'id', 'id', 'required' );
		$this->form_validation->set_rules( 'status', 'status', 'required' );
		if ($this->form_validation->run() === TRUE){

			$data_update = [
				'status'		=> $this->input->post( 'status' ),
			];

			$this->load->model('BankModel');

			$result = BankModel::find( $this->input->post('id') )->update( $data_update );

			json_generator( $result );
		}
	}

	public function delete(){
		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'id', 'id', 'required' );
		if ($this->form_validation->run() === TRUE){
			$this->load->model('BankModel');
			$result = BankModel::find( $this->input->post('id') )->forceDelete();

			json_generator( $result );
		}
	}

	public function detail(){
		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'id', 'id', 'required' );
		if ($this->form_validation->run() === TRUE){
			$this->load->model('BankModel');

			$result = BankModel::find( $this->input->post('id') );

			json_generator( $result );
		}
	}
}