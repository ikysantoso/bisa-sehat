<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Slider extends CI_Controller {
	public function __construct(){
		parent::__construct();
		if ( !$this->ion_auth->is_admin() ) {
			redirect(base_url('auth/login'), 'refresh');
		} else {
			$this->load->model( 'SliderModel' );
		}
	}

	public function index(){
		redirect(base_url('admin/slider/list'), 'refresh');
	}

	public function list(){
		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'draw', 'draw', 'required' );
		if ($this->form_validation->run() === TRUE){

			$this->load->model( 'datatables' );
			
			$selected_column = array(
				"title1"		=> 'title1',
				"start_date"	=> "start_date",
				"end_date"		=> "end_date",
				"created_at"	=> 'created_at',
				"status"		=> 'status',
				"id"			=> 'id',
			);

			$column_order = array( null, 'title1', 'title2', 'created_at' );
			$column_search = array( 'title1', 'title2' );
			$order = array( 'id' => 'desc' );

			$where = null;

		   	$custom_column 	= '<a class="btn btn-warning btn-sm btn-edit"><i class="fas fa-edit"></i> Edit</a>';

			$this->datatables->get_datatables( 'slider', $selected_column, $column_order, $column_search, $order, $where, $custom_column );
		

		} else {
			$data = [
				'page'			=> 'admin-slider-list',
				'page_title'	=> 'Slider List',
				'scripts'		=> [
					// datatable
					'https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js',
					'https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js',
					'https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js',
					'https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js',

					// jquery validate
					'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js',

					// sweetalert2
					base_url('node_modules/sweetalert2/dist/sweetalert2.all.min.js'),

					// app
					base_url( 'assets/js/app/admin/slider/slider-list.js' ),
				],
				'styles'		=> [
					// datatable
					'https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css',
					'https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap4.min.css',
				]
			];

			$this->load->view('template-part/admin/header', $data);
			$this->load->view('template-part/admin/sidebar', $data);
			$this->load->view('app/admin/slider/slider-list', $data);
			$this->load->view('template-part/admin/footer', $data);
		}
	}

	public function new(){
		$data = [
			'page'			=> 'admin-slider-new',
			'page_title'	=> 'Slider New',
			'scripts'	=> [
				'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js',
				base_url('node_modules/sweetalert2/dist/sweetalert2.all.min.js'),
				'https://cdn.jsdelivr.net/momentjs/latest/moment.min.js',
				'https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js',
				base_url('node_modules/gijgo/js/gijgo.min.js'),
				base_url( 'assets/js/app/admin/slider/slider-new.js' ),
			],
			'styles'	=> [
				'https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css',
				base_url('node_modules/gijgo/css/gijgo.min.css'),
				'https://cdnjs.cloudflare.com/ajax/libs/fastselect/0.7.3/fastselect.min.css',
			]
		];

		$this->load->view('template-part/admin/header', $data);
		$this->load->view('template-part/admin/sidebar', $data);
		$this->load->view('app/admin/slider/slider-new', $data);
		$this->load->view('template-part/admin/footer', $data);
	}

	public function save(){
		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'title1', 'title1', 'required' );
		// $this->form_validation->set_rules( 'title2', 'title2', 'required' );
		$this->form_validation->set_rules( 'active_option', 'active_option', 'required' );
		$this->form_validation->set_rules( 'status', 'status', 'required' );
		if ($this->form_validation->run() === TRUE){

			// upload images
			if ( $_FILES['image']['name'] ) {
				$uploaded_image = image_uploader( 'slider', md5( date('y-m-d-h-i-s') ), 'image' );
			} else {
				$uploaded_image = null;
			}


			$data_insert = [
				'title1'	=> $this->input->post('title1'),
				'title2'	=> ($this->input->post('title2')) ? $this->input->post('title2') : '',
				'image'		=> $uploaded_image,
				'link'		=> $this->input->post('link'),
				'status'	=> ( in_array( $this->input->post('status'), ['active','inactive'] ) ) ? $this->input->post('status') : 'active',
			];

			if ( $this->input->post('active_option') == 'forever' ) {
				$data_insert['start_date'] = null;
				$data_insert['end_date'] = null;
			}

			if ( $this->input->post('active_option') == 'start-date' && $this->input->post('datepicker') ) {
            	$start_date = date_format(date_create_from_format('d-m-Y', $this->input->post('datepicker')), 'Y-m-d');

				$data_insert['start_date'] = $start_date;
				$data_insert['end_date'] = null;
			}

			if ( $this->input->post('active_option') == 'periode' && $this->input->post('daterange') ) {
				$selected_date = explode(" - ", $this->input->post('daterange'));

            	$start_date = date_format(date_create_from_format('d/m/Y', $selected_date[0]), 'Y-m-d');
            	$end_date = date_format(date_create_from_format('d/m/Y', $selected_date[1]), 'Y-m-d');

				$data_insert['start_date'] = $start_date;
				$data_insert['end_date'] = $end_date;
			}

			json_generator( (SliderModel::create( $data_insert )) ? ['status' => 'success'] : [] );
		}
	}

	public function delete(){
		$this->load->library( 'form_validation' );

		$this->form_validation->set_rules( 'id', 'id', 'required' );
		if ($this->form_validation->run() === TRUE && $slider = SliderModel::find( $this->input->post( 'id' ) )){
			delete_image_file( 'slider', $slider->image );
			json_generator( $slider->forceDelete() );
		}
	}

	public function edit( $id = null ){
		if ( $id && $slider = SliderModel::find($id) ) {
			$data = [
				'page'			=> 'admin-slider-edtit',
				'page_title'	=> 'Slider Edit',
				'slider'		=> $slider,
				'scripts'		=> [
					'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js',
					base_url('node_modules/sweetalert2/dist/sweetalert2.all.min.js'),
					'https://cdn.jsdelivr.net/momentjs/latest/moment.min.js',
					'https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js',
					base_url('node_modules/gijgo/js/gijgo.min.js'),
					base_url( 'assets/js/app/admin/slider/slider-new.js' ),
				],
				'styles'		=> [
					'https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css',
					base_url('node_modules/gijgo/css/gijgo.min.css'),
					'https://cdnjs.cloudflare.com/ajax/libs/fastselect/0.7.3/fastselect.min.css',
				]
			];

			$this->load->view('template-part/admin/header', $data);
			$this->load->view('template-part/admin/sidebar', $data);
			$this->load->view('app/admin/slider/slider-edit', $data);
			$this->load->view('template-part/admin/footer', $data);
		}
	}

	public function update(){
		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'id', 'id', 'required' );
		$this->form_validation->set_rules( 'title1', 'title1', 'required' );
		$this->form_validation->set_rules( 'active_option', 'active_option', 'required' );
		$this->form_validation->set_rules( 'status', 'status', 'required' );
		// $this->form_validation->set_rules( 'title2', 'title2', 'required' );
		if ($this->form_validation->run() === TRUE && $slider = SliderModel::find($this->input->post('id'))){

			$data_update = [
				'title1'	=> $this->input->post('title1'),
				'title2'	=> ($this->input->post('title2')) ? $this->input->post('title2') : '',
				'link'		=> $this->input->post('link'),
				'status'	=> ( in_array( $this->input->post('status'), ['active','inactive'] ) ) ? $this->input->post('status') : 'active',
			];

			if ( !$this->input->post('current_image') ) {
				delete_image_file( 'slider', $slider->image );
				
				if ( $_FILES['image']['name'] ) {
					$data_update['image'] = image_uploader( 'slider', md5( date('y-m-d-h-i-s') ), 'image' );
				} else {
					$data_update['image'] = null;
				}
			}

			if ( $this->input->post('active_option') == 'forever' ) {
				$data_update['start_date'] = null;
				$data_update['end_date'] = null;
			}

			if ( $this->input->post('active_option') == 'start-date' && $this->input->post('datepicker') ) {
            	$start_date = date_format(date_create_from_format('d-m-Y', $this->input->post('datepicker')), 'Y-m-d');

				$data_update['start_date'] = $start_date;
				$data_update['end_date'] = null;
			}

			if ( $this->input->post('active_option') == 'periode' && $this->input->post('daterange') ) {
				$selected_date = explode(" - ", $this->input->post('daterange'));

            	$start_date = date_format(date_create_from_format('d/m/Y', $selected_date[0]), 'Y-m-d');
            	$end_date = date_format(date_create_from_format('d/m/Y', $selected_date[1]), 'Y-m-d');

				$data_update['start_date'] = $start_date;
				$data_update['end_date'] = $end_date;
			}

			json_generator( ($slider->update( $data_update )) ? ['status' => 'success'] : [] );
		}
	}
}