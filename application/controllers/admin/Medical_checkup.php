<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Medical_checkup extends CI_Controller {
	public function __construct(){
		parent::__construct();
		if ( !$this->ion_auth->is_admin() ) {
			redirect(base_url('auth/login'), 'refresh');
		}
	}

	public function list(){
		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'draw', 'draw', 'required' );
		if ($this->form_validation->run() === TRUE){

			$this->load->model( 'datatables' );
			
			$selected_column = array(
				"name"			=> "concat(first_name,'  ',COALESCE(last_name,''))",
				"corporate"		=> '(SELECT name FROM corporate WHERE id=(SELECT corporate_id FROM corporate_member WHERE user_id=users.id))',
				"latest_mcu_at"	=> '(SELECT created_at FROM medical_checkup WHERE user_id=users.id ORDER BY id desc LIMIT 1)',
				"mcu_total"		=> '(SELECT count(*) FROM medical_checkup WHERE user_id=users.id)',
				"id"			=> 'id',
				'first_name'	=> 'first_name',
				'last_name'		=> 'last_name',
				'username'		=> 'username',
				'email'			=> 'email',
			);

			$column_order = array( null, 'first_name', 'corporate','latest_mcu_at','mcu_total' );
			$column_search = array( 'first_name','last_name','username','email' );
			$order = array( 'id' => 'desc' );

			$where = ['id !=' => $this->ion_auth->user()->row()->id];

		   	$custom_column 	= 	'
	              				<a class="btn btn-success btn-sm btn-detail"><i class="fas fa-list"></i> Detail MCU</a>
	              				<a class="btn btn-info btn-sm btn-new"><i class="fas fa-plus"></i> Add new MCU</a>
	              				';

			$this->datatables->get_datatables( 'users', $selected_column, $column_order, $column_search, $order, $where, $custom_column );
		

		} else {
			$data = [
				'page'			=> 'admin-mcu-list',
				'page_title'	=> 'Medical Check Up',
				'scripts'		=> [
					// datatable
					'https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js',
					'https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js',
					'https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js',
					'https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js',

					// jquery validate
					'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js',

					// sweetalert2
					base_url('node_modules/sweetalert2/dist/sweetalert2.all.min.js'),

					// fastselect
					'https://cdnjs.cloudflare.com/ajax/libs/fastselect/0.7.3/fastselect.standalone.min.js',

					// app
					base_url( 'assets/js/app/admin/medical_checkup/mcu-list.js' ),
				],
				'styles'		=> [
					// datatable
					'https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css',
					'https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap4.min.css',

					// fastselect
					'https://cdnjs.cloudflare.com/ajax/libs/fastselect/0.7.3/fastselect.min.css',
				]
			];


			$this->load->view('template-part/admin/header', $data);
			$this->load->view('template-part/admin/sidebar', $data);
			$this->load->view('app/admin/medical_checkup/mcu-list', $data);
			$this->load->view('template-part/admin/footer', $data);
		}
	}

	public function new( $id = null ){

		if ( $id && $user = UserModel::find($id) ) {
			$data = [
				'user_id'	=> $id,
				'scripts'	=> [
					'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js',
					base_url('node_modules/sweetalert2/dist/sweetalert2.all.min.js'),
					base_url( 'assets/js/app/admin/medical_checkup/mcu-new.js' ),
				]
			];

			$this->load->view('template-part/admin/header', $data);
			$this->load->view('template-part/admin/sidebar', $data);
			$this->load->view('app/admin/medical_checkup/mcu-new', $data);
			$this->load->view('template-part/admin/footer', $data);
		}
	}

	public function save(){
		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'user_id', 'user_id', 'required' );
		if ($this->form_validation->run() === TRUE && $user = UserModel::find( $this->input->post('user_id') ) ){
			$data_insert = [
				'wee'					=> $this->input->post('wee'),
				'anamnesis'				=> $this->input->post('anamnesis'),
				'physical_examination'	=> $this->input->post('physical_examination'),
				'lab_result'			=> $this->input->post('lab_result'),
				'support_examination'	=> $this->input->post('support_examination'),
				'fitness_status'		=> $this->input->post('fitness_status'),
				'occupation'			=> $this->input->post('occupation'),
			];

			$result = $user->medical_checkups()->create( $data_insert );
			json_generator( $result );
		}
	}

	public function detail( $id ){
		if ( $id && $user = UserModel::find($id) ) {

			$data = [
				'user'		=> $user,
				'scripts'	=> [
					// datatable
					'https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js',
					'https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js',
					'https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js',
					'https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js',
					base_url( 'assets/js/app/admin/medical_checkup/mcu-detail.js' ),
				],
				'styles'		=> [
					// datatable
					'https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css',
					'https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap4.min.css',
				]
			];

			$this->load->view('template-part/admin/header', $data);
			$this->load->view('template-part/admin/sidebar', $data);
			$this->load->view('app/admin/medical_checkup/mcu-detail', $data);
			$this->load->view('template-part/admin/footer', $data);
		}
	}

	public function get( $user_id ){
		if ( $user_id && $user = UserModel::find($user_id) ) {
			$this->load->library( 'form_validation' );
			$this->form_validation->set_rules( 'draw', 'draw', 'required' );
			if ($this->form_validation->run() === TRUE){
				$this->load->model( 'datatables' );
				
				$selected_column = array(
					'created_at'	=> 'created_at',
					"id"			=> 'id',
				);

				$column_order = array( null, 'created_at' );
				$column_search = array( 'created_at' );
				$order = array( 'id' => 'desc' );

				$where = ['user_id' => $user_id];

			   	$custom_column 	= 	'
		              				<a class="btn btn-success btn-sm btn-detail"><i class="fas fa-list"></i> Details</a>
		              				<button class="btn btn-danger btn-sm btn-delete"><i class="fas fa-plus"></i> Delete</button>
		              				';

				$this->datatables->get_datatables( 'medical_checkup', $selected_column, $column_order, $column_search, $order, $where, $custom_column );
			
			}
		}
	}
}