<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends CI_Controller {
	public function __construct(){
		parent::__construct();
		if ( !$this->ion_auth->is_admin() ) {
			redirect(base_url('auth/login'), 'refresh');
			$this->load->model( 'TransactionModel', 'UserModel' );
		}
	}

	public function index(){
		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'draw', 'draw', 'required' );
		if ($this->form_validation->run() === TRUE){
			$tablename = 'transaction';
			$this->load->model( ['datatables','TransactionPaymentModel','TransactionModel'] );
			$selected_column = array(
				"user"			=> "(SELECT first_name FROM users WHERE id=transaction.user_id)",
				"payment_status"=> '(SELECT status FROM transaction_payment WHERE id=transaction.transaction_payment_id)',
				"status"        => 'status',
				"tracking_code" => 'tracking_code',
				"created_at"	=> 'created_at',
				"id"			=> 'id',
			);

			$column_order = array( null, 'user', 'payment_status', 'status', 'tracking_code', 'created_at' );
			$column_search = array( 'payment_status', 'created_at' );
			$order = array( 'id' => 'desc' );

			// get unpaid transaction
			$payment_ids = TransactionPaymentModel::whereIn('status',['waiting','unpaid'])->pluck('id');
			$unpaid_transactions = TransactionModel::whereIn( 'transaction_payment_id', $payment_ids )->get();

			$unpaid_transaction_payment_id = [];
			$all_unpaid_transaction_id = [];
			$showed_unpaid_transaction_id = [];

			foreach ($unpaid_transactions as $key => $unpaid_transaction) {

				if ( !in_array($unpaid_transaction->transaction_payment_id, $unpaid_transaction_payment_id) ) {
					$unpaid_transaction_payment_id[] = $unpaid_transaction->transaction_payment_id;
					$showed_unpaid_transaction_id[] = $unpaid_transaction->id;
				}

				$all_unpaid_transaction_id[] = $unpaid_transaction->id;
			}
			$hidden_unpaid_transaction_id = array_diff($all_unpaid_transaction_id, $showed_unpaid_transaction_id);

			if ( $hidden_unpaid_transaction_id ) {
				$where = [
					'id'	=> [
						'method'	=> 'where_not_in',
						'data'		=> $hidden_unpaid_transaction_id
					]
				];
			} else {
				$where = ['is_preorder' => 'no'];
			}

		   	$custom_column 	= '
		   		<a class="btn btn-success btn-sm btn-detail">Detail</a>
		   	';
			$this->datatables->get_datatables( $tablename, $selected_column, $column_order, $column_search, $order, $where, $custom_column );

		} else {
			$data = [
				'page'			=> 'admin-order',
				'page_title'	=> 'Order',
				'scripts'		=> [
					// datatable
					'https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js',
					'https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js',
					'https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js',
					'https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js',

					// jquery validate
					'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js',

					// sweetalert2
					base_url('node_modules/sweetalert2/dist/sweetalert2.all.min.js'),

					'https://cdnjs.cloudflare.com/ajax/libs/fastselect/0.7.3/fastselect.standalone.min.js',

					// app
					base_url( 'assets/js/app/admin/order/order/order-list.js' ),
				],
				'styles'		=> [
					// datatable
					'https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css',
					'https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap4.min.css',

					'https://cdnjs.cloudflare.com/ajax/libs/fastselect/0.7.3/fastselect.min.css',
				]
			];

			$this->load->view('template-part/admin/header', $data);
			$this->load->view('template-part/admin/sidebar', $data);
			$this->load->view('app/admin/order/order-list', $data);
			$this->load->view('template-part/admin/footer', $data);
		}
	}

	public function waiting_confirmation(){
		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'draw', 'draw', 'required' );
		if ($this->form_validation->run() === TRUE){
			$tablename = 'transaction';
			$this->load->model( 'datatables' );
			$selected_column = array(
				"user"			=> "(SELECT first_name FROM users WHERE id=transaction.user_id)",
				"payment_method"=> '(SELECT name FROM bank WHERE id=(SELECT payment_method FROM transaction_payment WHERE id=transaction.transaction_payment_id))',
				"total"			=> '(
										(SELECT SUM(shipping_fee) FROM transaction t2 WHERE t2.transaction_payment_id=transaction.transaction_payment_id)
										+ (SELECT unique_code FROM transaction_payment WHERE id=transaction.transaction_payment_id)
										+ (
											select sum(price*quantity) FROM transaction_item WHERE transaction_id IN (
												SELECT id FROM transaction t3 WHERE t3.transaction_payment_id=transaction.transaction_payment_id
											)
										  )
										- (IFNULL( (select nominal FROM voucher WHERE id=transaction.voucher_id),0 ))
									)',
				"updated_at"	=> 'updated_at',
				"id"			=> 'id',
			);

			$column_order = array( null, 'payment_status', 'updated_at' );
			$column_search = array( 'payment_status', 'updated_at' );
			$order = array( 'id' => 'desc' );

			// get unpaid transaction
			$payment_ids = TransactionPaymentModel::whereIn('status',['waiting'])->pluck('id');
			$unpaid_transactions = TransactionModel::whereIn( 'transaction_payment_id', $payment_ids )->get();

			$unpaid_transaction_payment_id = [];
			$all_unpaid_transaction_id = [];
			$showed_unpaid_transaction_id = [];

			foreach ($unpaid_transactions as $key => $unpaid_transaction) {

				if ( !in_array($unpaid_transaction->transaction_payment_id, $unpaid_transaction_payment_id) ) {
					$unpaid_transaction_payment_id[] = $unpaid_transaction->transaction_payment_id;
					$showed_unpaid_transaction_id[] = $unpaid_transaction->id;
				}

				$all_unpaid_transaction_id[] = $unpaid_transaction->id;
			}
			$hidden_unpaid_transaction_id = array_diff($all_unpaid_transaction_id, $showed_unpaid_transaction_id);

			if ( $showed_unpaid_transaction_id ) {
				$where = ['id'	=> $showed_unpaid_transaction_id ];
			} else {
				$where = ['id' => 9999999999999999999999999999999999999];
			}
		   	$custom_column 	= '
		   	<button data-action="approve" class="btn btn-sm btn-primary btn-action">Approve</button>
		   	<button data-action="reject" class="btn btn-sm btn-secondary btn-action">Reject</button>
		   	<a class="btn btn-success btn-sm btn-detail">Detail</a>
		   	';
			$this->datatables->get_datatables( $tablename, $selected_column, $column_order, $column_search, $order, $where, $custom_column );

		} else {
			$data = [
				'page'			=> 'admin-order-waiting-confirmation',
				'page_title'	=> 'Order',
				'scripts'		=> [
					// datatable
					'https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js',
					'https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js',
					'https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js',
					'https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js',

					// jquery validate
					'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js',

					// sweetalert2
					base_url('node_modules/sweetalert2/dist/sweetalert2.all.min.js'),

					'https://cdnjs.cloudflare.com/ajax/libs/fastselect/0.7.3/fastselect.standalone.min.js',

					// app
					base_url( 'assets/js/app/admin/order/order/order-waiting-confirmation.js' ),
				],
				'styles'		=> [
					// datatable
					'https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css',
					'https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap4.min.css',

					'https://cdnjs.cloudflare.com/ajax/libs/fastselect/0.7.3/fastselect.min.css',
				]
			];

			$this->load->view('template-part/admin/header', $data);
			$this->load->view('template-part/admin/sidebar', $data);
			$this->load->view('app/admin/order/order-waiting-confirmation', $data);
			$this->load->view('template-part/admin/footer', $data);
		}
	}

	public function approval_payment(){
		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'id', 'id', 'required' );
		$this->form_validation->set_rules( 'action', 'action', 'required' );
		if ($this->form_validation->run() === TRUE && $transaction = TransactionModel::find( $this->input->post('id') )  ){

			$status = ($this->input->post('action') == 'approve') ? 'paid' : 'failed';

			$transactions = TransactionModel::where('transaction_payment_id', $transaction->transaction_payment_id);

			$is_paid = $transaction->payment->update(
				[
					'status'	=> 'paid'
				]
			);

			$transactions->update(
				[
					'status'	=> 'pengemasan',
				]
			);

			// delete voucher user if paid
			// if(!empty($transaction->voucher_id)) {
			// 	$this->db->query("DELETE FROM voucher_user WHERE  voucher_id='$transaction->voucher_id' and user_id = '$transaction->user_id'");
			// }
			json_generator( $is_paid );
		}
	}

	public function detail( $id ){
		if ( $id && $transaction = TransactionModel::find($id) ) {
			$this->load->model( 'VoucherModel', 'WarehouseModel' );

			$transactionItem = $transaction->transaction_items()->first();
		    $warehouseData = WarehouseModel::where( 'id', $transactionItem->warehouse_id )->first();
			$data = [
				'transaction'	=> $transaction,
				'warehouse'		=> $warehouseData,
				'page'			=> 'admin-order-detail',
				'scripts'		=> [
					'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js',
					base_url('node_modules/sweetalert2/dist/sweetalert2.all.min.js'),
					base_url( 'assets/js/app/admin/order/order-detail.js' ),
				]
			];
			$this->load->view('template-part/admin/header', $data);
			$this->load->view('template-part/admin/sidebar', $data);
			$this->load->view('app/admin/order/order-detail', $data);
			$this->load->view('template-part/admin/footer', $data);
		} else {
			redirect(base_url('admin/order'), 'refresh');
		}
	}

	public function update_tracking_code(){
		$this->load->library('form_validation');
		$this->load->library('my_email');
		$this->form_validation->set_rules( 'id', 'id', 'required' );
		$this->form_validation->set_rules( 'tracking_code', 'tracking_code', 'required' );
		$this->form_validation->set_rules( 'delivery_service', 'delivery_service', 'required' );
		if ($this->form_validation->run() === TRUE && $transaction = TransactionModel::find( $this->input->post('id') )  ){

			$result = $transaction->update([
				'tracking_code'	=> $this->input->post('tracking_code'),
				'delivery_service'	=> $this->input->post('delivery_service'),
				'status'		=> 'in delivery',
				'is_preorder'	=> 'no',
			]);

			$email = $transaction->user->email;

			$data = [
				'transaction_detail'	=> $transaction,
				'email'	=> (string) $email,
			];

			$this->my_email->order_approve($data);

			json_generator($result);
		}
	}

	public function approval()
	{
		$type = $this->input->post('type');

		$transaction = TransactionModel::find($this->input->post('id'));

		if ($type == 'approve') {
			$transaction_payment = TransactionPaymentModel::find($transaction->transaction_payment_id);

			$transaction->payment_status = 'paid';
			$transaction->status = 'pengemasan';
			$transaction->save();

			$transaction_payment->status = 'paid';
			$transaction_payment->save();

			json_generator('success');
		}
	}
}