<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Preorder extends CI_Controller {
	public function __construct(){
		parent::__construct();
		if ( !$this->ion_auth->is_admin() ) {
			redirect(base_url('auth/login'), 'refresh');
			$this->load->model( 'WishOrderModel' );
		}
	}

	public function index(){
		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'draw', 'draw', 'required' );
		if ($this->form_validation->run() === TRUE){
			$tablename = 'transaction';
			$this->load->model( 'datatables' );
			$selected_column = array(
				"user"			=> "(SELECT first_name FROM users WHERE id=transaction.user_id)",
				"payment_status"=> '(SELECT status FROM transaction_payment WHERE id=transaction.transaction_payment_id)',
				"created_at"	=> 'created_at',
				"id"			=> 'id',
			);

			$column_order = array( null, 'payment_status', 'created_at' );
			$column_search = array( 'payment_status', 'created_at' );
			$order = array( 'id' => 'desc' );

			$where = ['is_preorder' => 'yes'];
			$having = ['payment_status' => 'paid'];

		   	$custom_column 	= '
		   		<a class="btn btn-success btn-sm btn-detail">Detail</a>
		   	';
			$this->datatables->get_datatables( $tablename, $selected_column, $column_order, $column_search, $order, $where, $custom_column, null, $having );

		} else {
			$data = [
				'page'			=> 'admin-order-preorder',
				'page_title'	=> 'Preorder',
				'scripts'		=> [
					// datatable
					'https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js',
					'https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js',
					'https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js',
					'https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js',

					// jquery validate
					'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js',

					// sweetalert2
					base_url('node_modules/sweetalert2/dist/sweetalert2.all.min.js'),

					'https://cdnjs.cloudflare.com/ajax/libs/fastselect/0.7.3/fastselect.standalone.min.js',

					// app
					base_url( 'assets/js/app/admin/order/preorder/preorder-list.js' ),
				],
				'styles'		=> [
					// datatable
					'https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css',
					'https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap4.min.css',
					
					'https://cdnjs.cloudflare.com/ajax/libs/fastselect/0.7.3/fastselect.min.css',
				]
			];

			$this->load->view('template-part/admin/header', $data);
			$this->load->view('template-part/admin/sidebar', $data);
			$this->load->view('app/admin/order/preorder-list', $data);
			$this->load->view('template-part/admin/footer', $data);
		}
	}
}