<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wishorder extends CI_Controller {
	public function __construct(){
		parent::__construct();
		if ( !$this->ion_auth->is_admin() ) {
			redirect(base_url('auth/login'), 'refresh');
			$this->load->model( 'WishOrderModel' );
		}
	}

	public function index(){
		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'draw', 'draw', 'required' );
		if ($this->form_validation->run() === TRUE){
			$tablename = 'wish_order';
			$this->load->model( 'datatables' );
			$selected_column = array(
				"user"			=> "(SELECT first_name FROM users WHERE id=wish_order.user_id)",
				"name"			=> 'name',
				"price"			=> 'price',
				"quantity"		=> 'quantity',
				"unit"			=> 'unit',
				"created_at"	=> 'created_at',
				"product_id"	=> 'product_id',
				"id"			=> 'id',
			);

			$column_order = array( null, 'name', 'quantity', 'unit' );
			$column_search = array( 'name', 'quantity', 'unit' );
			$order = array( 'id' => 'desc' );

			$where = null;

		   	$custom_column 	= '<button class="btn btn-sm btn-info btn-link-product">Link to product</button>';
			$this->datatables->get_datatables( $tablename, $selected_column, $column_order, $column_search, $order, $where, $custom_column );

		} else {
			$data = [
				'page'			=> 'admin-order-wishorder',
				'page_title'	=> 'Wishorder',
				'scripts'		=> [
					// datatable
					'https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js',
					'https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js',
					'https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js',
					'https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js',

					// jquery validate
					'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js',

					// sweetalert2
					base_url('node_modules/sweetalert2/dist/sweetalert2.all.min.js'),

					'https://cdnjs.cloudflare.com/ajax/libs/fastselect/0.7.3/fastselect.standalone.min.js',

					// app
					base_url( 'assets/js/app/admin/order/wishorder/wishorder-list.js' ),
				],
				'styles'		=> [
					// datatable
					'https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css',
					'https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap4.min.css',
					
					'https://cdnjs.cloudflare.com/ajax/libs/fastselect/0.7.3/fastselect.min.css',
				]
			];

			$this->load->view('template-part/admin/header', $data);
			$this->load->view('template-part/admin/sidebar', $data);
			$this->load->view('app/admin/order/wishorder-list', $data);
			$this->load->view('template-part/admin/footer', $data);
		}
	}

	public function link_to_product(){
		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'id', 'id', 'required' );
		$this->form_validation->set_rules( 'product_id', 'product_id', 'required' );
		if ($this->form_validation->run() === TRUE){
			$wishorder_update = WishOrderModel::find( $this->input->post('id') )->update( ['product_id' => $this->input->post('product_id')] );
			json_generator( $wishorder_update );
		}
	}
}