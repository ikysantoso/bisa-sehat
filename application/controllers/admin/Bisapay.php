<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bisapay extends CI_Controller {
	public function __construct(){
		parent::__construct();
		if ( !$this->ion_auth->is_admin() ) {
			redirect(base_url('auth/login'), 'refresh');
		}
	}

	public function list(){
		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'draw', 'draw', 'required' );
		if ($this->form_validation->run() === TRUE){
			$tablename = 'bisapay_topup';
			$this->load->model( 'datatables' );
			$selected_column = array(
				"user"			=> "(SELECT CONCAT(first_name, ' ',last_name) FROM users WHERE id=(SELECT user_id FROM bisapay WHERE id=(SELECT bisapay_id FROM bisapay_logs WHERE id=".$tablename.".bisapay_log_id)))",
				"nominal"		=> "(SELECT nominal FROM bisapay_logs WHERE id=".$tablename.".bisapay_log_id)",
				"kodeunik"		=> "(SELECT unique_number FROM bisapay_logs WHERE id=".$tablename.".bisapay_log_id)",
				"bank"			=> "(SELECT name FROM bank WHERE id=".$tablename.".bank_id)",
				"created_at"	=> "(SELECT created_at FROM bisapay_logs WHERE id=".$tablename.".bisapay_log_id)",
				"status"		=> "(SELECT status FROM bisapay_logs WHERE id=".$tablename.".bisapay_log_id)",
				"id"			=> 'id',
			);

			$column_order = array( null, null, 'nominal', 'kodeunik', 'bank', 'created_at', 'status' );
			$column_search = array( null );
			$order = array( 'id' => 'desc' );

			if ( $this->input->post('status') && $this->input->post('status') != 'all' ) {
				$having = ['status' => $this->input->post('status')];
			} else {
				$having = null;
			}

			$where = null;

		   	$custom_column 	= null;
			$this->datatables->get_datatables( $tablename, $selected_column, $column_order, $column_search, $order, $where, $custom_column, null, $having );

		} else {
			$data = [
				'page'			=> 'admin-bisapay-list',
				'page_title'	=> 'Bisapay Topup List',
				'scripts'		=> [
					// datatable
					'https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js',
					'https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js',
					'https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js',
					'https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js',

					// jquery validate
					'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js',

					// sweetalert2
					base_url('node_modules/sweetalert2/dist/sweetalert2.all.min.js'),

					// app
					base_url( 'assets/js/app/admin/bisapay/bisapay-list.js' ),
				],
				'styles'		=> [
					// datatable
					'https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css',
					'https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap4.min.css',
				]
			];

			$this->load->view('template-part/admin/header', $data);
			$this->load->view('template-part/admin/sidebar', $data);
			$this->load->view('app/admin/bisapay/bisapay-list', $data);
			$this->load->view('template-part/admin/footer', $data);
		}
	}

	public function topup_detail()
	{
		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'id', 'id', 'required' );
		if ($this->form_validation->run() === TRUE){
			$this->load->model( 'BisapayTopupModel' );

			$result = BisapayTopupModel::where( 'id', $this->input->post('id') )->first();

			$_result = $result;
			$_result['bank'] = $result->bank;
			$_result['log']	= $result->log;
			$_result['log']['bisapay'] = $result->log->bisapay;
			$_result['log']['bisapay']['user'] = $result->log->bisapay->user;

			json_generator( $_result );
		}
	}

	public function topup_approval()
	{
		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'id', 'id', 'required' );
		$this->form_validation->set_rules( 'approval', 'approval', 'required' );

		if ($this->form_validation->run() === TRUE){
			$this->load->library('my_email');

			$status = ($this->input->post('approval')=='approve') ? 'success' : 'failed';

			$this->load->model( 'BisapayTopupModel' );
			$topup = BisapayTopupModel::find( $this->input->post('id') );

			$result = $topup->log()->update([
				'status'	=> $status
			]);

			$mail_address = $topup->log->bisapay->user->email;

			$data = [
				'topup_detail'	=> $topup,
				'name'	        => $this->ion_auth->user()->row()->username,
			    'email'	        => $this->ion_auth->user()->row()->email,
				'mail_address'	=> (string) $mail_address,
			];

			$this->my_email->bisapay_topup_mail($data);

			json_generator($result);
		}
	}

	public function withdraw(){
		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'draw', 'draw', 'required' );
		if ($this->form_validation->run() === TRUE){
			$tablename = 'bisapay_withdraw';
			$this->load->model( 'datatables' );
			$selected_column = array(
				"user"		=> "(SELECT CONCAT(first_name,' ',last_name) FROM users WHERE id=(SELECT user_id FROM customer_bank WHERE id=bisapay_withdraw.customer_bank_id))",
				"nominal"	=> "(SELECT nominal FROM bisapay_logs WHERE id=bisapay_withdraw.bisapay_log_id)",
				"tujuan"	=> "(SELECT CONCAT(name, ' - ', account_number, '<br/>', account_name) FROM customer_bank WHERE id=bisapay_withdraw.customer_bank_id)",
				"date"		=> "created_at",
				"status"	=> "(SELECT status FROM bisapay_logs WHERE id=bisapay_withdraw.bisapay_log_id)",
				"id"		=> 'id',
			);

			$column_order = array( null, 'name', 'price' );
			$column_search = array( 'name', 'price' );
			$order = array( 'id' => 'desc' );

			$where = null;

			if ( $this->input->post('status') ) {
				if ( $this->input->post('status') == 'all' ) {
					$having = null;
				} else {
					$having = "status = '".$this->input->post('status')."'";
				}

			} else {
				$having = "status = 'waiting'";
			}

		   	$custom_column 	= null;
			$this->datatables->get_datatables( $tablename, $selected_column, $column_order, $column_search, $order, $where, $custom_column, null, $having );

		} else {
			$data = [
				'page'			=> 'admin-bisapay-withdraw',
				'page_title'	=> 'Bisapay Tarik Tunai',
				'scripts'		=> [
					// datatable
					'https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js',
					'https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js',
					'https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js',
					'https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js',

					// jquery validate
					'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js',

					// sweetalert2
					base_url('node_modules/sweetalert2/dist/sweetalert2.all.min.js'),

					// app
					base_url( 'assets/js/app/admin/bisapay/bisapay-withdraw-list.js' ),
				],
				'styles'		=> [
					// datatable
					'https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css',
					'https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap4.min.css',
				]
			];

			$this->load->view('template-part/admin/header', $data);
			$this->load->view('template-part/admin/sidebar', $data);
			$this->load->view('app/admin/bisapay/bisapay-withdraw-list', $data);
			$this->load->view('template-part/admin/footer', $data);
		}
	}

	public function withdraw_approval(){
		$this->load->library('form_validation');
		$this->load->library('my_email');
		$this->load->model( 'BisapayWithdrawModel' );

		$this->form_validation->set_rules( 'id', 'id', 'required' );
		$this->form_validation->set_rules( 'status', 'status', 'required' );
		if ($this->form_validation->run() === TRUE && $withdraw = BisapayWithdrawModel::find( $this->input->post('id') )){

			$result = $withdraw->bisapay_log()->update(['status' => 'success']);
			
			$mail_address = $withdraw->bisapay_log->bisapay->user->email;

			$data = [
				'withdraw_detail' => $withdraw,
				'mail_address'	  => (string) $mail_address,
			];

			$this->my_email->bisapay_withdraw_mail($data);

			json_generator( $result );
		}
	}

	public function logs(){
		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'draw', 'draw', 'required' );
		if ($this->form_validation->run() === TRUE){
			$tablename = 'bisapay_logs';
			$this->load->model( 'datatables' );
			$selected_column = array(
				"user"			=> "(SELECT CONCAT(first_name, ' ',last_name) FROM users WHERE id=(SELECT user_id FROM bisapay WHERE id=".$tablename.".bisapay_id ))",
				"nominal"		=> "nominal",
				"log_type"		=> "log_type",
				"log_type_description"	=> "log_type_description",
				"status"		=> "status",
				"created_at"	=> "created_at",
				"id"			=> 'id',
			);

			$column_order = array( null, null, 'nominal', 'log_type', 'log_type_description', 'status', 'created_at' );
			$column_search = array( 'nominal', 'log_type', 'log_type_description', 'status', 'created_at' );
			$order = array( 'id' => 'desc' );

			$where = null;

		   	$custom_column 	= null;
			$this->datatables->get_datatables( $tablename, $selected_column, $column_order, $column_search, $order, $where, $custom_column );

		} else {
			$data = [
				'page'			=> 'admin-bisapay-logs',
				'page_title'	=> 'Bisapay Logs',
				'scripts'		=> [
					// datatable
					'https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js',
					'https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js',
					'https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js',
					'https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js',

					// jquery validate
					'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js',

					// sweetalert2
					base_url('node_modules/sweetalert2/dist/sweetalert2.all.min.js'),

					// app
					base_url( 'assets/js/app/admin/bisapay/bisapay-logs.js' ),
				],
				'styles'		=> [
					// datatable
					'https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css',
					'https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap4.min.css',
				]
			];

			$this->load->view('template-part/admin/header', $data);
			$this->load->view('template-part/admin/sidebar', $data);
			$this->load->view('app/admin/bisapay/bisapay-logs', $data);
			$this->load->view('template-part/admin/footer', $data);
		}
	}

	public function lock(){
		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'draw', 'draw', 'required' );
		if ($this->form_validation->run() === TRUE){
			$tablename = 'bisapay_logs';
			$this->load->model( 'datatables' );
			$selected_column = array(
				"user"			=> "(SELECT CONCAT(first_name, ' ',last_name) FROM users WHERE id=(SELECT user_id FROM bisapay WHERE id=".$tablename.".bisapay_id ))",
				"nominal"		=> "nominal",
				"log_type_description"	=> "log_type_description",
				"created_at"	=> "created_at",
				"id"			=> 'id',
			);

			$column_order = array( null, null, 'nominal', 'log_type', 'log_type_description', 'status', 'created_at' );
			$column_search = array( 'nominal', 'log_type', 'log_type_description', 'status', 'created_at' );
			$order = array( 'id' => 'desc' );

			$where = ['log_type' => 'lock'];

		   	$custom_column 	= null;
			$this->datatables->get_datatables( $tablename, $selected_column, $column_order, $column_search, $order, $where, $custom_column );

		} else {
			$data = [
				'page'			=> 'admin-bisapay-lock',
				'page_title'	=> 'Lock Saldo',
				'scripts'		=> [
					// datatable
					'https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js',
					'https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js',
					'https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js',
					'https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js',

					// jquery validate
					'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js',

					// sweetalert2
					base_url('node_modules/sweetalert2/dist/sweetalert2.all.min.js'),

					'https://cdnjs.cloudflare.com/ajax/libs/fastselect/0.7.3/fastselect.standalone.min.js',

					// app
					base_url( 'assets/js/app/admin/bisapay/bisapay-lock.js' ),
				],
				'styles'		=> [
					'https://cdnjs.cloudflare.com/ajax/libs/fastselect/0.7.3/fastselect.min.css',

					// datatable
					'https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css',
					'https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap4.min.css',
				]
			];

			$this->load->view('template-part/admin/header', $data);
			$this->load->view('template-part/admin/sidebar', $data);
			$this->load->view('app/admin/bisapay/bisapay-lock', $data);
			$this->load->view('template-part/admin/footer', $data);
		}
	}

	public function ceksaldo(){

		$this->load->library( 'form_validation' );
		$this->load->model('BisapayModel');

		$this->form_validation->set_rules( 'id', 'id', 'required' );
		if ($this->form_validation->run() === TRUE && $bisapay_user = BisapayModel::where('user_id',$this->input->post('id'))->first() ) {
			json_generator( $bisapay_user->saldo() );
		}

	}

	public function lock_saldo(){
		$this->load->library( 'form_validation' );

		$this->form_validation->set_rules( 'id', 'id', 'required' );
		$this->form_validation->set_rules( 'nominal', 'nominal', 'required' );
		if ($this->form_validation->run() === TRUE){
			$this->load->model('BisapayModel');
			$bisapay = BisapayModel::where( 'user_id', $this->input->post('id') )->first();
			if ( $bisapay->saldo() >= $this->input->post('nominal') ) {

				$result = $bisapay->logs()->create([
					'nominal'				=> $this->input->post('nominal'),
					'log_type'				=> 'lock',
					'log_type_description'	=> 'lock saldo by admin',
					'status'				=> 'success',
				]);

				if ( $result ) {
					json_generator([
						'status'	=> 'success',
						'title'		=> 'Lock saldo sukses',
					]);
				}


			} else {
				json_generator([
					'status'	=> 'warning',
					'title'		=> 'Nominal melebihi saldo',
					'message'	=> 'Saldo sekarang '.rupiah($bisapay->saldo()).', tidak mencukupi untuk lock saldo sebesar '. rupiah($this->input->post('nominal')),
				]);
			}

		}
	}
}