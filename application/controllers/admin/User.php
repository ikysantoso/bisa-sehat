<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Capsule\Manager as DB;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class User extends CI_Controller {
	public function __construct(){
		parent::__construct();
		if ( $this->ion_auth->is_admin() || $this->ion_auth->is_co_admin() ) {
		} else {
			redirect(base_url('auth/login'), 'refresh');
		}
	}

	public function index(){
		redirect(base_url('admin/user/list'), 'refresh');
	}

	public function list(){
		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'draw', 'draw', 'required' );
		if ($this->form_validation->run() === TRUE){

			$this->load->model( 'datatables' );
			
			$selected_column = array(
				"name"			=> "concat(first_name,'  ',COALESCE(last_name,''))",
				"username"		=> 'username',
				"email"			=> 'email',
				"role"			=> "(SELECT description FROM `groups` WHERE id=(SELECT group_id FROM users_groups WHERE user_id=users.id LIMIT 1))",
				"active"		=> 'active',
				"id"			=> 'id',
			);

			$column_order = array( null, 'first_name', 'username', 'email', 'role', 'active' );
			$column_search = array( 'email', 'username', 'first_name', 'last_name' );
			$order = array( 'id' => 'desc' );

			if ( $this->ion_auth->is_admin() ) {
				$where = ['id !=' => $this->ion_auth->user()->row()->id];
			} else {

				// get super admin id
				$this->load->model(['GroupModel', 'UserModel']);
				$admin_id = GroupModel::where('name', 'admin')->first()->id;

				$admin_ids = UserGroupModel::distinct('user_id')->where('group_id', $admin_id)->get();
				
				$user_ids = [];
				foreach ($admin_ids as $key => $user) {
					$user_ids[] = $user->user_id;
				}

				$user_ids[] = $this->ion_auth->user()->row()->id;

				$where = [
					'id' => [
						'method'	=> 'where_not_in',
						'data'		=> $user_ids
					]
				];
			}
		   	$custom_column 	= '<a class="btn btn-warning btn-sm btn-edit"><i class="fas fa-edit"></i> Edit</a>';
			$this->datatables->get_datatables( 'users', $selected_column, $column_order, $column_search, $order, $where, $custom_column );

		} else {
			$data = [
				'page'			=> 'admin-user-list',
				'page_title'	=> 'User List',
				'scripts'		=> [
					// datatable
					'https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js',
					'https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js',
					'https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js',
					'https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js',

					// jquery validate
					'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js',

					// sweetalert2
					base_url('node_modules/sweetalert2/dist/sweetalert2.all.min.js'),

					// fastselect
					'https://cdnjs.cloudflare.com/ajax/libs/fastselect/0.7.3/fastselect.standalone.min.js',

					// app
					base_url( 'assets/js/app/admin/user/user-list.js' ),
				],
				'styles'		=> [
					// datatable
					'https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css',
					'https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap4.min.css',

					// fastselect
					'https://cdnjs.cloudflare.com/ajax/libs/fastselect/0.7.3/fastselect.min.css',
				]
			];

			$this->load->view('template-part/admin/header', $data);
			$this->load->view('template-part/admin/sidebar', $data);
			$this->load->view('app/admin/user/user-list', $data);
			$this->load->view('template-part/admin/footer', $data);
		}
	}

public function excel()
		{
			$this->load->model('UserModel');
			$spreadsheet = new Spreadsheet();
			$sheet = $spreadsheet->getActiveSheet();
			$sheet->setCellValue('A1', 'No');
			$sheet->setCellValue('B1', 'Name');
			$sheet->setCellValue('C1', 'Jenis Kelamin');
			$sheet->setCellValue('D1', 'Tanggal Lahir');
			$sheet->setCellValue('E1', 'Pekerjaan');
			$sheet->setCellValue('F1', 'Tinggi Badan');
			$sheet->setCellValue('G1', 'Berat Badan');
			$sheet->setCellValue('H1', 'BMI');
			$sheet->setCellValue('I1', 'Tekanan Darah');
			$sheet->setCellValue('J1', 'Gula Darah');
			$sheet->setCellValue('K1', 'Kolesterol');
			$sheet->setCellValue('L1', 'Asam Urat');
			$sheet->setCellValue('M1', 'Kebugaran');
			$sheet->setCellValue('N1', 'Provinsi');
			$sheet->setCellValue('O1', 'Kota/Kab');
			$sheet->setCellValue('P1', 'Kec');
			$sheet->setCellValue('Q1', 'Kel/Desa');
			
			$users = $this->db->query("SELECT * FROM users where first_name!='' ")->result();
			$no = 1;
			$x = 2;
			foreach($users as $row)
			{
				$sheet->setCellValue('A'.$x, $no++);
				$sheet->setCellValue('B'.$x, $row->first_name.' '.$row->last_name);
			$user_info=UserModel::find( $row->id );
				$gender=$user_info->user_meta('gender');
				$datebirth=$user_info->user_meta('date-birth');
				$sheet->setCellValue('C'.$x, $gender);
				$sheet->setCellValue('D'.$x, $datebirth);			
				$sheet->setCellValue('E'.$x, $user_info->user_meta('job'));			
            $medical_record = $user_info->medical_records->last(); 
			if($medical_record){ 
				$sheet->setCellValue('F'.$x, $medical_record->height);
				$sheet->setCellValue('G'.$x, $medical_record->weight);
				$sheet->setCellValue('H'.$x, $medical_record->bmi()['bmi']);
				$sheet->setCellValue('I'.$x, $medical_record->sistole.'/'.$medical_record->diastole);
				$sheet->setCellValue('J'.$x, $medical_record->blood_sugar_level);
				$sheet->setCellValue('K'.$x, $medical_record->cholesterol_level);
				$sheet->setCellValue('L'.$x, $medical_record->uric_acid_level);
			}else{
				$sheet->setCellValue('F'.$x, '');
				$sheet->setCellValue('G'.$x, '');
				$sheet->setCellValue('H'.$x, '');
				$sheet->setCellValue('I'.$x, '');
				$sheet->setCellValue('J'.$x, '');
				$sheet->setCellValue('K'.$x, '');
				$sheet->setCellValue('L'.$x, '');
			}
				$fitness = $user_info->fitnesses->last();
				$sheet->setCellValue('M'.$x, $fitness ? $fitness->mileage($datebirth,$gender)['status'] : '');
			$primary_address = CustomerAddressModel::where( ['user_id' =>$row->id, 'is_primary_address' => 'yes'] )->first();
		if($primary_address->village_id!=null){
        $village = $this->db2->query("SELECT * FROM ward as p where p.wid =$primary_address->village_id ")->row();
    	}else{
    	$village = $this->db2->query("SELECT * FROM ward as p where p.sdid =$primary_address->subdistrict_id ")->row();
    	}
		$sub_districts = $this->db2->query("SELECT * FROM sub_districts as p where p.sdid =$village->sdid ")->row();
		$districts = $this->db2->query("SELECT * FROM districts as p where p.did =$sub_districts->did ")->row();
		$province = $this->db2->query("SELECT * FROM province as p where p.pid =$districts->pid ")->row();
			if ( $primary_address ){ 
				$sheet->setCellValue('N'.$x, $province->province_name);
				$sheet->setCellValue('O'.$x, $districts->districts_name);
				$sheet->setCellValue('P'.$x, $sub_districts->sub_districts_name);
				$sheet->setCellValue('Q'.$x, $village->ward_name);
			}else{
				$sheet->setCellValue('N'.$x, '');
				$sheet->setCellValue('O'.$x, '');
				$sheet->setCellValue('P'.$x, '');
				$sheet->setCellValue('Q'.$x, '');
			}	
				$x++;
			}
			$writer = new Xlsx($spreadsheet);
			$filename = 'data-user';
			
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename="'. $filename .'.xlsx"'); 
			header('Cache-Control: max-age=0');
	
			$writer->save('php://output');
		}


	public function new(){

		$data = [
				'page'			=> 'admin-user-new',
				'page_title'	=> 'Add New User',
				'scripts'		=> [
					'https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js',

					// jquery validate
					'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js',

					// sweetalert2
					base_url('node_modules/sweetalert2/dist/sweetalert2.all.min.js'),

					// fastselect
					'https://cdnjs.cloudflare.com/ajax/libs/fastselect/0.7.3/fastselect.standalone.min.js',

					// quill editor
					'https://cdn.quilljs.com/1.3.6/quill.js',

					// app
					base_url( 'assets/js/app/admin/user/user-new.js' ),
				],
				'styles'		=> [
					'https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css',

					// fastselect
					'https://cdnjs.cloudflare.com/ajax/libs/fastselect/0.7.3/fastselect.min.css',
				
					// quill editor
					'https://cdn.quilljs.com/1.3.6/quill.snow.css',
				],
		];
		$this->load->view('template-part/admin/header', $data);
		$this->load->view('template-part/admin/sidebar', $data);
		$this->load->view('app/admin/user/user-new', $data);
		$this->load->view('template-part/admin/footer', $data);
	}

	public function edit( $id = null ){


		if ( $id && $user = $this->ion_auth->user($id)->row() ) {

			$data = [
				'page'			=> 'admin-user-new',
				'page_title'	=> 'Edit User',
				'user_detail'	=> $user,
				'scripts'		=> [
					'https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js',

					// jquery validate
					'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js',

					// sweetalert2
					base_url('node_modules/sweetalert2/dist/sweetalert2.all.min.js'),

					// fastselect
					'https://cdnjs.cloudflare.com/ajax/libs/fastselect/0.7.3/fastselect.standalone.min.js',

					// quill editor
					'https://cdn.quilljs.com/1.3.6/quill.js',

					// app
					base_url( 'assets/js/app/admin/user/user-edit.js' ),
				],
				'styles'		=> [
					'https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css',

					// fastselect
					'https://cdnjs.cloudflare.com/ajax/libs/fastselect/0.7.3/fastselect.min.css',
				
					// quill editor
					'https://cdn.quilljs.com/1.3.6/quill.snow.css',
				],
			];
			$this->load->view('template-part/admin/header', $data);
			$this->load->view('template-part/admin/sidebar', $data);
			$this->load->view('app/admin/user/user-edit', $data);
			$this->load->view('template-part/admin/footer', $data);
		}
	}

	public function save(){
		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'first_name', 'first_name', 'required' );
		$this->form_validation->set_rules( 'email', 'email', 'required' );
		$this->form_validation->set_rules( 'username', 'username', 'required' );
		$this->form_validation->set_rules( 'password', 'password', 'required' );
		$this->form_validation->set_rules( 'role', 'role', 'required' );
		if ($this->form_validation->run() === TRUE){
			$additional_data = [
				'first_name'	=> $this->input->post( 'first_name' ),
				'last_name'		=> $this->input->post( 'last_name' ),
				'username'		=> $this->input->post( 'username' ),
			];

			$identity_column = $this->config->item('identity', 'ion_auth');
			$identity =  ($identity_column === 'email') ? $this->input->post('email') : $this->input->post('username');

			if ( $result = $this->ion_auth->register(
					$identity, 
					$this->input->post('password'), 
					$this->input->post('email'), 
					$additional_data,
					[$this->input->post('role')]
				) 
			){
				json_generator( $result );
			}
		}
	}

	public function update(){
		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'id', 'id', 'required' );
		$this->form_validation->set_rules( 'first_name', 'first_name', 'required' );
		$this->form_validation->set_rules( 'email', 'email', 'required' );
		$this->form_validation->set_rules( 'username', 'username', 'required' );
		$this->form_validation->set_rules( 'role', 'role', 'required' );
		if ($this->form_validation->run() === TRUE && $user = $this->ion_auth->user( $this->input->post('id') )->row()){
			$update_data = [
				'first_name'	=> $this->input->post( 'first_name' ),
				'email'			=> $this->input->post( 'email' ),
				'username'		=> $this->input->post( 'username' ),
			];

			if ( $this->input->post('last_name') ) {
				$update_data['last_name'] = $this->input->post('last_name');
			} else {
				if ( $user->last_name && $user->last_name != "" ) {
					$update_data['last_name'] = null;
				}
			}

			$result = $this->ion_auth->update( $this->input->post('id'), $update_data );

			// update group member
			$group_check = $this->ion_auth->in_group( $this->input->post('role'), $this->input->post('id') );
			$current_group = $this->ion_auth->get_users_groups( $user->id )->row();
			if ( !$group_check ) {
				if ( isset( $current_group) ) {
					$this->ion_auth->remove_from_group( $current_group->id, $this->input->post('id') );
				}
				$this->ion_auth->add_to_group( $this->input->post('role'), $this->input->post('id') );
			}

			if ( $result ){
				json_generator( $result );
			}
		}
	}

	public function activation(){
		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'id', 'id', 'required' );
		$this->form_validation->set_rules( 'status', 'status', 'required' );
		if ($this->form_validation->run() === TRUE){
			if ( $this->input->post('status') == '1' ) {
				$result = $this->ion_auth->activate( $this->input->post('id') );
			} else {
				$result = $this->ion_auth->deactivate( $this->input->post('id') );
			}

			json_generator($result);
		}
	}

	public function delete(){
		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'id', 'id', 'required' );
		if ($this->form_validation->run() === TRUE){
			$result = $this->ion_auth->delete_user( $this->input->post('id') );
			json_generator($result);
		}
	}

	public function update_password(){
		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'id', 'id', 'required' );
		$this->form_validation->set_rules( 'password', 'password', 'required' );
		if ($this->form_validation->run() === TRUE){

			$result = $this->ion_auth->update( $this->input->post('id'), ['password' => $this->input->post('password')] );
			json_generator($result);
		}
	}

	public function corporate(){
		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'draw', 'draw', 'required' );
		if ($this->form_validation->run() === TRUE){

			$this->load->model( 'datatables' );
			
			$selected_column = array(
				"corporate"		=> '(SELECT name FROM corporate WHERE id=employee_import_logs.corporate_id)',
				"created_at"	=> 'created_at',
				"status"		=> 'status',
				"id"			=> 'id',
			);

			$column_order = array( null, 'corporate', 'created_at', 'status' );
			$column_search = array( 'status' );
			$order = array( 'id' => 'desc' );

			$where = null;

		   	$custom_column 	= 	'
	              				<a href="#" class="btn btn-info btn-sm btn-detail"><i class="fas fa-eye"></i> Lihat</a>
	              				';

			$this->datatables->get_datatables( 'employee_import_logs', $selected_column, $column_order, $column_search, $order, $where, $custom_column );
		

		} else {
			$data = [
				'page'			=> 'admin-user-corporate',
				'page_title'	=> 'Import Approval',
				'scripts'		=> [
					// datatable
					'https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js',
					'https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js',
					'https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js',
					'https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js',

					// jquery validate
					'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js',

					// sweetalert2
					base_url('node_modules/sweetalert2/dist/sweetalert2.all.min.js'),

					// fastselect
					'https://cdnjs.cloudflare.com/ajax/libs/fastselect/0.7.3/fastselect.standalone.min.js',

					// app
					base_url( 'assets/js/app/admin/user/user-corporate.js' ),
				],
				'styles'		=> [
					// datatable
					'https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css',
					'https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap4.min.css',

					// fastselect
					'https://cdnjs.cloudflare.com/ajax/libs/fastselect/0.7.3/fastselect.min.css',
				]
			];


			$this->load->view('template-part/admin/header', $data);
			$this->load->view('template-part/admin/sidebar', $data);
			$this->load->view('app/admin/user/user-corporate', $data);
			$this->load->view('template-part/admin/footer', $data);
		}
	}

	public function corporate_detail( $id ){

		$this->load->model( 'EmployeeImportModel' );

		if ( $id && $imported_log = EmployeeImportModel::find($id) ) {

			$extension = ".".pathinfo(FCPATH.'assets/imported-excel/'.$imported_log->file, PATHINFO_EXTENSION);

			$file = explode($extension,$imported_log->file);

			$this->load->library('my_excel');

			$data = [
				'page'			=> 'admin-user-corporate',
				'page_title'	=> 'Corporate User',
				'imported_log'	=> $imported_log,
				'imported_data'	=> (file_exists( FCPATH.'assets/imported-excel/'.$imported_log->file ) ) ? $this->my_excel->excel_to_json( FCPATH.'assets/imported-excel/'.$file[0], $extension ) : null,
			];

			$this->load->view('template-part/admin/header', $data);
			$this->load->view('template-part/admin/sidebar', $data);
			$this->load->view('app/admin/user/user-corporate-detail', $data);
			$this->load->view('template-part/admin/footer', $data);
		}
	}

	public function corporate_submit( $id ){
		$this->load->model( ['EmployeeImportModel', 'EmployeeInsertedlog', 'CorporateMemberModel'] );

		if ( $id && $imported_log = EmployeeImportModel::find($id) ) {

			$extension = ".".pathinfo(FCPATH.'assets/imported-excel/'.$imported_log->file, PATHINFO_EXTENSION);
			$file = explode($extension,$imported_log->file);
			$this->load->library('my_excel');
			$data = $this->my_excel->excel_to_json( FCPATH.'assets/imported-excel/'.$file[0], $extension );
			
			$insert_data = [];
			$column = [ 'A', 'B', 'C', 'D', 'E' ];

			foreach ($data as $key => $item) {

				if($key > 1){

					$error_column = [];
					foreach ($column as $_key => $column_item) {
						if ( $column_item == 'C' && user_checker( 'email', $item[ $column_item ] ) ) {
                        	$error_column[] = 'email';
                        }

						if ( $column_item == 'D' && user_checker( 'username', $item[ $column_item ] ) ) {
                        	$error_column[] = 'username';
                        }
					}

					if ( count($error_column) == 0 ) {
						$identity_column = $this->config->item('identity', 'ion_auth');
						$identity =  ($identity_column === 'email') ? $item[ 'C' ] : $item[ 'D' ];

						$additional_data = [
							'first_name'	=> $item[ 'A' ],
							'last_name'		=> $item[ 'B' ],
							'username'		=> $item[ 'D' ],
						];

						$user_id = $this->ion_auth->register(
							$identity, 
							$item[ 'E' ], 
							$item[ 'C' ], 
							$additional_data
						);

						if ( $user_id ) {
							CorporateMemberModel::create([
								'user_id'		=> $user_id,
								'corporate_id'	=> $imported_log->corporate_id
							]);
						}
					} else {
						$user_id = null;
					}

					$user_info_from_excel = [
						'first_name'	=> $item[ 'A' ],
						'last_name'		=> $item[ 'B' ],
						'email'			=> $item[ 'C' ],
						'username'		=> $item[ 'D' ],
					];

					$insert_data[] = [
						'imported_log_id'	=> $imported_log->id,
						'status'			=> ($user_id) ? 'approved' : 'rejected',
						'rejected_info'		=> (count($error_column) > 0) ? json_encode( ['user' => $user_info_from_excel,'error' => $error_column] ) : null,
						'user_id'			=> ($user_id) ? $user_id : null,
					];
				}
			}

			$result = EmployeeInsertedlog::insert( $insert_data );
			if ( $result && $imported_log->update( ['status' => 'approved'] ) ) {
				redirect(base_url('admin/user/corporate'), 'refresh');
			} else{
				redirect(base_url('admin/user/corporate_detail/'.$id), 'refresh');
			}
		}
	}

	public function unemployed_user(){

		$users = UserModel::select('id as value', DB::raw(
			"
			case 
		    	when last_name is not null	then CONCAT(first_name,' ', last_name, ' (', username, ')')
		    	when first_name is not null AND last_name is null then CONCAT(first_name,' (', username, ')')
		    	else CONCAT(username, ' (', email, ')')
		    end 
		    as text
		    "
		));
		if ( isset( $_GET['query'] ) ) {
			$users->where('first_name', 'like', '%'.$_GET['query'].'%');
			$users->where('last_name', 'like', '%'.$_GET['query'].'%');
			$users->where('username', 'like', '%'.$_GET['query'].'%');
		} 

		$this->load->model( 'CorporateMemberModel' );

		$users->whereNotIn( 'id', CorporateMemberModel::select('user_id')->get()->toArray() );
		$users->limit(15);

		json_generator( $users->get() );
	}

	public function bisapay_user(){
		$this->load->model(['UserModel','BisapayModel']);
		$user_ids = BisapayModel::select('user_id')->distinct('user_id')->where([
			'status'	=> 'active'
		])->pluck('user_id');

		$users = UserModel::selectRaw("id as value, CONCAT( COALESCE ( first_name, username ), ' ', COALESCE ( last_name, '' ) ) as text")->whereIn('id',$user_ids);

		if ( isset($_GET['query']) ) {
			$users->where('first_name', 'like', '%' . $_GET['query'] . '%')
					->orWhere('last_name', 'like', '%' . $_GET['query'] . '%')
					->orWhere('username', 'like', '%' . $_GET['query'] . '%')
					->orWhere('email', 'like', '%' . $_GET['query'] . '%');
		}

		json_generator( $users->take(5)->get() );
	}
}