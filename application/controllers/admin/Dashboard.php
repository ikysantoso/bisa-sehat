<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	public function __construct(){
		parent::__construct();
		if ( $this->ion_auth->is_admin() || $this->ion_auth->is_co_admin() ) {
		} else {
			redirect(base_url('auth/login'), 'refresh');
		}
	}

	public function index(){

		$this->load->model( ['UserGroupModel', 'CorporateModel','TransactionModel','ProductModel'] );

		$chart_data = [
			'total_member'		=> UserGroupModel::where('group_id', 2)->get()->count(),
			'total_corporate'	=> CorporateModel::all()->count(),
			'total_product'		=> ProductModel::all()->count(),
			'total_sales'		=> TransactionModel::where('is_preorder', 'no')->count(),
			'latest_order'		=> TransactionModel::take(5)->orderBy('id', 'desc')->get(),
		];

		$data = [
			'chart_data'	=> $chart_data,
			'page'			=> 'admin-dashboard',
				'scripts'		=> [
					// datatable
					'https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js',
					'https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js',
					'https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js',
					'https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js',

					// jquery validate
					'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js',

					// sweetalert2
					base_url('node_modules/sweetalert2/dist/sweetalert2.all.min.js'),

					// fastselect
					'https://cdnjs.cloudflare.com/ajax/libs/fastselect/0.7.3/fastselect.standalone.min.js',

					// app
					base_url( 'assets/js/app/admin/medical-record.js' ),
				],
				'styles'		=> [
					// datatable
					'https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css',
					'https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap4.min.css',

					// fastselect
					'https://cdnjs.cloudflare.com/ajax/libs/fastselect/0.7.3/fastselect.min.css',
				]

		];

		$this->load->view('template-part/admin/header', $data);
		$this->load->view('template-part/admin/sidebar', $data);
		$this->load->view('app/admin/dashboard', $data);
		$this->load->view('template-part/admin/footer', $data);
	}
	
	public function medical_record_approve(){
		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'id', 'id', 'required' );
		if (
			$this->form_validation->run() === TRUE
			&& $medical_record = MedicalRecordModel::where([
				'id'		=> $this->input->post('id'),
			])->first()
		){
			$data_update = [
				'status'			=> 1,
			];

			$result = $medical_record->update( $data_update );

			if ( $result ) {
				$this->session->set_flashdata('message', 'Data berhasil diupdate');
				$this->session->set_flashdata('status', 'info');
			} else {
				$this->session->set_flashdata('message', 'Data gagal diupdate');
				$this->session->set_flashdata('status', 'warning');
			}

			json_generator($result);
		}
	}
}