<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Corporate extends CI_Controller {
	public function __construct(){
		parent::__construct();
		if ( !$this->ion_auth->is_admin() ) {
			redirect(base_url('auth/login'), 'refresh');
		} else {
			$this->load->model( 'CorporateModel' );
		}
	}

	public function index(){
		redirect(base_url('admin/corporate/list'), 'refresh');
	}

	public function list(){
		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'draw', 'draw', 'required' );
		if ($this->form_validation->run() === TRUE){

			$this->load->model( 'datatables' );
			
			$selected_column = array(
				"name"			=> 'name',
				"employee"		=> "(SELECT COUNT(*) FROM corporate_member WHERE corporate_id=corporate.id)",
				"status"		=> 'status',
				"created_at"	=> 'created_at',
				"id"			=> 'id',
			);

			$column_order = array( null, 'name' );
			$column_search = array( 'name' );
			$order = array( 'id' => 'desc' );

			$where = null;

		   	$custom_column 	= 	'
		   						<button type="button" class="btn btn-danger btn-sm btn-delete"><i class="fas fa-trash"></i> Delete</button>
	              				<a class="btn btn-warning btn-sm btn-edit"><i class="fas fa-edit"></i> Edit</a>
	              				';

			$this->datatables->get_datatables( 'corporate', $selected_column, $column_order, $column_search, $order, $where, $custom_column );
		

		} else {
			$data = [
				'page'			=> 'admin-corporate-list',
				'page_title'	=> 'Data Perusahaan',
				'scripts'		=> [
					// datatable
					'https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js',
					'https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js',
					'https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js',
					'https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js',

					// jquery validate
					'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js',

					// sweetalert2
					base_url('node_modules/sweetalert2/dist/sweetalert2.all.min.js'),

					// app
					base_url( 'assets/js/app/admin/corporate/corporate-list.js' ),
				],
				'styles'		=> [
					// datatable
					'https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css',
					'https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap4.min.css',
				]
			];

			$this->load->view('template-part/admin/header', $data);
			$this->load->view('template-part/admin/sidebar', $data);
			$this->load->view('app/admin/corporate/corporate-list', $data);
			$this->load->view('template-part/admin/footer', $data);
		}
	}

	public function detail( $id = null ){
		if ( $id && $corporate = CorporateModel::find( $id ) ) {

			$data = [
				'page'			=> 'admin-corporate-list',
				'page_title'	=> 'Detail Perusahaan',
				'corporate'		=> $corporate,
				'scripts'		=> [
					'https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js',
					'https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js',
					'https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js',
					'https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js',
					base_url('node_modules/sweetalert2/dist/sweetalert2.all.min.js'),
					'https://cdnjs.cloudflare.com/ajax/libs/fastselect/0.7.3/fastselect.standalone.min.js',
					'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js',
					base_url( 'assets/js/app/admin/corporate/corporate-detail.js' ),
				],
				'styles'		=> [
					'https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css',
					'https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap4.min.css',
					'https://cdnjs.cloudflare.com/ajax/libs/fastselect/0.7.3/fastselect.min.css',
				]
			];

			$this->load->view('template-part/admin/header', $data);
			$this->load->view('template-part/admin/sidebar', $data);
			$this->load->view('app/admin/corporate/corporate-detail', $data);
			$this->load->view('template-part/admin/footer', $data);
		} else {
			$this->load->library( 'form_validation' );
			$this->form_validation->set_rules( 'id', 'id', 'required' );
			if ($this->form_validation->run() === TRUE && $corporate = CorporateModel::find( $this->input->post('id') )){
				json_generator( $corporate );
			}
		}
	}

	public function member( $id = null ){
		if ( $id ) {
			$this->load->library( 'form_validation' );
			$this->form_validation->set_rules( 'draw', 'draw', 'required' );
			if ($this->form_validation->run() === TRUE){
				$this->load->model( 'datatables' );
				
				$selected_column = array(
					"name"			=> "(SELECT CONCAT(first_name, ' ', last_name) FROM users WHERE id=corporate_member.user_id)",
					"username"		=> "(SELECT username FROM users WHERE id=corporate_member.user_id)",
					"email"			=> "(SELECT email FROM users WHERE id=corporate_member.user_id)",
					"role"			=> "(SELECT description FROM `groups` WHERE id=(SELECT group_id FROM users_groups WHERE user_id=corporate_member.user_id LIMIT 1))",
					"id"			=> 'id',
				);

				$column_order = array( null, 'name' );
				$column_search = array( 'name' );
				$order = array( 'id' => 'desc' );

				$where = ['corporate_id' => $id];

			   	$custom_column 	= 	'
			   						<button type="button" class="btn btn-danger btn-sm btn-delete"><i class="fas fa-trash"></i> Delete</button>
		              				';

				$this->datatables->get_datatables( 'corporate_member', $selected_column, $column_order, $column_search, $order, $where, $custom_column );
			

			}
		}
	}

	public function delete(){
		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'id', 'id', 'required' );
		if ($this->form_validation->run() === TRUE && $corporate = CorporateModel::find( $this->input->post('id') )){
			json_generator( $corporate->forceDelete() );
		}
	}
	public function approval(){
		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'id', 'id', 'required' );
		$this->form_validation->set_rules( 'status', 'status', 'required' );
		if ($this->form_validation->run() === TRUE && $corporate = CorporateModel::find( $this->input->post('id') )){
			json_generator( $corporate->update( ['status' => $this->input->post('status')] ) );
		}
	}

	public function remove_member(){
		$this->load->library( 'form_validation' );
		$this->load->model('CorporateMemberModel');
		$this->form_validation->set_rules( 'id', 'id', 'required' );
		if ($this->form_validation->run() === TRUE && $corporate_member = CorporateMemberModel::find( $this->input->post('id') )){
			json_generator( $corporate_member->forceDelete() );
		}
	}

	public function new(){
		$data = [
			'page'			=> 'admin-corporate-list',
			'page_title'	=> 'Add new corporte',
			'scripts'	=> [
				'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js',
				base_url('node_modules/sweetalert2/dist/sweetalert2.all.min.js'),
				base_url( 'assets/js/app/admin/corporate/corporate-new.js' ),
			]
		];

		$this->load->view('template-part/admin/header', $data);
		$this->load->view('template-part/admin/sidebar', $data);
		$this->load->view('app/admin/corporate/corporate-new', $data);
		$this->load->view('template-part/admin/footer', $data);
	}

	public function edit( $id ){

		if ( $id && $corporate = CorporateModel::find( $id ) ) {
			$data = [
				'scripts'	=> [
					'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js',
					base_url('node_modules/sweetalert2/dist/sweetalert2.all.min.js'),
					base_url( 'assets/js/app/admin/corporate/corporate-new.js' ),
				],
				'corporate'	=> $corporate,
			];

			$this->load->view('template-part/admin/header', $data);
			$this->load->view('template-part/admin/sidebar', $data);
			$this->load->view('app/admin/corporate/corporate-edit', $data);
			$this->load->view('template-part/admin/footer', $data);
		}
	}

	public function save(){
		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'name', 'name', 'required' );
		$this->form_validation->set_rules( 'phone', 'phone', 'required' );
		$this->form_validation->set_rules( 'office', 'office', 'required' );
		if ($this->form_validation->run() === TRUE){
			$data_insert = [
				'name'			=> $this->input->post('name'),
				'phone'			=> $this->input->post('phone'),
				'website'		=> $this->input->post('website'),
				'office'		=> $this->input->post('office'),
				'description'	=> $this->input->post('description'),
				'status'		=> 'approved',
			];

			$result = CorporateModel::create( $data_insert );
			json_generator( $result );
		}
	}

	public function update(){
		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'id', 'id', 'required' );
		$this->form_validation->set_rules( 'name', 'name', 'required' );
		$this->form_validation->set_rules( 'phone', 'phone', 'required' );
		$this->form_validation->set_rules( 'office', 'office', 'required' );
		if ($this->form_validation->run() === TRUE && $corporate = CorporateModel::find($this->input->post('id')) ){
			$data_update = [
				'name'			=> $this->input->post('name'),
				'phone'			=> $this->input->post('phone'),
				'website'		=> $this->input->post('website'),
				'office'		=> $this->input->post('office'),
				'description'	=> $this->input->post('description'),
				'status'		=> 'approved',
			];

			$result = $corporate->update( $data_update );
			json_generator( ($result) ? $corporate : null );
		}
	}

	public function add_member(){
		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'corporate_id', 'corporate_id', 'required' );
		$this->form_validation->set_rules( 'user_id', 'user_id', 'required' );
		if ($this->form_validation->run() === TRUE && $users = UserModel::whereIn( 'id', explode(',', $this->input->post('user_id')) )->get()  ){
			$result = [];
			foreach ($users as $key => $user) {
				$result[] = $user->corporate_member()->create([
					'corporate_id'	=> $this->input->post('corporate_id'),
					'status'		=> 'approved'
				]);
			}
			
			json_generator( (count($result) == $users->count()) ? ['status' => 'success'] : null );
		}
	}

	public function import( $id ){
		if ( $id && $corporate = CorporateModel::find( $id ) ) {
			$data = [
				'page'			=> 'admin-corporate-list',
				'page_title'	=> 'Import Employee New',
				'corporate'		=> $corporate,
				'scripts'	=> [
					base_url('node_modules/sweetalert2/dist/sweetalert2.all.min.js'),
					'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js',
					base_url( 'assets/js/app/admin/corporate/corporate-import-new.js' ),
				]
			];

			$this->load->view('template-part/admin/header', $data);
			$this->load->view('template-part/admin/sidebar', $data);
			$this->load->view('app/admin/corporate/corporate-import', $data);
			$this->load->view('template-part/admin/footer', $data);
		}
	}

	public function import_upload(){
		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'corporate_id', 'corporate_id', 'required' );
		
		if ($this->form_validation->run() === TRUE){
			$config['upload_path']  	= FCPATH.'assets/imported-excel';
	        $config['allowed_types']	= 'csv|xls|xlsx';
	        $config['max_size']     	= 1000;
	        $config['file_name']		= $this->input->post('corporate_id')."_".$this->ion_auth->user()->row()->id;
	        $config['overwrite']		= true;

	        $this->load->library('upload', $config);

	        if (!file_exists( $config['upload_path'] )) {
				mkdir( $config['upload_path'] , 0777, true);
				$this->load->helper('file');
				write_file($config['upload_path']."/index.html", ' ');
			}

	        if ( $this->upload->do_upload('imported_file')){

	        	$file = $this->upload->data();

	        	// read file execl to json
	        	$this->load->library('my_excel');
	        	$result = $this->my_excel->excel_to_json( $config['upload_path']."/".$config['file_name'], $file['file_ext'] );
	        	json_generator(
	        		[
	        			'file'	=> $config['file_name']."".$file['file_ext'],
	        			'data'	=> $result
	        		]
	        	);

	        } else {
	        	$error = array('error' => $this->upload->display_errors());
	        	print_r($error);
	        }
	    }
	}

	public function import_save(){
		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'file', 'file', 'required' );
		$this->form_validation->set_rules( 'corporate_id', 'corporate_id', 'required' );

		if ($this->form_validation->run() === TRUE){
			$filepath = FCPATH.'assets/imported-excel/'.$this->input->post('file');

			$extension = pathinfo($filepath, PATHINFO_EXTENSION);
			$new_filename = md5( date('Y-m-d-H-i-s')."#".rand(1,9999) ).".".$extension;

			$this->load->model('EmployeeImportModel');

			$result = EmployeeImportModel::create([
				'corporate_id'	=> $this->input->post('corporate_id'),
				'file'			=> $new_filename,
			]);

			if ( $result ) {
	            if ( file_exists( $filepath ) ) {
	                rename($filepath, FCPATH.'assets/imported-excel/'.$new_filename);
	            }

	            json_generator( $result );
			}

		}
	}
}