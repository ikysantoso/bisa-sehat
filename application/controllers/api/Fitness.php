<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Fitness extends RestController {

    private $incoBaseApi;

    public function __construct()
    {
      header('Access-Control-Allow-Origin: *');
      header('Access-Control-Allow-Methods: POST, GET, DELETE, PUT, PATCH, OPTIONS');
      parent::__construct();
      $this->load->database();
      $this->incoBaseApi = $this->config->item('inco_base_api');
    }
    function index_get() {
        $id = $this->get('id');
        if ($id == '') {
            $this->response(['status' => FALSE,'message' => 'Data not found'], RestController::HTTP_NOT_FOUND);
        } else {
            $this->db->where('user_id', $id);
            $fitness = FitnessModel::where('user_id', $id)->orderBy('created_at', 'desc')->get();

            $this->response(['status' => TRUE,'message' => 'Data found','data' => $fitness], RestController::HTTP_OK);
        }
    }

    function index_post() {
        $data_insert = [
            'user_id'	=> $this->post('id'),
            'height'	=> $this->post('height'),
            'weight'	=> $this->post('weight'),
            'mileage'	=> $this->post('mileage'),
        ];

        FitnessModel::create( $data_insert );

        $this->response(['status' => TRUE,'message' => 'Data insert success'], RestController::HTTP_OK);

    }
    public function reportToInco($data_insert)
    {
        $createFitnessIncoUrl = $this->incoBaseApi . '/fitness';

        $user = UserModel::where('id', $data_insert['user_id'])->first();
        $nik = $user->user_meta('nik');

        $input_test = [
            'nik' => $nik,
            'height' => $data_insert['height'],
            'weight' => $data_insert['weight'],
            'mileage' => $data_insert['mileage'],

        ];

        return $this->hitIncoAPI($createFitnessIncoUrl, $input_test);
    }

    private function hitIncoAPI($url, $params)
    {
        $client = new \GuzzleHttp\Client();
        $options = [
            'json' => $params
        ];
        $response = $client->request('POST', $url, $options);

        return $response->getBody();
    }
}