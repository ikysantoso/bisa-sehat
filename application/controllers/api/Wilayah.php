<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wilayah extends CI_Controller {

	private $rajaongkir_api;

	public function __construct(){
		parent::__construct();

		// if ( !$this->ion_auth->logged_in() ) {
		// 	redirect(base_url('auth/login'), 'refresh');
		// } 
		$this->db2 = $this->load->database('village_engine', TRUE);
		
		$this->load->model( 'WilayahModel' );

		$this->rajaongkir_api = $this->config->item( 'rajaongkir_api' );
	}

	public function get_data( $id=null ){
		if ( $id ){
			$result = WilayahModel::select('kode_wilayah as value', 'nama as text')->where('mst_kode_wilayah', $id)->orderBy('nama')->get();
			json_generator( $result );
		}
	}

	public function province(){

			$data = $this->db2->query("SELECT p.pid as value, p.province_name as text FROM province as p ")->result();
			json_generator( $data );
	}

	public function city( $id = null ){
		if ( $id ) {
			$data = $this->db2->query("SELECT p.did as value, p.districts_name as text FROM districts as p where p.pid =$id ")->result();
			json_generator( $data );
		}
	}

	public function subdistrict( $id = null ){
		if ( $id ) {
			$data = $this->db2->query("SELECT p.sdid as value, p.sub_districts_name as text FROM sub_districts as p where p.did =$id ")->result();
			json_generator( $data );
		}
	}

	public function village( $id = null ){
		if ( $id ) {
			$data = $this->db2->query("SELECT p.wid as value, p.ward_name as text FROM ward as p where p.sdid =$id ")->result();
			json_generator( $data );
		}
	}
}