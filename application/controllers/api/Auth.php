<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Auth extends RestController {
    public function __construct()
    {
        parent::__construct();

        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: POST, GET, DELETE, PUT, PATCH, OPTIONS');

        $this->load->database();

        $this->load->library(
            array('ion_auth')
        );

        $this->load->helper(
            array('url', 'language')
        );
    }

    function index_post($request)
    {
        switch ($request) {
            case 'login':
                $this->loginService();
                break;
            case 'register':
                $this->registerService();
                break;
            case 'logout':
                $this->logoutService();
                break;
            case 'multiple-register':
                $this->multipleRegisterService();
                break;
            default:
                break;
        }
    }

    function loginService()
    {
        $username = $this->post('username');

        $user = UserModel::where(function ($query) use ($username) {
            $query->where('email', $username)
                ->orWhere('phone', $username);
        })->first();

        if(isset($user)) {
            $identity = $user->username;
            $password = $this->post('password');

            if($this->ion_auth->login($identity, $password, FALSE)) {
                $user = UserModel::with('user_metas')
                    ->where('id', $user->id)
                    ->first();

                $this->response(array(
                    'status'    => TRUE,
                    'message'   => 'Login success',
                    'data'      => $user), RestController::HTTP_OK);
            } else {
                $this->response(array(
                    'status'    => FALSE,
                    'message'   => 'Invalid username or password',
                    'data'      => null), RestController::HTTP_UNAUTHORIZED);
            }
        } else {
            $this->response(array(
                'status'    => FALSE,
                'message'   => 'User not registered',
                'data'      => null
            ), RestController::HTTP_BAD_REQUEST);
        }
    }

    function registerService()
    {
        $register_type = $this->post('type');

        if($register_type == "email" || $register_type == "phone") {
            if($register_type == "email") {
                $this->registerEmailService();
            } else {
                $this->registerPhoneService();
            }
        } else {
            $this->response(array(
                'status'    => FALSE,
                'message'   => 'Register type invalid',
                'data'      => null
            ), RestController::HTTP_BAD_REQUEST);
        }
    }

    function checkUsernameUnRegistered($username)
    {
        $username_exist = UserModel::where('username', $username)
            ->first();

        if (!isset($username_exist)) {
            return true;
        } else {
            $this->response(array(
                'status'    => FALSE,
                'message'   => 'Username already registered',
                'data'      => null
            ), RestController::HTTP_BAD_REQUEST);;
        }
    }

    function registerEmailService()
    {
        $this->load->model('UserModel');

        $is_username_unregistered = $this->checkUsernameUnRegistered($this->post('username'));

        if($is_username_unregistered) {
            $email_exist = UserModel::where('email', $this->post('email'))
                ->first();

            if(!isset($email_exist)) {
                $full_name = $this->post('full_name');

                if(isset($full_name)) {
                    $identity_column = $this->config->item('identity', 'ion_auth');
                    $identity =  ($identity_column === 'email') ? $this->post('email') : $this->post('username');

                    $additional_data = array();

                    $split_name = preg_split('/\s+/', $full_name, 2);

                    if(count($split_name) >= 1 && $split_name[0] != "") {
                        if ($identity_column == 'email') {
                            $additional_data['username'] = $this->post('username');

                            $additional_data['first_name'] = $split_name[0];

                            if(count($split_name) > 1) {
                                $additional_data['last_name'] = $split_name[1];
                            }
                        }

                        if ($this->ion_auth->register($identity, $this->post('password'), $this->post('email'), $additional_data)) {
                            $this->response(array(
                                'status'    => TRUE,
                                'message'   => 'Registration success',
                                'data'      => null), RestController::HTTP_OK);
                        }
                    } else {
                        $this->response(array(
                            'status'    => FALSE,
                            'message'   => 'Full Name cannot be empty',
                            'data'      => null
                        ), RestController::HTTP_BAD_REQUEST);
                    }
                } else {
                    $this->response(array(
                        'status'    => FALSE,
                        'message'   => 'Full Name cannot be empty',
                        'data'      => null
                    ), RestController::HTTP_BAD_REQUEST);
                }
            } else {
                $this->response(array(
                    'status'    => FALSE,
                    'message'   => 'Email already registered',
                    'data'      => null
                ), RestController::HTTP_BAD_REQUEST);
            }
        }
    }

    function registerPhoneService()
    {
        $this->load->model('UserModel');

        $is_username_unregistered = $this->checkUsernameUnRegistered($this->post('username'));

        if($is_username_unregistered) {
            $phone_exist = UserModel::where('phone', $this->post('phone'))
                ->first();

            if (!isset($phone_exist)) {
                $full_name = $this->post('full_name');

                if(isset($full_name)) {
                    $identity_column = $this->config->item('identity', 'ion_auth');
                    $identity =  ($identity_column === 'phone') ? $this->post('phone') : $this->post('username');

                    $additional_data = array();

                    $split_name = preg_split('/\s+/', $full_name, 2);

                    if (count($split_name) >= 1 && $split_name[0] != "") {
                        if ($identity_column == 'email') {
                            $additional_data['username'] = $this->post('username');
                            $additional_data['phone']    = $this->post('phone');

                            $additional_data['first_name'] = $split_name[0];

                            if(count($split_name) > 1) {
                                $additional_data['last_name'] = $split_name[1];
                            }
                        }

                        if ($this->ion_auth->register($identity, $this->post('password'), '', $additional_data)) {
                            $this->response(array(
                                'status'    => TRUE,
                                'message'   => 'Registration success',
                                'data'      => null
                            ), RestController::HTTP_OK);
                        }
                    } else {
                        $this->response(array(
                            'status'    => FALSE,
                            'message'   => 'Full Name cannot be empty',
                            'data'      => null
                        ), RestController::HTTP_BAD_REQUEST);
                    }
                } else {
                    $this->response(array(
                        'status'    => FALSE,
                        'message'   => 'Full Name cannot be empty',
                        'data'      => null
                    ), RestController::HTTP_BAD_REQUEST);
                }
            } else {
                $this->response(array(
                    'status'    => FALSE,
                    'message'   => 'Phone already registered',
                    'data'      => null
                ), RestController::HTTP_BAD_REQUEST);
            }
        }
    }

    function logoutService()
    {
        $exist_user = $this->ion_auth->user()->row()->id;

        if ($exist_user) {
            $this->ion_auth->logout();

            $this->response(array(
                'status'   => TRUE,
                'message'  => 'Logout Success',
                'data'     => null,
            ), RestController::HTTP_OK);
        } else {
            $this->response(array(
                'status'   => FALSE,
                'message'  => 'User unauthorized',
                'data'     => null,
            ), RestController::HTTP_UNAUTHORIZED);
        }
    }

    function multipleRegisterService()
    {
        $this->load->model('UserModel');

        $identity_column = $this->config->item('identity', 'ion_auth');

        $success = false;
        $response_msg = null;

        foreach($this->post('data') as $data) {
            $username = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $data['name'])));

            $identity =  ($identity_column === 'phone') ? $data['phone'] : $username;

            $split_name = preg_split('/\s+/', $data['name'], 2);

            $additional_data = array();

            if ($identity_column == 'email') {
                $additional_data['username'] = $username;
                $additional_data['phone']    = $data['phone'];

                $additional_data['first_name'] = $split_name[0];

                if(count($split_name) > 1) {
                    $additional_data['last_name'] = $split_name[1];
                }
            }

            $exist_user = UserModel::where('username', $username)->where('phone', $data['phone'])->first();

            if ($exist_user) {
                $data_insert = array(
                    'user_id'                       => $exist_user->id,
                    'height'                        => $data['height'],
                    'weight'                        => $data['weight'],
                    'sistole'                       => $data['sistole'],
                    'diastole'                      => $data['diastole'],
                    'blood_sugar_level'             => $data['blood_sugar_level'],
                    'cholesterol_level'             => $data['cholesterol_level'],
                    'uric_acid_level'               => $data['uric_acid_level'],
                    'uric_acid_more_than_ten_level' => false
                );
        
                $query_last_record = $this->db->query("SELECT * FROM medical_record WHERE user_id = ' . $exist_user->id . ' ORDER BY created_at DESC LIMIT 1");
                $last_record = $query_last_record->result_array();
    
                if (count($last_record) > 0) {
                    foreach($last_record as $record) {
                        $data_insert['sistole'] = $data_insert['sistole'] == '' ? $record['sistole'] : $data_insert['sistole'];
                        $data_insert['diastole'] = $data_insert['diastole'] == '' ? $record['diastole'] : $data_insert['diastole'];
                        $data_insert['blood_sugar_level'] = $data_insert['blood_sugar_level'] == '' ? $record['blood_sugar_level'] : $data_insert['blood_sugar_level'];
                        $data_insert['cholesterol_level'] = $data_insert['cholesterol_level'] == '' ? $record['cholesterol_level'] : $data_insert['cholesterol_level'];
                        $data_insert['uric_acid_level'] = $data_insert['uric_acid_level'] == '' ? $record['uric_acid_level'] : $data_insert['uric_acid_level'];
                    }
                } else {
                    $data_insert['sistole'] = $data_insert['sistole'] == '' ? 0 : $data_insert['sistole'];
                    $data_insert['diastole'] = $data_insert['diastole'] == '' ? 0 : $data_insert['diastole'];
                    $data_insert['blood_sugar_level'] = $data_insert['blood_sugar_level'] == '' ? 0 : $data_insert['blood_sugar_level'];
                    $data_insert['cholesterol_level'] = $data_insert['cholesterol_level'] == '' ? 0 : $data_insert['cholesterol_level'];
                    $data_insert['uric_acid_level'] = $data_insert['uric_acid_level'] == '' ? 0 : $data_insert['uric_acid_level'];
                }
    
                MedicalRecordModel::create($data_insert);
            } else {
                $password = 'dicky-ganteng';

                $result = $this->ion_auth->register($identity, $password, '', $additional_data);

                if ($result) {
                    $user = UserModel::where('username', $username)->where('phone', $data['phone'])->first();

                    $data_insert = array(
                        'user_id'                       => $user->id,
                        'height'                        => $data['height'],
                        'weight'                        => $data['weight'],
                        'sistole'                       => $data['sistole'],
                        'diastole'                      => $data['diastole'],
                        'blood_sugar_level'             => $data['blood_sugar_level'],
                        'cholesterol_level'             => $data['cholesterol_level'],
                        'uric_acid_level'               => $data['uric_acid_level'],
                        'uric_acid_more_than_ten_level' => false
                    );
            
                    $query_last_record = $this->db->query("SELECT * FROM medical_record WHERE user_id = ' . $user->id . ' ORDER BY created_at DESC LIMIT 1");
                    $last_record = $query_last_record->result_array();
        
                    if (count($last_record) > 0) {
                        foreach($last_record as $record) {
                            $data_insert['sistole'] = $data_insert['sistole'] == '' ? $record['sistole'] : $data_insert['sistole'];
                            $data_insert['diastole'] = $data_insert['diastole'] == '' ? $record['diastole'] : $data_insert['diastole'];
                            $data_insert['blood_sugar_level'] = $data_insert['blood_sugar_level'] == '' ? $record['blood_sugar_level'] : $data_insert['blood_sugar_level'];
                            $data_insert['cholesterol_level'] = $data_insert['cholesterol_level'] == '' ? $record['cholesterol_level'] : $data_insert['cholesterol_level'];
                            $data_insert['uric_acid_level'] = $data_insert['uric_acid_level'] == '' ? $record['uric_acid_level'] : $data_insert['uric_acid_level'];
                        }
                    } else {
                        $data_insert['sistole'] = $data_insert['sistole'] == '' ? 0 : $data_insert['sistole'];
                        $data_insert['diastole'] = $data_insert['diastole'] == '' ? 0 : $data_insert['diastole'];
                        $data_insert['blood_sugar_level'] = $data_insert['blood_sugar_level'] == '' ? 0 : $data_insert['blood_sugar_level'];
                        $data_insert['cholesterol_level'] = $data_insert['cholesterol_level'] == '' ? 0 : $data_insert['cholesterol_level'];
                        $data_insert['uric_acid_level'] = $data_insert['uric_acid_level'] == '' ? 0 : $data_insert['uric_acid_level'];
                    }
        
                    MedicalRecordModel::create($data_insert);
                }
            }
        }

        $this->response(array(
            'status'   => TRUE,
            'message'  => 'Successfully multiple register',
            'data'     => null,
        ), RestController::HTTP_OK);
    }
}