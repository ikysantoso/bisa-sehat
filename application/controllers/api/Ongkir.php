<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ongkir extends CI_Controller {

	public function __construct(){
		parent::__construct();

		if ( !$this->ion_auth->logged_in() ) {
			redirect(base_url('auth/login'), 'refresh');
		} 
	}

	public function index(){
		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'cart_id', 'cart_id', 'required' );
		$this->form_validation->set_rules( 'destination', 'destination', 'required' );
		if ($this->form_validation->run() === TRUE){

			$couriers = ['jne'];
			// $couriers = ['jne', 'pos', 'tiki'];

			if ( $this->input->post('courier') ) {
				$couriers = [$this->input->post('courier')];
			}

			$this->load->model('CartModel');


			$cart_ids = explode(",", $this->input->post('cart_id'));
			$warehouse = CartModel::find( $cart_ids[0] )->warehouse;

				// weight counter
			$weight = 0;
			$carts = CartModel::whereIn( 'id', explode(',', $this->input->post('cart_id')) )->get();
			foreach ($carts as $key => $cart) {
				$weight += $cart->product->weight * $cart->quantity;
			}

			$this->load->model('ShippingModel');
			
			$data = $this->ShippingModel->ongkir( $warehouse->kabupaten->city_id, $this->input->post('destination'), $weight, $couriers );

			json_generator( ['weight'=> $weight, 'data' => $data] );
		}
	} 
}