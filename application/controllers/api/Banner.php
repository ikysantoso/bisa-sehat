<?php

use chriskacerguis\RestServer\RestController;

class Banner extends RestController
{
    public function __construct()
    {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: POST, GET, DELETE, PUT, PATCH, OPTIONS');
        parent::__construct();
        $this->load->database();
    }

    function index_get() {
        $banner = SliderModel::where('status', '=', 'active')->get();

        $this->response([
            'status' => TRUE,
            'message' => 'Fetch Banner Success',
            'data' => $banner],
            RestController::HTTP_OK);

    }
}