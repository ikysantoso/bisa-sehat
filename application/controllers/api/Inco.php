<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Inco extends RestController {

    public function __construct()
    {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: POST, GET, DELETE, PUT, PATCH, OPTIONS');
        parent::__construct();
        $this->load->model(['MedicalRecordModel', 'UserMetaModel', 'UserModel']);
    }

    public function index_get($request, $nik)
    {
        if ($request == 'getAllMedicalRecord') {
            $users = UserModel::with('medical_records')->get();

            json_generator([
                'success'         => true,
                'message'         => 'Berhasil mengambil seluruh data medical records',
                'data'            => $users
            ]);
        } else if($request == 'getLatestMedicalRecordPerUser') {
            $user = UserModel::whereHas('user_metas', function ($user_meta) use ($nik) {
                $user_meta->where('meta_key', 'nik')->where('meta_value', $nik);
            })->with(['medical_records' => function ($medical_record) {
                $medical_record->orderBy('created_at', 'desc');
            }])->first();

            json_generator([
                'success'         => true,
                'message'         => 'Berhasil mengambil data medical records',
                'data'            => $user
            ]);
        } else if ($request == 'getLatestFitness') {
            $user = UserModel::whereHas('user_metas', function ($user_meta) use ($nik) {
                $user_meta->where('meta_key', 'nik')->where('meta_value', $nik);
            })->with(['fitnesses' => function ($medical_record) {
                $medical_record->orderBy('created_at', 'desc');
            }])->first();

            json_generator([
                'success'         => true,
                'message'         => 'Berhasil mengambil data fitness',
                'data'            => $user
            ]);
        } else if ($request == 'getGender') {
            $user = UserModel::whereHas('user_metas', function ($user_meta) use ($nik) {
                $user_meta->where('meta_key', 'nik')->where('meta_value', $nik);
            })->first();

            $gender = UserMetaModel::where('user_id', $user->id)->where('meta_key', 'gender')->first();

            json_generator([
                'success'         => true,
                'message'         => 'Berhasil mengambil data gender',
                'data'            => $gender->meta_value
            ]);
        }
    }
}