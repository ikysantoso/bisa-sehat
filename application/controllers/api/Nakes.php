<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Nakes extends RestController {

    private $incoBaseApi;

    public function __construct()
    {
      header('Access-Control-Allow-Origin: *');
      header('Access-Control-Allow-Methods: POST, GET, DELETE, PUT, PATCH, OPTIONS');
      parent::__construct();
      $this->load->model(['MedicalRecordModel', 'UserMetaModel', 'UserModel']);
      $this->incoBaseApi = $this->config->item('inco_base_api');
    }

    public function index_get($request, $userId, $nik)
    {
      if ($request == 'getLastRecord') {
        $user = UserModel::find($userId);

        json_generator([
          'success'         => true,
          'message'         => 'data fetched',
          'data'            => $user->medical_records()->latest()->first()
        ]);
      } else {
        $user_meta = UserMetaModel::where([
          'meta_key'    => 'nik',
          'meta_value'	=> $nik
        ])->first();

        $birth_date = UserMetaModel::where('user_id', '=', $user_meta->user()->first()->id)
                                    ->where('meta_key', '=', 'date-birth')
                                    ->first();

        if ($user_meta) {
          json_generator([
            'success'         => true,
            'message'         => 'patient found',
            'data'            => $user_meta->user()->first(),
            'meta'            => $user_meta,
            'birth_date'      => $birth_date
          ]);
        } else {
          json_generator([
            'success'         => false,
            'message'         => 'patient not found'
          ]);
        }
      }
    }

    public function index_post($metaId)
    {
      header('Access-Control-Allow-Origin: *');
      header('Access-Control-Allow-Methods: POST, GET, DELETE, PUT, PATCH, OPTIONS');

      $data_insert = [
        'user_id'			      => $metaId,
        'height'			      => $this->input->post('height'),
        'weight'			      => $this->input->post('weight'),
        'sistole'			      => $this->input->post('sistole'),
        'diastole'			      => $this->input->post('diastole'),
        'blood_sugar_level'       => $this->input->post('bloodSugar'),
        'cholesterol_level'	      => $this->input->post('cholesterol'),
        'uric_acid_level'	      => $this->input->post('uricAcid'),
        'nursing_number'        => $this->input->post('nursingNumber'),
      ];

      $medical_record = MedicalRecordModel::create($data_insert);

      $this->reportToInco($data_insert);

      json_generator([
        'success'         => true,
        'message'         => 'record added',
        'medical_record'  => $medical_record,
        'bmi'             => $medical_record->bmi(),
        'user'            => $medical_record->user()->first()
      ]);
    }

    public function reportToInco($data_insert)
    {
        $createMedicalCheckupIncoUrl = $this->incoBaseApi . '/medical-checkup';

        $user = UserModel::where('id', $data_insert['user_id'])->first();
        $nik = $user->user_meta('nik');

        $input['nik'] = $nik;
        $input['height'] = $data_insert['height'];
        $input['weight'] = $data_insert['weight'];
        $input['sistole'] = $data_insert['sistole'];
        $input['diastole'] = $data_insert['diastole'];
        $input['bloodSugarLevel'] = $data_insert['blood_sugar_level'];
        $input['cholesterolLevel'] = $data_insert['cholesterol_level'];
        $input['uricAcidLevel'] = $data_insert['uric_acid_level'];
        $input['nursingNumber'] = $data_insert['nursing_number'];

        $this->hitIncoAPI($createMedicalCheckupIncoUrl, $input);
    }

    private function hitIncoAPI($url, $params)
    {
        $ch = curl_init();

        if (isset($params)) {
            $fullUrl = $url . '?' . http_build_query($params);
            log_message('error', $fullUrl);
            curl_setopt($ch, CURLOPT_URL, $fullUrl);
        } else {
            curl_setopt($ch, CURLOPT_URL, $url);
        }

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json')
        );
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        log_message('error', 'test');
        return curl_exec($ch);
    }
}