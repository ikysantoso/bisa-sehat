<?php

use chriskacerguis\RestServer\RestController;

class HealthArticle extends RestController
{
    public function __construct()
    {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: POST, GET, DELETE, PUT, PATCH, OPTIONS');
        parent::__construct();
        $this->load->database();
    }

    function index_get() {
        $this->response([
            'status'    => TRUE,
            'message'   => 'Fetch articles success',
            'data'      => PageModel::take(6)->orderBy('id', 'desc')->get()
        ], RestController::HTTP_OK);
    }
}