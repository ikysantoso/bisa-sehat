<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class User_meta extends RestController {

    private $incoBaseApi;

    public function __construct()
    {
      header('Access-Control-Allow-Origin: *');
      header('Access-Control-Allow-Methods: POST, GET, DELETE, PUT, PATCH, OPTIONS');
      parent::__construct();
      $this->load->database();
      $this->incoBaseApi = $this->config->item('inco_base_api');
    }
    function index_get() {
        $id = $this->get('id');
        if ($id == '') {
            $this->response(['status' => FALSE,'message' => 'Data not found'], RestController::HTTP_NOT_FOUND);
        } else {
            $this->db->where('user_id', $id);
            $user = $this->db->get('user_meta')->result();
            $this->response(['status' => TRUE,'message' => 'Data found','data' => $user], RestController::HTTP_OK);
        }
    }
}