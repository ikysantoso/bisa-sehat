<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Profile extends RestController {

    public function __construct()
    {
        parent::__construct();

        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: POST, GET, DELETE, PUT, PATCH, OPTIONS');

        $this->load->database();
    }

    function index_get() {
        $id = $this->get('id');

        if ($id == '') {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Data not found',
                'data' => null
            ), RestController::HTTP_BAD_REQUEST);
        } else {
            $user = UserModel::with('user_metas')
                ->where('id', $id)
                ->first();

            $this->response(array(
                'status' => TRUE,
                'message' => 'Data fetched',
                'data' => $user
            ), RestController::HTTP_OK);
        }
    }

    function index_post() {
        $id = $this->post('id');

        if ($id == '') {
            $this->response(array(
                'status' => FALSE,
                'message' => 'User ID not found',
                'data' => null
            ), RestController::HTTP_BAD_REQUEST);
        } else {
            $user = UserModel::find($id);

            if(isset($user)) {
                $this->load->model( 'UserMetaModel' );

                $main_update = array(
                    'phone'         => $this->post('phone'),
                );

                $meta_update = array(
                    'nik'           => $this->post('nik'),
                    'job'           => $this->post('job'),
                    'gender'        => $this->post('gender'),
                    'date-birth'    => $this->post('birthdate'),
                    'blood-type'    => $this->post('blood_type'),
                );



                $nik_exist = UserMetaModel::where(
                    array(
                        'meta_key'      => 'nik',
                        'meta_value'    => $this->post('nik'),
                    )
                )->where('user_id', '!=', $user['id'])->first();

                if ($nik_exist) {
                    $this->response(
                        array(

                            'status' => FALSE,
                            'message'  => 'NIK telah digunakan',
                            'field' => 'nik'
                        ),
                        RestController::HTTP_BAD_REQUEST
                    );
                } else {
                    $phone_exist = UserModel::where('phone', $this->post('phone'))->where('id', '!=', $this->post('id'))->first();

                    if ($phone_exist) {
                        $this->response(
                            array(
                                'status' => FALSE,
                                'message'  => 'Nomor HP telah digunakan',
                                'field' => 'phone'
                            ), RestController::HTTP_BAD_REQUEST
                        );
                    } else {
                        foreach ($meta_update as $key => $item) {
                            UserMetaModel::updateOrCreate(
                                array(
                                    'meta_key'      => $key,
                                    'user_id'       => $this->post('id')
                                ),
                                array(
                                    'meta_value'    => $item
                                )
                            );
                        }

                        $result = $user->update($main_update);
                        $this->response(array(
                            'status' => TRUE,
                            'message' => 'Data update success',
                            'data'    => $result
                        ), RestController::HTTP_OK);
                    }
                }

                if (!$nik_exist && !$phone_exist) {
                    foreach ($meta_update as $key => $item) {
                        UserMetaModel::updateOrCreate(
                            [
                                'meta_key'      => $key,
                                'user_id'       => $this->post('id')
                            ],
                            [
                                'meta_value'    => $item
                            ]
                        );
                    }

                    $result = $user->update( $main_update );
                    $this->response(array(
                        'status' => TRUE,
                        'message' => 'Data update success',
                        'data'      => $result
                    ), RestController::HTTP_OK);
                }
            } else {
                $this->response(array(
                    'status' => FALSE,
                    'message' => 'User not found',
                    'data' => null
                ), RestController::HTTP_BAD_REQUEST);
            }
        }
    }

    function index_put() {

            $this->load->model( 'UserMetaModel' );

            $main_update = [
                'first_name'    => $this->post('first_name'),
                'last_name'     => $this->post('last_name'),
                'phone'         => $this->post('phone'),
            ];

            $meta_update = [
                'nik'           => $this->post('nik'),
                'religion'      => $this->post('religion'),
                'job'           => $this->post('job'),
                'gender'        => $this->post('gender'),
                'date-birth'    => $this->post('date-birth'),
                'blood-type'    => $this->post('blood-type'),
            ];

            // check nik if exist
            $nik_exist = UserMetaModel::where(
                [
                    'meta_key'      => 'nik',
                    'meta_value'    => $this->post('nik'),

                ]
            )->where('user_id', '!=', $this->post('id'))->first();

            $user = UserModel::find( $this->post('id') );

            if($this->post('nik')!=''){
                if ( $nik_exist ) {
                        $this->response(
                            [

                                'status' => FALSE,
                                'message'  => 'NIK telah digunakan',
                                'field' => 'nik'
                            ],
                            409
                        );
                }
            }
            $phone_exist = UserModel::where('phone', $this->post('phone'))->where('id', '!=', $this->post('id'))->first();
            if ( $phone_exist  ) {
                    $this->response(
                        [
                            'status' => FALSE,
                            'message'  => 'Nomor HP telah digunakan',
                            'field' => 'phone'
                        ],
                        409
                    );
            }

            if($this->post('nik')==''){
            if (!$phone_exist  ) {
                foreach ($meta_update as $key => $item) {
                    UserMetaModel::updateOrCreate(
                        [
                            'meta_key'      => $key,
                            'user_id'       => $this->post('id')
                        ],
                        [
                            'meta_value'    => $item
                        ]
                    );
                }

                $result = $user->update( $main_update );
            $this->response(['status' => TRUE,'message' => 'Data update success'], RestController::HTTP_OK);
            }               
            }else{
            if ( !$nik_exist && !$phone_exist  ) {
                foreach ($meta_update as $key => $item) {
                    UserMetaModel::updateOrCreate(
                        [
                            'meta_key'      => $key,
                            'user_id'       => $this->post('id')
                        ],
                        [
                            'meta_value'    => $item
                        ]
                    );
                }

                $result = $user->update( $main_update );
            $this->response(['status' => TRUE,'message' => 'Data update success'], RestController::HTTP_OK);
            }
            }
        }
    
}