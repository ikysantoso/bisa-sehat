<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class MidtransNotificationPayment extends RestController
{
    public function __construct()
    {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: POST, GET, DELETE, PUT, PATCH, OPTIONS');

        parent::__construct();
    }

    public function index_post()
    {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: POST, GET, DELETE, PUT, PATCH, OPTIONS');

        log_message('error', 'hitted');

        log_message('error', $this->post('transaction_status'));
        json_generator([
            'success'         => $this->post('transaction_status')
        ]);
    }
}