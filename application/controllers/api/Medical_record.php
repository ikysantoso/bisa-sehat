<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Medical_record extends RestController {

    private $incoBaseApi;

    public function __construct()
    {
      header('Access-Control-Allow-Origin: *');
      header('Access-Control-Allow-Methods: POST, GET, DELETE, PUT, PATCH, OPTIONS');
      parent::__construct();
      $this->load->database();
      $this->load->model(['MedicalRecordModel', 'UserMetaModel', 'UserModel']);
      $this->incoBaseApi = $this->config->item('inco_base_api');
    }
    function index_get($request) {
        if ($request == 'latest') {
            $id = $this->get('id');

            if (!isset($id) || $id == '') {
                $this->response(['status' => FALSE,'message' => 'User id cannot be empty', 'data' => null], RestController::HTTP_BAD_REQUEST);
            } else {
                $user = UserModel::find($id);

                if (isset($user)) {
                    $medical_record = $user->medical_records->last();

                    $response = (object) [];
                    $response->imb = (object) [
                        'value'     => $medical_record->bmi()['bmi'] ? $medical_record->bmi()['bmi'] : 0,
                        'status'    => $medical_record->bmi_scale()['desc']
                    ];

                    $response->cholesterol = (object) [
                        'value'     => $medical_record->cholesterol_level.' mg/dL',
                        'status'    => $medical_record->cholesterol_level_scale()['desc']
                    ];

                    $response->blood_pressure = (object) [
                        'value'     => $medical_record->sistole.'/'.$medical_record->diastole.' mmHg',
                        'status'    => $medical_record->blood_pressure_scale()['desc']
                    ];

                    $response->uric_acid = (object) [
                        'value'     => $medical_record->uric_acid_level.' mg/dL',
                        'status'    => $medical_record->uric_acid_level_scale()['desc']
                    ];

                    $response->blood_sugar = (object) [
                        'value'     => $medical_record->blood_sugar_level.' mg/dL',
                        'status'    => $medical_record->blood_sugar_level_scale()['desc']
                    ];

                    $fitness = $user->fitnesses->last();

                    if (isset($fitness)) {
                        $response->fitness_status = $fitness->mileage_result()['status'];
                    } else {
                        $response->fitness_status = 'Belum ada data';
                    }

                    $this->response(['status' => TRUE,'message' => 'Fetch Latest Medical Record Success','data' => $response], RestController::HTTP_OK);
                } else {
                    $this->response([
                        'status'    => FALSE,
                        'message'   => 'User not found',
                        'data'      => null
                    ], RestController::HTTP_BAD_REQUEST);
                }
            }
        } else if ($request == 'user') {
            $id = $this->get('id');

            if (!isset($id) || $id == '') {
                $this->response(['status' => FALSE,'message' => 'User id cannot be empty'], RestController::HTTP_BAD_REQUEST);
            } else {
                $this->db->where('user_id', $id);
                $medical_record = MedicalRecordModel::where('user_id', $id)->orderBy('created_at', 'desc')->get();

                $result = [];

                foreach($medical_record as $key => $value) {
                    $problem_counter = 0;
                    $problem_desc = '';

                    $object = new stdClass();
                    $object->id = $value->id;
                    $object->user_id = $value->user_id;
                    $object->height = $value->height;
                    $object->weight = $value->weight;
                    $object->sistole = $value->sistole;
                    $object->diastole = $value->diastole;
                    $object->blood_sugar_level = $value->blood_sugar_level;
                    $object->cholesterol_level = $value->cholesterol_level;
                    $object->uric_acid_level = $value->uric_acid_level;
                    $object->created_at = $value->created_at;

                    $record = MedicalRecordModel::where('id', $value->id)->first();

                    if($record->bmi_scale()['count'] == 1){
                        $problem_counter += 1;

                        $problem_desc == '' ? $problem_desc = 'Overweight' : $problem_desc .= ', Overweight';
                    }else if($record->bmi_scale()['count'] == 2){
                        $problem_counter += 1;
                        $problem_desc == '' ? $problem_desc = 'Kegemukan' : $problem_desc .= ', Kegemukan';
                    }

                    if($record->cholesterol_level_scale()['count'] > 0){
                        $problem_counter += 1;
                        $problem_desc == '' ? $problem_desc = 'Kolesterol' : $problem_desc .= ', Kolesterol';
                    }

                    if($record->blood_pressure_scale()['count'] > 0){
                        $problem_counter += 1;
                        $problem_desc == '' ? $problem_desc = 'Hipertensi' : $problem_desc .= ', Hipertensi';
                    }

                    if($record->uric_acid_level_scale()['count'] > 0){
                        $problem_counter += 1;
                        $problem_desc == '' ? $problem_desc = 'Asam Urat' : $problem_desc .= ', Asam Urat';
                    }

                    if($record->blood_sugar_level_scale()['count'] > 0){
                        $problem_counter += 1;
                        $problem_desc == '' ? $problem_desc = 'Tekanan Darah Tinggi' : $problem_desc .= ', Tekanan Darah Tinggi';
                    }

                    if ($problem_counter == 0) {
                        $object->summary = "Kesehatan tubuh Anda masih oke. Tolong jaga tubuh Anda dengan konsumsi makanan yang sehat, istirahat cukup, dan pola hidup sehat.";
                        $object->suggestion = "Jika Anda mempunyai keluhan kesehatan, silahkan pergi ke Dokter.";
                    } else {
                        $object->summary = "Anda mempunyai indikator bahaya kesehatan = " . $problem_counter;

                        if ($problem_counter == 1) {
                            $object->summary .= ", yaitu pada " . $problem_desc. '. ';
                            $object->summary .= 'Anda mempunyai risiko sakit stroke, jantung, dan ginjal.';
                            $object->suggestion = "Saran : harus jaga pola makan dan jika ada keluhan kesehatan segera berobat ke Dokter.";
                        } else {
                            $all_problem = explode(",", $problem_desc);
                            $all_problem_desc = implode(",", $all_problem);

                            $object->summary .= ', yaitu pada '.$all_problem_desc.'. ';
                            $object->summary .= 'Anda mempunyai risiko sakit stroke, jantung, dan ginjal.';
                            $object->suggestion = "Saran : harus jaga pola makan dan jika ada keluhan kesehatan segera berobat ke Dokter.";
                        }
                    }

                    $result[] = $object;
                }

                $this->response(['status' => TRUE,'message' => 'Data found','data' => $result], RestController::HTTP_OK);
            }
        } else if ($request == 'diagnose') {
            $id = $this->get('id');

            if (!isset($id) || $id == '') {
                $this->response(['status' => FALSE,'message' => 'User id cannot be empty', 'data' => null], RestController::HTTP_BAD_REQUEST);
            } else {
                $medicalRecords = MedicalRecordModel::where('user_id', $id)->orderBy('created_at', 'desc')->take(5)->get();

                foreach($medicalRecords->reverse()->toArray() as $key => $value) {
                    $datetime = strtotime($value['created_at']);
                    $date[] = date('d F', $datetime);
                    $cholesterol[] = $value['cholesterol_level'];
                    $bloodSugar[] = $value['blood_sugar_level'];
                    $uricAcid[] = $value['uric_acid_level'];
                    $sistole[] = $value['sistole'];
                    $diastole[] = $value['diastole'];
                    $bodyMassIndex[] = $medicalRecords[(int)$key]->bmi()['bmi'];
                }

                $response = (object) [
                    'date'          => $date,
                    'cholesterol'   => $cholesterol,
                    'bloodSugar'    => $bloodSugar,
                    'uricAcid'      => $uricAcid,
                    'sistole'       => $sistole,
                    'diastole'      => $diastole,
                    'bodyMassIndex' => $bodyMassIndex
                ];

                $this->response([
                    'status'    => TRUE,
                    'message'   => 'Fetch Diagnose Success',
                    'data'      => $response
                ], RestController::HTTP_OK);
            }
        }
    }

    function index_post() {
        $uric_acid_more_than_ten_level = false;
        $id = $this->post('id');
        $nik = UserMetaModel::where('user_id',$id)->where('meta_key', 'nik')->first();

        $data_insert = array(
                'user_id'                       => $id,
                'height'                        => $this->post('height'),
                'weight'                        => $this->post('weight'),
                'sistole'                       => $this->post('sistole'),
                'diastole'                      => $this->post('diastole'),
                'blood_sugar_level'             => $this->post('blood_sugar_level'),
                'cholesterol_level'             => $this->post('cholesterol_level'),
                'uric_acid_level'               => $this->post('uric_acid_level'),
                'uric_acid_more_than_ten_level' => $uric_acid_more_than_ten_level);

            $query_last_record = $this->db->query("SELECT * FROM medical_record WHERE user_id = ' . $id . ' ORDER BY created_at DESC LIMIT 1");
            $last_record = $query_last_record->result_array();

            if (count($last_record) > 0) {
                foreach($last_record as $record) {
                    $data_insert['sistole'] = $data_insert['sistole'] == '' ? $record['sistole'] : $data_insert['sistole'];
                    $data_insert['diastole'] = $data_insert['diastole'] == '' ? $record['diastole'] : $data_insert['diastole'];
                    $data_insert['blood_sugar_level'] = $data_insert['blood_sugar_level'] == '' ? $record['blood_sugar_level'] : $data_insert['blood_sugar_level'];
                    $data_insert['cholesterol_level'] = $data_insert['cholesterol_level'] == '' ? $record['cholesterol_level'] : $data_insert['cholesterol_level'];
                    $data_insert['uric_acid_level'] = $data_insert['uric_acid_level'] == '' ? $record['uric_acid_level'] : $data_insert['uric_acid_level'];
                }
            } else {
                $data_insert['sistole'] = $data_insert['sistole'] == '' ? 0 : $data_insert['sistole'];
                $data_insert['diastole'] = $data_insert['diastole'] == '' ? 0 : $data_insert['diastole'];
                $data_insert['blood_sugar_level'] = $data_insert['blood_sugar_level'] == '' ? 0 : $data_insert['blood_sugar_level'];
                $data_insert['cholesterol_level'] = $data_insert['cholesterol_level'] == '' ? 0 : $data_insert['cholesterol_level'];
                $data_insert['uric_acid_level'] = $data_insert['uric_acid_level'] == '' ? 0 : $data_insert['uric_acid_level'];
            }

            MedicalRecordModel::create($data_insert);

            $this->response(['status' => TRUE,'message' => 'Data insert success'], RestController::HTTP_OK);
    }

    public function reportToInco($data_insert)
    {
        $createMedicalCheckupIncoUrl = $this->incoBaseApi . '/medical-checkup';

        $user = UserModel::where('id', $data_insert['user_id'])->first();
        $nik = $user->user_meta('nik');

        $input_test = [
            'nik' => $nik,
            'height' => $data_insert['height'],
            'weight' => $data_insert['weight'],
            'sistole' => $data_insert['sistole'],
            'diastole' => $data_insert['diastole'],
            'bloodSugarLevel' => $data_insert['blood_sugar_level'],
            'cholesterolLevel' => $data_insert['cholesterol_level'],
            'uricAcidLevel' => $data_insert['uric_acid_level'],
        ];

        return $this->hitIncoAPI($createMedicalCheckupIncoUrl, $input_test);
    }

    private function hitIncoAPI($url, $params)
    {
        $client = new \GuzzleHttp\Client();
        $options = [
            'json' => $params
        ];
        $response = $client->request('POST', $url, $options);

        return $response->getBody();
    }
    
    private function hitIncoAPIcompany($url, $params)
    {
        $ch = curl_init();

        if (isset($params)) {
            $fullUrl = $url . '?' . http_build_query($params);
            log_message('error', $fullUrl);
            curl_setopt($ch, CURLOPT_URL, $fullUrl);
        } else {
            curl_setopt($ch, CURLOPT_URL, $url);
        }

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json')
        );
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        return curl_exec($ch);
    }
}