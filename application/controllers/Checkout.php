<?php

use Carbon\Carbon;

defined('BASEPATH') OR exit('No direct script access allowed');

class Checkout extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		if (!$this->ion_auth->logged_in()) {
			redirect(base_url('auth/login'), 'refresh');
		} else if ($this->ion_auth->is_admin()) {
			redirect(base_url('admin/dashboard'), 'refresh');
		}
	}

	public function index()
	{
		$user_info=UserModel::find( $this->ion_auth->user()->row()->id );
                        $user_meta_data = [];
                        foreach ($user_info->user_metas as $key => $user_meta) {
                        $user_meta_data[ $user_meta->meta_key ] = $user_meta->meta_value;
                        }
		if($user_meta_data['nik']==''){
			$this->session->set_flashdata('message', 'Silahkan isi dulu NIK Anda.');
			redirect(base_url('member/dashboard'), 'refresh');
		}else{
		$this->load->library ('form_validation');
		$this->form_validation->set_rules('cart_id', 'cart_id', 'required');

		if ($this->form_validation->run() == TRUE) {
			$this->load->model( ['CartModel', 'CustomerAddressModel','BisapayModel', 'WarehouseModel'] );

			$warehouses = CartModel::whereIn( 'id', explode(',', $this->input->post('cart_id')) )->select('warehouse_id')->distinct()->pluck('warehouse_id');

			$carts = [];

			$warehouseData = null;

			foreach ($warehouses as $key => $warehouse) {
				$_carts = CartModel::whereIn( 'id', explode(',', $this->input->post('cart_id')) )
							->where('warehouse_id', $warehouse)
							->get();

				$warehouseData = WarehouseModel::where(['id' => $warehouse])->get();

				$nonpreorder_cart = [];
				foreach ($_carts as $cart) {
					if ($cart->is_preorder($warehouse)) {
						$carts[] = CartModel::where('id', $cart->id)->get();
					} else {
						$nonpreorder_cart[] = $cart->id;
					}
				}

				if (count($nonpreorder_cart) > 0) {
					$carts[] = CartModel::whereIn('id', $nonpreorder_cart)->get();
				}
			}

			$this->db->select('*');
			$this->db->from('bank');
			$this->db->where('status', 'active');
			$banks = $this->db->get()->result_array();

			$is_bisapay_active = BisapayModel::where('user_id',$this->ion_auth->user()->row()->id)->first();

			$vouchers = $this->db->query("SELECT v.id, v.name, v.nominal, v.quantity FROM voucher v INNER JOIN claimed_vouchers cv ON v.id = cv.voucher_id
											WHERE cv.user_id = ".$this->ion_auth->user()->row()->id." AND NOW() BETWEEN v.start_date AND v.expired")
											->result();

			if (count($carts) > 0) {
				$data = [
					'page'				 => 'checkout',
					'isBackDetail'       => 'true',
					'warehouse'			 => $warehouseData,
					'carts'				 => $carts,
					'cart_ids'			 => $this->input->post('cart_id'),
					'vouchers'			 => $vouchers,
					'customer_addresses' => CustomerAddressModel::where(['user_id' => $this->ion_auth->user()->row()->id])->get(),
					'primary_address'	 => CustomerAddressModel::where([
					'user_id' 			 => $this->ion_auth->user()->row()->id,
					'is_primary_address' => 'yes'
                                        ])->first(),
					'shipping_address'   => CustomerAddressModel::where([
					'user_id' 			 => $this->ion_auth->user()->row()->id,
					'is_shipping_address'=> 'yes'
										])->first(),
					'banks'					=> $banks,
					'is_bisapay_active'		=> $is_bisapay_active,
					'scripts'				=> [
						base_url('node_modules/sweetalert2/dist/sweetalert2.all.min.js'),
						base_url( 'assets/js/app/user/checkout.js' ),
						base_url( 'assets/js/app/user/dashboard/address-management.js' ),
						'https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js',
						'https://cdnjs.cloudflare.com/ajax/libs/fastselect/0.7.3/fastselect.standalone.min.js',
				
					],
					'styles'				=> [
						'https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css',
						'https://cdnjs.cloudflare.com/ajax/libs/fastselect/0.7.3/fastselect.min.css',
					],
				];

				$this->load->view('template-part/header', $data);
				$this->load->view('template-part/navbar', $data);
				$this->load->view('app/frontpage/checkout', $data);
				$this->load->view('template-part/footer', $data);
			} else {
				redirect(base_url('cart'), 'refresh');
			}
		} else {
			redirect(base_url('cart'), 'refresh');
		}
		}
	}

	private function transaction_payment()
	{
		$this->load->model(['TransactionPaymentModel']);

        $transaction_payment = TransactionPaymentModel::create([
            'unique_code'			=> randomNumber( get_setting('unique_code_digit') ),
            'payment_method'	    => null,
            'status'				=> 'unpaid'
        ]);

        return $transaction_payment;
	}

	public function save_transaction($transaction, $transaction_payment)
	{
		$this->load->library('my_email');

		$data_insert_transaction = $transaction['data']['transaction'];
		$data_insert_transaction['transaction_payment_id'] = $transaction_payment->id;
		$data_insert_transaction['status'] = 'waiting';

		$transaction_inserted = TransactionModel::create($data_insert_transaction);

		if ($transaction_inserted) {
			foreach ($transaction['data']['transaction_items'] as $data_transaction_item) {
				$transaction_item = TransactionItemModel::create([
					'transaction_id'			        => $transaction_inserted->id,
					'product_id'					    => $data_transaction_item['product_id'],
					'price'								=> $data_transaction_item['price'],
					'quantity'						    => $data_transaction_item['quantity'],
					'warehouse_id'				        => $transaction['warehouse_id'],
				]);

				if ($transaction_item) {
					$product_stock = ProductStockModel::where([
						'product_id'		=> $transaction_item->product_id,
						'warehouse_id'	    => $transaction['warehouse_id'],
					])->first();

                    $product_stock->last_stock = $product_stock->last_stock-$transaction_item->quantity;
                    $product_stock->out_stock = $transaction_item->quantity;
                    $product_stock->save();

                    $product = ProductModel::where('id', $data_transaction_item['product_id'])->get();

                    foreach($product as $data_product) {
                        $buyer_count = $data_product->buyer_count;

                        $updated_data = [
                            'buyer_count' => $buyer_count + 1
                        ];

                        $this->db->where('id', $data_product->id)->update('product', $updated_data);
                    }
				}
			}
		}

		return $transaction_inserted;
	}

	public function submit()
	{
		$this->load->model(['TransactionModel','CartModel','TransactionItemModel','ShippingModel','CustomerAddressModel','ProductStockModel', 'VoucherModel', 'WilayahProvinsiModel']);

		$customer_address = CustomerAddressModel::find($this->input->post('customer_address'));
		$customer_address->provinsi();

		$transaction_nominal = 0;
		$preorder_total = 0;
		$cart_ids = [];

		foreach ($this->input->post('cart_items') as $key => $cart_item) {
			$_carts = CartModel::whereIn('id', explode(',', $cart_item['cart_id']))->get();

			$weight = 0;
			$sub_total = 0;
			$data_transaction_items = [];

			foreach ($_carts as $cart) {

				$cart_ids[] = $cart->id;

				$weight += $cart->product->weight * $cart->quantity;

				if (isset($cart->product->discount)) {
					$sub_total += $cart->product->discount * $cart->quantity;
				} else {
					$sub_total += $cart->product->price * $cart->quantity;
				}

				$data_transaction_items[] = [
					'product_id'		=> $cart->product_id,
					'price'					=> isset($cart->product->discount) ? $cart->product->discount : $cart->product->price,
					'quantity'			=> $cart->quantity,
				];
			}

			$warehouse = $_carts[0]->warehouse;
			$data_ongkir = $this->input->post('shipping_fee');
			$nominal = $sub_total + $data_ongkir->value;
			$transaction_nominal += $nominal;

			$payment_status = "unpaid";

			$date_transaction_format = "Ymd-hi";

			$data_transaction = [
				'transaction_id'				=> 'BISA-ORD'.date($date_transaction_format).$this->ion_auth->user()->row()->id,
				'user_id'								=> $this->ion_auth->user()->row()->id,
				'customer_address_id'		=> $this->input->post('customer_address'),
				'customer_address_data'	=> json_encode( $customer_address ),
				'shipping_fee'					=> $data_ongkir,
				'shipping_courier'			=> $this->input->post('shipping_name'),
				'payment_status'				=> $payment_status,
				'weight'								=> $this->input->post('weight')
			];

			$transaction_data[] = [
				'nominal'				=> $nominal,
				'warehouse_id'	        => $warehouse->id,
				'data' => [
					'transaction'				=> $data_transaction,
					'transaction_items'	        => $data_transaction_items,
//					'is_preorder'				=> (in_array(true, $is_preorder)) ? true : false,
				]
			];

//			if (in_array(true, $is_preorder)) {
//				$preorder_total++;
//			}
		}

//		$is_transaction_valid = [];

//		if ($preorder_total == 0 || $preorder_total == count($transaction_data)) {
//			$is_preorder = ($preorder_total == 0) ? false : true;
//			$transaction_payment = $this->transaction_payment($is_preorder, $transaction_nominal);
//
//			if ($transaction_payment) {
//				foreach ($transaction_data as $transaction) {
//					$result = $this->save_transaction($transaction, $transaction_payment);
//					$is_transaction_valid[] = ($result) ? true : false;
//				}
//			}
//		} else {
//
//
//		}

        $transaction_inserted = null;

        foreach ($transaction_data as $transaction) {
            $transaction_payment = $this->transaction_payment();

            if ($transaction_payment) {
                $transaction_inserted = $this->save_transaction( $transaction, $transaction_payment );

//                $is_transaction_valid[] = ($result) ? true : false;
            }
        }

        // Set your Merchant Server Key
            \Midtrans\Config::$serverKey = 'Mid-server-rysSEBxkEPaK1p-OF6PHYj5d';
        // Set to Development/Sandbox Environment (default). Set to true for Production Environment (accept real transaction).
                \Midtrans\Config::$isProduction = true;
        // Set sanitization on (default)
                \Midtrans\Config::$isSanitized = true;
        // Set 3DS transaction for credit card to true
                \Midtrans\Config::$is3ds = true;

        $params = array(
            'transaction_details' => array(
                'order_id'      => $transaction_inserted->id,
                'gross_amount'  => $transaction_inserted->grand_total($transaction_inserted->id),
            ),
            'customer_details' => array(
                'first_name'    => $transaction_inserted->user->first_name,
                'last_name'     => $transaction_inserted->user->last_name,
                'email'         => $transaction_inserted->user->email,
                'phone'         => $transaction_inserted->user->phone,
            ),
        );

        $snapToken = \Midtrans\Snap::getSnapToken($params);

        $response = [
            'status' 	=> 'success',
            'redirect'	=> ($preorder_total == count($transaction_data)) ? base_url('member/order/preorder') : base_url('member/order'),
            'payment_token'  => $snapToken
        ];

        json_generator($response);
	}

	public function set_shipping_address() {
		$this->load->model('CustomerAddressModel');

		$id = $this->input->post('id');

		$shipping_address = CustomerAddressModel::where([
			'user_id' 						=> $this->ion_auth->user()->row()->id,
			'is_shipping_address'	=> 'yes'
		]
		)->first();

		if ($shipping_address) {
			CustomerAddressModel::updateOrCreate(
				[
					'id' => $shipping_address->id
				],
				[
					'is_shipping_address'	=> 'no'
				]
			);
		}

		$is_updated = CustomerAddressModel::updateOrCreate(
			[
				'id' => $id
			],
			[
				'is_shipping_address'	=> 'yes'
			]
		);

		if ($is_updated) {
			echo json_encode('success');
		}
	}

    public function show_payment_token()
    {
        $this->load->model(['TransactionModel']);

        $transaction_model = TransactionModel::find($this->input->post('id'));

        // Set your Merchant Server Key
        \Midtrans\Config::$serverKey = 'Mid-server-rysSEBxkEPaK1p-OF6PHYj5d';
        // Set to Development/Sandbox Environment (default). Set to true for Production Environment (accept real transaction).
        \Midtrans\Config::$isProduction = true;
        // Set sanitization on (default)
        \Midtrans\Config::$isSanitized = true;
        // Set 3DS transaction for credit card to true
        \Midtrans\Config::$is3ds = true;

        $params = array(
            'transaction_details' => array(
                'order_id'      => $transaction_model->id,
                'gross_amount'  => $transaction_model->total(),
            ),
            'customer_details' => array(
                'first_name'    => $transaction_model->user->first_name,
                'last_name'     => $transaction_model->user->last_name,
                'email'         => $transaction_model->user->email,
                'phone'         => $transaction_model->user->phone,
            ),
        );

        $snapToken = \Midtrans\Snap::getSnapToken($params);

        $response = [
            'status' 	=> 'success',
            'payment_token'  => $snapToken
        ];

        json_generator($response);
    }
}
