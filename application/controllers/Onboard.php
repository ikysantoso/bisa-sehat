<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Onboard extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		if ($this->ion_auth->is_admin()) {
			redirect(base_url('admin/dashboard'), 'refresh');
		}
	}
	public function index()
	{
		$this->load->helper('cookie');

		$cookie = get_cookie('access');

		if ($cookie == 'true') {
			$this->load->helper('url');

			redirect(base_url('home'), 'refresh');
		} else {
			set_cookie('access', 'true', 7200);

			$data = [
				'page'	=> 'onboard',
			];

			$this->load->view('template-part/header', $data);
			$this->load->view('template-part/navbar', $data);
			$this->load->view('app/frontpage/onboard', $data);
			$this->load->view('template-part/footer', $data);
		}
	}
}
