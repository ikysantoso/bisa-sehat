<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends CI_Controller {

	public function __construct(){
		parent::__construct();

		$this->load->model('VoucherModel');

		if (!$this->ion_auth->logged_in()) {
			redirect(base_url('auth/login'), 'refresh');
		} else if ($this->ion_auth->is_admin()) {
			redirect(base_url('admin/dashboard'), 'refresh');
		}
	}

	public function index()
	{
		$this->load->model('TransactionModel');

		$data = [
			'page'				=> 'member-order',
			'page_title'		=> 'Order',
			'transactions'      => TransactionModel::where('user_id', $this->ion_auth->user()->row()->id)->orderBy('id','desc')->get(),
            'scripts'			=> [
                base_url( 'assets/js/app/user/checkout.js' )
            ],
		];

		$this->load->view('template-part/header', $data);
		$this->load->view('template-part/navbar', $data);
		$this->load->view('app/member-dashboard/order/order', $data);
		$this->load->view('template-part/footer', $data);
	}

	public function detail($id = null)
	{
		$this->load->model('TransactionModel', 'WarehouseModel');

		$transaction = TransactionModel::find($id);
		$transactionItem = $transaction->transaction_items()->first();
		$warehouseData = WarehouseModel::where( 'id', $transactionItem->warehouse_id )->first();
		if ($id && $transaction) {
			$data = [
				'page'				=> 'member-order',
				'isBackDetail'      => 'true',
				'page_title'		=> 'Order',
				'warehouse'			=> $warehouseData,
				'transaction'		=> $transaction,
				'transactions'	    => TransactionModel::where( 'user_id', $this->ion_auth->user()->row()->id )->get(),
				'scripts'			=> [
					base_url('node_modules/sweetalert2/dist/sweetalert2.all.min.js'),
					base_url( 'assets/js/app/user/dashboard/order-detail.js' ),
				]
			];

			$this->load->view('template-part/header', $data);
			$this->load->view('template-part/navbar', $data);
			$this->load->view('app/member-dashboard/order/order-detail', $data);
			$this->load->view('template-part/footer', $data);
		}
	}

	public function payment_confirmation()
	{
		$this->load->model('TransactionModel');
		$this->load->library('form_validation');

		$this->form_validation->set_rules('id', 'id', 'required');

		if (
			$this->form_validation->run() == TRUE
			&& $transaction = TransactionModel::where(['id' => $this->input->post('id'), 'user_id' => $this->ion_auth->user()->row()->id])->first()
		){
			$config['upload_path']			= "./assets/images/transactions/product";
			$config['allowed_types']		= 'jpeg|jpg|png';
			$config['max_size']             = 1000;

			$file_name = 'Product ' . rand();
			$encrypted_file_name = md5($file_name);
			$config['file_name'] = $encrypted_file_name;

			$this->load->library('upload',$config);

			if ($this->upload->do_upload('file')) {
				$upload_data = $this->upload->data();
				$payment_image_name = $upload_data['file_name'];

				$transaction->payment->update([
					'status' => 'waiting'
				]);

				$transaction->update([
					'payment_image' => $payment_image_name
				]);

				$response = array('code' => '230', 'status' => 'success', 'title'=> 'Pengiriman Berhasil',
					'message' => 'Bukti pembayaran anda akan segera kami verifikasi');

				echo json_encode($response);
			} else {
				$response = array('code' => '400', 'status' => 'error', 'title' => 'Pengiriman Gagal',
									'message' => 'Bukti pembayaran anda tidak dapat kami terima');

				echo json_encode($response);
			}
		}
	}

	public function wishorder()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('draw', 'draw', 'required');

		if ($this->form_validation->run() == TRUE){
			$tablename = 'wish_order';
			$this->load->model('datatables');

			$selected_column = array(
				"created_at"	=> 'created_at',
				"name"			=> 'name',
				"price"			=> 'price',
				"quantity"		=> 'quantity',
				"unit"			=> 'unit',
				"product_id"	=> 'product_id',
				"id"			=> 'id',
			);

			$column_order = array(null, 'created_at', 'name', 'quantity', 'unit');
			$column_search = array('created_at', 'name', 'quantity', 'unit');
			$order = array('id' => 'desc');

			$where = [
				'user_id'	=> $this->ion_auth->user()->row()->id
			];

			$custom_column = '<button class="btn btn-sm btn-danger btn-delete"><i class="fas fa-trash"></i></button>';
			$this->datatables->get_datatables($tablename, $selected_column, $column_order, $column_search, $order, $where, $custom_column);

		} else {
			$data = [
				'page'				=> 'member-wishorder',
				'page_title'	=> 'Wishorder',
				'scripts'			=> [
					'https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js',
					'https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js',
					'https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js',
					'https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js',
					base_url('node_modules/sweetalert2/dist/sweetalert2.all.min.js'),
					base_url( 'assets/js/app/user/wishorder/wishorder-list.js' ),
				],
				'styles'			=> [
					'https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css',
					'https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap4.min.css',
				]
			];

			$this->load->view('template-part/header', $data);
			$this->load->view('template-part/navbar', $data);
			$this->load->view('app/member-dashboard/order/wishorder', $data);
			$this->load->view('template-part/footer', $data);
		}
	}

	public function wishorder_add()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('name', 'name', 'required');
		$this->form_validation->set_rules('price', 'price', 'required');
		$this->form_validation->set_rules('quantity', 'quantity', 'required');
		$this->form_validation->set_rules('unit', 'unit', 'required');
		$this->form_validation->set_rules('description', 'description', 'required');

		if ($this->form_validation->run() == TRUE) {
			$data_insert = [
				'user_id'			=> $this->ion_auth->user()->row()->id,
				'name'				=> $this->input->post('name'),
				'price'				=> $this->input->post('price'),
				'quantity'		    => $this->input->post('quantity'),
				'unit'				=> $this->input->post('unit'),
				'description'	    => $this->input->post('description')
			];

			$this->load->model('WishOrderModel');

			$wishorder = WishOrderModel::create($data_insert);
			json_generator($wishorder);
		} else {
			$data = [
				'page'				=> 'member-wishorder',
				'isBackDetail'      => 'true',
				'page_title'	    => 'Wishorder',
				'scripts'			=> [
					'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js',
					base_url('node_modules/sweetalert2/dist/sweetalert2.all.min.js'),
					base_url( 'assets/js/app/user/wishorder/wishorder-add.js' ),
				]
			];

			$this->load->view('template-part/header', $data);
			$this->load->view('template-part/navbar', $data);
			$this->load->view('app/member-dashboard/order/wishorder-add', $data);
			$this->load->view('template-part/footer', $data);
		}
	}

	public function wishorder_delete()
	{
		$this->load->model('WishOrderModel');

		$this->load->library('form_validation');

		$this->form_validation->set_rules('id', 'id', 'required');

		if (
			$this->form_validation->run() == TRUE
			&& $wishorder = WishOrderModel::where([
				'id'			=> $this->input->post('id'),
				'user_id'	=> $this->ion_auth->user()->row()->id
			])->first()
		){
			json_generator($wishorder->forceDelete());
		}
	}

	public function preorder()
	{
		$this->load->model('TransactionModel');

		$data = [
			'page'				=> 'member-preorder',
			'isBackDetail'      => 'true',
			'page_title'		=> 'Preorder',
			'transactions'	    => TransactionModel::where([
			'user_id'           => $this->ion_auth->user()->row()->id, 'is_preorder' => 'yes'
				                ])->orderBy('id','desc')->get()
		];

		$this->load->view('template-part/header', $data);
		$this->load->view('template-part/navbar', $data);
		$this->load->view('app/member-dashboard/order/preorder', $data);
		$this->load->view('template-part/footer', $data);
	}

	public function preorder_detail($id = null)
	{
		$this->load->model('TransactionModel');

		$transaction = TransactionModel::find($id);

		if ($id && $transaction) {
			$data = [
				'page'			  => 'member-preorder',
				'isBackDetail'    => 'true',
				'page_title'	  => 'Preorder',
				'transaction'	  => $transaction,
				'transactions'	  => TransactionModel::where( 'user_id', $this->ion_auth->user()->row()->id )->get(),
				'scripts'		  => [
					base_url('node_modules/sweetalert2/dist/sweetalert2.all.min.js'),
					base_url('assets/js/app/user/dashboard/order-detail.js'),
				]
			];

			$this->load->view('template-part/header', $data);
			$this->load->view('template-part/navbar', $data);
			$this->load->view('app/member-dashboard/order/order-detail', $data);
			$this->load->view('template-part/footer', $data);
		}
	}

	public function transaction_confirmation()
	{
		$this->load->model('TransactionModel');
		$this->load->library('form_validation');
		$this->load->library('my_email');

		$this->form_validation->set_rules('id', 'id', 'required');

		if (
			$this->form_validation->run() == TRUE
			&& $transaction = TransactionModel::where([
				'id'      => $this->input->post('id'), 
				'user_id' => $this->ion_auth->user()->row()->id
				])->first()
		){
			json_generator( $transaction->update([
				'status' => 'delivered'
			]));
		}

		$data = [
			'order'	    => $transaction,
			'name'	    => $this->ion_auth->user()->row()->username,
			'email'	    => $this->ion_auth->user()->row()->email,
		];

		$this->my_email->order_arrived($data);

		return $transaction;
	}

	public function repeat_order($transaction_id = null)
	{
		$this->load->model(['TransactionModel', 'CartModel']);

		$transaction = TransactionModel::find($transaction_id);

		if ($transaction_id && $transaction) {
			$data_cart = [];

			foreach ($transaction->transaction_items as $key => $transaction_item) {
				$data_cart[] = [
					'user_id'			=> $this->ion_auth->user()->row()->id,
					'product_id'		=> $transaction_item->product_id,
					'quantity'			=> $transaction_item->quantity,
					'warehouse_id'	    => $transaction_item->warehouse_id,
				];
			}

			$cart = CartModel::insert($data_cart);

			if ($cart) {
				redirect(base_url('cart'), 'refresh');
			}
		}
	}

    public function activation_phone()
    {
        log_message('debug', 'oke');
        $this->load->model(['UserModel']);

        $user = UserModel::where('username', $this->input->post('username_phone_activation'))->first();

        if ($this->input->post('activation_phone_code') == $user->phone_verification_code) {
            $user->phone_verified = 1;
            $user->save();

            json_generator('success');
        }
    }

    public function request_phone_activation()
    {
        $this->load->model(['UserModel']);
        $user = UserModel::where('username', $this->input->post('username'))->first();
        $user->phone_verification_code = rand(111111, 99999);
        $user->save();

        json_generator('success');
    }
}
