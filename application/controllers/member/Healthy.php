<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Healthy extends CI_Controller {

	public function __construct(){
		parent::__construct();

		if (!$this->ion_auth->logged_in()) {
			redirect(base_url('auth/login'), 'refresh');
		} else if ($this->ion_auth->is_admin()) {
			redirect(base_url('admin/dashboard'), 'refresh');
		}
	}

	public function status(){
		$data = [
			'page'			=> 'member-healthy-status',
			'page_title'	=> 'Status Kesehatan & Kebugaran',
			'scripts'		=> [
				base_url('node_modules/sweetalert2/dist/sweetalert2.all.min.js'),
				'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js',
				base_url('assets/js/app/user/dashboard/fitness.js'),
			]
		];

		$this->load->view('template-part/header', $data);
		$this->load->view('template-part/navbar', $data);
		$this->load->view('app/member-dashboard/healthy/healthy-status', $data);
		$this->load->view('template-part/footer', $data);
	}

	public function status_download(){
		$this->load->library('my_pdf');
		$this->my_pdf->healty_status_report( ['user_id' => $this->ion_auth->user()->row()->id] );
	}
}