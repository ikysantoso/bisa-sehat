<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {

	public function __construct(){
		parent::__construct();

		if (!$this->ion_auth->logged_in()) {
			redirect(base_url('auth/login'), 'refresh');
		} else if ($this->ion_auth->is_admin()) {
			redirect(base_url('admin/dashboard'), 'refresh');
		}
	}

	public function index(){
		redirect(base_url('member/profile/edit'), 'refresh');
	}

	public function edit(){
		$user = UserModel::find( $this->ion_auth->user()->row()->id );

		$this->db->from('corporate');
		$this->db->where('status', 'approved');
		$this->db->order_by('name', 'asc');
		$corporateResults = $this->db->get();

		$data = [
			'user_info' 	=> $user,
			'isBackDetail'  => 'true',
			'scripts'	    => [
				'https://cdnjs.cloudflare.com/ajax/libs/fastselect/0.7.3/fastselect.standalone.min.js',
				'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js',
				base_url('node_modules/sweetalert2/dist/sweetalert2.all.min.js'),
				base_url('node_modules/gijgo/js/gijgo.min.js'),
				base_url( 'assets/js/app/user/dashboard/profile-edit.js' ),
			],
			'styles'	    => [
				'https://cdnjs.cloudflare.com/ajax/libs/fastselect/0.7.3/fastselect.min.css',
				base_url('node_modules/gijgo/css/gijgo.min.css'),
			],
			'corporates'	=> $corporateResults->result(),
		];

		$this->load->view('template-part/header', $data);
		$this->load->view('template-part/navbar', $data);
		$this->load->view('app/member-dashboard/profile/profile-edit.php', $data);
		$this->load->view('template-part/footer', $data);
	}

	public function update(){
		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'nik', 'nik' );
		$this->form_validation->set_rules( 'first_name', 'first_name', 'required' );
		$this->form_validation->set_rules( 'gender', 'gender', 'required' );
		$this->form_validation->set_rules( 'date-birth', 'date-birth', 'required' );
		$this->form_validation->set_rules( 'blood-type', 'blood-type', 'required' );
		$this->form_validation->set_rules( 'phone', 'phone', 'required' );
		$this->form_validation->set_rules( 'religion', 'religion', 'required' );
		$this->form_validation->set_rules( 'job', 'job', 'required' );
		if ($this->form_validation->run() === TRUE){

			$this->load->model( 'UserMetaModel' );

			$birth= date('m/d/Y', strtotime($this->input->post('date-birth')));
			$main_update = [
				'first_name'	=> $this->input->post('first_name'),
				'last_name'		=> $this->input->post('last_name'),
				'phone'			=> $this->input->post('phone'),
			];

			$meta_update = [
				'nik'			=> $this->input->post('nik'),
				'religion'		=> $this->input->post('religion'),
				'job'			=> $this->input->post('job'),
				'gender'		=> $this->input->post('gender'),
				'date-birth'	=> $birth,
				'blood-type'	=> $this->input->post('blood-type'),
			];

			// check nik if exist
			$nik_exist = UserMetaModel::where(
				[
					'meta_key' 		=> 'nik',
					'meta_value' 	=> $this->input->post('nik'),

				]
			)->where('user_id', '!=', $this->ion_auth->user()->row()->id)->first();

			$user = UserModel::find( $this->ion_auth->user()->row()->id );

			if($this->input->post('nik')!=''){
			if ( $nik_exist ) {
					json_generator(
						[
							'text' 	=> 'NIK telah digunakan',
							'field'	=> 'nik'
						],
						409
					);
			}
			}
			$phone_exist = UserModel::where('phone', $this->input->post('phone'))->where('id', '!=', $this->ion_auth->user()->row()->id)->first();
			if ( $phone_exist  ) {
					json_generator(
						[
							'text' 	=> 'Nomor HP telah digunakan',
							'field'	=> 'phone'
						],
						409
					);
			}

			if($this->input->post('nik')==''){
			if (!$phone_exist  ) {
				foreach ($meta_update as $key => $item) {
					UserMetaModel::updateOrCreate(
						[
							'meta_key'		=> $key,
							'user_id'		=> $this->ion_auth->user()->row()->id
						],
						[
							'meta_value'	=> $item
						]
					);
				}

				$result = $user->update( $main_update );
				json_generator( $result );
			}				
			}else{
			if ( !$nik_exist && !$phone_exist  ) {
				foreach ($meta_update as $key => $item) {
					UserMetaModel::updateOrCreate(
						[
							'meta_key'		=> $key,
							'user_id'		=> $this->ion_auth->user()->row()->id
						],
						[
							'meta_value'	=> $item
						]
					);
				}

				$result = $user->update( $main_update );
				json_generator( $result );
			}
			}
		}
	}
}