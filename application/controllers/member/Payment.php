<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment extends CI_Controller
{
    public function __construct() {
        parent::__construct();
    }

    public function callback()
    {
        // Set your Merchant Server Key
        \Midtrans\Config::$serverKey = 'Mid-server-rysSEBxkEPaK1p-OF6PHYj5d';
        // Set to Development/Sandbox Environment (default). Set to true for Production Environment (accept real transaction).
        \Midtrans\Config::$isProduction = true;

        $notif = new \Midtrans\Notification();

        $transaction_status = $notif->transaction_status;
        $type = $notif->payment_type;
        $order_id = $notif->order_id;
        $fraud = $notif->fraud_status;

        if ($fraud == 'accept') {
            $transaction_model = TransactionModel::find($order_id);

            if (isset($transaction_model->payment)) {
                $transaction_model->payment->status = 'paid';
                $transaction_model->payment->save();
            }

            $transaction_model->payment_status = 'paid';
            $transaction_model->status = 'pengemasan';
            $transaction_model->save();
        }
    }
}