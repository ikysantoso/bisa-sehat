<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	private $incoBaseApi;

	public function __construct(){
		parent::__construct();

		if (!$this->ion_auth->logged_in()) {
			redirect(base_url('auth/login'), 'refresh');
		} else if ($this->ion_auth->is_admin()) {
			redirect(base_url('admin/dashboard'), 'refresh');
		}

		$this->db2 = $this->load->database('village_engine', TRUE);
		$this->incoBaseApi = $this->config->item('inco_base_api');
	}

	public function index()
	{
		$this->load->model( ['UserModel', 'CustomerBankModel', 'CustomerAddressModel'] );

        $allCompanyUrl = $this->incoBaseApi . '/company';
        $getExistVerificationRequestUrl = $this->incoBaseApi . '/employee/verification/request/search/nik';
        $getAlreadyEmployeeRegisteredUrl = $this->incoBaseApi . '/employee/nik/search';
        $getDeleteEmployeeRequestUrl = $this->incoBaseApi . '/employee/delete/request/search/nik';

        $allCompanyAPIResponse = $this->hitIncoAPI($allCompanyUrl, null);

        $birthdate = UserMetaModel::where('user_id',$this->ion_auth->user()->row()->id)->where('meta_key', 'date-birth')->first();
        $nik = UserMetaModel::where('user_id',$this->ion_auth->user()->row()->id)->where('meta_key', 'nik')->first();
        $fullname = $this->ion_auth->user()->row()->first_name . ' ' . $this->ion_auth->user()->row()->last_name;

        $getExistVerificationRequestAPIResponse = $this->hitIncoAPI($getExistVerificationRequestUrl, array(
            'nik' => $nik->meta_value
        ));

       $getAlreadyEmployeeRegistered = $this->hitIncoAPI($getAlreadyEmployeeRegisteredUrl, array(
           'nik' => $nik->meta_value
       ));
       $getDeleteEmployeeRequest = $this->hitIncoAPI($getDeleteEmployeeRequestUrl, array(
           'nik' => $nik->meta_value
       ));
       $primary_address = CustomerAddressModel::where( ['user_id' => $this->ion_auth->user()->row()->id, 'is_primary_address' => 'yes'] )->first();
		if($primary_address->village_id==null and $primary_address->subdistrict_id==null){
			$village='';
			$sub_districts='';
			$districts='';
			$province='';
		}else{
		if($primary_address->village_id==null){
    	$village = $this->db2->query("SELECT * FROM ward as p where p.sdid =$primary_address->subdistrict_id ")->row();
    	}else{
    	if(is_numeric($primary_address->village_id)){
        $village = $this->db2->query("SELECT * FROM ward as p where p.wid =$primary_address->village_id ")->row();
    	}else{
    		$village = $this->db2->query("SELECT * FROM ward as p where p.sdid =$primary_address->subdistrict_id")->row();
    	}
    	}
		$sub_districts = $this->db2->query("SELECT * FROM sub_districts as p where p.sdid =$village->sdid ")->row();
		$districts = $this->db2->query("SELECT * FROM districts as p where p.did =$sub_districts->did ")->row();
		$province = $this->db2->query("SELECT * FROM province as p where p.pid =$districts->pid ")->row();
		}
		$data = [
			'page'		       => 'member-dashboard',
			'customerbanks'	     => CustomerBankModel::where(['user_id' => $this->ion_auth->user()->row()->id])->get(),
            'customer_addresses' => CustomerAddressModel::where( 'user_id', $this->ion_auth->user()->row()->id )->get(),
			'companies'          => json_decode($allCompanyAPIResponse, true),
            'exist_request'      => json_decode($getExistVerificationRequestAPIResponse, true),
            'already_employee'   => json_decode($getAlreadyEmployeeRegistered, true),
            'delete_request'   => json_decode($getDeleteEmployeeRequest, true),
			'user_info'	       => UserModel::find( $this->ion_auth->user()->row()->id ),
			'primary_address'  => $primary_address,
			'village'	=> $village,
			'sub_districts'	=> $sub_districts,
			'districts'	=> $districts,
			'province'	=> $province,
            'nik'   => $nik->meta_value,
            'fullname'  => $fullname,
            'birthdate'          => $birthdate->meta_value,
			'scripts'	=> [
				'https://cdnjs.cloudflare.com/ajax/libs/fastselect/0.7.3/fastselect.standalone.min.js',
				'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js',
                base_url( 'assets/js/app/user/dashboard/order-detail.js' ),
				base_url('node_modules/sweetalert2/dist/sweetalert2.all.min.js'),
                base_url( 'assets/js/app/user/setting/bank.js'),
                base_url( 'assets/js/app/user/setting/company.js'),
                base_url( 'assets/js/app/user/dashboard/address-management.js'),
			],
			'styles'	=> [
				'https://cdnjs.cloudflare.com/ajax/libs/fastselect/0.7.3/fastselect.min.css',
			],
		];

		$this->load->view('template-part/header', $data);
		$this->load->view('template-part/navbar', $data);
		$this->load->view('app/member-dashboard/member-dashboard', $data);
		$this->load->view('template-part/footer', $data);
	}

	public function savebank(){
		$this->load->library( 'form_validation' );

		$this->form_validation->set_rules( 'name', 'name', 'required' );
		$this->form_validation->set_rules( 'account_name', 'account_name', 'required' );
		$this->form_validation->set_rules( 'account_number', 'account_number', 'required' );

		if ( $this->form_validation->run() === TRUE ){
			$data_insert = [
				'name'				=> $this->input->post('name'),
				'account_name'		=> $this->input->post('account_name'),
				'account_number'	=> $this->input->post('account_number'),
				'user_id'			=> $this->ion_auth->user()->row()->id,
			];

			$this->load->model( 'CustomerBankModel' );
			$result = CustomerBankModel::create( $data_insert );
			json_generator( $result );
		}
	}

	public function deletebank(){
		$this->load->library( 'form_validation' );
		$this->load->model('CustomerBankModel');

		$this->form_validation->set_rules( 'id', 'id', 'required' );

		if (
			$this->form_validation->run() === TRUE &&
			$bank = CustomerBankModel::where(
				[
					'id'	=> $this->input->post('id'),
					'user_id'	=> $this->ion_auth->user()->row()->id
				]
			)->first()
		){
			json_generator( $bank->forceDelete(), 200 );
		}
	}

	public function save_address(){
		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'name', 'name', 'required' );
		$this->form_validation->set_rules( 'village', 'village', 'required' );
		$this->form_validation->set_rules( 'address', 'address', 'required' );
		$this->form_validation->set_rules( 'zipcode', 'zipcode', 'required' );
		$this->form_validation->set_rules( 'phone', 'phone', 'required' );
		$this->form_validation->set_rules( 'is_primary_address', 'is_primary_address', 'required' );
		if ($this->form_validation->run() === TRUE){
			$this->load->model( 'CustomerAddressModel' );

			// update all primary address to "no"
			if ( $this->input->post( 'is_primary_address' ) == 'yes' ) {
				CustomerAddressModel::where( 'user_id', $this->ion_auth->user()->row()->id )->update( ['is_primary_address' => 'no'] );
			}

			$data_insert = [
				'name'					=> $this->input->post('name'),
				'receiver_name'			=> $this->input->post('receiver_name'),
				'phone'					=> $this->input->post('phone'),
				'subdistrict_id'		=> $this->input->post('sub-district'),
				'village_id'			=> $this->input->post('village'),
				'address'				=> $this->input->post('address'),
				'zipcode'				=> $this->input->post('zipcode'),
				'user_id'				=> $this->ion_auth->user()->row()->id,
				'is_primary_address'	=> $this->input->post( 'is_primary_address' ),
			];

			$result = $this->CustomerAddressModel->create( $data_insert );
			json_generator( $result );
		}
	}

	public function update_address(){
		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'id', 'id', 'required' );
		$this->form_validation->set_rules( 'name', 'name', 'required' );
		$this->form_validation->set_rules( 'village', 'village', 'required' );
		$this->form_validation->set_rules( 'address', 'address', 'required' );
		$this->form_validation->set_rules( 'zipcode', 'zipcode', 'required' );
		$this->form_validation->set_rules( 'phone', 'phone', 'required' );
		$this->form_validation->set_rules( 'is_primary_address', 'is_primary_address', 'required' );
		if ($this->form_validation->run() === TRUE){
			$this->load->model( 'CustomerAddressModel' );

			// update all primary address to "no"
			if ( $this->input->post( 'is_primary_address' ) == 'yes' ) {
				CustomerAddressModel::where( 'user_id', $this->ion_auth->user()->row()->id )->update( ['is_primary_address' => 'no'] );
			}

			$data_update = [
				'name'					=> $this->input->post('name'),
				'receiver_name'			=> $this->input->post('receiver_name'),
				'phone'					=> $this->input->post('phone'),
				'subdistrict_id'		=> $this->input->post('sub-district'),
				'village_id'			=> $this->input->post('village'),
				'address'				=> $this->input->post('address'),
				'zipcode'				=> $this->input->post('zipcode'),
				'user_id'				=> $this->ion_auth->user()->row()->id,
				'is_primary_address'	=> $this->input->post( 'is_primary_address' ),
			];

			$result = CustomerAddressModel::find( $this->input->post('id') )->update( $data_update );
			json_generator( $result );
		}
	}

	public function address_delete(){
		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'id', 'id', 'required' );
		if ($this->form_validation->run() === TRUE){
			$this->load->model( 'CustomerAddressModel' );
			$result = CustomerAddressModel::find( $this->input->post('id') )->forceDelete();
			json_generator( $result );
		}
	}

	public function address_detail(){
		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'id', 'id', 'required' );
		if ($this->form_validation->run() === TRUE){

			$this->load->model( 'CustomerAddressModel' );

			$_customer_address = CustomerAddressModel::where([
				'id'		=> $this->input->post('id'),
				'user_id'	=> $this->ion_auth->user()->row()->id,
			])->first();

		if($_customer_address->village_id!=null){
        $village = $this->db2->query("SELECT * FROM ward as p where p.wid =$_customer_address->village_id ")->row();
    	}else{
    	$village = $this->db2->query("SELECT * FROM ward as p where p.sdid =$_customer_address->subdistrict_id ")->row();
    	}
		$sub_districts = $this->db2->query("SELECT * FROM sub_districts as p where p.sdid =$village->sdid ")->row();
		$districts = $this->db2->query("SELECT * FROM districts as p where p.did =$sub_districts->did ")->row();
		$province = $this->db2->query("SELECT * FROM province as p where p.pid =$districts->pid ")->row();
			$customer_address = [
				'id'					=> $_customer_address->id,
				'name'					=> $_customer_address->name,
				'receiver_name'			=> $_customer_address->receiver_name,
				'village_id'			=> $_customer_address->village_id,
				'village_text'			=> $_customer_address->village_text,
				'subdistrict_id'		=> $_customer_address->subdistrict_id,
				'address'				=> $_customer_address->address,
				'zipcode'				=> $_customer_address->zipcode,
				'phone'					=> $_customer_address->phone,
				'is_primary_address'	=> $_customer_address->is_primary_address,
				'province'				=> [ 'value' => $province->pid, 'text' => $province->province_name],
				'district'				=> [ 'value' => $districts->did, 'text' => $districts->districts_name],
				'subdistrict'			=> [ 'value' => $sub_districts->sdid, 'text' => $sub_districts->sub_districts_name],
				'village'				=> [ 'value' => $village->wid, 'text' => $village->ward_name]
			];

			header('Content-Type: application/json');
			echo json_encode($customer_address);
		}
	}

    private function hitIncoAPI($url, $params)
    {
        $ch = curl_init();

        if (isset($params)) {
            $fullUrl = $url . '?' . http_build_query($params);
            log_message('error', $fullUrl);
            curl_setopt($ch, CURLOPT_URL, $fullUrl);
        } else {
            curl_setopt($ch, CURLOPT_URL, $url);
        }

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json')
        );
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        return curl_exec($ch);
    }
}
