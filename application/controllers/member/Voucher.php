<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Voucher extends CI_Controller {

	public function __construct(){
		parent::__construct();

		$this->load->model( ['VoucherModel','VoucherUserModel', 'UserModel', 'SharedVoucher', 'ClaimedVoucher'] );

		if (!$this->ion_auth->logged_in()) {
			redirect(base_url('auth/login'), 'refresh');
		} else if ($this->ion_auth->is_admin()) {
			redirect(base_url('admin/dashboard'), 'refresh');
		}
	}

	public function index(){
		date_default_timezone_set('Asia/Jakarta');
		$today_date = strtotime(date("Y-m-d"));
		$data = [
			'page'			=> 'member-voucher',
			'page_title'	=> 'My Voucher',
			'vouchers_available'		=> $this->db->query("SELECT v.*, vu.voucher_code
												FROM voucher v
												LEFT JOIN voucher_user vu on v.id=vu.voucher_id
												WHERE vu.user_id = ".$this->ion_auth->user()->row()->id."
												AND  NOW() between v.start_date and v.expired
												AND v.id not in (select t.voucher_id from transaction t where t.user_id =".$this->ion_auth->user()->row()->id." and t.voucher_id IS NOT NULL)
												")->result(),
			'vouchers_soon'		=> $this->db->query("SELECT v.*, vu.voucher_code
												FROM voucher v
												LEFT JOIN voucher_user vu on v.id=vu.voucher_id
												WHERE vu.user_id = ".$this->ion_auth->user()->row()->id."
												and  v.start_date > NOW()
												")->result(),
			'vouchers_used'		=> $this->db->query("SELECT v.*, vu.voucher_code
												FROM voucher v
												LEFT JOIN voucher_user vu on v.id=vu.voucher_id
												WHERE vu.user_id = ".$this->ion_auth->user()->row()->id."
												AND v.id in (select t.voucher_id from transaction t where t.user_id =".$this->ion_auth->user()->row()->id." and t.voucher_id IS NOT NULL)
												")->result(),
			'vouchers_expired'		=> $this->db->query("SELECT v.*, vu.voucher_code
												FROM voucher v
												LEFT JOIN voucher_user vu on v.id=vu.voucher_id
												WHERE vu.user_id = ".$this->ion_auth->user()->row()->id."
												and  v.expired < NOW()
												")->result(),

			'vouchers'		=> VoucherUserModel::where('user_id', $this->ion_auth->user()->row()->id)->get(),
			'scripts'		=> [
				base_url( 'assets/js/app/user/dashboard/voucher.js' ),
			]
		];

		$this->load->view('template-part/header', $data);
		$this->load->view('template-part/navbar', $data);
		$this->load->view('app/member-dashboard/voucher/voucher-list', $data);
		$this->load->view('template-part/footer', $data);
	}

	public function claim()
	{
		$id = $this->input->post('voucherId');

		$voucher = SharedVoucher::where('id', $id)->get();

		$shared_voucher_id = '';
		$shared_voucher_qty = '';

		foreach($voucher as $data) {
			$shared_voucher_id = $data['voucher_id'];
			$shared_voucher_qty = $data['quantity'];
		}

		$claim_voucher_data = [
			'voucher_id'	=> $shared_voucher_id,
			'user_id'		=> $this->ion_auth->user()->row()->id
		];

		ClaimedVoucher::create($claim_voucher_data);

		if ($shared_voucher_qty == 1) {
			$voucher->delete();
		} else {
			$updated_data = [
				'quantity' => $shared_voucher_qty - 1
			];

			$this->db->where('id', $id)->update('shared_vouchers', $updated_data);
		}

		json_generator($voucher);
	}

}
