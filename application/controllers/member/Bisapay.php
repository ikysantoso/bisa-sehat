<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bisapay extends CI_Controller {

	private $bisapay;

	public function __construct()
	{
		parent::__construct();

		if (!$this->ion_auth->logged_in()) {
			redirect(base_url('auth/login'), 'refresh');
		} else if ($this->ion_auth->is_admin()) {
			redirect(base_url('admin/dashboard'), 'refresh');
		} else {
			$this->load->model('BisapayModel');

			$this->bisapay = BisapayModel::where(
				[
					'user_id'	=> $this->ion_auth->user()->row()->id,
					'status'	=> 'active'
				]
			)->first();
		}
	}

	public function index()
	{
		$this->load->model(['BankModel','CustomerBankModel']);

		$data = [
			'page'		    => 'member-bisapay',
			'isBackDetail'  => 'true',
			'bisapay'	    => $this->bisapay,
			'banks'		    => BankModel::where('status', 'active')->get(),
			'customerbanks'	=> CustomerBankModel::where('user_id', $this->ion_auth->user()->row()->id)->get(),
			'scripts'	    => [
				'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js',
				'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/localization/messages_id.js',
				'https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js',
				'https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js',
				'https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js',
				'https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js',
				base_url('node_modules/sweetalert2/dist/sweetalert2.all.min.js'),
				base_url('assets/js/app/user/dashboard/bisapay.js')
			],
			'styles'	    => [
				'https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css',
				'https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap4.min.css',
			],
		];

		$this->load->view('template-part/header', $data);
		$this->load->view('template-part/navbar', $data);
		$this->load->view('app/member-dashboard/bisapay/bisapay-info', $data);
		$this->load->view('template-part/footer', $data);
	}

	public function activate()
	{
		$result = BisapayModel::updateOrCreate(
			[
				'user_id'	=> $this->ion_auth->user()->row()->id
			],
			[
				'status'	=> 'active'
			]
		);

		if ($result) {
			$this->load->library('my_email');

			$data = [
				'nama'	=> $this->ion_auth->user()->row()->first_name,
				'email'	=> $this->ion_auth->user()->row()->email,
			];

			if ($this->my_email->bisapay_activation($data)) {
				echo "<script>window.location.assign('".base_url('member/bisapay')."')</script>";
			} else {
				redirect(base_url('member/bisapay'), 'refresh');
			}
		}
	}

	public function topup(){
		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'nominal', 'nominal', 'required' );

		if ($this->form_validation->run() === TRUE) {
			$paymentAmount = $this->input->post('nominal');
			$this->db->where('status', 'active');
			$bankAccount = $this->db->get('bank')->result();

			$responseObject['response'] = array("nominal" => rupiah($paymentAmount), "bankAccount" => $bankAccount);

			header('Content-Type: application/json');
			echo json_encode( $responseObject );
		}
	}

	public function topup_detail($id)
	{
		$topup = BisapayTopupModel::where('bisapay_log_id', $id)->first();

		if ($topup) {
			$data = [
				'page'		   => 'member-bisapay',
				'isBackDetail' => 'true',
				'scripts'	   => [
					base_url('node_modules/sweetalert2/dist/sweetalert2.all.min.js'),
					base_url('assets/js/app/user/dashboard/bisapay.js'),
				],
				'styles'	   => [
				],
				'topup'		   => $topup,
			];

			$this->load->view('template-part/header', $data);
			$this->load->view('template-part/navbar', $data);
			$this->load->view('app/member-dashboard/bisapay/topup-detail', $data);
			$this->load->view('template-part/footer', $data);
		}
	}

	public function logs()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('draw', 'draw', 'required');

		if ($this->form_validation->run() == TRUE) {
			$this->load->model('datatables');

			$tablename = 'bisapay_logs';

			$selected_column = array(
				"created_at"	=> 'created_at',
				"log_type"		=> "CONCAT( log_type, ' (', log_type_description, ')' )",
				"status"			=> 'status',
				"nominal"			=> 'nominal',
				"kode_unik"		=> "(SELECT unique_number FROM bisapay_topup where bisapay_log_id=".$tablename.".id)",
				"id"					=> "id",
			);

			$column_order = array(null, 'created_at', 'status');
			$column_search = array('created_at', 'status');
			$order = array('id' => 'desc');

			$where = [
				'bisapay_id' => $this->bisapay->id
			];

			$this->datatables->get_datatables($tablename, $selected_column, $column_order, $column_search, $order, $where, null);
		}
	}

	public function withdraw()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('nominal', 'nominal', 'required');
		$this->form_validation->set_rules('destination', 'destination', 'required');

		if ($this->form_validation->run() == TRUE) {
			if ($this->bisapay->saldo() >= $this->input->post('nominal')) {
				$this->load->model('BisapayWithdrawModel');

				$bisapay_log = $this->bisapay->logs()->create([
			        'log_type'							=> 'out',
			        'log_type_description'	=> 'withdraw',
			        'nominal'								=> $this->input->post('nominal'),
				]);

				$bisapay_withdraw = BisapayWithdrawModel::create([
					'customer_bank_id'	=> $this->input->post('destination'),
					'bisapay_log_id'		=> $bisapay_log->id,
				]);

				if ($bisapay_withdraw) {
					json_generator(['status'	=> 'success']);
				}
			} else {
				json_generator(
					[
						'status'	=> 'warning',
						'title'		=> 'Tarik saldo gagal',
						'message'	=> 'Saldo bisapay tidak cukup'
					]
				);
			}
		}
	}

	public function transaction_logs() {
		$this->db->order_by('updated_at', 'desc');
		$this->db->where('bisapay_id', $this->bisapay->id);
		$this->db->from('bisapay_logs');
		$data = $this->db->get();

		echo json_encode($data->result_array());
	}

	public function online_transaction() {
		$merchantCode = "D8456";
		$merchantKey = "5419bbeb5c5cb98a28f8be9443c76895";
		$paymentAmount = $this->input->post('paymentAmount');
		$paymentMethod = $this->input->post('paymentMethod'); // WW = duitku wallet, VC = Credit Card, MY = Mandiri Clickpay, BK = BCA KlikPay
		$merchantOrderId = time(); // from merchant, unique
		$productDetails = 'BisaPay with duitku';
		$email = 'test@test.com'; // your customer email
		$customerVaName = 'John Doe'; // display name on bank confirmation display
		$callbackUrl = 'https://developer.sahabatsehat.co.id/public/member/payment_duitku/callback'; // url for callback
		$returnUrl = 'https://developer.sahabatsehat.co.id/public/member/bisapay/'; // url for redirect
		$expiryPeriod = '30'; // set the expired time in minutes

		$signature = md5($merchantCode . $merchantOrderId . $paymentAmount . $merchantKey);

		$params = array(
			'merchantCode' => $merchantCode,
			'paymentAmount' => $paymentAmount,
			'paymentMethod' => $paymentMethod,
			'merchantOrderId' => $merchantOrderId,
			'merchantUserInfo' => $this->ion_auth->user()->row()->id,
			'productDetails' => $productDetails,
			'customerVaName' => $customerVaName,
			'email' => $email,
			'callbackUrl' => $callbackUrl,
			'returnUrl' => $returnUrl,
			'signature' => $signature,
			'expiryPeriod' => $expiryPeriod
		);

		$params_string = json_encode($params);
		$url = 'https://sandbox.duitku.com/webapi/api/merchant/v2/inquiry';

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $params_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
			'Content-Length: ' . strlen($params_string))
		);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

		$response = curl_exec($ch);
		$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		header('Content-Type: application/json');
		echo json_encode( $response );
	}

	public function payment_status() {
		$merchantOrderId = $this->input->get('merchantOrderId');
	}

	public function manual_transaction() {
		$bisapay_log = $this->bisapay->logs()->create([
			'log_type'				=> 'in',
			'log_type_description'	=> 'topup',
			'nominal'				=> $this->input->post('paymentAmount'),
		]);

		$topup = $bisapay_log->topup()->create([
			'bisapay_log_id'	=> $bisapay_log->id,
			'unique_number'		=> rand(100,999),
			'bank_id'			=> $this->input->post('bankId'),
		]);

		json_generator($topup);
	}

	public function payment_confirmation()
	{
		$config['upload_path']		= "./assets/images/transactions/bisapay";
        $config['allowed_types']	= 'jpeg|jpg|png';
		$config['max_size']         = 1000;

		$file_name = 'TopUp ' . $this->input->post('nominal');
		$encrypted_file_name = md5($file_name);
		$config['file_name'] = $encrypted_file_name;

		$this->load->library('upload',$config);

		if ($this->upload->do_upload('file')) {
			$upload_data = $this->upload->data();
			$payment_image_name = $upload_data['file_name'];

			$data = [
				'payment_image' => $payment_image_name
			];

			if ($this->input->post('exist_image_name') != "") {
				$exist_image_name = $this->input->post('exist_image_name');
				$exist_path_image = $_SERVER['DOCUMENT_ROOT']."/assets/images/transactions/".$exist_image_name;
				$this->load->helper("file");
				// unlink($exist_path_image);
			}

			$this->db->where('id', $this->input->post('log_id'));
			$this->db->update('bisapay_logs', $data);

			$result = BisapayLogsModel::where([
				'id' => $this->input->post('log_id')
			])->first();

			$response = array('code' => '230', 'status' => 'success', 'title'=> 'Pengiriman Berhasil',
				'message' => 'Bukti pembayaran anda akan segera kami verifikasi', 'data' => $result);

				echo json_encode($response);
        } else {
			$response = array('code' => '400', 'status' => 'error', 'title' => 'Pengiriman Gagal',
								'message' => 'Bukti pembayaran anda tidak dapat kami terima');

			echo json_encode($response);
		}
	}
}