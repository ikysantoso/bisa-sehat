<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fitness extends CI_Controller {

    private $incoBaseApi;


    public function __construct(){
		parent::__construct();

		if (!$this->ion_auth->logged_in()) {
			redirect(base_url('auth/login'), 'refresh');
		} else if ($this->ion_auth->is_admin()) {
			redirect(base_url('admin/dashboard'), 'refresh');
		}

        $this->incoBaseApi = $this->config->item('inco_base_api');
    }

	public function index(){
		$data = [
			'page'			=> 'member-fitness',
			'page_title'	=> 'Data Tes Kebugaran',
			'fitnesses'		=> FitnessModel::where( 'user_id', $this->ion_auth->user()->row()->id )->orderBy('id', 'desc')->get(),
			'scripts'		=> [
				base_url('node_modules/sweetalert2/dist/sweetalert2.all.min.js'),
				'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js',
				base_url('assets/js/app/user/dashboard/fitness.js'),
			]
		];

		$this->load->view('template-part/header', $data);
		$this->load->view('template-part/navbar', $data);
		$this->load->view('app/member-dashboard/fitness/fitness-index', $data);
		$this->load->view('template-part/footer', $data);
	}

	public function new(){
		$data = [
			'page'			=> 'member-fitness',
			'page_title'	=> 'Data Tes Kebugaran',
			'fitnesses'		=> FitnessModel::where( 'user_id', $this->ion_auth->user()->row()->id )->get(),
			'scripts'		=> [
				base_url('node_modules/sweetalert2/dist/sweetalert2.all.min.js'),
				'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js',
				base_url('assets/js/app/user/dashboard/fitness.js'),
			]
		];

		$this->load->view('template-part/header', $data);
		$this->load->view('template-part/navbar', $data);
		$this->load->view('app/member-dashboard/fitness/fitness-new', $data);
		$this->load->view('template-part/footer', $data);
	}

	public function save(){
		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'height', 'height', 'required' );
		$this->form_validation->set_rules( 'weight', 'weight', 'required' );
		$this->form_validation->set_rules( 'mileage', 'mileage', 'required' );
		if ($this->form_validation->run() === TRUE){
        $allCompanyUrl = $this->incoBaseApi . '/company';
        $allCompanyAPIResponse = $this->hitIncoAPIcompany($allCompanyUrl, null);
        $getAlreadyEmployeeRegisteredUrl = $this->incoBaseApi . '/employee/nik/search';
        $uric_acid_more_than_ten_level = false;
        // $id = $this->post('id');
        $nik = UserMetaModel::where('user_id', $this->ion_auth->user()->row()->id)->where('meta_key', 'nik')->first();
        $getAlreadyEmployeeRegistered = $this->hitIncoAPIcompany($getAlreadyEmployeeRegisteredUrl, array(
           'nik' => $nik->meta_value
        ));
        $already_employee = json_decode($getAlreadyEmployeeRegistered, true);
			$data_insert = [
				'user_id'	=> $this->ion_auth->user()->row()->id,
				'height'	=> $this->input->post('height'),
				'weight'	=> $this->input->post('weight'),
				'mileage'	=> $this->input->post('mileage'),
			];

			$result = FitnessModel::create( $data_insert );

            if($already_employee['success']==true){
            $this->reportToInco($data_insert);
            }

			if ( $result ) {
				$this->session->set_flashdata('message', 'Data berhasil disimpan');
				$this->session->set_flashdata('status', 'info');
			} else {
				$this->session->set_flashdata('message', 'Data gagal disimpan');
				$this->session->set_flashdata('status', 'warning');
			}

			json_generator( $result );
			redirect(base_url('member/fitness'));
		}
			redirect(base_url('member/fitness'));
	}

	public function update(){
		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'id', 'id', 'required' );
		$this->form_validation->set_rules( 'height', 'height', 'required' );
		$this->form_validation->set_rules( 'weight', 'weight', 'required' );
		if (
			$this->form_validation->run() === TRUE
			&& $fitness = FitnessModel::where([
				'id' 		=> $this->input->post('id'),
				'user_id' 	=> $this->ion_auth->user()->row()->id
			])->first()
		){

			$data_update = [
				'height'	=> $this->input->post('height'),
				'weight'	=> $this->input->post('weight'),
			];

			$result = $fitness->update($data_update);

			if ( $result ) {
				$this->session->set_flashdata('message', 'Data berhasil diupdate');
				$this->session->set_flashdata('status', 'info');
			} else {
				$this->session->set_flashdata('message', 'Data gagal diupdate');
				$this->session->set_flashdata('status', 'warning');
			}

			json_generator( $result );
		}
	}

	public function delete( $id = null ){

		$is_deleted = false;
		if ($id && $fitness = FitnessModel::where( ['id' => $id, 'user_id' => $this->ion_auth->user()->row()->id] )->first()){
			$is_deleted = ( $fitness->forceDelete() ) ? true : false;
		}

		if ( $is_deleted ) {
			$this->session->set_flashdata('message', 'Data berhasil dihapus');
			$this->session->set_flashdata('status', 'info');
		} else {
			$this->session->set_flashdata('message', 'Data gagal dihapus');
			$this->session->set_flashdata('status', 'warning');
		}

		redirect(base_url('member/fitness'), 'refresh');
	}

	public function detail(){
		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'id', 'id', 'required' );
		if (
			$this->form_validation->run() === TRUE
			&& $fitness = FitnessModel::where([
				'id'		=> $this->input->post('id'),
				'user_id'	=> $this->ion_auth->user()->row()->id,
			])->first()
		){
			json_generator( $fitness );
		}
	}

	public function download( $id = null ){
		if ( $id ){
		    $fitnesses = FitnessModel::where([
				'id'		=> $id,
				'user_id'	=> $this->ion_auth->user()->row()->id,
			])->get();
		} else {
			$fitnesses = FitnessModel::where([
				'user_id'	=> $this->ion_auth->user()->row()->id,
			])->orderBy('id', 'desc')->get();
		}

		if ( $fitnesses->count() > 0 ) {
			$this->load->library('my_pdf');
			$this->my_pdf->fitness_report( ['fitnesses' => $fitnesses] );
		} else {
			$this->session->set_flashdata('message', 'Belum ada data yang bisa didownload');
			$this->session->set_flashdata('status', 'info');
			redirect(base_url('member/fitness'), 'refresh');
		}
	}

    public function reportToInco($data_insert)
    {
        $createFitnessIncoUrl = $this->incoBaseApi . '/fitness';

        $user = UserModel::where('id', $data_insert['user_id'])->first();
        $nik = $user->user_meta('nik');

        $input_test = [
            'nik' => $nik,
            'height' => $data_insert['height'],
            'weight' => $data_insert['weight'],
            'mileage' => $data_insert['mileage'],

        ];

        return $this->hitIncoAPI($createFitnessIncoUrl, $input_test);
    }

    private function hitIncoAPI($url, $params)
    {
        $client = new \GuzzleHttp\Client();
        $options = [
            'json' => $params
        ];
        $response = $client->request('POST', $url, $options);

        return $response->getBody();
    }
    private function hitIncoAPIcompany($url, $params)
    {
        $ch = curl_init();

        if (isset($params)) {
            $fullUrl = $url . '?' . http_build_query($params);
            log_message('error', $fullUrl);
            curl_setopt($ch, CURLOPT_URL, $fullUrl);
        } else {
            curl_setopt($ch, CURLOPT_URL, $url);
        }

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json')
        );
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        return curl_exec($ch);
    }
}