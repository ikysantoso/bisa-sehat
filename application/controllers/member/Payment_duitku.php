<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment_duitku extends CI_Controller {

    public function __construct() {
            parent::__construct();
    }

    public function callback() {
        $apiKey = "5419bbeb5c5cb98a28f8be9443c76895";
		$merchantCode = isset($_POST['merchantCode']) ? $_POST['merchantCode'] : null;
		$amount = isset($_POST['amount']) ? $_POST['amount'] : null;
		$merchantOrderId = isset($_POST['merchantOrderId']) ? $_POST['merchantOrderId'] : null;
		$productDetail = isset($_POST['productDetail']) ? $_POST['productDetail'] : null;
		$additionalParam = isset($_POST['additionalParam']) ? $_POST['additionalParam'] : null;
		$paymentMethod = isset($_POST['paymentCode']) ? $_POST['paymentCode'] : null;
		$resultCode = isset($_POST['resultCode']) ? $_POST['resultCode'] : null;
		$merchantUserId = isset($_POST['merchantUserId']) ? $_POST['merchantUserId'] : null;
		$reference = isset($_POST['reference']) ? $_POST['reference'] : null;
		$signature = isset($_POST['signature']) ? $_POST['signature'] : null;

        if(!empty($merchantCode) && !empty($amount) && !empty($merchantOrderId) && !empty($signature))
        {
            $params = $merchantCode . $amount . $merchantOrderId . $apiKey;
            $calcSignature = md5($params);

            if($signature == $calcSignature)
            {
                $this->load->model( 'BisapayModel' );

                $bisapay = BisapayModel::where(
                    [
                        'user_id'	=> $merchantUserId,
                        'status'	=> 'active'
                    ]
                )->first();

                $bisapay_log = $bisapay->logs()->create([
                    'log_type'				=> 'in',
                    'log_type_description'	=> 'topup',
                    'nominal'				=> $amount,
                    'status'                => 'success',
                    'merchant_order_id'		=> $reference
                ]);

                $topup = $bisapay_log->topup()->create([
                    'bisapay_log_id'	=> $bisapay_log->id,
                    'unique_number'		=> 000,
                    'bank_id'			=> 100,
                ]);

                echo "SUCCESS";
            }
            else
            {
                throw new Exception('Bad Signature');
            }
        }
        else
        {
            throw new Exception('Bad Parameter');
        }
	}
}
?>