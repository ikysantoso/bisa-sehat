<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends CI_Controller {

	public function __construct(){
		parent::__construct();

		if (!$this->ion_auth->logged_in()) {
			redirect(base_url('auth/login'), 'refresh');
		} else if ($this->ion_auth->is_admin()) {
			redirect(base_url('admin/dashboard'), 'refresh');
		}
	}
}