<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Medical_record extends CI_Controller {

    private $incoBaseApi;

    public function __construct(){
		parent::__construct();

		$this->load->model(['MedicalRecordModel', 'UserMetaModel']);

		if (!$this->ion_auth->logged_in()) {
			redirect(base_url('auth/login'), 'refresh');
		} else if ($this->ion_auth->is_admin()) {
			redirect(base_url('admin/dashboard'), 'refresh');
		}

        $this->incoBaseApi = $this->config->item('inco_base_api');
		$this->load->library("pagination");
    }

	public function index(){
		$config['base_url'] = base_url() . 'index.php/member/medical_record/index';
		$config['total_rows'] = MedicalRecordModel::where( 'user_id', $this->ion_auth->user()->row()->id )->count();
		$config['per_page'] = 5;
		$this->pagination->initialize($config);
		$offset = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;;
		$total_pages = ceil($config['total_rows']/$config['per_page']);
		$data = [
			'page'			=> 'member-medical-record',
			'page_title'	=> 'Data Rekam Medis',
			'offset' => $offset,
			'total_pages' => $total_pages,
			'links' => $this->pagination->create_links(),
			'medical_records'	=> MedicalRecordModel::where( 'user_id', $this->ion_auth->user()->row()->id )->orderBy('id', 'desc')->limit($config['per_page'])->offset($offset)->get(),
			'scripts'	=> [
				base_url('node_modules/sweetalert2/dist/sweetalert2.all.min.js'),
				'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js',
				base_url('assets/js/app/user/dashboard/medical-record.js'),
			]
		];

		$this->load->view('template-part/header', $data);
		$this->load->view('template-part/navbar', $data);
		$this->load->view('app/member-dashboard/medical-record/medical-record-index', $data);
		$this->load->view('template-part/footer', $data);
	}

	public function new(){
		$data = [
			'page'			=> 'member-medical-record',
			'page_title'	=> 'Tambah Rekam Medis',
			'medical_records'	=> MedicalRecordModel::where( 'user_id', $this->ion_auth->user()->row()->id )->orderBy('id', 'desc')->get(),
			'scripts'	=> [
				base_url('node_modules/sweetalert2/dist/sweetalert2.all.min.js'),
				'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js',
				base_url('assets/js/app/user/dashboard/medical-record.js'),
			]
		];

		$this->load->view('template-part/header', $data);
		$this->load->view('template-part/navbar', $data);
		$this->load->view('app/member-dashboard/medical-record/medical-record-new', $data);
		$this->load->view('template-part/footer', $data);
	}

	public function save(){
        $this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'height', 'height', 'required' );
		$this->form_validation->set_rules( 'weight', 'weight', 'required' );

        $uric_acid_more_than_ten_level = false;

		if ($this->form_validation->run() === TRUE){
		// $this->load->helper('string');
  //       $name_attachment=random_string('alnum',10);
  //       if (isset($_FILES['attachment']['name'])) {
  //           $attachment = $_FILES['attachment']['name'];
  //       }        
  //       $this->load->library('upload');
  //       if (!empty($attachment)) {
  //           $config['upload_path'] = './assets/rekam_medis/';
  //           $config['allowed_types'] = 'pdf|jpg|jpeg|png';
  //           $config['file_ext_tolower'] = TRUE;
  //           $config['max_size'] = '2048';
  //           $config['overwrite'] = TRUE;
  //           $x = explode(".", $attachment);
  //           $ext = strtolower(end($x));
  //           $config['file_name'] = $name_attachment."-attachment.".$ext;
  //           $ij = $config['file_name'];

  //           $this->upload->initialize($config);
  //           $this->upload->do_upload('attachment');
  //       }
  //       if (!empty($attachment) && !$this->upload->do_upload('attachment')) {
  //           $this->upload->display_errors();
  //       }
			$data_insert = [
				'user_id'			            => $this->ion_auth->user()->row()->id,
				'height'			            => $this->input->post('height'),
				'weight'			            => $this->input->post('weight'),
				'sistole'			            => $this->input->post('sistole'),
				'diastole'			            => $this->input->post('diastole'),
				'blood_sugar_level'	            => $this->input->post('blood_sugar_level'),
				'cholesterol_level'	            => $this->input->post('cholesterol_level'),
				'uric_acid_level'	            => $this->input->post('uric_acid_level'),
                'uric_acid_more_than_ten_level' => $uric_acid_more_than_ten_level,
                // "attachment"  						=> (!empty($ij)) ? $ij : '',
			];

			$query_last_record = $this->db->query('SELECT * FROM medical_record WHERE user_id = ' . $this->ion_auth->user()->row()->id . ' ORDER BY created_at DESC LIMIT 1');
			$last_record = $query_last_record->result_array();

			if (count($last_record) > 0) {
				foreach($last_record as $record) {
					$data_insert['sistole'] = $data_insert['sistole'] == '' ? $record['sistole'] : $data_insert['sistole'];
					$data_insert['diastole'] = $data_insert['diastole'] == '' ? $record['diastole'] : $data_insert['diastole'];
					$data_insert['blood_sugar_level'] = $data_insert['blood_sugar_level'] == '' ? $record['blood_sugar_level'] : $data_insert['blood_sugar_level'];
					$data_insert['cholesterol_level'] = $data_insert['cholesterol_level'] == '' ? $record['cholesterol_level'] : $data_insert['cholesterol_level'];
					$data_insert['uric_acid_level'] = $data_insert['uric_acid_level'] == '' ? $record['uric_acid_level'] : $data_insert['uric_acid_level'];
				}
			} else {
				$data_insert['sistole'] = $data_insert['sistole'] == '' ? 0 : $data_insert['sistole'];
				$data_insert['diastole'] = $data_insert['diastole'] == '' ? 0 : $data_insert['diastole'];
				$data_insert['blood_sugar_level'] = $data_insert['blood_sugar_level'] == '' ? 0 : $data_insert['blood_sugar_level'];
				$data_insert['cholesterol_level'] = $data_insert['cholesterol_level'] == '' ? 0 : $data_insert['cholesterol_level'];
				$data_insert['uric_acid_level'] = $data_insert['uric_acid_level'] == '' ? 0 : $data_insert['uric_acid_level'];
			}

            $medical_record_inserted_model = MedicalRecordModel::create($data_insert);

            $data_insert['nik'] = UserMetaModel::where('user_id',$this->ion_auth->user()->row()->id)->where('meta_key', 'nik')->first()->meta_value;

            $this->reportToInco($data_insert);

            $this->session->set_flashdata('message', 'Data berhasil disimpan');
            $this->session->set_flashdata('status', 'info');

			json_generator($medical_record_inserted_model);
			redirect(base_url('member/medical_record'));
		}
	}

	public function update(){
		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'id', 'id', 'required' );
		$this->form_validation->set_rules( 'height', 'height', 'required' );
		$this->form_validation->set_rules( 'weight', 'weight', 'required' );
		if (
			$this->form_validation->run() === TRUE
			&& $medical_record = MedicalRecordModel::where([
				'id'		=> $this->input->post('id'),
				'user_id'	=> $this->ion_auth->user()->row()->id,
			])->first()
		){
			$data_update = [
				'height'			=> $this->input->post('height'),
				'weight'			=> $this->input->post('weight'),
			];

			$result = $medical_record->update( $data_update );

			if ( $result ) {
				$this->session->set_flashdata('message', 'Data berhasil diupdate');
				$this->session->set_flashdata('status', 'info');
			} else {
				$this->session->set_flashdata('message', 'Data gagal diupdate');
				$this->session->set_flashdata('status', 'warning');
			}

			json_generator($result);
		}
	}

	public function delete( $id = null ){

		$is_deleted = false;
		if ($id && $medical_record = MedicalRecordModel::where( ['id' => $id, 'user_id' => $this->ion_auth->user()->row()->id] )->first()){
			$is_deleted = ( $medical_record->forceDelete() ) ? true : false;
		}

		if ( $is_deleted ) {
			$this->session->set_flashdata('message', 'Data berhasil dihapus');
			$this->session->set_flashdata('status', 'info');
		} else {
			$this->session->set_flashdata('message', 'Data gagal dihapus');
			$this->session->set_flashdata('status', 'warning');
		}

		redirect(base_url('member/medical_record'), 'refresh');
	}

	public function detail(){
		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'id', 'id', 'required' );
		if (
			$this->form_validation->run() === TRUE
			&& $medical_record = MedicalRecordModel::where([
				'id'		=> $this->input->post('id'),
				'user_id'	=> $this->ion_auth->user()->row()->id,
			])->first()
		){
			json_generator( $medical_record );
		}
	}

	public function download( $id = null ){
		if ( $id ){
			$medical_records = MedicalRecordModel::where([
				'id'		=> $id,
				'user_id'	=> $this->ion_auth->user()->row()->id,
			])->get();
		} else {
			$medical_records = MedicalRecordModel::where([
				'user_id'	=> $this->ion_auth->user()->row()->id,
			])->orderBy('id', 'desc')->get();
		}
		if ( $medical_records->count() > 0 ) {
			$this->load->library('my_pdf');
			$this->my_pdf->medical_record_report( ['medical_records' => $medical_records] );
		} else {
			$this->session->set_flashdata('message', 'Belum ada data yang bisa didownload');
			$this->session->set_flashdata('status', 'info');
			redirect(base_url('member/medical_record'), 'refresh');
		}
	}

	public function save_by_nakes()
	{
		header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: POST, GET, DELETE, PUT, PATCH, OPTIONS');
		$user_meta = UserMetaModel::where([
			'meta_key'		=> 'nik',
			'meta_value'	=> $this->input->post('nik')
		])->first();

		$data_insert = [
			'user_id'			=> $user_meta->user->id,
			'height'			=> $this->input->post('height'),
			'weight'			=> $this->input->post('weight'),
			'sistole'			=> $this->input->post('sistole'),
			'diastole'			=> $this->input->post('diastole'),
			'blood_sugar_level'	=> $this->input->post('bloodSugar'),
			'cholesterol_level'	=> $this->input->post('cholesterol'),
			'uric_acid_level'	=> $this->input->post('uricAcid'),
		];

		$this->session->set_flashdata('message', 'Data berhasil disimpan');
		$this->session->set_flashdata('status', 'info');

		json_generator( MedicalRecordModel::create( $data_insert ) );
	}

    public function reportToInco($data_insert)
    {
        $createMedicalCheckupIncoUrl = $this->incoBaseApi . '/medical-checkup';

        $user = UserModel::where('id', $data_insert['user_id'])->first();
        $nik = $user->user_meta('nik');

        $input_test = [
            'nik' => $nik,
            'height' => $data_insert['height'],
            'weight' => $data_insert['weight'],
            'sistole' => $data_insert['sistole'],
            'diastole' => $data_insert['diastole'],
            'bloodSugarLevel' => $data_insert['blood_sugar_level'],
            'cholesterolLevel' => $data_insert['cholesterol_level'],
            'uricAcidLevel' => $data_insert['uric_acid_level'],
        ];

        return $this->hitIncoAPI($createMedicalCheckupIncoUrl, $input_test);
    }

    private function hitIncoAPI($url, $params)
    {
        $client = new \GuzzleHttp\Client();
        $options = [
            'json' => $params
        ];
        $response = $client->request('POST', $url, $options);

        return $response->getBody();
    }
}