<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Terms extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		if ($this->ion_auth->is_admin()) {
			redirect(base_url('admin/dashboard'), 'refresh');
		}
	}
	public function index()
	{
			$data = [
				'page'	=> 'terms-conditions',
				'isBackDetail' => 'true',
			];

			$this->load->view('template-part/header', $data);
			$this->load->view('template-part/navbar', $data);
			$this->load->view('app/frontpage/terms-conditions', $data);
			$this->load->view('template-part/footer', $data);
		
	}
}
