<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main_page extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		if ($this->ion_auth->is_admin()) {
			redirect(base_url('admin/dashboard'), 'refresh');
		}
	}
	public function index()
	{
		// $data = [
		// 	'page'	=> 'main-page',
		// ];
		
		// $this->load->view('template-part/header', $data);
		// $this->load->view('app/frontpage/main-page', $data);
		redirect(base_url('home'), 'refresh');
	}
}
