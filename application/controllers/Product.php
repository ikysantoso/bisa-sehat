<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();

		if (!$this->ion_auth->logged_in()) {
			redirect(base_url('auth/login'), 'refresh');
		} else if ($this->ion_auth->is_admin()) {
			redirect(base_url('admin/dashboard'), 'refresh');
		}
	}
	public function all()
	{
		$this->load->model('ProductModel');

		$data = [
			'page'			=> 'product',
			'isBackDetail'  => 'true',
			'isSearchBar'   => 'true',
			'page_detail'	=> 'product',
			'products'		=> ProductModel::all(),
			'scripts'		=> [
				base_url('node_modules/sweetalert2/dist/sweetalert2.all.min.js'),
				base_url('assets/js/app/user/product-all.js'),
				base_url('assets/js/app/user/dashboard/product-service.js')
			]
		];

		$this->load->view('template-part/header', $data);
		$this->load->view('template-part/navbar', $data);
		$this->load->view('app/frontpage/products/product-all', $data);
		$this->load->view('template-part/footer', $data);
	}

	public function discount()
	{
		$this->load->model('ProductModel');

		$product_on_promotion = ProductModel::whereRaw("
						discount IS NOT NULL
		")->get();

		$data = [
			'page'			       => 'product',
			'page_detail'	       => 'product',
			'isBackDetail'         => 'true',
			'isSearchBar' 		   => 'true',
			'product_on_promotion' => $product_on_promotion,
			'scripts'		       => [
				base_url('node_modules/sweetalert2/dist/sweetalert2.all.min.js'),
				base_url('assets/js/app/user/product-all.js'),
				base_url('assets/js/app/user/dashboard/product-service.js')
			]
		];

		$this->load->view('template-part/header', $data);
		$this->load->view('template-part/navbar', $data);
		$this->load->view('app/frontpage/products/product-discount', $data);
		$this->load->view('template-part/footer', $data);
	}

	public function stock()
	{
		$this->load->model('ProductModel');
		$this->load->library('form_validation');

		$this->form_validation->set_rules('id', 'id', 'required');

		$product = ProductModel::find($this->input->post('id'));

		if ($this->form_validation->run() === TRUE && $product) {
			$stocks = [];

			foreach ($product->stocks as $key => $stock) {
				$stocks[] = [
					'id'		=> $stock->warehouse_id,
					'warehouse'	=> $stock->warehouse->name,
					'stock'		=> $stock->last_stock,
				];
			}

			$data = [
				'name'			=> $product->name,
				'price'			=> $product->price,
				'discount'		=> $product->discount,
				'image'			=> ($product->images->count() > 0) ? $product->images[0]->image : null,
				'stocks'		=> $stocks,
				'is_preorder'	=> $product->is_preorder,
			];

			json_generator( $data );
		}
	}

	public function categories()
	{
		$this->load->model('ProductCategoryModel');

		$data = [
			'page'			=> 'product_categories',
			'isBackDetail'  => 'true',
			'categories'	=> ProductCategoryModel::all(),
		];

		$this->load->view('template-part/header', $data);
		$this->load->view('template-part/navbar', $data);
		$this->load->view('app/frontpage/products/product-categories', $data);
		$this->load->view('template-part/footer', $data);

	}

	public function category($id = null)
	{
		$this->load->model('ProductCategoryModel', 'ProductModel');

		$category = ProductCategoryModel::find($id);

		if ($id && $category) {
			$data = [
				'page'			=> 'product_category',
				'isBackDetail'  => 'true',
				'page_detail'	=> 'product-category-'.$id,
				'category'		=> $category,
				'products'		=> ProductModel::where('category_id', $id)->get(),
				'scripts'		=> [
					base_url('node_modules/sweetalert2/dist/sweetalert2.all.min.js'),
					base_url('assets/js/app/user/product-all.js'),
					base_url('assets/js/app/user/dashboard/product-service.js')
				]
			];

			$this->load->view('template-part/header', $data);
			$this->load->view('template-part/navbar', $data);
			$this->load->view('app/frontpage/products/product-all', $data);
			$this->load->view('template-part/footer', $data);
		} else {
			redirect(base_url('product/all'), 'refresh');
		}
	}

	public function detail($id = null)
	{
		$this->load->model('ProductModel', 'SettingModel');

		if ($id) {
			$data = [
				'page'			=> 'product',
				'isBackDetail'  => 'true',
				'page_detail'	=> 'product-detail',
				'product'		=> ProductModel::find( $id ),
				'socmeds'		=> SettingModel::whereIn('meta_key', ['whatsapp_number','facebook', 'instagram'])->get(),
				'scripts'		=> [
					base_url('node_modules/sweetalert2/dist/sweetalert2.all.min.js'),
					'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js',
					base_url('assets/js/app/user/product-detail.js'),
					base_url('assets/js/app/user/dashboard/product-service.js'),
				]
			];

			$this->load->view('template-part/header', $data);
			$this->load->view('template-part/navbar', $data);
			$this->load->view('app/frontpage/products/product-detail', $data);
			$this->load->view('template-part/footer', $data);
		}
	}

	public function add_to_cart()
	{
		if ($this->ion_auth->logged_in()) {
			$this->load->model('CustomerAddressModel');

			$customer_address = CustomerAddressModel::where( ['user_id' => $this->ion_auth->user()->row()->id] )->get();

			if ($customer_address->count() === 0) {
				$response = [
					'type'		=> 'warning',
					'title'		=> 'Alamat tidak ditemukan',
					'message'	=> 'Anda belum melakukan aktivasi ataupun menambahkan data alamat',
					'redirect'	=> base_url('member/setting')
				];

				json_generator($response);
			} else {
				$this->load->model('ProductStockModel');
				$product_stock = ProductStockModel::where(['product_id' => $this->input->post('product_id')])->first();

				if ($product_stock->last_stock >= $this->input->post('quantity')) {
					$this->load->model('CartModel');

					$exist_cart = CartModel::where([
						'product_id'	=> $this->input->post('product_id'),
						'user_id'		=> $this->ion_auth->user()->row()->id,
						'type'			=> 'cart'
					])->first();

					$type = ($this->input->post('transaction_type') == 'cart') ? 'cart' : 'checkout';

					if ($exist_cart && ($type == 'cart')) {
						$updated_quantity = $exist_cart->quantity + $this->input->post('quantity');

						$input_update_cart = [
							'quantity'	=> $updated_quantity
						];

						$exist_cart->update($input_update_cart);

						$response = [
							'type'		=> 'success',
							'redirect'	=> base_url('cart'),
							'data'		=> $exist_cart
						];

						json_generator($response);
					} else {
						$input_cart = [
							'user_id'		=> $this->ion_auth->user()->row()->id,
							'product_id'	=> $this->input->post('product_id'),
							'warehouse_id'	=> $this->input->post('warehouse_id'),
							'quantity'		=> $this->input->post('quantity'),
							'type'			=> $type
						];

						$cart = CartModel::create($input_cart);

						if ($cart) {
							$response = [
								'type'		=> 'success',
								'redirect'	=> base_url('cart'),
								'data'		=> $cart
							];

							json_generator($response);
						}
					}
				} else {
					$response = [
						'type'		=> 'warning',
						'title'		=> 'Maaf, Stock produk tidak mencukupi',
						'message'	=> 'Silakan untuk memilih stock yang tersedia'
					];

					json_generator($response);
				}
			}
		} else {
			$response = [
				'type'		=> 'warning',
				'title'		=> 'Anda belum login',
				'message'	=> 'Login terlebih dahulu sebelum melakukan transaksi',
				'redirect'	=> base_url('auth/login')
			];

			json_generator($response);
		}
	}

	public function search()
	{
		if (isset($_GET['q'])) {
			$products = ProductModel::Where('name', 'like', '%' . $_GET['q'] . '%')->get();

			$data = [
				'page'			=> 'product',
				'page_detail'	=> 'product',
				'products'		=> $products,
				'isBackDetail'  => 'true',
			    'isSearchBar'   => 'true',
				'scripts'		=> [
					base_url('node_modules/sweetalert2/dist/sweetalert2.all.min.js'),
					base_url('assets/js/app/user/product-all.js'),
					base_url('assets/js/app/user/dashboard/product-service.js')
				]
			];

			$this->load->view('template-part/header', $data);
			$this->load->view('template-part/navbar', $data);
			$this->load->view('app/frontpage/products/product-all', $data);
			$this->load->view('template-part/footer', $data);
		} else {
			redirect(base_url('product/all'), 'refresh');
		}
	}

    public function suggestion($suggestion_for = null)
    {
        $this->load->model('ProductModel');

        $products = ProductModel::where('suggestion_for', $suggestion_for)->get();

        $data = [
            'page'			=> 'product_suggestion',
            'isBackDetail'  => 'true',
            'products'		=> $products,
            'scripts'		=> [
                base_url('node_modules/sweetalert2/dist/sweetalert2.all.min.js'),
                base_url('assets/js/app/user/product-all.js'),
                base_url('assets/js/app/user/dashboard/product-service.js')
            ]
        ];

        $this->load->view('template-part/header', $data);
        $this->load->view('template-part/navbar', $data);
        $this->load->view('app/frontpage/products/product-all', $data);
        $this->load->view('template-part/footer', $data);
    }
}
