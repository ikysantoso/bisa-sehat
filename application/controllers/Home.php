<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();

		if (!$this->ion_auth->logged_in()) {
			redirect(base_url('auth/login'), 'refresh');
		} else if ($this->ion_auth->is_admin()) {
			redirect(base_url('admin/dashboard'), 'refresh');
		}
	}

	public function index()
	{
		$current_date = date('Y-m-d');
		$this->load->model( ['SliderModel', 'ProductModel', 'SharedVoucher'] );

		$sliders = SliderModel::whereRaw("
						status = 'active' AND
						(start_date IS NULL AND end_date IS NULL)
						or (start_date <= '$current_date' AND end_date IS NULL)
						or (start_date <= '$current_date' AND end_date >= '$current_date' )
					")->get();

		$product_on_promotion = ProductModel::whereRaw("
						discount IS NOT NULL
		")->take(8)->get();

		$best_sell_products = ProductModel::whereRaw("
						buyer_count IS NOT NULL AND buyer_count > 0
		")->orderBy('buyer_count', 'desc')->take(8)->get();

		$shared_voucher = null;

		if ($this->ion_auth->logged_in()) {
			$shared_voucher = $this->db->query("SELECT v.name, v.nominal, v.id as v_id, sv.id as sv_id FROM voucher v INNER JOIN shared_vouchers sv ON v.id = sv.voucher_id
								WHERE sv.user_id = ".$this->ion_auth->user()->row()->id." AND NOW() BETWEEN v.start_date AND v.expired")
								->result();
		}

		$data = [
			'page'				=> 'home',
			'sliders'			=> $sliders,
			'latest_products'	=> ProductModel::take(4)->orderBy('id', 'desc')->get(),
			'article'	        => PageModel::take(6)->orderBy('id', 'desc')->get(),
			'categories'		=> ProductCategoryModel::take(7)->orderBy('id', 'desc')->get(),
			'scripts'			=> [
				base_url('node_modules/sweetalert2/dist/sweetalert2.all.min.js'),
				base_url('assets/js/app/user/product-all.js'),
				base_url('assets/js/app/user/dashboard/product-service.js'),
                base_url('assets/js/app/user/dashboard/medical-record-service.js'),
				base_url('assets/js/app/user/dashboard/voucher-service.js'),
				'https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js',
				'https://cdnjs.cloudflare.com/ajax/libs/chartjs-plugin-annotation/0.5.7/chartjs-plugin-annotation.min.js',
			],
			'isSearch' 				=> 'true',
			'isHealth' 				=> 'true',
			'product_on_promotion' 	=> $product_on_promotion,
			'shared_voucher' 		=> $shared_voucher,
			'best_sell_products' 	=> $best_sell_products
		];

		$this->load->view('template-part/header', $data);
		$this->load->view('template-part/navbar', $data);
		$this->load->view('app/frontpage/home', $data);
		$this->load->view('template-part/footer', $data);
	}

	public function testing(){
		$this->load->library('my_email');
		$this->my_email->test();
	}

    public function getDiagnoseTotal() {
        $medicalRecords = MedicalRecordModel::where('user_id', $this->ion_auth->user()->row()->id)->orderBy('created_at', 'desc')->take(10)->get();

        foreach($medicalRecords->reverse()->toArray() as $key => $value) {
            $datetime = strtotime($value['created_at']);
            $date[] = date('d F', $datetime);
            $cholesterol[] = $value['cholesterol_level'];
            $bloodSugar[] = $value['blood_sugar_level'];
            $uricAcid[] = $value['uric_acid_level'];
            $sistole[] = $value['sistole'];
            $diastole[] = $value['diastole'];
            $bodyMassIndex[] = $medicalRecords[(int)$key]->bmi()['bmi'];
        }

        $response = (object) [
            'date'          => $date,
            'cholesterol'   => $cholesterol,
            'bloodSugar'    => $bloodSugar,
            'uricAcid'      => $uricAcid,
            'sistole'       => $sistole,
            'diastole'      => $diastole,
			'bodyMassIndex' => $bodyMassIndex
        ];

        return json_generator($response);
    }

	public function getFitnessResults() {
        $fitness = FitnessModel::with('user')->where('user_id', $this->ion_auth->user()->row()->id)->orderBy('created_at', 'desc')->take(10)->get();

        foreach($fitness->reverse()->toArray() as $key => $value) {
            $datetime = strtotime($fitness[(int) $key]['created_at']);
            $fitnessResult = $fitness[(int) $key]->mileage_result();

            $date[] = date('d F', $datetime);;
            $no[] = $fitnessResult['status'] == 'Anda bugar' ? 1 : 0;
        }

        $response = (object) [
            'date' => $date,
            'no'   => $no,
        ];

        return json_generator($response);
    }

	public function getDiagnose() {
        $medicalRecords = MedicalRecordModel::where('user_id', $this->ion_auth->user()->row()->id)->orderBy('created_at', 'desc')->take(10)->get();

        foreach($medicalRecords->reverse()->toArray() as $key => $value) {
            $datetime = strtotime($value['created_at']);
            $date[] = date('d F', $datetime);
            $cholesterol[] = $value['cholesterol_level'];
            $bloodSugar[] = $value['blood_sugar_level'];
            $uricAcid[] = $value['uric_acid_level'];
            $sistole[] = $value['sistole'];
            $diastole[] = $value['diastole'];
            $bodyMassIndex[] = $medicalRecords[(int)$key]->bmi()['bmi'];
        }

        $response = (object) [
            'date'          => $date,
            'cholesterol'   => $cholesterol,
            'bloodSugar'    => $bloodSugar,
            'uricAcid'      => $uricAcid,
            'sistole'       => $sistole,
            'diastole'      => $diastole,
			'bodyMassIndex' => $bodyMassIndex
        ];

        return json_generator($response);
    }

	public function getFitness() {
        $fitness = FitnessModel::with('user')->where('user_id', $this->ion_auth->user()->row()->id)->orderBy('created_at', 'desc')->take(10)->get();

        foreach($fitness->reverse()->toArray() as $key => $value) {
            $datetime = strtotime($fitness[(int) $key]['created_at']);
            $fitnessResult = $fitness[(int) $key]->mileage_result();

            $date[] = date('d F', $datetime);;
            $no[] = $fitnessResult['status'] == 'Anda bugar' ? 1 : 0;
        }

        $response = (object) [
            'date' => $date,
            'no'   => $no,
        ];

        return json_generator($response);
    }
}
