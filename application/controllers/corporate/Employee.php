<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employee extends CI_Controller {
	private $corporate_id;

	public function __construct(){
		parent::__construct();
		if ( !$this->ion_auth->in_group('corporate') ) {
			redirect(base_url('auth/login'), 'refresh');
		} else {
			// get current corporate id
			$this->load->model( ['CorporateMemberModel'] );
			$corporate = CorporateMemberModel::where('user_id', $this->ion_auth->user()->row()->id)->first();
			$this->corporate_id = $corporate->corporate_id;
		
			if ( !$this->corporate_id || $corporate->corporate->status != 'approved' ) {

				if ( !$this->corporate_id ) {
					$this->session->set_flashdata('message', 'Belum ada corporate profile');
				} 

				if ( $corporate->corporate->status == 'waiting' ) {
					$this->session->set_flashdata('message', 'Corporate belum disetujui');
				}

				if ( $corporate->corporate->status == 'rejected' ) {
					$this->session->set_flashdata('message', 'Corporate ditolak');
				}

				redirect(base_url('corporate/profile'), 'refresh');
			}
		}
	}

	public function index(){
		redirect(base_url('corporate/employee/list'), 'refresh');
	}

	public function list(){
		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'draw', 'draw', 'required' );
		if ($this->form_validation->run() === TRUE){

			$this->load->model( 'datatables' );
			
			$selected_column = array(
				"name"			=> "concat(first_name,'  ',COALESCE(last_name,''))",
				"username"		=> 'username',
				"email"			=> 'email',
				"role"			=> "(SELECT name FROM `groups` WHERE id=(SELECT group_id FROM users_groups WHERE user_id=users.id LIMIT 1))",
				"status"		=> '(SELECT status FROM corporate_member WHERE user_id=users.id)',
				"id"			=> 'id',
			);

			$column_order = array( null, 'username', 'email' );
			$column_search = array( 'username', 'email' );
			$order = array( 'id' => 'desc' );

			
			// get employee id
			$user_ids = [];
			foreach (CorporateModel::find( $this->corporate_id )->members as $key => $corporate_member) {
				// if ( $corporate_member->user_id != $this->ion_auth->user()->row()->id ) {
					$user_ids[] = $corporate_member->user_id;
				// }
			}

			if ( count($user_ids) == 0 ) {
				$where = ['id' => 99999999999999999];
			} else {
				$where = ['id' => $user_ids];
			}

		   	$custom_column 	= 	'
		   						<button type="button" class="btn btn-danger btn-sm btn-delete"><i class="fas fa-trash"></i> Delete</button>
	              				';

			$this->datatables->get_datatables( 'users', $selected_column, $column_order, $column_search, $order, $where, $custom_column );
		

		} else {
			$data = [
				'page'			=> 'corporate-employee-list',
				'page_title'	=> 'Employee',
				'scripts'		=> [
					// datatable
					'https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js',
					'https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js',
					'https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js',
					'https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js',

					// jquery validate
					'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js',

					// sweetalert2
					base_url('node_modules/sweetalert2/dist/sweetalert2.all.min.js'),

					// fastselect
					'https://cdnjs.cloudflare.com/ajax/libs/fastselect/0.7.3/fastselect.standalone.min.js',

					// app
					base_url( 'assets/js/app/corporate/employee/employee-list.js' ),
				],
				'styles'		=> [
					// datatable
					'https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css',
					'https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap4.min.css',

					// fastselect
					'https://cdnjs.cloudflare.com/ajax/libs/fastselect/0.7.3/fastselect.min.css',
				]
			];


			$this->load->view('template-part/admin/header', $data);
			$this->load->view('template-part/admin/sidebar-corporate', $data);
			$this->load->view('app/corporate/employee/employee-list', $data);
			$this->load->view('template-part/admin/footer', $data);
		}
	}

	public function waiting_list(){
		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'draw', 'draw', 'required' );
		if ($this->form_validation->run() === TRUE){

			$this->load->model( 'datatables' );
			
			$selected_column = array(
				"name"			=> "concat(first_name,'  ',COALESCE(last_name,''))",
				"username"		=> 'username',
				"email"			=> 'email',
				"role"			=> "(SELECT name FROM `groups` WHERE id=(SELECT group_id FROM users_groups WHERE user_id=users.id LIMIT 1))",
				"status"		=> '(SELECT status FROM corporate_member WHERE user_id=users.id)',
				"id"			=> 'id',
			);

			$column_order = array( null, 'username', 'email' );
			$column_search = array( 'username', 'email' );
			$order = array( 'id' => 'desc' );

			
			// get employee id
			$user_ids = [];
			foreach (CorporateModel::find( $this->corporate_id )->members as $key => $corporate_member) {
				if ( $corporate_member->user_id != $this->ion_auth->user()->row()->id && $corporate_member->status != 'approved' ) {
					$user_ids[] = $corporate_member->user_id;
				}
			}

			if ( count($user_ids) == 0 ) {
				$where = ['id' => 99999999999999999];
			} else {
				$where = ['id' => $user_ids];
			}

		   	$custom_column 	= '';

			$this->datatables->get_datatables( 'users', $selected_column, $column_order, $column_search, $order, $where, $custom_column );
		

		} else {
			$data = [
				'page'			=> 'corporate-employee-list-waiting',
				'page_title'	=> 'Employee',
				'scripts'		=> [
					// datatable
					'https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js',
					'https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js',
					'https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js',
					'https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js',

					// jquery validate
					'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js',

					// sweetalert2
					base_url('node_modules/sweetalert2/dist/sweetalert2.all.min.js'),

					// fastselect
					'https://cdnjs.cloudflare.com/ajax/libs/fastselect/0.7.3/fastselect.standalone.min.js',

					// app
					base_url( 'assets/js/app/corporate/employee/employee-waiting-list.js' ),
				],
				'styles'		=> [
					// datatable
					'https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css',
					'https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap4.min.css',

					// fastselect
					'https://cdnjs.cloudflare.com/ajax/libs/fastselect/0.7.3/fastselect.min.css',
				]
			];


			$this->load->view('template-part/admin/header', $data);
			$this->load->view('template-part/admin/sidebar-corporate', $data);
			$this->load->view('app/corporate/employee/employee-waiting-list', $data);
			$this->load->view('template-part/admin/footer', $data);
		}
	}

	public function remove(){
		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'id', 'id', 'required' );
		if ($this->form_validation->run() === TRUE){
			$result = CorporateMemberModel::where([
				'user_id'		=> $this->input->post('id'),
				'corporate_id'	=> $this->corporate_id
			])->forceDelete();

			json_generator( $result );
		}
	}

	public function import(){
		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'draw', 'draw', 'required' );
		if ($this->form_validation->run() === TRUE){

			$this->load->model( 'datatables' );
			
			$selected_column = array(
				"created_at"	=> "created_at",
				"status"		=> "status",
				"rejected_user"	=> 'id',
			);

			$column_order = array( null, 'created_at', 'status' );
			$column_search = array( 'status' );
			$order = array( 'id' => 'desc' );

			$where = ['corporate_id' => $this->corporate_id];

		   	$custom_column 	= 	'
	              				<a href="#" class="btn btn-info btn-sm btn-detail"><i class="fas fa-eye"></i> Lihat</a>
	              				';

			$this->datatables->get_datatables( 'employee_import_logs', $selected_column, $column_order, $column_search, $order, $where, $custom_column );
		

		} else {
			$data = [
				'page'			=> 'corporate-employee-import',
				'page_title'	=> 'Import Employee',
				'scripts'		=> [
					// datatable
					'https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js',
					'https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js',
					'https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js',
					'https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js',

					// jquery validate
					'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js',

					// sweetalert2
					base_url('node_modules/sweetalert2/dist/sweetalert2.all.min.js'),

					// fastselect
					'https://cdnjs.cloudflare.com/ajax/libs/fastselect/0.7.3/fastselect.standalone.min.js',

					// app
					base_url( 'assets/js/app/corporate/employee/employee-import-logs.js' ),
				],
				'styles'		=> [
					// datatable
					'https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css',
					'https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap4.min.css',

					// fastselect
					'https://cdnjs.cloudflare.com/ajax/libs/fastselect/0.7.3/fastselect.min.css',
				]
			];


			$this->load->view('template-part/admin/header', $data);
			$this->load->view('template-part/admin/sidebar-corporate', $data);
			$this->load->view('app/corporate/employee/employee-import-logs', $data);
			$this->load->view('template-part/admin/footer', $data);
		}
	}

	public function import_new(){

		$data = [
			'page'		=> 'corporate-employee-import',
			'page_title'	=> 'Import Employee New',
			'scripts'	=> [
				base_url('node_modules/sweetalert2/dist/sweetalert2.all.min.js'),
				'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js',
				base_url( 'assets/js/app/corporate/employee/employee-import-new.js' ),
			]
		];

		$this->load->view('template-part/admin/header', $data);
		$this->load->view('template-part/admin/sidebar-corporate', $data);
		$this->load->view('app/corporate/employee/employee-import-new', $data);
		$this->load->view('template-part/admin/footer', $data);
	}

	public function import_upload(){

		$config['upload_path']  	= FCPATH.'assets/imported-excel';
        $config['allowed_types']	= 'csv|xls|xlsx';
        $config['max_size']     	= 1000;
        $config['file_name']		= $this->corporate_id."_".$this->ion_auth->user()->row()->id;
        $config['overwrite']		= true;

        $this->load->library('upload', $config);

        if (!file_exists( $config['upload_path'] )) {
			mkdir( $config['upload_path'] , 0777, true);
			$this->load->helper('file');
			write_file($config['upload_path']."/index.html", ' ');
		}

        if ( $this->upload->do_upload('imported_file')){

        	$file = $this->upload->data();

        	// read file execl to json
        	$this->load->library('my_excel');
        	$result = $this->my_excel->excel_to_json( $config['upload_path']."/".$config['file_name'], $file['file_ext'] );
        	json_generator(
        		[
        			'file'	=> $config['file_name']."".$file['file_ext'],
        			'data'	=> $result
        		]
        	);

        } else {
        	$error = array('error' => $this->upload->display_errors());
        	print_r($error);
        }
	}

	public function import_save(){
		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'file', 'file', 'required' );
		if ($this->form_validation->run() === TRUE){
			$filepath = FCPATH.'assets/imported-excel/'.$this->input->post('file');

			$extension = pathinfo($filepath, PATHINFO_EXTENSION);
			$new_filename = md5( date('Y-m-d-H-i-s')."#".rand(1,9999) ).".".$extension;

			$this->load->model('EmployeeImportModel');

			$result = EmployeeImportModel::create([
				'corporate_id'	=> $this->corporate_id,
				'file'			=> $new_filename,
			]);

			if ( $result ) {
	            if ( file_exists( $filepath ) ) {
	                rename($filepath, FCPATH.'assets/imported-excel/'.$new_filename);
	            }

	            json_generator( $result );
			}

		}
	}

	public function import_detail( $id ){
		$this->load->model( 'EmployeeImportModel' );

		if ( $id && $imported_log = EmployeeImportModel::where(['id' => $id, 'corporate_id' => $this->corporate_id])->first() ) {

			$extension = ".".pathinfo(FCPATH.'assets/imported-excel/'.$imported_log->file, PATHINFO_EXTENSION);

			$file = explode($extension,$imported_log->file);

			$this->load->library('my_excel');

			$data = [
				'page'		=> 'corporate-employee-import',
				'page_title'	=> 'Import Employee Detail',
				'imported_log'	=> $imported_log,
				'imported_data'	=> $this->my_excel->excel_to_json( FCPATH.'assets/imported-excel/'.$file[0], $extension )
			];

			$this->load->view('template-part/admin/header', $data);
			$this->load->view('template-part/admin/sidebar-corporate', $data);
			$this->load->view('app/corporate/employee/employee-import-detail', $data);
			$this->load->view('template-part/admin/footer', $data);
		}
	}

	public function approval_member(){
		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'id', 'id', 'required' );
		$this->form_validation->set_rules( 'status', 'status', 'required' );
		if ($this->form_validation->run() === TRUE && $corporate_member = CorporateMemberModel::where('user_id',$this->input->post('id'))->first() ){
			if ( $this->input->post('status') != 'rejected' ) {
				$result = $corporate_member->update([ 'status' => $this->input->post('status') ]);
			} else {
				$result = $corporate_member->forceDelete();
			}
			json_generator($result);
		}
	}
}