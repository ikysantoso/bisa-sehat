<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	private $corporate_id;

	public function __construct(){
		parent::__construct();
		if ( !$this->ion_auth->in_group('corporate') ) {
			redirect(base_url('auth/login'), 'refresh');
		} else {
			// get current corporate id
			$this->load->model( ['CorporateMemberModel'] );
			$corporate = CorporateMemberModel::where('user_id', $this->ion_auth->user()->row()->id)->first();
			$this->corporate_id = $corporate->corporate_id;
		
			if ( !$this->corporate_id || $corporate->corporate->status != 'approved' ) {

				if ( !$this->corporate_id ) {
					$this->session->set_flashdata('message', 'Belum ada corporate profile');
				} 

				if ( $corporate->corporate->status == 'waiting' ) {
					$this->session->set_flashdata('message', 'Corporate belum disetujui');
				}

				if ( $corporate->corporate->status == 'rejected' ) {
					$this->session->set_flashdata('message', 'Corporate ditolak');
				}

				redirect(base_url('corporate/profile'), 'refresh');
			}
		}
	}

	public function index(){

		$this->load->model(['EmployeeImportModel']);

		$chart_data = [
			'total_employee'	=> CorporateMemberModel::where('corporate_id', $this->corporate_id)->count(),
			'import_waiting'	=> EmployeeImportModel::where(['status' => 'waiting', 'corporate_id' => $this->corporate_id])->count(),
			'import_approved'	=> EmployeeImportModel::where(['status' => 'approved', 'corporate_id' => $this->corporate_id])->count(),
			'import_latest'		=> null,
		];

		$data = [
			'page'			=> 'corporate-dashboard',
			'chart_data'	=> $chart_data,
		];

		$this->load->view('template-part/admin/header', $data);
		$this->load->view('template-part/admin/sidebar-corporate', $data);
		$this->load->view('app/corporate/dashboard', $data);
		$this->load->view('template-part/admin/footer', $data);
	}
}