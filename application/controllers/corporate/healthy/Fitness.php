<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fitness extends CI_Controller {

	private $corporate_id;

	public function __construct(){
		parent::__construct();
		if ( !$this->ion_auth->in_group('corporate') ) {
			redirect(base_url('auth/login'), 'refresh');
		} else {
			// get current corporate id
			$this->load->model( ['CorporateMemberModel'] );
			$corporate = CorporateMemberModel::where('user_id', $this->ion_auth->user()->row()->id)->first();
			$this->corporate_id = $corporate->corporate_id;
		
			if ( !$this->corporate_id || $corporate->corporate->status != 'approved' ) {

				if ( !$this->corporate_id ) {
					$this->session->set_flashdata('message', 'Belum ada corporate profile');
				} 

				if ( $corporate->corporate->status == 'waiting' ) {
					$this->session->set_flashdata('message', 'Corporate belum disetujui');
				}

				if ( $corporate->corporate->status == 'rejected' ) {
					$this->session->set_flashdata('message', 'Corporate ditolak');
				}

				redirect(base_url('corporate/profile'), 'refresh');
			}
		}
	}

	public function index(){
		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'draw', 'draw', 'required' );
		if ($this->form_validation->run() === TRUE){

			$this->load->model( 'datatables' );
			
			$selected_column = array(
				"name"				=> "concat(first_name,'  ',COALESCE(last_name,''))",
				"first_name"		=> 'first_name',
				"last_name"			=> 'last_name',
				"latest_updated"	=> '(SELECT created_at FROM fitness WHERE user_id=users.id LIMIT 1)',
				"total"				=> "(SELECT count(*) FROM fitness WHERE user_id=users.id)",
				"id"				=> 'id',
			);

			$column_order = array( null, null, 'first_name', 'last_name', 'latest_updated', 'total' );
			$column_search = array('first_name','last_name');
			$order = array( 'id' => 'desc' );

			
			// get employee id
			$user_ids = [];
			foreach (CorporateModel::find( $this->corporate_id )->members as $key => $corporate_member) {
				// if ( $corporate_member->user_id != $this->ion_auth->user()->row()->id ) {
					$user_ids[] = $corporate_member->user_id;
				// }
			}

			if ( count($user_ids) == 0 ) {
				$where = ['id' => 99999999999999999];
			} else {
				$where = ['id' => $user_ids];
			}

		   	$custom_column 	= 	'
		   						<a href="#" class="btn btn-download btn-secondary btn-sm"><i class="fas fa-download"></i> Download</a>
		   						<a href="#" class="btn btn-detail btn-info btn-sm"><i class="fas fa-file"></i> Detail</a>
	              				';

			$this->datatables->get_datatables( 'users', $selected_column, $column_order, $column_search, $order, $where, $custom_column );
		

		} else {
			$data = [
				'page'			=> 'corporate-healthy-fitness',
				'page_title'	=> 'Fitness Data of Employees',
				'scripts'		=> [
					// datatable
					'https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js',
					'https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js',
					'https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js',
					'https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js',

					// jquery validate
					'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js',

					// sweetalert2
					base_url('node_modules/sweetalert2/dist/sweetalert2.all.min.js'),

					// fastselect
					'https://cdnjs.cloudflare.com/ajax/libs/fastselect/0.7.3/fastselect.standalone.min.js',

					// app
					base_url( 'assets/js/app/corporate/healthy/fitness.js' ),
				],
				'styles'		=> [
					// datatable
					'https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css',
					'https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap4.min.css',

					// fastselect
					'https://cdnjs.cloudflare.com/ajax/libs/fastselect/0.7.3/fastselect.min.css',
				]
			];


			$this->load->view('template-part/admin/header', $data);
			$this->load->view('template-part/admin/sidebar-corporate', $data);
			$this->load->view('app/corporate/healthy/fitness', $data);
			$this->load->view('template-part/admin/footer', $data);
		}
	}

	public function download( $id = null ){

		if ( $id && $user = UserModel::find( $id ) ) {

			if ( $user->corporate_member->corporate_id == $this->corporate_id ) {

				$this->load->model( 'FitnessModel' );

				$fitnesses = FitnessModel::where([
					'user_id'	=> $id,
				])->get();
				
				if ( $fitnesses->count() > 0 ) {
					$this->load->library('my_pdf');
					$this->my_pdf->fitness_report( ['fitnesses' => $fitnesses] );
				} 

			}
		}
	}

	public function single_download( $id = null ){
		$this->load->model( 'FitnessModel' );

		if ( $id && $fitnesses = FitnessModel::where(['id'	=> $id])->get() ) {
			
			if ( $fitnesses->count() > 0 && ($fitnesses[0]->user->corporate_member->corporate_id == $this->corporate_id) ) {
				$this->load->library('my_pdf');
				$this->my_pdf->fitness_report( ['fitnesses' => $fitnesses] );
			}

		}
	}

	public function detail( $id ){

		if ( $id && $user = UserModel::find( $id ) ) {

			if ( $user->corporate_member->corporate_id == $this->corporate_id ) {

				$this->load->model( 'FitnessModel' );

				$data = [
					'fitnesses'		=> FitnessModel::where( 'user_id', $id )->orderBy('id', 'desc')->get(),
					'page_title'	=> 'Fitness Detail',
					'page'			=> 'corporate-healthy-fitness',
					'user_id'		=> $id,
				];

				$this->load->view('template-part/admin/header', $data);
				$this->load->view('template-part/admin/sidebar-corporate', $data);
				$this->load->view('app/corporate/healthy/fitness-detail', $data);
				$this->load->view('template-part/admin/footer', $data);
			}
		}
	}
}