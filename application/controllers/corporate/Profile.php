<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {
	public function __construct(){
		parent::__construct();
		if ( !$this->ion_auth->in_group('corporate') ) {
			redirect(base_url('auth/login'), 'refresh');
		} 
	}

	public function index(){

		$this->load->model( 'CorporateMemberModel' );
		$corporate_member = CorporateMemberModel::where( 'user_id', $this->ion_auth->user()->row()->id )->first();

		$data = [
			'page'		=> 'corporate-profile',
			'corporate'	=> ($corporate_member) ? $corporate_member->corporate : null,
			'scripts'	=> [
				base_url('node_modules/sweetalert2/dist/sweetalert2.all.min.js'),
				'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js',
				base_url( 'assets/js/app/corporate/profile/profile-update.js' ),
			]
		];

		$this->load->view('template-part/admin/header', $data);
		$this->load->view('template-part/admin/sidebar-corporate', $data);
		$this->load->view('app/corporate/profile/profile-info', $data);
		$this->load->view('template-part/admin/footer', $data);
	}

	public function save(){
		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'name', 'name', 'required' );
		$this->form_validation->set_rules( 'office', 'office', 'required' );
		$this->form_validation->set_rules( 'description', 'description', 'required' );
		if ($this->form_validation->run() === TRUE){
			$this->load->model( 'CorporateModel' );

			$data_corporate = [
				'name'			=> $this->input->post('name'),
				'office'		=> $this->input->post('office'),
				'description'	=> $this->input->post('description'),
			];

			if ( !$this->input->post('id') ) {
				// create new
				$corporate = CorporateModel::create( $data_corporate );
				$result = $corporate->members()->create([
					'user_id'	=> $this->ion_auth->user()->row()->id
				]);
			} else {
				$corporate = CorporateModel::find( $this->input->post('id') );
				$result = $corporate->update( $data_corporate );
			}

			json_generator($result);
		}
	}
}