<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->helper(array('url'));
		$this->load->library('pagination');
		$this->load->model('PagesModel');
	}

	public function article(){
		//get data keyword
		if ($this->input->post('submit')) {
			$data['keyword'] = $this->input->post('keyword');
			$this->session->set_userdata('keyword', $data['keyword']);
		} else {
			$data['keyword'] = $this->session->userdata('keyword');
		}

		//setup config
		$this->db->like('title', $data['keyword']);
		$this->db->from('page');
		$config['total_rows'] = $this->db->count_all_results();
		$config['per_page'] = 4;
		$from = $this->uri->segment(4);

		$this->pagination->initialize($config);		
		
		$data = [
			'article'  => $this->PagesModel->data($config['per_page'],$from, $data['keyword']),
			'isBack'   => 'true',
		];
		$this->load->view('template-part/header', $data);
		$this->load->view('template-part/navbar', $data);
		$this->load->view('app/frontpage/article/page-article', $data);
		$this->load->view('template-part/footer', $data);	
	}

	public function detail($id){
		$data = [
			'article'      => $this->PagesModel->get_id($id),
			'isBackDetail' => 'true',
		];
		$this->load->view('template-part/header', $data);
		$this->load->view('template-part/navbar', $data);
		$this->load->view('app/frontpage/article/page-detail', $data);
		$this->load->view('template-part/footer', $data);
	}

}
