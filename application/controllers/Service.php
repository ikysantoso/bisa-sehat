<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Service extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();

		if (!$this->ion_auth->logged_in()) {
			redirect(base_url('auth/login'), 'refresh');
		} else if ($this->ion_auth->is_admin()) {
			redirect(base_url('admin/dashboard'), 'refresh');
		}
	}
	public function index()
	{
		redirect(base_url('service/all'), 'refresh');
	}

	public function all(){

		$this->load->model( 'ServiceModel' );

		$data = [
			'page'				=> 'service',
			'page_detail'		=> 'service',
			'services'	=> ServiceModel::all(),
		];

		$this->load->view('template-part/header', $data);
		$this->load->view('template-part/navbar', $data);
		$this->load->view('app/frontpage/service/service-all', $data);
		$this->load->view('template-part/footer', $data);
	}

	public function category( $id=null ){

		$this->load->model( ['ServiceModel', 'ServiceCategoryModel'] );

		$data = [
			'page'			=> 'service',
			'page_detail'	=> 'service-category-'.$id,
			'category'	=> ServiceCategoryModel::find( $id ),
			'services'	=> ServiceModel::where('category_id', $id)->get(),
		];

		$this->load->view('template-part/header', $data);
		$this->load->view('template-part/navbar', $data);
		$this->load->view('app/frontpage/service/service-all', $data);
		$this->load->view('template-part/footer', $data);
	}

	public function detail( $id=null ){
		$this->load->model( 'ServiceModel' );

		if ( $id && $service = ServiceModel::find( $id ) ) {
			$data = [
				'page'				=> 'service',
				'page_detail'		=> 'service-detail',
				'service'			=> $service,
				'whatsapp_number'	=> SettingModel::where('meta_key','whatsapp_number')->first()->meta_value,
			];

			$this->load->view('template-part/header', $data);
			$this->load->view('template-part/navbar', $data);
			$this->load->view('app/frontpage/service/service-detail', $data);
			$this->load->view('template-part/footer', $data);
		}
	}
}
?>