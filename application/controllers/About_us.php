<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About_us extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		if ($this->ion_auth->is_admin()) {
			redirect(base_url('admin/dashboard'), 'refresh');
		}
	}
	public function index()
	{
			$data = [
				'page'	=> 'about_us',
				'isBackDetail' => 'true',
			];

			$this->load->view('template-part/header', $data);
			$this->load->view('template-part/navbar', $data);
			$this->load->view('app/frontpage/about_us', $data);
			$this->load->view('template-part/footer', $data);
		
	}
	public function tdpse()
	{
			$data = [
				'page'	=> 'tdpse',
				'isBackDetail' => 'true',
			];

			$this->load->view('template-part/header', $data);
			$this->load->view('template-part/navbar', $data);
			$this->load->view('app/frontpage/tdpse', $data);
			$this->load->view('template-part/footer', $data);
		
	}
}
