  <div class="content-page">
    <!-- Page Content -->
      <div class="products_list">
        <div class="container">
	        <div class="row">
            <div class="col-md-6">
              <form action="<?= base_url('page/article') ?>" method="post">
                <div class="input-group border-1" style="border-radius:10px; background-color:#DCDCDC;">
                  <input type="search" class="form-control border-0 bg-transparent custom-text-main" name="keyword" placeholder="Cari Article...">
                    <div class="input-group-append">
                      <input class="btn btn-primary" type="submit" name="submit" value="Cari"></input>
                    </div>
                </div>
              </form>   
            </div>
	        </div>
          <?php if (empty($article)) : ?>
            <div class="alert alert-danger" role="alert" style="margin:100px;">
              Tidak Ada Data Silahkan Input Kembali
            </div>
          <?php endif; ?>	   
          <?php foreach($article as $key => $page): ?>
            <div class="image-article" style="max-width: 560px; margin:40px 0 0 5px;">
              <div class="d-flex flex-row">
                <div class="col-md-4">
                  <div class="card-content">
                      <img src="<?php echo ($page->image) ? base_url('assets/images/upload/page/'.$page->image) : 'https://via.placeholder.com/348x261?text=No%20Image' ?>" width="100%">
                  </div>
                </div>
                <div class="col-md-8">
                  <div class="bundle-content">
                    <p class="article-date text-right"><?php echo $page->created_at; ?></p>
                    <h4><?php echo $page->title; ?></h4>
                    <p class="content"><?php echo printTruncated(100, $page->description) ?></p>
                    <p><a class="card-text" href="<?php echo base_url('page/detail/'.$page->id); ?>">Lebih Lanjut</a></p>
                  </div>
                </div>    
              </div> 
            </div>
          <?php endforeach; ?>
          <div class="row justify-content-start" style="margin: 30px;">
            <?php echo $this->pagination->create_links(); ?>
          </div>
        </div>
      </div>
  </div>
