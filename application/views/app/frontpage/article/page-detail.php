<div class="content-page">
    <?php foreach($article as $key => $page): ?>
    <div class="container medical-record">
        <div class="embed-responsive embed-responsive-21by9 bg-vertical-center" style="background-image: url('<?php echo ($page->image) ? base_url('assets/images/upload/page/'.$page->image) : '' ?>'); margin-top: 15px; border-radius:10px;"></div>
        <div class="detail-article" style="margin-top:25px;">
            <h4 class="mb-2 title"><?php echo $page->title; ?></h4>
            <p class="content"><?php echo $page->description ?></p>
        </div>
    </div>
    <?php endforeach; ?>
</div>
