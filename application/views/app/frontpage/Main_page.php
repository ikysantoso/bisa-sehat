<div class="container" style="margin-top: 80px">
  <div class="text-left">
    <?php if( $this->ion_auth->logged_in()): ?>
    <div class="text-greeting">
      <h4>Halo, <?php echo $this->ion_auth->user()->row()->username ?></h4>
    </div>
    <?php endif; ?>
    <div class="text-content">
      <h5>Silahkan pilih halaman yang anda inginkan ...</h5> 
    </div>
    <div class="health">
      <a href="<?php echo base_url('home'); ?>">
        <h4><img src="assets/images/health_white.png" style="height:70px;" alt=""><span style="margin-left:20px;">Kesehatan</span></h4>
      </a>
    </div>
    <div class="ecommerce">
      <a href="<?php echo base_url('home'); ?>">
        <h4><img src="assets/images/ecommerce_white.png" style="height:70px;" alt=""><span style="margin-left:35px;">Belanja</span></h4>
      <a>
    </div>
  </div>
  <div class="sosmed-mobile d-md-none d-lg-none d-xl-none">
      <a href="https://www.facebook.com/aplikasibisa.id" target="_blank" title="Facebook"><img src="<?php echo base_url('assets/images/social_media/logo_fb.png'); ?>" alt=""></a>    
      <a href="https://www.instagram.com/aplikasibisa.id/" target="_blank" title="Instagram"><img src="<?php echo base_url('assets/images/social_media/logo_ig.png'); ?>" alt=""></a>
      <a href="https://youtube.com/channel/UC1zjZyzNgWNUc99szQ6HL0A" target="_blank" title="Youtube"><img src="<?php echo base_url('assets/images/social_media/logo_youtube.png'); ?>" alt=""></a>
      <a href="https://wa.me/6281318465774" target="_blank" title="WhatsApp"><img src="<?php echo base_url('assets/images/social_media/logo_wa.png'); ?>" alt=""></i></a>
      <a href="https://vt.tiktok.com/ZSef1RXsc" target="_blank" title="Tiktok"><img src="<?php echo base_url('assets/images/social_media/logo_tiktok.png'); ?>" alt=""></a>
  </div>
</div>

