<div class="content-page">
	<div class="page-heading header-text" style="background-image:url(<?php echo base_url('assets/images/products/hero.jpg')?>);">
		<div class="overlay"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="text-content">
						<h2><?php echo $title; ?></h2>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="product-single">
		<div class="container">
			<div class="alert alert-<?php echo $type ?>"><?php echo $message; ?></div>
		</div>
	</div>  
</div>