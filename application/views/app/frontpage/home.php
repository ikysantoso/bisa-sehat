<div class="content-page">
    <?php if( $this->ion_auth->logged_in() )
        {
           $this->load->model(['BisapayModel', 'CustomerBankModel']);
           $bisapay = BisapayModel::where(
                [
                    'user_id'   => $this->ion_auth->user()->row()->id,
                    'status'    => 'active'
                ]
            )->first();
            $customerbanks = CustomerBankModel::where('user_id', $this->ion_auth->user()->row()->id)->get();
        }
    ?>

    <div class="banner header-text" style="margin-top: 90px">
        <?php if($sliders->count() > 0): ?>
        <div class="container">
            <div class="owl-banner owl-carousel">
                <?php foreach ($sliders as $key => $slider): ?>
                    <img src="<?php echo ($slider->image) ? base_url('assets/images/upload/slider/'.$slider->image) : '' ?>" style="margin-top: 7px; border-radius: 10px; object-fit: fill;"/>
                <?php endforeach; ?>
            </div>
        </div>
        <?php else: ?>
            <div class="alert alert-info">Belum ada slider</div>
        <?php endif; ?>
    </div>
    
    <!-- Web view chart -->
    <!-- <div class="container mt-5 d-none d-lg-block d-md-block"> -->
    <div class="container mt-5 d-none d-lg-block d-md-block">
        <?php $user = UserModel::find( $this->ion_auth->user()->row()->id ); ?>
        <?php $medical_record = $user->medical_records->last(); ?>
            <?php if($medical_record): $problem = []; ?>
                <div class="row ml-1" style="border-radius: 16px; background-color: #ffffff; box-shadow: 0px 2px 2px 2px rgba(0, 128, 128, 0.1); padding: 10px">
                    <div class="col-12">
                        <span class="text-headline">Monitor Kesehatan Pribadi</span>
                    </div>
                    <div class="col-12 col-md-6">
                        <div class="line-charts"> 
                            <canvas id="chart-bmi" height="300"></canvas>
                        </div>
                    </div>
                    <div class="col-12 col-md-6">                        
                    <?php 
                        if($medical_record->blood_pressure_scale()['desc'] != 'Belum ada data'){
                            echo $problem[] = "<div class='line-charts'><canvas id='chart-blood-pressure' height='300'></canvas></div>";
                                }
                                else {
                                    echo $problem[] = "<div class='alert alert-warning'>Belum ada data tekanan darah</div>";
                                }
                            ?>
                    </div>
                    <div class="col-12 col-md-6">                        
                    <?php 
                        if($medical_record->blood_sugar_level_scale()['desc'] != 'Belum ada data'){
                            echo $problem[] = "<div class='line-charts'><canvas id='chart-blood-sugar' height='300'></canvas></div>";
                                }
                                else {
                                    echo $problem[] = "<div class='alert alert-warning'>Belum ada data gula darah</div>";
                                }
                            ?>
                    </div>
                    <div class="col-12 col-md-6">
                    <?php 
                        if($medical_record->cholesterol_level_scale()['desc'] != 'Belum ada data'){
                            echo $problem[] = "<div class='line-charts'><canvas id='chart-cholesterol' height='300'></canvas></div>";
                                }
                                else {
                                    echo $problem[] = "<div class='alert alert-warning'>Belum ada data kolesterol</div>";
                                }
                            ?>
                    </div>
                    <div class="col-12 col-md-6">
                    <input type="hidden" id="line-uric-acid" value="<?=$medical_record->uric_acid_level_scale()['line']?>">                        
                    <?php 
                        if($medical_record->uric_acid_level_scale()['desc'] != 'Belum ada data'){
                            echo $problem[] = "<div class='line-charts'><canvas id='chart-uric-acid' height='300'></canvas></div>";
                                }
                                else {
                                    echo $problem[] = "<div class='alert alert-warning'>Belum ada data asam urat</div>";
                                }
                            ?>
                    </div>
                    <div class="col-12 col-md-6">
                        <div class="line-charts"> 
                            <canvas id="chart-fitness" height="300"></canvas>
                        </div>
                    </div>    
                </div>
            <?php else: ?>
            <div class="col-md-12">
                <div class="alert alert-warning">Belum ada data monitoring kesehatan</div>
            </div>
        <?php endif; ?>
    </div> 
        
    <!-- Mobile view chart -->
    <div class="container d-lg-none d-md-none">
    <?php $user = UserModel::find( $this->ion_auth->user()->row()->id ); ?>
        <?php $medical_record = $user->medical_records->last(); ?>
            <?php if($medical_record): $problem = []; ?>
            <div class="row">
            <span class="text-headline mb-3 mt-4">Monitor Kesehatan Pribadi</span>
                <div class="owl-monitoring owl-carousel mr-2">
                    <div class="owl-slide">
                        <button type="button" data-number="1" class="active">Body Mass Index</button>
                    </div>
                    <div class="owl-slide">
                        <button type="button" data-number="2">Tekanan Darah</button>
                    </div>
                    <div class="owl-slide">
                        <button type="button" data-number="3">Gula Darah</button>
                    </div>
                    <div class="owl-slide">
                        <button type="button" data-number="4">Kolesterol</button>
                    </div>
                    <div class="owl-slide">
                        <button type="button" data-number="5">Asam Urat</button>
                    </div>
                    <div class="owl-slide">
                        <button type="button" data-number="6">Status Kebugaran</button>
                    </div>
                </div>
            </div>
            <div class="contentSection">
                <div class="content" data-number="1">
                    <div class="line-charts"> 
                        <canvas id="chart-bmi-mobile" height="300"></canvas>
                    </div>
                </div>
                <div class="content" data-number="2">
                    <?php 
                        if($medical_record->blood_pressure_scale()['desc'] != 'Belum ada data'){
                            echo $problem[] = "<div class='line-charts'><canvas id='chart-blood-pressure-mobile' height='300'></canvas></div>";
                                }
                                else {
                                    echo $problem[] = "<div class='alert alert-warning'>Belum ada data tekanan darah</div>";
                                }
                            ?>
                </div>
                <div class="content" data-number="3">
                    <?php 
                        if($medical_record->blood_sugar_level_scale()['desc'] != 'Belum ada data'){
                            echo $problem[] = "<div class='line-charts'><canvas id='chart-blood-sugar-mobile' height='300'></canvas></div>";
                                }
                                else {
                                    echo $problem[] = "<div class='alert alert-warning'>Belum ada data gula darah</div>";
                                }
                            ?>
                </div>
                <div class="content" data-number="4">
                    <?php 
                        if($medical_record->cholesterol_level_scale()['desc'] != 'Belum ada data'){
                            echo $problem[] = "<div class='line-charts'><canvas id='chart-cholesterol-mobile' height='300'></canvas></div>";
                                }
                                else {
                                    echo $problem[] = "<div class='alert alert-warning'>Belum ada data kolesterol</div>";
                                }
                            ?>
               </div>
                <div class="content" data-number="5">
                    <input type="hidden" id="line-uric-acid" value="<?=$medical_record->uric_acid_level_scale()['line']?>">                        
                    <?php 
                        if($medical_record->uric_acid_level_scale()['desc'] != 'Belum ada data'){
                            echo $problem[] = "<div class='line-charts'><canvas id='chart-uric-acid-mobile' height='300'></canvas></div>";
                                }
                                else {
                                    echo $problem[] = "<div class='alert alert-warning'>Belum ada data asam urat</div>";
                                }
                            ?>
                </div>
                <div class="content" data-number="6">
                    <div class="line-charts"> 
                        <canvas id="chart-fitness-mobile" height="300"></canvas>
                    </div>
                </div>
            </div>
        <?php else: ?>
            <div class="col-md-12">
                <div class="alert alert-warning">Belum ada data monitoring kesehatan</div>
            </div>
        <?php endif; ?>
    </div>

    <!-- Web view health -->
    <div class="container mt-5 mb-5 d-none d-lg-block d-md-block">
        <?php if( $this->ion_auth->logged_in()): ?>
            <?php if (isset($isHealth) && $isHealth == "true"): ?>
                <?php $user = UserModel::find( $this->ion_auth->user()->row()->id ); ?>
                    <?php $medical_record = $user->medical_records->last(); ?>
                        <?php if($medical_record): $problem = []; ?>
                        <div class="row">
                            <div class="col-12 d-flex justify-content-between">
                                <div>
                                    <span class="text-headline">Status Kesehatan Terkini</span>
                                </div>
                                <div style="background-color: #008080; border-radius: 32px;">
                                    <div class="customNextBtn"><i class="fa fa-chevron-right text-[#FFFFFF]" aria-hidden="true" style="font-size: 25px;"></i></div>
                                    <div class="customPreviousBtn"><i class="fa fa-chevron-left text-[#FFFFFF]" aria-hidden="true" style="font-size: 25px;"></i></div>
                                </div>
                            </div>
                            <div class="owl-diagnose owl-carousel">
                                <div class="card-health">
                                    <h4 class="text-box">Indeks Masa Tubuh</h4>
                                    <img src="assets/images/rekam_medis/bmi.png" alt="">
                                    <div class="description-health d-flex justify-content-between">
                                        <div>
                                            <p class="number"><?php echo $medical_record->bmi()['bmi']; ?></p>
                                            <p class="desc"><?php echo health_result_color($medical_record->bmi_scale()['desc']); ?></p>
                                            <?php 
                                                if($medical_record->bmi_scale()['count'] == 1){
                                                    $problem[] = 'overweight';
                                                }else if($medical_record->bmi_scale()['count'] == 2){
                                                    $problem[] = 'kegemukan';
                                                }
                                            ?>
                                        </div>
                                        <div>
                                            <p><?php echo health_result_circle($medical_record->bmi_scale()['desc']);?></p>
                                        </div>
                                    </div>
                                </div>  
                                <div class="card-health">
                                    <h4 class="text-box">Kolesterol</h4>
                                    <img src="assets/images/rekam_medis/kolesterol.png" alt="">
                                    <div class="description-health d-flex justify-content-between">
                                        <div>
                                            <p class="number"><?php echo $medical_record->cholesterol_level; ?> mg/dL</p>
                                            <p class="desc"><?php echo health_result_color($medical_record->cholesterol_level_scale()['desc']); ?></p>
                                            <?php 
                                                if($medical_record->cholesterol_level_scale()['count'] > 0){
                                                    $problem[] = 'kolesterol';
                                                }
                                            ?>
                                        </div>
                                        <div>
                                            <p><?php echo health_result_circle($medical_record->cholesterol_level_scale()['desc']);?></p>
                                        </div>
                                    </div>
                                </div> 
                                <div class="card-health">
                                    <h4 class="text-box">Tekanan Darah</h4>
                                    <img src="assets/images/rekam_medis/tekanan_darah.png" alt="">
                                    <div class="description-health d-flex justify-content-between">
                                        <div>
                                            <p class="number"><?php echo $medical_record->sistole; ?>/<?php echo $medical_record->diastole; ?> mmHg</p>
                                            <p class="desc"><?php echo health_result_color($medical_record->blood_pressure_scale()['desc']); ?></p>
                                            <?php 
                                                if($medical_record->blood_pressure_scale()['count'] > 0){
                                                    $problem[] = 'hipertensi';
                                                }
                                            ?>
                                        </div>
                                        <div>
                                            <p><?php echo health_result_circle($medical_record->blood_pressure_scale()['desc']); ?></p>
                                        </div>
                                    </div>
                                </div> 
                                <div class="card-health">
                                    <h4 class="text-box">Asam Urat</h4>
                                    <img src="assets/images/rekam_medis/asam_urat.png" alt="">
                                    <div class="description-health d-flex justify-content-between">
                                        <div>
                                            <p class="number"><?php echo $medical_record->uric_acid_level; ?> mg/dL</p>
                                            <p class="desc"><?php echo health_result_color($medical_record->uric_acid_level_scale()['desc']); ?></p>
                                            <?php 
                                                if($medical_record->uric_acid_level_scale()['count'] > 0){
                                                    $problem[] = 'asam urat';
                                                    }
                                                ?>
                                        </div>
                                        <div>
                                            <p><?php echo health_result_circle($medical_record->uric_acid_level_scale()['desc']); ?></p>
                                        </div>
                                    </div>
                                </div>   
                                <div class="card-health">
                                    <h4 class="text-box">Gula Darah</h4>
                                    <img src="assets/images/rekam_medis/gula_darah.png" alt="">
                                    <div class="description-health d-flex justify-content-between">
                                        <div>
                                            <p class="number"><?php echo $medical_record->blood_sugar_level; ?> mg/dL</p>
                                            <p class="desc"><?php echo health_result_color($medical_record->blood_sugar_level_scale()['desc']); ?></p>
                                            <?php 
                                                if($medical_record->blood_sugar_level_scale()['count'] > 0){
                                                    $problem[] = 'diabetes';
                                                }
                                            ?>
                                        </div>
                                        <div>
                                            <p><?php echo health_result_circle($medical_record->blood_sugar_level_scale()['desc']); ?></p>
                                        </div>
                                    </div>
                                </div>
                                <?php $fitness = $user->fitnesses->last(); ?>
                                <div class="card-health">
                                    <h4 class="text-box">Status Kebugaran</h4>
                                    <img src="assets/images/rekam_medis/bugar.png" alt="">
                                    <div class="description-health mb-5">
                                        <h5><?php echo ($fitness) ? $fitness->mileage_result()['status'] : 'Belum ada data'; ?></h5>
                                    </div>
                                </div>                         
                            </div>
                        </div>
                        <?php else: ?>
                        <div class="col-md-12">
                            <div class="alert alert-warning mt-3">Belum ada data test kesehatan</div>
                        </div>
            <?php endif; ?>
            <?php endif; ?>
        <?php endif; ?>
    </div>
        
        <!-- Mobile view health -->
        <?php if( $this->ion_auth->logged_in()): ?>
        <?php if (isset($isHealth) && $isHealth == "true"): ?>
        <div class="container d-lg-none d-md-none">  
          <div class="row">  
                <?php $user = UserModel::find( $this->ion_auth->user()->row()->id ); ?>
                <?php $medical_record = $user->medical_records->last(); ?>
                <?php 
               if($medical_record): 
                    $problem = [];
                ?>
             <div class="col-6 col-md-4">
                <div class="card-health">
                    <p class="text-box">Indeks Masa Tubuh</p>
                    <img src="assets/images/rekam_medis/bmi.png" alt="">
                    <div class="description-health d-flex justify-content-between">
                        <div>
                            <p class="number"><?php echo $medical_record->bmi()['bmi']; ?></p>
                            <p class="desc"><?php echo health_result_color($medical_record->bmi_scale()['desc']); ?></p>
                        </div>
                        <div>
                            <p><?php echo health_result_circle($medical_record->bmi_scale()['desc']);?></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-6 col-md-4">
                <div class="card-health">
                    <p class="text-box">Kolesterol</p>
                    <img src="assets/images/rekam_medis/kolesterol.png" alt="">
                    <div class="description-health d-flex justify-content-between">
                        <div>
                            <p class="number"><?php echo $medical_record->cholesterol_level; ?> mg/dL</p>
                            <p class="desc"><?php echo health_result_color($medical_record->cholesterol_level_scale()['desc']); ?></p>
                        </div>
                        <div>
                            <p><?php echo health_result_circle($medical_record->cholesterol_level_scale()['desc']);?></p>
                        </div>
                    </div>
                </div>
            </div>
             <div class="col-6 col-md-4">
                <div class="card-health">
                    <p class="text-box">Tekanan Darah</p>
                    <img src="assets/images/rekam_medis/tekanan_darah.png" alt="">
                    <div class="description-health d-flex justify-content-between">
                        <div>
                            <p class="number"><?php echo $medical_record->sistole; ?>/<?php echo $medical_record->diastole; ?> mmHg</p>
                            <p class="desc"><?php echo health_result_color($medical_record->blood_pressure_scale()['desc']); ?></p>
                        </div>
                        <div>
                            <p><?php echo health_result_circle($medical_record->blood_pressure_scale()['desc']); ?></p>
                        </div>
                    </div>
                </div>
             </div>
             <div class="col-6 col-md-4">
                <div class="card-health">
                    <p class="text-box">Asam Urat</p>
                    <img src="assets/images/rekam_medis/asam_urat.png" alt="">
                    <div class="description-health d-flex justify-content-between">
                        <div>
                            <p class="number"><?php echo $medical_record->uric_acid_level; ?> mg/dL</p>
                            <p class="desc"><?php echo health_result_color($medical_record->uric_acid_level_scale()['desc']); ?></p>
                        </div>
                        <div>
                            <p><?php echo health_result_circle($medical_record->uric_acid_level_scale()['desc']); ?></p>
                        </div>
                    </div>
                </div>
             </div>
             <div class="col-6 col-md-4">
               <div class="card-health">
                    <p class="text-box">Gula Darah</p>
                    <img src="assets/images/rekam_medis/gula_darah.png" alt="">
                    <div class="description-health d-flex justify-content-between">
                        <div>
                            <p class="number"><?php echo $medical_record->blood_sugar_level; ?> mg/dL</p>
                            <p class="desc"><?php echo health_result_color($medical_record->blood_sugar_level_scale()['desc']); ?></p>
                        </div>
                        <div>
                            <p><?php echo health_result_circle($medical_record->blood_sugar_level_scale()['desc']); ?></p>
                        </div>
                    </div>
               </div>
             </div>
            <?php $fitness = $user->fitnesses->last(); ?>
            <div class="col-6 col-md-4">
               <div class="card-health">
                    <p class="text-box">Status Kebugaran</p>
                    <img src="assets/images/rekam_medis/bugar.png" alt="">
                    <div class="description-health">
                        <h5><?php echo ($fitness) ? $fitness->mileage_result()['status'] : 'Belum ada data'; ?></h5>
                    </div>
               </div>
            </div>
          <?php else: ?>
         <div class="container">
            <div class="alert alert-warning mt-2">Belum ada data test kesehatan</div>
         </div>
         <?php endif; ?>
         </div>
         </div>
        <?php endif; ?>
        <?php endif; ?>

    <div class="container">
        <div class="row">
            <div class="col-12 d-flex justify-content-between">
                <div class="text-headline">
                    <span>Artikel Kesehatan</span>
                </div>
                <div class="d-none d-lg-block d-md-block" style="background-color: #008080; border-radius: 32px;">
                    <div class="customNextArticle"><i class="fa fa-chevron-right text-[#FFFFFF]" aria-hidden="true" style="font-size: 25px;"></i></div>
                    <div class="customPreviousArticle"><i class="fa fa-chevron-left text-[#FFFFFF]" aria-hidden="true" style="font-size: 25px;"></i></div>
                </div>
            </div>
            <div class="owl-article owl-carousel">
                <?php foreach($article as $key => $page): ?>
                    <a href="<?php echo base_url('page/detail/'.$page->id); ?>">
                        <div class="card-article">
                            <div>
                                <img src="<?php echo ($page->image) ? base_url('assets/images/upload/page/'.$page->image) : 'https://via.placeholder.com/348x261?text=No%20Image' ?>" width="100%" style="border-radius: 16px">
                            </div>
                            <div class="bundle-content">
                                <h4><?php echo $page->title; ?></h4>
                                <p class="content-article mt-1"><?php echo printTruncated(80, $page->description) ?></p>
                            </div>
                        </div>
                    </a>
                <?php endforeach; ?>
                <?php if($article->count() >= 6): ?>
                    <a href="<?php echo base_url('page/article'); ?>">
                        <div class="card-article">
                            <img src="assets/images/more.png" alt="">
                            <div class="bundle-content">
                                <h4>Lihat Lainnya</h4>
                            </div>
                        </div>
                    </a>
                <?php endif; ?>
            </div>
        </div>
    </div>
    

    <div class="modal" id="modal-buy" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Pilih Gudang</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="media">
                        <img id="product-image" class="mr-3" style="width: 150px;height: auto" />
                        <div class="media-body d-flex align-self-stretch flex-column justify-content-between">
                            <div id="modal-product-container"></div>
                            <div class="d-flex flex-fill flex-column justify-content-center">
                                <select id="product-stock-list" class="form-control"></select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-between">
                    <button id="btn-add-to-cart" disabled="disabled" type="button" class="btn btn-custom" style="background-color: #87d7bf;" onclick="addProductToCart()"><img src="<?php echo base_url('assets/images/shop_cart.png'); ?>" style="height: 20px!important;width: auto!important;"><span style="color:#ffffff; margin-left:4px;">Masukan Keranjang</span></button>
                    <button id="btn-payment" disabled="disabled" type="button" class="btn btn-primary" onclick="continuePayment()"><span>Lanjut Pembayaran</span></button>
                </div>
            </div>
        </div>
    </div>
</div>
