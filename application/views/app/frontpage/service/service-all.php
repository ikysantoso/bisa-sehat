<div class="content-page">
	<!-- Page Content -->
	<div class="page-heading header-text" style="background-image:url(<?php echo base_url('assets/images/service/hero.jpg')?>);">
		<div class="overlay"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="text-content">
						<?php
						if ( isset($category) ) {
							echo '<h4>our services</h4><h2>'.$category->name.'</h2>';
						} else {
							echo '<h2>our services</h2>';
						}
						?>
						
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="products_list">
		<div class="container">
			<div class="row">

				<?php foreach ($services as $key => $service): ?>
				<div class="col-md-4">
					<div class="product_item">
						<a href="<?php echo base_url('service/detail/'.$service->id); ?>">
							<div class="thumb-container">
								<div class="bg-light embed-responsive embed-responsive-4by3 bg-vertical-center" style="background-image: url('<?php echo (isset($service->images) && count($service->images) > 0) ? base_url('assets/images/upload/service/'.$service->images[0]->image) : 'https://via.placeholder.com/348x261?text=No%20Image' ?>');" alt=""></div>
							</div>
						</a>
						<div class="down-content">
							<h4><a class="text-dark" href="<?php echo base_url('service/detail/'.$service->id); ?>"><?php echo $service->title; ?></a></h4>
							<p>
								<?php echo printTruncated(100, $service->description) ?>
							</p>
							<a href="<?php echo base_url('service/detail/'.$service->id); ?>" class="filled-button">Read Mode</a>
						</div>
					</div>
				</div>
				<?php endforeach ?>
				
			</div>
		</div>
	</div>
</div>
