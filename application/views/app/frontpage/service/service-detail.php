<div class="content-page">
	<!-- Page Content -->
	<div class="page-heading header-text" style="background-image:url(<?php echo base_url('assets/images/upload/service/'.$service->images[0]->image); ?>);">
		<div class="overlay"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="text-content">
						<h2>our services</h2>
						<h4><?php echo $service->title ?></h4>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="product-single">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="section-heading">
						<h2 class="text-capitalize">Apa yang kami lakukan?</h2>
					</div>
				</div>
				<div class="col-md-6">
					<div class="left-content">
						<?php echo $service->description ?>		
						<?php 
						if ($whatsapp_number) {
							echo '<a target="_blank" href="https://wa.me/'.$whatsapp_number.'?text=Halo%20Sahabat%20Sehat" class="btn filled-button">Hubungi kami</a>';
						}
						?>
					</div>
				</div>
				<div class="col-md-6">
					<?php if( isset($service->images) && count($service->images) > 0 ): ?>
					<div class="right-image">
						<div class="owl-product owl-carousel">
							<?php foreach ($service->images as $key => $service_image): ?>
							<img src="<?php echo base_url('assets/images/upload/service/'.$service_image->image); ?>" alt="">
							<?php endforeach; ?>
						</div>
					</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>  
</div>