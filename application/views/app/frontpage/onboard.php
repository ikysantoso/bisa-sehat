<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Bootstrap CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins">
    </head>
  <body>
    <div class="container">
        <div class="onboard-modal modal animated" id="myModal" style="background-color:#008080;">
            <div class="modal-dialog modal-fullscreen-sm-down">
                <div class="modal-content text-center" style="background-image: url('assets/images/onboarding/logo_BISA.png');background-position: 50% 0;background-size: cover;">
                    <div class="onboard-slider-container">
                        <div class="onboard-slide">
                            <button class="close">
                                <a class="close-label" href="<?php echo base_url( 'auth/login' ); ?>">Lewati</a>
                            </button>       
                            <div class="text-center onboard-text-big">
                                Selamat datang di BISA Sehat
                            </div>
                            <div class="onboard-media">
                                <img src="assets/images/onboarding/monitoring_app.png" alt="" style="height:200px; width:200px;">
                            </div>
                            <div class="onboard-text-small">
                                Pemantauan kesehatan online
                            </div>
                            <div class="onboard-media">
                                <img src="assets/images/onboarding/shopping.png" alt="" style="height:200px; width:200px;">
                            </div>
                            <div class="onboard-text-small mb-4">
                                Belanja kebutuhan Anda secara online
                            </div>
                        </div>
                        <div class="onboard-slide">       
                            <div class="text-left onboard-text-medium ml-4">
                                Pantau kesehatan Anda hanya dalam 1 halaman
                            </div>
                            <div class="onboard-media">
                                <img src="assets/images/onboarding/health_data.png" alt="" style="width:200px; margin-left:20px;">
                            </div>
                            <div class="text-right onboard-text-medium mt-5 mr-4">
                                Anda dapat mengisi data kesehatan dengan klik "Health"
                            </div>
                            <div class="onboard-media">
                                <img src="assets/images/onboarding/navbar_health.png" alt="" style="width: 200px; margin-right:20px; margin-bottom:40px;">
                            </div>
                        </div>
                        <div class="onboard-slide">       
                            <div class="text-left onboard-text-medium ml-4">
                                Cari produk yang anda inginkan pada kolom "Cari Produk"
                            </div>
                            <div class="onboard-media">
                                <img src="assets/images/onboarding/header.png" alt="" style="width:200px; margin-left:25px;">
                            </div>
                            <div class="d-flex justify-content-between">
                                <div class="onboard-media">
                                    <img src="assets/images/onboarding/category.png" alt="" style="width:200px; margin-left:25px;">
                                </div>
                                <div class="text-right onboard-text-small mt-4 mr-4">
                                     Anda juga bisa cari produk yang anda inginkan dengan klik kategori
                                </div>
                            </div>
                            <div class="d-flex justify-content-between">
                                <div class="text-left onboard-text-small mt-4 ml-4">
                                    <p>Temukan beragam produk yang sedang promosi pada bagian "Spesial Buat Anda"</p>
                                    <p>Klik "Beli Sekarang" apabila ingin segera membeli produk</p>
                                </div>
                                <div class="onboard-media">
                                    <img src="assets/images/onboarding/product.png" alt="" style="width:200px; height:200px; margin-right:20px;">
                                </div>
                            </div>
                            <div class="text-left onboard-text-small ml-4">
                                Lihat seluruh pesanan Anda dengan klik "Order"
                            </div>
                            <img src="assets/images/onboarding/navbar_order.png" alt="" style="width:200px; margin-left:25px; margin-bottom:40px;">
                        </div>
                        <div class="onboard-slide">       
                            <div class="text-center onboard-text-medium">
                                Masukan rekomendasi produk yang Anda inginkan dengan cara klik "Wishlist"
                            </div>
                            <div class="onboard-media">
                                <img src="assets/images/onboarding/navbar_wishlist.png" alt="" style="width: 200px;">
                            </div>
                            <div class="onboard-media">
                                <img src="assets/images/onboarding/hero.png" alt="" style="width: 200px; height:200px; margin-top:40px;">
                            </div>
                            <div class="onboard-text-medium mt-3 mb-5">
                                Perjalanan Anda menuju kehidupan yang lebih baik bersama BISA dimulai dari sekarang!
                            </div>
                            <a href="<?php echo base_url( 'auth/login' ); ?>" type="button" class="btn btn-success mb-5" style="background-color:#ffffff; color:#008080; font-weight: bold; font-family: 'Poppins'; font-size: 15px;">Mulai Sekarang</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <script>
        $('.onboard-modal .onboard-slider-container').slick({
            dots: true,
            infinite: false,
            adaptiveHeight: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            nextArrow: '<div class="slick-next">Lanjut</div>',
            prevArrow: '<div class="slick-prev">Kembali</div>'
        });
        var myModal = new bootstrap.Modal(document.getElementById('myModal'), {backdrop: 'static', keyboard: false})
        myModal.toggle()
   </script>
  </body>
</html>