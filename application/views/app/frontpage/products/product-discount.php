<div class="container">
    <div class="content-page">
        <!-- Page Content -->
        <div class="products_list mt-3">
            <h2 class="text-detail mb-3">Produk Kita</h2>
                <?php if($product_on_promotion->count() > 0 ): ?>
                <div class="row">
                    <?php foreach ($product_on_promotion as $key => $product): ?>
                    <div class="col-lg-3 col-md-4 col-6">
                        <div class="product_item">
                            <div style="border-radius: 10px; box-shadow: rgba(67, 71, 85, 0.27) 0px 0px 0.25em, rgba(90, 125, 188, 0.05) 0px 0.25em 1em;">
                                <a class="text-dark" href="<?php echo base_url('product/detail/'.$product->id); ?>">
                                    <img src="<?php echo (isset($product->images) && count($product->images) > 0) ? base_url('assets/images/upload/product/'.$product->images[0]->image) : 'https://via.placeholder.com/348x261?text=No%20Image' ?>" class="card-img-top" width="100%">
                                </a>
                                <div class="d-flex justify-content-between">
                                    <div class="d-flex flex-column" style="margin-left:10px">
                                        <h2><a class="product-text" href="<?php echo base_url('product/detail/'.$product->id); ?>"><?php echo $product->name; ?></a></h2>
                                        <strike class="before-discount"><?php echo(rupiah($product->price)); ?></strike>
                                        <h5 class="mb-2 after-discount"><?php echo(rupiah($product->discount)); ?></h5>
                                        <p><?php echo printTruncated(60, $product->description) ?></p>
                                    </div>
                                </div>
                                <div class="mt-2">
                                    <button data-id="<?php echo $product->id; ?>" class="btn filled-button buy-btn"><?php echo ($product->is_preorder == 'yes') ? 'Preorder' : 'Beli Sekarang'; ?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endforeach ?>
                </div>
            <?php else: ?>
                <div class="alert alert-info" style="margin-bottom:30%;">Produk tidak ditemukan</div>
            <?php endif; ?>
            <div class="alert alert-info" role="alert">
                Anda tidak menemukan produk yang anda inginkan silahkan klik. <a href="<?php echo base_url('member/order/wishorder_add'); ?>" class="alert-link text-primary">WISHLIST</a>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="modal-buy" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Pilih Gudang</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="media">
                    <img id="product-image" class="mr-3" style="width: 150px;height: auto" />
                    <div class="media-body d-flex align-self-stretch flex-column justify-content-between">
                        <div id="modal-product-container"></div>
                        <div class="d-flex flex-fill flex-column justify-content-center">
                            <select id="product-stock-list" class="form-control"></select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer d-flex justify-content-between">
			    <button id="btn-add-to-cart" disabled="disabled" type="button" class="btn btn-custom" style="background-color: #87d7bf" onclick="addProductToCart()"><img src="<?php echo base_url('assets/images/shop_cart.png'); ?>" style="height: 20px!important;width: auto!important;"><span style="color:#ffffff; margin-left:4px;">Masukan Keranjang</span></button>
                <button id="btn-payment" disabled="disabled" type="button" class="btn btn-primary" onclick="continuePayment()"><span>Lanjut Pembayaran</span></button>
            </div>
        </div>
    </div>
</div>