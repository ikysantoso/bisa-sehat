<div class="content-page">
	<!-- Page Content -->
	<div class="product-single mt-3">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="section-heading">
						<h2 class="text-capitalize">Keterangan tentang <?php echo $product->name ?></h2>
					</div>
				</div>
				<div class="col-md-6">
					<div class="left-content">
					    <?php if(($product->discount)): ?>
                            <strike class="mt-1 before-discount"><?php echo(rupiah($product->price)); ?></strike>
                            <h5 class="after-discount"><?php echo(rupiah($product->discount)); ?></h5>
                        <?php else: ?>
                            <h6 class="mt-1 after-discount"><?php echo(rupiah($product->price)); ?></h6>
                        <?php endif; ?>
						<div class="mt-3">
						    <?php echo $product->description ?>
						</div>

						<form id="form-add-to-cart">
							<input type="hidden" name="product_id" value="<?php echo $product->id ?>">
							<div class="form-group">
								<select id="product-stock-list" name="warehouse_id" required="required" class="form-control mr-3 mb-3">
									<option value="0" disabled="disabled" selected="selected">--Pilih Gudang--</option>
									<?php

									foreach ($product->stocks as $key => $stock) {
										$warehouse_id = $stock->warehouse->id;
										$warehouse_name = $stock->warehouse->name;

										$shipping_collection = array();
										$matched = "false";

										if ($stock->warehouse->shipping) {
											foreach(json_decode($stock->warehouse->shipping) as $shipping) {
												array_push($shipping_collection, $shipping);
											}

											if ($shipping_collection && in_array('Close', $shipping_collection)) {
												$matched = "true";
											}

											if ($matched === "false") {
												$stock_info = ($product->is_preorder == 'no') ? '('.$stock->last_stock.' stok)' : '';
												$stock_data = ($product->is_preorder == 'no') ? $stock->last_stock : '';
												echo '<option data-stock="'.$stock_data.'" value="'.$warehouse_id.'">'.$warehouse_name.' '.$stock_info.'</option>';
											}
										}
									}

									?>
								</select>
							</div>
							<div class="d-flex justify-content-between align-items-center">
								<button id="btn-add-to-cart" data-id="<?php echo $product->id; ?>" type="button" class="btn btn-custom" style="background-color: #87d7bf;" onclick="addProductToCart()"><img src="<?php echo base_url('assets/images/shop_cart.png'); ?>" style="height: 20px!important;width: auto!important;"><span style="color:#ffffff; margin-left:4px;">Masukan Keranjang</span></button>
								<button type="submit" data-button="buy" class="submit-btn btn btn-custom buy-now-btn" style="background-color: #F0FFF0; color: #008080; font-weight:bold;"><span>Beli Sekarang</span></button>
							</div>

						</form>

					</div>
				</div>
				<div class="col-md-6">
					<?php if( isset($product->images) && count($product->images) > 0 ): ?>
					<div class="right-image">
						<div class="owl-product owl-carousel">
							<?php foreach ($product->images as $key => $product_image): ?>
							<img src="<?php echo base_url('assets/images/upload/product/'.$product_image->image); ?>" alt="">
							<?php endforeach; ?>
						</div>
						<div style="margin-top: 20px; text-align: center;">
							<h5 style="color: #002855; font-family: arial;"><< Geser untuk melihat lebih >></p>
						</div>
					</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</div>