<div class="container">
    <div class="content-page">
	    <div class="products_list mt-3">
	        <h2 class="text-headline mb-2">Semua Kategori</h2>
			<div class="row">
				<?php foreach ($categories as $key => $category): ?>
				<div class="col-md-2 col-4">
					<div class="product_item">
						<a class="text-dark" href="<?php echo base_url('product/category/'.$category->id); ?>">
						    <div class="other-category">
                               <img src="<?php echo ($category->image) ? base_url('assets/images/upload/product_category/'.$category->image) : 'https://via.placeholder.com/348x261?text=No%20Image' ?>" width="100%">
                            </div>
						</a>
						<h4 class="text-center" style="font-size:20px;"><a class="text-dark" href="<?php echo base_url('product/category/'.$category->id); ?>"><?php echo $category->name; ?></a></h4>
					</div>
				</div>
				<?php endforeach ?>
			</div>
		</div>
	</div>
</div>