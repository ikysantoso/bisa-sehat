<div class="medical-record container">
    <h1 class="mt-3" style="font-weight: bold; font-size: 30px">Terms & Conditions</h1>
    
    <ul class="mt-4 ml-4" style="list-style-type: upper-alpha;">
        <li style="font-weight: bold; font-size: 20px">Definisi</li>
        <ol class="ml-4" style="font-size: 15px;">
            <li>App BISA ini TIDAK dipakai untuk mendiagnosis penyakit. App BISA ini hanya sebagai alat screening dan MONITOR KESEHATAN Anda saja.</li>
            <li>Dasar Perhitungan di App BISA ini menggunakan sumber dari Kementerian Kesehatan, WHO, dan Sumber-sumber kesehatan yang lain.</li>
        </ol>
        <li class="mt-3" style="font-weight: bold; font-size: 20px">Kekayaan Intelektual</li>
        <ol class="ml-4" style="font-size: 15px;">
            <li>Semua isi pada aplikasi ini dilindungi oleh hak cipta dan hak kekayaan intelektual hukum Republik Indonesia.</li>
        </ol>
        <li class="mt-3" style="font-weight: bold; font-size: 20px">Keamanan Pengguna</li>
        <ol class="ml-4" style="font-size: 15px;">
            <li>Data Anda aman dan dilindungi UU ITE Kementerian Hukum Republik Indonesia.</li>
        </ol>
    </ul>
</div>