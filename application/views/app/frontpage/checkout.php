<div class="content-page">
    <?php
        $primary_address = ($primary_address) ? $primary_address : $customer_addresses[0];

        if ($shipping_address) {
            $primary_address = $shipping_address;
        }

        $jabodetabek_collection = "/(Jakarta|Bogor|Depok|Tangerang|Bekasi)/";

        $matched_address = "false";

        if (preg_match($jabodetabek_collection, $primary_address->kabupaten->city_name) == 1) {
            $matched_address = "true";
        }
    ?>
    
<div class="container">
<h2 class="text-headline mt-3">Checkout</h2>
    <section id="checkout-section" data-cart="<?php echo $cart_ids; ?>" class="py-5">
            <div class="row">
                <div class="col-lg-6 mb-lg-0">
                    <div class="border-0 card">
                        <div class="card-body">
                            <?php
                            $primary_address = ($primary_address) ? $primary_address : $customer_addresses[0];
                            ?>
                            <h5 class="font-weight-bold"><i class="fas fa-map-marker-alt"></i> ALAMAT PENGIRIMAN</h5>
                            <hr/>
                            <div id="selected-address" data-city="<?php echo $primary_address->kabupaten->city_id; ?>" data-id="<?php echo $primary_address->id; ?>" class="card">
                                <div class="bg-transparent card-header font-weight-bold">
                                    <?php $phone = ($primary_address->phone) ? ' ('.$primary_address->phone.')' : ''; ?>
                                    <?php echo $primary_address->name .' - '. $primary_address->receiver_name .$phone; ?>
                                </div>
                                <div class="card-body">
                                    <div class="address">
                                        <?php
                                        echo $primary_address->address."<br/>";
                                        echo $primary_address->village_text."<br/>";
                                        echo $primary_address->kecamatan->subdistrict_name." - ";
                                        echo $primary_address->kabupaten->city_name." - ";
                                        echo $primary_address->provinsi->province." - ";
                                        echo $primary_address->zipcode;
                                        ?>
                                    </div>
                                </div>
                                <div class="text-right bg-transparent card-footer">
                                    <a href="#" class="text-accent font-weight-bold text-decoration-none" data-toggle="modal" data-target="#change-address-modal">Ganti Pilihan Lain</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="border-0 card h-100">
                        <div class="p-0 card-body">
                            <h5 class="pb-3 my-3 font-weight-bold border-bottom"><i class="fas fa-shopping-cart"></i> INFO PESANAN</h5>
                            <?php foreach ($carts as $key => $_carts): ?>
                            <div class="mb-5 border cart-item">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th class="text-white bg-info" colspan="2"><i class="fas fa-warehouse"></i> <?php echo $_carts[0]->warehouse->name; ?></th>
                                            </tr>
                                            <tr>
                                                <th class="px-3 text-left border-top-0">Nama Produk</th>
                                                <th class="px-3 text-right border-top-0">Total</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $sub_total = 0;
                                            foreach ($_carts as $cart): ?>
                                            <tr>
                                                <td class="px-3">
                                                    <?php
                                                        echo $cart->product->name." ".$cart->quantity ?>
                                                    <?php
                                                    if ($cart->preorder_time($cart->warehouse_id)) {
                                                        echo '<br/><span class="badge badge-pill badge-secondary">'.$cart->preorder_time($cart->warehouse_id).'</span>';
                                                    }
                                                    ?>
                                                </td>
                                                <td class="px-3 text-right align-middle font-weight-bold"><?php echo
                                                    isset($cart->product->discount) ?
                                                    rupiah($cart->product->discount * $cart->quantity) : rupiah($cart->product->price * $cart->quantity); ?></td>
                                            </tr>
                                            <?php
                                            if (isset($cart->product->discount)) {
                                                $sub_total += $cart->product->discount * $cart->quantity;
                                            } else {
                                                $sub_total += $cart->product->price * $cart->quantity;
                                            }
                                            endforeach; ?>
                                            <tr>
                                                <td class="px-3 text-right font-weight-bold">SUBTOTAL PRODUK</td>
                                                <td class="px-3 text-right font-weight-bold sub-total" data-subtotal="<?php echo $sub_total; ?>"><?php echo rupiah($sub_total); ?></td>
                                            </tr>
                                            <tr>
                                                <td class="px-3 text-right font-weight-bold">ONGKOS KIRIM</td>
                                                <td class="text-right font-weight-bold px-3 shipping-fee">-</td>
                                            </tr>
                                            <tr class="weight-total" data-weight-total="<?php echo $cart->quantity * $cart->product->weight; ?>">
                                                <td class="px-3 text-right font-weight-bold">Berat Total (<?php echo $cart->quantity; ?> x <?php echo $cart->product->weight; ?>)</td>
                                                <td class="px-3 text-right font-weight-bold"><?php echo $cart->quantity * $cart->product->weight; ?> gram</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div>
                                    <div style="position: relative;" class="p-3 pb-0">
                                        <h5 class="mb-3 font-weight-bold"><i class="fas fa-shipping-fast"></i> METODE PENGIRIMAN</h5>
                                        <div class="form-group">
                                            <select data-cart="<?php echo $_carts->pluck('id')->implode(','); ?>" class="form-control shipping-fee-list"></select>
                                            <input type="hidden" name="courier-code"/>
                                            <input type="hidden" name="courier-service"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php endforeach; ?>

                            <div class="table-responsive">
                                <table class="table">
                                    <tbody>
                                        <tr>
                                            <td class="text-right font-weight-bold">TOTAL PRODUK</td>
                                            <td class="text-right font-weight-bold" id="product-total">0</td>
                                        </tr>
                                        <tr>
                                            <td class="text-right font-weight-bold">TOTAL ONGKOS KIRIM</td>
                                            <td class="text-right font-weight-bold" id="shipping-fee-total">0</td>
                                        </tr>
                                        <tr>
                                            <td class="text-right font-weight-bold">DISCOUNT</td>
                                            <td class="text-right font-weight-bold" id="voucher-user">0</td>
                                        </tr>
                                        <tr>
                                            <td class="text-right font-weight-bold">GRAND TOTAL</td>
                                            <td class="text-right font-weight-bold h4 text-action" id="grand-total">0</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="mt-4">
                                <button id="btn-checkout" class="py-3 btn filled-button btn-block" onclick="checkoutOrder()">PLACE ORDER</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
    </div>
</div>

<div class="modal" id="change-address-modal" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Ganti Alamat</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="ml-0 accordion" id="address-list">

                    <?php foreach ($customer_addresses as $address):
                        $_phone = ($address->phone) ? ' ('.$address->phone.')' : '';
                    ?>
                    <div class="card">
                        <div class="card-header d-flex justify-content-start align-items-center" id="heading<?php echo $address->id ?>">
                            <div class="form-check">
                                <input data-city="<?php echo $address->kabupaten->city_id; ?>" class="form-check-input position-static" type="radio" name="address" id="address<?php echo $address->id ?>" value="<?php echo $address->id ?>">
                            </div>
                            <h2 class="mb-0">
                                <button class="text-left btn btn-link btn-block btn-check-address" type="button" data-toggle="collapse" data-target="#collapse<?php echo $address->id ?>" aria-expanded="true" aria-controls="collapse<?php echo $address->id ?>">
                                <?php echo $address->name.' - '.$address->receiver_name .$_phone ?>
                                </button>
                            </h2>
                        </div>
                        <div id="collapse<?php echo $address->id ?>" class="collapse <?php //show ?>" aria-labelledby="heading<?php echo $address->id ?>" data-parent="#address-list">
                            <div class="card-body">
                                <?php
                                    echo $address->address."<br/>";
                                    echo $address->village_text."<br/>";
                                    echo $address->kecamatan->subdistrict_name." - ";
                                    echo $address->kabupaten->city_name." - ";
                                    echo $address->provinsi->province." - ";
                                    echo $address->zipcode;
                                ?>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>
            <h6 data-toggle="modal" data-target="#modal-address"><button class="text-left btn btn-link ml-2" style="color:#A9A9A9;">Tambah Alamat</button></h6>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" id="change-address-btn" class="btn btn-primary" onclick="changeAddress()">Pilih alamat</button>
            </div>
        </div>
    </div>
</div>

<div style="z-index: 9999" class="modal" id="modal-bank" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Pilih Bank</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul class="list-group">
                    <li class="list-group-item">
                        <div class="form-check d-flex">
                            <input class="form-check-input" type="radio" name="payment-method" id="bisapay" value="bisapay">
                            <label class="form-check-label flex-fill" for="bisapay">
                                <?php echo '<div class="d-flex justify-content-between"><span>Bisapay</span> <b>'.rupiah( ($is_bisapay_active) ? $is_bisapay_active->saldo() : 0).'</b></div>'; ?>
                            </label>
                        </div>
                    </li>
                    <?php foreach ($banks as $bank) {
                        echo
                        '<li class="list-group-item">
                            <div class="form-check d-flex">
                                <input data-bank='."'".json_encode( $bank )."'".' class="form-check-input" type="radio" name="payment-method" id="bank_'.$bank['id'].'" value="'.$bank['id'].'">
                                <label class="form-check-label flex-fill" for="bank_'.$bank['id'].'">
                                '.$bank['name'].' - '.$bank['account_name'].' ('.$bank['account_number'].')
                                </label>
                            </div>
                        </li>';
                    } ?>
                </ul>
            </div>
        </div>
    </div>
</div>

<div id="modal-address" class="modal" tabindex="-1" style="z-index: 9999999">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <form id="form-address">
                <input type="hidden" name="id"/>
                <div class="modal-header">
                    <h5 class="modal-title">Tambah Alamat</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Nama Alamat</label>
                                <input required="required" class="form-control" type="text" placeholder="Contoh: Alamat Kantor/Alamat Rumah" name="name"/>
                            </div>
                            <div class="form-group">
                                <label>Nama Penerima</label>
                                <input class="form-control" type="text" placeholder="Nama Penerima" name="receiver_name" required="required" />
                            </div>
                            <div class="form-group">
                                <label>Phone</label>
                                <input class="form-control" type="text" placeholder="Nomor telepon" name="phone" required="required" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h5 class="mb-3">Alamat Tempat Tinggal</h5>
                            <div class="form-group">
                                <label>Provinsi</label>
                                <input required="required" type="text" data-url="<?php echo base_url('api/wilayah/province'); ?>" data-load-once="true" name="province" />
                            </div>
                            <div class="form-group">
                                <label>Kabupaten/Kota</label>
                                <input required="required" type="text" class="form-control" disabled="disabled" name="district" />
                            </div>
                            <div class="form-group">
                                <label>Kecamatan</label>
                                <input required="required" type="text" class="form-control" disabled="disabled" name="sub-district" />
                            </div>
                            <div class="form-group">
                                <label>Kelurahan/Desa</label>
                                <input required="required" type="text" class="form-control" disabled="disabled" name="village" />
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Kodepos</label>
                                        <input required="required" type="number" min="1" max="99999" minlength="5" maxlength="5" class="form-control" disabled="disabled" name="zipcode" />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Alamat Utama?</label><br/>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input value="yes" required="required" type="radio" id="customRadioInline1" name="is_primary_address" class="custom-control-input">
                                            <label class="custom-control-label" for="customRadioInline1">Ya</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input value="no" required="required" type="radio" id="customRadioInline2" name="is_primary_address" class="custom-control-input">
                                            <label class="custom-control-label" for="customRadioInline2">Tidak</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Alamat</label>
                                <textarea style="min-height: 103px;" required="required" class="form-control" placeholder="Nama Jalan/RT/RW" disabled="disabled" name="address"></textarea>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript"
        src="https://app.midtrans.com/snap/snap.js"
        data-client-key="Mid-client-pOLsTz-6lTCR76tT"></script>