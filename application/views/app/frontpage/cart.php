<div class="content-page">
    <!-- Page Content -->
    <section class="py-5">
        <div class="container">
            <?php if($carts->count() > 0): ?>

            <div class="table-responsive">
                <table id="table-cart" class="table table-bordered">
                    <thead>
                        <tr>
                            <th valign="center" class="text-center"><input type="checkbox" name="check-all"/></th>
                            <th class="text-left" colspan="2">PRODUCT</th>
                            <th class="text-right">PRICE</th>
                            <th class="text-center" width="180px">QUANTITY</th>
                            <th class="text-right">TOTAL</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($carts as $key => $cart): ?>
                        <tr>
                            <td class="text-center align-middle">
                                <input type="checkbox" value="<?php echo $cart->id; ?>" class="product-check">
                            </td>
                            <td class="align-middle" width="60px">
                                <img src="<?php echo (isset($cart->product->images) && count($cart->product->images) > 0) ? base_url('assets/images/upload/product/'.$cart->product->images[0]->image) : 'https://via.placeholder.com/60x60?text=No%20Image'; ?>" alt="" width="60">
                            </td>
                            <td valign="center">
                                <div class="d-flex justify-content-between align-items-center">
                                    <strong>#<?php echo $cart->product->id; ?></strong>
                                    <?php
                                    if ( $cart->preorder_time( $cart->warehouse_id ) ) {
                                        echo '<span class="badge badge-pill badge-secondary">'.$cart->preorder_time( $cart->warehouse_id ).'</span>';
                                    }
                                    ?>
                                </div>
                                <?php echo $cart->product->name; ?>
                                <br/>
                                <small class="text-muted"><?php echo printTruncated(50, $cart->product->description); ?></small>
                                <div>
                                    <span class="badge badge-pill badge-info"><?php echo $cart->warehouse->name; ?></span>
                                </div>
                            </td>
                            <td class="text-right align-middle price" id="price-<?php echo $cart->id; ?>" data-price="<?php echo isset($cart->product->discount) ? $cart->product->discount : $cart->product->price; ?>"><?php echo rupiah(isset($cart->product->discount) ? $cart->product->discount : $cart->product->price); ?></td>
                            <td class="align-middle">
                                <div class="input-group">
                                    <div class="input-group-prepend quantity-update-btn decrease-value">
                                        <span class="input-group-text">-</span>
                                    </div>
                                    <input readonly="readonly" type="text" class="form-control text-center quantity bg-white" id="quantity-<?php echo $cart->id; ?>" value="<?php echo $cart->quantity; ?>">
                                    <div class="input-group-append quantity-update-btn increase-value">
                                        <span class="input-group-text">+</span>
                                    </div>
                                </div>
                            </td>
                            <td id="sub_total-<?php echo $cart->id; ?>" class="text-right font-weight-bold text-action align-middle"><?php echo rupiah(isset($cart->product->discount) ? $cart->quantity * $cart->product->discount : $cart->quantity * $cart->product->price ); ?></td>
                        </tr>
                        <?php endforeach; ?>

                    </tbody>
                    <tfoot>
                        <tr>
                            <th class="text-center" colspan="5">Sub Total</th>
                            <th id="sub-total" class="text-right">0</th>
                        </tr>
                    <!--     <tr>
                            <td colspan="6" class="text-lg-right">
                                <a href="#" class="btn btn-light py-3 px-4">UPDATE CART</a>
                            </td>
                        </tr> -->
                    </tfoot>
                </table>
            </div>
            <div class="mt-4">
                <div class="text-lg-right">
                    <button disabled="disabled" type="button" id="checkout-btn" class="btn filled-button buy-button py-3 px-4">PROCEED TO CHECKOUT</button>
                </div>
            </div>
            <?php else: ?>
                <div class="alert alert-warning" style="margin-bottom:130px;">Keranjang belanja kosong, lihat <a href="<?php echo base_url('product/all'); ?>">produk</a></div>
            <?php endif; ?>
        </div>
    </section>
</div>