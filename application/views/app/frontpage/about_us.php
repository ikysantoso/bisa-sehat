<div class="container">
    <h1 class="mt-3" style="font-weight: bold; font-size: 30px">Tentang Kami</h1>
    
    <ul class="mt-4 ml-4" style="list-style-type: upper-alpha;">
        <li style="font-weight: bold; font-size: 20px">Legalitas Perusahaan</li>
        <ol class="ml-4" style="font-size: 15px;">
            <li>App BISA merupakan aplikasi yang dikelola oleh PT SAHABAT MEDIKA TEKNOLOGI.</li>
            <li>App BISA telah terdaftar di KEMKOMINFO dan memiliki Perizinan Berusaha dan Tanda Daftar Penyelenggara Sistem Elektronikk (<a href="<?php echo base_url('About_us/tdpse'); ?>">TD PSE</a>) Domestik PB-UMKU: 161221002804400060001. Nomor TDPSE : 004724.01/DJAI.PSE/07/2022. </li>
        </ol>
        <li class="mt-3" style="font-weight: bold; font-size: 20px">Perangkat Lunak</li>
        <ol class="ml-4" style="font-size: 15px;">
            <li>App BISA betujuan untuk memberikan pelayanan di bidang kesehatan khususnya rekam medis, sehingga pengguna dapat melihat catatan kesehatan dari waktu ke waktu dan mengetahui status kesehatan pengguna. Dengan mengetahui status kesehatan, pengguna dapat mengetahui jika ada resiko bahaya kesehatan.</li>
            <li>App BISA merupakan perangkat lunak yang dikembangkan oleh Tim Developer App BISA dan juga Pihak Ketiga.</li>
        </ol>
        <li class="mt-3" style="font-weight: bold; font-size: 20px">Sumber Referensi</li>
        <ol class="ml-4" style="font-size: 15px;">
            <li>App BISA menggunakan perhitungan untuk rumus kesehatan berdasarkan Kementerian Kesehatan, WHO, dan Sumber-sumber kesehatan yang lain.</li>
        </ol>
    </ul>
</div>