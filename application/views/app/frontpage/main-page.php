<div class="container" style="margin-top: 50px">
  <div class="text-left">
    <?php if( $this->ion_auth->logged_in()): ?>
    <div class="text-greeting">
      <h4>Hai, <?php echo $this->ion_auth->user()->row()->username ?></h4>
    </div>
    <?php endif; ?>
    <img class="embed-responsive embed-responsive-21by9 bg-vertical-center" src="<?php echo base_url('assets/images/Banner.png'); ?>" style="margin-top: 25px; border-radius: 10px; object-fit: fill;" alt="">
    <div class="text-content">
      <h5>Selamat datang di BISA SEHAT, anda dapat memilih halaman yang anda inginkan di bawah ini...</h5> 
    </div>
    <div class="card-box mb-5">
      <div class="health">
        <a href="<?php echo base_url('home'); ?>">
          <h4><img src="assets/images/health_white.png" style="height:70px;" alt="">
            <div class="d-flex flex-column" style="margin-left:20px;">
              <div style="font-size: 28px; font-family: 'Poppins';">Health</div>
              <div style="font-size: 15px; font-family: 'Poppins';">Monitoring kesehatan anda</div>
            </div>
          </h4>
        </a>
      </div>
      <div class="ecommerce">
        <a>
          <h4><img src="assets/images/ecommerce_white.png" style="height:70px;" alt="">
            <div class="d-flex flex-column" style="margin-left:20px;">
              <div style="font-size: 28px; font-family: 'Poppins';">Ecommerce</div>
              <div style="font-size: 15px; font-family: 'Poppins';">Belanja kebutuhan anda</div>
            </div>
          </h4>
        </a>
      </div>
    </div>
  </div>
  <!-- <nav class="sosmed-mobile navbar navbar-expand d-md-none d-lg-none d-xl-none">
    <ul class="navbar-nav nav-justified w-100 mb-1">
      <li class="nav-item">
        <a href="https://www.facebook.com/aplikasibisa.id" target="_blank" title="Facebook"><img src="<?php echo base_url('assets/images/social_media/logo_fb.png'); ?>" alt=""></a>    
      </li>
      <li class="nav-item">
        <a href="https://www.instagram.com/aplikasibisa.id/" target="_blank" title="Instagram"><img src="<?php echo base_url('assets/images/social_media/logo_ig.png'); ?>" alt=""></a>
      </li>
      <li class="nav-item">
        <a href="https://youtube.com/channel/UC1zjZyzNgWNUc99szQ6HL0A" target="_blank" title="Youtube"><img src="<?php echo base_url('assets/images/social_media/logo_youtube.png'); ?>" alt=""></a>
      </li>
      <li class="nav-item">
        <a href="https://wa.me/6281320005015" target="_blank" title="WhatsApp"><img src="<?php echo base_url('assets/images/social_media/logo_wa.png'); ?>" alt=""></i></a>
      </li>
      <li class="nav-item">
        <a href="https://vt.tiktok.com/ZSef1RXsc" target="_blank" title="Twitter"><img src="<?php echo base_url('assets/images/social_media/logo_twitter.png'); ?>" alt=""></a>
      </li>
    </ul>
  </nav> -->
</div>

