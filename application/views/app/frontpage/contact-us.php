<?php
$facebook = '';
$whatsapp_number = '';
$instagram = '';
$map = '';
$our_office = '';
foreach ($socmeds as $key => $socmed) {
    if ( $socmed->meta_key == 'facebook' ) {
        $facebook = ($socmed->meta_value) ? '<a href="'.$socmed->meta_value.'" type="button" style="background-color: #4267B2;color: #fff" class="btn btn-default mx-1"><i class="fab fa-facebook-f"></i></a>' : '';
    } else if ( $socmed->meta_key == 'whatsapp_number' ) {
        $whatsapp_number = ($socmed->meta_value) ? '<a href="https://wa.me/'.$socmed->meta_value.'?text=Halo%20Sahabat%20Sehat" type="button" style="background-color: #4FCE5D;color: #fff" class="btn btn-default mx-1"><i class="fab fa-whatsapp"></i></a>' : '';
    } else if ( $socmed->meta_key == 'instagram' ) {
        $instagram = ($socmed->meta_value) ? '<a href="'.$socmed->meta_value.'" type="button" style="background: radial-gradient(circle farthest-corner at 35% 90%, #fec564, transparent 50%), radial-gradient(circle farthest-corner at 0 140%, #fec564, transparent 50%), radial-gradient(ellipse farthest-corner at 0 -25%, #5258cf, transparent 50%), radial-gradient(ellipse farthest-corner at 20% -50%, #5258cf, transparent 50%), radial-gradient(ellipse farthest-corner at 100% 0, #893dc2, transparent 50%), radial-gradient(ellipse farthest-corner at 60% -20%, #893dc2, transparent 50%), radial-gradient(ellipse farthest-corner at 100% 100%, #d9317a, transparent), linear-gradient(#6559ca, #bc318f 30%, #e33f5f 50%, #f77638 70%, #fec66d 100%);color: #fff" class="btn btn-default border-0  mx-1"><i class="fab fa-instagram"></i></a>' : '';
    } else if ( $socmed->meta_key == 'map' ) {
        $map = ($socmed->meta_value) ? $socmed->meta_value : '';
    } else if ( $socmed->meta_key == 'our_office' ) {
        $our_office = ($socmed->meta_value) ? $socmed->meta_value : '';
    }
}; 
?>

<div class="content-page">
    <!-- Page Content -->
    <div class="page-heading header-text" style="background-image:url('https://sahabatsehat.co.id/uploads/hero-contact.jpg');">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="text-content">
                        <h4>contact us</h4>
                        <h2>let’s get in touch</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="find-us">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-heading">
                        <h2>Our Location on Maps</h2>
                    </div>
                </div>
                <div class="col-md-8">
                    <!-- How to change your own map point
                        1. Go to Google Maps
                        2. Click on your location point
                        3. Click "Share" and choose "Embed map" tab
                        4. Copy only URL and paste it within the src="" field below
                        -->
                    <div id="map">
                        <?php echo $map; ?>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="left-content">
                        <h4>About our office</h4>
                        <p class="pb-2 mb-3"><?php echo $our_office; ?></p>
                        <div class="text-right">
                            <?php echo $facebook.$whatsapp_number.$instagram; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="send-message">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-heading">
                        <h2>Send us a Message</h2>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="hidden" role="alert"></div>
                    <div class="contact-form">
                        <form id="form-contact">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 mb-3">
                                    <fieldset>
                                        <input name="name" type="text" class="form-control mb-2" id="name" placeholder="Full Name" required="">
                                    </fieldset>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 mb-3">
                                    <fieldset>
                                        <input name="email" type="email" class="form-control mb-2" id="email" placeholder="E-Mail Address" required="">
                                    </fieldset>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 mb-3">
                                    <fieldset>
                                        <input name="subject" type="text" class="form-control mb-2" id="subject" placeholder="Subject" required="">
                                    </fieldset>
                                </div>
                                <div class="col-lg-12 mb-3">
                                    <fieldset>
                                        <textarea name="message" rows="6" class="form-control mb-2" id="message" placeholder="Your Message" required=""></textarea>
                                    </fieldset>
                                </div>
                                <div class="col-lg-12 mb-3">
                                    <fieldset>
                                        <button type="submit" id="form-submit" class="filled-button send_email">Send Message</button>
                                    </fieldset>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>