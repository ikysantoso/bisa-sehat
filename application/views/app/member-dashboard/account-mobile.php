<div class="container">
   <div class="row">
      <div class="btn-group-vertical" role="group" aria-label="Basic example">
         <div class="mt-4">
            <a href="<?php echo base_url('member/setting') ?>" <?php echo ($page == 'member-setting') ? 'active' : ''; ?>>
               <img src="<?php echo base_url('assets/images/favicon/settings.png'); ?>" style="height: 30px; width: auto;">
               <span style="font-size: 20px; color: #000000;">Setting</span>
            </a>
         </div>
         <div class="mt-4">
            <a href="<?php echo base_url('auth/logout') ?>">
               <img src="<?php echo base_url('assets/images/favicon/logout.png'); ?>" style="height: 30px; width: auto;">
               <span style="font-size: 20px; color: #000000;">Logout</span>
            </a>
         </div>
      </div>
   </div>
</div>
