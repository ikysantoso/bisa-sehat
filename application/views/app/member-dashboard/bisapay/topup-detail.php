<div class="content-page">
    <!-- Page Content -->
    <div class="container">
        <div class="row">
            <?php include VIEWPATH.'/app/member-dashboard/dashboard-nav.php' ?>
            <div class="col-12">
            	<div class="d-flex justify-content-between align-items-center">
					<h3 class="my-4">Topup Detail</h3>
					<div>
						<a onclick="history.go(-1);" class="d-none d-lg-block d-md-block btn btn-info btn-sm px-1" >Back</a>
					</div>
            	</div>
            </div>
            <div class="col-md-6">
            	<div class="border p-3">
	            	<div class="d-flex justify-content-between align-items-center mb-3">
	            		<div>Tanggal Topup</div>
	            		<div class="border-bottom flex-fill mx-3"></div>
	            		<div><?php echo $topup->created_at->format('d-m-Y H:i:s'); ?></div>
	            	</div>
	            	<div class="d-flex justify-content-between align-items-center mb-3">
	            		<div>Status</div>
	            		<div class="border-bottom flex-fill mx-3"></div>
	            		<div><?php echo $topup->log->status; ?></div>
	            	</div>
	            	<div class="d-flex justify-content-between align-items-center mb-3">
	            		<div>Nominal</div>
	            		<div class="border-bottom flex-fill mx-3"></div>
	            		<div><?php echo rupiah($topup->log->nominal); ?></div>
	            	</div>
	            	<div class="d-flex justify-content-between align-items-center mb-3">
	            		<div>Kode Unik</div>
	            		<div class="border-bottom flex-fill mx-3"></div>
	            		<div><?php echo $topup->unique_number; ?></div>
	            	</div>
            	</div>
            </div>

            <div class="col-md-6">
            	<div class="border p-3">
	            	<div class="d-flex justify-content-between align-items-center mb-3">
	            		<div>Rekening Tujuan</div>
	            		<div class="border-bottom flex-fill mx-3"></div>
	            		<div><?php echo $topup->bank->name; ?></div>
	            	</div>
	            	<div class="d-flex justify-content-between align-items-center mb-3">
	            		<div>No. Rekening</div>
	            		<div class="border-bottom flex-fill mx-3"></div>
	            		<div><?php echo $topup->bank->account_number; ?></div>
	            	</div>
	            	<div class="d-flex justify-content-between align-items-center mb-3">
	            		<div>Atas Nama</div>
	            		<div class="border-bottom flex-fill mx-3"></div>
	            		<div><?php echo $topup->bank->account_name; ?></div>
	            	</div>
	            	<div class="d-flex justify-content-between align-items-center mb-3">
	            		<div>Total</div>
	            		<div class="border-bottom flex-fill mx-3"></div>
	            		<div><?php echo rupiah( $topup->unique_number + $topup->log->nominal ); ?></div>
	            	</div>
            	</div>
            </div>
        </div>

		<?php if($topup->log->payment_image == null): ?>
			<div style="margin: 20px 0 0 0; width: 100%;">
				<button type="button" class="btn payment-confirmation-btn" style="background-color:#008080;"><span style="font-weight: bold; color: #ffffff">Kirim Bukti Pembayaran</span></button>
			</div>
		<?php else: ?>
			<div style="margin: 20px 0 0 0; width: 100%;">
				<h4 style="font-weight: bold; margin-bottom: 10px; margin-top: 30px;">Bukti Pembayaran</h4>
				<img class="payment-confirmation-img" src="<?php echo base_url('assets/images/transactions/bisapay/'.$topup->log->payment_image); ?>"/>
				<?php if($topup->log->status == "waiting"): ?>
					<div style="margin-top: 20px;">
						<button type="button" class="btn reconfirm-payment-btn" style="background-color:#008080;">
							<span style="font-weight: bold; color: #ffffff">Kirim Ulang</span>
						</button>
					</div>
				<?php endif; ?>
			</div>
		<?php endif; ?>

		<div class="modal" id="modal-payment-confirmation" tabindex="-1">
			<div class="modal-dialog modal-dialog-centered">
					<div class="modal-content">
							<form id="form-payment-confirmation">
									<div class="modal-header">
										<h5 class="modal-title">Kirim Bukti Pembayaran</h5>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<div class="modal-body">
										<input type="text" id="topup_log_id" value="<?php echo $topup->log->id; ?>" class="form-control" hidden>
										<input type="text" id="topup_nominal" value="<?php echo $topup->log->nominal; ?>" class="form-control" hidden>
										<input type="text" id="exist_image_name" value="<?php echo $topup->log->payment_image; ?>" class="form-control" hidden>
										<b>Silahkan masukan file</b>
										<input type="file" name="payment-confirmation-file" id="payment-confirmation-file">
									</div>
									<div class="modal-footer">
											<button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
											<button type="submit" class="btn btn-primary">Kirim</button>
									</div>
							</form>
					</div>
			</div>
	</div>
    </div>
</div>