<div class="content-page">
    <div class="health-check container">
        <div class="row">
            <div class="col-12">
            <div class="d-flex justify-content-between align-items-center">
	                <h3 class="ml-3">Bisapay</h3>
                    <?php if ( $bisapay ) { ?>
    	            <div class="btn-group" role="group" aria-label="Basic example">
					  	<button type="button" class="btn btn-info" data-toggle="modal" data-target="#modal-bisapay">Setor</button>
					  	<button type="button" class="btn btn-light" id="withdraw-btn">Tarik</button>
					</div>
                    <?php } ?>
            	</div>
                <?php
                if (!$bisapay) {
                	echo '<div class="alert alert-info">Anda belum mengaktifkan Bisapay, <a class="bisapay-activate-btn" href="#">Aktifkan sekarang?</a></div>';
                } else {
                	?>
                    <div class="text-center mb-3 pb-3">
                		<p class="mb-3">Saldo Bisapay</p>
                        <?php $saldo = $bisapay->saldo(); ?>
                		<h3 id="saldo" class="p-3 bg-light text-dark d-inline-block rounded"><?php echo rupiah($saldo); ?></h3>
                	</div>
                
                    <div class="container bisapay-logs-container">
                        <ul class="nav nav-pills" style="border-radius: 10px;">
                            <div class="bisapay-logs-nav-pills">
                                <li class="nav-item">
                                    <a class="nav-link nav-link-color active" id="done-transaction" data-toggle="tab"><b><h6>Transaksi Selesai</h6></b></a>
                                </li>
                                <li class="nav-item ml-4">
                                    <a class="nav-link nav-link-color" id="inprogress-transaction" data-toggle="tab"><b><h6>Transaksi Diproses</h6></b></a>
                                </li>
                            </div>
                        </ul>
                        <table class="table table-striped table-sm bisapay-logs-table">
                            <tbody id="bisapay-logs-table-body"></tbody>
                        </table>
                    </div>
                	<?php
                }
                ?>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="modal-bisapay" tabindex="-1">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <form id="form-bisapay">
                            <div class="modal-header">
                                <h5 class="modal-title">Pembayaran</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <label>Nominal</label>
                                    <input min="10000" type="number" name="nominal" class="form-control" required="required"/>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                <button id="getPosts" type="submit" class="btn btn-primary">Lanjut</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            
            <div class="modal" id="payment-method-modal" tabindex="-1">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Lanjut Pembayaran</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body" id="payment-method-modal-body">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                            <button id="payNowBtn" type="submit" class="btn btn-primary">Bayar</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal" id="modal-bisapay-withdraw" tabindex="-1">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <form id="form-bisapay-withdraw">
                            <div class="modal-header">
                                <h5 class="modal-title">Tarik Dana Bisapay</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <div class="d-flex justify-content-between">
                                        <label>Nominal</label>
                                    </div>
                                    <input type="number" name="nominal" min="1" max="<?php echo $saldo; ?>" class="form-control" required="required"/>
                                </div>
                                <div class="form-group">
                                    <label>Bank Tujuan</label>
                                    <select data-bank-total="<?php echo $customerbanks->count(); ?>" name="destination" class="form-control" required="required">
                                        <option value="" selected="selected" disabled="disabled">-- Pilih Bank --</option>
                                        <?php foreach ($customerbanks as $key => $customerbank): ?>
                                        <option value="<?php echo $customerbank->id; ?>"><?php echo $customerbank->name; ?> - <?php echo $customerbank->account_name; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                <button type="submit" class="btn btn-primary">Tarik</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

