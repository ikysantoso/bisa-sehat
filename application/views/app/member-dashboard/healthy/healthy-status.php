<div class="content-page">
    <!-- Page Content -->
    <div class="health-check container">
        <div class="row">
            
            <?php include VIEWPATH.'/app/member-dashboard/dashboard-nav.php' ?>

            <?php $user = UserModel::find( $this->ion_auth->user()->row()->id ); ?>
            <?php $medical_record = $user->medical_records->last(); ?>

            <div class="medical-record col-md-12">
              <?php include VIEWPATH.'/app/member-dashboard/health-nav.php'; ?>
                <div class="nav-content">
                    <div class="d-flex mb-3 flex-column">
                        <div>
                            <h3 class="mt-2 mb-3"><?php echo $page_title; ?></h3>
                        </div>
                        <div>
                            <a href="<?php echo base_url('member/healthy/status_download'); ?>" class="<?php echo (!$medical_record) ? 'disabled' : '' ?> import-data">Download status kesehatan</a>
                        </div>
                    </div>

                    <div class="card mb-3">
                        <div class="card-header">Data Member</div>
                        <div class="card-body">
                            <h5 class="card-title"><?php echo $user->first_name." ".$user->last_name; ?></h5>
                            <p><?php echo $user->user_meta('gender'); ?> / <?php echo $user->age(); ?> tahun</p>
                            <p><?php echo $user->email; ?></p>
                        </div>
                    </div>
                    <?php 
                    if($medical_record): 
                        $problem = [];
                    ?>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <tr>
                                <td>1</td>
                                <th>BMI</th>
                                <td>
                                    <p><em>Diinput tanggal : <?php echo $medical_record->created_at; ?></em></p>
                                    <p>TB: <?php echo $medical_record->height; ?> cm</p>
                                    <p>BB: <?php echo $medical_record->weight; ?> kg</p>
                                    <p>BB Ideal: <?php echo $medical_record->bmi()['min']; ?> s/d <?php echo $medical_record->bmi()['max']; ?> kg</p>
                                    <p>BMI: <?php echo $medical_record->bmi()['bmi']; ?></p>
                                    <p>Ket: <?php echo health_result_color($medical_record->bmi_scale()['desc']); ?></p>
                                </td>
                                <td class="icon-column"><?php echo health_result_icon( $medical_record->bmi_scale()['desc'] );?></td>
                            </tr>
                            <?php 
                                if($medical_record->bmi_scale()['count'] == 1){
                                    $problem[] = 'overweight';
                                }else if($medical_record->bmi_scale()['count'] == 2){
                                    $problem[] = 'kegemukan';
                                }
                            ?>
                            <tr>
                                <td>2</td>
                                <th>Tekanan Darah</th>
                                <td><?php if ($medical_record->sistole==0){ echo 'Belum ada data';}else{?>
                                    <p><em>Diinput tanggal : <?php echo $medical_record->created_at; ?></em></p>
                                    <p><?php echo $medical_record->sistole; ?>/<?php echo $medical_record->diastole; ?> mmHg</p>
                                    <p>Ket: <?php echo health_result_color($medical_record->blood_pressure_scale()['desc']); ?></p><?php }?>
                                </td>
                                <td class="icon-column"><?php echo health_result_icon( $medical_record->blood_pressure_scale()['desc'] );?></td>
                            </tr>
                            <?php 
                            if($medical_record->blood_pressure_scale()['count'] > 0){
                                $problem[] = 'hipertensi';
                            }
                            ?>

                            <tr>
                                <td>3</td>
                                <th>Gula Darah</th>
                                <td><?php if ($medical_record->blood_sugar_level==0){ echo 'Belum ada data';}else{?>
                                    <p><em>Diinput tanggal : <?php echo $medical_record->created_at; ?></em></p>
                                    <p><?php echo $medical_record->blood_sugar_level; ?> mg/dL</p>
                                    <p>Ket: <?php echo health_result_color($medical_record->blood_sugar_level_scale()['desc']); ?></p><?php }?>
                                </td>
                                <td class="icon-column"><?php echo health_result_icon( $medical_record->blood_sugar_level_scale()['desc'] );?></td>
                            </tr>
                            <?php 
                            if($medical_record->blood_sugar_level_scale()['count'] > 0){
                                $problem[] = 'diabetes';
                            }
                            ?>

                            <tr>
                                <td>4</td>
                                <th>Kolesterol</th>
                                <td><?php if ($medical_record->cholesterol_level==0){ echo 'Belum ada data';}else{?>
                                    <p><em>Diinput tanggal : <?php echo $medical_record->created_at; ?></em></p>
                                    <p><?php echo $medical_record->cholesterol_level; ?> mg/dL</p>
                                    <p>Ket: <?php echo health_result_color($medical_record->cholesterol_level_scale()['desc']); ?></p><?php }?>
                                </td>
                                <td class="icon-column"><?php echo health_result_icon( $medical_record->cholesterol_level_scale()['desc'] );?></td>
                            </tr>
                            <?php 
                            if($medical_record->cholesterol_level_scale()['count'] > 0){
                                $problem[] = 'kolesterol';
                            }
                            ?>

                            <tr>
                                <td>5</td>
                                <th>Asam urat</th>
                                <td><?php if ($medical_record->uric_acid_level==0){ echo 'Belum ada data';}else{?>
                                    <p><em>Diinput tanggal : <?php echo $medical_record->created_at; ?></em></p>
                                    <p><?php echo $medical_record->uric_acid_level; ?> mg/dL</p>
                                    <p><?php echo health_result_color($medical_record->uric_acid_level_scale()['desc']); ?></p><?php }?>
                                </td>
                                <td class="icon-column"><?php echo health_result_icon( $medical_record->uric_acid_level_scale()['desc'] );?></td>
                            </tr>
                            <?php
                            if($medical_record->uric_acid_level_scale()['count'] > 0){
                                $problem[] = 'asam urat';
                            }
                            ?>
                            
                            <?php $fitness = $user->fitnesses->last(); ?>
                            <tr>
                                <td>6</td>
                                <th>Status kebugaran</th>
                                <td>
                                    <?php
                                    echo ($fitness) ? $fitness->mileage_result()['status'] : '<div class="alert alert-warning">Belum ada data test kebugaran</div>'; 
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td>**</td>
                                <th>Kesimpulan</th>
                                <td>
                                    <?php 
                                    $kesimpulan = get_status_kesimpulan($problem);
                                    echo $kesimpulan;
                                    ?>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <?php else: ?>
                        <div class="alert alert-warning">Belum ada data test kesehatan</div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>