<!DOCTYPE html>
<html>
<head>
	<title>RANGKUMAN DATA KESEHATAN</title>
	<style type="text/css">
			.table{
                border-collapse: collapse;
                width: 100%;
            }
            .table,
            .table th,
            .table td,
            {
                padding: 10px;
                border: 1px solid #ddd;
            }
            
            .table-striped tr:nth-child(even) {
              background-color: #f2f2f2;
            }
            .text-danger{
            	color: red;
            }
	</style>
</head>
<body>
    <div style="display: flex; justify-content: space-between;">
		<img src="<?php echo base_url('assets/images/BISA.png'); ?>" alt="" style="height:70px; width:70px; margin-left:60px; margin-top: 25px; position:absolute;">
		<div style="font-family:Roboto; font-weight:bold;">
			<h2 align="center">RANGKUMAN DATA KESEHATAN</h2>
			<h4 align="center">www.bisa-sehat.com</h4>
		</div>
	</div>
	<?php $user = UserModel::find( $user_id ); ?>
	<table class="table">
        <thead>
            <tr style="background-color: #676767;color: #fff">
                <th align="left" colspan="4">Data Member</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td style="width:20%;border: 0;">Nama Lengkap</td>
                <td style="width:25%;border: 0"><?php echo $user->first_name." ".$user->last_name; ?></td>
                <td style="width:auto;border-right: 0;border-top: 0;border-bottom: 0">Email</td>
                <td style="width:auto;border: 0"><?php echo $user->email; ?></td>
            </tr>
            <tr>
                <td style="width:20%;border: 0;">Gender / Umur</td>
                <td style="width:25%;border: 0;"><?php echo $user->user_meta('gender'); ?> / <?php echo $user->age(); ?> tahun</td>
                <td style="width:auto;border-right: 0;border-top: 0;border-bottom: 0">Golongan Darah</td>
                <td style="width:auto;border: 0"><?php echo $user->user_meta('blood-type'); ?></td>
            </tr>
        </tbody>
    </table>

				<?php 
                    if($medical_record = $user->medical_records->last()): 
                        $problem = [];
                    ?>
                        <table class="table">
                            <tr>
                                <td>1</td>
                                <th>BMI</th>
                                <td>
                                    <p><em>Diinput tanggal : <?php echo $medical_record->created_at; ?></em></p>
                                    <p>TB: <?php echo $medical_record->height; ?> cm</p>
                                    <p>BB: <?php echo $medical_record->weight; ?> kg</p>
                                    <p>BB Ideal: <?php echo $medical_record->bmi()['min']; ?> s/d <?php echo $medical_record->bmi()['max']; ?> kg</p>
                                    <p>BMI: <?php echo $medical_record->bmi()['bmi']; ?></p>
                                    <p>Ket: <?php echo health_result_color($medical_record->bmi_scale()['desc']); ?></p>
                                </td>
                                <td class="icon-column"><?php echo health_result_icon( $medical_record->bmi_scale()['desc'] );?></td>
                            </tr>
                            <?php 
                                if($medical_record->bmi_scale()['count'] == 1){
                                    $problem[] = 'overweight';
                                }else if($medical_record->bmi_scale()['count'] == 2){
                                    $problem[] = 'kegemukan';
                                }
                            ?>
                            <tr>                            
                                <td>2</td>
                                <th>Tekanan Darah</th>
                                <td><?php if ($medical_record->sistole==0){ echo 'Belum ada data';}else{?>
                                    <p><em>Diinput tanggal : <?php echo $medical_record->created_at; ?></em></p>
                                    <p><?php echo $medical_record->sistole; ?>/<?php echo $medical_record->diastole; ?> mmHg</p>
                                    <p>Ket: <?php echo health_result_color($medical_record->blood_pressure_scale()['desc']); ?></p><?php }?>
                                </td>
                                <td class="icon-column"><?php echo health_result_icon( $medical_record->blood_pressure_scale()['desc'] );?></td>
                            </tr>
                            <?php 
                            if($medical_record->blood_pressure_scale()['count'] > 0){
                                $problem[] = 'hipertensi';
                            }
                            ?>

                            <tr>
                                <td>3</td>
                                <th>Gula Darah</th>
                                <td><?php if ($medical_record->blood_sugar_level==0){ echo 'Belum ada data';}else{?>
                                    <p><em>Diinput tanggal : <?php echo $medical_record->created_at; ?></em></p>
                                    <p><?php echo $medical_record->blood_sugar_level; ?> mg/dL</p>
                                    <p>Ket: <?php echo health_result_color($medical_record->blood_sugar_level_scale()['desc']); ?></p><?php }?>
                                </td>
                                <td class="icon-column"><?php echo health_result_icon( $medical_record->blood_sugar_level_scale()['desc'] );?></td>
                            </tr>
                            <?php 
                            if($medical_record->blood_sugar_level_scale()['count'] > 0){
                                $problem[] = 'diabetes';
                            }
                            ?>

                            <tr>
                                <td>4</td>
                                <th>Kolesterol</th>
                                <td><?php if ($medical_record->cholesterol_level==0){ echo 'Belum ada data';}else{?>
                                    <p><em>Diinput tanggal : <?php echo $medical_record->created_at; ?></em></p>
                                    <p><?php echo $medical_record->cholesterol_level; ?> mg/dL</p>
                                    <p>Ket: <?php echo health_result_color($medical_record->cholesterol_level_scale()['desc']); ?></p><?php }?>
                                </td>
                                <td class="icon-column"><?php echo health_result_icon( $medical_record->cholesterol_level_scale()['desc'] );?></td>
                            </tr>
                            <?php 
                            if($medical_record->cholesterol_level_scale()['count'] > 0){
                                $problem[] = 'kolesterol';
                            }
                            ?>

                            <tr>
                                <td>5</td>
                                <th>Asam urat</th>
                                <td><?php if ($medical_record->uric_acid_level==0){ echo 'Belum ada data';}else{?>
                                    <p><em>Diinput tanggal : <?php echo $medical_record->created_at; ?></em></p>
                                    <p><?php echo $medical_record->uric_acid_level; ?> mg/dL</p>
                                    <p><?php echo health_result_color($medical_record->uric_acid_level_scale()['desc']); ?></p><?php }?>
                                </td>
                                <td class="icon-column"><?php echo health_result_icon( $medical_record->uric_acid_level_scale()['desc'] );?></td>
                            </tr>
                            <?php 
                            if($medical_record->uric_acid_level_scale()['count'] > 0){
                                $problem[] = 'asam urat';
                            }
                            ?>
                            
                            <?php $fitness = $user->fitnesses->last(); ?>
                            <tr>
                                <td>6</td>
                                <th>Status Kebugaran</th>
                                <td>
                                    <?php
                                    echo ($fitness) ? $fitness->mileage_result()['status'] : '<div class="alert alert-warning">Belum ada data test kebugaran</div>'; 
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td>**</td>
                                <th>Kesimpulan</th>
                                <td>
                                    <?php 
                                    $kesimpulan = get_status_kesimpulan($problem);
                                    echo $kesimpulan;
                                    ?>
                                </td>
                            </tr>
                        </table>
                    <?php else: ?>
                        <div class="alert alert-warning">Belum ada data test kesehatan.</div>
                    <?php endif; ?>

</body>
</html>