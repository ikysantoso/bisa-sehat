    <div class="container" style="padding-top:30px; margin-top: 75px;">
        <div class="row">
            <div class="col-md-6 mb-4 mx-auto">
                <div class="border rounded p-4">
                    <h3 class="mb-3">Update Profile</h3>
                    <form id="form-update-profile">
                        <div class="form-group">
                            <label>Username</label>
                            <input required="required" type="text" class="form-control" disabled="disabled" value="<?php echo $user_info->username; ?>" />
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input required="required" type="text" class="form-control" disabled="disabled" value="<?php echo $user_info->email; ?>"/>
                        </div>
                        <div class="form-group">
                            <label>NIK/No. KTP</label>
                            <input value="<?php echo $user_info->user_meta( 'nik' ); ?>" type="text" minlength="16" maxlength="16" class="form-control" name="nik"/>
                            <small class="form-text text-muted">* Nik/No. KTP bersifat optional dapat diisi ataupun tidak</small>
                        </div>
                        <div class="form-group">
                            <label>Nama Depan</label>
                            <input value="<?php echo $user_info->first_name; ?>" required="required" type="text" class="form-control" name="first_name"/>
                        </div>
                        <div class="form-group">
                            <label>Nama Belakang</label>
                            <input value="<?php echo $user_info->last_name; ?>" type="text" class="form-control" name="last_name"/>
                        </div>
                        <div class="form-group">
                            <label class="d-block">Jenis Kelamin</label>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input value="Laki-Laki" <?php echo ($user_info->user_meta( 'gender' ) == 'Laki-Laki') ? 'checked="checked"' : ''; ?> required="required" type="radio" id="laki-Laki" name="gender" class="custom-control-input">
                                <label class="custom-control-label" for="laki-Laki">Laki-Laki</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input value="Perempuan" <?php echo ($user_info->user_meta( 'gender' ) == 'Perempuan') ? 'checked="checked"' : ''; ?>required="required" type="radio" id="perempuan" name="gender" class="custom-control-input">
                                <label class="custom-control-label" for="perempuan">Perempuan</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Tanggal lahir</label>
                            <input required="required" value="<?php echo date('Y-m-d', strtotime($user_info->user_meta( 'date-birth' ))); ?>" type="date" class="form-control" name="date-birth" placeholder="dd/mm/yyyy"/>
                        </div>
                        <div class="form-group">
                            <label class="d-block">Golongan Darah</label>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input value="A" <?php echo ($user_info->user_meta( 'blood-type' ) == 'A') ? 'checked="checked"' : ''; ?> required="required" type="radio" id="a" name="blood-type" class="custom-control-input">
                                <label class="custom-control-label" for="a">A</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input value="B" <?php echo ($user_info->user_meta( 'blood-type' ) == 'B') ? 'checked="checked"' : ''; ?> required="required" type="radio" id="b" name="blood-type" class="custom-control-input">
                                <label class="custom-control-label" for="b">B</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input value="AB" <?php echo ($user_info->user_meta( 'blood-type' ) == 'AB') ? 'checked="checked"' : ''; ?> required="required" type="radio" id="ab" name="blood-type" class="custom-control-input">
                                <label class="custom-control-label" for="ab">AB</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input value="O" <?php echo ($user_info->user_meta( 'blood-type' ) == 'O') ? 'checked="checked"' : ''; ?> required="required" type="radio" id="o" name="blood-type" class="custom-control-input">
                                <label class="custom-control-label" for="o">O</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input value="None" <?php echo ($user_info->user_meta( 'blood-type' ) == 'None') ? 'checked="checked"' : ''; ?> required="required" type="radio" id="none" name="blood-type" class="custom-control-input">
                                <label class="custom-control-label" for="none">Tidak Tahu</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Nomor Telepon</label>
                            <input value="<?php echo ($user_info->phone == '') ? '+62' : $user_info->phone; ?>" required="required" type="text" class="form-control" name="phone">
                        </div>

                        <div class="form-group">
                            <label>Pekerjaan</label>
                            <input value="<?php echo $user_info->user_meta( 'job' ); ?>" type="text" class="form-control" name="job">
                        </div>
                        <div class="form-group">
                            <label>Agama</label>
                            <select name="religion" class="form-control">
                                <option <?php echo ($user_info->user_meta( 'religion' ) == '-') ? 'selected' : ''; ?> value="-">-</option>
                                <option <?php echo ($user_info->user_meta( 'religion' ) == 'Islam') ? 'selected' : ''; ?> value="Islam">Islam</option>
                                <option <?php echo ($user_info->user_meta( 'religion' ) == 'Budha') ? 'selected' : ''; ?> value="Budha">Budha</option>
                                <option <?php echo ($user_info->user_meta( 'religion' ) == 'Hindu') ? 'selected' : ''; ?> value="Hindu">Hindu</option>
                                <option <?php echo ($user_info->user_meta( 'religion' ) == 'Kristen') ? 'selected' : ''; ?> value="Kristen">Kristen</option>
                                <option <?php echo ($user_info->user_meta( 'religion' ) == 'Katolik') ? 'selected' : ''; ?> value="Katolik">Katolik</option>
                                <option <?php echo ($user_info->user_meta( 'religion' ) == 'Konghucu') ? 'selected' : ''; ?> value="Konghucu">Konghucu</option>
                            </select>
                        </div>

                        <button type="submit" class="btn btn-primary mb-3">Update</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
