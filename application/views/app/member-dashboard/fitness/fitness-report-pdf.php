<!DOCTYPE html>
<html>
<head>
	<title>RANGKUMAN DATA KEBUGARAN</title>
	<style type="text/css">
			.table{
                border-collapse: collapse;
                width: 100%;
            }
            .table,
            .table th,
            .table td,
            {
                padding: 10px;
                border: 1px solid #ddd;
            }
            
            .table-striped tr:nth-child(even) {
              background-color: #f2f2f2;
            }
            .text-danger{
            	color: red;
            }
	</style>
</head>
<body>
	<div style="display: flex; justify-content: space-between;">
		<img src="<?php echo base_url('assets/images/BISA.png'); ?>" alt="" style="height:70px; width:70px; margin-left:60px; margin-top: 25px; position:absolute;">
		<div style="font-family:Roboto; font-weight:bold;">
			<h2 align="center">RANGKUMAN DATA KEBUGARAN</h2>
			<h4 align="center">www.bisa-sehat.com</h4>
		</div>
	</div>
	<?php 
	$user = UserModel::find( $fitnesses[0]->user->id ); 
	$total = $fitnesses->count();
	?>
	<table class="table">
		<thead>
			<tr style="background-color: #676767;color: #fff">
				<th align="left" colspan="4">Data Member</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td style="width:20%;border: 0;">Nama Lengkap</td>
				<td style="width:25%;border: 0"><?php echo $user->first_name." ".$user->last_name; ?></td>
				<td style="width:auto;border-right: 0;border-top: 0;border-bottom: 0">Email</td>
				<td style="width:auto;border: 0"><?php echo $user->email; ?></td>
			</tr>
			<tr>
				<td style="width:20%;border: 0;">Gender / Umur</td>
				<td style="width:25%;border: 0;"><?php echo $user->user_meta('gender'); ?> / <?php echo $user->age(); ?> tahun</td>
				<td style="width:auto;border-right: 0;border-top: 0;border-bottom: 0">Golongan Darah</td>
				<td style="width:auto;border: 0"><?php echo $user->user_meta('blood-type'); ?></td>
			</tr>
		</tbody>
	</table>

	<table class="table">
		<thead>
			<tr style="background-color: #676767;color: #fff">
				<?php 
				if ( $total > 1 ) {
					echo '<th>No</th>';
				}
				?>
				<th>Tgl Input</th>
				<th>Tinggi Badan (cm)</th>
				<th>Berat Badan (Kg)</th>
				<th>Jarak Tempuh Ideal (m)</th>
				<th>Jarak Tempuh (m)</th>
				<th>Status</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($fitnesses as $key => $fitness): ?>
			<tr>
				<?php 
				if ( $total > 1 ) {
					echo '<td>'.($key+1).'</td>';
				}
				?>
				<td><?php echo $fitness->created_at->format('d-m-Y H:i:s'); ?></td>
				<td><?php echo $fitness->height; ?></td>
                <td><?php echo $fitness->weight; ?></td>
                <td><?php echo $fitness->mileage_result()['ideal']; ?></td>
                <td><?php echo $fitness->mileage; ?></td>
                <td><?php echo $fitness->mileage_result()['status']; ?></td>
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
</body>
</html>