<style>
    table {
    border: 1px solid #ccc;
    border-collapse: collapse;
    margin: 0;
    padding: 0;
    width: 100%;
    table-layout: fixed;
    }

    table caption {
    font-size: 1.5em;
    margin: .5em 0 .75em;
    }

    /* table tr {
    background-color: #f8f8f8;
    border: 1px solid #ddd;
    padding: .35em;
    } */

    table th,
    table td {
    text-align: left;
    }

    table th {
        font-size: 15px;
        font-weight: bold;
        font-family: Poppins;
    }

    @media screen and (max-width: 600px) {
    table {
        border: 0;
    }

    table caption {
        font-size: 1.3em;
    }
    
    table thead {
        border: none;
        clip: rect(0 0 0 0);
        height: 1px;
        margin: -1px;
        overflow: hidden;
        padding: 0;
        position: absolute;
        width: 1px;
    }
    
    table tr {
        background-color: #f8f8f8;
        border-bottom: 3px solid #ddd;
        display: block;
        margin-bottom: .625em;
    }
    
    table td {
        border-bottom: 1px solid #ddd;
        display: block;
        font-size: .8em;
        text-align: right;
    }
    
    table td::before {
        /*
        * aria-label has no advantage, it won't be read inside a table
        content: attr(aria-label);
        */
        content: attr(data-label);
        float: left;
        font-weight: bold;
        text-transform: uppercase;
    }
    
    table td:last-child {
        border-bottom: 0;
    }
    }
</style>

<div class="content-page">
    <!-- Page Content -->
    <div class="health-check container">
        <div class="row">
            
            <?php include VIEWPATH.'/app/member-dashboard/dashboard-nav.php' ?>

            <div class="medical-record col-md-12">
              <?php include VIEWPATH.'/app/member-dashboard/health-nav.php'; ?>
                <div class="nav-content">
                    <div class="d-flex flex-column mb-3">
                        <div>
                            <h3 class="mt-2 mb-3"><?php echo $page_title; ?></h3>
                        </div>
                        <div>
                            <a href="<?php echo base_url('member/fitness/new'); ?>" class="import-data">Tambah Data</a>
                            <?php if($fitnesses->count() > 0): ?>
                            <a href="<?php echo base_url('member/fitness/download'); ?>" class="import-data">Download Semua</a>
                            <?php endif; ?>  
                        </div>
                    </div>
                    <?php 
                        if ( $this->session->flashdata('message') && $this->session->flashdata('status') ) {
                            echo '<div class="alert alert-'.$this->session->flashdata('status').'">'.$this->session->flashdata('message').'</div>';
                        }
                    ?>
                    <?php if($fitnesses->count() > 0): ?>
                    <div class="table-responsive">
                        <table id="table-fitness" class="table table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Tanggal Input</th>
                                    <th>Tinggi Badan (cm)</th>
                                    <th>Berat Badan (kg)</th>
                                    <th>Jarak Tempuh Ideal (m)</th>
                                    <th>Jarak Tempuh (m)</th>
                                    <th>Status</th>
                                    <th>#</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($fitnesses as $key => $fitness) : ?>
                                <tr>
                                    <td data-label="No"><?php echo $key+1; ?></td>
                                    <td data-label="Tanggal Input"><?php echo $fitness->created_at; ?></td>
                                    <td data-label="Tinggi Badan (cm)"><?php echo $fitness->height; ?></td>
                                    <td data-label="Berat Badan (kg)"><?php echo $fitness->weight; ?></td>
                                    <td data-label="Jarak Tempuh Ideal (m)"><?php echo $fitness->mileage_result()['ideal']; ?></td>
                                    <td data-label="Jarak Tempuh (m)"><?php echo $fitness->mileage; ?></td>
                                    <td data-label="Status"><?php echo $fitness->mileage_result()['status']; ?></td>
                                    <td data-label="#">
                                        <!-- <button data-id="<?php echo $fitness->id ?>" class="btn btn-sm btn-warning btn-edit"><i class="fas fa-edit"></i></button>
                                        <button data-id="<?php echo $fitness->id ?>" class="btn btn-sm btn-danger btn-delete"><i class="fas fa-trash-alt"></i></button> -->
                                        <a href="<?php echo base_url('member/fitness/download/'.$fitness->id); ?>" class="btn btn-sm btn-secondary"><i class="fas fa-file-download"></i></a>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    <?php else: ?>
                    <div class="alert alert-warning" style="margin-bottom:100px;">Belum ada data test kesehatan</div>    
                    <?php endif; ?>                    
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="modal-fitness" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form id="form-fitness">
                <input type="hidden" name="id"/>
                <div class="modal-header">
                    <h5 class="modal-title">Tambah Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Tinggi Badan</label>
                                <div class="input-group mb-3">
                                    <input min="1" maxlength="3" class="form-control" type="number" name="height" required="required"/>
                                    <div class="input-group-append">
                                        <span class="input-group-text">cm</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Berat Badan</label>
                                <div class="input-group mb-3">
                                    <input maxlength="3" min="1" class="form-control" type="number" name="weight" required="required"/>
                                    <div class="input-group-append">
                                        <span class="input-group-text">kg</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php 
                    $user = UserModel::find( $this->ion_auth->user()->row()->id );
                    $gender = $user->user_meta('gender');
                    $age = date_diff(date_create($user->user_meta('date-birth')), date_create('now'))->y;
                    ?>
                    <div class="form-group">
                        <label>Jarak Tempuh Jalan Cepat (Bukan Berlari !, tapi Berjalan Cepat) selama 6 menit</label>
                        <div class="input-group mb-3">
                            <input data-gender="<?php echo $gender; ?>" data-age="<?php echo $age; ?>" disabled="disabled" min="1" maxlength="4" class="form-control" type="number" name="mileage" required="required"/>
                            <div class="input-group-append">
                                <span class="input-group-text">m</span>
                            </div>
                        </div>
                        <small class="form-text text-muted">Masukkan angkanya dalam satuan meter</small>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
