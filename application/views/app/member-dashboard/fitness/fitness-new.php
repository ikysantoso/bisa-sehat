<div class="content-page">
    <!-- Page Content -->
    <div class="health-check container">
        <div class="row">
            
            <?php include VIEWPATH.'/app/member-dashboard/dashboard-nav.php' ?>

            <div class="col-md-12 medical-record">
                <div class="nav-content">
                    <div class="d-flex mb-3 justify-content-between align-items-center">
                        <h3 class="mb-0"><?php echo $page_title; ?></h3>
                    </div>
                    
                    <form style="max-width: 800px" id="form-fitness">
                            <input type="hidden" name="id"/>
                       
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Tinggi Badan</label>
                                        <div class="input-group mb-3">
                                            <input min="1" maxlength="3" class="form-control" type="number" name="height" required="required"/>
                                            <div class="input-group-append">
                                                <span class="input-group-text">cm</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Berat Badan</label>
                                        <div class="input-group mb-3">
                                            <input maxlength="3" min="1" class="form-control" type="number" name="weight" required="required"/>
                                            <div class="input-group-append">
                                                <span class="input-group-text">kg</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php 
                            $user = UserModel::find( $this->ion_auth->user()->row()->id );
                            $gender = $user->user_meta('gender');
                            $age = date_diff(date_create($user->user_meta('date-birth')), date_create('now'))->y;
                            ?>
                            <div class="form-group">
                                <label>Jarak Tempuh Jalan Cepat (Bukan Berlari !, tapi Berjalan Cepat) selama 6 menit</label>
                                <div class="input-group mb-3">
                                    <input data-gender="<?php echo $gender; ?>" data-age="<?php echo $age; ?>" disabled="disabled" min="1" maxlength="4" class="form-control" type="number" name="mileage" required="required"/>
                                    <div class="input-group-append">
                                        <span class="input-group-text">m</span>
                                    </div>
                                </div>
                                <small class="form-text text-muted">Masukkan angkanya dalam satuan meter</small>
                            </div>
                            <div class="text-center">
                                <a href="<?php echo base_url('member/fitness'); ?>" class="btn btn-secondary"><i class="fas fa-chevron-left"></i> Kembali</a>
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                    </form>
                              
                </div>
            </div>
        </div>
    </div>
</div>