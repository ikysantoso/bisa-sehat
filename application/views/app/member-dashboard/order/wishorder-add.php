<div class="content-page">
    <!-- Page Content -->
    <div class="health-check container">
        <div class="row">
            
            <?php include VIEWPATH.'/app/member-dashboard/dashboard-nav.php' ?>
            
            <div class="col-md-12">
                <div class="nav-content">
                    <h3 class="mb-4">Wishlist</h3>
                    <form id="form-wishorder">
                        <div class="form-group row">
                            <label for="" class="col-form-label col-lg-2">Nama Barang</label>
                            <div class="col-lg-5">
                                <input type="text" class="form-control" name="name" required="required"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="" class="col-form-label col-lg-2">Harga Yang Diinginkan</label>
                            <div class="col-lg-3 input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Rp</span>
                                </div>
                                <input type="number" class="form-control text-right" name="price" required="required"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="" class="col-form-label col-lg-2">Jumlah Yang di Inginkan</label>
                            <div class="col-lg-3">
                                <div class="row mx-n1">
                                    <div class="col px-1">
                                        <input type="number" class="form-control text-right" name="quantity" required="required">
                                    </div>
                                    <div class="col-auto px-1">
                                        <select name="unit" class="form-control" required="required">
                                            <option value="0" selected="selected" disabled="disabled">-- unit --</option>
                                            <option value="Kg">Kg</option>
                                            <option value="Liter">Liter</option>
                                            <option value="Buah">Buah</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="" class="col-form-label col-lg-2">Keterangan</label>
                            <div class="col-lg-6">
                                <textarea name="description" cols="30" rows="5" class="form-control" required="required"></textarea>
                            </div>
                        </div>
                        <div class="form-group row mt-4">
                            <div class="col offset-md-2">
                                <button type="submit" class="btn filled-button">Pesan Sekarang</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
