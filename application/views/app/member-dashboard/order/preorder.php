<div class="content-page">
    <!-- Page Content -->
    <div class="health-check container">
        <div class="row">
            
            <?php include VIEWPATH.'/app/member-dashboard/dashboard-nav.php' ?>

            <div class="col-md-12">
              <?php include VIEWPATH.'/app/member-dashboard/order/order-nav.php'; ?>
                <div class="nav-content">
                    
                    <?php if ($transactions->count() > 0): ?>

                        <?php 
                        $showed_order = 0;
                        foreach($transactions as $key => $transaction ): 
                            
                            $payment_status = ($transaction->payment) ? $transaction->payment->status : $transaction->payment_status;
                            if( $payment_status != 'unpaid' ):

                                $showed_order++;

                                $product = $transaction->transaction_items[0]->product;

                                ?>
                                <div class="card mb-3">
                                    <div class="card-header bg-transparent">
                                        <small class="text-muted"><?php echo format_date( $transaction->created_at, 'd F Y H:i:s' ); ?></small>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-lg-auto">
                                                <div class="bg-light embed-responsive embed-responsive-4by3 bg-vertical-center" style="background-image: url('<?php echo (isset($product->images) && count($product->images) > 0) ? base_url('assets/images/upload/product/'.$product->images[0]->image) : 'https://via.placeholder.com/348x261?text=No%20Image' ?>');" alt=""></div>
                                            </div>
                                            <div class="col-lg">
                                                <strong>#<?php echo $transaction->id; ?></strong><br/>
                                                <?php echo ($product) ? $product->name : '-- deleted product --'; ?>
                                                <br/>
                                                <small class="text-muted"><?php echo ($product) ? printTruncated(100, $product->description) : ''; ?></small>
                                            </div>
                                            <div class="col-lg-3 ml-auto mb-2 mb-lg-0 border-left">
                                                <label class="text-muted m-0"><small>Total Belanja</small></label>
                                                <div class="form-text font-weight-bold text-accent"><?php echo rupiah($transaction->grand_total( $transaction->id )); ?></div>
                                            </div>
                                            <div class="col-lg-2 mb-2 mb-lg-0 border-left">
                                                <label class="text-muted m-0"><small>Status</small></label>
                                                <div class="form-text font-weight-bold">
                                                    <?php 
                                                        $transaction_status = ($transaction->status == 'pengemasan') ? 'diproses' : $transaction->status;
                                                        echo transaction_status_icon( ($payment_status == 'paid') ? $transaction_status : $payment_status );
                                                    ?>
                                                </div>
                                            </div>
                                            <div class="col-lg-2 border-left">
                                                <div class="d-flex align-items-center h-100">
                                                    <a href="<?php echo base_url('member/order/repeat_order/'.$transaction->id) ?>" class="btn filled-button repeat-order">Beli Lagi</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-footer bg-transparent">
                                        <a href="<?php echo base_url('member/order/preorder_detail/'.$transaction->id); ?>" class="text-decoration-none text-danger"><i class="fa fa-eye mr-2"></i> Lihat Detail Pesanan</a>
                                    </div>
                                </div>
                        <?php 
                            else:
                            endif;
                        endforeach; ?>

                        <?php if($showed_order == 0): ?>
                        <div class="alert alert-danger d-flex justify-content-between align-items-center" style="margin-bottom:100px">
                            Anda belum mempunyai list order saat ini. Yuk belanja sekarang!
                            <a href="<?php echo base_url('product/all') ?>" class="btn btn-dark">Lihat Produk</a>
                        </div>
                        <?php endif; ?>
                    
                    <?php else: ?>
                    <div class="alert alert-danger d-flex justify-content-between align-items-center" style="margin-bottom:100px">
                        Anda belum mempunyai list order saat ini. Yuk belanja sekarang!
                        <a href="<?php echo base_url('product/all') ?>" class="btn btn-dark">Lihat Produk</a>
                    </div>
                    <?php endif; ?>
                    
                </div>
            </div>
        </div>
    </div>
</div>
