<div class="content-page">
    <!-- Page Content -->
    <div class="health-check container">
        <div class="row">
            
            <?php include VIEWPATH.'/app/member-dashboard/dashboard-nav.php' ?>

            <div class="col-md-12">
                <div class="nav-content">
                    <div class="mb-4">
                        <a href="<?php echo base_url('member/order/wishorder_add') ?>" class="btn btn-light"><i class="fa fa-plus fa-fw"></i> Ajukan Permohonan Produk</a>
                    </div>
                    <div class="table-responsive">
                        <table id="table-wishorder" class="table table-bordered">
                            <thead>
                                <tr>
                                    <th width="10">No</th>
                                    <th class="text-center">Tanggal</th>
                                    <th>Nama Barang</th>
                                    <th class="text-right">Harga Yang Diinginkan</th>
                                    <th class="text-right">Qty</th>
                                    <th class="text-left">Unit</th>
                                    <th class="text-center">Status</th>
                                    <th>ID</th>
                                    <th>#</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
