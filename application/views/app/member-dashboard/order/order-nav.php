<div class="container">
   <div class="row">
      <div class="btn-group" role="group" aria-label="Basic example">
          <a href="<?php echo base_url('member/order') ?>" class="btn btn-outline-info <?php echo ($page == 'member-order') ? 'active' : ''; ?>">Order</a>
          <a href="<?php echo base_url('member/order/preorder') ?>" class="btn btn-outline-info <?php echo ($page == 'member-preorder') ? 'active' : ''; ?>">Pre order</a>
      </div>
   </div>
</div>
