<div class="content-page">
    <!-- Page Content -->
    <div class="health-check container">
        <div class="row">
            <?php
                include VIEWPATH.'/app/member-dashboard/dashboard-nav.php';
                $payment_status = ($transaction->payment) ? $transaction->payment->status : $transaction->payment_status;
                $payment_method = ($transaction->payment) ? $transaction->payment->payment_method : $transaction->payment_method;
            ?>

            <div class="col-md-12">
                <div class="nav-content">
                	<div class="row">
                		<div class="col-md-6 mb-4">
                			<h5 class="mb-3">Detail Order</h5>
                			<div class="border p-3 rounded">
			                	<div class="mb-2 d-flex justify-content-between align-items-center">
			                		<b>Order ID</b>
			                		<div class="flex-fill border mx-3"></div>
			                		<span>#<?php echo $transaction->transaction_id; ?></span>
			                	</div>
			                	<div class="mb-2 d-flex justify-content-between align-items-center">
			                		<b>Tanggal Order</b>
			                		<div class="flex-fill border mx-3"></div>
			                		<span><?php echo $transaction->created_at; ?></span>
			                	</div>
                            
                                <div class="mb-2 d-flex justify-content-between align-items-center">
			                		<b>Gudang</b>
			                		<div class="flex-fill border mx-3"></div>
			                		<span><?php echo $warehouse->name; ?></span>
			                	</div>
                               
                                <div class="mb-2 d-flex justify-content-between align-items-center">
                                    <b>Status</b>
                                    <div class="flex-fill border mx-3"></div>
                                    <span>
                                        <?php
                                            $payment_status = ($transaction->payment) ? $transaction->payment->status : $transaction->payment_status;
                                            $transaction_status = ($transaction->status == 'pengemasan' && $transaction->is_preorder=='yes') ? 'diproses' : $transaction->status;
                                            echo transaction_status_icon( ($payment_status == 'paid') ? $transaction_status : $payment_status );
                                        ?>
                                    </span>
                                </div>

                                <?php if($transaction->is_preorder == 'yes' && $transaction->status != 'cancelled'): ?>
                                <div class="mb-2 d-flex justify-content-between align-items-center">
                                    <b>Sisa Waktu Preorder</b>
                                    <div class="flex-fill border mx-3"></div>
                                    <span><?php countdown( date('Y-m-d H:i:s', strtotime($transaction->created_at. ' + 3 days')) ); ?></span>
                                </div>
                                <?php endif; ?>

                                <?php if($transaction->status =='in delivery' || $transaction->status =='delivered'): ?>
                                <!-- <div class="mb-2 d-flex justify-content-between align-items-center">
                                    <b>Jenis Pengiriman</b>
                                    <div class="flex-fill border mx-3"></div>
                                    <span><?php echo $transaction->shipping_courier; ?></span>
                                </div> -->
                                <div class="mb-2 d-flex justify-content-between align-items-center">
                                    <b>Resi Pengiriman</b>
                                    <div class="flex-fill border mx-3"></div>
                                    <span><?php echo $transaction->tracking_code; ?></span>
                                </div>
                                <div class="mb-2 d-flex justify-content-between align-items-center">
                                    <b>Jasa Pengiriman</b>
                                    <div class="flex-fill border mx-3"></div>
                                    <span><?php echo $transaction->delivery_service; ?></span>
                                </div>
                                <?php endif; ?>

                			</div>
                		</div>
                		<div class="col-md-6 mb-4">
                			<h5 class="mb-3">Alamat Pengiriman</h5>
                			<?php
                				$address = json_decode($transaction->customer_address_data);
                				$this->load->model( 'WilayahProvinsiModel' );
                				$province = WilayahProvinsiModel::where( 'province_id',$address->kabupaten->province_id )->first();
                            ?>
                			<div class="card">
                				<div class="card-body">
                					<div class="text-capitalize"><?php echo $address->name; ?> - <b><?php echo $address->receiver_name; ?></b> </div>
                					<?php
                					echo $address->village_text.' - '.
                						 $address->kecamatan->subdistrict_name.' - '.
                						 $address->kabupaten->city_name.'<br/>'.$province->province.' <br/> '.
                						 'Phone: '.$address->phone
                					?>
                				</div>
                			</div>

                		</div>
                		<div class="col-12 mb-4">
                            <div class="d-flex justify-content-between align-items-center mb-3">
                    			<h5 class="mb-0">Products</h5>
                                <?php
                                    if ( $transaction->status == 'in delivery' ) {
                                        echo '<button data-id="'.$transaction->id.'" class="btn btn-sm btn-primary btn-transaction-confirmation" onclick="transactionConfirmation();">Sudah terima barang?</button>';
                                    }
                                ?>
                            </div>

                            <?php
                            $total_ongkos_kirim = 0;
                            $total_produk = 0;

                            foreach (($transaction->payment->status == 'paid') ? [$transaction] : $transaction->payment->transactions as $key => $transaction): ?>
                            <table class="table table-striped table-bordered">
                				<thead>
                					<tr>
                						<td class="text-center">#</td>
                						<td>Product</td>
                						<td class="text-center">Quantity</td>
                						<td class="text-right">Price</td>
                						<td class="text-right">Sub Total</td>
                					</tr>
                				</thead>
                				<tbody>
                                <?php foreach($transaction->transaction_items as $key => $transaction_item): ?>
                					<tr>
                						<td class="text-center"><?php echo $key+1; ?></td>
                						<td><?php
                                            echo ($transaction_item->product) ? $transaction_item->product->name.( ($transaction_item->preorder_time()) ? '<br/><span class="badge badge-pill badge-secondary">'.$transaction_item->preorder_time().'</span>' : '' ) : '--deleted product--'; ?></td>
                                            <td class="text-center"><?php echo $transaction_item->quantity; ?></td>
                                            <td class="text-right"><?php echo rupiah($transaction_item->price); ?></td>
                                            <td class="text-right"><?php echo rupiah($transaction_item->quantity * $transaction_item->price);
                                        ?></td>
                					</tr>
                				<?php endforeach; ?>
                				</tbody>
                				<tfoot>
                					<tr>
                						<td colspan="4" class="text-right">Product Total</td>
                						<td class="text-right"><?php echo rupiah($transaction->total()); ?></td>
                					</tr>
                					<tr>
                                        <td colspan="4" class="text-right">Ongkos Kirim <?php echo $transaction->shipping_courier; ?></td>
                                        <?php if($transaction->shipping_courier === "Non Jabodetabek"): ?>
                                            <td class="text-right font-weight-bold" style="color: red; font-size: 20pt;" onclick="showHint();">?</td>
                                        <?php else: ?>
                                            <td class="text-right"><?php echo rupiah($transaction->shipping_fee); ?></td>
                                        <?php endif; ?>
                                    </tr>

                                    <?php if($transaction->payment_unique_code): ?>
                                    <tr>
                                        <td colspan="4" class="text-right">Kode Unik</td>
                                        <td class="text-right"><?php echo rupiah($transaction->payment_unique_code); ?></td>
                                    </tr>
                                    <?php endif; ?>

                                    <?php if($transaction->voucher_name && $transaction->$voucher_nominal): ?>
                                    <tr>
                                        <td colspan="4" class="text-right">Voucher</td>
                                        <td class="text-right"><?php echo $transaction->voucher_name; ?> <br /> <?php echo rupiah($transaction->voucher_nominal); ?></td>
                                    </tr>
                                    <?php endif; ?>

                                    <tr>
                						<th colspan="4" class="text-right">Total</th>
                                        <th class="text-right"><?php echo rupiah($transaction->grand_total( $transaction->id )); ?></th>
                					</tr>
                				</tfoot>
                			</table>
                            <?php
                                $total_produk += $transaction->total();
                                $total_ongkos_kirim += $transaction->shipping_fee;
                            endforeach; ?>

                		</div>
                        <?php if($payment_method && $payment_status != 'paid' || 'unpaid'): ?>
                        <div class="col-md-12">
                            <h5 class="mb-3">Pembayaran</h5>
                            <div class="row">
                                <div class="col-md-6 mb-3 order-md-2">
                                    <div class="shadow-sm border p-3 mx-auto mb-4">
                                        <div class="d-flex justify-content-between align-items-center">
                                            <b>Total Produk</b>
                                            <div class="border-bottom mx-3 flex-fill"></div>
                                            <b><?php echo rupiah($total_produk); ?></b>
                                        </div>
                                        <div class="d-flex justify-content-between align-items-center">
                                            <b>Total Ongkos Kirim</b>
                                            <div class="border-bottom mx-3 flex-fill"></div>
                                            <b><?php echo rupiah($total_ongkos_kirim); ?></b>
                                        </div>

                                        <?php
                                        $unique_code = $transaction->payment->unique_code;
                                        ?>

                                        <?php if($unique_code != 0): ?>
                                        <div class="d-flex justify-content-between align-items-center">
                                            <b>Kode Unik</b>
                                            <div class="border-bottom mx-3 flex-fill"></div>
                                            <b><?php echo rupiah($unique_code); ?></b>
                                        </div>
                                        <?php endif; ?>

                                        <?php
                                        $diskon = ($transaction->payment->voucher_user_id) ? ($transaction->payment->voucher_user->voucher->nominal) : 0;
                                        ?>
                                        <?php if($diskon != 0): ?>
                                        <div class="d-flex justify-content-between align-items-center">
                                            <b>Diskon (<?php echo $transaction->payment->voucher_user->voucher->name; ?>)</b>
                                            <div class="border-bottom mx-3 flex-fill"></div>
                                            <b><?php echo rupiah($diskon * -1); ?></b>
                                        </div>
                                        <?php endif; ?>

                                        <div class="d-flex justify-content-between align-items-center mt-2">
                                            <h5 class="mb-0">Grand Total</h5>
                                            <div class="border-bottom mx-3 flex-fill"></div>
                                            <h5 class="mb-0"><?php echo rupiah($total_produk + $total_ongkos_kirim + $unique_code - $diskon); ?></h5>
                                        </div>
                                    </div>
                                    <?php endif; ?>
                                    
                                    <?php if($payment_method && $payment_status != 'paid'): ?>

                                    <div class="border rounded p-3 bg-primary text-white">
                                        <b><?php echo $transaction->payment->bank->name; ?></b>
                                        <p class="text-white">Atas nama: <?php echo $transaction->payment->bank->account_name; ?></p>
                                        <p class="text-white">Nomor Rekening: <?php echo $transaction->payment->bank->account_number; ?></p>
                                    </div>
                                </div>
                                <div class="col-md-6 text-center mb-3 order-md-1">
                                    <?php if($payment_status == 'unpaid'): ?>
                                        <?php
                                            $date = new DateTime( $transaction->created_at );
                                            $date->add(new DateInterval('P1D'));
                                        ?>
                                        <div class="alert alert-warning">Silahkan selesaikan pembayaran sebelum <b><?php echo $date->format('d F Y, H:i:s'); ?></b></div>
                                        <button id="payment-confirmation" class="btn btn-info btn-sm">Konfirmasi Pembayaran</button>
                                    <?php else: ?>
                                        <div class="alert alert-info">Konfirmasi pembayaran sudah dilakukan, silahkan tunggu pengecekan pembayaran dari kami.</div>
                                    <?php endif; ?>
                                </div>
                                <?php endif; ?>
                            </div>
                        </div>
                	</div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal" id="modal-order-confirmation" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <form id="form-order-confirmation">
                    <div class="modal-header">
                        <h5 class="modal-title">Kirim Bukti Pembayaran</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <b>Silahkan masukan file</b>
                        <br />
                        <input type="file" name="order-confirmation-file" id="order-confirmation-file">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        <button data-id="<?php echo $transaction->id; ?>" type="submit" class="btn btn-primary submit-order-confirmation">Kirim</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>
