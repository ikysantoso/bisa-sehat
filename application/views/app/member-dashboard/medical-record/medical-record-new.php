<div class="content-page">
    <!-- Page Content -->
    <div class="health-check container">
        <div class="row">
            
            <?php include VIEWPATH.'/app/member-dashboard/dashboard-nav.php' ?>

            <div class="col-md-12 medical-record">
                <div class="nav-content">
                    <div class="d-flex mb-3 justify-content-between align-items-center">
                        <h3 class="mb-0"><?php echo $page_title; ?></h3>
                    </div>

                    <form method="post" action="<?=base_url('member/medical_record/save')?>" enctype="multipart/form-data">
                        <input type="hidden" name="id"/>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Tinggi Badan</label>
                                    <div class="input-group mb-3">
                                        <input class="form-control" type="number" name="height" required="required"/>
                                        <div class="input-group-append">
                                            <span class="input-group-text">cm</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Berat Badan</label>
                                    <div class="input-group mb-3">
                                        <input class="form-control" type="number" name="weight" required="required"/>
                                        <div class="input-group-append">
                                            <span class="input-group-text">kg</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Tekanan darah atas (sistole)</label>
                                    <div class="input-group mb-3">
                                        <input class="form-control" type="number" name="sistole"/>
                                        <div class="input-group-append">
                                            <span class="input-group-text">mmHg</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Tekanan darah bawah (diastole)</label>
                                    <div class="input-group mb-3">
                                        <input class="form-control" type="number" name="diastole"/>
                                        <div class="input-group-append">
                                            <span class="input-group-text">mmHg</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        
                        <div class="form-group">
                            <label>Glukosa darah sewaktu (gula darah sewaktu)</label>
                            <div class="input-group mb-3">
                                <input class="form-control" type="number" name="blood_sugar_level"/>
                                <div class="input-group-append">
                                    <span class="input-group-text">mg/dL</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Kolesterol total</label>
                            <div class="input-group mb-3">
                                <input class="form-control" type="number" name="cholesterol_level"/>
                                <div class="input-group-append">
                                    <span class="input-group-text">mg/dL</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Asam Urat</label>
                            <div class="input-group mb-3">
                                <input class="form-control" type="number" step="0.1" name="uric_acid_level"/>
                                <div class="input-group-append">
                                    <span class="input-group-text">mg/dL</span>
                                </div>
                            </div>
                            <small class="form-text text-muted">jika hasil desimal, tuliskan dengan menggunakan . (titik). Misal : 4.7</small>
                        </div>

                        <!-- <div class="form-group">
                            <label>Lampiran (optional)</label>
                            <div class="input-group mb-3">
                                <input class="form-control" type="file" name="attachment"/>
                            </div>
                            <small class="form-text text-muted">Lampirkan file (.jpg/.pdf max 2Mb) hasil pemeriksaan kesehatan dari tempat lain untuk keabsahaan data, data anda akan diverifikasi oleh admin bisa sehat.</small>
                        </div> -->
                        
                        <div class="text-center">
                            <a href="<?php echo base_url('member/medical_record'); ?>" class="btn btn-secondary"><i class="fas fa-chevron-left"></i> Kembali</a>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>