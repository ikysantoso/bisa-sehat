<!DOCTYPE html>
<html>
<head>
	<title>RANGKUMAN DATA MEDIS</title>
	<style type="text/css">
			.table{
                border-collapse: collapse;
                width: 100%;
            }
            .table,
            .table th,
            .table td,
            {
                padding: 10px;
                border: 1px solid #ddd;
            }
            
            .table-striped tr:nth-child(even) {
              background-color: #f2f2f2;
            }
            .text-danger{
            	color: red;
            }
		
	</style>
</head>
<body>
	<div style="display: flex; justify-content: space-between;">
		<img src="<?php echo base_url('assets/images/BISA.png'); ?>" alt="" style="height:70px; width:70px; margin-left:90px; margin-top: 25px; position:absolute;">
		<div style="font-family:Roboto; font-weight:bold;">
			<h2 align="center">RANGKUMAN DATA MEDIS</h2>
			<h4 align="center">www.bisa-sehat.com</h4>
		</div>
	</div>
	<?php $user = UserModel::find( $medical_records[0]->user->id ); ?>
	<table class="table">
		<thead>
			<tr style="background-color: #676767;color: #fff;">
				<th align="left" colspan="4">Data Member</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td style="width:20%;border: 0;">Nama Lengkap</td>
				<td style="width:25%;border: 0"><?php echo $user->first_name." ".$user->last_name; ?></td>
				<td style="width:auto;border-right: 0;border-top: 0;border-bottom: 0">Email</td>
				<td style="width:auto;border: 0"><?php echo $user->email; ?></td>
			</tr>
			<tr>
				<td style="width:20%;border: 0;">Gender / Umur</td>
				<td style="width:25%;border: 0;"><?php echo $user->user_meta('gender'); ?> / <?php echo $user->age(); ?> tahun</td>
				<td style="width:auto;border-right: 0;border-top: 0;border-bottom: 0">Golongan Darah</td>
				<td style="width:auto;border: 0"><?php echo $user->user_meta('blood-type'); ?></td>
			</tr>
		</tbody>
	</table>

	<table class="table">
		<?php 
		$total = $medical_records->count(); 
		$rowspan = ($total>1) ? 5 : 4;
		?>
		<thead>
			<tr style="background-color: #676767;color: #fff">
				<?php 
				if ( $total > 1 ) {
					echo '<td>No</td>';
				}
				?>
				<th>Tgl Input</th>
				<th>Berat Badan</th>
				<th>Tek. Darah</th>
				<th>G. Darah</th>
				<th>Kolesterol</th>
				<th>Asam Urat</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($medical_records as $key => $medical_record): ?>
			<?php $problem = []; ?>
			<tr>
				<?php 
				if ( $total > 1 ) {
					echo '<td>'.($key+1).'</td>';
				}
				?>
				<td><?php echo $medical_record->created_at->format('d-m-Y H:i:s'); ?></td>
				<td>
					TB: <span><?php echo $medical_record->height; ?> cm</span><br/>
					BB: <span><?php echo $medical_record->weight; ?> kg</span><br/>
					BB Ideal: <span><?php echo $medical_record->bmi()['min']; ?> s/d <?php echo $medical_record->bmi()['max']; ?> kg</span><br/>
					BMI: <span><?php echo $medical_record->bmi()['bmi']; ?></span><br/>
					Ket: <?php echo health_result_color($medical_record->bmi_scale()['desc']); ?>
					<?php 
                        if($medical_record->bmi_scale()['count'] == 1){
                            $problem[] = 'overweight';
                        }else if($medical_record->bmi_scale()['count'] == 2){
                            $problem[] = 'kegemukan';
                        }
                    ?>
				</td>
				<td>
					<p><?php if ($medical_record->sistole==0){ echo 'Belum ada data';}else{echo $medical_record->sistole; ?>/<?php echo $medical_record->diastole; ?> mmHg</p>
					<?php echo health_result_color($medical_record->blood_pressure_scale()['desc']); } ?>
					<?php 
                    if($medical_record->blood_pressure_scale()['count'] > 0){
                    	$problem[] = 'hipertensi';
                    }
					?>
				</td>
				<td>
					<p><?php if ($medical_record->blood_sugar_level==0){ echo 'Belum ada data';}else{echo $medical_record->blood_sugar_level; ?> mg/dL</p>
					<?php echo health_result_color($medical_record->blood_sugar_level_scale()['desc']); } ?>
					<?php 
                    if($medical_record->blood_sugar_level_scale()['count'] > 0){
                    	$problem[] = 'diabetes';
                    }
					?>
				</td>
				<td>
					<p><?php if ($medical_record->cholesterol_level==0){ echo 'Belum ada data';}else{echo $medical_record->cholesterol_level; ?> mg/dL</p>
					<?php 
                    if($medical_record->cholesterol_level_scale()['count'] > 0){
                    	$problem[] = 'kolesterol';
                    }
					?>
					<?php echo health_result_color($medical_record->cholesterol_level_scale()['desc']); } ?>
				</td>
				<td>
                    <p><?php if ($medical_record->uric_acid_level==0){ echo 'Belum ada data';}else{echo $medical_record->uric_acid_level; ?> mg/dL</p>
                    <?php echo health_result_color($medical_record->uric_acid_level_scale()['desc']); } ?>
					<?php 
                    if($medical_record->uric_acid_level_scale()['count'] > 0){
                    	$problem[] = 'asam urat';
                    }
					?>
				</td>
			</tr>
			<tr>
				<td colspan="2">Kesimpulan</td>
				<td colspan="<?php echo $rowspan; ?>">
					<?php 
                    $kesimpulan = get_kesimpulan($problem);
                    echo $kesimpulan;
					?>
				</td>
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
</body>
</html>