<style>
    table {
    border: 1px solid #ccc;
    border-collapse: collapse;
    margin: 0;
    padding: 0;
    width: 100%;
    table-layout: fixed;
    }

    table caption {
    font-size: 1.5em;
    margin: .5em 0 .75em;
    }

    /* table tr {
    background-color: #f8f8f8;
    border: 1px solid #ddd;
    padding: .35em;
    } */

    table th,
    table td {
    text-align: left;
    }

    table th {
        font-size: 15px;
        font-weight: bold;
        font-family: Poppins;
    }

    @media screen and (max-width: 600px) {
    table {
        border: 0;
    }

    table caption {
        font-size: 1.3em;
    }
    
    table thead {
        border: none;
        clip: rect(0 0 0 0);
        height: 1px;
        margin: -1px;
        overflow: hidden;
        padding: 0;
        position: absolute;
        width: 1px;
    }
    
    table tr {
        background-color: #f8f8f8;
        border-bottom: 3px solid #ddd;
        display: block;
        margin-bottom: .625em;
    }
    
    table td {
        border-bottom: 1px solid #ddd;
        display: block;
        font-size: .8em;
        text-align: right;
    }
    
    table td::before {
        /*
        * aria-label has no advantage, it won't be read inside a table
        content: attr(aria-label);
        */
        content: attr(data-label);
        float: left;
        font-weight: bold;
        text-transform: uppercase;
    }
    
    table td:last-child {
        border-bottom: 0;
    }
    }
</style>

<div class="content-page">
    <!-- Page Content -->
    <div class="health-check container">
        <div class="row">
            
            <?php include VIEWPATH.'/app/member-dashboard/dashboard-nav.php' ?>

            <div class="medical-record col-md-12">
              <?php include VIEWPATH.'/app/member-dashboard/health-nav.php'; ?>
                <div class="nav-content">
                    <div class="d-flex flex-column mb-3">
                        <div>
                            <h3 class="mt-2 mb-3"><?php echo $page_title; ?></h3>
                        </div>
                        <div>
                            <a href="<?php echo base_url('member/medical_record/new') ?>" class="import-data">Tambah Data</a>
                            <?php if($medical_records->count() > 0): ?>
                            <a href="<?php echo base_url('member/medical_record/download'); ?>" class="import-data">Download Semua</a>
                            <?php endif; ?>
                        </div>
                    </div>
                    <?php 
                        if ( $this->session->flashdata('message') && $this->session->flashdata('status') ) {
                            echo '<div class="alert alert-'.$this->session->flashdata('status').'">'.$this->session->flashdata('message').'</div>';
                        }
                    ?>
                    <?php if($medical_records->count() > 0): ?>
                    <div class="table-responsive">
                        <table id="table-medical-record" class="table table-striped">
                            <thead>
                                <tr>
                                    <th width="5%">No</th>
                                    <th width="15%">Tanggal Input</th>
                                    <th width="14%">Berat Badan</th>
                                    <th width="14%">Tekanan Darah</th>
                                    <th width="14%">Gula Darah</th>
                                    <th width="14%">Kolesterol</th>
                                    <th width="14%">Asam Urat</th>
                                    <th width="10%">#</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no=$offset+1; foreach ($medical_records as $key => $medical_record) : 
                                    $problem = [];
                                    ?>
                                <tr>
                                    <td data-label="No"><?php echo ($no++); ?></td>
                                    <td data-label="Tanggal Input"><?php echo $medical_record->created_at; ?></td>
                                    <td data-label="Berat Badan">Body Mass Index
                                        <div class="d-flex justify-content-between align-items-center">
                                            <b>TB</b>
                                            <div class="border-bottom flex-fill mx-2"></div>
                                            <span><?php echo $medical_record->height; ?> cm</span>
                                        </div>
                                        <div class="d-flex justify-content-between align-items-center">
                                            <b>BB</b>
                                            <div class="border-bottom flex-fill mx-2"></div>
                                            <span><?php echo $medical_record->weight; ?> kg</span>
                                        </div>
                                        <div class="d-flex justify-content-between align-items-center">
                                            <b>BB Ideal</b>
                                            <div class="border-bottom flex-fill mx-2"></div>
                                            <span><?php echo $medical_record->bmi()['min']; ?> s/d <?php echo $medical_record->bmi()['max']; ?> kg</span>
                                        </div>
                                        <div class="d-flex justify-content-between align-items-center">
                                            <b>BMI</b>
                                            <div class="border-bottom flex-fill mx-2"></div>
                                            <span><?php echo $medical_record->bmi()['bmi']; ?></span>
                                        </div>
                                        <div class="d-flex justify-content-between align-items-center">
                                            <b>Keterangan</b>
                                            <div class="border-bottom flex-fill mx-2"></div>
                                            <span class="badge badge-default"><?php echo health_result_color($medical_record->bmi_scale()['desc']); ?></span>
                                        </div>
                                        <?php 
                                        if($medical_record->bmi_scale()['count'] > 0){
                                            $problem[] = $medical_record->bmi_scale()['desc'];
                                        }
                                        ?>
                                    </td>
                                    
                                    <td data-label="Tekanan Darah">
                                        <p><?php if($medical_record->sistole==0){ echo 'Belum ada data';}else{echo $medical_record->sistole; ?>/<?php echo $medical_record->diastole; ?> mmHg</p>
                                        <span class="badge badge-default"><?php echo health_result_color($medical_record->blood_pressure_scale()['desc']); }?></span>
                                        <?php 
                                        if($medical_record->blood_pressure_scale()['count'] > 0){
                                            $problem[] = 'hipertensi';
                                        }
                                        ?>
                                    </td>
                                    <td data-label="Gula Darah">
                                        <p><?php if($medical_record->blood_sugar_level==0){ echo 'Belum ada data';}else{echo $medical_record->blood_sugar_level.' mg/dL'; ?></p>
                                        <span class="badge badge-default"><?php echo health_result_color($medical_record->blood_sugar_level_scale()['desc']); }?></span>
                                        <?php 
                                        if($medical_record->blood_sugar_level_scale()['count']  == 1){
                                            $problem[] = 'diabetes';
                                        }else if($medical_record->blood_sugar_level_scale()['count'] == 2){
                                            $problem[] = 'hipoglikemik';
                                        }
                                        ?>
                                    </td>
                                    <td data-label="Kolesterol">
                                        <p><?php if ($medical_record->cholesterol_level==0){ echo 'Belum ada data';}else{ echo $medical_record->cholesterol_level.' mg/dL'; ?></p>
                                        <span class="badge badge-default"><?php echo health_result_color($medical_record->cholesterol_level_scale()['desc']); }?></span>
                                        <?php 
                                        if($medical_record->cholesterol_level_scale()['count'] > 0){
                                            $problem[] = 'kolesterol';
                                        }
                                        ?>
                                    </td>
                                    <td data-label="Asam Urat">
                                        <p><?php if($medical_record->uric_acid_level==0){ echo 'Belum ada data';}else{echo $medical_record->uric_acid_level.'mg/dL'; ?> </p>
                                        <span class="badge badge-default"><?php echo health_result_color($medical_record->uric_acid_level_scale()['desc']); }?></span>
                                        <?php 
                                        if($medical_record->uric_acid_level_scale()['count'] > 0){
                                            $problem[] = 'asam urat';
                                        }
                                        ?>
                                    </td>
                                    <td data-label="#">
                                        <a href="<?php echo base_url('member/medical_record/download/'.$medical_record->id); ?>" class="btn btn-sm btn-secondary"><i class="fas fa-file-download"></i></a>                                   
                                    <!-- <?php 
                                        if($medical_record->status==1){?>
                                        <a target="_blank" href="<?php echo base_url('assets/rekam_medis/'.$medical_record->attachment); ?>" class="badge badge-success"><i class="fas fa-paperclip"></i> verified</a>
                                    <?php }else if($medical_record->status==0 and $medical_record->attachment!=''){?>
                                        <a target="_blank" href="<?php echo base_url('assets/rekam_medis/'.$medical_record->attachment); ?>" class="badge badge-info"><i class="fas fa-paperclip"></i> process</a>
                                    <?php }else{?>
                                    <?php }?> -->
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">Kesimpulan</td>
                                    <td colspan="6">
                                        <?php 
                                        $kesimpulan = get_kesimpulan($problem);
                                        echo $kesimpulan;
                                        ?>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
            <center><p class="pages justify-content-center"> <?php echo $links; ?></p></center>
                    </div>
                    <?php else: ?>
                    <div class="alert alert-warning" style="margin-bottom:100px;">Belum ada data test kesehatan</div>    
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- <div class="modal" id="modal-medical-record" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form id="form-medical-record">
                <input type="hidden" name="id"/>
                <div class="modal-header">
                    <h5 class="modal-title">Tambah Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Tinggi Badan</label>
                                <div class="input-group mb-3">
                                    <input class="form-control" type="number" name="height" required="required"/>
                                    <div class="input-group-append">
                                        <span class="input-group-text">cm</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Berat Badan</label>
                                <div class="input-group mb-3">
                                    <input class="form-control" type="number" name="weight" required="required"/>
                                    <div class="input-group-append">
                                        <span class="input-group-text">kg</span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Tekanan darah atas (sistole)</label>
                                <div class="input-group mb-3">
                                    <input class="form-control" type="number" name="sistole"/>
                                    <div class="input-group-append">
                                        <span class="input-group-text">mmHg</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Tekanan darah bawah (diastole)</label>
                                <div class="input-group mb-3">
                                    <input class="form-control" type="number" name="diastole"/>
                                    <div class="input-group-append">
                                        <span class="input-group-text">mmHg</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
                    <div class="form-group">
                        <label>Glukosa darah sewaktu (gula darah sewaktu)</label>
                        <div class="input-group mb-3">
                            <input class="form-control" type="number" name="blood_sugar_level"/>
                            <div class="input-group-append">
                                <span class="input-group-text">mg/dL</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Kolesterol total</label>
                        <div class="input-group mb-3">
                            <input class="form-control" type="number" name="cholesterol_level"/>
                            <div class="input-group-append">
                                <span class="input-group-text">mg/dL</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Asam Urat</label>
                        <div class="input-group mb-3">
                            <input class="form-control" max="9" type="number" step="0.1" name="uric_acid_level"/>
                            <div class="input-group-append">
                                <span class="input-group-text">mg/dL</span>
                            </div>
                        </div>
                        <small class="form-text text-muted">jika hasil desimal, tuliskan dengan menggunakan . (titik). Misal : 4.7</small>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div> -->
