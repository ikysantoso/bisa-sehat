<div class="content-page">
    <!-- Page Content -->
    <div class="page-heading header-text" style="background-image:url(<?php echo base_url('assets/images/placeholder_hero.jpg'); ?>);"></div>
    <div class="health-check container">
        <div class="row">
            
            <?php include VIEWPATH.'/app/member-dashboard/dashboard-nav.php' ?>

            <div class="col-md-12">
                <div class="nav-content">
                    <div class="d-flex mb-3 justify-content-between align-items-center">
                        <h3 class="mb-0"><?php echo $page_title; ?></h3>
                        <!-- <h5 class="mb-0"><div class="alert alert-warning" role="alert">
                            Total Voucher : <?php echo count($vouchers); ?>
                        </div></h5> -->
                    </div>
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                          <a class="nav-link active" data-toggle="tab" href="#available">Available</a>
                      </li>
                      <li class="nav-item">
                          <a class="nav-link" data-toggle="tab" href="#soon">Soon</a>
                      </li>
                      <li class="nav-item">
                          <a class="nav-link" data-toggle="tab" href="#used">Used</a>
                      </li>
                      <li class="nav-item">
                          <a class="nav-link" data-toggle="tab" href="#expired">Expired</a>
                      </li>
                  </ul>

                  <div class="tab-content">
                    <div id="available" class="container tab-pane active"><br>
                      <h3>Available</h3>
                      <?php if (count($vouchers_available) > 0): ?>
                        <div class="table-responsive">
                            <table id="table-voucher" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Name</th>
                                        <th>Nominal</th>
                                        <th>Start Date</th>
                                        <th>Expired Date</th>
                                        <th>Voucher Code</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($vouchers_available as $key => $voucher) : ?>
                                            <tr>
                                                <td><?php echo $key+1; ?></td>
                                                <td><?php echo $voucher->name ?></td>
                                                <td><?php echo rupiah($voucher->nominal); ?></td>
                                                <td><?php echo format_date($voucher->start_date, 'd F Y ,  H:iA'); ?></td>
                                                <td><?php echo format_date($voucher->expired, 'd F Y ,  H:iA'); ?></td>
                                                <td><?php echo $voucher->voucher_code ?></td>
                                            </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                        <?php else: ?>
                            <div class="alert alert-warning">Belum ada data voucher</div>    
                        <?php endif; ?>  
                    </div>
                    <div id="soon" class="container tab-pane"><br>
                      <h3>Soon</h3>
                      <?php if (count($vouchers_soon) > 0): ?>
                        <div class="table-responsive">
                            <table id="table-voucher" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Name</th>
                                        <th>Nominal</th>
                                        <th>Start Date</th>
                                        <th>Expired Date</th>
                                        <th>Voucher Code</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($vouchers_soon as $key => $voucher) : ?>
                                            <tr>
                                                <td><?php echo $key+1; ?></td>
                                                <td><?php echo $voucher->name ?></td>
                                                <td><?php echo rupiah($voucher->nominal); ?></td>
                                                <td><?php echo format_date($voucher->start_date, 'd F Y ,  H:iA'); ?></td>
                                                <td><?php echo format_date($voucher->expired, 'd F Y ,  H:iA'); ?></td>
                                                <td><?php echo $voucher->voucher_code ?></td>
                                            </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                        <?php else: ?>
                            <div class="alert alert-warning">Belum ada data voucher</div>    
                        <?php endif; ?>  
                    </div>
                    <div id="used" class="container tab-pane"><br>
                      <h3>Used</h3>
                      <?php if (count($vouchers_used) > 0): ?>
                        <div class="table-responsive">
                            <table id="table-voucher" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Name</th>
                                        <th>Nominal</th>
                                        <th>Start Date</th>
                                        <th>Expired Date</th>
                                        <th>Voucher Code</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($vouchers_used as $key => $voucher) : ?>
                                        <tr>
                                            <td><?php echo $key+1; ?></td>
                                            <td><?php echo $voucher->name ?></td>
                                            <td><?php echo rupiah($voucher->nominal); ?></td>
                                            <td><?php echo format_date($voucher->start_date, 'd F Y ,  H:iA'); ?></td>
                                            <td><?php echo format_date($voucher->expired, 'd F Y ,  H:iA'); ?></td>
                                            <td><?php echo $voucher->voucher_code ?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                        <?php else: ?>
                            <div class="alert alert-warning">Belum ada data voucher</div>    
                        <?php endif; ?>  
                    </div>

                    <div id="expired" class="container tab-pane"><br>
                      <h3>Expired</h3>
                      <?php if (count($vouchers_expired) > 0): ?>
                        <div class="table-responsive">
                            <table id="table-voucher" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Name</th>
                                        <th>Nominal</th>
                                        <th>Start Date</th>
                                        <th>Expired Date</th>
                                        <th>Voucher Code</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($vouchers_expired as $key => $voucher) : ?>
                                            <tr>
                                                <td><?php echo $key+1; ?></td>
                                                <td><?php echo $voucher->name ?></td>
                                                <td><?php echo rupiah($voucher->nominal); ?></td>
                                                <td><?php echo format_date($voucher->start_date, 'd F Y ,  H:iA'); ?></td>
                                                <td><?php echo format_date($voucher->expired, 'd F Y ,  H:iA'); ?></td>
                                                <td><?php echo $voucher->voucher_code ?></td>
                                            </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                        <?php else: ?>
                            <div class="alert alert-warning">Belum ada data voucher</div>    
                        <?php endif; ?>  
                    </div>

              </div>
          </div>
      </div>
  </div>
</div>
