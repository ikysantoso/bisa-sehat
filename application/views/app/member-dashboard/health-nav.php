<div class="container d-none d-lg-block d-md-block">
   <div class="row d-flex justify-content-center">
      <div class="nav-tab d-flex justify-content-center" style="gap:20px;" role="group" aria-label="Basic example">  
         <a href="<?php echo base_url('member/medical_record') ?>" class="nav-link <?php echo ($page == 'member-medical-record') ? 'active' : ''; ?>" data-toggle="tab">Rekam Medis</a>
         <a href="<?php echo base_url('member/fitness') ?>" class="nav-link <?php echo ($page == 'member-fitness') ? 'active' : ''; ?>" data-toggle="tab">Tes Kebugaran</a>
         <a href="<?php echo base_url('member/healthy/status') ?>" class="nav-link <?php echo ($page == 'member-healthy-status') ? 'active' : ''; ?>" data-toggle="tab">Status Kesehatan</a>
      </div>
   </div>
</div>

<div class="container d-lg-none d-md-none">
   <div class="row">
      <div class="swiper swiper-health">
         <div class="swiper-wrapper" role="group" aria-label="Basic example">
            <div class="nav-tab swiper-slide">
               <a href="<?php echo base_url('member/medical_record') ?>" class="nav-link <?php echo ($page == 'member-medical-record') ? 'active' : ''; ?>" data-toggle="tab">Rekam Medis</a>
            </div>
            <div class="nav-tab swiper-slide">
               <a href="<?php echo base_url('member/fitness') ?>" class="nav-link <?php echo ($page == 'member-fitness') ? 'active' : ''; ?>" data-toggle="tab">Tes Kebugaran</a>
            </div>
            <div class="nav-tab swiper-slide">
               <a href="<?php echo base_url('member/healthy/status') ?>" class="nav-link <?php echo ($page == 'member-healthy-status') ? 'active' : ''; ?>" data-toggle="tab">Status Kesehatan</a>
            </div>
         </div>
      </div>
   </div>
</div>


            
