<div class="content-page">
    <div id="modal-address" class="modal" tabindex="-1" style="z-index: 9999999">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <form id="form-address">
                    <input type="hidden" name="id"/>
                    <div class="modal-header">
                        <h5 class="modal-title">Tambah Alamat</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Nama Alamat</label>
                                    <input required="required" class="form-control" type="text" placeholder="Contoh: Alamat Kantor/Alamat Rumah" name="name"/>
                                </div>
                                <div class="form-group">
                                    <label>Nama Penerima</label>
                                    <input class="form-control" type="text" placeholder="Nama Penerima" name="receiver_name" required="required" />
                                </div>
                                <div class="form-group">
                                    <label>Phone</label>
                                    <input class="form-control" type="text" placeholder="Nomor telepon" name="phone" required="required" />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <h5 class="mb-3">Alamat Tempat Tinggal</h5>
                                <div class="form-group">
                                    <label>Provinsi</label>
                                    <input required="required" type="text" data-url="<?php echo base_url('api/wilayah/province'); ?>" data-load-once="true" name="province" />
                                </div>
                                <div class="form-group">
                                    <label>Kabupaten/Kota</label>
                                    <input required="required" type="text" class="form-control" disabled="disabled" name="district" />
                                </div>
                                <div class="form-group">
                                    <label>Kecamatan</label>
                                    <input required="required" type="text" class="form-control" disabled="disabled" name="sub-district" />
                                </div>
                                <div class="form-group">
                                    <label>Kelurahan/Desa</label>
                                    <input required="required" type="text" class="form-control" disabled="disabled" name="village" />
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Kodepos</label>
                                            <input required="required" type="number" min="1" max="99999" minlength="5" maxlength="5" class="form-control" disabled="disabled" name="zipcode" />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Alamat Utama?</label><br/>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input value="yes" required="required" type="radio" id="customRadioInline1" name="is_primary_address" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadioInline1">Ya</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input value="no" required="required" type="radio" id="customRadioInline2" name="is_primary_address" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadioInline2">Tidak</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Alamat</label>
                                    <textarea style="min-height: 103px;" required="required" class="form-control" placeholder="Nama Jalan/RT/RW" disabled="disabled" name="address"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Web View -->
        <div class="container d-none d-md-block d-lg-block d-xl-block">
            <div class="account-profile row p-3">
                <div class="col-md-12 mb-3 d-flex justify-content-start">
                    <h3 class="">Biodata</h3>
                    <?php if($already_employee['success'] == 'true'): ?>
                    <?php elseif(isset($exist_request) && $exist_request['success'] == 'true'): ?>
                    <?php else: ?>
                        <a class="ml-2 btn btn-warning btn-sm" href="<?php echo base_url('member/profile/edit'); ?>">Edit</a>
                    <?php endif; ?>
                </div>
                <div class="col-lg-3 form-group">
                    <label>Username</label>
                    <input required="required" type="text" class="form-control" disabled="disabled" value="<?php echo $this->ion_auth->user()->row()->username ?>" />
                </div>
                <div class="col-lg-3 form-group">
                    <label>Email</label>
                    <input required="required" type="text" class="form-control" disabled="disabled" value="<?php echo $this->ion_auth->user()->row()->email ?>" />
                </div>
                <?php
                    $user_meta_data = [];
                    foreach ($user_info->user_metas as $key => $user_meta) {
                    $user_meta_data[ $user_meta->meta_key ] = $user_meta->meta_value;
                    }
                ?>
                <div class="col-lg-3 form-group">
                    <label>NIK</label>
                    <input required="required" type="text" class="form-control" disabled="disabled" value="<?php echo (isset($user_meta_data['nik'])) ? $user_meta_data['nik'] : '-'; ?>" />
                    <small class="form-text text-muted">* Nik/No. KTP bersifat optional dapat diisi ataupun tidak</small>
                </div>
                <div class="col-lg-3 form-group">
                    <label>Nama Lengkap</label>
                    <input required="required" type="text" class="form-control" disabled="disabled" value="<?php echo $user_info->first_name." ".$user_info->last_name ?>" />
                </div>
                <div class="col-lg-3 form-group">
                    <label>Jenis Kelamin</label>
                    <input required="required" type="text" class="form-control" disabled="disabled" value="<?php echo (isset($user_meta_data['gender'])) ? $user_meta_data['gender'] : '-'; ?>" />
                </div>
                <div class="col-lg-3 form-group">
                    <label>Tanggal Lahir</label>
                    <input required="required" type="text" class="form-control" disabled="disabled" value="<?php echo(isset($user_meta_data['date-birth'])) ? date('d/m/Y', strtotime($user_meta_data['date-birth'])) : '-'; ?>" />
                </div>
                <div class="col-lg-3 form-group">
                    <label>Golongan Darah</label>
                    <input required="required" type="text" class="form-control" disabled="disabled" value="<?php echo (isset($user_meta_data['blood-type'])) ? $user_meta_data['blood-type'] == "None" ? "Tidak tahu" : $user_meta_data['blood-type'] : '-'; ?>" />
                </div>
                <div class="col-lg-3 form-group">
                    <label>Telepon</label>
                    <input required="required" type="text" class="form-control" disabled="disabled" value="<?php echo $user_info->phone ?>" />
                    <?php
                        if ($user_info->phone_verified == 0):
                    ?>
                        <input id="hidden_username_for_phone_activation" required="required" type="text" class="form-control" hidden value="<?php echo $this->ion_auth->user()->row()->username ?>"/>
<!--                        <button id="request_phone_activation_button" type="button" class="btn btn-primary mt-2" data-toggle="modal" data-target="#activation-phone-modal">Aktivasi</button>-->
                    <?php endif; ?>
                </div>
                <div class="col-lg-3 form-group">
                    <label>Pekerjaan</label>
                    <input required="required" type="text" class="form-control" disabled="disabled" value="<?php echo (isset($user_meta_data['job'])) ? $user_meta_data['job'] : '-'; ?>" />
                </div>
                <div class="col-lg-3 form-group">
                    <label>Agama</label>
                    <input required="required" type="text" class="form-control" disabled="disabled" value="<?php echo (isset($user_meta_data['religion'])) ? $user_meta_data['religion'] : '-'; ?>" />
                </div>
                <div class="col-lg-3 form-group">
                    <label>Alamat</label>
                    <textarea disabled="disabled" class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="<?php if ( $primary_address ) 
                    { if($primary_address->village_id==null and $primary_address->subdistrict_id==null){}else{if ( $primary_address->village_id == null ) {
                        echo
                        $primary_address->address.' '.
                        $primary_address->village_text.' '.
                        $sub_districts->sub_districts_name.' '.
                        $districts->districts_name.' '.
                        $province->province_name;
                        }else{
                        echo
                        $primary_address->address.' '.
                        $village->ward_name.' '.
                        $sub_districts->sub_districts_name.' '.
                        $districts->districts_name.' '.
                        $province->province_name;
                        }}
                    }
                    ?>"></textarea>
                </div>
            </div>
            <div class="account-profile row">
                <div class="col">
                   <?php if($delete_request['success']==true): ?>
                       <div class="d-flex align-content-between align-items-center mb-3">
                           <h5 class="flex-fill mt-3">Perusahaan / Institusi Terdaftar</h5>
                       </div>
                        <ul id="company-registered" class="list-group" style="margin-bottom:70px;">
                            <li class="list-group-item d-flex align-content-between align-items-center">
                                <div class="flex-fill">
                                   <p><?php echo $delete_request['data']['company']['companyName']; ?></p>
                                   <span class="badge badge-sm badge-light text-sm text-muted"><?php echo $delete_request['data']['division']; ?></span>
                               </div>
                                <div class="btn-group ml-4" role="group" aria-label="Basic example">
    <badge class="badge badge-sm badge-warning text-muted">Menunggu Keluar</badge>
                                </div>
                            </li>
                        </ul>
                   <?php elseif($already_employee['success']==true): ?>
                       <div class="d-flex align-content-between align-items-center mb-3">
                           <h5 class="flex-fill mt-3">Perusahaan / Institusi Terdaftar</h5>
                       </div>
                        <ul id="company-registered" class="list-group" style="margin-bottom:70px;">
                            <li class="list-group-item d-flex align-content-between align-items-center">
                                <div class="flex-fill">
                                   <p><?php echo $already_employee['data']['company']['companyName']; ?></p>
                                   <span class="badge badge-sm badge-light text-sm text-muted"><?php echo $already_employee['data']['division']; ?></span>
                                   <input type="hidden" id="already_company" value="<?php echo $already_employee['data']['company']['companyId']; ?>">
                                   <input type="hidden" id="already_division" value="<?php echo $already_employee['data']['division']; ?>">
                               </div>
                                <div class="btn-group ml-4" role="group" aria-label="Basic example">
    <a class="btn btn-sm btn-success text-white" href="#">Terdaftar</a>
    <a class="btn btn-sm btn-danger text-white" data-toggle="modal" data-target="#modal-delete-company">Keluar</a>
                                </div>
                            </li>
                        </ul>
                    <?php elseif(isset($exist_request) && $exist_request['success']): ?>
                       <div class="d-flex align-content-between align-items-center mb-3">
                           <h5 class="flex-fill mt-3">Perusahaan / Institusi Terdaftar</h5>
                       </div>
                        <ul id="company-registered" class="list-group" style="margin-bottom:70px;">
                            <li class="list-group-item d-flex align-content-between align-items-center">
                                <div class="flex-fill">
                                    <p><?php echo $exist_request['data']['company']['companyName']; ?></p>
                                </div>
                                <div class="btn-group ml-4" role="group" aria-label="Basic example">
                                   <span class="badge badge-light">Request</span>
                               </div>
                           </li>
                        </ul>
                   <?php else: ?>
                       <div class="d-flex align-content-between align-items-center mb-3">
                           <h5 class="flex-fill mt-3">Perusahaan / Institusi Terdaftar</h5>
                           <?php if($nik==''){?>
                           <button class="btn btn-sm btn-primary mt-3" id="nik_null">Daftar Perusahaan / Institusi</button>
                           <?php }else{?>
                           <button class="btn btn-sm btn-primary mt-3" data-toggle="modal" data-target="#modal-company">Daftar Perusahaan / Institusi</button>
                           <?php }?>
                       </div>
                        <div class="alert alert-warning" style="margin-bottom:100px;">Belum terdaftar di Perusahaan / Institusi apapun</div>
                    <?php endif; ?>

                    <!-- <div class="d-flex align-content-between align-items-center mb-3">
                        <h5 class="flex-fill mt-3">Data Bank Saya</h5>
                        <button class="btn btn-sm btn-primary mt-3" data-toggle="modal" data-target="#modal-bank">Tambah Bank</button>
                    </div>
                    <?php if($customerbanks->count() > 0): ?>
                        <ul id="bank-accounts" class="list-group" style="margin-bottom:70px;">
                            <?php foreach( $customerbanks as $customerbank ): ?>
                                <li class="list-group-item d-flex align-content-between align-items-center">
                                    <div class="flex-fill">
                                        <p><?php echo $customerbank->name; ?> - <?php echo $customerbank->account_name; ?></p>
                                        <p><?php echo $customerbank->account_number; ?></p>
                                    </div>
                                    <div class="btn-group ml-4" role="group" aria-label="Basic example">
                                        <button type="button" data-id="<?php echo $customerbank->id; ?>" class="btn btn-sm btn-danger btn-delete">Delete</button>
                                    </div>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    <?php else: ?>
                        <div class="alert alert-warning" style="margin-bottom:100px;">Belum ada data bank</div>
                    <?php endif; ?> -->
                </div>
                <div class="col">
                    <div class="d-flex justify-content-between">
                        <h5 class="flex-fill mt-3">Alamat</h5>
                        <div>
                            <button data-toggle="modal" data-target="#modal-address" class="btn btn-sm btn-success py-1 mt-3 ml-5">Tambah Alamat</button>
                        </div>
                    </div>
                    <div class="accordion ml-0 mt-3" id="address-list">
                        <?php if(count($customer_addresses) == 0):?>
                            <div class="alert alert-danger" style="margin-bottom:100px;">
                                Anda belum memasukkan alamat, silahkan klik tambah alamat untuk menambahkan
                             </div>
                        <?php else:?>
                            <ul class="list-group">
                                <?php foreach ($customer_addresses as $key => $customer_address): ?>
                                    <?php if($customer_address->subdistrict_id==null){}else{?>
                                    <li class="list-group-item p-0 d-flex align-items-center">
                                        <a data-id="<?php echo $customer_address->id; ?>" class="address-item d-block p-1 mb-0 flex-fill pl-3" href="#!"><small><?php echo $customer_address->name ?></small></a>
                                        <button data-id="<?php echo $customer_address->id; ?>" type="button" class="btn btn-sm btn-danger px-1 py-0 mr-2 btn-delete"><small><i class="fas fa-trash-alt"></i></small></button>
                                    </li>
                                <?php } ?>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif;?>
                    </div>
                </div>
                <div class="w-100"></div>
            </div>
            <div class="mt-4">
                <a href="<?php echo base_url('auth/logout') ?>">
                    <img src="<?php echo base_url('assets/images/favicon/logout.png'); ?>" style="height: 35px; width: auto;">
                    <span style="font-size: 28px; color: #000000;">Logout</span>
                    <!-- <img src="<?php echo base_url('assets/images/favicon/next.png'); ?>" style="height: 30px; width: auto;"> -->
                </a>
            </div>
        </div>
        
        <!-- Mobile View -->
        <div class="d-md-none d-lg-none d-xl-none">
            <div class="row medical-record mx-auto">
                <div class="col-md-5">
                    <div class="mt-4 mb-3 d-flex justify-content-between">
                        <h3 class="">Biodata</h3>
                        <a href="<?php echo base_url('member/profile/edit'); ?>">
                            <img src="<?php echo base_url('assets/images/favicon/edit.png'); ?>" style="height: 30px; width: auto;">
                        </a>       
                    </div>
                    <div class="form-group">
                        <label>Username</label>
                        <input required="required" type="text" class="form-control" disabled="disabled" value="<?php echo $this->ion_auth->user()->row()->username ?>" />
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input required="required" type="text" class="form-control" disabled="disabled" value="<?php echo $this->ion_auth->user()->row()->email ?>" />
                    </div>
                    <?php
                        $user_meta_data = [];
                        foreach ($user_info->user_metas as $key => $user_meta) {
                        $user_meta_data[ $user_meta->meta_key ] = $user_meta->meta_value;
                        }
                    ?>
                    <div class="form-group">
                        <label>NIK</label>
                        <input required="required" type="text" class="form-control" disabled="disabled" value="<?php echo $nik ; ?>" />
                        <small class="form-text text-muted">* Nik/No. KTP bersifat optional dapat diisi ataupun tidak</small>
                    </div>
                    <div class="form-group">
                        <label>Nama Lengkap</label>
                        <input required="required" type="text" class="form-control" disabled="disabled" value="<?php echo $user_info->first_name." ".$user_info->last_name ?>" />
                    </div>
                    <div class="form-group">
                        <label>Jenis Kelamin</label>
                        <input required="required" type="text" class="form-control" disabled="disabled" value="<?php echo (isset($user_meta_data['gender'])) ? $user_meta_data['gender'] : '-'; ?>" />
                    </div>
                    <div class="form-group">
                        <label>Tanggal Lahir</label>
                        <input required="required" type="text" class="form-control" disabled="disabled" value="<?php echo(isset($user_meta_data['date-birth'])) ? date('d/m/Y', strtotime($user_meta_data['date-birth'])) : '-'; ?>" />
                    </div>
                    <div class="form-group">
                        <label>Golongan Darah</label>
                        <input required="required" type="text" class="form-control" disabled="disabled" value="<?php echo (isset($user_meta_data['blood-type'])) ? $user_meta_data['blood-type'] == "None" ? "Tidak tahu" : $user_meta_data['blood-type'] : '-'; ?>" />
                    </div>
                    <div class="form-group">
                        <label>Telepon</label>
                        <input required="required" type="text" class="form-control" disabled="disabled" value="<?php echo $user_info->phone ?>" />
                    </div>
                    <div class="form-group">
                        <label>Pekerjaan</label>
                        <input required="required" type="text" class="form-control" disabled="disabled" value="<?php echo (isset($user_meta_data['job'])) ? $user_meta_data['job'] : '-'; ?>" />
                    </div>
                    <div class="form-group">
                        <label>Agama</label>
                        <input required="required" type="text" class="form-control" disabled="disabled" value="<?php echo (isset($user_meta_data['religion'])) ? $user_meta_data['religion'] : '-'; ?>" />
                    </div>
                    <div class="form-group">
                        <label>Alamat</label>
                        <textarea disabled="disabled" type="text" class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="<?php if ( $primary_address ) 
                        { if($primary_address->village_id==null and $primary_address->subdistrict_id==null){}else{if ( $primary_address->village_id == null ) {
                        echo
                        $primary_address->address.' '.
                        $primary_address->village_text.' '.
                        $sub_districts->sub_districts_name.' '.
                        $districts->districts_name.' '.
                        $province->province_name;
                        }else{
                        echo
                        $primary_address->address.' '.
                        $village->ward_name.' '.
                        $sub_districts->sub_districts_name.' '.
                        $districts->districts_name.' '.
                        $province->province_name;
                        }}
                        }
                        ?>"></textarea>
                    </div>
                </div>
                <div class="col-md-4">
                    <?php include VIEWPATH.'/app/member-dashboard/account-mobile.php'; ?>
                </div>
            </div>
        </div>
</div>

<div class="modal" id="activation-phone-modal" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="form-activation-phone">
                <div class="modal-header">
                    <h5 class="modal-title">Aktivasi Telepon</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input id="username_phone_activation" required="required" type="text" class="form-control" hidden value="<?php echo $this->ion_auth->user()->row()->username ?>" />
                    <div class="form-group">
                        <label>Kode Aktivasi</label>
                        <input required="required" type="number" class="form-control" id="activation_phone_code"/>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button id="phone_activation_button" type="button" class="btn btn-primary">Activate</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal" id="modal-bank" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form id="form-bank">
                <div class="modal-header">
                    <h5 class="modal-title">Form Akun Bank</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Nama Bank</label>
                        <input class="form-control" type="text" name="name" required="required"/>
                    </div>
                    <div class="form-group">
                        <label>Atas Nama</label>
                        <input class="form-control" type="text" name="account_name" required="required"/>
                    </div>
                    <div class="form-group">
                        <label>Nomor Rekening</label>
                        <input class="form-control" type="number" name="account_number" required="required"/>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal" id="modal-company" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
           <form>
               <div class="modal-header">
                   <h5 class="modal-title">Form Perusahaan / Institusi</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
               </div>
               <div class="modal-body">
                    <div class="form-group">
                        <h5>Pilih Perusahaan / Institusi</h5>
                        <h6 style="margin-top: 2rem; margin-bottom: 2rem;">Silakan memilih Perusahaan / Institusi dan akan dilakukan permintaan pendaftaran sebagai bagian dari Perusahaan / Institusi tersebut</h6>
                    </div>
                    <label>
                        <select id="company" class="form-control" required>
                            <option>-</option>
                           <?php
                               foreach($companies['data'] as $comp => $value):
                                    echo '<option value="'.$value['companyId'].'"> '.$value['companyName'].'</option>';
                                endforeach;
                            ?>
                        </select>
                   </label>
                    <label>
                        <input type="hidden" id="nik" value="<?php echo $nik; ?>">
                    </label>
                    <label>
                        <input type="hidden" id="fullname" value="<?php echo $fullname; ?>">
                    </label>
                    <div class="form-group mt-4">
                        <h6>Divisi/Bagian</h6>
                        <label>
                            <input type="text" id="division">
                        </label>
                    </div>
                    <div class="form-group">
                        <label>
                            <input type="hidden" id="birthdate" value="<?php echo (isset($user_meta_data['date-birth'])) ? str_replace("-","/",$user_meta_data['date-birth']) : '-'; ?>">
                        </label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-confirmation-company" data-dismiss="modal">Request</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal" id="modal-confirmation-company" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form>
                <div class="modal-header">
                    <h5 class="modal-title">Konfirmasi Pendaftaran</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <p style="margin-left: 15px; margin-top: 20px;">Dengan melakukan konfirmasi <br> Anda setuju data kesehatan Anda akan di-berikan ke manajemen
                    Perusahaan / Institusi Anda untuk kepentingan Anda. <br> dan Program Kesehatan Kerja Perusahaan / Institusi Anda.</p>
                <br>
                <p style="margin-left: 15px; margin-bottom: 10px;">Anda pikirkan baik-baik sebelum melakukan konfirmasi.</p>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-primary" id="confirmation-request-company-btn">Konfirmasi</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal" id="modal-delete-company" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form>
                <div class="modal-header">
                    <h5 class="modal-title">Konfirmasi keluar dari Perusahaan / Institusi</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <p style="margin-left: 15px; margin-top: 20px;">Dengan melakukan konfirmasi <br> Anda setuju keluar dari Perusahaan / Institusi.
                <br>
                <p style="margin-left: 15px; margin-bottom: 10px;">Anda pikirkan baik-baik sebelum melakukan konfirmasi.</p>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-primary"  id="delete-request-company-btn">Konfirmasi</button>
                </div>
            </form>
        </div>
    </div>
</div>

