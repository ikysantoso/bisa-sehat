<div class="content-page">
    <div class="container d-flex justify-content-center" style="margin-top: 5rem;">
        <p class="text-center" style="font-weight: bold; font-size: 30pt; font-family: 'Poppins';">Register</p>
    </div>
    	<center>
                        <div id="phone-otp" class="mt-4">
                            <div class="alert alert-danger" id="error" style="display: none; max-width:450px;"></div>
                            <form>
                            	<small class="form-text text-muted my-4">Daftar menggunakan nomor handphone</small>
                                <input type="text" id="number" class="form-control required"  value="+62" placeholder="+62xxx" required="required" style="max-width:450px;" />
                                <div id="recaptcha-container" class="mt-2"></div>
                                <button type="button" id="btn-otp" class="btn btn-otp btn-primary mt-3" onclick="sendOTP();">Send OTP</button>
                            </form>
                        </div>
                        <div class="mt-4" id="phone-verify" style="display: none; ">                            
                            <div class="alert alert-success" id="successAuth" style="display: none; max-width:450px;"></div>
                            <form>
                                <input type="text" id="number-verify" class="form-control required"  value="+62" placeholder="+62xxx" required="required" readonly style="max-width:450px;" />
                            	<label><small>Masukan Kode OTP</small></label>
                                <input type="text" id="verification" class="form-control" placeholder="Verification code"  style="max-width:450px;" >
                                <button type="button" id="btn-verify" class="btn btn-verify btn-success mt-3" onclick="verify()">Verify code</button>
                                <br><a  href="<?php echo base_url('auth/register_p'); ?>"><small>kembali</small></a>
                            </form>
                        </div>
    <div class="login form-member">
        <div class="container register-container">
                            <div class="alert alert-success" id="successOtpAuth" style="display: none; max-width:450px;"></div>
            <form id="form-register">
                <div class="contact-form" id="register"  style="display: none;">
                    <?php
                        if ($this->session->flashdata('message')) {
                            echo '<div class="alert alert-info">'.$this->session->flashdata('message').'</div>';
                            unset($_SESSION['message']);
                        }
                    ?>
                    <div class="row">
                        <div id="default-field" class="col-md-12">
                            <div class="inputset col-md-12">
                                <fieldset>
                                    <input style="border-radius: 10px;" name="phone" id="phone" type="text" minlength="9" class="form-control required" placeholder="+62xxx" required="required" readonly />
                                </fieldset>
                            </div>
                            <div class="inputset col-md-12">
                                <fieldset>
                            	<!-- <label><small>Username</small></label> -->
                                    <input style="border-radius: 10px;" minlength="5" maxlength="20" name="username" type="text" class="form-control required" placeholder="Username" required="required"/>
                                </fieldset>
                                <small class="form-text text-muted">min 5 karakter, max 20 karakter, huruf dan atau angka</small>
                            </div>
                            <div class="inputset col-md-12">
                            	<!-- <label><small>Password</small></label> -->
                                <fieldset>
                                    <input style="border-radius: 10px;" minlength="8" id="new_password" name="password" type="password" class="form-control required" id="password" placeholder="Password" required="required"/>
                                    <h3 style="float: right;margin-top: -30px; padding-right:40px; position: relative;z-index: 2;" toggle="#password-field" class="fa fa-fw fa-eye fa-lg field_icon toggle-password"></h3>
                                    <small class="form-text text-muted">min 8 karakter</small>
                                </fieldset>
                            </div>
                            <div class="inputset col-md-12">
                            	<!-- <label><small>Konfirmasi Password</small></label> -->
                                <fieldset>
                                    <input style="border-radius: 10px;" minlength="8" name="password_confirm" type="password" class="form-control required" id="password_confirm" placeholder="Confirm Password" required="required"/>
                                    <h3 style="float: right;margin-top: -30px; padding-right:40px; position: relative;z-index: 2;" toggle="#password-field" class="fa fa-fw fa-eye fa-lg field_icon toggle-confirm-password"></h3>
                                </fieldset>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12" style="margin-top: 2rem;">
                        <button type="submit" id="form-submit" class="btn login p-2 btn-block" style="background-color: #008080; align-items: center; border-radius: 10px;">
                            <p style="font-weight: bold; font-size: 15pt; color: white;">Register</p>
                        </button>
                                <br><a  href="<?php echo base_url('auth/register_p'); ?>"><small>kembali</small></a>
                    </div>

                    <div class="mt-4 container">
                        <h6 class="text-center">Dengan mendaftar, Anda setuju dengan <a href="<?php echo base_url('Terms/index'); ?>">Syarat dan Ketentuan dari BISA SEHAT</a></h6>
                    </div>

                    <div class="text-center mt-4 container">
                        <p style="font-weight: bold; color: #0DA4C5; font-size: 15pt;">
                            Already have an account?
                        </p>
                        <a href="<?php echo base_url('auth/login'); ?>" style="font-weight: bold; font-size: 15pt;">
                            <b style="color: #008080;">
                                Login
                            </b>
                        </a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
        <!-- Firebase App (the core Firebase SDK) is always required and must be listed first -->
        <script src="https://www.gstatic.com/firebasejs/6.0.2/firebase.js"></script>
        <script>
            var firebaseConfig = {
    apiKey: "AIzaSyDq4QVhUDWXSOo0gZ0Q_qw2-MSLnLQF3c8",
    authDomain: "phone-otp-3569b.firebaseapp.com",
    projectId: "phone-otp-3569b",
    storageBucket: "phone-otp-3569b.appspot.com",
    messagingSenderId: "1066587057219",
    appId: "1:1066587057219:web:6237edd4b3835893fdf8f6",
            };
            firebase.initializeApp(firebaseConfig);
        </script>
        <script type="text/javascript">
            window.onload = function () {
                render();
                    $("#phone-otp").show();
                    $("#phone-verify").hide();
                    $("#register").hide();
                    $("#number").val("+62");
            };
            
            function render() {
                window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container');
                recaptchaVerifier.render();
            }
            function back() {
                    $("#phone-otp").show();
                    $("#phone-verify").hide();
                    $("#register").hide();
            }
            function showverify(number) {
                    $("#phone-verify").show();
                    $("#number-verify").val(''+number);
            }
            function showregist(number) {
                    $("#register").show();
                    $("#phone").val(''+number);
            }
            
            function sendOTP() {
                var number = $("#number").val();
                firebase.auth().signInWithPhoneNumber(number, window.recaptchaVerifier).then(function (confirmationResult) {
                    window.confirmationResult = confirmationResult;
                    coderesult = confirmationResult;
                    console.log(coderesult);
                    $("#successAuth").text("Message sent");
                    $("#successAuth").show();
                    $("#phone-otp").hide();
                    showverify(number);
                }).catch(function (error) {
                    $("#error").text(error.message);
                    $("#error").show();
                    $("#phone-verify").hide();
                    $("#phone-otp").show();
                });
            }
            
            function verify() {
                var code = $("#verification").val();
                var number = $("#number-verify").val();
                coderesult.confirm(code).then(function (result) {
                    var user = result.user;
                    console.log(user);
                    $("#successOtpAuth").text("Phone Number Verify");
                    $("#successOtpAuth").show();
                    $("#phone-verify").hide();
                    showregist(number);
                }).catch(function (error) {
                    $("#error").text(error.message);
                    $("#error").show();
                });
            }
        </script>