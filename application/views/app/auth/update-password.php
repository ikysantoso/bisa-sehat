<div class="content-page">
    <div class="container" style="margin-top: 4rem;">
        <p class="text-center" style="font-weight: bold; font-size: 30pt;">Forgot Password</p>
    </div>
    <div class="login form-member" style="margin-top: 4rem;">
        <div class="container login-container">
            <div class="contact-form">
                <form id="form-update-password" method="post">
                    <input type="hidden" name="forgot_password_code" value="<?php echo $forgot_password_code; ?>" />

                    <div class="inputset">
                        <label for="new_password">New Password</label>
                        <fieldset>
                            <input minlength="8" id="new_password" name="password" type="password" class="form-control required" placeholder="Password" required="required"/>
                            <h3 style="float:right; margin-top:-30px; padding-right:40px; position:relative; z-index: 2;" toggle="#password-field" class="fa fa-fw fa-eye fa-lg field_icon toggle-update-password" style="float:right; margin-top: 3px;"></h3>
                        </fieldset>
                        <small class="form-text text-muted">min 8 karakter</small>
                    </div>

                    <div class="inputset">
                        <label for="password_confirm">Confirm Password</label>
                        <fieldset>
                            <input minlength="8" name="password_confirm" type="password" class="form-control required" id="password_confirm" placeholder="Confirm Password" required="required"/>
                            <h3 style="float:right; margin-top:-30px; padding-right:40px; position:relative; z-index: 2;" toggle="#password-field" class="fa fa-fw fa-eye fa-lg field_icon toggle-reset-password" style="float:right; margin-top: 3px;"></h3>
                        </fieldset>
                    </div>

                    <div style="margin-top: 2rem;">
                        <button type="submit" id="form-submit" class="btn btn-block p-2" style="background-color: #008080; align-items: center;">
                            <p style="font-weight: bold; font-size: 15pt; color: white;">Update Password</p>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
