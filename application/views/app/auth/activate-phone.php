<div class="container" style="margin-top: 4rem;">
    <div class="container">
        <p class="text-center" style="font-weight: bold; font-size: 30pt;">Aktivasi Akun</p>
    </div>
    <div class="container  justify-content-center" >
        <form id="account-activation-form">
            <div class="row">
                <div class="col">
                    <div class="border rounded p-4">
                        <p style="font-weight: bold; font-size: 18pt;">Data Diri</p>
                        <div style="margin-top: 2rem;">
                            <input type="hidden" name="activation_code" value="<?php echo $user_info->activation_code ?>"/>
                            <div class="form-group">
                                <label>Nomor Telepon</label>
                                <input required="required" type="text" class="form-control" name="phone" value="<?php echo $user_info->phone ?>" readonly>
                            </div>
                            <div class="form-group">
                                <label>Username</label>
                                <input required="required" type="text" class="form-control" disabled="disabled" value="<?php echo $user_info->username; ?>" />
                            </div>
                            <div class="form-group">
                                <label>Nama Lengkap</label>
                                <input required="required" type="text" class="form-control" name="fullname"/>
                            </div>
                            <div class="form-group">
                                <label class="d-block">Jenis Kelamin</label>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input value="Laki-Laki" required="required" type="radio" id="laki-Laki" name="gender" class="custom-control-input">
                                    <label class="custom-control-label" for="laki-Laki">Laki-Laki</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input value="Perempuan" required="required" type="radio" id="perempuan" name="gender" class="custom-control-input">
                                    <label class="custom-control-label" for="perempuan">Perempuan</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Tanggal lahir</label>
                                <input required="required" type="date" class="form-control" name="date-birth" placeholder="dd/mm/yyyy"/>
                            </div>
                            <button type="submit" class="btn p-2 rounded" style="background-color: #0E6E83; width: 100%;">
                                <p style="font-weight: bold; font-size: 15pt; color: white;">Aktivasi</p>
                            </button>
                        </div>
                    </div>
                </div>

            </div>
        </form>
    </div>
</div>