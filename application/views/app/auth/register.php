<div class="content-page">
    <div class="container d-flex justify-content-center" style="margin-top: 5rem;">
        <p class="text-center" style="font-weight: bold; font-size: 30pt; font-family: 'Poppins';">Register</p>
    </div>
    <div class="login form-member">
        <div class="container register-container d-flex justify-content-center">
            <div class="contact-form col-md-5">
                <form id="form-register">
                    <?php
                        if ($this->session->flashdata('message')) {
                            echo '<div class="alert alert-info">'.$this->session->flashdata('message').'</div>';
                            unset($_SESSION['message']);
                        }
                    ?>
                    <div id="default-field">
                        <div class="inputset">
                            <fieldset>
                                <input style="border-radius: 10px;" minlength="5" maxlength="20" name="username" type="text" class="form-control required" placeholder="Username" required="required"/>
                            </fieldset>
                            <small class="form-text text-muted">min 5 karakter, max 20 karakter, huruf dan atau angka</small>
                        </div>
                        <div class="inputset">
                            <fieldset>
                                <input style="border-radius: 10px;" name="email" type="email" class="form-control required" placeholder="Email Address" required="required"/>
                            </fieldset>
                        </div>
                        <div class="inputset">
                            <fieldset>
                                <input style="border-radius: 10px;" minlength="8" id="new_password" name="password" type="password" class="form-control required" id="password" placeholder="Password" required="required"/>
                                <h3 style="float: right;margin-top: -30px; padding-right:40px; position: relative;z-index: 2;" toggle="#password-field" class="fa fa-fw fa-eye fa-lg field_icon toggle-password"></h3>
                                <small class="form-text text-muted">min 8 karakter</small>
                            </fieldset>
                        </div>
                        <div class="inputset">
                            <fieldset>
                                <input style="border-radius: 10px;" minlength="8" name="password_confirm" type="password" class="form-control required" id="password_confirm" placeholder="Confirm Password" required="required"/>
                                <h3 style="float: right;margin-top: -30px; padding-right:40px; position: relative;z-index: 2;" toggle="#password-field" class="fa fa-fw fa-eye fa-lg field_icon toggle-confirm-password"></h3>
                            </fieldset>
                        </div>
                    </div>
                    <div style="margin-top: 2rem;">
                        <button type="submit" id="form-submit" class="btn login p-2 btn-block" style="background-color: #008080; align-items: center; border-radius: 10px;">
                            <p style="font-weight: bold; font-size: 15pt; color: white; font-family: 'Poppins';">Register</p>
                        </button>
                    </div>
                    <div class="mt-4 container">
                        <h6 style="font-family: 'Poppins';" class="text-center">Dengan mendaftar, Anda setuju dengan <a href="<?php echo base_url('Terms/index'); ?>">Syarat dan Ketentuan dari BISA SEHAT</a><br>atau</h6>
                    </div>
                    <div class="text-center mt-2 container">
                        <a href="<?php echo base_url('auth/register_p'); ?>" style="font-weight: bold; font-size: 15pt;">
                            <b style="color: #008080;">
                                Buat akun dengan  nomor handphone
                            </b>
                        </a>
                    </div>
                    <div class="text-center mt-2 container d-flex justify-content-center d-flex flex-column ">
                        <p style="font-weight: bold; color: #0DA4C5; font-size: 15pt; font-family: 'Poppins';">
                            Already have an account?
                        </p>
                        <a href="<?php echo base_url('auth/login'); ?>" style="font-weight: bold; font-size: 15pt; font-family: 'Poppins';">
                            <b style="color: #008080;">
                                Login
                            </b>
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
