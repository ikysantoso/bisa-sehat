<div class="content-page">
    <div class="container d-flex justify-content-center" style="margin-top: 5rem;">
        <p class="text-center" style="font-weight: bold; font-size: 30pt; font-family: 'Poppins';">Login</p>
    </div>
    <div class="login form-member">
        <div class="container login-container d-flex justify-content-center">
            <div class="contact-form col-md-5">
                <form id="form-login" method="post">
                    <?php
                        if ($this->session->flashdata('message')) {
                            echo '<div class="alert alert-info">'.$this->session->flashdata('message').'</div>';

                            unset($_SESSION['message']);
                        }
                    ?>

                    <div class="inputset">
                        <div class="hidden" role="alert"></div>
                        <fieldset>
                            <input style="border-radius: 10px;" name="identity" type="text" class="form-control required" id="email" placeholder="E-Mail Address / Username" required="required"/>
                        </fieldset>
                    </div>

                    <div class="inputset">
                        <fieldset>
                            <input style="border-radius: 10px;" name="password" type="password" class="form-control required" id="password" placeholder="Password" required="required"/>
                            <h3 style="float:right; margin-top:-30px; padding-right:40px; position:relative; z-index: 2;" toggle="#password-field" class="fa fa-fw fa-eye fa-lg field_icon toggle-login-password" style="float:right; margin-top: 3px;"></h3>
                        </fieldset>
                    </div>

                    <div style="margin-top: 2rem;">
                        <button type="submit" id="form-submit" class="btn login p-2 btn-block" style="background-color: #008080; align-items: center; border-radius: 10px;">
                            <p style="font-weight: bold; font-size: 15pt; color: white; font-family: 'Poppins';">Login</p>
                        </button>
                    </div>

                    <div class="text-center mt-4">
                        <a href="<?php echo base_url('auth/forgot_password'); ?>" style="font-weight: bold; font-size: 13pt; font-family: 'Poppins';">
                            <b style="color: #008080;">
                                Forgot password
                            </b>
                        </a>
                        <h3 style="margin-top: 15px; font-size: 13pt; font-family: 'Poppins';">Dont have an account? 
                            <a href="<?php echo base_url('auth/register'); ?>" style="font-weight: bold; font-size: 13pt; margin-left: 3px; font-family: 'Poppins';">
                                <b style="color: #008080;">
                                    Register
                                </b>
                            </a> 
                        </h3>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
