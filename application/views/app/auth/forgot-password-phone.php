<div class="content-page">
    <div class="container d-flex justify-content-center" style="margin-top: 5rem;">
        <p class="text-center" style="font-weight: bold; font-size: 30pt; font-family: 'Poppins';">Forgot Password</p>
    </div>
    <div class="login form-member">
        <div class="container login-container d-flex justify-content-center">
                    <?php
                        if ($this->session->flashdata('message')) {
                            $status = ( $this->session->flashdata('status') ) ? $this->session->flashdata('status') : 'info';
                            echo '<div class="alert alert-'.$status.'">'.$this->session->flashdata('message').'</div>';

                            unset($_SESSION['message']);
                        }
                    ?>
                            <div class="alert alert-danger" id="error" style="display: none; max-width:450px;"></div>
                                    <div class="mt-4" id="phone-otp" style=" margin-top: 4rem;">                            
                
                <form>
                    <input type="text" id="number" class="form-control required" value="+62"  placeholder="+62xxx" required="required" style="max-width:450px;" />
                    <div id="recaptcha-container" class="mt-2"></div>
                    <button type="button" id="btn-otp" class="btn btn-otp btn-primary mt-3" onclick="sendOTP();">Kirim OTP</button>

                    <div class="text-center after-submit-btn">
                        <a href="<?php echo base_url('auth/forgot_password'); ?>" style="font-weight: bold; font-size: 12pt;">
                            Gunakan email
                        </a>
                    </div>
                    <div class="text-center after-submit-btn">
                        <a href="<?php echo base_url('auth/login'); ?>" style="font-weight: bold; font-size: 13pt;">
                            <b style="color: #008080;">
                                Back to login
                            </b>
                        </a>
                    </div>
                </form>
                        </div>

                        <div class="mt-4" id="phone-verify" style="display: none; margin-top: 4rem;">                            
                            <div class="alert alert-success" id="successAuth" style="display: none; max-width:450px;"></div>
                            <form>
                                <input type="text" id="number-verify" class="form-control required"  required="required" readonly style="max-width:450px;" />
                                <label><small>Masukan Kode OTP</small></label>
                                <input type="text" id="verification" class="form-control" placeholder="Verification code"  style="max-width:450px;" >
                                <center>
                                <button type="button" id="btn-verify" class="btn btn-verify btn-success mt-3" onclick="verify()">Verify code</button>
                                <button type="button" id="btn-send" class="btn btn-verify btn-success mt-3" onclick="send()">Lanjutkan</button>
                                <br><a  href="<?php echo base_url('auth/forgot_password_p'); ?>"><small>kembali</small></a>
                            </center>
                            </form>
                        </div>

        </div>
    </div>
</div>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
        <!-- Firebase App (the core Firebase SDK) is always required and must be listed first -->
        <script src="https://www.gstatic.com/firebasejs/6.0.2/firebase.js"></script>
        <script>
            var firebaseConfig = {
    apiKey: "AIzaSyDq4QVhUDWXSOo0gZ0Q_qw2-MSLnLQF3c8",
    authDomain: "phone-otp-3569b.firebaseapp.com",
    projectId: "phone-otp-3569b",
    storageBucket: "phone-otp-3569b.appspot.com",
    messagingSenderId: "1066587057219",
    appId: "1:1066587057219:web:6237edd4b3835893fdf8f6",
            };
            firebase.initializeApp(firebaseConfig);
        </script>
        <script type="text/javascript">
            window.onload = function () {
                render();
                    $("#phone-verify").hide();
                    // $("#btn-send").hide();
            };
            
            function render() {
                window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container');
                recaptchaVerifier.render();
            }
            function back() {
                    $("#phone-otp").show();
                    $("#phone-verify").hide();
            }
            function showverify(number) {
                    $("#phone-verify").show();
                    $("#number-verify").val(''+number);
            }
            
            function sendOTP() {
                var number = $("#number").val();
                firebase.auth().signInWithPhoneNumber(number, window.recaptchaVerifier).then(function (confirmationResult) {
                    window.confirmationResult = confirmationResult;
                    coderesult = confirmationResult;
                    $("#successAuth").text("Message sent");
                    $("#successAuth").show();
                    $("#phone-otp").hide();
                    $("#btn-send").hide();
                    showverify(number);
                }).catch(function (error) {
                    $("#error").text(error.message);
                    $("#error").show();
                    $("#phone-verify").hide();
                    $("#phone-otp").show();
                    $("#btn-send").hide();
                });
            }
            
            function verify() {
                var code = $("#verification").val();
                var number = $("#number-verify").val();                
                coderesult.confirm(code).then(function (result) {
                    var user = result.user;
                    console.log(user);
                    $("#btn-verify").hide();
                    $("#btn-send").show();
                    send();
                }).catch(function (error) {
                    $("#error").text(error.message);
                    $("#error").show();
                    $("#btn-send").hide();
                });
            }

            function send(){                
            var number = $("#number-verify").val();                
            $.ajax({
                type: 'post',
                url: base_url+'auth/forgot_password_p',
                data: {'phone':number},
                typedata:JSON,
            beforeSend:function(f){             
            },
            success: function(data){
                    location.reload();
                    window.location = base_url+'auth/update_password_p/'+ "<?=$this->session->flashdata('message')?>" ;
                }
            });
            }
        </script>