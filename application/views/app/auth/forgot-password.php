<div class="content-page">
    <div class="container d-flex justify-content-center" style="margin-top: 5rem;">
        <p class="text-center" style="font-weight: bold; font-size: 30pt; font-family: 'Poppins';">Forgot Password</p>
    </div>
    <div class="login form-member">
        <div class="container login-container d-flex justify-content-center">
            <div class="contact-form col-md-5">
                <form id="form-forgot-password">
                    <?php
                        if ($this->session->flashdata('message')) {
                            $status = ( $this->session->flashdata('status') ) ? $this->session->flashdata('status') : 'info';
                            echo '<div class="alert alert-'.$status.'">'.$this->session->flashdata('message').'</div>';

                            unset($_SESSION['message']);
                        }
                    ?>

                    <div class="inputset">
                        <fieldset>
                            <input style="border-radius: 10px;" name="email" type="email" class="form-control required" id="email" placeholder="E-Mail Address" required="">
                        </fieldset>
                    </div>

                    <div style="margin-top: 2rem;">
                        <button type="submit" id="form-submit" class="btn btn-block p-2 send_email" style="background-color: #008080; align-items: center; border-radius: 10px;">
                            <p style="font-weight: bold; font-size: 15pt; color: white; font-family: 'Poppins';">Request New Password</p>
                        </button>
                    </div>

                    <div class="text-center after-submit-btn">
                        <a href="<?php echo base_url('auth/forgot_password_p'); ?>" style="font-weight: bold; font-size: 12pt;">
                            Gunakan nomor handphone
                        </a>
                    </div>
                    
                    <div class="text-center after-submit-btn">
                        <a href="<?php echo base_url('auth/login'); ?>" style="font-weight: bold; font-size: 13pt; font-family: 'Poppins';">
                            <b style="color: #008080;">
                                Back to login
                            </b>
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>