						<div class="btn-group" role="group" aria-label="Basic example">
                            <a href="<?php echo base_url('corporate/employee/list') ?>" class="btn btn-sm btn-secondary <?php echo ($page == 'corporate-employee-list') ? 'active' : ''; ?>">All</a>
                            <a href="<?php echo base_url('corporate/employee/waiting_list') ?>" class="btn btn-sm btn-secondary <?php echo ($page == 'corporate-employee-list-waiting') ? 'active' : ''; ?>">Waiting Confirmation</a>
                        </div>