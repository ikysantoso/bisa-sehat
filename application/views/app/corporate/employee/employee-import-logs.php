<div class="main-content" id="panel">
    <?php include VIEWPATH.'/template-part/admin/top-nav.php'; ?>

    <!-- Header -->
    <div class="header bg-primary pb-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <?php include VIEWPATH.'/template-part/admin/breadcrumb.php'; ?>
                    <div class="col-lg-6 col-5 text-right">
                        <a href="<?php echo base_url('corporate/employee/import_new'); ?>" class="btn btn-sm btn-neutral" id="btn-add-new"><i class="fas fa-plus"></i> New</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header border-0">
                        <h3 class="mb-0">Employee Import Logs</h3>
                    </div>
                    <!-- Light table -->
                    <div class="table-responsive">
                        <table id="table-user-list" class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th>#</th>
                                    <th>Date Uploaded</th>
                                    <th>Status</th>
                                    <th>ID</th>
                                    <th>#</th>
                                </tr>
                            </thead>
                            <tbody class="list"></tbody>
                        </table>
                    </div>
                    
                </div>
            </div>
        </div>
        
        <?php include VIEWPATH.'/template-part/admin/footer-text.php'; ?>
    </div>
</div>
