<div class="main-content" id="panel">
    <?php include VIEWPATH.'/template-part/admin/top-nav.php'; ?>

    <!-- Header -->
    <div class="header bg-primary pb-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <?php include VIEWPATH.'/template-part/admin/breadcrumb.php'; ?>
                    
                    <div class="col-lg-6 col-5 text-right">
                        <a href="<?php echo base_url('admin/user/new'); ?>" class="btn btn-sm btn-neutral">New</a>
                        <a href="#" class="btn btn-sm btn-neutral">Filters</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
                <div class="card">
                    <!-- Card header -->
                    
                    <div class="card-body">
                        <div class="d-flex justify-content-between align-items-center mb-3">
                            <div>Uploaded Date</div>
                            <div class="flex-fill border mx-3"></div>
                            <b><?php echo $imported_log->created_at->format('d F Y H:i:s'); ?></b>                            
                        </div>
                        <div class="d-flex justify-content-between align-items-center mb-3">
                            <div>Status</div>
                            <div class="flex-fill border mx-3"></div>
                            <b><?php echo $imported_log->status; ?></b>                            
                        </div>
                        <?php if($imported_log->status != 'waiting'): ?>
                        <div class="d-flex justify-content-between align-items-center mb-3">
                            <div>Approval Date</div>
                            <div class="flex-fill border mx-3"></div>
                            <b><?php echo $imported_log->updated_at->format('d F Y H:i:s'); ?></b>                            
                        </div>
                        <?php endif; ?>
                    </div>

                    <h3 class="mx-4 mb-3">Imported User Tabel</h3>
                    <div class="table-responsive text-center">
                        <table class="table align-items-center table-flush table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Firstname</th>
                                    <th>Lastname</th>
                                    <th>Email</th>
                                    <th>Username</th>
                                    <th>Password</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php 
                                if ( $imported_log->status == 'waiting' ) {

                                    $column = [ 'A', 'B', 'C', 'D', 'E' ];
                                    $error_row = [];
                                    ?>
                                    <?php 
                                    foreach ($imported_data as $key => $employee){
                                        if($key > 1){

                                            $column_data = '';
                                            $error_column = [];

                                            foreach ($column as $_key => $column_item) {

                                                $error = '';

                                                if ( $column_item == 'C' && user_checker( 'email', $employee[ $column_item ] ) ) {
                                                    $error = 'Email telah digunakan';
                                                }

                                                if ( $column_item == 'D' && user_checker( 'username', $employee[ $column_item ] ) ) {
                                                    $error = 'Username telah digunakan';
                                                }

                                                $column_data .= '<td>'.$employee[ $column_item ];
                                                if ( $error != '' ) {
                                                    $column_data .= '<em class="text-danger d-block">'.$error.'</em>';
                                                    $error_column[] = $error;
                                                } 
                                                $column_data .= '</td>';

                                            }

                                            if ( count($error_column) == 0 ) {
                                                $column_data .= '<td class="text-success"><i class="fas fa-check-circle"></i></td>';
                                            } else {
                                                $column_data .= '<td class="text-danger"><i class="far fa-times-circle"></i></td>';
                                            }

                                            echo '
                                                <tr>
                                                    <td>'.($key-1).'</td>
                                                    '.$column_data.'
                                                </tr>
                                            ';

                                            if ( count($error_column) > 0 ) {
                                                $error_row[] = $key;
                                            }
                                        }
                                    }
                                } else {
                                    foreach ($imported_log->employee_inserted as $key => $employee_inserted) {


                                        if ( $employee_inserted->user ) {
                                            echo '
                                            <tr>
                                                <td>'.($key+1).'</td>
                                                <td>'.$employee_inserted->user->first_name.'</td>
                                                <td>'.$employee_inserted->user->last_name.'</td>
                                                <td>'.$employee_inserted->user->email.'</td>
                                                <td>'.$employee_inserted->user->username.'</td>
                                                <td>-</td>
                                                <td class="text-success"><i class="fas fa-check-circle"></i></td>
                                            </tr>
                                            ';
                                        } else {

                                            $user_data = json_decode($employee_inserted->rejected_info);

                                            $error_email = (in_array('email', $user_data->error)) ? '<em class="text-danger d-block">Email sudah pernah digunakan</em>' : '';
                                            $error_username = (in_array('username', $user_data->error)) ? '<em class="text-danger d-block">Username sudah pernah digunakan</em>' : '';

                                            echo '
                                            <tr>
                                                <td>'.($key+1).'</td>
                                                <td>'.$user_data->user->first_name.'</td>
                                                <td>'.$user_data->user->last_name.'</td>
                                                <td>'.$user_data->user->email.$error_email.'</td>
                                                <td>'.$user_data->user->username.$error_username.'</td>
                                                <td>-</td>
                                                <td class="text-danger"><i class="far fa-times-circle"></i></td>
                                            </tr>
                                            ';
                                           
                                        }
                                    }
                                }
                                ?>
                            </tbody>
                        </table>

                       
                    </div>
                </div>
            </div>
        </div>
        
        <?php include VIEWPATH.'/template-part/admin/footer-text.php'; ?>
    </div>
</div>
