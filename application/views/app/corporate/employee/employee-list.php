<div class="main-content" id="panel">
    <?php include VIEWPATH.'/template-part/admin/top-nav.php'; ?>

    <!-- Header -->
    <div class="header bg-primary pb-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <?php include VIEWPATH.'/template-part/admin/breadcrumb.php'; ?>
                </div>
            </div>
        </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header border-0 d-flex justify-content-between align-items-center">
                        <h3 class="mb-0">Employee List</h3>
                        <?php include VIEWPATH.'/app/corporate/employee/employee-list-nav.php'; ?>
                    </div>
                    <!-- Light table -->
                    <div class="table-responsive">
                        <table id="table-user-list" class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Username</th>
                                    <th>Email</th>
                                    <th>Role</th>
                                    <th>Status</th>
                                    <th>ID</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody class="list"></tbody>
                        </table>
                    </div>
                    
                </div>
            </div>
        </div>
        
        <?php include VIEWPATH.'/template-part/admin/footer-text.php'; ?>
    </div>
</div>
<div class="modal" id="approval-member-modal" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="approval-member-form">
                <input type="hidden" name="id">
                <div class="modal-header">
                    <h5 class="modal-title">Corporate Approval Member</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" name="name" class="form-control" disabled="disabled"/>
                            </div>
                            <div class="form-group">
                                <label>Username</label>
                                <input type="text" name="username" class="form-control" disabled="disabled"/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Email</label>
                                <input type="text" name="email" class="form-control" disabled="disabled"/>
                            </div>
                            <div class="form-group">
                                <label>Role</label>
                                <input type="text" name="role" class="form-control" disabled="disabled"/>
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="d-block">Status</label>
                        <div class="form-check form-check-inline">
                            <input required="required" class="form-check-input" type="radio" name="status" id="inlineRadio3" value="waiting">
                            <label class="form-check-label" for="inlineRadio3">Waiting</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input required="required" class="form-check-input" type="radio" name="status" id="inlineRadio1" value="rejected">
                            <label class="form-check-label" for="inlineRadio1">Reject</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input required="required" class="form-check-input" type="radio" name="status" id="inlineRadio2" value="approved">
                            <label class="form-check-label" for="inlineRadio2">Approve</label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>