<div class="main-content" id="panel">
    <?php include VIEWPATH.'/template-part/admin/top-nav.php'; ?>

    <!-- Header -->
    <div class="header bg-primary pb-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <?php include VIEWPATH.'/template-part/admin/breadcrumb.php'; ?>
                </div>
            </div>
        </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header border-0 d-flex justify-content-between align-items-center">
                        <h3 class="mb-0">Fitness Data</h3>
                    </div>
                    <?php $user = UserModel::find( $user_id ); ?>
                    <div class="card-body">
                        <h5 class="card-title"><?php echo $user->first_name." ".$user->last_name; ?></h5>
                        <p><?php echo $user->user_meta('gender'); ?> / <?php echo $user->age(); ?> tahun</p>
                        <p><?php echo $user->email; ?></p>
                    </div>
                    
                    <!-- Light table -->
                    <div class="table-responsive">
                        <table id="table-medical-record" class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th>#</th>
                                    <th>Tanggal Input</th>
                                    <th>Berat Badan (kg)</th>
                                    <th>Tinggi Badan (cm)</th>
                                    <!-- <th>Jarak Tempuh Ideal (m)</th> -->
                                    <th>Jarak Tempuh (m)</th>
                                    <th>Status</th>
                                    <th>#</th>
                                </tr>
                            </thead>
                            <tbody class="list">
                                <?php foreach ($fitnesses as $key => $fitness) : ?>
                                <tr>
                                    <td><?php echo $key+1; ?></td>
                                    <td><?php echo $fitness->created_at; ?></td>
                                    <td><?php echo $fitness->height; ?></td>
                                    <td><?php echo $fitness->weight; ?></td>
                                    <!-- <td><?php echo $fitness->mileage_result()['ideal']; ?></td> -->
                                    <td><?php echo $fitness->mileage; ?></td>
                                    <td><?php echo $fitness->mileage_result()['status']; ?></td>
                                    <td class="align-middle" width="145px">
                                        <a href="<?php echo base_url('corporate/healthy/fitness/single_download/'.$fitness->id); ?>" class="btn btn-sm btn-secondary"><i class="fas fa-file-download"></i></a>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    
                </div>
            </div>
        </div>
        
        <?php include VIEWPATH.'/template-part/admin/footer-text.php'; ?>
    </div>
</div>