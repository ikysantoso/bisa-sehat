<div class="main-content" id="panel">
    <?php include VIEWPATH.'/template-part/admin/top-nav.php'; ?>

    <!-- Header -->
    <div class="header bg-primary pb-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <?php include VIEWPATH.'/template-part/admin/breadcrumb.php'; ?>
                </div>
            </div>
        </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header border-0 d-flex justify-content-between align-items-center">
                        <h3 class="mb-0">Medical Record Data</h3>
                    </div>

                    <?php $user = UserModel::find( $user_id ); ?>
                    <div class="card-body">
                        <h5 class="card-title"><?php echo $user->first_name." ".$user->last_name; ?></h5>
                        <p><?php echo $user->user_meta('gender'); ?> / <?php echo $user->age(); ?> tahun</p>
                        <p><?php echo $user->email; ?></p>
                    </div>
                    <!-- Light table -->
                    <div class="table-responsive">
                        <table id="table-medical-record" class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th>#</th>
                                    <th>Tanggal Input</th>
                                    <th>Berat Badan</th>
                                    <th>Tekanan Darah</th>
                                    <th>Gula Darah</th>
                                    <th>Kolestrol</th>
                                    <th>Asam Urat</th>
                                    <th>#</th>
                                </tr>
                            </thead>
                            <tbody class="list">
                                <?php foreach ($medical_records as $key => $medical_record) : 
                                    $problem = [];
                                    ?>
                                <tr>
                                    <td class="align-middle"><?php echo ($key+1); ?></td>
                                    <td class="align-middle" width="160px"><?php echo $medical_record->created_at; ?></td>
                                    <td width="200px">
                                        <div class="d-flex justify-content-between align-items-center">
                                            <b>TB</b>
                                            <div class="border-bottom flex-fill mx-2"></div>
                                            <span><?php echo $medical_record->height; ?> cm</span>
                                        </div>
                                        <div class="d-flex justify-content-between align-items-center">
                                            <b>BB</b>
                                            <div class="border-bottom flex-fill mx-2"></div>
                                            <span><?php echo $medical_record->weight; ?> kg</span>
                                        </div>
                                        <div class="d-flex justify-content-between align-items-center">
                                            <b>BB Ideal</b>
                                            <div class="border-bottom flex-fill mx-2"></div>
                                            <span><?php echo $medical_record->bmi()['min']; ?> s/d <?php echo $medical_record->bmi()['max']; ?> kg</span>
                                        </div>
                                        <div class="d-flex justify-content-between align-items-center">
                                            <b>BMI</b>
                                            <div class="border-bottom flex-fill mx-2"></div>
                                            <span><?php echo $medical_record->bmi()['bmi']; ?></span>
                                        </div>
                                        <div class="d-flex justify-content-between align-items-center">
                                            <b>Keterangan</b>
                                            <div class="border-bottom flex-fill mx-2"></div>
                                            <span class="badge badge-light border"><?php echo health_result_color($medical_record->bmi_scale()['desc']); ?></span>
                                        </div>
                                        <?php 
                                        if($medical_record->bmi_scale()['count'] > 0){
                                            $problem[] = 'kegemukan';
                                        }
                                        ?>
                                    </td>
                                    <td class="text-center align-middle">
                                        <p><?php echo $medical_record->sistole; ?>/<?php echo $medical_record->diastole; ?> mmHg</p>
                                        <span class="badge badge-light border"><?php echo health_result_color($medical_record->blood_pressure_scale()['desc']); ?></span>
                                        <?php 
                                        if($medical_record->blood_pressure_scale()['count'] > 0){
                                            $problem[] = 'hipertensi';
                                        }
                                        ?>
                                    </td>
                                    <td class="text-center align-middle">
                                        <p><?php echo $medical_record->blood_sugar_level; ?> mg/dL</p>
                                        <span class="badge badge-light border"><?php echo health_result_color($medical_record->blood_sugar_level_scale()['desc']); ?></span>
                                        <?php 
                                        if($medical_record->blood_sugar_level_scale()['count'] > 0){
                                            $problem[] = 'diabetes';
                                        }
                                        ?>
                                    </td>
                                    <td class="text-center align-middle">
                                        <p><?php echo $medical_record->cholesterol_level; ?> mg/dL</p>
                                        <span class="badge badge-light border"><?php echo health_result_color($medical_record->cholesterol_level_scale()['desc']); ?></span>
                                        <?php 
                                        if($medical_record->cholesterol_level_scale()['count'] > 0){
                                            $problem[] = 'kolesterol';
                                        }
                                        ?>
                                    </td>
                                    <td class="text-center align-middle">
                                        <p><?php echo $medical_record->uric_acid_level; ?> mg/dL</p>
                                        <span class="badge badge-light border"><?php echo health_result_color($medical_record->uric_acid_level_scale()['desc']); ?></span>
                                        <?php 
                                        if($medical_record->uric_acid_level_scale()['count'] > 0){
                                            $problem[] = 'asam urat';
                                        }
                                        ?>
                                    </td>
                                    <td class="align-middle" width="145px">
                                        <a href="<?php echo base_url('corporate/healthy/medical_record/single_download/'.$medical_record->id); ?>" class="btn btn-sm btn-secondary"><i class="fas fa-file-download"></i> Download</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">Kesimpulan</td>
                                    <td colspan="6">
                                        <?php 
                                        $kesimpulan = get_kesimpulan($problem);
                                        echo $kesimpulan;
                                        ?>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    
                </div>
            </div>
        </div>
        
        <?php include VIEWPATH.'/template-part/admin/footer-text.php'; ?>
    </div>
</div>