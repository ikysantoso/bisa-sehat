<div class="main-content" id="panel">
    <?php include VIEWPATH.'/template-part/admin/top-nav.php'; ?>

    <!-- Header -->
    <div class="header bg-primary pb-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <?php include VIEWPATH.'/template-part/admin/breadcrumb.php'; ?>
                </div>
            </div>
        </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header border-0 d-flex justify-content-between align-items-center">
                        <h3 class="mb-0">Fitness Data</h3>
                    </div>
                    <?php $user = UserModel::find( $user_id ); ?>
                    <div class="card-body">
                        <h5 class="card-title"><?php echo $user->first_name." ".$user->last_name; ?></h5>
                        <p><?php echo $user->user_meta('gender'); ?> / <?php echo $user->age(); ?> tahun</p>
                        <p><?php echo $user->email; ?></p>
                    </div>
                    
                    <?php 
                    if($medical_record = $user->medical_records->last()): 
                        $problem = [];
                    ?>
                    <div class="table-responsive">
                        <table class="table table-striped table-healthy-status">
                            <tr>
                                <td>1</td>
                                <th>BMI</th>
                                <td>
                                    <p><em>Diinput tanggal : <?php echo $medical_record->created_at; ?></em></p>
                                    <p>TB: <?php echo $medical_record->height; ?> cm</p>
                                    <p>BB: <?php echo $medical_record->weight; ?> kg</p>
                                    <p>BB Ideal: <?php echo $medical_record->bmi()['min']; ?> s/d <?php echo $medical_record->bmi()['max']; ?> kg</p>
                                    <p>BMI: <?php echo $medical_record->bmi()['bmi']; ?></p>
                                    <p>Ket: <?php echo health_result_color($medical_record->bmi_scale()['desc']); ?></p>
                                </td>
                                <td class="icon-column"><?php echo health_result_icon( $medical_record->bmi_scale()['desc'] );?></td>
                            </tr>
                            <?php 
                            if($medical_record->bmi_scale()['count'] > 0){
                                $problem[] = 'kegemukan';
                            }
                            ?>
                            <tr>
                                <td>2</td>
                                <th>Tekanan Darah</th>
                                <td>
                                    <p><em>Diinput tanggal : <?php echo $medical_record->created_at; ?></em></p>
                                    <p><?php echo $medical_record->sistole; ?>/<?php echo $medical_record->diastole; ?> mmHg</p>
                                    <p>Ket: <?php echo health_result_color($medical_record->blood_pressure_scale()['desc']); ?></p>
                                </td>
                                <td class="icon-column"><?php echo health_result_icon( $medical_record->blood_pressure_scale()['desc'] );?></td>
                            </tr>
                            <?php 
                            if($medical_record->blood_pressure_scale()['count'] > 0){
                                $problem[] = 'hipertensi';
                            }
                            ?>

                            <tr>
                                <td>3</td>
                                <th>Gula Darah</th>
                                <td>
                                    <p><em>Diinput tanggal : <?php echo $medical_record->created_at; ?></em></p>
                                    <p><?php echo $medical_record->blood_sugar_level; ?> mg/dL</p>
                                    <p>Ket: <?php echo health_result_color($medical_record->blood_sugar_level_scale()['desc']); ?></p>
                                </td>
                                <td class="icon-column"><?php echo health_result_icon( $medical_record->blood_sugar_level_scale()['desc'] );?></td>
                            </tr>
                            <?php 
                            if($medical_record->blood_sugar_level_scale()['count'] > 0){
                                $problem[] = 'diabetes';
                            }
                            ?>

                            <tr>
                                <td>4</td>
                                <th>Kolesterol</th>
                                <td>
                                    <p><em>Diinput tanggal : <?php echo $medical_record->created_at; ?></em></p>
                                    <p><?php echo $medical_record->cholesterol_level; ?> mg/dL</p>
                                    <p>Ket: <?php echo health_result_color($medical_record->cholesterol_level_scale()['desc']); ?></p>
                                </td>
                                <td class="icon-column"><?php echo health_result_icon( $medical_record->cholesterol_level_scale()['desc'] );?></td>
                            </tr>
                            <?php 
                            if($medical_record->cholesterol_level_scale()['count'] > 0){
                                $problem[] = 'kolesterol';
                            }
                            ?>

                            <tr>
                                <td>5</td>
                                <th>Asam urat</th>
                                <td>
                                    <p><em>Diinput tanggal : <?php echo $medical_record->created_at; ?></em></p>
                                    <p><?php echo $medical_record->uric_acid_level; ?> mg/dL</p>
                                    <p>Ket: <?php echo health_result_color($medical_record->uric_acid_level_scale()['desc']); ?></p>
                                </td>
                                <td class="icon-column"><?php echo health_result_icon( $medical_record->uric_acid_level_scale()['desc'] );?></td>
                            </tr>
                            <?php 
                            if($medical_record->uric_acid_level_scale()['count'] > 0){
                                $problem[] = 'asam urat';
                            }
                            ?>
                            
                            <?php $fitness = $user->fitnesses->last(); ?>
                            <tr>
                                <td>6</td>
                                <th>Status kebugaran</th>
                                <td>
                                    <?php
                                    echo ($fitness) ? $fitness->mileage_result()['status'] : '<div class="alert alert-warning">Belum ada data test kebugaran</div>'; 
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td>**</td>
                                <th>Kesimpulan</th>
                                <td>
                                    <?php 
                                    $kesimpulan = get_kesimpulan($problem);
                                    echo $kesimpulan;
                                    ?>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <?php else: ?>
                        <div class="alert alert-warning">Belum ada data test kesehatan</div>
                    <?php endif; ?>
                </div>
                    
                </div>
            </div>
        </div>
        
        <?php include VIEWPATH.'/template-part/admin/footer-text.php'; ?>
    </div>
</div>