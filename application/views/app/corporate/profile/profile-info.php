<div class="main-content" id="panel">
    <?php include VIEWPATH.'/template-part/admin/top-nav.php'; ?>

    <!-- Header -->
    <div class="header bg-primary pb-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <?php include VIEWPATH.'/template-part/admin/breadcrumb.php'; ?>
                </div>
            </div>
        </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header border-0">
                        <div class="d-flex justify-content-between align-items-center">
                            <h3 class="mb-0">Corporate Profile</h3>
                            <div>
                            <?php 
                            if ( $corporate ) {
                                if ( $corporate->status == 'waiting' ) {
                                    $btn_class = 'btn-secondary border-dark';
                                } else if ( $corporate->status == 'approved' ) {
                                    $btn_class = 'btn-success';
                                } else if ( $corporate->status == 'rejected' ) {
                                    $btn_class = 'btn-danger';
                                }
                                echo 'Status: <button disabled="disabled" class="ml-3 btn btn-sm '.$btn_class.'">'.$corporate->status.'</button>';
                            }
                            ?>
                            </div>

                        </div>
                    </div>

                    <div class="card-body">
                        <?php 
                            if ( $this->session->flashdata('message') ) {
                                echo '<div class="alert alert-info mb-3">'.$this->session->flashdata('message').'</div>';
                            }
                        ?>
                    	<form class="row" id="form-corporate">
                    		<input type="hidden" name="id" value="<?php echo ($corporate) ? $corporate->id : ''; ?>"/>
                        	<div class="form-group col-12">
                        		<label>Coporate Name</label>
                        		<input value="<?php echo ($corporate) ? $corporate->name : ''; ?>" class="form-control" type="text" name="name" required="required">
                        	</div>
                        	<div class="form-group col-12">
                        		<label>Office</label>
                        		<textarea class="form-control" name="office" required="required"><?php echo ($corporate) ? $corporate->office : ''; ?></textarea>
                        	</div>
                            <div class="form-group col-md-6">
                                <label>Website</label>
                                <input value="<?php echo ($corporate) ? $corporate->website : ''; ?>" class="form-control" type="text" name="website">
                            </div>
                            <div class="form-group col-md-6">
                                <label>Phone</label>
                                <input value="<?php echo ($corporate) ? $corporate->phone : ''; ?>" class="form-control" type="text" name="phone">
                            </div>
                        	<div class="form-group col-12">
                        		<label>Description</label>
                        		<textarea class="form-control" name="description" required="required"><?php echo ($corporate) ? $corporate->description : ''; ?></textarea>
                        	</div>
                            <div class="col-12">
                                <button type="submit" class="btn btn-primary ml-auto">Save</button>
                            </div>
                        </form>
                    </div>
                   
                </div>
            </div>
        </div>
        
        <?php include VIEWPATH.'/template-part/admin/footer-text.php'; ?>
    </div>
</div>
