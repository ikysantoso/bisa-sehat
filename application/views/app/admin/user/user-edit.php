<div class="main-content" id="panel">
    <?php include VIEWPATH.'/template-part/admin/top-nav.php'; ?>

    <!-- Header -->
    <div class="header bg-primary pb-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <?php include VIEWPATH.'/template-part/admin/breadcrumb.php'; ?>
                </div>
            </div>
        </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col-xl-8">
                <div class="card">
                    <!-- Card header -->
                    <div class="card-body">
                        <form id="form-user">
                            <input type="hidden" name="id" value="<?php echo $user_detail->id ?>"/>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>First Name</label>
                                        <input value="<?php echo $user_detail->first_name ?>" class="form-control" type="text" name="first_name" required="required"/>
                                    </div>
                                    <div class="form-group">
                                        <label>Last Name</label>
                                        <input value="<?php echo $user_detail->last_name ?>" class="form-control" type="text" name="last_name"/>
                                    </div>
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input value="<?php echo $user_detail->email ?>" class="form-control" type="email" name="email" required="required"/>
                                    </div>
                                    <div class="form-group">
                                        <label>Username</label>
                                        <input value="<?php echo $user_detail->username ?>" class="form-control" type="text" name="username" required="required"/>
                                    </div>
                                    <div class="form-group">
                                        <label>Role</label>
                                        <select class="form-control" name="role" required="required">
                                            <option selected="selected" disabled="disabled">-- Choose Role --</option>
                                            <?php 
                                            $group = $this->ion_auth->get_users_groups( $user_detail->id )->row();
                                            foreach ($this->ion_auth->groups()->result() as $key => $user_group) : ?>
                                                <option <?php echo (isset($group) && $group->id == $user_group->id) ? 'selected="selected"' : ''; ?> value="<?php echo $user_group->id; ?>" class="text-capitalize"><?php echo $user_group->name; ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="d-flex justify-content-between">
                                <div>
                                    <a href="<?php echo base_url('admin/user/list') ?>" class="btn btn-link" data-dismiss="modal">Back</a>
                                    <button type="submit" class="btn btn-primary ml-auto">Save</button>
                                </div>
                                <button type="button" data-toggle="modal" data-target="#modal-update-password" class="btn btn-secondary">Update Password</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        <?php include VIEWPATH.'/template-part/admin/footer-text.php'; ?>
    </div>
</div>

<div id="modal-update-password" class="modal" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="form-password">
                <input type="hidden" value="<?php echo $user_detail->id ?>" name="id"/>
                <div class="modal-header">
                    <h5 class="modal-title">Update Password</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <input type="text" class="form-control" name="password" required="required"/>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>