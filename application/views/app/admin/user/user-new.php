<div class="main-content" id="panel">
    <?php include VIEWPATH.'/template-part/admin/top-nav.php'; ?>

    <!-- Header -->
    <div class="header bg-primary pb-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <?php include VIEWPATH.'/template-part/admin/breadcrumb.php'; ?>
                </div>
            </div>
        </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col-xl-8">
                <div class="card">
                    <!-- Card header -->
                    <div class="card-body">
                        <form id="form-user">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>First Name</label>
                                        <input class="form-control" type="text" name="first_name" required="required"/>
                                    </div>
                                    <div class="form-group">
                                        <label>Last Name</label>
                                        <input class="form-control" type="text" name="last_name"/>
                                    </div>
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input class="form-control" type="email" name="email" required="required"/>
                                    </div>
                                    <div class="form-group">
                                        <label>Username</label>
                                        <input class="form-control" type="text" name="username" required="required"/>
                                    </div>
                                    <div class="form-group">
                                        <label>Password</label>
                                        <input class="form-control" type="text" name="password" required="required"/>
                                    </div>
                                    <div class="form-group">
                                        <label>Role</label>
                                        <select class="form-control" name="role" required="required">
                                            <option selected="selected" disabled="disabled">-- Choose Role --</option>
                                            <?php 
                                            foreach ($this->ion_auth->groups()->result() as $key => $user_group) : 
                                                if ( $this->ion_auth->is_admin() ) {
                                                    ?><option value="<?php echo $user_group->id; ?>" class="text-capitalize"><?php echo $user_group->description; ?></option><?php
                                                } else if( $this->ion_auth->is_co_admin() ){
                                                    if ( !in_array($user_group->name, ['admin', 'co_admin']) ) {
                                                    ?><option value="<?php echo $user_group->id; ?>" class="text-capitalize"><?php echo $user_group->description; ?></option><?php
                                                    }
                                                }
                                            endforeach;
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <a href="<?php echo base_url('admin/user/list') ?>" class="btn btn-link" data-dismiss="modal">Back</a>
                            <button type="submit" class="btn btn-primary ml-auto">Save</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        <?php include VIEWPATH.'/template-part/admin/footer-text.php'; ?>
    </div>
</div>