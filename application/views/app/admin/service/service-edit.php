<div class="main-content" id="panel">
    <?php include VIEWPATH.'/template-part/admin/top-nav.php'; ?>

    <!-- Header -->
    <div class="header bg-primary pb-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <?php include VIEWPATH.'/template-part/admin/breadcrumb.php'; ?>
                </div>
            </div>
        </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header border-0">
                        <h3 class="mb-0">Service Edit</h3>
                    </div>
                    
                    <div class="card-body">
                    	<form id="form-service">
                            <input type="hidden" name="id" value="<?php echo $service->id; ?>"/>
	                    	<div class="row">
	                    		<div class="col-md-8">
			                    	<div class="form-group">
			                    		<label>Title</label>
			                    		<input value="<?php echo $service->title; ?>" type="text" class="form-control" required="required" name="title"/>
			                    	</div>
                                    <div class="form-group">
                                        <label>Category</label>
                                        <input data-user-option-allowed="true" data-initial-value='<?php echo json_encode(['value' => $service->category_id, 'text' => $service->category->name]) ?>' value="<?php echo $service->category_id; ?>" type="text" class="singleInputDynamic" data-url="<?php echo base_url('admin/service/category_fastselect'); ?>" name="category_id" required="required"/>
                                    </div>
			                    	<div class="form-group">
			                    		<label>Description</label>
			                    		<div id="editor"><?php echo $service->description; ?></div>
										<input type="hidden" class="description-field" name="description"/>
			                    	</div>
	                    		</div>
	                    		<div class="col-md-4">
	                    			<div class="form-group">
                                        <label>Images</label>
                                        <div class="row input-img-container">
                                            <?php 
                                            $current_images = [];
                                            foreach ($service->images as $key => $image):
                                                $current_images[] = $image->id;
                                            ?>
                                            <div class="input-img-item mb-3 col-4">
                                                <div class="bg-light embed-responsive embed-responsive-4by3 rounded bg-vertical-center" style="background-image: url('<?php echo base_url('assets/images/upload/service/'.$image->image) ?>')">
                                                    <button style="display: none" onclick="$(this).parent().find('.input-img').click();" type="button" class="btn btn-upload-img"><i class="fas fa-plus"></i></button>
                                                    <input class="input-img" type="file" accept="image/*" style="visibility: hidden;position: absolute;" name="image[]">
                                                    <i title="Remove image" data-id="<?php echo $image->id; ?>" class="fas fa-trash btn-remove-img"></i>
                                                </div>
                                            </div>
                                            <?php endforeach; ?>

                                            <input type="hidden" name="current_images" value="<?php echo implode(",", $current_images) ?>">

                                            <div id="field-image" class="input-img-item mb-3 col-4">
                                                <div class="bg-light embed-responsive embed-responsive-4by3 rounded bg-vertical-center">
                                                    <button onclick="$(this).parent().find('.input-img').click();" type="button" class="btn btn-upload-img"><i class="fas fa-plus"></i></button>
                                                    <input class="input-img" type="file" accept="image/*" style="visibility: hidden;position: absolute;" name="image[]">
                                                    <i title="Remove image" style="display: none" class="fas fa-trash btn-remove-img"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
	                    		</div>
	                    	</div>

	                    	<a class="btn btn-secondary" href="<?php echo base_url('admin/service'); ?>">Cancel</a>
	                    	<button type="submit" class="btn btn-primary">Save</button>
                    	</form>
                    </div>
                </div>
            </div>
        </div>
        
        <?php include VIEWPATH.'/template-part/admin/footer-text.php'; ?>
    </div>
</div>