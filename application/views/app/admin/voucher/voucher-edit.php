<div class="main-content" id="panel">
    <?php include VIEWPATH.'/template-part/admin/top-nav.php'; ?>

    <!-- Header -->
    <div class="header bg-primary pb-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <?php include VIEWPATH.'/template-part/admin/breadcrumb.php'; ?>
                </div>
            </div>
        </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
                <div class="card">
                    
                    <div class="card-body">
                    	<form id="form-edit-voucher">
                            <input type="hidden" name="id" value="<?php echo $voucher->id; ?>" >
                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" name="name" value="<?php echo $voucher->name; ?>" required="required" class="form-control"/>
                            </div>
                            <div class="form-group">
                                <label>Nominal</label>
                                <input type="number" name="nominal" value="<?php echo $voucher->nominal; ?>" required="required" class="form-control"/>
                            </div>
                            <div class="form-group">
                                <label>Quantity</label>
                                <input type="number" value="<?php echo $voucher->quantity; ?>"  name="quantity" class="form-control"/>
                                <small id="passwordHelpBlock" class="form-text text-muted">Biarkan kosong jika tanpa batasan</small>
                            </div>
                            <div class="form-group">
                                <label>Start Date</label>
                                <input type="text" id="datepicker" name="start_date" value="<?php echo $voucher->start_date; ?>" required="required" class="form-control"/>
                            </div>
                            <div class="form-group">
                                <label>Expired Date</label>
                                <!-- <input type="text" id="datepicker2" name="expired" value="03/31/2021 17:37" required="required" class="form-control"/> -->
                                <input type="text" id="datepicker2" name="expired" value="<?php echo $voucher->expired; ?>" required="required" class="form-control"/>
                            </div>
	                    		
	                    	<a class="btn btn-secondary" href="<?php echo base_url('admin/voucher'); ?>">Cancel</a>
	                    	<button type="submit" class="btn btn-primary">Save</button>
                    	</form>
                    </div>
                </div>
            </div>
        </div>
        
        <?php include VIEWPATH.'/template-part/admin/footer-text.php'; ?>
    </div>
</div>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-ui-timepicker-addon/1.6.3/jquery-ui-timepicker-addon.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-ui-timepicker-addon/1.6.3/jquery-ui-timepicker-addon.min.js"></script>
    <script>
    jQuery(function($) {

        $("#datepicker").datetimepicker({
            dateFormat: 'yy-mm-dd',
            timeFormat: 'hh:mm:ss',
        });
        $("#datepicker2").datetimepicker({
            dateFormat: 'yy-mm-dd',
            timeFormat: 'hh:mm:ss',
        });
    });
  </script>