<div class="main-content" id="panel">
    <?php include VIEWPATH.'/template-part/admin/top-nav.php'; ?>

    <!-- Header -->
    <div class="pb-6 header bg-primary">
        <div class="container-fluid">
            <div class="header-body">
                <div class="py-4 row align-items-center">
                    <?php include VIEWPATH.'/template-part/admin/breadcrumb.php'; ?>
                    <div class="text-right col-lg-6 col-5">
                        <a href="<?php echo base_url('admin/voucher/new'); ?>" class="btn btn-sm btn-neutral" id="btn-add-new"><i class="fas fa-plus"></i> New</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
                <div class="card">
                    <!-- Card header -->
                    <div class="border-0 card-header">
                        <h3 class="mb-0">Voucher List</h3>
                    </div>
                    <!-- Light table -->
                    <div class="table-responsive">
                        <table id="table-voucher-list" class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Start Date</th>
                                    <th>Expired Date</th>
                                    <th>Nominal</th>
                                    <th>Quantity per user</th>
                                    <th>ID</th>
                                    <th>#</th>
                                </tr>
                            </thead>
                            <tbody class="list"></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <?php include VIEWPATH.'/template-part/admin/footer-text.php'; ?>
    </div>
</div>