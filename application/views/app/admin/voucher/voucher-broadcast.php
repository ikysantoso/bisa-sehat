<?php 
if(isset($type_broadcast)) :
    $type_selected = $type_broadcast->type_broadcast;
else :
    $type_selected = "";
endif;
?>
<div class="main-content" id="panel">
    <?php include VIEWPATH.'/template-part/admin/top-nav.php'; ?>

    <!-- Header -->
    <div class="header bg-primary pb-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <?php include VIEWPATH.'/template-part/admin/breadcrumb.php'; ?>
                </div>
            </div>
        </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
                <form id="form-voucher-broadcast" class="card">
                    <!-- Card header -->
                    <div class="card-header border-0">
                        <h3 class="mb-0">Broadcast Voucher</h3>
                    </div>
                    <div class="card-body">
                    <input type="hidden" name="voucher_id" value="<?= $voucher_id ?>">
                        <div class="form-group">
	                 		<label>Broadcast Voucher </label><br/>
                            <div class="form-check mb-3">
                                <input class="form-check-input type_broadcast" id="all_user" type="radio" name="type_broadcast" <?= ($type_selected == 1) ? "checked" : ""; ?> id="type_broadcast"  value="1" required="required"/>
                                <label class="form-check-label" for="all_user">All User</label>
                            </div>
                            <?php $uh_array= []; ?>
                            <?php foreach ($usersselected as $userselect ): ?>
                            <?php $uh_array[] = $userselect->user_id; ?>
                            <?php endforeach; ?>

                            <?php $uh_array_corporate= []; ?>
                            <?php foreach ($corporatedselected as $corporatedselect ): ?>
                            <?php $uh_array_corporate[] = $corporatedselect->corporate_id; ?>
                            <?php endforeach; ?>

                            <div class="form-check mb-3">
                                <input class="form-check-input type_broadcast" id="selected_user" type="radio" name="type_broadcast" <?= ($type_selected == 2) ? "checked" : ""; ?> id="type_broadcast"  value="2" required="required"/>
                                <label class="form-check-label" for="selected_user">Selected User</label>
                                <div class="content-type-broadcast" style="display:<?= ($type_selected == 2) ? "block" : "none"; ?>">
                                    <select class="js-example-basic-multiple" name="users[]" multiple="multiple">
                                        <?php foreach ($users as $key => $user): ?>
                                            <?php if(count($usersselected)>0) : ?>
                                                <option <?php echo in_array($user->id, $uh_array) ? 'selected' : ''; ?>  value="<?php echo $user->id ?>"><?php echo $user->username ?></option>
                                            <?php else : ?>
                                                <option  value="<?php echo $user->id ?>"><?php echo $user->username ?></option>
                                            <?php endif ?>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-check mb-3">
                                <input class="form-check-input type_broadcast" id="corporated" type="radio" name="type_broadcast" <?= ($type_selected == 3) ? "checked" : ""; ?> id="type_broadcast"  value="3" required="required"/>
                                <label class="form-check-label" for="corporated">Corporated</label>
                                <div class="content-type-broadcast" style="display:<?= ($type_selected == 3) ? "block" : "none"; ?>">
                                    <select class="js-example-basic-multiple2" name="corporates[]" data-allow-clear="true" multiple="multiple">
                                       <?php foreach ($corporates as $key => $corporate): ?>
                                            <?php if(count($corporatedselected)>0) : ?>
                                                <option  <?php echo in_array($corporate->id, $uh_array_corporate) ? 'selected' : ''; ?>  value="<?php echo $corporate->id ?>"><?php echo $corporate->name ?></option>
                                            <?php else : ?>
                                                <option  value="<?php echo $corporate->id ?>"><?php echo $corporate->name ?></option>
                                            <?php endif ?>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer text-center">
                        <a href="<?php echo base_url('admin/voucher/list'); ?>" class="btn btn-secondary">Cancel</a>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
        
        <?php include VIEWPATH.'/template-part/admin/footer-text.php'; ?>
        
    </div>
</div>

