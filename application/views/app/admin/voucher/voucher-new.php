
<div class="main-content" id="panel">
    <?php include VIEWPATH.'/template-part/admin/top-nav.php'; ?>

    <!-- Header -->
    <div class="pb-6 header bg-primary">
        <div class="container-fluid">
            <div class="header-body">
                <div class="py-4 row align-items-center">
                    <?php include VIEWPATH.'/template-part/admin/breadcrumb.php'; ?>
                </div>
            </div>
        </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
                <form id="form-new-voucher" class="card">
                    <!-- Card header -->
                    <div class="border-0 card-header">
                        <h3 class="mb-0">Add new voucher</h3>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" name="name" id="voucher-name" required="required" class="form-control"/>
                        </div>
                        <div class="form-group">
                            <label>Nominal</label>
                            <input type="number" name="nominal" id="voucher-nominal" required="required" class="form-control"/>
                        </div>
                        <div class="form-group">
                            <label>Quantity</label>
                            <input type="number" name="quantity" id="voucher-quantity" class="form-control"/>
                            <small id="passwordHelpBlock" class="form-text text-muted">Biarkan kosong jika tanpa batasan</small>
                        </div>
                        <div class="form-group">
                            <label>Start Date</label>
                            <input type="text" name="start_date" id="datepicker" required="required" class="form-control"/>
                        </div>
                        <div class="form-group">
                            <label>Expired Date</label>
                            <input type="text" name="expired" id="datepicker2" required="required" class="form-control"/>
                        </div>
                        <div class="form-group">
                            <label>Select User</label>
                            <br>
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="flex-row d-flex">
                                    <input type="text" name="voucherUserSelect" id="voucherUserSelect" class="form-control"/>
                                    <button type="button" class="ml-3 btn btn-light" style="background: #fff !important; border-color: #5f6b89; color: #5f6b89;" onclick="doSearchUserForVoucher();">Search</button>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>List Selected Users</label>
                            <small class="form-text text-muted">Distribusi voucher untuk semua user jika kosong</small>
                            <div id="userSelectedContainer" style="margin-top: 10px;width:50%;">
                                <span class="text-muted" id="hint-voucher-distribution">
                                    Kosong
                                </span>
                                <ul style="list-style-type: none;padding: 0;margin: 0;"></ul>
                            </div>
                        </div>
                    </div>
                    <div class="modal" id="modal-voucher" tabindex="-1">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Pilih User</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div id="modal-voucher-body" class="modal-body">
                                </div>
                                <div class="modal-footer">
                                    <button type="button" data-dismiss="modal" class="btn btn-primary" onclick="doConfirmUserVoucherSelected()">Pilih</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-center card-footer">
                        <a href="<?php echo base_url('admin/voucher/list'); ?>" class="btn btn-secondary">Cancel</a>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>

        <?php include VIEWPATH.'/template-part/admin/footer-text.php'; ?>

    </div>
</div>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-ui-timepicker-addon/1.6.3/jquery-ui-timepicker-addon.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-ui-timepicker-addon/1.6.3/jquery-ui-timepicker-addon.min.js"></script>
    <script>
    jQuery(function($) {

        $("#datepicker").datetimepicker({
            dateFormat: 'yy-mm-dd',
            timeFormat: 'hh:mm:ss',
        });
        $("#datepicker2").datetimepicker({
            dateFormat: 'yy-mm-dd',
            timeFormat: 'hh:mm:ss',
        });
    });
  </script>

