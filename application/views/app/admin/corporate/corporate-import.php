<div class="main-content" id="panel">
    <?php include VIEWPATH.'/template-part/admin/top-nav.php'; ?>

    <!-- Header -->
    <div class="header bg-primary pb-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <?php include VIEWPATH.'/template-part/admin/breadcrumb.php'; ?>

                    <div class="col text-right">
                        <span class="text-light">Download Template</span> 
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <a target="_blank" href="<?php echo base_url('assets/template-import/import-template.xls') ?>" class="btn btn-sm btn-neutral">XLS</a>
                            <a target="_blank" href="<?php echo base_url('assets/template-import/import-template.xlsx') ?>" class="btn btn-sm btn-neutral">XLSX</a>
                            <a target="_blank" href="<?php echo base_url('assets/template-import/import-template.csv') ?>" class="btn btn-sm btn-neutral">CSV</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col-xl-8">
                <div class="card">
                    <!-- Card header -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <form id="form-import">
                                	<input type="hidden" name="corporate_id" value="<?php echo $corporate->id; ?>"/>
                                    <div class="form-group">
                                        <label>File</label>
                                        <div class="input-group mb-3">
                                            <input accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" class="form-control" type="file" name="imported_file" required="required"/>
                                            <div class="input-group-append">
                                                <button type="submit" class="btn btn-info ml-auto">Import</button>
                                            </div>
                                        </div>

                                        <small class="form-text text-muted">
                                            Accepted file format: xls, xlsx, csv
                                        </small>
                                    </div>
                                </form>
                                <div class="table-responsive text-center">
                                    <table id="table-import-preview" style="display: none" class="mb-3 table table-striped">
                                        <thead><tr></tr></thead>
                                        <tbody></tbody>
                                    </table>
                                    <button style="display: none;" id="reimport-btn" class="btn btn-secondary">Reimport</button>
                                    <button style="display: none;" data-corporate-id="<?php echo $corporate->id; ?>" id="import-submit-btn" type="button" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <?php include VIEWPATH.'/template-part/admin/footer-text.php'; ?>
    </div>
</div>