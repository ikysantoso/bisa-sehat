<div class="main-content" id="panel">
    <?php include VIEWPATH.'/template-part/admin/top-nav.php'; ?>

    <!-- Header -->
    <div class="header bg-primary pb-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <?php include VIEWPATH.'/template-part/admin/breadcrumb.php'; ?>
                </div>
            </div>
        </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
                <form id="form-new-corporate" class="card">
                    <input type="hidden" name="id" value="<?php echo $corporate->id; ?>" />
                    <!-- Card header -->
                    <div class="card-header border-0">
                        <h3 class="mb-0">Edit corporate</h3>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label>Nama</label>
                            <input value="<?php echo $corporate->name; ?>" type="text" name="name" required="required" class="form-control"/>
                        </div>
                        <div class="form-group">
                            <label>Phone</label>
                            <input value="<?php echo $corporate->phone; ?>" type="text" name="phone" required="required" class="form-control"/>
                        </div>
                        <div class="form-group">
                            <label>Website</label>
                            <input value="<?php echo $corporate->website; ?>" type="text" name="website" class="form-control"/>
                        </div>
                        <div class="form-group">
                            <label>Office</label>
                            <textarea name="office" required="required" class="form-control"><?php echo $corporate->office; ?></textarea>
                        </div>
                        <div class="form-group">
                            <label>Description</label>
                            <textarea name="description" class="form-control"><?php echo $corporate->description; ?></textarea>
                        </div>
                        <div class="form-group">
                            <label class="d-block">Status</label>
                            <div class="form-check form-check-inline">
                                <input <?php echo ($corporate->status == 'waiting') ? 'checked="checked"' : ''; ?> class="form-check-input" type="radio" name="status" id="inlineRadio1" value="waiting">
                                <label class="form-check-label" for="inlineRadio1">Waiting</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input <?php echo ($corporate->status == 'approved') ? 'checked="checked"' : ''; ?> class="form-check-input" type="radio" name="status" id="inlineRadio2" value="approved">
                                <label class="form-check-label" for="inlineRadio2">Approved</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input <?php echo ($corporate->status == 'waiting') ? 'checked="rejected"' : ''; ?>class="form-check-input" type="radio" name="status" id="inlineRadio3" value="rejected">
                                <label class="form-check-label" for="inlineRadio3">Rejected</label>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer text-center">
                        <a href="<?php echo base_url('admin/corporate/list'); ?>" class="btn btn-secondary">Cancel</a>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
        
        <?php include VIEWPATH.'/template-part/admin/footer-text.php'; ?>
    </div>
</div>