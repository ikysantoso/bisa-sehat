<div class="main-content" id="panel">
    <?php include VIEWPATH.'/template-part/admin/top-nav.php'; ?>

    <!-- Header -->
    <div class="header bg-primary pb-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <?php include VIEWPATH.'/template-part/admin/breadcrumb.php'; ?>
                </div>
            </div>
        </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header border-0 d-flex align-items-center justify-content-between">
                        <h3 data-id="<?php echo $corporate->id; ?>" id="corporate_id" class="mb-0">Info Perusahaan</h3>
                        <a class="btn btn-warning btn-edit btn-sm" href="<?php echo base_url('admin/corporate/edit/'.$corporate->id); ?>"><i class="fas fa-edit"></i> Edit</a>
                    </div>
                    <div class="card-body">
                    	<div class="d-flex justify-content-between align-items-center mb-3">
                    		<div>Nama</div>
                    		<div class="flex-fill border-bottom mx-3"></div>
                    		<div><?php echo $corporate->name; ?></div>
                    	</div>
                    	<div class="d-flex justify-content-between align-items-center mb-3">
                    		<div>Phone</div>
                    		<div class="flex-fill border-bottom mx-3"></div>
                    		<div><?php echo $corporate->phone; ?></div>
                    	</div>
                    	<div class="d-flex justify-content-between align-items-center mb-3">
                    		<div>Website</div>
                    		<div class="flex-fill border-bottom mx-3"></div>
                    		<div><?php echo $corporate->website; ?></div>
                    	</div>
                    	<div class="d-flex justify-content-between align-items-center mb-3">
                    		<div>Office</div>
                    		<div class="flex-fill border-bottom mx-3"></div>
                    		<div><?php echo $corporate->office; ?></div>
                    	</div>
                    	<div class="d-flex justify-content-between align-items-center mb-3">
                    		<div>Description</div>
                    		<div class="flex-fill border-bottom mx-3"></div>
                    		<div><?php echo $corporate->description; ?></div>
                    	</div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header border-0 d-flex align-items-center justify-content-between">
                        <h3 class="mb-0">Employees</h3>
                        <div>
                            <button class="btn btn-info btn-add-member btn-sm"><i class="fas fa-plus"></i> Add Employee</button>
                            <a href="<?php echo base_url('admin/corporate/import/'.$corporate->id); ?>" class="btn btn-primary btn-sm"><i class="fas fa-file-excel"></i> Import</a>
                        </div>
                    </div>
                    <div class="table-responsive">
						<table id="table-coporate-member" class="table align-items-center table-flush">
	                    	<thead class="thead-light">
	                            <tr>
	                            	<th>#</th>
                                    <th>Nama</th>
                                    <th>Username</th>
	                                <th>Email</th>
	                                <th>Role</th>
	                                <th>ID</th>
	                                <th>#</th>
	                        	</tr>
	                        </thead>
	                        <tbody class="list"></tbody>
	                    </table>
					</div>
                </div>
            </div>
        </div>
        
        <?php include VIEWPATH.'/template-part/admin/footer-text.php'; ?>
    </div>
</div>

<div class="modal" id="modal-add-employee" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered">
        <form id="form-add-corporate-member" class="modal-content">
            <input type="hidden" name="corporate_id" value="<?php echo $corporate->id; ?>"/>
            <div class="modal-header">
                <h5 class="modal-title">Add New Employee</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>User</label>
                    <input type="text" class="multipleSelect" multiple data-url="<?php echo base_url('admin/user/unemployed_user'); ?>" name="user_id" required="required"/>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
        </form>
    </div>
</div>