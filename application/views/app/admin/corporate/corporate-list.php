<div class="main-content" id="panel">
    <?php include VIEWPATH.'/template-part/admin/top-nav.php'; ?>

    <!-- Header -->
    <div class="header bg-primary pb-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <?php include VIEWPATH.'/template-part/admin/breadcrumb.php'; ?>
                    <div class="col-lg-6 col-5 text-right">
                        <a href="<?php echo base_url('admin/corporate/new'); ?>" class="btn btn-sm btn-neutral" id="btn-add-new"><i class="fas fa-plus"></i> New</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header border-0">
                        <!-- <h3 class="mb-0"></h3> -->
                    </div>
                    <!-- Light table -->
                    <div class="table-responsive">
                        <table id="table-slider-list" class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th>#</th>
                                    <th>Nama</th>
                                    <th>Employee Total</th>
                                    <th>Status</th>
                                    <th>Date Created</th>
                                    <th>ID</th>
                                    <th>#</th>
                                </tr>
                            </thead>
                            <tbody class="list"></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        
        <?php include VIEWPATH.'/template-part/admin/footer-text.php'; ?>
    </div>
</div>

<div id="corporate-approval-modal" class="modal" tabindex="-1">
    <div class="modal-dialog">
        <form id="corporate-approval-form">
            <input type="hidden" name="id"/>
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Corporate Approval Status</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" name="name" class="form-control" disabled="disabled"/>
                    </div>
                    <div class="form-group">
                        <label class="d-block">Status</label>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="status" id="inlineRadio1" value="waiting">
                            <label class="form-check-label" for="inlineRadio1">Waiting</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="status" id="inlineRadio2" value="approved">
                            <label class="form-check-label" for="inlineRadio2">Approve</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="status" id="inlineRadio3" value="rejected">
                            <label class="form-check-label" for="inlineRadio3">Reject</label>
                        </div>
                    </div>
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </form>
    </div>
</div>