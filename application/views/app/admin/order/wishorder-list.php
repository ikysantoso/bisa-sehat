<div class="main-content" id="panel">
    <?php include VIEWPATH.'/template-part/admin/top-nav.php'; ?>

    <!-- Header -->
    <div class="header bg-primary pb-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <?php include VIEWPATH.'/template-part/admin/breadcrumb.php'; ?>
                </div>
            </div>
        </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header border-0">
                        <h3 class="mb-0">Wishorder List</h3>
                    </div>
                    <!-- Light table -->
                    <div class="table-responsive">
                        <table id="table-wishorder" class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th>#</th>
                                    <th class="text-center">User</th>
                                    <th>Nama Barang</th>
                                    <th class="text-right">Harga Yang Diinginkan</th>
                                    <th class="text-right">Qty</th>
                                    <th class="text-left">Unit</th>
                                    <th class="text-center">Tanggal</th>
                                    <th class="text-center">Status</th>
                                    <th>ID</th>
                                    <th>Linked Product</th>
                                </tr>
                            </thead>
                            <tbody class="list"></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        
        <?php include VIEWPATH.'/template-part/admin/footer-text.php'; ?>
    </div>
</div>

<div class="modal" id="modal-link-to-product" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
        	<form id="form-link-to-product">
                <input type="hidden" name="id"/>
	            <div class="modal-header">
	                <h5 class="modal-title">Link to Product</h5>
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                <span aria-hidden="true">&times;</span>
	                </button>
	            </div>
	            <div class="modal-body">
                    <div class="form-group">
                        <div class="d-flex justify-content-between align-items-center mb-3">
                            <label>Product</label>
                            <button type="button" id="btn-link-to-new-product" class="btn btn-sm btn-outline-info">Link to new product</button>
                        </div>
                        <input type="text" class="singleInputDynamic" data-url="<?php echo base_url('admin/product/product_fastselect'); ?>" name="product_id" required="required"/>
                    </div>
	            </div>
	            <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
	                <button type="submit" class="btn btn-primary">Save changes</button>
	            </div>
        	</form>
        </div>
    </div>
</div>