						<div class="btn-group" role="group" aria-label="Basic example">
                            <a href="<?php echo base_url('admin/order') ?>" class="btn btn-sm btn-secondary <?php echo ($page == 'admin-order') ? 'active' : ''; ?>">All</a>
                            <a href="<?php echo base_url('admin/order/waiting_confirmation') ?>" class="btn btn-sm btn-secondary <?php echo ($page == 'admin-order-waiting-confirmation') ? 'active' : ''; ?>">Waiting Confirmation</a>
                        </div>