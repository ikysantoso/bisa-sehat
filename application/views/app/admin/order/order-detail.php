<div class="main-content" id="panel">
    <?php include VIEWPATH.'/template-part/admin/top-nav.php';
    $payment_status = ($transaction->payment) ? $transaction->payment->status : $transaction->payment_status;
    ?>

    <!-- Header -->
    <div class="header bg-primary pb-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <?php include VIEWPATH.'/template-part/admin/breadcrumb.php'; ?>
                </div>
            </div>
        </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header border-0 d-flex justify-content-between align-items-center">
                        <h3 class="mb-0"><?php echo ($transaction->is_preorder == 'yes') ? 'Preorder' : 'Order'; ?> Detail</h3>
                        <?php if( !$transaction->tracking_code && $payment_status == 'paid' ): ?>
                        <button data-toggle="modal" data-target="#modal-tracking-code" class="btn btn-info btn-sm"><i class="fas fa-truck" __cpp="1"></i> Input Resi Pengiriman</button>
                        <?php endif; ?>
                    </div>
                    <div style="font-size: 14px" class="card-body">
                        <div class="row">

                            <div class="col-md-6 mb-4">
                                <h5 class="mb-3">Detail Order</h5>
                                <div class="border p-3 rounded">
                                    <div class="mb-2 d-flex justify-content-between align-items-center">
                                        <b>Order ID</b>
                                        <div class="flex-fill border mx-3"></div>
                                        <span>#<?php echo $transaction->transaction_id; ?></span>
                                    </div>
                                    <div class="mb-2 d-flex justify-content-between align-items-center">
                                        <b>Tanggal Order</b>
                                        <div class="flex-fill border mx-3"></div>
                                        <span><?php echo $transaction->created_at; ?></span>
                                    </div>
                                    <div class="mb-2 d-flex justify-content-between align-items-center">
                                        <b>Gudang</b>
                                        <div class="flex-fill border mx-3"></div>
                                        <span><?php echo $warehouse->name; ?></span>
                                    </div>
                                    <div class="mb-2 d-flex justify-content-between align-items-center">
                                        <b>Payment Method</b>
                                        <div class="flex-fill border mx-3"></div>
                                        <span><?php echo isset($transaction->payment->payment_method) ? $transaction->payment->bank->account_name : 'Payment Gateway'; ?></span>
                                    </div>
                                    <div class="mb-2 d-flex justify-content-between align-items-center">
                                        <b>Status</b>
                                        <div class="flex-fill border mx-3"></div>
                                        <span>
                                            <?php
                                                $transaction_status = ($transaction->status == 'pengemasan' && $transaction->is_preorder=='yes') ? 'diproses' : $transaction->status;
                                                echo transaction_status_icon( ($payment_status == 'paid') ? $transaction_status : $payment_status );
                                            ?>
                                    </div>

                                    <?php if($transaction->status=='in delivery' || $transaction->status=='delivered'): ?>
                                    <div class="mb-2 d-flex justify-content-between align-items-center">
                                        <b>Resi Pengiriman</b>
                                        <div class="flex-fill border mx-3"></div>
                                        <span><?php echo $transaction->shipping_courier." #".$transaction->tracking_code; ?></span>
                                    </div>
                                    <?php endif; ?>

                                    <?php if($transaction->is_preorder == 'yes' && $transaction->status != 'cancelled'): ?>
                                    <div class="mb-2 d-flex justify-content-between align-items-center">
                                        <b>Sisa Waktu Preorder</b>
                                        <div class="flex-fill border mx-3"></div>
                                        <span><?php countdown( date('Y-m-d H:i:s', strtotime($transaction->created_at. ' + 3 days')) ); ?></span>
                                    </div>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <div class="col-md-6 mb-4">
                                <h5 class="mb-3">Alamat Pengiriman</h5>
                                <?php
                                    $address = json_decode($transaction->customer_address_data);
                                    $this->load->model( 'WilayahProvinsiModel' );
                                    $province = WilayahProvinsiModel::where( 'province_id',$address->kabupaten->province_id )->first();
                                    ?>
                                <div class="card border shadow-none">
                                    <div class="card-body p-3">
                                        <div class="text-capitalize"><?php echo $address->name; ?> - <b><?php echo $address->receiver_name; ?></b> </div>
                                        <?php
                                        echo $address->address.'<br />'.
                                             $address->village_text.' - '.
                                             $address->kecamatan->subdistrict_name.' - '.
                                             $address->kabupaten->city_name.'<br/>'.$province->province.' <br/> '.
                                             'Phone: '.$address->phone
                                        ?>
                                    </div>
                                </div>

                                <h5 class="mb-3">Payment Confirmation</h5>
                                <?php if(isset($transaction->payment_image)):
                                    echo '<img src="../../../assets/images/transactions/product/'.$transaction->payment_image.'"style="margin-top: 10px; width: 100%;" alt="Payment Confirmation Image">'
                                ?>
                                <?php else: ?>
                                    <h6>No Confirmation</h6>
                                <?php endif; ?>

                                <?php if($transaction->status == 'waiting'): ?>
                                <div style="margin-top: 10px;">
                                    <button type="button" class="btn btn-primary" onclick="approvalOrder(<?php echo $transaction->id ?>, 'approve');">Approve</button>
                                    <button type="button" class="btn btn-danger" onclick="approvalOrder(<?php echo $transaction->id ?>, 'decline');">Decline</button>
                                </div>
                                <?php endif; ?>
                            </div>

                            <div class="col-12 mb-4">
                                <h5 class="mb-3">Products</h5>

                                <?php
                                $total_ongkos_kirim = 0;
                                $total_produk = 0;

                                foreach (($transaction->payment->status == 'paid') ? [$transaction] : $transaction->payment->transactions as $key => $transaction): ?>
                                <table class="table table-striped table-bordered mb-3">
                                    <thead>
                                        <tr>
                                            <td class="text-center">#</td>
                                            <td>Product</td>
                                            <td class="text-center">Quantity</td>
                                            <td class="text-center">Weight (gram)</td>
                                            <td class="text-right">Price</td>
                                            <td class="text-right">Sub Total</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach($transaction->transaction_items as $key => $transaction_item): ?>
                                        <tr>
                                            <td class="text-center"><?php echo $key+1; ?></td>
                                            <td><?php echo ($transaction_item->product) ? $transaction_item->product->name : '--deleted product--'; ?></td>
                                            <td class="text-center"><?php echo $transaction_item->quantity; ?></td>
                                            <td class="text-center"><?php echo isset($transaction->weight) ? $transaction->weight : '-'; ?></td>
                                            <td class="text-right"><?php echo rupiah($transaction_item->price); ?></td>
                                            <td class="text-right"><?php echo rupiah($transaction_item->quantity * $transaction_item->price); ?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="5" class="text-right">Product Total</td>
                                            <td class="text-right"><?php echo rupiah($transaction->total()); ?></td>
                                        </tr>
                                        <tr>
                                            <td colspan="5" class="text-right">Ongkos Kirim <?php echo $transaction->shipping_courier; ?></td>
                                            <?php if($transaction->shipping_courier === "Non Jabodetabek"): ?>
                                                <td class="text-right font-weight-bold hint-non-jabodetabek" style="color: red; font-size: 20pt;">!</td>
                                            <?php else: ?>
                                                <td class="text-right"><?php echo rupiah($transaction->shipping_fee); ?></td>
                                            <?php endif; ?>
                                        </tr>
                                        <tr>
                                            <th colspan="5" class="text-right">Total</th>
                                            <th class="text-right"><?php echo rupiah($transaction->total() + $transaction->shipping_fee + $transaction->payment_unique_code - ((isset($transaction->voucher->nominal)) ? $transaction->voucher->nominal : 0)); ?></th>
                                        </tr>
                                    </tfoot>
                                </table>
                                <?php
                                    $total_produk += $transaction->total();
                                    $total_ongkos_kirim += $transaction->shipping_fee;
                                endforeach; ?>

                                <?php if($payment_status != 'paid' || 'unpaid'): ?>
                                <div class="shadow-sm border p-3 mx-auto mb-4">
                                        <div class="d-flex justify-content-between align-items-center mb-3">
                                            <b>Total Produk</b>
                                            <div class="border-bottom mx-3 flex-fill"></div>
                                            <b><?php echo rupiah($total_produk); ?></b>
                                        </div>
                                        <div class="d-flex justify-content-between align-items-center mb-3">
                                            <b>Total Ongkos Kirim</b>
                                            <div class="border-bottom mx-3 flex-fill"></div>
                                            <b><?php echo rupiah($total_ongkos_kirim); ?></b>
                                        </div>

                                        <?php
                                        $unique_code = $transaction->payment->unique_code;
                                        ?>

                                        <?php if($unique_code != 0): ?>
                                        <div class="d-flex justify-content-between align-items-center mb-3">
                                            <b>Kode Unik</b>
                                            <div class="border-bottom mx-3 flex-fill"></div>
                                            <b><?php echo rupiah($unique_code); ?></b>
                                        </div>
                                        <?php endif; ?>

                                        <?php
                                        $diskon = ($transaction->payment->voucher_user_id) ? ($transaction->payment->voucher_user->voucher->nominal) : 0;
                                        ?>
                                        <?php if($diskon != 0): ?>
                                        <div class="d-flex justify-content-between align-items-center mb-3">
                                            <b>Diskon (<?php echo $transaction->payment->voucher_user->voucher->name; ?>)</b>
                                            <div class="border-bottom mx-3 flex-fill"></div>
                                            <b><?php echo rupiah($diskon * -1); ?></b>
                                        </div>
                                        <?php endif; ?>

                                        <div class="d-flex justify-content-between align-items-center">
                                            <h3 class="mb-0">Grand Total</h3>
                                            <div class="border-bottom mx-3 flex-fill"></div>
                                            <h3 class="mb-0"><?php echo rupiah($total_produk + $total_ongkos_kirim + $unique_code - $diskon); ?></h3>
                                        </div>
                                </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php include VIEWPATH.'/template-part/admin/footer-text.php'; ?>
    </div>
</div>
<div class="modal" id="modal-tracking-code" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="form-tracking-code">
                <input type="hidden" name="id" value="<?php echo $transaction->id ?>" />
                <div class="modal-header">
                    <h5 class="modal-title">Resi Pengiriman</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Resi Pengiriman</label>
                        <input required="required" type="text" value="<?php echo $transaction->tracking_code ?>" class="form-control" name="tracking_code"/>
                    </div>
                    <div class="form-group">
                        <label>Jasa Pengiriman</label>
                        <input required="required" type="text" value="<?php echo $transaction->delivery_service ?>" class="form-control" name="delivery_service"/>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>