<div class="main-content" id="panel">
    <?php include VIEWPATH.'/template-part/admin/top-nav.php'; ?>

    <!-- Header -->
    <div class="pb-6 header bg-primary">
        <div class="container-fluid">
            <div class="header-body">
                <div class="py-4 row align-items-center">
                    <?php include VIEWPATH.'/template-part/admin/breadcrumb.php'; ?>
                </div>
            </div>
        </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col-xl-8">
                <div class="card">
                    <!-- Card header -->
                    <div class="border-0 card-header">
                        <h3 class="mb-0"><i class="fas fa-edit"></i> Edit Product</h3>
                    </div>
                   	<div class="card-body">
                    	<form id="form-add-product">
                    		<input type="hidden" name="product_id" value="<?php echo $product->id; ?>"/>
		                    <div class="row">

		                        <div class="col-md-6">
		                            <div class="form-group">
				                        <label>Name</label>
				                    	<input class="form-control" type="text" value="<?php echo $product->name; ?>" name="name" required="required"/>
				                    </div>
		                        </div>

		                        <div class="col-md-6">
									<div class="form-group">
		                        		<label>Category</label>
		                                <input value="<?php echo $product->category_id; ?>" data-user-option-allowed="true" type="text" class="singleInputDynamic" data-initial-value='<?php echo json_encode(['value' => $product->category_id, 'text' => $product->category->name]) ?>' data-url="<?php echo base_url('admin/product/category_fastselect'); ?>" name="category" required="required"/>
		                           	</div>
								</div>

								<div class="col-md-6">
		                        	<div class="form-group">
		                                <label>Purchase Price</label>
		                            	<input class="form-control" type="number" name="purchase_price" value="<?php echo $product->purchase_price ?>" id="purchase_price" required="required"/>
		                            </div>
		                        </div>
								<div class="col-md-6">
		                        	<div class="form-group">
		                                <label>Purchase VAT</label>
		                            	<input class="form-control" type="number" name="purchase_vat" value="<?php echo $product->purchase_vat ?>" id="purchase_vat" required="required"/>
		                            </div>
		                        </div>
								<div class="col-md-6">
		                        	<div class="form-group">
		                                <label>Margin</label>
		                            	<input class="form-control" type="number" name="margin" value="<?php echo $product->margin ?>" id="margin" required="required"/>
		                            </div>
		                        </div>
								<div class="col-md-6">
		                        	<div class="form-group">
		                                <label>Sales VAT</label>
		                            	<input class="form-control" type="number" name="sales_vat" value="<?php echo $product->sales_vat ?>" id="sales_vat" required="required"/>
		                            </div>
		                        </div>

		                        <div class="col-md-6">
		                        	<div class="form-group">
		                                <label>Weight (g)</label>
		                            	<input class="form-control" value="<?php echo $product->weight; ?>" min="0" type="number" name="weight" required="required"/>
		                            </div>
		                        </div>

								<div class="col-md-6">
		                        	<div class="form-group">
		                                <label>Price After Discount</label>
		                            	<div class="input-group md-6">
											<input class="form-control" min="0" type="number" name="discount" value="<?php echo $product->discount; ?>" max="<?php echo $product->price; ?>"/>
											<div class="input-group-append">
												<span class="input-group-text" id="basic-addon2">Rp</span>
											</div>
										</div>
		                            </div>
		                        </div>

								<div class="col-md-3">
		                        	<div class="form-group">
		                                <label>Start Date Sales</label>
		                            	<div class="input-group md-3">
											<input class="form-control" type="date" min="<?= $product->start_date_sales ?>"  name="start_date_sales"  value="<?php echo $product->start_date_sales ?>"/>
											<div class="input-group-append">
											</div>
										</div>
		                            </div>
		                        </div>
								<div class="col-md-3">
		                        	<div class="form-group">
		                                <label>End Date Sales</label>
		                            	<div class="input-group md-3">
											<input class="form-control" type="date" min="<?= date('Y-m-d')?>"  name="end_date_sales" value="<?php echo $product->end_date_sales ?>"/>
											<div class="input-group-append">
											</div>
										</div>
		                            </div>
		                        </div>
		                        <div class="col-md-6">
		                        	<div id="preorder-time" class="form-group">
		                        		<label>Preorder Time <small>( Jika stok di gudang kosong)</small></label>
			                        	<div class="mb-3 input-group">
										    <input type="number" name="preorder-time" value="<?php echo $product->preorder_time; ?>" class="form-control" placeholder="Waktu yang dibutuhkan preorder" aria-describedby="basic-addon2"/>
										    <div class="input-group-append">
										        <span class="input-group-text" id="basic-addon2">Hari</span>
										    </div>
										</div>
									</div>
		                        </div>

		                        <div class="col-12">
		                            <div class="form-group">
		                                <label>Description</label>
		                                <div id="editor"><?php echo $product->description; ?></div>
		                                <input type="hidden" class="description-field" name="description"/>
		                            </div>
		                        </div>

		                        <div class="col-md-6">
		                        	<div class="form-group">
		                            	<label>Images</label>
			                            <div class="row input-img-container">
			                            	<?php
			                            	$current_images = [];
			                            	foreach ($product->images as $key => $image):
			                            		$current_images[] = $image->id;
			                            	?>
			                                <div class="mb-3 input-img-item col-4">
			                                    <div class="rounded bg-light embed-responsive embed-responsive-4by3 bg-vertical-center" style="background-image: url('<?php echo base_url('assets/images/upload/product/'.$image->image) ?>')">
			                                        <button style="display: none" onclick="$(this).parent().find('.input-img').click();" type="button" class="btn btn-upload-img"><i class="fas fa-plus"></i></button>
			                                        <input class="input-img" type="file" accept="image/*" style="visibility: hidden;position: absolute;" name="image[]">
			                                        <i title="Remove image" data-id="<?php echo $image->id; ?>" class="fas fa-trash btn-remove-img"></i>
			                                    </div>
			                                </div>
			                            	<?php endforeach; ?>

			                            	<input type="hidden" name="current_images" value="<?php echo implode(",", $current_images) ?>">

			                                <div id="field-image" class="mb-3 input-img-item col-4">
			                                    <div class="rounded bg-light embed-responsive embed-responsive-4by3 bg-vertical-center">
			                                        <button onclick="$(this).parent().find('.input-img').click();" type="button" class="btn btn-upload-img"><i class="fas fa-plus"></i></button>
			                                        <input class="input-img" type="file" accept="image/*" style="visibility: hidden;position: absolute;" name="image[]">
			                                        <i title="Remove image" style="display: none" class="fas fa-trash btn-remove-img"></i>
			                                    </div>
			                                </div>
			                            </div>
		                            </div>
		                        </div>

		                        <div class="col-md-6">
		                          	<div class="form-group d-flex justify-content-between">
		                            	<label>Stock</label>
		                            	<?php
                    					$stocks = [];
		                            	foreach ($product->stocks as $key => $stock) {
		                            		$stocks[] = [
		                            			'warehouse_id'	=> $stock->warehouse_id,
		                            			'warehouse_name'=> $stock->warehouse->name,
		                            			'stock'			=> $stock->last_stock
		                            		];
		                            	}
		                            	?>
		                            	<input type="hidden" value='<?php echo json_encode($stocks) ?>' name="stocks">
		                            	<button type="button" id="btn-add-stock" class="btn btn-sm btn-info">Add new stock</button>
		                            </div>
		                            <ul id="stock-list" class="list-group"></ul>
								</div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Suggestion For</label>
                                        <select id="suggestion_for" class="form-control" name="suggestion_for">
                                            <option value="" <?php echo $product->suggestion_for == "" ? "selected" : "" ?>>
                                                Tidak Ada Saran
                                            </option>
                                            <option value="obesity" <?php echo $product->suggestion_for == "obesity" ? "selected" : "" ?>>
                                                Obesitas
                                            </option>
                                            <option value="high_blood_pressure" <?php echo $product->suggestion_for == "high_blood_pressure" ? "selected" : "" ?>>
                                                Tekanan Darah
                                            </option>
                                            <option value="blood_sugar" <?php echo $product->suggestion_for == "blood_sugar" ? "selected" : "" ?>>
                                                Gula Darah
                                            </option>
                                            <option value="cholesterol" <?php echo $product->suggestion_for == "cholesterol" ? "selected" : "" ?>>
                                                Kolesterol
                                            </option>
                                            <option value="uric_acid" <?php echo $product->suggestion_for == "uric_acid" ? "selected" : "" ?>>
                                                Asam Urat
                                            </option>
                                        </select>
                                    </div>
                                </div>

		                    </div>
		                    <a href="<?php echo base_url('admin/product/list') ?>" class="btn btn-link" data-dismiss="modal">Back</a>
		                    <button type="submit" class="ml-auto btn btn-primary">Save</button>
		            	</form>
		            </div>
                </div>
            </div>
        </div>

        <?php include VIEWPATH.'/template-part/admin/footer-text.php'; ?>
    </div>
</div>

<div class="modal" id="modal-stock" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
        	<form id="form-stock">
	            <div class="modal-header">
	                <h5 class="modal-title">Add stock</h5>
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                <span aria-hidden="true">&times;</span>
	                </button>
	            </div>
	            <div class="modal-body">
	            	<div class="form-group">
	            		<label>Warehouse</label>
	            		<select required="required" class="form-control" name="warehouse">
	            			<option value="0" selected="selected" disabled="disabled">--Choose warehouse--</option>
	            			<?php foreach ($warehouses as $key => $warehouse): ?>
	            			<option value="<?php echo $warehouse->id ?>"><?php echo $warehouse->name ?></option>
	            			<?php endforeach; ?>
	            		</select>
	            	</div>
	            	<div class="form-group">
	            		<label>Stok</label>
	            		<input type="number" value="0" min="0" class="form-control" name="stock" required="required"/>
	            	</div>
	            </div>
	            <div class="modal-footer">
	                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	                <button type="submit" class="btn btn-primary">Save changes</button>
	            </div>
	        </form>
        </div>
    </div>
</div>