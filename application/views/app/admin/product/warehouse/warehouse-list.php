<div class="main-content" id="panel">
    <?php include VIEWPATH.'/template-part/admin/top-nav.php'; ?>

    <!-- Header -->
    <div class="header bg-primary pb-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <?php include VIEWPATH.'/template-part/admin/breadcrumb.php'; ?>

                    <div class="col-lg-6 col-5 text-right">
                        <button class="btn btn-sm btn-neutral" id="btn-add-new"><i class="fas fa-plus"></i> New</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header border-0">
                        <h3 class="mb-0">Warehouse List</h3>
                    </div>
                    <!-- Light table -->
                    <div class="table-responsive">
                        <table id="table-warehouse-list" class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Address</th>
                                    <th>ID</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody class="list"></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <?php include VIEWPATH.'/template-part/admin/footer-text.php'; ?>
    </div>
</div>

<div class="modal" id="modal-warehouse" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
        	<form id="form-warehouse">
                <input type="hidden" name="id"/>
	            <div class="modal-header">
	                <h5 class="modal-title">Add new warehouse</h5>
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                <span aria-hidden="true">&times;</span>
	                </button>
	            </div>
            	<div class="modal-body">
            		<div class="row">
	            		<div class="col-md-6">
			            	<div class="form-group">
			            		<label>Name</label>
			            		<input type="text" class="form-control" name="name" placeholder="Warehouse Name" required="required"/>
			            	</div>
	            		</div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Provinsi</label>
                                <input required="required" type="text" data-url="<?php echo base_url('api/wilayah/province'); ?>" data-load-once="true" name="province" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Kabupaten/Kota</label>
                                <input required="required" type="text" class="form-control" disabled="disabled" name="district" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Kecamatan</label>
                                <input required="required" type="text" class="form-control" disabled="disabled" name="sub-district" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Kelurahan/Desa</label>
                                <input required="required" type="text" class="form-control" disabled="disabled" name="village" />
                            </div>
                            <div class="form-group">
                            	<label>Kodepos</label>
                                <input required="required" type="number" min="1" max="99999" minlength="5" maxlength="5" class="form-control" disabled="disabled" name="zipcode" />
                        	</div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Alamat</label>
                                <textarea style="min-height: 148px;" required="required" class="form-control" placeholder="Nama Jalan/RT/RW" disabled="disabled" name="address"></textarea>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Metode Pengiriman</label>
                                <div style="display: flex;">
                                    <div style="padding-right: 10px;">
                                        <div class="radio-inline">
                                            <input type="checkbox" class="shipping-method-checkbox" name="reguler" value="Reguler" id="reguler-shipping" data-waschecked="false"> Reguler
                                        </div>
                                        <div class="radio-inline">
                                            <input type="checkbox" class="shipping-method-checkbox" name="cargo" value="Cargo" id="cargo-shipping" data-waschecked="false"> Cargo
                                        </div>
                                        <div class="radio-inline">
                                            <input type="checkbox" class="shipping-method-checkbox" name="instan" value="Instan" id="instan-shipping" data-waschecked="false"> Instan
                                        </div>
                                    </div>
                                    <div>
                                        <div class="radio-inline">
                                            <input type="checkbox" class="shipping-method-checkbox" name="pickup" value="Pickup" id="pickup-shipping" data-waschecked="false"> Pickup
                                        </div>
                                        <div class="radio-inline">
                                            <input type="checkbox" class="shipping-method-checkbox" name="close" value="Close" id="close-shipping" data-waschecked="false"> Close
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
            		</div>
	            </div>
	            <div class="modal-footer">
	                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	                <button type="submit" class="btn btn-primary">Save changes</button>
	            </div>
	       	</form>
        </div>
    </div>
</div>