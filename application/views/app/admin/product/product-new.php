<div class="main-content" id="panel">
    <?php include VIEWPATH.'/template-part/admin/top-nav.php'; ?>

    <!-- Header -->
    <div class="pb-6 header bg-primary">
        <div class="container-fluid">
            <div class="header-body">
                <div class="py-4 row align-items-center">
                    <?php include VIEWPATH.'/template-part/admin/breadcrumb.php'; ?>
                </div>
            </div>
        </div>
    </div>

    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col-xl-8">
                <div class="card">
                    <!-- Card header -->
                    <div class="border-0 card-header">
                        <h3 class="mb-0"><i class="fas fa-plus"></i> Add New Product</h3>
                    </div>
                   	<div class="card-body">
                    	<form id="form-add-product">
                    		<?php
                    			if ( $wishorder ) {
                    			echo '<input type="hidden" name="wishorder_id" value="'.$wishorder->id.'"/>';
                    			}
                    		?>
		                    <div class="row">
		                        <div class="col-md-6">
		                            <div class="form-group">
				                        <label>Name</label>
				                    	<input class="form-control" value="<?php echo ($wishorder) ? $wishorder->name : ''; ?>" type="text" name="name" required="required"/>
				                    </div>
		                        </div>
								<div class="col-md-6">
									<div class="form-group">
		                        		<label>Category</label>
		                                <input type="text" class="singleInputDynamic" data-url="<?php echo base_url('admin/product/category_fastselect'); ?>" name="category" required="required"/>
		                           	</div>
								</div>
								<div class="col-md-6">
		                        	<div class="form-group">
		                                <label>Purchase Price</label>
		                            	<input class="form-control" value="0" type="number" name="purchase_price" id="purchase_price" required="required"/>
		                            </div>
		                        </div>
								<div class="col-md-6">
		                        	<div class="form-group">
		                                <label>Purchase VAT</label>
		                            	<input class="form-control" value="10" type="number" name="purchase_vat" id="purchase_vat" required="required"/>
		                            </div>
		                        </div>
								<div class="col-md-6">
		                        	<div class="form-group">
		                                <label>Margin</label>
		                            	<input class="form-control" value="0" type="number" name="margin" id="margin" required="required"/>
		                            </div>
		                        </div>
								<div class="col-md-6">
		                        	<div class="form-group">
		                                <label>Sales VAT</label>
		                            	<input class="form-control" value="10" type="number" name="sales_vat" id="sales_vat" required="required"/>
		                            </div>
		                        </div>	                        


								<div class="col-md-6">
		                        	<div class="form-group">
		                                <label>Weight (g)</label>
		                            	<input class="form-control" min="0" type="number" name="weight" required="required"/>
		                            </div>
		                        </div>

								<div class="col-md-6">
		                        	<div class="form-group">
		                                <label>Price After Discount</label>
		                            	<div class="input-group md-6">
											<input class="form-control" min="0" type="number" name="discount"/>
											<div class="input-group-append">
												<span class="input-group-text" id="basic-addon2">Rp</span>
											</div>
										</div>
		                            </div>
		                        </div>

								<div class="col-md-3">
		                        	<div class="form-group">
		                                <label>Start Date Sales</label>
		                            	<div class="input-group md-3">
											<input class="form-control" type="date" min="<?= date('Y-m-d')?>" value="<?= date('Y-m-d')?>" name="start_date_sales"/>
											<div class="input-group-append">
											</div>
										</div>
		                            </div>
		                        </div>
								<div class="col-md-3">
		                        	<div class="form-group">
		                                <label>End Date Sales</label>
		                            	<div class="input-group md-3">
											<input class="form-control" type="date" min="<?= date('Y-m-d')?>" value="<?= date('Y-m-d')?>" name="end_date_sales"/>
											<div class="input-group-append">
											</div>
										</div>
		                            </div>
		                        </div>

		                        <div class="col-md-6">
		                        	<div id="preorder-time" class="form-group">
		                        		<label>Preorder Time <small>( Jika stok di gudang kosong)</small></label>
			                        	<div class="mb-3 input-group">
										    <input min="0" type="number" name="preorder-time" class="form-control" placeholder="Waktu yang dibutuhkan preorder" aria-describedby="basic-addon2"/>
										    <div class="input-group-append">
										        <span class="input-group-text" id="basic-addon2">Hari</span>
										    </div>
										</div>
									</div>
		                        </div>

		                        <div class="col-12">
		                            <div class="form-group">
		                                <label>Description</label>
		                                <div id="editor"></div>
		                                <input type="hidden" class="description-field" name="description"/>
		                            </div>
		                        </div>

		                        <div class="col-md-6">
		                        	<div class="form-group">
		                            	<label>Images</label>
			                            <div class="row input-img-container">
			                                <div class="input-img-item col">
			                                    <div class="rounded bg-light embed-responsive embed-responsive-4by3 bg-vertical-center">
			                                        <button onclick="$(this).parent().find('.input-img').click();" type="button" class="btn btn-upload-img"><i class="fas fa-plus"></i></button>
			                                        <input class="input-img" type="file" accept="image/*" style="visibility: hidden;position: absolute;" name="image[]">
			                                        <i title="Remove image" style="display: none" class="fas fa-trash btn-remove-img"></i>
			                                    </div>
			                                </div>
			                            </div>
		                            </div>
		                        </div>

		                        <div class="col-md-6">
		                          	<div class="form-group d-flex justify-content-between">
		                            	<label>Stock</label>
		                            	<input type="hidden" name="stocks">
		                            	<button type="button" id="btn-add-stock" class="btn btn-sm btn-info">Add new stock</button>
		                            </div>
		                            <ul id="stock-list" class="list-group"></ul>
								</div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Suggestion For</label>
                                        <select id="suggestion_for" class="form-control" name="suggestion_for">
                                            <option value="">
                                                Tidak Ada Saran
                                            </option>
                                            <option value="obesity">
                                                Obesitas
                                            </option>
                                            <option value="high_blood_pressure">
                                                Tekanan Darah
                                            </option>
                                            <option value="blood_sugar">
                                                Gula Darah
                                            </option>
                                            <option value="cholesterol">
                                                Kolesterol
                                            </option>
                                            <option value="uric_acid">
                                                Asam Urat
                                            </option>
                                        </select>
                                    </div>
                                </div>
		                    </div>

							<a href="<?php echo base_url('admin/product/list') ?>" class="btn btn-link" data-dismiss="modal">Back</a>
		                    <button type="submit" class="ml-auto btn btn-primary">Save</button>
		            	</form>
		            </div>
                </div>
            </div>
        </div>

        <?php include VIEWPATH.'/template-part/admin/footer-text.php'; ?>
    </div>
</div>

<div class="modal" id="modal-stock" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
        	<form id="form-stock">
	            <div class="modal-header">
	                <h5 class="modal-title">Add stock</h5>
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                <span aria-hidden="true">&times;</span>
	                </button>
	            </div>
	            <div class="modal-body">
	            	<div class="form-group">
	            		<label>Warehouse</label>
	            		<select required="required" class="form-control" name="warehouse">
	            			<option value="0" selected="selected" disabled="disabled">--Choose warehouse--</option>
	            			<?php foreach ($warehouses as $key => $warehouse): ?>
	            			<option value="<?php echo $warehouse->id ?>"><?php echo $warehouse->name ?></option>
	            			<?php endforeach; ?>
	            		</select>
	            	</div>
	            	<div class="form-group">
	            		<label>Stok</label>
	            		<input type="number" value="0" min="0" class="form-control" name="stock" required="required"/>
	            	</div>
	            </div>
	            <div class="modal-footer">
	                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	                <button type="submit" class="btn btn-primary">Save changes</button>
	            </div>
	        </form>
        </div>
    </div>
</div>