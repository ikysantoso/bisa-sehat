<div class="main-content" id="panel">
    <?php include VIEWPATH.'/template-part/admin/top-nav.php'; ?>

    <!-- Header -->
    <div class="pb-6 header bg-primary">
        <div class="container-fluid">
            <div class="header-body">
                <div class="py-4 row align-items-center">
                    <?php include VIEWPATH.'/template-part/admin/breadcrumb.php'; ?>

                    <div class="text-right col-lg-6 col-5">
                        <a href="<?php echo base_url('admin/product/new'); ?>" class="btn btn-sm btn-neutral" id="btn-add-new"><i class="fas fa-plus"></i> New</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
                <div class="card">
                    <!-- Card header -->
                    <div class="border-0 card-header">
                        <h3 class="mb-0">Product List</h3>
                    </div>
                    <!-- Light table -->
                    <div class="table-responsive">
                        <table id="table-product-list" class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th>#</th>
                                    <th>Product Name</th>
                                    <th>Price</th>
                                    <th>Category</th>
                                    <th>ID</th>
                                    <th>Price After Discount</th>
                                    <th>First Stock</th>
                                    <th>Last Stock</th>
                                    <th style="color: green;">In Stock</th>
                                    <th style="color: red;">Out Stock</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody class="list"></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <?php include VIEWPATH.'/template-part/admin/footer-text.php'; ?>
    </div>
</div>