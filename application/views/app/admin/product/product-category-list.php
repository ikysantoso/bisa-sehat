<div class="main-content" id="panel">
    <?php include VIEWPATH.'/template-part/admin/top-nav.php'; ?>

    <!-- Header -->
    <div class="header bg-primary pb-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <?php include VIEWPATH.'/template-part/admin/breadcrumb.php'; ?>
                    
                    <div class="col-lg-6 col-5 text-right">
                        <button class="btn btn-sm btn-neutral" id="btn-add-new"><i class="fas fa-plus"></i> New</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header border-0">
                        <h3 class="mb-0">Product Category List</h3>
                    </div>
                    <!-- Light table -->
                    <div class="table-responsive">
                        <table id="table-product-category-list" class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th>#</th>
                                    <th>Category Name</th>
                                    <th>ID</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody class="list"></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        
        <?php include VIEWPATH.'/template-part/admin/footer-text.php'; ?>
    </div>
</div>

<div class="modal fade" id="modal-add-new" tabindex="-1" role="dialog" aria-labelledby="modal-default" aria-hidden="true">
    <div class="modal-dialog modal- modal-dialog-centered modal-" role="document">
        <div class="modal-content">
            <form id="form-add-product-category">
                <input type="hidden" name="id">
                <div class="modal-header">
                    <h6 class="modal-title" id="modal-title-default"><i class="fas fa-plus"></i> Add new product category</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Name</label>
                        <input class="form-control" type="text" name="name" required="required"/>
                    </div>
                    <div class="form-group">
                        <label>Image</label>
                        <!-- <small id="emailHelp" class="form-text text-muted mb-2">Ukuran foto 400 x 300 pixel, maksimal 1MB</small> -->
                        <div class="row input-img-container">
                            <div class="input-img-item col">
                                <div class="bg-light embed-responsive embed-responsive-4by3 rounded bg-vertical-center">
                                    <button onclick="$(this).parent().find('.input-img').click();" type="button" class="btn btn-upload-img"><i class="fas fa-plus"></i></button>
                                    <input class="input-img" type="file" accept="image/*" style="visibility: hidden;position: absolute;" name="image">
                                    <i title="Remove image" style="display: none" class="fas fa-trash btn-remove-img"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary ml-auto">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
