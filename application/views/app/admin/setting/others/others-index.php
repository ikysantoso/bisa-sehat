<div class="main-content" id="panel">
    <?php include VIEWPATH.'/template-part/admin/top-nav.php'; ?>

    <!-- Header -->
    <div class="header bg-primary pb-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <?php include VIEWPATH.'/template-part/admin/breadcrumb.php'; ?>
                </div>
            </div>
        </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">

                <!-- unique code -->
                <div class="card bg-transparent">
                    <ul class="nav nav-tabs custom-tabs" id="myTab" role="tablist">
                        <li class="nav-item" role="presentation">
                            <a class="nav-link active" id="unique_code-tab" data-toggle="tab" href="#unique_code" role="tab" aria-controls="unique_code" aria-selected="true">Kode Unik</a>
                        </li>
                        <li class="nav-item" role="presentation">
                            <a class="nav-link" id="contact_info-tab" data-toggle="tab" href="#contact_info" role="tab" aria-controls="contact_info" aria-selected="false">Contact Info</a>
                        </li>
                        <!-- <li class="nav-item" role="presentation">
                            <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Setting 3</a>
                        </li> -->
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="unique_code" role="tabpanel" aria-labelledby="unique_code-tab">
                            <div style="border-top-left-radius: 0;border-top-right-radius: 0" class="card-header border-0">
                                <div class="row">
                                    <div class="col-md-4">
                                        <h3 class="mb-3">Kode Unik</h3>
                                        <div class="form-group">
                                            <label>Jumlah Digit Kode Unik</label>
                                            <form id="form-unique-code-digit">
                                                <div class="input-group mb-3">
                                                    <input value="<?php echo (isset($settings['unique_code_digit'])) ? $settings['unique_code_digit'] : ''; ?>" type="number" class="form-control" name="unique_code_digit" required="required"/>
                                                    <div class="input-group-append">
                                                        <button type="submit" class="btn btn-primary">Simpan</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="contact_info" role="tabpanel" aria-labelledby="contact_info-tab">
                            <div style="border-top-left-radius: 0;border-top-right-radius: 0" class="card-header border-0">
                                <h3 class="mb-3">Contact Info</h3>
                                <form id="form-contact-info">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Whatsapp Number</label>
                                                <input value="<?php echo (isset($settings['whatsapp_number'])) ? $settings['whatsapp_number'] : ''; ?>" type="text" class="form-control" name="whatsapp_number" required="required"/>
                                            </div>

                                            <div class="form-group">
                                                <label>Facebook</label>
                                                <input value="<?php echo (isset($settings['facebook'])) ? $settings['facebook'] : ''; ?>" type="text" class="form-control" name="facebook" required="required"/>
                                            </div>

                                            <div class="form-group">
                                                <label>Instagram</label>
                                                <input value="<?php echo (isset($settings['instagram'])) ? $settings['instagram'] : ''; ?>" type="text" class="form-control" name="instagram" required="required"/>
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label>About our office</label>
                                                <textarea class="form-control" required="required" name="our_office"><?php echo (isset($settings['our_office'])) ? $settings['our_office'] : ''; ?></textarea>
                                            </div>

                                            <div class="form-group">
                                                <label>Map</label>
                                                <textarea class="form-control" required="required" name="map"><?php echo (isset($settings['map'])) ? $settings['map'] : ''; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text-center border p-3">
                                        <button type="submit" class="btn btn-primary">Save</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                       <!--  <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                            <div style="border-top-left-radius: 0;border-top-right-radius: 0" class="card-header border-0">
                                <h3 class="mb-3">Setting 3</h3>
                            </div>
                        </div> -->
                    </div>
                </div>

            </div>
        </div>
        
        <?php include VIEWPATH.'/template-part/admin/footer-text.php'; ?>
    </div>
</div>