<div class="main-content" id="panel">
    <?php include VIEWPATH.'/template-part/admin/top-nav.php'; ?>

    <!-- Header -->
    <div class="header bg-primary pb-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <?php include VIEWPATH.'/template-part/admin/breadcrumb.php'; ?>
                    
                    <div class="col-lg-6 col-5 text-right">
                        <button type="button" class="btn btn-sm btn-neutral" data-toggle="modal" data-target="#modal-bank-accounts">New</button>
                        <a href="#" class="btn btn-sm btn-neutral">Filters</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header border-0">
                        <h3 class="mb-0">Bank Account List</h3>
                    </div>
                    <!-- Light table -->
                    <div class="table-responsive">
                        <table id="table-bank-account-list" class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Account Name</th>
                                    <th>Account Number</th>
                                    <th>Active?</th>
                                    <th>ID</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody class="list"></tbody>
                        </table>
                    </div>
                    
                </div>
            </div>
        </div>
        
        <?php include VIEWPATH.'/template-part/admin/footer-text.php'; ?>
    </div>
</div>

<div id="modal-bank-accounts" class="modal" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form id="form-bank-acccount">
            	<input type="hidden" name="id"/>
	            <div class="modal-header">
	                <h5 class="modal-title">Add New Bank Account</h5>
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                <span aria-hidden="true">&times;</span>
	                </button>
	            </div>
	            <div class="modal-body">
	            	<div class="form-group">
	            		<label>Name</label>
	            		<input class="form-control" type="text" name="name" required="required" />
	            	</div>
	            	<div class="form-group">
	            		<label>Account Name</label>
	            		<input class="form-control" type="text" name="account_name" required="required" />
	            	</div>
	            	<div class="form-group">
	            		<label>Account Number</label>
	            		<input class="form-control" type="text" name="account_number" required="required" />
	            	</div>
	            	<div class="form-group">
	            		<label>Status</label><br/>
	            		<div class="form-check form-check-inline">
						  	<input class="form-check-input" type="radio" name="status" id="status_active" value="active" required="required"/>
						  	<label class="form-check-label" for="status_active">Active</label>
						</div>
						<div class="form-check form-check-inline">
						  	<input class="form-check-input" type="radio" name="status" id="status_inactive" value="inactive" required="required"/>
						  	<label class="form-check-label" for="status_inactive">Inactive</label>
						</div>
	            	</div>
	            </div>
	            <div class="modal-footer">
	                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	                <button type="submit" class="btn btn-primary">Save changes</button>
	            </div>
            </form>
        </div>
    </div>
</div>
