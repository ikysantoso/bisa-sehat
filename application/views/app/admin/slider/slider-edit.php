<div class="main-content" id="panel">
    <?php include VIEWPATH.'/template-part/admin/top-nav.php'; ?>

    <!-- Header -->
    <div class="header bg-primary pb-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <?php include VIEWPATH.'/template-part/admin/breadcrumb.php'; ?>
                </div>
            </div>
        </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
                <div class="card">
                    
                    <div class="card-body">
                    	<form id="form-slider">
                            <input type="hidden" name="id" value="<?php echo $slider->id; ?>" >
	                    	<div class="row">
	                    		<div class="col-8">
			                    	<div class="form-group">
			                    		<label>Title</label>
			                    		<input value="<?php echo $slider->title1; ?>" type="text" class="form-control" required="required" name="title1"/>
			                    	</div>
                                    <!-- <div class="form-group">
                                        <label>Title 2</label>
                                        <input value="<?php echo $slider->title2; ?>" type="text" class="form-control" required="required" name="title2"/>
                                    </div> -->
                                    <div class="form-group">
                                        <label>Link</label>
                                        <input value="<?php echo $slider->link; ?>" type="text" class="form-control" name="link"/>
                                    </div>
                                    <div class="form-group">
                                        <label class="d-block">Active at?</label>
                                        <div>
                                            <div class="form-check form-check-inline">
                                                <input <?php echo (!$slider->start_date) ? 'checked' : ''; ?> class="form-check-input active_at_option" type="radio" name="active_option" id="exampleRadios1" value="forever" >
                                                <label class="form-check-label" for="exampleRadios1">
                                                Forever
                                                </label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input <?php echo ($slider->start_date && !$slider->end_date) ? 'checked' : ''; ?> class="form-check-input active_at_option" type="radio" name="active_option" id="exampleRadios2" value="start-date">
                                                <label class="form-check-label" for="exampleRadios2">
                                                Start date
                                                </label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input <?php echo ($slider->start_date && $slider->end_date) ? 'checked' : ''; ?> class="form-check-input active_at_option" type="radio" name="active_option" id="exampleRadios3" value="periode">
                                                <label class="form-check-label" for="exampleRadios3">
                                                Period
                                                </label>
                                            </div>
                                        </div>
                                        <div style="display: none" class="pt-3" id="daterange">
                                            <?php 
                                            if ($slider->start_date && $slider->end_date) {
                                                $start_date = date_format(date_create_from_format('Y-m-d', $slider->start_date), 'd/m/Y');
                                                $end_date = date_format(date_create_from_format('Y-m-d', $slider->end_date), 'd/m/Y');

                                                $periode_date = $start_date.' - '.$end_date;
                                            } else {
                                                $periode_date = '';
                                            }
                                            ?>
                                            <input type="text" value="<?php echo $periode_date; ?>" required="required" class="form-control" name="daterange"/>
                                        </div>
                                        <div style="display: none" class="pt-3" id="datepicker">
                                            <?php 
                                            if ($slider->start_date) {
                                                $start_date_datepicker = date_format(date_create_from_format('Y-m-d', $slider->start_date), 'd-m-Y');
                                            } else {
                                                $start_date_datepicker = '';
                                            }
                                            ?>
                                            <input type="text" value="<?php echo $start_date_datepicker; ?>" required="required" class="form-control" name="datepicker"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="d-block">Status</label>
                                        <div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="status" id="status_active" <?php echo ($slider->status=='active') ? 'checked' : ''; ?> value="active" required>
                                                <label class="form-check-label" for="status_active">
                                                Active
                                                </label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="status" id="status_inactive" <?php echo ($slider->status=='inactive') ? 'checked' : ''; ?> value="inactive" required>
                                                <label class="form-check-label" for="status_inactive">
                                                Inactive
                                                </label>
                                            </div>
                                        </div>
                                    </div>
	                    		</div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <label>Images</label>
                                        <div class="row input-img-container">
                                            <div class="input-img-item col">
                                                <div style="background-image: url('<?php echo ($slider->image) ? base_url('assets/images/upload/slider/'.$slider->image) : ''; ?>');" class="bg-light embed-responsive embed-responsive-4by3 rounded bg-vertical-center">
                                                    <button onclick="$(this).parent().find('.input-img').click();" type="button" class="btn btn-upload-img"><i class="fas fa-plus"></i></button>
                                                    <input class="input-img" type="file" accept="image/*" style="visibility: hidden;position: absolute;" name="image">
                                                    <i title="Remove image" <?php echo (!$slider->image) ? 'style="display:none"' : ''; ?> class="fas fa-trash btn-remove-img"></i>
                                                </div>
                                            </div>
                                        </div>

                                        <input type="hidden" name="current_image" value="<?php echo ($slider->image) ? $slider->image : ''; ?>"/>
                                    </div>
                                </div>
	                    	</div>

	                    	<a class="btn btn-secondary" href="<?php echo base_url('admin/slider'); ?>">Cancel</a>
	                    	<button type="submit" class="btn btn-primary">Save</button>
                    	</form>
                    </div>
                </div>
            </div>
        </div>
        
        <?php include VIEWPATH.'/template-part/admin/footer-text.php'; ?>
    </div>
</div>