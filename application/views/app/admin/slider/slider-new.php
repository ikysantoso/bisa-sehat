<div class="main-content" id="panel">
    <?php include VIEWPATH.'/template-part/admin/top-nav.php'; ?>

    <!-- Header -->
    <div class="header bg-primary pb-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <?php include VIEWPATH.'/template-part/admin/breadcrumb.php'; ?>
                </div>
            </div>
        </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
                <div class="card">
                    
                    <div class="card-body">
                    	<form id="form-slider">
	                    	<div class="row">
	                    		<div class="col-8">
			                    	<div class="form-group">
			                    		<label>Title</label>
			                    		<input type="text" class="form-control" required="required" name="title1"/>
			                    	</div>
                                    <!-- <div class="form-group">
                                        <label>Title 2</label>
                                        <input type="text" class="form-control" required="required" name="title2"/>
                                    </div> -->
                                    <div class="form-group">
                                        <label>Link</label>
                                        <input type="text" class="form-control" name="link"/>
                                    </div>

                                    <div class="form-group">
                                        <label class="d-block">Active at?</label>
                                        <div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input active_at_option" type="radio" name="active_option" id="exampleRadios1" value="forever" checked>
                                                <label class="form-check-label" for="exampleRadios1">
                                                Forever
                                                </label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input active_at_option" type="radio" name="active_option" id="exampleRadios2" value="start-date">
                                                <label class="form-check-label" for="exampleRadios2">
                                                Start date
                                                </label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input active_at_option" type="radio" name="active_option" id="exampleRadios3" value="periode">
                                                <label class="form-check-label" for="exampleRadios3">
                                                Period
                                                </label>
                                            </div>
                                        </div>
                                        <div style="display: none" class="pt-3" id="daterange">
                                            <input type="text" required="required" class="form-control" name="daterange"/>
                                        </div>
                                        <div style="display: none" class="pt-3" id="datepicker">
                                            <input type="text" required="required" class="form-control" name="datepicker"/>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="d-block">Status</label>
                                        <div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="status" id="status_active" value="active" checked required>
                                                <label class="form-check-label" for="status_active">
                                                Active
                                                </label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="status" id="status_inactive" value="inactive" required>
                                                <label class="form-check-label" for="status_inactive">
                                                Inactive
                                                </label>
                                            </div>
                                        </div>
                                    </div>
	                    		</div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <label>Images</label>
                                        <div class="row input-img-container">
                                            <div class="input-img-item col">
                                                <div class="bg-light embed-responsive embed-responsive-4by3 rounded bg-vertical-center">
                                                    <button onclick="$(this).parent().find('.input-img').click();" type="button" class="btn btn-upload-img"><i class="fas fa-plus"></i></button>
                                                    <input class="input-img" type="file" accept="image/*" style="visibility: hidden;position: absolute;" name="image">
                                                    <i title="Remove image" style="display: none" class="fas fa-trash btn-remove-img"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
	                    	</div>

	                    	<a class="btn btn-secondary" href="<?php echo base_url('admin/page'); ?>">Cancel</a>
	                    	<button type="submit" class="btn btn-primary">Save</button>
                    	</form>
                    </div>
                </div>
            </div>
        </div>
        
        <?php include VIEWPATH.'/template-part/admin/footer-text.php'; ?>
    </div>
</div>