<div class="main-content" id="panel">
    <?php include VIEWPATH.'/template-part/admin/top-nav.php'; ?>

    <!-- Header -->
    <div class="header bg-primary pb-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
    				<?php include VIEWPATH.'/template-part/admin/breadcrumb.php'; ?>
                </div>
                <!-- Card stats -->
                <div class="row">
                    <div class="col-xl-3 col-md-6">
                        <div onclick="{location.href=base_url+'admin/user'}" class="card card-stats cart-dashboard">
                            <!-- Card body -->
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <h5 class="card-title text-uppercase text-muted mb-0">Total Member</h5>
                                        <span class="h2 font-weight-bold mb-0"><?php echo $chart_data['total_member']; ?></span>
                                    </div>
                                    <div class="col-auto">
                                        <div class="icon icon-shape bg-gradient-blue text-white rounded-circle shadow">
                                            <i class="fas fa-users"></i>
                                        </div>
                                    </div>
                                </div>
                                <!-- <p class="mt-3 mb-0 text-sm">
                                    <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 3.48%</span>
                                    <span class="text-nowrap">Since last month</span>
                                </p> -->
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-3 col-md-6">
                        <div class="card card-stats cart-dashboard">
                            <!-- Card body -->
                            <div onclick="{location.href=base_url+'admin/corporate/list'}" class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <h5 class="card-title text-uppercase text-muted mb-0">Total Corporate</h5>
                                        <span class="h2 font-weight-bold mb-0"><?php echo $chart_data['total_corporate']; ?></span>
                                    </div>
                                    <div class="col-auto">
                                        <div class="icon icon-shape bg-gradient-green text-white rounded-circle shadow">
                                            <i class="fas fa-building"></i>
                                        </div>
                                    </div>
                                </div>
                                <!-- <p class="mt-3 mb-0 text-sm">
                                    <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 3.48%</span>
                                    <span class="text-nowrap">Since last month</span>
                                </p> -->
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-3 col-md-6">
                        <div onclick="{location.href=base_url+'admin/product/list'}" class="card card-stats cart-dashboard">
                            <!-- Card body -->
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <h5 class="card-title text-uppercase text-muted mb-0">Total Product</h5>
                                        <span class="h2 font-weight-bold mb-0"><?php echo $chart_data['total_product']; ?></span>
                                    </div>
                                    <div class="col-auto">
                                        <div class="icon icon-shape bg-gradient-indigo text-white rounded-circle shadow">
                                            <i class="fas fa-tags"></i>
                                        </div>
                                    </div>
                                </div>
                                <!-- <p class="mt-3 mb-0 text-sm">
                                    <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 3.48%</span>
                                    <span class="text-nowrap">Since last month</span>
                                </p> -->
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-xl-3 col-md-6">
                        <div onclick="{location.href=base_url+'admin/order'}" class="card card-stats cart-dashboard">
                            <!-- Card body -->
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <h5 class="card-title text-uppercase text-muted mb-0">Total Order</h5>
                                        <span class="h2 font-weight-bold mb-0"><?php echo $chart_data['total_sales']; ?></span>
                                    </div>
                                    <div class="col-auto">
                                        <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                                            <i class="fas fa-shopping-cart"></i>
                                        </div>
                                    </div>
                                </div>
                                <!-- <p class="mt-3 mb-0 text-sm">
                                    <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 3.48%</span>
                                    <span class="text-nowrap">Since last month</span>
                                </p> -->
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <?php /*
        <div class="row">
            <div class="col-xl-8">
                <div class="card bg-default">
                    <div class="card-header bg-transparent">
                        <div class="row align-items-center">
                            <div class="col">
                                <h6 class="text-light text-uppercase ls-1 mb-1">Overview</h6>
                                <h5 class="h3 text-white mb-0">Sales value</h5>
                            </div>
                            <div class="col">
                                <ul class="nav nav-pills justify-content-end">
                                    <li class="nav-item mr-2 mr-md-0" data-toggle="chart" data-target="#chart-sales-dark" data-update="{&quot;data&quot;:{&quot;datasets&quot;:[{&quot;data&quot;:[0, 20, 10, 30, 15, 40, 20, 60, 60]}]}}" data-prefix="$" data-suffix="k">
                                        <a href="#" class="nav-link py-2 px-3 active" data-toggle="tab">
                                        <span class="d-none d-md-block">Month</span>
                                        <span class="d-md-none">M</span>
                                        </a>
                                    </li>
                                    <li class="nav-item" data-toggle="chart" data-target="#chart-sales-dark" data-update="{&quot;data&quot;:{&quot;datasets&quot;:[{&quot;data&quot;:[0, 20, 5, 25, 10, 30, 15, 40, 40]}]}}" data-prefix="$" data-suffix="k">
                                        <a href="#" class="nav-link py-2 px-3" data-toggle="tab">
                                        <span class="d-none d-md-block">Week</span>
                                        <span class="d-md-none">W</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <!-- Chart -->
                        <div class="chart">
                            <div class="chartjs-size-monitor">
                                <div class="chartjs-size-monitor-expand">
                                    <div class=""></div>
                                </div>
                                <div class="chartjs-size-monitor-shrink">
                                    <div class=""></div>
                                </div>
                            </div>
                            <!-- Chart wrapper -->
                            <canvas id="chart-sales-dark" class="chart-canvas chartjs-render-monitor" width="875" height="350" style="display: block; width: 875px; height: 350px;"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4">
                <div class="card">
                    <div class="card-header bg-transparent">
                        <div class="row align-items-center">
                            <div class="col">
                                <h6 class="text-uppercase text-muted ls-1 mb-1">Performance</h6>
                                <h5 class="h3 mb-0">Total orders</h5>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <!-- Chart -->
                        <div class="chart">
                            <div class="chartjs-size-monitor">
                                <div class="chartjs-size-monitor-expand">
                                    <div class=""></div>
                                </div>
                                <div class="chartjs-size-monitor-shrink">
                                    <div class=""></div>
                                </div>
                            </div>
                            <canvas id="chart-bars" class="chart-canvas chartjs-render-monitor" style="display: block; width: 875px; height: 350px;" width="875" height="350"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        */ ?>

        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col">
                                <h3 class="mb-0">Latest Order</h3>
                            </div>
                            <div class="col text-right">
                                <!-- <a href="#!" class="btn btn-sm btn-primary">See all</a> -->
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <!-- Projects table -->
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">ID</th>
                                    <th scope="col">Customer</th>
                                    <th scope="col">Date</th>
                                    <th class="text-center" scope="col">Payment Status</th>
                                    <th class="text-right" scope="col">Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($chart_data['latest_order'] as $key => $order) {
                                    $status = [
                                        'paid'      => '<span class="badge badge-success">paid</span>',
                                        'unpaid'    => '<span class="badge badge-warning">unpaid</span>',
                                        'waiting'   => '<span class="badge badge-info">waiting</span>',
                                        'failed'    => '<span class="badge badge-danger">failed</span>',
                                    ];

                                    $customer = ( $order->user ) ? ( ($order->user->first_name) ? $order->user->first_name ." ".$order->user->last_name : $order->user->email ) : '-';

                                    echo '
                                    <tr>
                                        <td>'.($key+1).'</td>
                                        <td><a class="font-weight-bold" href="'.base_url('admin/order/detail/'.$order->id).'">#'.$order->id.'</a></td>
                                        <td>'.$customer.'</td>
                                        <td>'.$order->created_at->format('d-m-Y H:i:s').'</td>
                                        <td class="text-center">'.$status[$order->payment_status].'</td>
                                        <td class="text-right">'.rupiah($order->total()).'</td>
                                    </tr>';
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col">
                                <h3 class="mb-0">Medical Record Request Verification</h3>
                            </div>
                            <div class="col text-right">
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive" id="medical-record">
                        <table id="table-medical-record" class="table table-striped table-responsive table-hover">
                            <thead>
                                <tr>
                                    <th width="2%">No</th>
                                    <th width="10%">Tanggal Input</th>
                                    <th width="10%">Nama</th>
                                    <th width="10%">Pemeriksaan</th>
                                    <th width="10%"></th>
                                    <th width="10%">File</th>
                                    <th width="5%">#</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no=1; foreach ($chart_data['medical_records'] as $key => $medical_record) : 
                                    $problem = []; $customer = ( $medical_record->user ) ? ( ($medical_record->user->first_name) ? $medical_record->user->first_name ." ".$medical_record->user->last_name : $medical_record->user->email ) : '-'; if($medical_record->attachment!=''){
                                    ?>

                                <tr>
                                    <td data-label="No"><?php echo ($no++); ?></td>
                                    <td data-label="Tanggal Input"><?php echo $medical_record->created_at; ?></td>
                                    <td data-label="Customer"><?php echo $customer; ?></td>
                                    <td data-label="Berat Badan">
                                        <div class="d-flex justify-content-between align-items-center">
                                            <b>TB</b>
                                            <div class="border-bottom flex-fill mx-2"></div>
                                            <span><?php echo $medical_record->height; ?> cm</span>
                                        </div>
                                        <div class="d-flex justify-content-between align-items-center">
                                            <b>BB</b>
                                            <div class="border-bottom flex-fill mx-2"></div>
                                            <span><?php echo $medical_record->weight; ?> kg</span>
                                        </div>
                                        <div class="d-flex justify-content-between align-items-center">
                                            <b>BB Ideal</b>
                                            <div class="border-bottom flex-fill mx-2"></div>
                                            <span><?php echo $medical_record->bmi()['min']; ?> s/d <?php echo $medical_record->bmi()['max']; ?> kg</span>
                                        </div>
                                        <div class="d-flex justify-content-between align-items-center">
                                            <b>BMI</b>
                                            <div class="border-bottom flex-fill mx-2"></div>
                                            <span><?php echo $medical_record->bmi()['bmi']; ?></span>
                                        </div>
                                    </td>
                                    <td data-label="Pemeriksaan">
                                        <div class="d-flex justify-content-between align-items-center">
                                            <b>Tekanan Darah</b>
                                            <div class="border-bottom flex-fill mx-1"></div>
                                        <span><?php if($medical_record->sistole==0){ echo '-';}else{echo $medical_record->sistole; ?>/<?php echo $medical_record->diastole; ?> mmHg<?php }?></span>
                                        </div>
                                        <div class="d-flex justify-content-between align-items-center">
                                            <b>Gula Darah</b>
                                            <div class="border-bottom flex-fill mx-1"></div>
                                        <span><?php if($medical_record->blood_sugar_level==0){ echo '-';}else{echo $medical_record->blood_sugar_level.' mg/dL';} ?></span>
                                        </div>
                                        <div class="d-flex justify-content-between align-items-center">
                                            <b>Kolesterol</b>
                                            <div class="border-bottom flex-fill mx-1"></div>
                                        <span><?php if ($medical_record->cholesterol_level==0){ echo '-';}else{ echo $medical_record->cholesterol_level.' mg/dL';} ?></span>
                                        </div>
                                        <div class="d-flex justify-content-between align-items-center">
                                            <b>Asam Urat</b>
                                            <div class="border-bottom flex-fill mx-1"></div>
                                        <span><?php if($medical_record->uric_acid_level==0){ echo '-';}else{echo $medical_record->uric_acid_level.'mg/dL';} ?> </span>
                                        </div>
                                    </td>
                                    <td data-label="file">
                                        <?php 
                                        if($medical_record->status==1){?>
                                        <span class="badge badge-success">verified</span>
                                    <?php }else if($medical_record->attachment!=''){?>
                                        <a target="_blank" href="<?php echo base_url('assets/member/medical-record/'.$medical_record->attachment); ?>" class="btn btn-sm btn-block btn-white"><i class="fas fa-paperclip"></i><span class="badge badge-block badge-info">lampiran file</span></a>
                                    <?php }else{?>
                                        <span class="badge badge-default">not verified</span>
                                    <?php }?>
                                    </td>
                                    <td data-label="#">
                                    <?php if($medical_record->attachment!='' and $medical_record->status==0){?>
                                        <button data-id="<?php echo $medical_record->id; ?>" onclick="approve(<?php echo $medical_record->id; ?>)" type="button" class="btn btn-sm btn-success btn-approve text-white text-uppercase">approve</button>
                                    <?php }?>
                                    </td>
                                </tr>
                                <?php } endforeach; ?>
                            </tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div> -->
        <?php include VIEWPATH.'/template-part/admin/footer-text.php'; ?>
    </div>
</div>
