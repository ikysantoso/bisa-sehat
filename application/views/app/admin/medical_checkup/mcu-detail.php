<div class="main-content" id="panel">
    <?php include VIEWPATH.'/template-part/admin/top-nav.php'; ?>

    <!-- Header -->
    <div class="header bg-primary pb-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <?php include VIEWPATH.'/template-part/admin/breadcrumb.php'; ?>
                    
                    <!-- <div class="col-lg-6 col-5 text-right">
                        <a href="<?php echo base_url('admin/medical_checkup/new'); ?>" class="btn btn-sm btn-neutral">New</a>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header border-0">
                        <h3 class="mb-0">Medical Checkup Detail</h3>
                    </div>

                    <div id="user_id" data-id="<?php echo $user->id; ?>" class="card-body">
                        <h4 class="card-title"><?php echo $user->first_name." ".$user->last_name; ?></h4>
                        <p class="mb-1"><?php echo $user->user_meta('gender'); ?> / <?php echo $user->age(); ?> tahun</p>
                        <p class="mb-1"><?php echo $user->email; ?></p>
                    </div>

                    <div class="table-responsive">
                        <table id="table-medical-checkup-user" class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th>#</th>
                                    <th>Created At</th>
                                    <th>ID</th>
                                    <th>#</th>
                                </tr>
                            </thead>
                            <tbody class="list"></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        
        <?php include VIEWPATH.'/template-part/admin/footer-text.php'; ?>
    </div>
</div>
