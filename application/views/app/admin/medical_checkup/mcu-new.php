<div class="main-content" id="panel">
    <?php include VIEWPATH.'/template-part/admin/top-nav.php'; ?>

    <!-- Header -->
    <div class="header bg-primary pb-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <?php include VIEWPATH.'/template-part/admin/breadcrumb.php'; ?>
                </div>
            </div>
        </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header border-0">
                        <h3 class="mb-0">New Medical Checkup</h3>
                    </div>
                    <div class="card-body">
                        <form id="form-medical-checkup">
                            <input type="hidden" name="user_id" value="<?php echo $user_id ?>"/>
                            <div class="form-group">
                                <label>Paparan Lingkungan kerja</label>
                                <textarea class="form-control" name="wee"></textarea>
                            </div>
                            <div class="form-group">
                                <label>Anamnesa</label>
                                <textarea class="form-control" name="anamnesis"></textarea>
                            </div>
                            <div class="form-group">
                                <label>Kesimpulan pemeriksaan fisik</label>
                                <textarea class="form-control" name="physical_examination"></textarea>
                            </div>
                            <div class="form-group">
                                <label>Kesimpulan pemeriksaan laboratorium</label>
                                <textarea class="form-control" name="lab_result"></textarea>
                            </div>
                            <div class="form-group">
                                <label>Kesimpulan pemeriksaan penunjang,</label>
                                <textarea class="form-control" name="support_examination"></textarea>
                            </div>
                            <div class="form-group">
                                <label>Fitnes Status</label>
                                <textarea class="form-control" name="fitness_status"></textarea>
                            </div>
                            <div class="form-group">
                                <label>Kesimpulan Okupasi</label>
                                <textarea class="form-control" name="occupation"></textarea>
                            </div>

                            <div class="text-center">
                                <a class="btn btn-secondary" href="<?php echo base_url('admin/medical_checkup/list') ?>">Cancel</a>
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        <?php include VIEWPATH.'/template-part/admin/footer-text.php'; ?>
    </div>
</div>
