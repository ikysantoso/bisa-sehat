<div class="main-content" id="panel">
    <?php include VIEWPATH.'/template-part/admin/top-nav.php'; ?>

    <!-- Header -->
    <div class="header bg-primary pb-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <?php include VIEWPATH.'/template-part/admin/breadcrumb.php'; ?>
                </div>
            </div>
        </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
                <div class="card">
                    
                    <div class="card-body">
                    	<form id="form-page">
                            <input type="hidden" value="<?php echo $page->id; ?>" name="id"/>
	                    	<div class="row">
	                    		<div class="col-12">
			                    	<div class="form-group">
			                    		<label>Title</label>
			                    		<input value="<?php echo $page->title; ?>" type="text" class="form-control" required="required" name="title"/>
			                    	</div>
			                    	<div class="form-group">
			                    		<label>Description</label>
			                    		<div id="caption"><?php echo $page->description; ?></div>
										<input type="hidden" class="description-field" name="description"/>
			                    	</div>
                                    <div class="form-group">
                                        <label>Image</label>
                                        <div class="row input-img-container">
                                            <div class="input-img-item col">
                                                <div style="background-image: url('<?php echo ($page->image) ? base_url('assets/images/upload/page/'.$page->image) : ''; ?>'); width:400px;" class="bg-light embed-responsive embed-responsive-4by3 rounded bg-vertical-center">
                                                    <button onclick="$(this).parent().find('.input-img').click();" type="button" class="btn btn-upload-img"><i class="fas fa-plus"></i></button>
                                                    <input class="input-img" type="file" accept="image/*" style="visibility: hidden;position: absolute;" name="image">
                                                    <i title="Remove image" <?php echo (!$page->image) ? 'style="display:none"' : ''; ?> class="fas fa-trash btn-remove-img"></i>
                                                </div>
                                            </div>
                                        </div>

                                        <input type="hidden" name="current_image" value="<?php echo ($page->image) ? $page->image : ''; ?>"/>
                                    </div>
	                    		</div>
	                    	</div>

	                    	<a class="btn btn-secondary" href="<?php echo base_url('admin/page'); ?>">Cancel</a>
	                    	<button type="submit" class="btn btn-primary">Save</button>
                    	</form>
                    </div>
                </div>
            </div>
        </div>
        
        <?php include VIEWPATH.'/template-part/admin/footer-text.php'; ?>
    </div>
</div>