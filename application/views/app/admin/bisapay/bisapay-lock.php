<div class="main-content" id="panel">
    <?php include VIEWPATH.'/template-part/admin/top-nav.php'; ?>

    <!-- Header -->
    <div class="header bg-primary pb-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <?php include VIEWPATH.'/template-part/admin/breadcrumb.php'; ?>
                </div>
            </div>
        </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header border-0 text-center">
                        <button id="lock-saldo-btn" class="btn btn-secondary btn-sm"><i class="fas fa-unlock-alt"></i> Lock Saldo User</button>
                    </div>
                    <!-- Light table -->
                    <div class="table-responsive">
                        <table id="table-bisapay-list" class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th>#</th>
                                    <th>User</th>
                                    <th>Nominal</th>
                                    <th>Description</th>
                                    <th>Date</th>
                                    <th>ID</th>
                                </tr>
                            </thead>
                            <tbody class="list"></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        
        <?php include VIEWPATH.'/template-part/admin/footer-text.php'; ?>
    </div>
</div>

<div id="modal-lock-saldo" class="modal" tabindex="-1">
    <div class="modal-dialog">
        <form id="form-lock-saldo" class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Lock Saldo User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>User</label>
                    <input type="text" class="singleInputDynamic" data-url="<?php echo base_url('admin/user/bisapay_user'); ?>" name="user" required="required"/>
                </div>
                <div class="form-group">
                    <label>Saldo Bisapay</label>
                    <input type="text" name="saldo" disabled="disabled" value="-" class="form-control">
                </div>
                <div class="form-group">
                    <label>Nominal Lock Saldo</label>
                    <input type="number" required="required" class="form-control" disabled="disabled" name="nominal"/>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Lock Saldo</button>
            </div>
        </form>
    </div>
</div>