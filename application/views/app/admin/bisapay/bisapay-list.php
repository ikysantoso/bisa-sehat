<div class="main-content" id="panel">
    <?php include VIEWPATH.'/template-part/admin/top-nav.php'; ?>

    <!-- Header -->
    <div class="header bg-primary pb-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <?php include VIEWPATH.'/template-part/admin/breadcrumb.php'; ?>
                </div>
            </div>
        </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header border-0 d-flex justify-content-between">
                        <h3 class="mb-0">Topup List</h3>
                        <div style="font-size: .8em;">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="datatable_filter" id="inlineRadio1" value="all" checked>
                                <label class="form-check-label" for="inlineRadio1">All</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="datatable_filter" id="inlineRadio2" value="waiting">
                                <label class="form-check-label" for="inlineRadio2">Waiting</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="datatable_filter" id="inlineRadio3" value="success">
                                <label class="form-check-label" for="inlineRadio3">Success</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="datatable_filter" id="inlineRadio4" value="failed">
                                <label class="form-check-label" for="inlineRadio4">Failed</label>
                            </div>
                        </div>
                    </div>
                    <!-- Light table -->
                    <div class="table-responsive">
                        <table id="table-bisapay-list" class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th>#</th>
                                    <th>User</th>
                                    <th>Nominal</th>
                                    <th>Kodeunik</th>
                                    <th>Tujuan</th>
                                    <th>Date</th>
                                    <th>Status</th>
                                    <th>ID</th>
                                </tr>
                            </thead>
                            <tbody class="list"></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <?php include VIEWPATH.'/template-part/admin/footer-text.php'; ?>
    </div>
</div>

<div class="modal" id="modal-topup" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="form-approval">
                <input type="hidden" name="id" id="id"/>
                <div class="modal-header">
                    <h5 class="modal-title">Bisapay Approval</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="text-center">
                        <h2 id="user-name"></h2>
                        <p id="topup-date"></p>
                    </div>
                    <div class="d-flex justify-content-between align-items-center mb-3">
                        <span>Nominal</span>
                        <div class="flex-fill border-bottom mx-3" style="border-style: dashed;border-width: 1px;opacity: 0.2"></div>
                        <b id="nominal"></b>
                    </div>
                    <div class="d-flex justify-content-between align-items-center mb-3">
                        <span>Destination</span>
                        <div class="flex-fill border-bottom mx-3" style="border-style: dashed;border-width: 1px;opacity: 0.2"></div>
                        <b id="bank-name"></b>
                    </div>
                    <div class="d-flex justify-content-between align-items-center mb-3">
                        <span>Account Name</span>
                        <div class="flex-fill border-bottom mx-3" style="border-style: dashed;border-width: 1px;opacity: 0.2"></div>
                        <b id="account-name"></b>
                    </div>
                    <div class="d-flex justify-content-between align-items-center mb-3">
                        <span>Account Number</span>
                        <div class="flex-fill border-bottom mx-3" style="border-style: dashed;border-width: 1px;opacity: 0.2"></div>
                        <b id="account-number"></b>
                    </div>
                    <div id="payment-image-container" class="align-items-center mb-3">
                    </div>
                    <div class="d-flex justify-content-between align-items-center mb-3">
                        <span>Approval</span>
                        <div class="flex-fill border-bottom mx-3" style="border-style: dashed;border-width: 1px;opacity: 0.2"></div>
                        <div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input required="required" type="radio" id="approval1" name="approval" value="approve" class="custom-control-input">
                                <label class="custom-control-label" for="approval1">Approve</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input required="required" type="radio" id="approval2" name="approval" value="refuse" class="custom-control-input">
                                <label class="custom-control-label" for="approval2">Refuse</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary btn-submit-approval">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>