<div class="main-content" id="panel">
    <?php include VIEWPATH.'/template-part/admin/top-nav.php'; ?>

    <!-- Header -->
    <div class="header bg-primary pb-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <?php include VIEWPATH.'/template-part/admin/breadcrumb.php'; ?>
                </div>
            </div>
        </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header border-0 d-flex justify-content-between">
                        <h3 class="mb-0">Daftar Tarik</h3>
                        <div style="font-size: .8em;">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="datatable_filter" id="inlineRadio1" value="all">
                                <label class="form-check-label" for="inlineRadio1">All</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="datatable_filter" id="inlineRadio2" value="waiting" checked>
                                <label class="form-check-label" for="inlineRadio2">Waiting</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="datatable_filter" id="inlineRadio3" value="success">
                                <label class="form-check-label" for="inlineRadio3">Success</label>
                            </div>
                        </div>
                    </div>
                    <!-- Light table -->
                    <div class="table-responsive">
                        <table id="table-bisapay-withdraw-list" class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th>#</th>
                                    <th>User</th>
                                    <th>Nominal</th>
                                    <th>Tujuan</th>
                                    <th>Date</th>
                                    <th>Status</th>
                                    <th>ID</th>
                                </tr>
                            </thead>
                            <tbody class="list"></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        
        <?php include VIEWPATH.'/template-part/admin/footer-text.php'; ?>
    </div>
</div>