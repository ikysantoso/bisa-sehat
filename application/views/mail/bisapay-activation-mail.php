<!doctype html>
<html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto">
  <script src="https://cdn.tailwindcss.com"></script>
</head>
<body>
  <div class="flex justify-center">
    <img class="h-40 mt-10" src="<?php echo base_url('assets/images/email/payment.png'); ?>" alt="...">
  </div>
  <div class="text-justify m-10">
    <h3 class="font-bold text-lg">Hi, <?php echo $nama; ?></h3>
    <p>Selamat! Anda telah berhasil mengaktifkan <b>Bisapay.</b></p>
    <p class="mt-3">Anda sekarang sudah dapat menikmati kemudahan dalam bertransaksi di Aplikasi BISA</p>
    <h3 class="mt-5 font-bold text-lg">Salam, <br>Tim Aplikasi BISA</h3>
    <?php include VIEWPATH.'/mail/components/footer-mail.php' ?>
  </div>
</body>
</html>