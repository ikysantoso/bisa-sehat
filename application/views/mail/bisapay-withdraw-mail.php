<!doctype html>
<html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto">
  <script src="https://cdn.tailwindcss.com"></script>
</head>
<body>
  <div class="flex justify-center">
    <img class="h-40 mt-10" src="<?php echo base_url('assets/images/email/congrats.png'); ?>" alt="...">
  </div>
  <div class="text-justify m-10">
    <h3 class="font-bold text-lg">Hi, <?php echo $withdraw_detail->customer_bank->account_name; ?></h3>
    <p>SELAMAT! penarikan bisapay anda telah sukses...</p>
    <h2 class="mt-5 font-bold text-lg">Detail Withdraw</h2>
    <p>Withdraw ID     : <?php echo $withdraw_detail->id; ?></p>
    <p>Nominal         : <?php echo(rupiah($withdraw_detail->bisapay_log->nominal)); ?></p>
    <p>Destination     : <?php echo $withdraw_detail->customer_bank->name; ?></p>
    <p>Account Name    : <?php echo $withdraw_detail->customer_bank->account_name; ?></p>
    <p>Account Number  : <?php echo $withdraw_detail->customer_bank->account_number; ?></p>
    <p>Status Approval : <?php if($withdraw_detail->bisapay_log->status == 'success'): ?>
                         <?php echo $withdraw_detail->bisapay_log->status; ?><?php endif; ?></p>
    <p class="mt-10">Terima kasih, sudah mempercayai <b>BISAPAY</b> sebagai dompet digital anda.</p>
    <h3 class="mt-5 font-bold text-lg">Salam, <br>Tim Aplikasi BISA</h3>
    <?php include VIEWPATH.'/mail/components/footer-mail.php' ?>
  </div>
</body>
</html>