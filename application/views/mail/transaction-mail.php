<!doctype html>
<html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto">
  <script src="https://cdn.tailwindcss.com"></script>
</head>
<body>
    <?php
    $total_ongkos_kirim = 0;
    $total_produk = 0;
    $total_produk += $transaction->total();
    $total_ongkos_kirim += $transaction->shipping_fee;
    $unique_code = $transaction->payment->unique_code;
    $diskon = ($transaction->payment->voucher_user_id) ? ($transaction->payment->voucher_user->voucher->nominal) : 0;
    $payment_status = ($transaction->payment) ? $transaction->payment->status : $transaction->payment_status;
    $payment_method = ($transaction->payment) ? $transaction->payment->payment_method : $transaction->payment_method;
    $date = new DateTime( $transaction->created_at );
    $date->add(new DateInterval('P1D'));
    ?>
    
  <div class="flex justify-center">
  <img class="h-40 mt-10" src="<?php echo base_url('assets/images/email/waiting.png'); ?>" alt="...">
  </div>
  <div class="text-justify m-10">
    <h3 class="font-bold text-lg">Hi, <?php echo $name; ?></h3>
    <p>TERIMA KASIH, atas pesanan anda di Aplikasi BISA.</p>
    <h2 class="mt-5 font-bold text-lg">Detail Pembayaran</h2>
    <p>Order ID          : <?php echo $transaction->transaction_id; ?></p>
    <p>Metode Bayar      : <?php echo isset($transaction->payment->payment_method) ? 'Transfer Bank' : 'Bisapay'; ?></p>
    <p>Total Transaksi   : <?php echo rupiah($total_produk + $total_ongkos_kirim + $unique_code - $diskon); ?></p>
    <p>Order Status      : <?php echo $transaction->status; ?></p>
    <?php if($payment_method && $payment_status != 'paid'): ?>
        <p>Bank Penerima : <?php echo $transaction->payment->bank->name; ?></p>
        <p>Nama Penerima : <?php echo $transaction->payment->bank->account_name; ?></p>
        <p>No. Rekening  : <?php echo $transaction->payment->bank->account_number; ?></p>
        <p class="mt-3"><b>Mohon lakukan pembayaran sesuai Total Transaksi Pesanan Anda diatas, paling lambat : </b></p>
        <p><b><?php echo $date->format('d F Y, H:i:s'); ?></b></p>
    <?php endif; ?>
    <p class="mt-3">Pesanan Anda akan segera diproses dan selanjutnya dikirimkan ke alamat Anda. </p>
    <p class="mt-3">Mohon menunggu sampai Pesanan Anda tiba, terima kasih sudah ber-Transaksi di BISA Sehat. </p>
    <h3 class="mt-5 font-bold text-lg">Salam, <br>Tim BISA Sehat</h3>
    <?php include VIEWPATH.'/mail/components/footer-mail.php' ?>
  </div>
</body>
</html>