<!doctype html>
<html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto">
  <script src="https://cdn.tailwindcss.com"></script>
</head>
<body>
  <div class="text-justify m-10">
    <h2 class="font-bold text-lg text-center">Selamat datang di BISA Sehat</h2>
    <p>Terima kasih Anda telah bergabung di BISA Sehat.  Untuk memverifikasi alamat email Anda, silakan klik tautan ini:
    </p>
    <table class="mx-auto" style="font-family: Roboto,serif border-spacing: 0; border-collapse: separate !important; border-radius: 4px; margin: 0 auto;">
      <tbody>
          <tr>
              <td style="border-spacing: 0; border-collapse: collapse; line-height: 24px; font-size: 16px; border-radius: 4px; margin: 0;" align="center" bgcolor="#008080">
                  <a href="<?php echo base_url('auth/finish_registration/'.$code); ?>" style="font-size: 16px; font-family: Roboto; border-radius: 4px; line-height: 20px; display: inline-block; font-weight: normal; white-space: nowrap; background-color: #008080; color: #ffffff; padding: 8px 12px;">Aktivasi Akun</a>
              </td>
          </tr>
      </tbody>
    </table>
    <p class="mt-3">Tautan tidak berfungsi? Salin tautan berikut ke bilah alamat browser Anda:</p>
    <p><?php echo base_url('auth/finish_registration/'.$code); ?></p>
    <h2 class="mt-3 font-bold text-lg">Detail Pengguna</h2>
    <p>Username        : <?php echo $username; ?></p>
    <p>Email           : <?php echo $email; ?></p>
    <h3 class="mt-5 font-bold text-lg">Salam, <br>Tim BISA Sehat</h3>
    <?php include VIEWPATH.'/mail/components/footer-mail.php' ?>
  </div>
</body>
</html>