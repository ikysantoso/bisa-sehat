        <!-- Header -->
        <?php $page = (isset($page)) ? $page : ''; ?>
        <?php $page_detail = (isset($page_detail)) ? $page_detail : ''; ?>
        <?php
            if( $this->ion_auth->logged_in() ){
                $this->load->model('CartModel');

                $carts = CartModel::where(['user_id' => $this->ion_auth->user()->row()->id, 'type' => 'cart'])->get();
                $carts_total = $carts->count();
            }
        ?>

        <!-- <header class="">
            <nav class="navbar navbar-expand-lg">
                <div class="container align-items-center">
                    <a class="navbar-brand" href="<?php echo base_url('home'); ?>">
                        <img src="<?php echo base_url('assets/images/logo-bisa.png'); ?>" class="d-none d-lg-block" alt="">
                    </a>
                    <?php if (isset($isBack) && $isBack == "true"): ?>
                    <a onclick="history.go(-1);" class="navbar-brand">
                        <img src="<?php echo base_url('assets/images/favicon/back_arrow.png'); ?>" class="d-block d-lg-none d-md-none" style="height: 40px!important;width: auto!important;">
                    </a>
                    <h4 class="d-block d-lg-none d-md-none" style="position: relative;margin:8% 0 0 18%; color:#fff; font-size:22px; bottom:7px;">Article</h4>
                    <?php endif; ?>
                    <?php if (isset($isBackDetail) && $isBackDetail == "true"): ?>
                    <a onclick="history.go(-1);" class="navbar-brand">
                        <img src="<?php echo base_url('assets/images/favicon/back_arrow.png'); ?>" class="d-block d-lg-none d-md-none" style="height: 40px!important;width: auto!important;">
                    </a>
                    <?php endif; ?>
                    <?php if( $this->ion_auth->logged_in() ):?>
                    <div style="position: absolute;top:18px;right:24px;height:36px;" class="d-block d-lg-none">
                    <?php else:?>
                    <div style="position: absolute;top:18px;right:24px;height:36px;" class="d-block d-lg-none">
                    <?php endif;?>
                        <?php if( $this->ion_auth->logged_in() ):?>
                            <a href="<?php echo base_url('cart'); ?>" class="nav-shop">
                                <img src="<?php echo base_url('assets/images/shop_cart.png'); ?>" alt="">
                                <span class="badge badge-danger badge-pill position-absolute mt-n2"><?php echo $carts_total; ?></span>
                            </a>
                        <?php else:?>
                            <a href="<?php echo base_url('auth/login'); ?>" class="nav-login">
                                <img src="<?php echo base_url('assets/images/login_white.png'); ?>" alt="">
                            </a>
                        <?php endif;?>
                    </div>
                    <?php if (isset($isSearch) && $isSearch == "true"): ?>
                        <form action="<?php echo base_url('product/search') ?>" class="searchform">
                            <div class="input-group bg-light border-1" style="border-radius:10px;">
                                <input class="form-control border-0 bg-transparent custom-text-main" name="q" type="search" placeholder="Cari Produk">
                                <div class="input-group-append">
                                    <button class="btn" type="submit"><img src="<?php echo base_url('assets/images/searchicon.jpg'); ?>" alt="" class=""></button>
                                </div>
                            </div>
                        </form>
                    <?php endif; ?>
                    <?php if (isset($isSearchBar) && $isSearchBar == "true"): ?>
                        <form action="<?php echo base_url('product/search') ?>" class="searchbar">
                            <div class="input-group bg-light border-1" style="border-radius:10px;">
                                <input class="form-control border-0 bg-transparent custom-text-main" name="q" type="search" placeholder="Cari Produk">
                                <div class="input-group-append">
                                    <button class="btn" type="submit"><img src="<?php echo base_url('assets/images/searchicon.jpg'); ?>" alt="" class=""></button>
                                </div>
                            </div>
                        </form>
                    <?php endif; ?>
                    <div class="collapse navbar-collapse" id="navbarResponsive">
                        <ul class="navbar-nav ml-auto d-none d-lg-flex">
                            <?php if( $this->ion_auth->logged_in() ):?>
                                <li class="nav-item">
                                   <a class="nav-link <?php is_menu_active('home', $page ) ?>" href="<?php echo base_url('home'); ?>">Home</a>
                                </li>
                                <li class="nav-item">
                                   <a class="nav-link <?php is_menu_active('member-wishorder', $page ) ?>" href="<?php echo base_url('member/order/wishorder'); ?>">Wishlist</a>
                                </li>
                                <li class="nav-item">
                                   <a class="nav-link <?php is_menu_active(['member-medical-record','member-fitness','member-healthy-status' ], $page ) ?>" href="<?php echo base_url('member/medical_record'); ?>">Health</a>
                                </li>
                                <li class="nav-item">
                                   <a class="nav-link <?php is_menu_active(['member-order', 'member-preorder'], $page ) ?>" href="<?php echo base_url('member/order'); ?>">Order</a>
                                </li>
                                <li class="nav-item">
                                   <a class="nav-link <?php is_menu_active(['member-dashboard', 'member-setting'], $page ) ?>" href="<?php echo base_url('member/dashboard'); ?>">Account</a>
                                </li>
                                <li class="d-none d-md-block <?php is_menu_active( 'cart', $page ) ?>">
                                    <a href="<?php echo base_url('cart'); ?>" class="nav-link">
                                        <img src="<?php echo base_url('assets/images/shop_cart.png'); ?>" alt="">
                                        <?php if ($page !== 'cart') : ?>
                                            <span class="badge badge-danger badge-pill position-absolute mt-n2"><?php echo $carts_total; ?></span>
                                        <?php endif; ?>
                                    </a>
                                </li>
                                <?php else:?>
                                <li class="nav-item d-none d-md-block">
                                    <a class="nav-link <?php is_menu_active( 'login', $page ) ?>" href="<?php echo base_url( 'auth/login' ); ?>">Login</a>
                                </li>
                            <?php endif;?>
                        </ul>
                    </div>
                </div>
            </nav>
        </header> -->

    <link rel='stylesheet' id='semplice-frontend-stylesheet-css'  href='https://mymind.com/wp-content/themes/semplice6/assets/css/frontend.min.css?ver=6.0.3'/>
		<style>
			.menu > .active > a {
				color: #008080;
			}
			.menu-item > a {
			    color: #96DADA;
				font-size: 20px;
				font-family: 'Poppins';
				font-style: normal;
				font-weight: 400;
			}
			.menu-item > a:hover {
			    color: #008080;
			}
			.sticky-nav.semplice-navbar {
				padding: 2rem 2.2rem;
				background-color: #ffffff;
			}
			.sticky-nav .menu-item span {
				font-family: 'Poppins';
				letter-spacing: -0.03em !important;
			}
			.sticky-nav.is-sticky {
				height: 3rem;
				padding: 1.6rem 15px 1.74em !important;
				box-shadow: 0px 4px 22px rgb(116 130 151 / 14%);
				border-radius: 30px;
				top: 25px;
				left: 50%;
				width: 980px;
				transform: translateX(-50%) !important;
				transition: top .3s ease-out;
			}
			.sticky-nav.is-sticky img {
				transform: scale(.9) translateX(-5px);
				padding-top: 7px;
			}
			.sticky-nav.is-sticky .menu-item {
			    transform: translateY(-7px);
			}
			.sticky-nav.is-sticky .menu-item span {
			    font-size: 20px;
			}
			.sticky-nav.is-sticky .standard.navbar-right .menu li:last-child a::after {
			    top: 44%;
			}

			@media (max-width: 991px) {
				.is-frontend .semplice-navbar {
					position: fixed !important;
					height: 30px;
					width: 100%;
					padding: 25px 15px !important;
					border-radius: 0;
					top: 0;
					left: 0;
					transform: none !important;
				}
				.is-frontend .semplice-navbar .semplice-menu {
					right: 0 !important;
				}
			}

			@media (max-width: 767px) {
			.is-frontend #content-231 .sections {
				margin-top: 0 !important;
			}
			#content-holder .pin-spacer {
				padding-bottom: 0 !important;
				height: auto !important;
			}
			#content-holder .section-pin {
				position: static !important;
				width: 100% !important;
				height: 100% !important;
				transform: none !important;
			}
			#content-holder .section-pin .column,
			#content-holder .section-pin .is-content {
				opacity: 1 !important;
			}
			}
			@media (min-width: 1651px) {
			.sticky-nav.semplice-navbar {
				padding-left: 2.6rem;
				padding-right: 2.6rem;
			}
			}
			#content-231 #content_7113f8331 .spacer { background-color: #e2e8e8; }#content-231 #content_7113f8331 .spacer { height: 0.05555555555555555rem; }#content-231 #content_7113f8331 {padding-top: 0rem;}#content-231 #content_7113f8331 .is-content {}#content-231 #section_b41a1f219 {padding-top: 2.388888888888889rem;padding-bottom: 5.555555555555555rem;background-color: #ffffff;}#content-231 #content_091e8db96 {padding-top: 1.5rem;}#content-231 #content_091e8db96 .is-content {}#content-231 #content_34e9425f3 {padding-top: 1.3888888888888888rem;}#content-231 #content_34e9425f3 .is-content {}#content-231 #content_99db14c5b {padding-top: 1.9444444444444444rem;}#content-231 #content_99db14c5b .is-content {}#content-231 #content_325e1b29a {padding-top: 1.1111111111111112rem;}#content-231 #content_325e1b29a .is-content {}#content-231 #content_3c272c7d2 {padding-top: 1.3888888888888888rem;}#content-231 #content_3c272c7d2 .is-content {}#content-231 #content_176fb2013 {padding-top: 1.3888888888888888rem;}#content-231 #content_176fb2013 .is-content {}
			.nav_kghmh9pcm { background-color: transparent;; }.nav_kghmh9pcm { height: 4.833333333333333rem; }.is-frontend #content-231 .sections { margin-top: 4.833333333333333rem; }.nav_kghmh9pcm { padding-top: 1.8888888888888888rem; }.nav_kghmh9pcm { padding-bottom: 1.8888888888888888rem; }.nav_kghmh9pcm .navbar-inner .navbar-left, .nav_kghmh9pcm .navbar-inner .navbar-center, .nav_kghmh9pcm .navbar-inner .navbar-distributed { left: 1.7777777777777777rem; }.nav_kghmh9pcm .container-fluid .navbar-inner .navbar-right, .nav_kghmh9pcm .container-fluid .navbar-inner .navbar-distributed { right: 1.7777777777777777rem; }.nav_kghmh9pcm .container-fluid .hamburger a:after { padding-right: 1.1111111111111rem; }.nav_kghmh9pcm .navbar-inner .logo { margin-top: 0.05555555555555555rem; }.nav_kghmh9pcm .logo img, .nav_kghmh9pcm .logo svg { width: 8.055555555555555rem; }.nav_kghmh9pcm .navbar-inner .hamburger a.menu-icon span { background-color: #000000; }.nav_kghmh9pcm .navbar-inner .hamburger a.menu-icon { width: 24; }.nav_kghmh9pcm .navbar-inner .hamburger a.menu-icon span { height: 2px; }.nav_kghmh9pcm .navbar-inner .hamburger a.open-menu span::before { transform: translateY(-6px); }.nav_kghmh9pcm .navbar-inner .hamburger a.open-menu span::after { transform: translateY(6px); }.nav_kghmh9pcm .navbar-inner .hamburger a.open-menu:hover span::before { transform: translateY(-8px); }.nav_kghmh9pcm .navbar-inner .hamburger a.open-menu:hover span::after { transform: translateY(8px); }.nav_kghmh9pcm .navbar-inner .hamburger a.menu-icon { height: 14px; }.nav_kghmh9pcm .navbar-inner .hamburger a.menu-icon span { margin-top: 7px; }.nav_kghmh9pcm .navbar-inner nav ul li a span { font-size: 1.3333333333333333rem; }.nav_kghmh9pcm .navbar-inner nav ul li a span { color: #020101; }.nav_kghmh9pcm .navbar-inner nav ul li a span { text-transform: none; }.nav_kghmh9pcm .navbar-inner nav ul li a span { letter-spacing: 0.011111111111111112rem; }.nav_kghmh9pcm .navbar-inner nav ul li a span { border-bottom-width: 0rem; }.nav_kghmh9pcm .navbar-inner nav ul li a span { padding-bottom: 0rem; }.nav_kghmh9pcm .navbar-inner nav.standard ul { align-items: center; }.nav_kghmh9pcm .navbar-inner nav ul li a:hover span, .nav_kghmh9pcm .navbar-inner nav ul li.wrap-focus a span, .nav_kghmh9pcm .navbar-inner nav ul li.current-menu-item a span, .nav_kghmh9pcm .navbar-inner nav ul li.current_page_item a span { color: #ff5924; }[data-post-type="project"] .navbar-inner nav ul li.portfolio-grid a span, [data-post-type="post"] .navbar-inner nav ul li.blog-overview a span { color: #ff5924; }.nav_kghmh9pcm .navbar-inner nav ul li a:hover span, .nav_kghmh9pcm .navbar-inner nav ul li.current-menu-item a span, .nav_kghmh9pcm .navbar-inner nav ul li.current_page_item a span  { border-bottom-color: #748297; }[data-post-type="project"] .navbar-inner nav ul li.portfolio-grid a span, [data-post-type="post"] .navbar-inner nav ul li.blog-overview a span { border-bottom-color: #748297; }#overlay-menu { background-color: rgba(245, 245, 245, 1); }@media screen and (min-width: 992px) and (max-width: 1169.98px) { .nav_kghmh9pcm .navbar-inner .hamburger a.menu-icon { height: 14px; }.nav_kghmh9pcm .navbar-inner .hamburger a.menu-icon span { margin-top: 7px; }}@media screen and (min-width: 768px) and (max-width: 991.98px) { .nav_kghmh9pcm .navbar-inner .hamburger a.menu-icon { height: 14px; }.nav_kghmh9pcm .navbar-inner .hamburger a.menu-icon span { margin-top: 7px; }}@media screen and (min-width: 544px) and (max-width: 767.98px) { .nav_kghmh9pcm .navbar-inner .hamburger a.menu-icon { height: 14px; }.nav_kghmh9pcm .navbar-inner .hamburger a.menu-icon span { margin-top: 7px; }}@media screen and (max-width: 543.98px) { .nav_kghmh9pcm .navbar-inner .hamburger a.menu-icon { height: 14px; }.nav_kghmh9pcm .navbar-inner .hamburger a.menu-icon span { margin-top: 7px; }}

		</style>
	
	    <body class="home page-template-default page page-id-231 is-frontend static-mode static-transitions mejs-semplice-ui" data-post-type="page" data-post-id="231">
		    <div id="content-holder" data-active-post="231">
				<header class="nav_kghmh9pcm semplice-navbar active-navbar sticky-nav  no-headroom"  data-cover-transparent="disabled" data-bg-overlay-visibility="visible" data-mobile-fallback="enabled">
					<div class="container-fluid" data-nav="logo-left-menu-right">
						<div class="navbar-inner menu-type-text" data-xl-width="12" data-navbar-type="container-fluid">
                            <div class="logo navbar-left"><a class="d-none d-lg-block" href="<?php echo base_url('home'); ?>"><img src="<?php echo base_url('assets/images/BISA.png'); ?>" alt="logo" style="height: 70px!important; width: auto!important;"></a></div>
							<div class="logo navbar-left d-lg-none d-md-none">
								<?php if (isset($isBack) && $isBack == "true"): ?>
									<a onclick="history.go(-1);" class="navbar-brand">
										<img src="<?php echo base_url('assets/images/favicon/back_arrow.png'); ?>" class="d-block d-lg-none d-md-none" style="height: 35px!important;width: auto!important;">
									</a>
									<!-- <h4 class="d-block d-lg-none d-md-none" style="position: relative;margin:8% 0 0 18%; color:#fff; font-size:22px; bottom:7px;">Article</h4> -->
									<?php endif; ?>
									<?php if (isset($isBackDetail) && $isBackDetail == "true"): ?>
									<a onclick="history.go(-1);" class="navbar-brand">
										<img src="<?php echo base_url('assets/images/favicon/back_arrow.png'); ?>" class="d-block d-lg-none d-md-none" style="height: 35px!important;width: auto!important;">
									</a>
								<?php endif; ?>
							</div>
                            <nav class="standard navbar-right">
								<ul class="menu">
									<li class="menu-item <?php is_menu_active('home', $page ) ?>" id="menu-item-383">
										<a class="nav-link" href="<?php echo base_url('home'); ?>">
										    Home
									    </a>
									</li>
									<li class="menu-item <?php is_menu_active(['member-medical-record','member-fitness','member-healthy-status' ], $page ) ?>" id="menu-item-384">
										<a class="nav-link" href="<?php echo base_url('member/medical_record'); ?>">
										    Health
									    </a>
									</li>
									<li class="menu-item <?php is_menu_active(['member-dashboard', 'member-setting'], $page ) ?>" id="menu-item-385">
										<a class="nav-link" href="<?php echo base_url('member/dashboard'); ?>">
										    Account
									    </a>
									</li>
								</ul>
							</nav>
                            <!-- <div class="hamburger navbar-right semplice-menu"><a class="open-menu menu-icon"><span></span></a></div> -->
                        </div>
					</div>
			    </header>
				<div class="back-to-top d-none d-lg-block d-md-block" data-arrow-align="right">
					<a class="semplice-event" data-event-type="helper" data-event="scrollToTop"><img src="https://mymind.com/wp-content/uploads/2021/04/backtotop2.svg" alt="Back to top Arrow"></a>
				</div>
				<script type='text/javascript' src='https://mymind.com/wp-content/themes/semplice6/assets/js/shared.scripts.min.js?ver=6.0.3' id='semplice-shared-scripts-js'></script>
				<script type='text/javascript' src='https://mymind.com/wp-content/themes/semplice6/assets/js/frontend.scripts.min.js?ver=6.0.3' id='semplice-frontend-scripts-js'></script>
				<script type='text/javascript' id='mediaelement-core-js-before'>
				var mejsL10n = {"language":"en","strings":{"mejs.download-file":"Download File","mejs.install-flash":"You are using a browser that does not have Flash player enabled or installed. Please turn on your Flash player plugin or download the latest version from https:\/\/get.adobe.com\/flashplayer\/","mejs.fullscreen":"Fullscreen","mejs.play":"Play","mejs.pause":"Pause","mejs.time-slider":"Time Slider","mejs.time-help-text":"Use Left\/Right Arrow keys to advance one second, Up\/Down arrows to advance ten seconds.","mejs.live-broadcast":"Live Broadcast","mejs.volume-help-text":"Use Up\/Down Arrow keys to increase or decrease volume.","mejs.unmute":"Unmute","mejs.mute":"Mute","mejs.volume-slider":"Volume Slider","mejs.video-player":"Video Player","mejs.audio-player":"Audio Player","mejs.captions-subtitles":"Captions\/Subtitles","mejs.captions-chapters":"Chapters","mejs.none":"None","mejs.afrikaans":"Afrikaans","mejs.albanian":"Albanian","mejs.arabic":"Arabic","mejs.belarusian":"Belarusian","mejs.bulgarian":"Bulgarian","mejs.catalan":"Catalan","mejs.chinese":"Chinese","mejs.chinese-simplified":"Chinese (Simplified)","mejs.chinese-traditional":"Chinese (Traditional)","mejs.croatian":"Croatian","mejs.czech":"Czech","mejs.danish":"Danish","mejs.dutch":"Dutch","mejs.english":"English","mejs.estonian":"Estonian","mejs.filipino":"Filipino","mejs.finnish":"Finnish","mejs.french":"French","mejs.galician":"Galician","mejs.german":"German","mejs.greek":"Greek","mejs.haitian-creole":"Haitian Creole","mejs.hebrew":"Hebrew","mejs.hindi":"Hindi","mejs.hungarian":"Hungarian","mejs.icelandic":"Icelandic","mejs.indonesian":"Indonesian","mejs.irish":"Irish","mejs.italian":"Italian","mejs.japanese":"Japanese","mejs.korean":"Korean","mejs.latvian":"Latvian","mejs.lithuanian":"Lithuanian","mejs.macedonian":"Macedonian","mejs.malay":"Malay","mejs.maltese":"Maltese","mejs.norwegian":"Norwegian","mejs.persian":"Persian","mejs.polish":"Polish","mejs.portuguese":"Portuguese","mejs.romanian":"Romanian","mejs.russian":"Russian","mejs.serbian":"Serbian","mejs.slovak":"Slovak","mejs.slovenian":"Slovenian","mejs.spanish":"Spanish","mejs.swahili":"Swahili","mejs.swedish":"Swedish","mejs.tagalog":"Tagalog","mejs.thai":"Thai","mejs.turkish":"Turkish","mejs.ukrainian":"Ukrainian","mejs.vietnamese":"Vietnamese","mejs.welsh":"Welsh","mejs.yiddish":"Yiddish"}};
				</script>
				<script type='text/javascript' src='https://mymind.com/wp-includes/js/mediaelement/mediaelement-and-player.min.js?ver=4.2.16' id='mediaelement-core-js'></script>
				<script type='text/javascript' src='https://mymind.com/wp-includes/js/mediaelement/mediaelement-migrate.min.js?ver=5.9.3' id='mediaelement-migrate-js'></script>
				<script type='text/javascript' id='mediaelement-js-extra'>
				/* <![CDATA[ */
				var _wpmejsSettings = {"pluginPath":"\/wp-includes\/js\/mediaelement\/","classPrefix":"mejs-","stretching":"responsive"};
				/* ]]> */
				</script>
				<script type='text/javascript' id='semplice-frontend-js-js-extra'>
				/* <![CDATA[ */
				var semplice = {"default_api_url":"https:\/\/mymind.com\/wp-json","semplice_api_url":"https:\/\/mymind.com\/wp-json\/semplice\/v1\/frontend","template_dir":"https:\/\/mymind.com\/wp-content\/themes\/semplice6","category_base":"\/category\/","tag_base":"\/tag\/","nonce":"89e178a6e1","frontend_mode":"static","static_transitions":"enabled","site_name":"A brand new mind for you.","base_url":"https:\/\/mymind.com","frontpage_id":"231","blog_home":"https:\/\/mymind.com\/blog","sr_status":"disabled","blog_sr_status":"enabled","is_preview":"","password_form":"\r\n<div class=\"post-password-form\">\r\n\t<div class=\"inner\">\r\n\t\t<form action=\"https:\/\/mymind.com\/wp-login.php?action=postpass\" method=\"post\">\r\n\t\t\t<div class=\"password-lock\"><svg xmlns=\"http:\/\/www.w3.org\/2000\/svg\" width=\"35\" height=\"52\" viewBox=\"0 0 35 52\">\r\n  <path id=\"Form_1\" data-name=\"Form 1\" d=\"M31.3,25.028H27.056a0.755,0.755,0,0,1-.752-0.757V14.654a8.8,8.8,0,1,0-17.608,0v9.616a0.755,0.755,0,0,1-.752.757H3.7a0.755,0.755,0,0,1-.752-0.757V14.654a14.556,14.556,0,1,1,29.111,0v9.616A0.755,0.755,0,0,1,31.3,25.028Zm-3.495-1.514h2.743V14.654a13.051,13.051,0,1,0-26.1,0v8.859H7.192V14.654a10.309,10.309,0,1,1,20.617,0v8.859Zm4.43,28.475H2.761A2.77,2.77,0,0,1,0,49.213V25.28a1.763,1.763,0,0,1,1.755-1.766H33.242A1.763,1.763,0,0,1,35,25.28V49.213A2.77,2.77,0,0,1,32.239,51.988ZM1.758,25.028a0.252,0.252,0,0,0-.251.252V49.213a1.259,1.259,0,0,0,1.254,1.262H32.239a1.259,1.259,0,0,0,1.254-1.262V25.28a0.252,0.252,0,0,0-.251-0.252H1.758ZM20.849,43h-6.7a0.75,0.75,0,0,1-.61-0.314,0.763,0.763,0,0,1-.1-0.682l1.471-4.44a4.1,4.1,0,1,1,5.184,0L21.563,42a0.763,0.763,0,0,1-.1.682A0.75,0.75,0,0,1,20.849,43ZM15.2,41.487H19.8l-1.319-3.979a0.76,0.76,0,0,1,.33-0.891,2.6,2.6,0,1,0-2.633,0,0.76,0.76,0,0,1,.33.891Z\"\/>\r\n<\/svg>\r\n<\/div>\r\n\t\t\t<p class=\"title\">This content is protected.<\/p>\r\n\t\t\t<p class=\"subtitle\">To view, please enter the password.<\/p>\r\n\t\t\t<div class=\"input-fields\">\r\n\t\t\t\t<input name=\"post_password\" class=\"post-password-input\" type=\"password\" size=\"20\" maxlength=\"20\" placeholder=\"Enter password\" \/><input type=\"submit\" class=\"post-password-submit\" name=\"Submit\" value=\"Submit\" \/>\t\t\t<\/div>\r\n\t\t<\/form>\r\n\t<\/div>\r\n<\/div>","portfolio_order":[],"customize":{"cursor":null},"gallery":{"prev":"<svg version=\"1.1\" id=\"Ebene_1\" xmlns=\"http:\/\/www.w3.org\/2000\/svg\" xmlns:xlink=\"http:\/\/www.w3.org\/1999\/xlink\" x=\"0px\" y=\"0px\"\n\twidth=\"18px\" height=\"40px\"  viewBox=\"0 0 18 40\" enable-background=\"new 0 0 18 40\" xml:space=\"preserve\">\n<g id=\"Ebene_2\">\n\t<g>\n\t\t<polygon points=\"16.3,40 0.3,20 16.3,0 17.7,1 2.5,20 17.7,39 \t\t\"\/>\n\t<\/g>\n<\/g>\n<\/svg>\n","next":"<svg version=\"1.1\" id=\"Ebene_1\" xmlns=\"http:\/\/www.w3.org\/2000\/svg\" xmlns:xlink=\"http:\/\/www.w3.org\/1999\/xlink\" x=\"0px\" y=\"0px\"\n\twidth=\"18px\" height=\"40px\" viewBox=\"0 0 18 40\" enable-background=\"new 0 0 18 40\" xml:space=\"preserve\">\n<g id=\"Ebene_2\">\n\t<g>\n\t\t<polygon points=\"0.3,39 15.5,20 0.3,1 1.7,0 17.7,20 1.7,40 \t\t\"\/>\n\t<\/g>\n<\/g>\n<\/svg>\n"}};
				/* ]]> */
				</script>
				<script type='text/javascript' src='https://mymind.com/wp-content/themes/semplice6/assets/js/frontend.min.js?ver=6.0.3' id='semplice-frontend-js-js'></script>

				<script type="text/javascript" id="semplice-custom-javascript">// For sticky nav
				var pageTop = document.getElementById('content-holder').offsetTop;
				var headerEl = document.querySelector('.sticky-nav');

				window.onscroll = function() {
					let y = (document.documentElement && document.documentElement.scrollTop) || document.body.scrollTop;
					if (y > pageTop) {
					headerEl.classList.add('is-sticky');
					}
					else {
					headerEl.classList.remove('is-sticky');
					}
				}
			</script>	
        </body>

						

            