<!-- Web View -->
<footer class="d-none d-lg-block border-top" style="margin-top: 15%;">
    <div class="container">
        <div class="row ml-3">
            <div class="col-md-12 d-flex flex-row" style="display: grid; gap: 60px;">
                <div>
                    <div class="d-flex flex-row mt-5">
                        <div><img src="<?php echo base_url('assets/images/BISA.png'); ?>" alt="logo" style="height: 50px!important; width: auto!important;"></div>
                        <div class="ml-3 mt-2" style="font-family: 'poppins'; font-style: normal; font-weight: 600;">BISA Sehat</div>
                    </div>
                    <div class="d-flex justify-content-start text-left mt-3 mb-5">
                        <div style="font-family: 'poppins'; font-style: normal; font-weight: 400; font-size: 18px;">Bersama Indonesia <br>Sehat Alami, PT <?php echo date('Y'); ?></div>
                    </div>
                </div>
                <div>
                    <div class="mt-5 text-left">
                        <div style="font-family: 'poppins'; font-style: normal; font-weight: 400; font-size: 18px;"><a class="text-dark" href="<?php echo base_url('About_us/index'); ?>">Tentang Kami</a></div>
                        <div class="terms"><a href="<?php echo base_url('Terms/index'); ?>">Syarat dan Ketentuan</a></div>
                    </div>
                </div>
            </div>
            <!-- <div class="col-md-12">
                <div class="social-media">
                    <h6 class="ml-3 text-left" style="color: white; font-weight: bold; font-size: 20px;">Ikuti Kami</h6>
                    <ul>
                        <li><a href="https://wa.me/6281320005015" target="_blank" title="WhatsApp"><img src="<?php echo base_url('assets/images/social_media/logo_wa.png'); ?>" alt=""></i></a></li>
                        <li><a href="https://youtube.com/channel/UC1zjZyzNgWNUc99szQ6HL0A" target="_blank" title="Youtube"><img src="<?php echo base_url('assets/images/social_media/logo_youtube.png'); ?>" alt=""></a></li>
                        <li><a href="https://vt.tiktok.com/ZSef1RXsc" target="_blank" title="Tiktok"><img src="<?php echo base_url('assets/images/social_media/logo_tiktok.png'); ?>" alt=""></a></li>
                        <li><a href="https://www.instagram.com/aplikasibisa.id/" target="_blank" title="Instagram"><img src="<?php echo base_url('assets/images/social_media/logo_ig.png'); ?>" alt=""></a></li>
                        <li><a href="https://www.facebook.com/aplikasibisa.id" target="_blank" title="Facebook"><img src="<?php echo base_url('assets/images/social_media/logo_fb.png'); ?>" alt=""></a></li>
                    </ul>
                </div>
            </div> -->
        </div>
    </div>
</footer>

<!-- Mobile View -->
<?php if ((current_url() == base_url() . 'index.php/home') ||
        (current_url() == base_url() . 'index.php/member/medical_record') ||
        (current_url() == base_url() . 'index.php/member/dashboard') ||
        (current_url() == base_url() . 'index.php/member/fitness') ||
        (current_url() == base_url() . 'index.php/member/healthy/status')
        ): ?>
    <?php log_message('debug', current_url()); ?>
    <!-- Floating Bottom -->
    <div class="container d-lg-none d-xl-none" style="margin-bottom:22%;">
        <nav class="navbar navbar-expand d-lg-none d-xl-none fixed-bottom" style="left: 30px; right: 30px; bottom: 20px; padding: 5px; background-color:#008080; border-radius:25px">
            <ul class="navbar-nav nav-justified w-100 pr-5">
                <li class="nav-item">
                    <a href="<?php echo base_url('home'); ?>" class="float-button">
                        <?php
                            $this->load->helper('url');

                            if (current_url() == base_url() . 'index.php/home'):
                        ?>
                            <img src="<?php echo base_url('assets/images/floating_button/home_white.png'); ?>" alt="">
                        <?php else: ?>
                            <img src="<?php echo base_url('assets/images/floating_button/home_outline.png'); ?>" alt="">
                        <?php endif; ?>

                        <!-- <h6 class="small d-block">Home</h6> -->
                    </a>
                </li>
                <!-- <li class="nav-item">
                    <a href="<?php echo base_url('member/order/wishorder'); ?>" class="float-button">
                        <?php
                            $this->load->helper('url');

                            if (current_url() == base_url() . 'index.php/member/order/wishorder'):
                        ?>
                            <img src="<?php echo base_url('assets/images/floating_button/wishlist_white.png'); ?>" alt="">
                        <?php else: ?>
                            <img src="<?php echo base_url('assets/images/floating_button/wishlist_outline.png'); ?>" alt="">
                        <?php endif; ?>

                        <h6 class="small d-block">Wishlist</h6>
                    </a>
                </li> -->
                <li class="nav-item">
                    <a href="<?php echo base_url('member/medical_record'); ?>" class="float-button">
                        <?php
                            $this->load->helper('url');

                            if ((current_url() == base_url() . 'index.php/member/medical_record') ||
                            (current_url() == base_url() . 'index.php/member/fitness') ||
                            (current_url() == base_url() . 'index.php/member/healthy/status') ):
                        ?>
                            <img src="<?php echo base_url('assets/images/floating_button/health_white.png'); ?>" alt="">
                        <?php else: ?>
                            <img src="<?php echo base_url('assets/images/floating_button/health_outline.png'); ?>" alt="">
                        <?php endif; ?>

                        <!-- <h6 class="small d-block">Health</h6> -->
                    </a>
                </li>
                <!-- <li class="nav-item">
                    <a href="<?php echo base_url('member/order'); ?>" class="float-button">
                        <?php
                            $this->load->helper('url');

                            if (current_url() == base_url() . 'index.php/member/order'):
                        ?>
                            <img src="<?php echo base_url('assets/images/floating_button/order_white.png'); ?>" alt="">
                        <?php else: ?>
                            <img src="<?php echo base_url('assets/images/floating_button/order_outline.png'); ?>" alt="">
                        <?php endif; ?>

                        <h6 class="small d-block">Order</h6>
                    </a>
                </li> -->
                <li class="nav-item">
                    <a href="<?php echo base_url('member/dashboard'); ?>" class="float-button">
                        <?php
                            $this->load->helper('url');

                            if (current_url() == base_url() . 'index.php/member/dashboard'):
                        ?>
                            <img src="<?php echo base_url('assets/images/floating_button/account_white.png'); ?>" alt="">
                        <?php else: ?>
                            <img src="<?php echo base_url('assets/images/floating_button/account_outline.png'); ?>" alt="">
                        <?php endif; ?>

                        <!-- <h6 class="small d-block">Account</h6> -->
                    </a>
                </li>
            </ul>
        </nav>
    </div>
<?php endif; ?>
    <!-- Start of Tawk.to Script-->
<!--    <script type="text/javascript">-->
<!--        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();-->
<!--        (function(){-->
<!--        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];-->
<!--        s1.async=true;-->
<!--        s1.src='https://embed.tawk.to/61cb2d1fc82c976b71c3d420/1fo0qghss';-->
<!--        s1.charset='UTF-8';-->
<!--        s1.setAttribute('crossorigin','*');-->
<!--        s0.parentNode.insertBefore(s1,s0);-->
<!--        })();-->
<!--    </script>-->
    <!--End of Tawk.to Script -->

    <!-- Initialize Swiper -->
    <script>
        var swiper = new Swiper(".mySwiper", {
            breakpoints: {
                300:{
                    slidesPerView: 2.3,
                },
                768:{
                    slidesPerView: 4,
                    spaceBetween: 10,
                },
                1200:{
                    slidesPerView: 5,
                    spaceBetween: 25,
                },
            },
            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            },
        });

        var cardSwiper = new Swiper(".cardSwiper", {
            spaceBetween: 50,
            breakpoints: {
                300:{
                    slidesPerView: 2.2,
                },
                768:{
                    slidesPerView: 4,
                },
                1200:{
                    slidesPerView: 4,
                },
            },
        });

        var swiper = new Swiper(".swiper-chart", {
            breakpoints: {
                300:{
                    slidesPerView: 1.2,
                },
                768:{
                    slidesPerView: 2.2,
                    spaceBetween: 10,
                },
                1200:{
                    slidesPerView: 2.2,
                    spaceBetween: 25,
                },
            },
        });

        $(document).ready(function(){
            var owl = $('.owl-monitoring');
            owl.owlCarousel({
                items:2,
            });
        });

        var swiper = new Swiper(".swiper-health", {
            slidesPerView: 2.2,
            spaceBetween: 7,
        });

        $(document).ready(function(){
            var owl = $('.owl-diagnose');
            owl.owlCarousel({
                mouseDrag: false,
                responsive:{
                    0   :{
                        items:1
                    },
                    600:{
                        items:3
                    },
                    1000:{
                        items:4
                    }
                }
            });
            
            // Custom Button
            $('.customNextBtn').click(function() {
                owl.trigger('next.owl.carousel');
            });
            $('.customPreviousBtn').click(function() {
                owl.trigger('prev.owl.carousel');
            });
        });

        $(document).ready(function(){
            var owl = $('.owl-article');
            owl.owlCarousel({
                // nav:true,
                responsive:{
                    0:{
                        items:1.3,
                        loop: true
                    },
                    600:{
                        items:3,
                        mouseDrag: false,
                    },
                    1000:{
                        items:4,
                        mouseDrag: false,
                    }
                }
            });
            
            // Custom Button
            $('.customNextArticle').click(function() {
                owl.trigger('next.owl.carousel');
            });
            $('.customPreviousArticle').click(function() {
                owl.trigger('prev.owl.carousel');
            });
        });
    </script>

    <div id="included-js"></div>
    <script type="text/javascript">
            function includeJs(jsFilePath) {
                var js = document.createElement("script");

                js.type = "text/javascript";
                js.src = jsFilePath;

                // document.body.appendChild(js);
                $("#included-js").after( js );
            }
    </script>

    <script type="text/javascript">
        // jquery extend function
        $.extend(
        {
            redirectPost: function(location, args)
            {
                var form = '';
                $.each( args, function( key, value ) {
                    form += '<input type="hidden" name="'+key+'" value="'+value+'">';
                });
                $('<form action="'+location+'" method="POST">'+form+'</form>').appendTo('body').submit();
            }
        });
    </script>

    <?php
        if ( isset( $scripts ) ) {
            foreach ($scripts as $key => $script) {
                if ( is_string($script) ) {
                    echo '<script type="text/javascript" src="'.$script.'?v='.date('YmdHis').'"></script>';
                } else {
                    if ( $script['attr'] ) {
                        $attribute = $script['attr']['name'].'="'.$script['attr']['value'].'"';
                    } else {
                        $attribute ='';
                    }
                    echo '<script type="text/javascript" src="'.$script['src'].'?v='.date('YmdHis').'" '.$attribute.' ></script>';
                }
            }
        }
    ?>

</body>
</html>