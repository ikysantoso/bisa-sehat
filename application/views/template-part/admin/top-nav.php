<!-- Topnav -->
    <nav class="navbar navbar-top navbar-expand navbar-dark bg-primary border-bottom">
        <div class="container-fluid">
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Search form -->
                <form class="navbar-search navbar-search-light form-inline mr-sm-3 invisible" id="navbar-search-main">
                    <div class="form-group mb-0">
                        <div class="input-group input-group-alternative input-group-merge">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-search"></i></span>
                            </div>
                            <input class="form-control" placeholder="Search" type="text">
                        </div>
                    </div>
                    <button type="button" class="close" data-action="search-close" data-target="#navbar-search-main" aria-label="Close">
                    <span aria-hidden="true">×</span>
                    </button>
                </form>
                <!-- Navbar links -->
                <ul class="navbar-nav align-items-center  ml-md-auto ">
                    <li class="nav-item d-xl-none">
                        <!-- Sidenav toggler -->
                        <div class="pr-3 sidenav-toggler sidenav-toggler-dark active" data-action="sidenav-pin" data-target="#sidenav-main">
                            <div class="sidenav-toggler-inner">
                                <i class="sidenav-toggler-line"></i>
                                <i class="sidenav-toggler-line"></i>
                                <i class="sidenav-toggler-line"></i>
                            </div>
                        </div>
                    </li>
                    <li class="nav-item d-sm-none">
                        <a class="nav-link" href="#" data-action="search-show" data-target="#navbar-search-main">
                        <i class="ni ni-zoom-split-in"></i>
                        </a>
                    </li>
                    <li class="nav-item dropdown">
                        <?php 
                        $notification_total = 0;

                        $models = ['BisapayLogsModel', 'TransactionModel'];

                        foreach ($models as $key => $_model) {
                            if ( !class_exists($_model) ) {
                                $this->load->model($_model);
                            }
                        }
                        
                        // topup notification
                        $bisapay_topup = BisapayLogsModel::where(
                            [
                                'status'                => 'waiting',
                                'log_type_description'  => 'topup'
                            ]
                        )->orderBy('id', 'desc')->get();
                        if ($bisapay_topup->count() > 0) {
                            $notification_total++;
                        }

                        // withdraw notification
                        $bisapay_withdraw = BisapayLogsModel::where(
                            [
                                'status'                => 'waiting',
                                'log_type_description'  => 'withdraw'
                            ]
                        )->orderBy('id', 'desc')->get();
                        if ($bisapay_withdraw->count() > 0) {
                            $notification_total++;
                        }

                        // transaction manual payment notification
                        $transactions = TransactionPaymentModel::where('status','waiting')->orderBy('id', 'desc')->get();
                        if ( $transactions->count() > 0 ) {
                            $notification_total++;
                        }
                                               
                        ?>

                        <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="ni ni-bell-55"></i>
                            <?php if ($notification_total > 0): ?>
                            <span class="badge badge-danger p-1 position-absolute"><?php echo $notification_total; ?></span>
                            <?php endif; ?>
                        </a>
                        <div class="dropdown-menu dropdown-menu-xl  dropdown-menu-right  py-0 overflow-hidden">
                            <!-- Dropdown header -->
                            <div class="px-3 py-3">
                                <h6 class="text-sm text-muted m-0">
                                    <?php if ($notification_total > 0): ?>
                                        You have <strong class="text-primary"><?php echo $notification_total; ?></strong> notifications.
                                    <?php else: ?>
                                        No notification.
                                    <?php endif; ?>
                                </h6>
                            </div>
                            <!-- List group -->
                            <div class="list-group list-group-flush">

                                <?php if ( $bisapay_topup->count() > 0 ): ?>
                                <a href="<?php echo base_url('admin/bisapay/list?status=waiting') ?>" class="list-group-item list-group-item-action">
                                    <div class="row align-items-center">
                                        <div class="col-auto">
                                            <!-- Avatar -->
                                            <img alt="Image placeholder" src="<?php echo base_url('assets/admin/img/icons/fa/hand-holding-usd-solid.svg') ?>" class="avatar rounded-circle p-2 bg-success">
                                        </div>
                                        <div class="col ml--2">
                                            <div class="d-flex justify-content-between align-items-center">
                                                <div>
                                                    <h4 class="mb-0 text-sm">Bisapay Topup</h4>
                                                </div>
                                                <div class="text-right text-muted">
                                                    <small><?php echo time_elapsed_string($bisapay_topup[0]->created_at) ?></small>
                                                </div>
                                            </div>
                                            <p class="text-sm mb-0">Ada <?php echo ($bisapay_topup->count() == 1) ? 'satu' : 'beberapa' ?> aktifitas topup saldo Bisapay yang perlu dikonfirmasi</p>
                                        </div>
                                    </div>
                                </a>
                                <?php endif; ?>

                                <?php if ( $bisapay_withdraw->count() > 0 ): ?>
                                <a href="<?php echo base_url('admin/bisapay/withdraw?status=waiting') ?>" class="list-group-item list-group-item-action">
                                    <div class="row align-items-center">
                                        <div class="col-auto">
                                            <!-- Avatar -->
                                            <img alt="Image placeholder" src="<?php echo base_url('assets/admin/img/icons/fa/money-bill-wave-alt-solid.svg') ?>" class="avatar rounded-circle p-2 bg-warning">
                                        </div>
                                        <div class="col ml--2">
                                            <div class="d-flex justify-content-between align-items-center">
                                                <div>
                                                    <h4 class="mb-0 text-sm">Bisapay Withdraw</h4>
                                                </div>
                                                <div class="text-right text-muted">
                                                    <small><?php echo time_elapsed_string($bisapay_withdraw[0]->created_at) ?></small>
                                                </div>
                                            </div>
                                            <p class="text-sm mb-0">Ada <?php echo ($bisapay_withdraw->count() == 1) ? 'satu' : 'beberapa' ?> aktifitas tarik saldo Bisapay yang perlu dikonfirmasi</p>
                                        </div>
                                    </div>
                                </a>
                                <?php endif; ?>

                                <?php if ( $transactions->count() > 0 ): ?>
                                <a href="<?php echo base_url('admin/order/waiting_confirmation') ?>" class="list-group-item list-group-item-action">
                                    <div class="row align-items-center">
                                        <div class="col-auto">
                                            <!-- Avatar -->
                                            <img alt="Image placeholder" src="<?php echo base_url('assets/admin/img/icons/fa/file-invoice-dollar-solid.svg') ?>" class="avatar rounded-circle p-2 bg-info">
                                        </div>
                                        <div class="col ml--2">
                                            <div class="d-flex justify-content-between align-items-center">
                                                <div>
                                                    <h4 class="mb-0 text-sm">Konfirmasi Pembayaran</h4>
                                                </div>
                                                <div class="text-right text-muted">
                                                    <small><?php echo time_elapsed_string($transactions[0]->created_at) ?></small>
                                                </div>
                                            </div>
                                            <p class="text-sm mb-0">Ada <?php echo ($transactions->count() == 1) ? 'satu' : 'beberapa' ?> pembayaran transaksi yang perlu dikonfirmasi</p>
                                        </div>
                                    </div>
                                </a>
                                <?php endif; ?>
                            </div>
                            <!-- View all -->
                            <!-- <a href="#!" class="dropdown-item text-center text-primary font-weight-bold py-3">View all</a> -->
                        </div>
                    </li>
                </ul>
                <ul class="navbar-nav align-items-center  ml-auto ml-md-0 ">
                    <li class="nav-item dropdown">
                        <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <div class="media align-items-center">
                                <span class="avatar avatar-sm rounded-circle">
                                <img alt="Image placeholder" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAA0lBMVEUAebrw8PAAebwAa7b9+Pax1OPw8O8Beb0Aerzx7/Dx8O/v8PLv8e4BerkAb7f///oAbrcAdLn///YAdbnw7/MAabQAbrP49fIAdLcAZbL/+/QAc7sAY7L///sAbrsAarSjxdxIi8VcoMy/2efM3ur89PRvq9IAYLMAfLj09e/k7O3M4+qr0OOZxt6Ivtx8t9ZCmMYAhL5+rdDW6erR5e9Ul8eWvtuGtdm60+Q/isZSlclgns8sjMLD3eV7qdDd5etxr9Z1pdMTib6SttWkzt83lcs6XG4pAAAVfElEQVR4nO1dC1vbuNK2FWNZluRYthMlNpQQQhqn0JZbCCk07Lfb//+XvhmH9hByJbYJ3mfnbLucLsV+o9HcNPPKMP/tYuz7BUqX/xBWX/5DWH0pEyEzbZNxYTOTw5ccvjZdV8Afwp8I1zSF7br4XfAfbbu0tyh7DREeBzBCx3E9ji2Q2IpjrZlrm/AfmLBF9n2lvUGJCCPbjSLTtYW2giCOTg/OOxdfv337evHPPwc33KoH8Rljth1xWNsSpUSEAhQS0KW8d/FdyrB9CP9rHDfa7fbh54a8vOx2pkJrLeyovAU0S0Vom2msrzv9E+r7XqKIQ6QklBDpGNJQTc9vypOr86lluW6ZEMtDyOLAvOhKP/EMSaVBHIfCLwkoHUIpdahhtNQndf90YAW6tLcoHiHnbmQzZro6Pe2qtkeM9eIo/zi5urZ0GtkczJJb8PsUjhDsoxCwr1I+7itPSboBoeMQYrRq8uom1eg4SpCCEQqTwVqcBTe3LY9QUEi5AaEB30EHypf9YQCWVUTFvo9ZOELGXKGDYb/hgUkhBiFqg5YiSkKoQf1PHZHyYbGvg1L4PjS1OB/4BnUMwAe/1qMjoKdO9m1U+bcHcVrw65gl7MN42PUVrB4F54CLs3H9QJMpuA/HIJ5/UbeKfR+zQITcNQWzz3RP+Zvs5woBP/L59iZ2IzMq0qIWhtAGQ8/TdGQkjrMbQorLKE8tiMiLDOMKQ+hiiFbvwg40dkYoE6mSTixcUdRbmUXuw4jp4W0bPCDZEWG2KSVt3x0VGooXp6UsvpnU0Hw6O+5DCOYgvnNI+04UGcUVhlBY17KJwebuWoqfDkSv0v+rSKdRAEIGJoaJ9MvAo7vq5wsBz2i0J0faFaKYIK4AhFiCsM+uT7wC8M0sqtG+SjU3i4ngitFSNz2a+HSzg99GUFelN6pHbjH2pgCELuNn5m07hOyvGC2VsJ2P7wLxYRBCvqTvamhh5I5W9KVI/KCIQ9u9+MNoqeDBuAlOEN1ZfoQG1gGIE6r7YTE+Ix9CBtmSaVvX3hZJ0htx0uNbAR9efoOaC6FruzYXWkyON2W6b5UQ8mL/LrCZnTuAy4cwwrJMfeQZtAgr+kLA2shW4ya2Gc+7iLkQMkiYmO6FLawQFo0QcuIJ6Gluc5MPIXzALOj61CkimnkpYLIUlc3OkblfLbWFG1kH7YL8xEvJonAC9tTdr5bakdDD+1ax4F6IrP0Icqf7uRBGpmt1vNIAGkR5Q5234p/PH9pMTJoFO4oXQo3GVyvvIubzFjzo7Fp32kYkBEnXeSObXAi5GT/4s5cp2JY+IzSIP9orQhY/tov29S+FUJk85M0w8iC0RfzUKNpPvBQqCfXGFmx3tru9ybWGenhStKufE6za+d06NgPsC+FYERKWhxDyMScMhWZ5OhnyIfzZVOX5CvQWkjqH5zGzc7iMHAiZO7xXeKhSmqCWSr9vsTx1/jxrqKcydDadn+URh9IB9Sb8DFKY90bITSbco1Gz8MRwiahHbeZwGbuvobCDrucYRdTX1kvSgUR49424K0KbCzOWCZHvsIY/U5GjdrorQsFdzUlIdz2GeQvCH3g0+e4ITWbrntciG7stCkA44XlOhXdHKILzQ8jundLVtOVMWY5axo4IGWdu8LVNNzZbFIFQ9XSO5r6d15Az61uDzrxyuUKSsbUPhCY763sOMQqp5K8VmnQsvrua7oxQuLrrA8LyjSlpjoIc/Zk7I7RdcauczV1P+cXxroIcoffOHl/w4UmI7bDlI6z1g/eP2gTjOkNYup2B7KJxG9i7F/d3RZgl+O+E8BjS/HePaWzXRoSUvIM/dPxuPUdZeFeENtNikrwLQoKW5v1tKeQzuuttanIuRBxvVH//KgYzmWv13wchUSNrD5UoYbP4Ww2yw/K9hVLjeA91Gttk8ddDrNmWbk3DsKdzHCLujBDW8OIQLU3p2VNoTN19RN62GT9iq17RJ/hLED4M3T3sQyaYHhL1HnGp+iFM/u4IbSYibSlllHpu8YzwrzTPYf7uVQzTDrpNSWWJdRqamTFIDwXfS83bro98Sko9P8x+I4/a3scaAkKrFyalZk/4o0lyMjT3dH5o48kMKfOEVGK3qd8/g1Tm3bWU41GJK/5S1ChVSx2DNC6sfVSEM4TmWScpDx4iBEvT+jTUeY6ecp9yh+V0YcyEOo7h3x65uYbZ8/XTpH2/TEsDe1z553GeV8yJUFgH7TItjSFpOBhqiGj2tYbcjC9LbPrCdhPvKkXuhb1pqRmMaiUCxDnU3l57okzBymy+BEtz3NWzAsZ+OhVw5tDqeA6RTtHlDIpj0mBJ29PcQ1459yGkUFKVUHCbjW6Q4195TiwKQAiJvmldHJbgEgkqhQyNns49bZmvR5jbjAW/PEpk4SMlCNIfWYLlbdbP5w9Nl5lxT7XKKNYQCllFmn9oJp+WMm4Lpn9CYFOwojoSnP3hQczsHGejBSCcCUSnvpQQJZPiIjgcea71626OM6cCEXLr5lhh1U0WZ1PBUzQns4Hn/fbqZyJ4MDp2BkZIius8kVS2TmfRzAdAaDO7/tOnkoTFdS048vA8yLDl5jkrYkpWcD2c1LA5qjB7E/r9WBTDHlXIpLMdxdxpJrKwNj7Z+Cv93Qf1AdYQxT6aPngOldSReXuIKJjkRj91RUF8Q0VxKtjx46CFbjHveRtSgfm/sHs9T3P3CymMNYLpU7+JEfMgn7khUh4/XOsozwDCnBTH3iLim3vw/DSnzxiAisIKQqyUoy/4pRTH3uJG8XTyyRnktDak1tWaIysa/2AII5dHFr88NHKdmUrjcz/VHMdv83Q+v5Q3IMyOR7JQH1QIQm4cArY5MtAJM5qZdst8UtjsRt7WhoIWGPmwqPTD8wD8q8ieBo8SM3YMnvUoYTr6dsaTt6xhZCPFgA0oIyFwkNuG52G5lmdUqyaam7TjN51Zgv6GpYMPBdkiaO3kNIYfjW6CZb8Ej1x8EGQxPIqQ75Qjp2ZZCBEKJKTI2mlZVr0eBPDrjCPH159KEY97g8+AUL7F3kBmosBAqc9dZIrg0e+TJvgE46AOT0FuV42UfEjJ+Maq1BsQCiYEx75SKxW9cefp1+XD5WW/F8CKst+643Iei69eU70pRKUkhB3o3/9TP2Mui/6UZpjm4++XDw+X37+dj3vD1EIvab9xf74BIY9MzuLYfBx1J6qpfB+AND8ZP69j9ueI1mVc6Pj0R+ttdDzgRH11d20xXEAXn5R5Q/hBnu95PkjiycnP8y9nluZvDHXeYmnAHwRfvt4mfivJTAmhyG1Zu/8ntn5HWDaqsWul56r9lsoN9T8/9OopMn1kFXyBe9CKn6iX5Zw4XEVU4nlO92J4pN/mRbZCyBlqhk7T8fdGI6HzLC3SOHyYxqg/7M+3R3H6z6TWQspALKZKtJVZRGcYs7AO/58Et0LwFJT4re5j/fkABuwoEqAKnZ47DVD2Od9DvMPaUy/VJkfTZtvRFrnVVghh9bhric7E8xWlr54Ktt5zRjxm9u/pJCTp1PjtyktoVtxwkHYWuUqzLyh2IDjZHzjS8f2wP9bx76ohbETb5sKaduEv47PmNrR0Qp9OHlNL2BDXuduEPdshjJgIvtw2fQOZcl8/1ZADVZt8qafRbyuHtoe7sej1ZaPmZ+mC83u6xnFkRvuJPwJet3H4MOqllhv9oWmBcI3peufeN3AFXz8LPpSWHz5NgwjrfOYWPRpbIYSYc9hv+7N3wyUg8w+ldNBsXA0t94+ZBzsPmqSD+PHpu1RKtULj+e8iWvxCtVSiZPdrL4i1a4PXe97JYKvSdPrQUJIOFhFS5MuA7KrW0TEHy+1u9v/b7UOrd1ILs8Z8SRcQEicElKH/ME6fKR4g4nGRq5WzMyvmN+dX3XtDJbXmse/Xasf+cc1TSj78GI2/6BjUk0Eo8ceNR9GRGA08g4QOWbKGMgsOSOL1b+LtvMY2CFm9EzYhq6HSyDbG6x4aHNeFqCtJ+mLWrszxHwGqzWFPpdqKxXDa6/S73e7379/h9+7duDcdihhWD/QMnCmOo8/el6XBzYmv8GcSkj1t7lkENi+efUMOSb4E+OPzIsx0Lb1qG3RjzgBbtPnpAMNmPJNi85GH6+oYY5OZHNUtPZf+sSw6Q09vDb81/K26AVvJeRZssGg91dIGhPABawEANz9SogNoJbenR6mLfX3zvXYcObMgabdnwuabK4QNoSgE8cwSB9Jrbce8KFVyEbuzSwfWmdRNa8jd+p0H9m9jcxcWvB1H+d63YV2zCG9CeL1P/pihxc8RQEYQg552VRNUUG0V8slWG1YRP7C1NNmb9qGo94/Vol9aFDwOQ35r2Ui60zRO5xnz7Bf3OyxuHdS1NOan3xu1jKx9sA1AtAn+eZxlOzkQgpHB40EKRmZzR0Jm/KijmvKvsYi1/bqUtPI9mBvHw/MJhIMOki9uWayDWEk5p0fsRTS1A8J4nLRgh0ljCz49gmcXqKq01Wzdfp0GcxHkGqvn1uObpxOFDPUY6Uoj3A4imFmPphhC7YQQ7aGwblSyQ/0T/HLS+PS9g/mOxiDMzbLKSNhZmh5hAMPhTyEI1RCATe/+r72NMXstWGRv3ppn6+nqV68h5Llu8Ku5C49Xpq2h7xv9ziN4vTM8YsmUKbuXJUIvBqZIQDhgDU//voXI+00J8wtxHH90tD4nXokQ8lA3vjhErdkJIZ4AtvxPrdtv51NdDzA2w9xqdqcMn11bcnPx10nSVBDRhTucPc7+QiIf47U8oCsRgm7rU98hA/r2TpJZ5IM869JI2hB7316NTqfD4VCgcPji5vTvp18ehHHh7MfLXY+tJG06R2e7eXwcEk0w0n37w59jO+yyCcH8KAVQlCPl/eS22719uIdMQ9Uas9iT4pAmcXauQRKjPVrb27cSoc2DkScz0/bmZ2P3N0avmDRlNwMgISmKSkAU/FspsJoZ36xEL+ps6yFeyuw0Dz4e2VvHxr8KoR1pDp/7mx/7/kIN/0eahabLd+NqbxF/bZTPW5JfCBmE4WmM03TLPe4ShDPfrKf3mC59fIE4z78N+EqCl2UIs9/Tvz2jhE6nwiW7yqZ9Con0igm+FVpqwxKGmxOKDyCYfku/q1fSgC5FGNlu/W/ImUgFtNTJCg+wE1cFvksQQlQV6eGn1jbR9kcRNUlXtYctQYilyHh07LzLsH1R0lzZLb3UluIkunLegSCpMCG1p2DFnOkShJDfpOOsWF3mtEixQpWRrmCrXWZpuK2v/AxeBWzpTCRtHKwI3ZYg5MIdPiiMGsufYi5KpGz04+Uuf6nHr3eKuY3j/QQiePVlua1Ztg958FA1hBBBeyuSqGX78EwkYWX0cyYODZMf/FkFN69h/M9hlTwFCmTQoZwu7SlehtD61qhAuDYnWIL0x/Eyf7EEoeaTckdDSxAC4Xetf+QuyS+WIExPw12Le/sTOnCUA05/8ch0CUKr8z68M0WK4zghbd7oJa0LiwjdtNt0SiR4LkckJELtC2vJtOkCQlsP75V8B7qLgkXJQa1rCXdhEmwRoXWK5eqKqSn2FEl1MlzCLzGPEEyRa3V8w6lOzP0skipC1I21OCc1jxA+ABZ0vawcX0FRnXhxXHEOoY23idYv/erkFPOi7nS0kAe/2oeMa5OqXWrsH0GSrvm7l3cFQtyl9dPD7Pbzfb/tLpIMhmy9LcUeYOuiLd+BhrwUaanp2UKP9BxCNxJCP9WIUyo3UnmiWufxwon3/D4UEYTdraqlTv+T46dggRXslT+M9PCkVU0VRfG7wfqoDYvd1/fVqSG+Fsf/FS9UvucQgqHRByXewVW2UNViC1XTVzGNDTFbJY1MJtTwpgsFt3mE3LZGzX2/5+5CpH8ar7WljAkxqcCp6CpxjPbXhcv25vchQLyt8ho6h98Wiqav9iET1atC/U+I03ha6P6ej2kwwa9cAeOlNLsLEzXzCE19I0tkRSpfvAl/bUzntTSKx6qSWcWzUHK/QLv0ah9a46SyQSn6QxounEC9yoCtTrOyMRt28ZGNCONRldcQr/V5XIuQmfG3qp0czgkhrdPXh93zCHnQL/Pm1NIF1HS8FiGSH3vVDbxxdLPVWb8PWf2hymuIs7QLd8++Qli9E/w5kdk9CusQmsFlhRNgVNPk700IH6qMkADCu41r6O/7NXMI9pxsQGjXK42QOFLdrbc0dnzZrLItdWjz7mwdQtusOkLSXL+GNo8vvSojJGSDllYeIazh1XqEqKUV9viZlm6wpf+Cfbg+txDgD6uMkIA/XK+lEJdW2R9CYOr9vRahiOoPVc6eiHRUZ62WulHwVOV9iLP863N8k8ejKlcTkSZkQyXKtsZehWveOMZ0vRYhZ3rqILsTrc7E00shRm1DVd/mrpi0lvDeVEQ2n8zghel9v8oI2+frT0jBIerzJq0uwlBNrbXn+Eh2cHMfbsHV8kHF/3W0cFvEvKURgumuqi7C2tdggUHi1emaK+ID5MKoWoLhGJLSQeIMxfruSzviXKcyQcqdfb/z2wRJ4AyqfqbRwgDbPELb5jy48ys0WvksyB3hDJJTy15gAZ/PnlzXtNPhoHrNChn10mHfcvnCIs4hjDiLXJaOmqXe21iG0Iz178Zawt86r6XChQ9BD+9bVZtGwAkRv1sX9uKts8um1a3HwxCZtAq866A8Ic+/DXxn+UD+MoRm/cqnmG1VxysSI2mNl19+tXRa3RSTGt5zUJV+dimNwedv2086Q2Rzdn3iSUdVZa6EGOT4NljBEb2cNcK0rh3PeGaN/fjiOP4P4a6g/lg6y+0Kbj0m/qAK008OMk0d44UY23ObzFQ1vj6p4SVOsyCVGB9PYx0kxMWZtdDvBli72J41Yibx9aQRZrRrH1SURCo04jT9b1qvpmVciZAzLa5qHijqR000HJwDpcaxd27txEjnMpvXvzwgpe/MK3443yipoYyW1xXxbhy0eK8Ds4YXD231QVeRGNTz7sdCQ6S5hjB5JUIkZxa2tvhd0shYbz/cfqR++7IDC7jhXvJNLLu2GYjzSctTKsHSBt4/gl6SvL8YWU0bma7BiLYSldD+QRBvvihhExc0JJTaEo+jH4PQ8z3kgQ8poeEeACLdvUGQfNGvJcn9z861ttaSXm6HEAmdBZKOi2Fv1P1Fmp7n+cc1v/b+0vhUO/a9pucPvvfPp8OzWHPhRpshbkBoZ1fTR0hKqIN67PLply+n44Pzg73I+LR3M+TwItZZmtU+t7lPaJOWRmZ2k8Qs6nO1TrW2tBVb+xA9EzejOxcu0p5vcQ/rpjV0BV4Y4trPzJJsxqaOpOrvLLOgDB8On7jA2yU43q6XW0v/BfIfwurL/wM0WbTqN7sXiAAAAABJRU5ErkJggg==">
                                </span>
                                <div class="media-body  ml-2  d-none d-lg-block">
                                    <span class="mb-0 text-sm  font-weight-bold"><?php echo $this->ion_auth->user()->row()->first_name ?> <?php echo $this->ion_auth->user()->row()->last_name ?></span>
                                </div>
                            </div>
                        </a>
                        <div class="dropdown-menu  dropdown-menu-right ">
                            <div class="dropdown-header noti-title">
                                <h6 class="text-overflow m-0">Welcome!</h6>
                            </div>
                            <!--
                            <a href="#!" class="dropdown-item">
                            <i class="ni ni-single-02"></i>
                            <span>My profile</span>
                            </a>
                            <a href="#!" class="dropdown-item">
                            <i class="ni ni-settings-gear-65"></i>
                            <span>Settings</span>
                            </a>
                            <a href="#!" class="dropdown-item">
                            <i class="ni ni-calendar-grid-58"></i>
                            <span>Activity</span>
                            </a>
                            <a href="#!" class="dropdown-item">
                            <i class="ni ni-support-16"></i>
                            <span>Support</span>
                            </a>
                        -->
                            <div class="dropdown-divider"></div>
                            <a href="<?php echo base_url('auth/logout'); ?>" class="dropdown-item">
                            <i class="ni ni-user-run"></i>
                            <span>Logout</span>
                            </a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </nav>