<?php $page = (isset($page)) ? $page : null; ?>
<!-- Sidenav -->
<nav class="sidenav navbar navbar-vertical  fixed-left  navbar-expand-xs navbar-light bg-white" id="sidenav-main">
    <div class="scrollbar-inner">
        <!-- Brand -->
        <div class="sidenav-header  align-items-center border-bottom">
            <a class="navbar-brand" href="javascript:void(0)">
                <div class="d-flex justify-content-between align-items-center">
                    <img src="<?php echo base_url('assets/images/logo.svg') ?>" class="navbar-brand-img" alt="...">
                    <div class="flex-fill text-center">Corporate</div>
                </div>
            </a>
        </div>
        <div class="navbar-inner">
            <!-- Collapse -->
            <div class="collapse navbar-collapse" id="sidenav-collapse-main">
                <!-- Nav items -->
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link <?php is_menu_active( 'corporate-dashboard', $page ) ?>" href="<?php echo base_url( 'corporate/dashboard' ); ?>">
                        <i class="ni ni-tv-2 text-dark"></i>
                        <span class="nav-link-text">Dashboard</span>
                        </a>
                    </li>
                    <?php 
                    $employee_menus = ['corporate-employee-list','corporate-employee-import','corporate-employee-approval','corporate-employee-list-waiting'];
                    ?>
                    <li class="nav-item">
                        <a class="nav-link <?php is_menu_active( $employee_menus, $page ) ?>" href="#navbar-employee" data-toggle="collapse" role="button" aria-expanded="<?php echo (in_array($page, $employee_menus)) ? 'true' : 'false'; ?>" aria-controls="navbar-employee">
                        <i class="ni ni-single-02 text-brown"></i>
                        <span class="nav-link-text">Employees</span>
                        </a>
                        <div class="collapse <?php echo (in_array($page, $employee_menus)) ? 'show' : ''; ?>" id="navbar-employee">
                            <ul class="nav nav-sm flex-column">
                                <li class="nav-item">
                                    <a href="<?php echo base_url('corporate/employee/list'); ?>" class="nav-link <?php is_menu_active( ['corporate-employee-list','corporate-employee-list-waiting'], $page ) ?>">List</a>
                                    <a href="<?php echo base_url('corporate/employee/import'); ?>" class="nav-link <?php is_menu_active( 'corporate-employee-import', $page ) ?>">Import</a>
                                    <!-- <a href="<?php echo base_url('corporate/employee/approval'); ?>" class="nav-link <?php is_menu_active( 'corporate-employee-approval', $page ) ?>">Approval</a> -->
                                </li>
                            </ul>
                        </div>
                    </li>

                    <?php 
                    $healthy_menus = ['corporate-healthy-medical-record','corporate-healthy-fitness','corporate-healthy-status'];
                    ?>
                    <li class="nav-item">
                        <a class="nav-link <?php is_menu_active( $healthy_menus, $page ) ?>" href="#navbar-healthy" data-toggle="collapse" role="button" aria-expanded="<?php echo (in_array($page, $healthy_menus)) ? 'true' : 'false'; ?>" aria-controls="navbar-employee">
                        <i class="fas fa-notes-medical"></i>
                        <span class="nav-link-text">Healthy of Employees</span>
                        </a>
                        <div class="collapse <?php echo (in_array($page, $healthy_menus)) ? 'show' : ''; ?>" id="navbar-healthy">
                            <ul class="nav nav-sm flex-column">
                                <li class="nav-item">
                                    <a href="<?php echo base_url('corporate/healthy/medical_record'); ?>" class="nav-link <?php is_menu_active( 'corporate-healthy-medical-record', $page ) ?>">Medical Record</a>
                                    <a href="<?php echo base_url('corporate/healthy/fitness'); ?>" class="nav-link <?php is_menu_active( 'corporate-healthy-fitness', $page ) ?>">Fitness</a>
                                    <a href="<?php echo base_url('corporate/healthy/status'); ?>" class="nav-link <?php is_menu_active( 'corporate-healthy-status', $page ) ?>">Healthy Status</a>
                                </li>
                            </ul>
                        </div>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link <?php is_menu_active( 'corporate-profile', $page ) ?>" href="<?php echo base_url( 'corporate/profile' ); ?>">
                        <i class="fas fa-user-tie"></i>
                        <span class="nav-link-text">Corporate Profile</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>