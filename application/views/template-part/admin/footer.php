		<!-- Argon Scripts -->
		<!-- Core -->
		<script src="<?php echo base_url('assets/admin/vendor/jquery/dist/jquery.min.js') ?>"></script>
		<script src="<?php echo base_url('assets/admin/vendor/bootstrap/dist/js/bootstrap.bundle.min.js') ?>"></script>
		<script src="<?php echo base_url('assets/admin/vendor/js-cookie/js.cookie.js') ?>"></script>
		<script src="<?php echo base_url('assets/admin/vendor/jquery.scrollbar/jquery.scrollbar.min.js') ?>"></script>
		<script src="<?php echo base_url('assets/admin/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js') ?>"></script>
		<!-- Optional JS -->
		<script src="<?php echo base_url('assets/admin/vendor/chart.js/dist/Chart.min.js') ?>"></script>
		<script src="<?php echo base_url('assets/admin/vendor/chart.js/dist/Chart.extension.js') ?>"></script>
		<!-- Argon JS -->
		<script src="<?php echo base_url('assets/admin/js/argon.js?v=1.2.0') ?>"></script>
		<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
		<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

		<div id="included-js"></div>
		<script type="text/javascript">
			function includeJs(jsFilePath) {
			    var js = document.createElement("script");

			    js.type = "text/javascript";
			    js.src = jsFilePath;

			    // document.body.appendChild(js);
			    $("#included-js").after( js );
			}
		</script>
	
		<?php 
	        if ( isset( $scripts ) ) {
	            foreach ($scripts as $key => $script) {
	                if ( is_string($script) ) {
	                    echo '<script type="text/javascript" src="'.$script.'?v='.date('YmdHis').'"></script>';
	                } else {
	                    if ( $script['attr'] ) {
	                        $attribute = $script['attr']['name'].'="'.$script['attr']['value'].'"';
	                    } else {
	                        $attribute ='';
	                    }
	                    echo '<script type="text/javascript" src="'.$script['src'].'?v='.date('YmdHis').'" '.$attribute.' ></script>';
	                }
	            }
	        }
	    ?>
	</body>
</html>
