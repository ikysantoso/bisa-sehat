<?php $page = (isset($page)) ? $page : null; ?>
<!-- Sidenav -->
<nav class="bg-white sidenav navbar navbar-vertical fixed-left navbar-expand-xs navbar-light" id="sidenav-main">
    <div class="scrollbar-inner">
        <!-- Brand -->
        <div class="sidenav-header align-items-center border-bottom">
            <a class="navbar-brand" href="javascript:void(0)">
                <div class="d-flex justify-content-between align-items-center">
                    <img src="<?php echo base_url('assets/images/logo.svg') ?>" class="navbar-brand-img" alt="...">
                    <div class="text-center flex-fill">Administrator</div>
                </div>
            </a>
        </div>
        <div class="navbar-inner">
            <!-- Collapse -->
            <div class="collapse navbar-collapse" id="sidenav-collapse-main">
                <!-- Nav items -->
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link <?php is_menu_active( 'admin-dashboard', $page ) ?>" href="<?php echo base_url( 'admin/dashboard' ); ?>">
                        <i class="ni ni-tv-2 text-dark"></i>
                        <span class="nav-link-text">Dashboard</span>
                        </a>
                    </li>

                    <?php
                    $user_menus = ['admin-user-list'];
                    ?>
                    <li class="nav-item">
                        <a class="nav-link <?php is_menu_active( $user_menus, $page ) ?>" href="#navbar-user" data-toggle="collapse" role="button" aria-expanded="<?php echo (in_array($page, $user_menus)) ? 'true' : 'false'; ?>" aria-controls="navbar-user">
                        <i class="ni ni-single-02 text-brown"></i>
                        <span class="nav-link-text">User</span>
                        </a>
                        <div class="collapse <?php echo (in_array($page, $user_menus)) ? 'show' : ''; ?>" id="navbar-user">
                            <ul class="nav nav-sm flex-column">
                                <li class="nav-item">
                                    <a href="<?php echo base_url('admin/user/list'); ?>" class="nav-link <?php is_menu_active( 'admin-user-list', $page ) ?>">List</a>
                                </li>
                            </ul>
                        </div>
                    </li>

                    <?php
                    $mcu_menus = ['admin-mcu-list'];
                    ?>
                    <li class="nav-item">
                        <a class="nav-link <?php is_menu_active( $mcu_menus, $page ) ?>" href="#navbar-mcu" data-toggle="collapse" role="button" aria-expanded="<?php echo (in_array($page, $mcu_menus)) ? 'true' : 'false'; ?>" aria-controls="navbar-mcu">
                        <i class="fas fa-stethoscope"></i>
                        <span class="nav-link-text">Medical Check Up</span>
                        </a>
                        <div class="collapse <?php echo (in_array($page, $mcu_menus)) ? 'show' : ''; ?>" id="navbar-mcu">
                            <ul class="nav nav-sm flex-column">
                                <li class="nav-item">
                                    <a href="<?php echo base_url('admin/medical_checkup/list'); ?>" class="nav-link <?php is_menu_active( 'admin-mcu-list', $page ) ?>">List</a>
                                </li>
                            </ul>
                        </div>
                    </li>

                    <?php
                    $corporate_menus = ['admin-corporate-list', 'admin-user-corporate'];
                    ?>
                    <li class="nav-item">
                        <a class="nav-link <?php is_menu_active( $corporate_menus, $page ) ?>" href="#navbar-corporate" data-toggle="collapse" role="button" aria-expanded="<?php echo (in_array($page, $corporate_menus)) ? 'true' : 'false'; ?>" aria-controls="navbar-user">
                        <i class="fas fa-building"></i>
                        <span class="nav-link-text">Data Perusahaan</span>
                        </a>
                        <div class="collapse <?php echo (in_array($page, $corporate_menus)) ? 'show' : ''; ?>" id="navbar-corporate">
                            <ul class="nav nav-sm flex-column">
                                <li class="nav-item">
                                    <a href="<?php echo base_url('admin/corporate/list'); ?>" class="nav-link <?php is_menu_active( 'admin-corporate-list', $page ) ?>">List</a>
                                    <a href="<?php echo base_url('admin/user/corporate'); ?>" class="nav-link <?php is_menu_active( 'admin-user-corporate', $page ) ?>">Import Approval </a>
                                </li>
                            </ul>
                        </div>
                    </li>

                    <?php
                    $service_menus = ['admin-service-list','admin-service-new','admin-service-category','admin-service-edit'];
                    ?>
                    <li class="nav-item">
                        <a class="nav-link <?php is_menu_active( $service_menus, $page ) ?>" href="#navbar-services" data-toggle="collapse" role="button" aria-expanded="<?php echo (in_array($page, $service_menus)) ? 'true' : 'false'; ?>" aria-controls="navbar-services">
                        <i class="fas fa-hand-holding"></i>
                        <span class="nav-link-text">Service</span>
                        </a>
                        <div class="collapse <?php echo (in_array($page, $service_menus)) ? 'show' : ''; ?>" id="navbar-services">
                            <ul class="nav nav-sm flex-column">
                                <li class="nav-item">
                                    <a href="<?php echo base_url('admin/service/list'); ?>" class="nav-link <?php is_menu_active( 'admin-service-list', $page ) ?>">List</a>
                                    <a href="<?php echo base_url('admin/service/category'); ?>" class="nav-link <?php is_menu_active( 'admin-service-category', $page ) ?>">Category</a>
                                </li>
                            </ul>
                        </div>
                    </li>

                    <?php
                    $product_menus = ['admin-product-list','admin-product-category', 'admin-product-warehouse'];
                    ?>
                    <li class="nav-item">
                        <a class="nav-link <?php is_menu_active( $product_menus, $page ) ?>" href="#navbar-products" data-toggle="collapse" role="button" aria-expanded="<?php echo (in_array($page, $product_menus)) ? 'true' : 'false'; ?>" aria-controls="navbar-products">
                        <i class="ni ni-bag-17 text-dark"></i>
                        <span class="nav-link-text">Product</span>
                        </a>
                        <div class="collapse <?php echo (in_array($page, $product_menus)) ? 'show' : ''; ?>" id="navbar-products">
                            <ul class="nav nav-sm flex-column">
                                <li class="nav-item">
                                    <a href="<?php echo base_url('admin/product/list'); ?>" class="nav-link <?php is_menu_active( 'admin-product-list', $page ) ?>">List</a>
                                    <a href="<?php echo base_url('admin/product/category'); ?>" class="nav-link <?php is_menu_active( 'admin-product-category', $page ) ?>">Category</a>
                                    <a href="<?php echo base_url('admin/product/warehouse'); ?>" class="nav-link <?php is_menu_active( 'admin-product-warehouse', $page ) ?>">Warehouse</a>
                                </li>
                            </ul>
                        </div>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link <?php is_menu_active( ['admin-voucher-list','admin-voucher-new'], $page ) ?>" href="<?php echo base_url( 'admin/voucher/list' ); ?>">
                        <i class="fas fa-ticket-alt"></i>
                        <span class="nav-link-text">Vouchers</span>
                        </a>
                    </li>

                    <!-- <?php
                    $bisapay_menus = ['admin-bisapay-list','admin-bisapay-withdraw','admin-bisapay-lock','admin-bisapay-logs'];
                    ?>
                    <li class="nav-item">
                        <a class="nav-link <?php is_menu_active( $bisapay_menus, $page ) ?>" href="#navbar-bisapay" data-toggle="collapse" role="button" aria-expanded="<?php echo (in_array($page, $bisapay_menus)) ? 'true' : 'false'; ?>" aria-controls="navbar-bisapay">
                        <i class="ni ni-money-coins text-brown"></i>
                        <span class="nav-link-text">Bisapay</span>
                        </a>
                        <div class="collapse <?php echo (in_array($page, $bisapay_menus)) ? 'show' : ''; ?>" id="navbar-bisapay">
                            <ul class="nav nav-sm flex-column">
                                <li class="nav-item">
                                    <a href="<?php echo base_url( 'admin/bisapay/list' ); ?>" class="nav-link <?php is_menu_active( 'admin-bisapay-list', $page ) ?>">Topup</a>
                                </li>
                                <li class="nav-item">
                                    <a href="<?php echo base_url( 'admin/bisapay/withdraw' ); ?>" class="nav-link <?php is_menu_active( 'admin-bisapay-withdraw', $page ) ?>">Tarik Tunai</a>
                                </li>
                                <li class="nav-item">
                                    <a href="<?php echo base_url( 'admin/bisapay/lock' ); ?>" class="nav-link <?php is_menu_active( 'admin-bisapay-lock', $page ) ?>">Lock Saldo</a>
                                </li>
                                <li class="nav-item">
                                    <a href="<?php echo base_url( 'admin/bisapay/logs' ); ?>" class="nav-link <?php is_menu_active( 'admin-bisapay-logs', $page ) ?>">Logs</a>
                                </li>
                            </ul>
                        </div>
                    </li> -->

                    <?php
                    $order_menus = ['admin-order','admin-order-detail','admin-order-waiting-confirmation','admin-order-wishorder','admin-order-preorder'];
                    ?>
                    <li class="nav-item">
                        <a class="nav-link <?php is_menu_active( $order_menus, $page ) ?>" href="#navbar-order" data-toggle="collapse" role="button" aria-expanded="<?php echo (in_array($page, $order_menus)) ? 'true' : 'false'; ?>" aria-controls="navbar-order">
                        <i class="ni ni-archive-2 text-dark"></i>
                        <span class="nav-link-text">Order</span>
                        </a>
                        <div class="collapse <?php echo (in_array($page, $order_menus)) ? 'show' : ''; ?>" id="navbar-order">
                            <ul class="nav nav-sm flex-column">
                                <li class="nav-item">
                                    <a href="<?php echo base_url('admin/order/'); ?>" class="nav-link <?php is_menu_active( ['admin-order','admin-order-waiting-confirmation'], $page ) ?>">Order</a>
                                </li>
                                <li class="nav-item">
                                    <a href="<?php echo base_url('admin/order/preorder'); ?>" class="nav-link <?php is_menu_active( 'admin-order-preorder', $page ) ?>">Pre Order</a>
                                </li>
                                <li class="nav-item">
                                    <a href="<?php echo base_url('admin/order/wishorder'); ?>" class="nav-link <?php is_menu_active( 'admin-order-wishorder', $page ) ?>">Wish Order</a>
                                </li>
                            </ul>
                        </div>
                    </li>

                    <?php
                    $page_menus = ['admin-page-list','admin-page-new','admin-page-edit'];
                    ?>
                    <li class="nav-item">
                        <a class="nav-link <?php is_menu_active( $page_menus, $page ) ?>" href="#navbar-pages" data-toggle="collapse" role="button" aria-expanded="<?php echo (in_array($page, $page_menus)) ? 'true' : 'false'; ?>" aria-controls="navbar-pages">
                        <i class="far fa-newspaper"></i>
                        <span class="nav-link-text">Page Tips Hidup Sehat</span>
                        </a>
                        <div class="collapse <?php echo (in_array($page, $page_menus)) ? 'show' : ''; ?>" id="navbar-pages">
                            <ul class="nav nav-sm flex-column">
                                <li class="nav-item">
                                    <a href="<?php echo base_url('admin/page/list'); ?>" class="nav-link <?php is_menu_active( 'admin-page-list', $page ) ?>">List</a>
                                    <!-- <a href="<?php echo base_url('admin/page/setting'); ?>" class="nav-link <?php is_menu_active( 'admin-page-setting', $page ) ?>">Setting</a> -->
                                </li>
                            </ul>
                        </div>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link <?php is_menu_active( ['admin-slider-list','admin-slider-new'], $page ) ?>" href="<?php echo base_url( 'admin/slider/list' ); ?>">
                        <i class="far fa-images"></i>
                        <span class="nav-link-text">Banner Iklan</span>
                        </a>
                    </li>

                    <?php
                    $setting_menus = ['admin-setting-bank_accounts','admin-setting-others'];
                    ?>
                    <li class="nav-item">
                        <a class="nav-link <?php is_menu_active( $setting_menus, $page ) ?>" href="#navbar-maps" data-toggle="collapse" role="button" aria-expanded="<?php echo (in_array($page, $setting_menus)) ? 'true' : 'false'; ?>" aria-controls="navbar-maps">
                        <i class="ni ni-settings text-dark"></i>
                        <span class="nav-link-text">Settings</span>
                        </a>
                        <div class="collapse <?php echo (in_array($page, $setting_menus)) ? 'show' : ''; ?>" id="navbar-maps">
                            <ul class="nav nav-sm flex-column">
                                <li class="nav-item">
                                    <a href="<?php echo base_url('admin/setting/bank_accounts'); ?>" class="nav-link <?php is_menu_active( 'admin-setting-bank_accounts', $page ) ?>">Bank Accounts</a>
                                </li>
                                <li class="nav-item">
                                    <a href="<?php echo base_url('admin/setting/others'); ?>" class="nav-link <?php is_menu_active( 'admin-setting-others', $page ) ?>">Others</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>