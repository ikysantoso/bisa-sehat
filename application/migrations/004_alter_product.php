<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Alter_product extends CI_Migration {

        private $table = 'product';

        public function up()
        {
                $fields = array(
                    'description' => array(
						'type' 			=> 'TEXT',
                    ),
                );

                $this->dbforge->add_column($this->table, $fields);
        }

        public function down()
        {
                $this->dbforge->drop_table( $this->table );
        }
}