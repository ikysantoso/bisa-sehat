<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_warehouse_table extends CI_Migration {

        private $table = 'warehouse';

        public function up()
        {
                $fields = array(
                        'id' => array(
                                'type'           => 'INT(11)',
                                'auto_increment' => TRUE
                        ),
                        'name' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '100',
                        ),
                        'village_id' => array(
                            'type'          => 'VARCHAR',
                            'constraint'    => '50',
                            'default'       => null,
                        ),
                        'village_text' => array(
							'type'          => 'VARCHAR',
	                        'constraint'    => '100',
	                        'default'       => null,
	                        'after'         => 'village_id',
	                    ),
	                    'subdistrict_id' => array(
							'type'          => 'VARCHAR',
	                        'constraint'    => '10',
	                        'after'         => 'village_text',
	                    ),
                        'address'   => array(
                            'type'          => 'VARCHAR',
                            'constraint'    => '200',
                        ),
                        'zipcode'   => array(
                            'type'          => 'VARCHAR',
                            'constraint'    => '5',
                            'default'       => null,
                        ),
                        'created_at' => array(
							'type' 			=> 'TIMESTAMP default CURRENT_TIMESTAMP',
	                    ),
	                    'updated_at' => array(
							'type' 			=> 'TIMESTAMP on update CURRENT_TIMESTAMP',
							'default'		=> null,
	                    ),
                       
                );
                

                $this->dbforge->add_field($fields);
				$this->dbforge->add_key('id', TRUE);
				$this->dbforge->create_table($this->table);
        }

        public function down()
        {
                $this->dbforge->drop_table($this->table);
        }
}