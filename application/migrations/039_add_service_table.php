<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_service_table extends CI_Migration {

        private $table = 'service';

        public function up()
        {
                $fields = array(
                        'id' => array(
                            'type'           => 'INT(11)',
                            'auto_increment' => TRUE
                        ),
                        'title' => array(
                            'type'          => 'VARCHAR',
                            'constraint'    => '255',
                        ),
                        'description' => array(
                            'type'          => 'TEXT',
                            'null'          => true,
                            'default'       => null,
                        ),
                        'category_id' => array(
                            'type'      => 'INT(11)',
                            'default'   => null,
                        ),
                        'created_at' => array(
                            'type'          => 'TIMESTAMP default CURRENT_TIMESTAMP',
                        ),
                        'updated_at' => array(
                            'type'          => 'TIMESTAMP on update CURRENT_TIMESTAMP',
                            'default'       => null,
                        ),
                );

                $this->dbforge->add_field($fields);
                $this->dbforge->add_key('id', TRUE);
                $this->dbforge->create_table($this->table);
                $this->db->query(add_foreign_key($this->table, 'category_id', 'service_category(id)', 'SET NULL', 'CASCADE'));
        }

        public function down()
        {
                $this->dbforge->drop_table($this->table);
        }
}
?>