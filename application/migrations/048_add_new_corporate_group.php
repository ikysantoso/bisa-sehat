<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_new_corporate_group extends CI_Migration {
	private $tables;

	public function __construct() {
		parent::__construct();
		$this->load->dbforge();

		$this->load->config('ion_auth', TRUE);
		$this->tables = $this->config->item('tables', 'ion_auth');
	}

	public function up() {

		$data = [
			[
				'name'        => 'corporate',
				'description' => 'Corporate'
			]
		];
		$this->db->insert_batch($this->tables['groups'], $data);
	}

	public function down() {
	}
}
