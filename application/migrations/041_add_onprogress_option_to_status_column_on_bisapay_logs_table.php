<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_onprogress_option_to_status_column_on_bisapay_logs_table extends CI_Migration {

        private $table = 'bisapay_logs';

        public function up()
        {
                
                $this->dbforge->modify_column($this->table, [
                   'status' => array(
                        'type'          => 'ENUM("success","waiting", "failed","onprogress","checking")',
                        'default'       => 'waiting',
                        'null'          => FALSE,
                    ),
                ]);

        }

        public function down()
        {
                // $this->dbforge->drop_table( $this->table );
        }
}
?>