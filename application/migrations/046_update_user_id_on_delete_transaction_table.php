<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update_user_id_on_delete_transaction_table extends CI_Migration {

        private $table = 'transaction';

        public function up()
        {
                $fields = array(
                    'user_id' => array(
                            'type'          => 'MEDIUMINT',
                            'constraint'    => '8',
                            'unsigned'      => TRUE,
                            'null'          => true
                    ),
                );

                $this->dbforge->modify_column($this->table, $fields);

				$this->db->query(drop_foreign_key($this->table, 'user_id'));
                $this->db->query(add_foreign_key($this->table, 'user_id', 'users(id)', 'SET NULL', 'CASCADE'));

        }

        public function down()
        {
                $this->dbforge->drop_table( $this->table );
        }
}