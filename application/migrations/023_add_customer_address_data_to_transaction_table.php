<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_customer_address_data_to_transaction_table extends CI_Migration {

        private $table = 'transaction';

        public function up()
        {
                $fields = array(
                    'customer_address_data' => array(
						'type'     	  => 'TEXT',
                        'default'     => null,
						'after'       => 'customer_address_id',
                    ),
                );

                $this->dbforge->add_column($this->table, $fields);
        }

        public function down()
        {
                $this->dbforge->drop_table( $this->table );
        }
}