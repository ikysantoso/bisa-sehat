<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update_corporate_table extends CI_Migration {

        private $table = 'corporate';

        public function up()
        {
                $fields_modify = array(
                   'description' => array(
                        'type'      => 'TEXT',
                        'null'      => true,
                        'defaut'    => null,
                    ),
                   
                );

                $field_new = [
                    'phone' => array(
                        'type'          => 'VARCHAR',
                        'constraint'    => '20',
                        'after'         => 'office',
                    ),
                   'website' => array(
                        'type'          => 'VARCHAR',
                        'constraint'    => '100',
                        'after'         => 'phone',
                        'null'          => true,
                    ),
                ];

                $this->dbforge->modify_column($this->table, $fields_modify);
                $this->dbforge->add_column($this->table, $field_new);
        }

        public function down()
        {
                $this->dbforge->drop_table( $this->table );
        }
}