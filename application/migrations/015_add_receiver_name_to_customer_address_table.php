<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_receiver_name_to_customer_address_table extends CI_Migration {

        private $table = 'customer_address';

        public function up()
        {
                
                $fields = array(
                    'receiver_name' => array(
						'type'          => 'VARCHAR',
                        'constraint'    => '100',
                        'default'       => null,
                        'after'         => 'name',
                    ),
                );
                $this->dbforge->add_column($this->table, $fields);
        }

        public function down()
        {
                $this->dbforge->drop_table( $this->table );
        }
}
?>