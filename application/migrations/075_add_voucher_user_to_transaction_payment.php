<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_voucher_user_to_transaction_payment extends CI_Migration {

        private $table = 'transaction_payment';

        public function up()
        {
                $field_new = [
                    'voucher_user_id' => array(
                        'type'          => 'INT(11)',
                        'null'          => true,
                        'after'         => 'payment_method'
                    ),
                ];

                $this->dbforge->add_column($this->table, $field_new);
                $this->db->query(add_foreign_key($this->table, 'voucher_user_id', 'voucher_user(id)', 'SET NULL', 'CASCADE'));
        }

        public function down()
        {
                $this->dbforge->drop_table( $this->table );
        }
}