<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_payment_method_to_transaction_payment_table extends CI_Migration {

        private $table = 'transaction_payment';

        public function up()
        {
                $field_new = [
                        'payment_method' => array(
                                'type'           => 'INT(11)',
                                'default'       => null,
                                'after'         => 'unique_code',
                        ),
                ];

                $this->dbforge->add_column($this->table, $field_new);
                $this->db->query(add_foreign_key($this->table, 'payment_method', 'bank(id)', 'SET NULL', 'CASCADE'));
        }

        public function down()
        {
                $this->dbforge->drop_table( $this->table );
        }
}