<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_status_column_to_corporate_table extends CI_Migration {

        private $table = 'corporate';

        public function up()
        {
                $field_new = [
                    'status' => array(
                        'type'          => 'ENUM("waiting","approved","rejected")',
                        'default'       => 'waiting',
                        'after'         => 'description',
                    ),
                ];

                $this->dbforge->add_column($this->table, $field_new);
        }

        public function down()
        {
                $this->dbforge->drop_table( $this->table );
        }
}