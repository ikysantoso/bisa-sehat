<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_update_voucher_table extends CI_Migration {

        private $table = 'voucher';

        public function up()
        {
                $fields_modify = [
                        'start_date' => array(
                                'type'         => 'DATETIME',
                                'default'      => null,
                                'null'         => true,
                        ),
                        'expired' => array(
                                'type'         => 'DATETIME',
                                'default'      => null,
                                'null'         => true,
                        ),
                ];

                $this->dbforge->modify_column($this->table, $fields_modify);
        }

        public function down()
        {
                $this->dbforge->drop_table( $this->table );
        }
}