<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_quantity_column_to_voucher_table extends CI_Migration {

        private $table = 'voucher';

        public function up()
        {
                $field_new = [
                    'quantity' => array(
                        'type'          => 'INT(11)',
                        'null'          => true,
                    ),
                ];

                $this->dbforge->add_column($this->table, $field_new);
        }

        public function down()
        {
                $this->dbforge->drop_table( $this->table );
        }
}