<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_setting extends CI_Migration {

        private $table = 'setting';

        public function up()
        {
                $fields = array(
                        'id' => array(
                            'type'           => 'INT(11)',
                            'auto_increment' => TRUE
                        ),
                        'meta_key' => array(
                            'type' 			=> 'VARCHAR',
                            'constraint' 	=> '50',
                        ),
                        'meta_value' => array(
							'type' 			=> 'VARCHAR',
                            'constraint' 	=> '255',
                        ),
                        'created_at' => array(
							'type' 			=> 'TIMESTAMP default CURRENT_TIMESTAMP',
	                    ),
	                    'updated_at' => array(
							'type' 			=> 'TIMESTAMP on update CURRENT_TIMESTAMP',
							'default'		=> null,
	                    ),
                );
                

                $this->dbforge->add_field($fields);
				$this->dbforge->add_key('id', TRUE);
				$this->dbforge->create_table($this->table);
        }

        public function down()
        {
                $this->dbforge->drop_table($this->table);
        }
}