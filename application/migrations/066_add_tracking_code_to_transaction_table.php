<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_tracking_code_to_transaction_table extends CI_Migration {

        private $table = 'transaction';

        public function up()
        {
                $field_new = [
                    'tracking_code' => array(
                        'type'          => 'VARCHAR',
                        'constraint'    => '100',
                        'null'          => true,
                        'after'         => 'is_preorder',
                    ),
                ];

                $this->dbforge->add_column($this->table, $field_new);
        }

        public function down()
        {
                $this->dbforge->drop_table( $this->table );
        }
}