<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_payment_id_to_transaction_table extends CI_Migration {

        private $table = 'transaction';

        public function up()
        {
                $field_new = [
                        'transaction_payment_id' => array(
                                'type'          => 'INT(11)',
                                'default'       => null,
                                'after'         => 'shipping_courier',
                        ),
                ];

                $this->dbforge->add_column($this->table, $field_new);
                $this->db->query(add_foreign_key($this->table, 'transaction_payment_id', 'transaction_payment(id)', 'CASCADE', 'CASCADE'));
        }

        public function down()
        {
                $this->dbforge->drop_table( $this->table );
        }
}