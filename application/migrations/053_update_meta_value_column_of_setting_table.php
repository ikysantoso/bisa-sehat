<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update_meta_value_column_of_setting_table extends CI_Migration {

        private $table = 'setting';

        public function up()
        {
                
                $this->dbforge->modify_column($this->table, [
                   'meta_value' => array(
                        'type'          => 'TEXT',
                    ),
                ]);

        }

        public function down()
        {
                // $this->dbforge->drop_table( $this->table );
        }
}
?>