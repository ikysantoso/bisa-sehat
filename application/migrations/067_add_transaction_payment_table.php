<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_transaction_payment_table extends CI_Migration {

        private $table = 'transaction_payment';

        public function up()
        {
                $fields = array(
                        'id' => array(
                            'type'          => 'INT(11)',
                            'auto_increment'=> TRUE
                        ),
                        'unique_code' => array(
                            'type'          => 'INT(11)'
                        ),
                        'status' => array(
                            'type'          => 'ENUM("unpaid","paid","waiting","failed")',
                            'default'       => 'unpaid',
                            'null'          => FALSE,
                        ),
                        'created_at' => array(
                            'type'          => 'TIMESTAMP default CURRENT_TIMESTAMP',
                        ),
                        'updated_at' => array(
                            'type'          => 'TIMESTAMP on update CURRENT_TIMESTAMP',
                            'default'       => null,
                        ),
                );

                $this->dbforge->add_field($fields);
                $this->dbforge->add_key('id', TRUE);
                $this->dbforge->create_table($this->table);
        }

        public function down()
        {
                $this->dbforge->drop_table($this->table);
        }
}
?>