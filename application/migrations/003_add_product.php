<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_product extends CI_Migration {

        private $table = 'product';

        public function up()
        {
                $fields = array(
                        'id' => array(
                                'type'           => 'INT(11)',
                                'auto_increment' => TRUE
                        ),
                        'name' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '100',
                        ),
                        'price' => array(
                                'type' => 'INT',
                        ),
                        'category_id' => array(
                                'type'           => 'INT(11)',
                        ),
                );
                

                $this->dbforge->add_field($fields);
				$this->dbforge->add_key('id', TRUE);
				$this->dbforge->create_table($this->table);
				$this->db->query(add_foreign_key($this->table, 'category_id', 'product_category(id)', 'CASCADE', 'CASCADE'));
        }

        public function down()
        {
                $this->dbforge->drop_table($this->table);
        }
}