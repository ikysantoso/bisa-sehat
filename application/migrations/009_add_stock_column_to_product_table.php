<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_stock_column_to_product_table extends CI_Migration {

        private $table = 'product';

        public function up()
        {
                $fields = array(
                    'stock' => array(
						'type' 			=> 'INT(11)',
                        'default'       => 0,
                        'after'         => 'category_id',
                        'null'          => false,
                    ),
                );

                $this->dbforge->add_column($this->table, $fields);
        }

        public function down()
        {
                $this->dbforge->drop_table( $this->table );
        }
}