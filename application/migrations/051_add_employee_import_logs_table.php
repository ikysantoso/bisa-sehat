<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_employee_import_logs_table extends CI_Migration {

        private $table = 'employee_import_logs';

        public function up()
        {
                $fields = array(
                        'id' => array(
                            'type'           => 'INT(11)',
                            'auto_increment' => TRUE
                        ),
                        'corporate_id' => array(
                            'type'           => 'INT(11)',
                        ),
                        'file' => array(
                            'type'          => 'VARCHAR',
                            'constraint'    => '50',
                        ),
                        'status' => array(
                            'type'          => 'ENUM("waiting","approved","rejected")',
                            'default'       => 'waiting',
                        ),
                        'created_at' => array(
                            'type'          => 'TIMESTAMP default CURRENT_TIMESTAMP',
                        ),
                        'updated_at' => array(
                            'type'          => 'TIMESTAMP on update CURRENT_TIMESTAMP',
                            'default'       => null,
                        ),
                );
                

                $this->dbforge->add_field($fields);
				$this->dbforge->add_key('id', TRUE);
				$this->dbforge->create_table($this->table);
                $this->db->query(add_foreign_key($this->table, 'corporate_id', 'corporate(id)', 'CASCADE', 'CASCADE'));
        }

        public function down()
        {
                $this->dbforge->drop_table($this->table);
        }
}