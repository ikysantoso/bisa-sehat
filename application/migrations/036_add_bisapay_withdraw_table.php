<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_bisapay_withdraw_table extends CI_Migration {

        private $table = 'bisapay_withdraw';

        public function up()
        {
                $fields = array(
                        'id' => array(
                            'type'           => 'INT(11)',
                            'auto_increment' => TRUE
                        ),
                        'customer_bank_id' => array(
                            'type'           => 'INT(11)',
                        ),
                        'bisapay_log_id' => array(
                            'type'           => 'INT(11)',
                        ),
                        'created_at' => array(
                            'type'          => 'TIMESTAMP default CURRENT_TIMESTAMP',
                        ),
                        'updated_at' => array(
                            'type'          => 'TIMESTAMP on update CURRENT_TIMESTAMP',
                            'default'       => null,
                        ),
                );
                

                $this->dbforge->add_field($fields);
                $this->dbforge->add_key('id', TRUE);
                $this->dbforge->create_table($this->table);
                $this->db->query(add_foreign_key($this->table, 'customer_bank_id', 'customer_bank(id)', 'CASCADE', 'CASCADE'));
                $this->db->query(add_foreign_key($this->table, 'bisapay_log_id', 'bisapay_logs(id)', 'CASCADE', 'CASCADE'));
        }

        public function down()
        {
                $this->dbforge->drop_table($this->table);
        }
}
?>