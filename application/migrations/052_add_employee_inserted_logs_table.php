<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_employee_inserted_logs_table extends CI_Migration {

        private $table = 'employee_inserted_logs';

        public function up()
        {
                $fields = array(
                        'id' => array(
                            'type'           => 'INT(11)',
                            'auto_increment' => TRUE
                        ),
                        'imported_log_id' => array(
                            'type'           => 'INT(11)',
                        ),
                        'status' => array(
                            'type'          => 'ENUM("approved","rejected")',
                            'default'       => 'approved',
                        ),
                        'rejected_info' => array(
                            'type'          => 'TEXT',
                            'default'       => null,
                        ),
                        'user_id' => array(
                            'type'           => 'MEDIUMINT',
                            'constraint'     => '8',
                            'unsigned'       => TRUE,
                            'default'       => null,
                        ),
                );
                

                $this->dbforge->add_field($fields);
				$this->dbforge->add_key('id', TRUE);
				$this->dbforge->create_table($this->table);
                $this->db->query(add_foreign_key($this->table, 'imported_log_id', 'employee_import_logs(id)', 'CASCADE', 'CASCADE'));
                $this->db->query(add_foreign_key($this->table, 'user_id', 'users(id)', 'CASCADE', 'CASCADE'));
        }

        public function down()
        {
                $this->dbforge->drop_table($this->table);
        }
}