<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_medical_check_up_table extends CI_Migration {

        private $table = 'medical_checkup';

        public function up()
        {
                $fields = array(
                        'id' => array(
                            'type'           => 'INT(11)',
                            'auto_increment' => TRUE
                        ),
                        'user_id' => array(
                            'type'           => 'MEDIUMINT',
                            'constraint'     => '8',
                            'unsigned'       => TRUE,
                        ),
                        // Workplace Environmental Exposure
                        'wee'   => [
                            'type'  => 'TEXT',
                            'null'  => true
                        ],

                        'anamnesis'   => [
                            'type'  => 'TEXT',
                            'null'  => true
                        ],

                        'physical_examination'   => [
                            'type'  => 'TEXT',
                            'null'  => true
                        ],
                        'lab_result'   => [
                            'type'  => 'TEXT',
                            'null'  => true
                        ],
                        'support_examination'   => [
                            'type'  => 'TEXT',
                            'null'  => true
                        ],
                        'fitness_status'   => [
                            'type'  => 'TEXT',
                            'null'  => true
                        ],
                        'occupation'   => [
                            'type'  => 'TEXT',
                            'null'  => true
                        ],
                        'created_at' => array(
                            'type'          => 'TIMESTAMP default CURRENT_TIMESTAMP',
                        ),
                        'updated_at' => array(
                            'type'          => 'TIMESTAMP on update CURRENT_TIMESTAMP',
                            'default'       => null,
                        ),
                );

                $this->dbforge->add_field($fields);
                $this->dbforge->add_key('id', TRUE);
                $this->dbforge->create_table($this->table);

                $this->db->query(add_foreign_key($this->table, 'user_id', 'users(id)', 'CASCADE', 'CASCADE'));
        }

        public function down()
        {
                $this->dbforge->drop_table($this->table);
        }
}
?>