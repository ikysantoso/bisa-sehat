<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_status_column_to_corporate_member_table extends CI_Migration {

        private $table = 'corporate_member';

        public function up()
        {
                $field_new = [
                    'status' => array(
                        'type'          => 'ENUM("waiting","approved")',
                        'default'       => 'waiting',
                        'after'         => 'user_id',
                    ),
                ];

                $this->dbforge->add_column($this->table, $field_new);
        }

        public function down()
        {
                $this->dbforge->drop_table( $this->table );
        }
}