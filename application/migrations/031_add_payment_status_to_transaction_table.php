<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_payment_status_to_transaction_table extends CI_Migration {

        private $table = 'transaction';

        public function up()
        {
                $fields = array(
                    'payment_status' => array(
                        'type'          => 'ENUM("unpaid","paid","failed")',
                        'default'       => 'unpaid',
                        'null'          => FALSE,
                        'after'         => 'payment_method',
                    ),
                );

                $this->dbforge->add_column($this->table, $fields);
        }

        public function down()
        {
                $this->dbforge->drop_table( $this->table );
        }
}
?>