<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_weight_column_to_product_table extends CI_Migration {

        private $table = 'product';

        public function up()
        {
                $fields = array(
                    'weight' => array(
						'type'     	=> 'INT(11)',
						'after'		=> 'stock',
                    ),
                );

                $this->dbforge->add_column($this->table, $fields);
        }

        public function down()
        {
                $this->dbforge->drop_table( $this->table );
        }
}