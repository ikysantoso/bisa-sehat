<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_status_column_to_transaction_table extends CI_Migration {

        private $table = 'transaction';

        public function up()
        {
                $field_new = [
                    'status' => array(
                        'type'          => 'VARCHAR',
                        'constraint'    => '50',
                        'default'       => 'waiting',
                        'after'         => 'is_preorder',
                    ),
                ];

                $this->dbforge->add_column($this->table, $field_new);
        }

        public function down()
        {
                $this->dbforge->drop_table( $this->table );
        }
}