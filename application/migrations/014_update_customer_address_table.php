<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update_customer_address_table extends CI_Migration {

        private $table = 'customer_address';

        public function up()
        {
                
        		$update_table = array(
				        'village_id' => array(
				            'type' 			=> 'VARCHAR',
                        	'constraint'    => '100',
                        	'default'       => null,
				        ),
				);
                $this->dbforge->modify_column($this->table, $update_table);

                $fields = array(
                    'village_text' => array(
						'type'          => 'VARCHAR',
                        'constraint'    => '100',
                        'default'       => null,
                        'after'         => 'village_id',
                    ),
                    'subdistrict_id' => array(
						'type'          => 'VARCHAR',
                        'constraint'    => '10',
                        'default'       => null,
                        'after'         => 'village_text',
                    ),
                );
                $this->dbforge->add_column($this->table, $fields);
        }

        public function down()
        {
                $this->dbforge->drop_table( $this->table );
        }
}
?>