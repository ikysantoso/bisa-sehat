<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_start_date_column_to_voucher_table extends CI_Migration {

        private $table = 'voucher';

        public function up()
        {
                $field_new = [
                        'start_date' => array(
                                'type'         => 'DATE'
                                ),
                ];

                $this->dbforge->add_column($this->table, $field_new);
        }

        public function down()
        {
                $this->dbforge->drop_table( $this->table );
        }
}