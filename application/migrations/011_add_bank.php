<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_bank extends CI_Migration {

        private $table = 'bank';

        public function up()
        {
                $fields = array(
                        'id' => array(
                            'type'           => 'INT(11)',
                            'auto_increment' => TRUE
                        ),
                        'name' => array(
                            'type' 			=> 'VARCHAR',
                            'constraint' 	=> '50',
                        ),
                        'account_number' => array(
							'type' 			=> 'VARCHAR',
                            'constraint' 	=> '100',
                        ),
                        'account_name' => array(
                            'type' 			=> 'VARCHAR',
                            'constraint' 	=> '50',
                        ),
                        'status' => array(
							'type' 			=> 'ENUM("active","inactive")',
							'default' 		=> 'active',
							'null' 			=> FALSE,
						),
                        'created_at' => array(
							'type' 			=> 'TIMESTAMP default CURRENT_TIMESTAMP',
	                    ),
	                    'updated_at' => array(
							'type' 			=> 'TIMESTAMP on update CURRENT_TIMESTAMP',
							'default'		=> null,
	                    ),
                );
                

                $this->dbforge->add_field($fields);
				$this->dbforge->add_key('id', TRUE);
				$this->dbforge->create_table($this->table);
        }

        public function down()
        {
                $this->dbforge->drop_table($this->table);
        }
}