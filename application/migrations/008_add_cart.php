<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_cart extends CI_Migration {

        private $table = 'cart';

        public function up()
        {
                $fields = array(
                        'id' => array(
                            'type'           => 'INT(11)',
                            'auto_increment' => TRUE
                        ),
                        'product_id' => array(
                            'type'           => 'INT(11)',
                        ),
                        'user_id' => array(
                            'type'           => 'MEDIUMINT',
							'constraint'     => '8',
							'unsigned'       => TRUE,
                        ),
                        'quantity' => array(
							'type'           => 'INT(11)',
                        ),
                        'created_at' => array(
							'type' 			=> 'TIMESTAMP default CURRENT_TIMESTAMP',
	                    ),
	                    'updated_at' => array(
							'type' 			=> 'TIMESTAMP on update CURRENT_TIMESTAMP',
							'default'		=> null,
	                    ),
                );
                

                $this->dbforge->add_field($fields);
				$this->dbforge->add_key('id', TRUE);
				$this->dbforge->create_table($this->table);
				$this->db->query(add_foreign_key($this->table, 'product_id', 'product(id)', 'CASCADE', 'CASCADE'));
				$this->db->query(add_foreign_key($this->table, 'user_id', 'users(id)', 'CASCADE', 'CASCADE'));
        }

        public function down()
        {
                $this->dbforge->drop_table($this->table);
        }
}