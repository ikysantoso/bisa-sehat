<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update_role extends CI_Migration {
		public function __construct() {
			parent::__construct();

			$this->load->model( 'GroupModel' );
		}

        public function up()
        {
        	GroupModel::find(1)->update( ['description' => 'Super Admin'] );
        	GroupModel::find(2)->update( ['description' => 'Member'] );
        	GroupModel::find(3)->update( ['description' => 'Corporate Manager'] );

        	GroupModel::create([
        		'id'			=> 4,
        		'name'			=> 'co_admin',
        		'description'	=> 'Administrator'
        	]);
        }

        public function down()
        {
        }
}