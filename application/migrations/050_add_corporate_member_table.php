<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_corporate_member_table extends CI_Migration {

        private $table = 'corporate_member';

        public function up()
        {
                $fields = array(
                        'id' => array(
                            'type'           => 'INT(11)',
                            'auto_increment' => TRUE
                        ),
                        'corporate_id' => array(
                            'type'           => 'INT(11)',
                        ),
                        'user_id' => array(
                            'type'           => 'MEDIUMINT',
                            'constraint'     => '8',
                            'unsigned'       => TRUE,
                        ),
                );
                

                $this->dbforge->add_field($fields);
				$this->dbforge->add_key('id', TRUE);
				$this->dbforge->create_table($this->table);
                $this->db->query(add_foreign_key($this->table, 'corporate_id', 'corporate(id)', 'CASCADE', 'CASCADE'));
                $this->db->query(add_foreign_key($this->table, 'user_id', 'users(id)', 'CASCADE', 'CASCADE'));
        }

        public function down()
        {
                $this->dbforge->drop_table($this->table);
        }
}