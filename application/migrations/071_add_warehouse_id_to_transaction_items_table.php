<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_warehouse_id_to_transaction_items_table extends CI_Migration {

        private $table = 'transaction_item';

        public function up()
        {
                $field_new = [
                        'warehouse_id' => array(
                                'type'          => 'INT(11)',
                                'after'         => 'quantity',
                                'default'       => null,          
                        ),
                ];


                $this->dbforge->add_column($this->table, $field_new);
        }

        public function down()
        {
                $this->dbforge->drop_table( $this->table );
        }
}