<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_medical_record_table extends CI_Migration {

        private $table = 'medical_record';

        public function up()
        {
                $fields = array(
                        'id' => array(
                            'type'           => 'INT(11)',
                            'auto_increment' => TRUE
                        ),
                        'user_id' => array(
                            'type'           => 'MEDIUMINT',
                            'constraint'     => '8',
                            'unsigned'       => TRUE,
                        ),
                        'height' => array(
                            'type' => 'INT',
                        ),
                        'weight' => array(
                            'type' => 'INT',
                        ),
                        'sistole' => array(
                            'type' => 'INT',
                            'null'  => true,
                        ),
                        'diastole' => array(
                            'type' => 'INT',
                            'null'  => true,
                        ),
                        'blood_sugar_level' => array(
                            'type' => 'INT',
                            'null'  => true,
                        ),
                        'cholesterol_level' => array(
                            'type' => 'INT',
                            'null'  => true,
                        ),
                        'uric_acid_level' => array(
                            'type' => 'DECIMAL(2,1)',
                            'null'  => true,
                        ),
                        'created_at' => array(
                            'type'          => 'TIMESTAMP default CURRENT_TIMESTAMP',
                        ),
                        'updated_at' => array(
                            'type'          => 'TIMESTAMP on update CURRENT_TIMESTAMP',
                            'default'       => null,
                        ),
                );

                $this->dbforge->add_field($fields);
                $this->dbforge->add_key('id', TRUE);
                $this->dbforge->create_table($this->table);
                $this->db->query(add_foreign_key($this->table, 'user_id', 'users(id)', 'CASCADE', 'CASCADE'));
        }

        public function down()
        {
                $this->dbforge->drop_table($this->table);
        }
}
?>