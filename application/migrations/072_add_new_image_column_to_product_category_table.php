<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_new_image_column_to_product_category_table extends CI_Migration {

        private $table = 'product_category';

        public function up()
        {
                $field_new = [
                        'image' => array(
                            'type'          => 'VARCHAR',
                            'constraint'    => '50',
                            'default'		=> null,
                            'after'			=> 'name',
                        ),
                ];

                $this->dbforge->add_column($this->table, $field_new);
        }

        public function down()
        {
                $this->dbforge->drop_table( $this->table );
        }
}