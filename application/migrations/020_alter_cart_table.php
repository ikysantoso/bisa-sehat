<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Alter_cart_table extends CI_Migration {

        private $table = 'cart';

        public function up()
        {
                $fields = array(
                    'warehouse_id' => array(
                       	'type'          => 'INT(11)',
                       	'after'			=> 'quantity'
					),
                );

                $this->dbforge->add_column($this->table, $fields);
				$this->db->query(add_foreign_key($this->table, 'warehouse_id', 'warehouse(id)', 'CASCADE', 'CASCADE'));
        }

        public function down()
        {
                $this->dbforge->drop_table( $this->table );
        }
}