<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_update_nullable_column_to_customer_address_table extends CI_Migration {

        private $table = 'customer_address';

        public function up()
        {
                
                $this->dbforge->modify_column($this->table, [
                        'address'       => [
                                'type'          => 'VARCHAR(200)',
                                "null"          => true,
                                "default"       => null,
                        ],
                	'zipcode' 	=> [
				'type'          => 'VARCHAR(5)',
				"null"		=> true,
				"default"	=> null,
                	]
                ]);

        }

        public function down()
        {
                // $this->dbforge->drop_table( $this->table );
        }
}
?>