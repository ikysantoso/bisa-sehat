<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_corporate_table extends CI_Migration {

        private $table = 'corporate';

        public function up()
        {
                $fields = array(
                        'id' => array(
                            'type'           => 'INT(11)',
                            'auto_increment' => TRUE
                        ),
                        'name' => array(
                            'type'          => 'VARCHAR',
                            'constraint'    => '100',
                        ),
                        'office' => array(
                            'type'          => 'VARCHAR',
                            'constraint'    => '255',
                        ),
                        'description' => array(
                            'type'          => 'TEXT',
                        ),
                        'created_at' => array(
                            'type'          => 'TIMESTAMP default CURRENT_TIMESTAMP',
                        ),
                        'updated_at' => array(
                            'type'          => 'TIMESTAMP on update CURRENT_TIMESTAMP',
                            'default'       => null,
                        ),
                );

                $this->dbforge->add_field($fields);
                $this->dbforge->add_key('id', TRUE);
                $this->dbforge->create_table($this->table);
        }

        public function down()
        {
                $this->dbforge->drop_table($this->table);
        }
}
?>