<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_user_meta_table extends CI_Migration {

        private $table = 'user_meta';

        public function up()
        {
                $fields = array(
                        'id' => array(
                            'type'           => 'INT(11)',
                            'auto_increment' => TRUE
                        ),
                        'user_id' => array(
                            'type'           => 'MEDIUMINT',
                            'constraint'     => '8',
                            'unsigned'       => TRUE,
                        ),
                       	'meta_key' => array(
							'type'           => 'VARCHAR',
                            'constraint'     => '100',
						),
                        'meta_value' => array(
                            'type'           => 'TEXT',
                        ),
						'created_at' => array(
							'type' 			=> 'TIMESTAMP default CURRENT_TIMESTAMP',
	                    ),
	                    'updated_at' => array(
							'type' 			=> 'TIMESTAMP on update CURRENT_TIMESTAMP',
							'default'		=> null,
	                    ),
                );
                

                $this->dbforge->add_field($fields);
                $this->dbforge->add_key('id', TRUE);
                $this->dbforge->create_table($this->table);
                $this->db->query(add_foreign_key($this->table, 'user_id', 'users(id)', 'CASCADE', 'CASCADE'));
        }

        public function down()
        {
                $this->dbforge->drop_table($this->table);
        }
}