<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Alter_product_update_timestamp extends CI_Migration {

        private $table = 'product';

        public function up()
        {
                $fields = array(
                    'created_at' => array(
						'type' 			=> 'TIMESTAMP default CURRENT_TIMESTAMP',
                    ),
                    'updated_at' => array(
						'type' 			=> 'TIMESTAMP on update CURRENT_TIMESTAMP',
						'default'		=> null,
                    ),
                );
                
                $this->dbforge->add_column($this->table, $fields);
        }

        public function down()
        {
                $this->dbforge->drop_table( $this->table );
        }
}