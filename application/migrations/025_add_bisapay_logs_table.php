<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_bisapay_logs_table extends CI_Migration {

        private $table = 'bisapay_logs';

        public function up()
        {
                $fields = array(
                        'id' => array(
                            'type'           => 'INT(11)',
                            'auto_increment' => TRUE
                        ),
                        'bisapay_id' => array(
                            'type'           => 'INT(11)'
                        ),
                        'nominal' => array(
                            'type'           => 'INT(11)'
                        ),
                        'log_type' => array(
							'type' 			=> 'ENUM("in","out")',
							'null' 			=> FALSE,
						),
						'log_type_description' => array(
							'type' 			=> 'VARCHAR',
                            'constraint'    => '100',
							'null' 			=> true,
						),
                        'status' => array(
							'type' 			=> 'ENUM("success","waiting", "failed")',
							'default' 		=> 'waiting',
							'null' 			=> FALSE,
						),
                        'created_at' => array(
							'type' 			=> 'TIMESTAMP default CURRENT_TIMESTAMP',
	                    ),
	                    'updated_at' => array(
							'type' 			=> 'TIMESTAMP on update CURRENT_TIMESTAMP',
							'default'		=> null,
	                    ),
                );
                

                $this->dbforge->add_field($fields);
                $this->dbforge->add_key('id', TRUE);
                $this->dbforge->create_table($this->table);
                $this->db->query(add_foreign_key($this->table, 'bisapay_id', 'bisapay(id)', 'CASCADE', 'CASCADE'));
        }

        public function down()
        {
                $this->dbforge->drop_table($this->table);
        }
}