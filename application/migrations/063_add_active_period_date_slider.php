<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_active_period_date_slider extends CI_Migration {

        private $table = 'slider';

        public function up()
        {
                $field_new = [
                    'start_date' => array(
                        'type'          => 'DATE',
                        'default'       => null,
                        'after'         => 'link',
                    ),
                    'end_date' => array(
                        'type'          => 'DATE',
                        'default'       => null,
                        'after'         => 'start_date',
                    ),
                ];

                $this->dbforge->add_column($this->table, $field_new);
        }

        public function down()
        {
                $this->dbforge->drop_table( $this->table );
        }
}