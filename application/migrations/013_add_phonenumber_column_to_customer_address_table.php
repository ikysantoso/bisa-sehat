<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_phonenumber_column_to_customer_address_table extends CI_Migration {

        private $table = 'customer_address';

        public function up()
        {
                $fields = array(
                    'phone' => array(
						'type'          => 'VARCHAR',
                        'constraint'    => '200',
                        'default'       => null,
                        'after'         => 'zipcode',
                    ),
                );

                $this->dbforge->add_column($this->table, $fields);
        }

        public function down()
        {
                $this->dbforge->drop_table( $this->table );
        }
}
?>