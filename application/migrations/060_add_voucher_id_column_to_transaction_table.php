<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_voucher_id_column_to_transaction_table extends CI_Migration {

        private $table = 'transaction';

        public function up()
        {
                $field_new = [
                        'voucher_id' => array(
                                'type'           => 'INT(11)',
                                'default'       => null,
                                ),
                ];

                $this->dbforge->add_column($this->table, $field_new);
                $this->db->query(add_foreign_key($this->table, 'voucher_id', 'voucher(id)', 'CASCADE', 'CASCADE'));
        }

        public function down()
        {
                $this->dbforge->drop_table( $this->table );
        }
}