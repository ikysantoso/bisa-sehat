<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_is_preorder_column_to_transaction_table extends CI_Migration {

        private $table = 'transaction';

        public function up()
        {
                $fields = array(
                    'is_preorder' => array(
                        'type'          => 'ENUM("yes","no")',
                        'default'       => 'no',
                        'null'          => FALSE,
                        'after'         => 'payment_unique_code',
                    ),
                );

                $this->dbforge->add_column($this->table, $fields);
        }

        public function down()
        {
                $this->dbforge->drop_table( $this->table );
        }
}
?>