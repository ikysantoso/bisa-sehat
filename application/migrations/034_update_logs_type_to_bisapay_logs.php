<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update_logs_type_to_bisapay_logs extends CI_Migration {

        private $table = 'bisapay_logs';

        public function up()
        {
                
                $this->dbforge->modify_column($this->table, [
                   'log_type' => array(
                            'type'          => 'ENUM("in","out","lock")',
                            'null'          => FALSE,
                    ),
                ]);

        }

        public function down()
        {
                // $this->dbforge->drop_table( $this->table );
        }
}
?>