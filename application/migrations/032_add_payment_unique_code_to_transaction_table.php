<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_payment_unique_code_to_transaction_table extends CI_Migration {

        private $table = 'transaction';

        public function up()
        {
                $fields = array(
                    'payment_unique_code' => array(
                        'type'          => 'INT(11)',
                        'default'       => null,
                        'after'         => 'payment_status',
                    ),
                );

                $this->dbforge->add_column($this->table, $fields);
        }

        public function down()
        {
                $this->dbforge->drop_table( $this->table );
        }
}
?>