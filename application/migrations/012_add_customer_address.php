<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_customer_address extends CI_Migration {

        private $table = 'customer_address';

        public function up()
        {
                $fields = array(
                        'id' => array(
                            'type'           => 'INT(11)',
                            'auto_increment' => TRUE
                        ),
                        'name' => array(
                            'type'          => 'VARCHAR',
                            'constraint'    => '50',
                        ),
                        'village_id' => array(
                            'type'          => 'VARCHAR',
                            'constraint'    => '50',
                        ),
                        'address'   => array(
                            'type'          => 'VARCHAR',
                            'constraint'    => '200',
                        ),
                        'zipcode'   => array(
                            'type'          => 'VARCHAR',
                            'constraint'    => '5',
                        ),
                        'user_id' => array(
                            'type'           => 'MEDIUMINT',
                            'constraint'     => '8',
                            'unsigned'       => TRUE,
                        ),
                        'is_primary_address' => array(
                            'type'          => 'ENUM("yes","no")',
                            'default'       => 'no',
                            'null'          => FALSE,
                        ),
                        'created_at' => array(
                            'type'          => 'TIMESTAMP default CURRENT_TIMESTAMP',
                        ),
                        'updated_at' => array(
                            'type'          => 'TIMESTAMP on update CURRENT_TIMESTAMP',
                            'default'       => null,
                        ),
                );
                

                $this->dbforge->add_field($fields);
                $this->dbforge->add_key('id', TRUE);
                $this->dbforge->create_table($this->table);
                $this->db->query(add_foreign_key($this->table, 'user_id', 'users(id)', 'CASCADE', 'CASCADE'));
        }

        public function down()
        {
                $this->dbforge->drop_table($this->table);
        }
}