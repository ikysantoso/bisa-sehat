<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_type_column_to_cart_table extends CI_Migration {

        private $table = 'cart';

        public function up()
        {
                $field_new = [
                        'type' => array(
                                'type'          => 'VARCHAR',
                                'constraint'    => '50',
                                'default'       => null,
                                'after'         => 'warehouse_id',
                        ),
                ];

                $this->dbforge->add_column($this->table, $field_new);
        }

        public function down()
        {
                $this->dbforge->drop_table( $this->table );
        }
}