<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Remove_stock_column_from_product_table extends CI_Migration {

        private $table = 'product';

        public function up()
        {
        	$this->dbforge->drop_column($this->table, 'stock');
        }

        public function down()
        {
			$this->dbforge->drop_table($this->table);
        }
}