<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update_customer_address_ondelete_transaction_table extends CI_Migration {

        private $table = 'transaction';

        public function up()
        {
                $fields = array(
                   'customer_address_id' => array(
                        'type'  => 'INT(11)',
                        'null'  => true,
                    ),
                );

                $this->dbforge->modify_column($this->table, $fields);

				$this->db->query(drop_foreign_key($this->table, 'customer_address_id'));
                $this->db->query(add_foreign_key($this->table, 'customer_address_id', 'customer_address(id)', 'SET NULL', 'CASCADE'));
        }

        public function down()
        {
                $this->dbforge->drop_table( $this->table );
        }
}