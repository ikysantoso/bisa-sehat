<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_transaction_item_table extends CI_Migration {

        private $table = 'transaction_item';

        public function up()
        {
                $fields = array(
                        'id' => array(
                            'type'           => 'INT(11)',
                            'auto_increment' => TRUE
                        ),
                        'transaction_id' => array(
                            'type'           => 'INT(11)'
                        ),
                        'product_id' => array(
                            'type'           => 'INT(11)'
                        ),
                        'price' => array(
                            'type'           => 'INT(11)'
                        ),
                       	'quantity' => array(
                            'type'           => 'INT(11)'
                        ),
                );
                

                $this->dbforge->add_field($fields);
                $this->dbforge->add_key('id', TRUE);
                $this->dbforge->create_table($this->table);
                $this->db->query(add_foreign_key($this->table, 'transaction_id', 'transaction(id)', 'CASCADE', 'CASCADE'));
                $this->db->query(add_foreign_key($this->table, 'product_id', 'product(id)', 'CASCADE', 'CASCADE'));
        }

        public function down()
        {
                $this->dbforge->drop_table($this->table);
        }
}