<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_product_images extends CI_Migration {

        private $table = 'product_images';

        public function up()
        {
                $fields = array(
                        'id' => array(
                            'type'           => 'INT(11)',
                            'auto_increment' => TRUE
                        ),
                        'image' => array(
                            'type' => 'VARCHAR',
                            'constraint' => '100',
                        ),
                        'product_id' => array(
                          	'type'           => 'INT(11)',
                        ),
                );
                

                $this->dbforge->add_field($fields);
				$this->dbforge->add_key('id', TRUE);
				$this->dbforge->create_table($this->table);
				$this->db->query(add_foreign_key($this->table, 'product_id', 'product(id)', 'CASCADE', 'CASCADE'));
        }

        public function down()
        {
                $this->dbforge->drop_table($this->table);
        }
}