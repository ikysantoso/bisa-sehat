<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_status_column_to_slider_table extends CI_Migration {

        private $table = 'slider';

        public function up()
        {
                $field_new = [
                    'status' => array(
                        'type' 			=> 'ENUM("active","inactive")',
                        'default' 		=> 'active',
                        'null' 			=> FALSE,
                        'after'         => 'end_date',
                    ),
                ];

                $this->dbforge->add_column($this->table, $field_new);
        }

        public function down()
        {
                $this->dbforge->drop_table( $this->table );
        }
}