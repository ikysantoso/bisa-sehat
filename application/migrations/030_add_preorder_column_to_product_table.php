<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_preorder_column_to_product_table extends CI_Migration {

        private $table = 'product';

        public function up()
        {
                $fields = array(
                    'is_preorder' => array(
                        'type'          => 'ENUM("yes","no")',
                        'default'       => 'no',
                        'null'          => FALSE,
                        'after'         => 'description',
                    ),
                    'preorder_time' => array(
                        'type'          => 'INT(11)',
                        'default'       => null,
                        'after'         => 'is_preorder',
                    ),
                );

                $this->dbforge->add_column($this->table, $fields);
        }

        public function down()
        {
                $this->dbforge->drop_table( $this->table );
        }
}
?>