<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update_payment_status_option extends CI_Migration {

        private $table = 'transaction';

        public function up()
        {
                
                $this->dbforge->modify_column($this->table, [
                   'payment_status' => array(
                        'type'          => 'ENUM("unpaid","paid","waiting","failed")',
                        'default'       => 'unpaid',
                        'null'          => FALSE,
                    ),
                ]);

        }

        public function down()
        {
                // $this->dbforge->drop_table( $this->table );
        }
}
?>