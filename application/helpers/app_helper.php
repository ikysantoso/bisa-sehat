<?php 
function is_menu_active( $menu, $active_menu ){
    if ( is_array( $menu ) ) {
        if ( in_array( $active_menu, $menu) ) {
            echo 'active';
        }
    } else {
        if ( $menu == $active_menu ) {
            echo 'active';
        }
    }
}

function user_checker( $column, $value ){
    $ci =& get_instance();
    $ci->load->model('UserModel');

    return UserModel::where( $column, $value )->first();
}

function format_date( $date, $format ){
    return date( $format, strtotime( $date ));
}

function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

if (!function_exists('image_uploader')) {

    function image_uploader( $path, $filename, $post_name, $index=null ){
            
            $CI =& get_instance();

            if ( $index ) {
                $i = $index - 1;
                $_FILES['file']['name']       = $_FILES[$post_name]['name'][$i];
                $_FILES['file']['type']       = $_FILES[$post_name]['type'][$i];
                $_FILES['file']['tmp_name']   = $_FILES[$post_name]['tmp_name'][$i];
                $_FILES['file']['error']      = $_FILES[$post_name]['error'][$i];
                $_FILES['file']['size']       = $_FILES[$post_name]['size'][$i];

                $post_name = 'file';

                $filename = md5($filename."#".rand(1,99999)."#".generateRandomString());
            } 

            $path_upload = FCPATH.'assets/images/upload/'.$path;
            if (!file_exists( $path_upload )) {
                mkdir( $path_upload , 0777, true);
                $CI->load->helper('file');
                write_file($path_upload."/index.html", ' ');
            }

            $config['upload_path']          = $path_upload;
            $config['allowed_types']        = 'gif|jpg|jpeg|png';
            // $config['max_size']             = 500;
            $config['file_name']            = $filename;

            $CI->load->library('upload');
            $CI->upload->initialize( $config );

            if ( $CI->upload->do_upload( $post_name )) {
                $file = $CI->upload->data();

                $final_filename = $config['file_name'].$file['file_ext'];

                // image_reupload($path_upload.'/'.$final_filename);

                return $final_filename;
            } else {
                $error = array('error' => $CI->upload->display_errors());
                print_r($error);
                print_r($config['upload_path']);
            }
    }
}

function printTruncated($maxLength, $text)
{
    $text = strip_tags( $text );
    if (strlen($text) <= $maxLength) {
        return $text;
    }
    $text = $text." ";
    $text = substr($text,0,$maxLength);
    $text = substr($text,0,strrpos($text,' '));
    $text = $text."...";
    return $text;
}


function rupiah($angka){
    
    $hasil_rupiah = "Rp " . number_format($angka,0,'.','.');
    return $hasil_rupiah;
 
}

if (!function_exists('delete_image_file')) {
    function delete_image_file( $directory, $image_path=null ){

        if ( $image_path ) {

            $ci = &get_instance();

            // delete main image
            $image_path = FCPATH.'assets/images/upload/'.$directory.'/'.$image_path;

            if ( file_exists( $image_path ) ) {
                unlink( $image_path );
            }

            // // load the allowed image presets
            // $ci->load->config("images");
            // $sizes = $ci->config->item("image_sizes");
            
            // // delete image by size
            // $pathinfo = pathinfo($image_path);
            // foreach ($sizes as $key => $size) {
            //     $delete_image_path =  $pathinfo['dirname']."/".$pathinfo['filename']."-".implode("x", $size).".".$pathinfo['extension'];
            //     if (file_exists($delete_image_path)) {
            //         unlink($delete_image_path);
            //     }
            // }
        }
    }
}

function raja_ongkir_api( $path, $api_key, $method="GET", $fields=null ){
    $curl = curl_init();

        $header = ["key: ".$api_key];

        if ( $method == "POST" ) {
            $header[] = "content-type: application/x-www-form-urlencoded";
        }

        $args = [
          CURLOPT_URL               => "https://api.rajaongkir.com/starter/".$path,
          CURLOPT_RETURNTRANSFER    => true,
          CURLOPT_ENCODING          => "",
          CURLOPT_MAXREDIRS         => 10,
          CURLOPT_TIMEOUT           => 30,
          CURLOPT_HTTP_VERSION      => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST     => $method,
          CURLOPT_HTTPHEADER        => $header,
        ];

        if ($fields) {
            $args += [CURLOPT_POSTFIELDS => $fields];
        }

        curl_setopt_array($curl, $args);

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        return ($err) ? $err : $response;
}

function json_generator( $data, $code = 200 ){
    $ci =& get_instance();
    return $ci->output
                    ->set_content_type('application/json')
                    ->set_status_header( $code )
                    ->set_output( json_encode( $data ) );
}

function randomNumber($length) {
    $result = '';

    for($i = 0; $i < $length; $i++) {
        $result .= mt_rand(1, 9);
    }

    return $result;
}

function get_setting( $meta_key ){
    $ci =& get_instance();
    $ci->load->model('SettingModel');
    return SettingModel::where('meta_key',$meta_key)->first()->meta_value;
}

if( ! function_exists('get_kesimpulan')){
    function get_kesimpulan($problem){
        $ci =& get_instance();
        $ci->load->model('ProductModel');

        if(count($problem) > 0 ){
            $kesimpulan = 'Anda mempunyai indikator bahaya kesehatan = '.count($problem);
            if(count($problem) == '1'){
                $kesimpulan .= ' yaitu pada '.$problem[0].'. ';
                $kesimpulan .= 'Anda mempunyai risiko sakit stroke, jantung, dan ginjal. Saran : harus jaga pola makan dan jika ada keluhan kesehatan segera berobat ke Dokter.';

                // if (str_contains($problem[0], "kegemukan")) {
                //     $kesimpulan .= ' Berikut Produk yang baik untuk penderita obesitas: ' . preg_replace('!(((f|ht)tp(s)?://)[-a-zA-Zа-яА-Я()0-9@:%_+.~#?&;/=]+)!i', '<a href="$1">Klik Lihat Produk.</a>', base_url('/product/suggestion/obesity'));
                // } else if (str_contains($problem[0], "asam urat")) {
                //     $kesimpulan .= ' Berikut Produk yang baik untuk penderita asam urat: ' . preg_replace('!(((f|ht)tp(s)?://)[-a-zA-Zа-яА-Я()0-9@:%_+.~#?&;/=]+)!i', '<a href="$1">Klik Lihat Produk.</a>', base_url('/product/suggestion/uric_acid'));
                // } else if (str_contains($problem[0], "hipertensi")) {
                //     $kesimpulan .= ' Berikut Produk yang baik untuk penderita hipertensi: ' . preg_replace('!(((f|ht)tp(s)?://)[-a-zA-Zа-яА-Я()0-9@:%_+.~#?&;/=]+)!i', '<a href="$1">Klik Lihat Produk.</a>', base_url('/product/suggestion/high_blood_pressure'));
                // } else if (str_contains($problem[0], "diabetes")) {
                //     $kesimpulan .= ' Berikut Produk yang baik untuk penderita diabetes: ' . preg_replace('!(((f|ht)tp(s)?://)[-a-zA-Zа-яА-Я()0-9@:%_+.~#?&;/=]+)!i', '<a href="$1">Klik Lihat Produk.</a>', base_url('/product/suggestion/blood_sugar'));
                // } else if (str_contains($problem[0], "kolesterol")) {
                //     $kesimpulan .= ' Berikut Produk yang baik untuk penderita kolesterol: ' . preg_replace('!(((f|ht)tp(s)?://)[-a-zA-Zа-яА-Я()0-9@:%_+.~#?&;/=]+)!i', '<a href="$1">Klik Lihat Produk.</a>', base_url('/product/suggestion/cholesterol'));
                // }
            } else {
                $all_problem = implode(",", $problem);
                $kesimpulan .= ' (yaitu pada '.$all_problem.'). ';
                $kesimpulan .= 'Anda mempunyai risiko sakit stroke, jantung, dan ginjal. Saran : harus jaga pola makan dan jika ada keluhan kesehatan segera berobat ke Dokter.';
                // for($i=0; $i<count($problem); $i++){
                // if (str_contains($problem[$i], "kegemukan")) {
                //     $kesimpulan .= ' Berikut Produk yang baik untuk penderita kegemukan: ' . preg_replace('!(((f|ht)tp(s)?://)[-a-zA-Zа-яА-Я()0-9@:%_+.~#?&;/=]+)!i', '<a href="$1">Klik Lihat Produk. </a>', base_url('/product/suggestion/obesity'));
                // } else if (str_contains($problem[$i], "asam urat")) {
                //     $kesimpulan .= ' Berikut Produk yang baik untuk penderita asam urat: ' . preg_replace('!(((f|ht)tp(s)?://)[-a-zA-Zа-яА-Я()0-9@:%_+.~#?&;/=]+)!i', '<a href="$1">Klik Lihat Produk. </a>', base_url('/product/suggestion/uric_acid'));
                // } else if (str_contains($problem[$i], "hipertensi")) {
                //     $kesimpulan .= ' Berikut Produk yang baik untuk penderita hipertensi: ' . preg_replace('!(((f|ht)tp(s)?://)[-a-zA-Zа-яА-Я()0-9@:%_+.~#?&;/=]+)!i', '<a href="$1">Klik Lihat Produk. </a>', base_url('/product/suggestion/high_blood_pressure'));
                // } else if (str_contains($problem[$i], "diabetes")) {
                //     $kesimpulan .= ' Berikut Produk yang baik untuk penderita diabetes: ' . preg_replace('!(((f|ht)tp(s)?://)[-a-zA-Zа-яА-Я()0-9@:%_+.~#?&;/=]+)!i', '<a href="$1">Klik Lihat Produk. </a>', base_url('/product/suggestion/blood_sugar'));
                // } else if (str_contains($problem[$i], "kolesterol")) {
                //     $kesimpulan .= ' Berikut Produk yang baik untuk penderita kolesterol: ' . preg_replace('!(((f|ht)tp(s)?://)[-a-zA-Zа-яА-Я()0-9@:%_+.~#?&;/=]+)!i', '<a href="$1">Klik Lihat Produk. </a>', base_url('/product/suggestion/cholesterol'));
                // }}
            }
        }else{
            $kesimpulan = 'Kesehatan tubuh Anda masih oke. Tolong jaga tubuh Anda dengan konsumsi makanan yang sehat, istirahat cukup, dan pola hidup sehat. Jika Anda mempunyai keluhan kesehatan, silahkan pergi ke Dokter.';
        }


        return $kesimpulan;
    }
}

if( ! function_exists('get_status_kesimpulan')){
    function get_status_kesimpulan($problem){
        $ci =& get_instance();
        $ci->load->model('ProductModel');

        if(count($problem) > 0 ){
            $kesimpulan = 'Anda mempunyai indikator bahaya kesehatan = '.count($problem);
            if(count($problem) == '1'){
                $kesimpulan .= ' yaitu pada '.$problem[0].'. ';
                $kesimpulan .= 'Anda mempunyai risiko sakit stroke, jantung, dan ginjal. Saran : harus jaga pola makan dan jika ada keluhan kesehatan segera berobat ke Dokter.';
            } else {
                $all_problem = implode(",", $problem);
                $kesimpulan .= ' (yaitu pada '.$all_problem.'). ';
                $kesimpulan .= 'Anda mempunyai risiko sakit stroke, jantung, dan ginjal. Saran : harus jaga pola makan dan jika ada keluhan kesehatan segera berobat ke Dokter.';
            }
        }else{
            $kesimpulan = 'Kesehatan tubuh Anda masih oke. Tolong jaga tubuh Anda dengan konsumsi makanan yang sehat, istirahat cukup, dan pola hidup sehat. Jika Anda mempunyai keluhan kesehatan, silahkan pergi ke Dokter.';
        }


        return $kesimpulan;
    }
}

function health_result_color($text){
    if($text != ''){
        $text = strtolower($text);
        if($text == 'Normal' || $text == 'normal'){
            return '<span class="text-success">'.ucfirst($text).'</span>';
        }else{
            if(strtolower($text) == 'underweight' || strtolower($text) == 'tidak normal'){
                return '<span class="text-warning">'.ucfirst($text).'</span>';
            }else if($text == 'hati-hati' || $text == 'overweight' || $text == 'hipertensi stage 1' || $text == 'hipertensi stage 2' || $text == 'obesitas'){
                return '<span class="text-danger">'.ucfirst($text).'</span>';
            }else{
                return ucfirst($text);
            }
        }
    }else{
        return '';
    }
}

function health_result_icon($text,$is_admin = false){
    if(!empty($text)){
        $text = strtolower($text);
        if($text == 'Normal' || $text == 'normal'){
            return '';
        }else{
            if(strtolower($text) == 'underweight'){
                return '<span><i class="fas fa-exclamation-triangle orange"></i></span>';
            }else if($text == 'hati-hati' || $text == 'overweight' || $text == 'hipertensi stage 1' || $text == 'hipertensi stage 2' || $text == 'obesitas'){
                if($is_admin == true){
                    return '<span><i class="fas fa-exclamation-triangle" style="color:#eb9da6;"></i></span>';
                }else{
                    return '<span><i class="fas fa-exclamation-triangle text-danger"></i></span>';
                }
            }else{
                return '';
            }
        }
    }else{
        return '';
    }
}

function health_result_circle($text){
    if(!empty($text)){
        $text = strtolower($text);
        if($text == 'Normal' || $text == 'normal'){
            return '<span class="btn btn-circle" style="background-color: #69BC68"></span>';
        }else{
            if(strtolower($text) == 'underweight' || strtolower($text) == 'tidak normal'){
                return '<span class="btn btn-circle" style="background-color: #FFC007"></span>';
            }else if($text == 'hati-hati' || $text == 'overweight' || $text == 'hipertensi stage 1' || $text == 'hipertensi stage 2' || $text == 'obesitas'){
                return '<span class="btn btn-circle" style="background-color: #DC3444"></span>';
            }else{
                return '';
            }
        }
    }else{
        return '';
    }
}

function time_elapsed_string($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}

function countdown( $time ){
    $rem = strtotime($time) - time(); // change date and time to suit.
    $day = floor($rem / 86400);
    $hr  = floor(($rem % 86400) / 3600);
    $min = floor(($rem % 3600) / 60);
    $sec = ($rem % 60);
    if($day > 0) echo $day. " Hari ";
    if($hr > 0 && !$day) echo $hr. " Jam ";
    if($min > 0 && !$hr) echo $min. " Menit ";
    if($sec > 0 && !$min) echo $sec. " Detik ";
}

function transaction_status_icon( $status ){
    $icon = [
        'pengemasan'    => '<i class="fas fa-box"></i>',
        'in delivery'   => '<i class="fas fa-truck"></i>',
        'delivered'     => '<i class="fas fa-check-circle"></i>',
        'waiting'       => '<i class="fas fa-search-dollar"></i>',
        'diproses'      => '<i class="far fa-clock"></i>',
        'unpaid'        => '<i class="fas fa-hourglass-start"></i>',
    ];

    return ((isset($icon[$status])) ? $icon[$status] : '')." <span class='text-capitalize font-weight-bold'>".(($status == 'waiting') ? 'Pengecekan Pembayaran' : $status)."<span>";
}

function socmed_data(){
    $ci =& get_instance();
    $ci->load->model('UserModel');

    return SettingModel::whereIn('meta_key', ['whatsapp_number','facebook', 'instagram'])->get();
}
?>