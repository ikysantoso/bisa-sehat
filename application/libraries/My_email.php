<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PHPMailer\PHPMailer\Exception;
class My_email {
	private $CI;

	public function __construct()
    {
        $this->CI = &get_instance();
    }

    private function send_mail($params)
    {
        $credentials = \SendinBlue\Client\Configuration::getDefaultConfiguration()->setApiKey('api-key', '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');
        $api_instance = new \SendinBlue\Client\Api\TransactionalEmailsApi(new GuzzleHttp\Client(), $credentials);

        $send_mail = new \SendinBlue\Client\Model\SendSmtpEmail([
            'subject' => $params['subject'],
            'sender' => ['name' => 'No Reply - BISA Sehat', 'email' => 'no-reply@aplikasibisa.id'],
            'replyTo' => ['name' => 'No Reply - BISA Sehat', 'email' => 'no-reply@aplikasibisa.id'],
            'to' => [['email' => $params['to']]],
            'htmlContent' => $params['message']
        ]);

        try {
            $api_instance->sendTransacEmail($send_mail);

            return ['status' => 'success'];
        } catch (Exception $e) {
            return [
                'status'    => 'failed',
                'message'   => $e->getMessage()
            ];
        }
    }

	public function registration_email($data)
    {
        $message = $this->CI->load->view('mail/register-mail', $data, TRUE);

        $params = [
            'to'        => $data['email'],
            'subject'   => 'BISA Sehat - Welcoming Message',
            'message'   => $message,
        ];

        return $this->send_mail($params);
	}

    public function forgot_password($data)
    {
        $message = $this->CI->load->view('mail/forgot-password-mail', $data, TRUE);

        $params = [
            'to'        => $data['email'],
            'subject'   => 'BISA Sehat - Reset Password',
            'message'   => $message,
        ];

        return $this->send_mail($params);
    }

    public function bisapay_activation($data)
    {
        $message = $this->CI->load->view('mail/bisapay-activation-mail', $data, TRUE);

        $params = [
            'to'        => $data['email'],
            'subject'   => 'BISA Sehat - Aktivasi Bisapay',
            'message'   => $message,
        ];

        return $this->send_mail( $params );
    }

    public function transaction_mail($data)
    {
        $message = $this->CI->load->view('mail/transaction-mail', $data, TRUE);

        $params = [
            'to'        => $data['email'],
            'subject'   => 'BISA Sehat - Detail Transaksi #'.$data['transaction']->id,
            'message'   => $message
        ];

        return $this->send_mail($params);
    }

    public function order_arrived($data)
    {
        $message = $this->CI->load->view('mail/order-arrived', $data, TRUE);

        $params = [
            'to'        => $data['email'],
            'subject'   => 'BISA Sehat - Pesanan Sudah Tiba',
            'message'   => $message
        ];

        return $this->send_mail($params);
    }

    public function order_approve($data)
    {
        $message = $this->CI->load->view('mail/order-approve', $data, TRUE);

        $params = [
            'to'        => $data['email'],
            'subject'   => 'BISA Sehat - Pesanan Sedang Dikirim',
            'message'   => $message
        ];

        return $this->send_mail($params);
    }

    public function send_contact_mail($data)
    {
        $params = [
            'name'      => $data['name'],
            'from'      => $data['email'],
            'subject'   => $data['subject'],
            'message'   => $data['message'],
        ];

        return $this->send_mail( $params );
    }

    public function voucher_mail($data)
    {
        $message = $this->CI->load->view('mail/voucher-mail', $data, TRUE);

        $params = [
            'to'        => $data['email'],
            'subject'   => 'BISA Sehat - Voucher Belanja',
            'message'   => $message,
        ];

        return $this->send_mail( $params );
    }

    public function bisapay_topup_mail($data)
    {
        $message = $this->CI->load->view('mail/bisapay-topup-mail', $data, TRUE);

        $params = [
            'to'        => $data['mail_address'],
            'subject'   => 'BISA Sehat - Topup Saldo Bisapay',
            'message'   => $message,
        ];

        return $this->send_mail($params);
    }

    public function bisapay_withdraw_mail($data)
    {
        $message = $this->CI->load->view('mail/bisapay-withdraw-mail', $data, TRUE);

        $params = [
            'to'        => $data['mail_address'],
            'subject'   => 'BISA Sehat - Withdraw Saldo Bisapay',
            'message'   => $message,
        ];

        return $this->send_mail($params);
    }
}
?>