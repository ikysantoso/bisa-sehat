<?php defined('BASEPATH') OR exit('No direct script access allowed');
use Dompdf\Dompdf;
class My_pdf extends Dompdf{
   
    private $dompdf;

    public function __construct(){
        parent::__construct();
        $this->dompdf = new Dompdf();
    }
   
    protected function ci()
    {
        return get_instance();
    }
    
    public function medical_record_report($data){


        $html = $this->ci()->load->view('app/member-dashboard/medical-record/medical-record-pdf', $data, TRUE);
        
        $this->dompdf->load_html($html);
        $this->dompdf->setPaper('A4', 'portrait');
        $this->dompdf->set_option('isRemoteEnabled', true);
        $this->dompdf->render();

        $filename = ($data['medical_records']->count() == 1) ? 'medical-record-'.$data['medical_records'][0]->id.'.pdf' : 'medical-record-all.pdf';

        $this->dompdf->stream( $filename , array("Attachment" => true));

    }

    public function fitness_report($data){


        $html = $this->ci()->load->view('app/member-dashboard/fitness/fitness-report-pdf', $data, TRUE);
        
        $this->dompdf->load_html($html);
        $this->dompdf->setPaper('A4', 'portrait');
        $this->dompdf->set_option('isRemoteEnabled', true);
        $this->dompdf->render();

        $filename = ($data['fitnesses']->count() == 1) ? 'fitness-report-'.$data['fitnesses'][0]->id.'.pdf' : 'fitness-report-all.pdf';

        $this->dompdf->stream($filename, array("Attachment" => true));

    }

    public function healty_status_report( $data ){
        $html = $this->ci()->load->view('app/member-dashboard/healthy/healthy-status-pdf', $data, TRUE);
        
        $this->dompdf->load_html($html);
        $this->dompdf->setPaper('A4', 'portrait');
        $this->dompdf->set_option('isRemoteEnabled', true);
        $this->dompdf->render();

        $filename = 'healthy-status-report-'.$data['user_id'].'.pdf';

        $this->dompdf->stream($filename, array("Attachment" => true));
    }
}