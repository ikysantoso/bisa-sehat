<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class My_excel{

	// public function __construct(){
	// }
	public function excel_to_json( $file, $filetype ){
		// if ( file_exists( $file ) ) {
			$inputFileType = explode(".", $filetype);
			$reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader(ucfirst($inputFileType[1]));
			$reader->setReadDataOnly(true);
			$spreadsheet = $reader->load( $file."".$filetype );
			return $spreadsheet->getActiveSheet()->toArray(null, true, true ,true);
		// }
	}
}