<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class PageModel extends Eloquent
{
    protected $table = 'page';

    protected $fillable = [
        'title',
        'description',
        'image'
    ];

}