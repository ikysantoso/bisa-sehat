<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Mailgun\Mailgun;
class MailModel
{
    
    public static function send( $params ){
    	$mgClient = Mailgun::create('a164aef5c17499021908fce46edef037-4de08e90-e44b1960', 'https://api.mailgun.net/v3/sandbox1d8dbfc0403a46fe9bf218598dd2fe19.mailgun.org');
		$domain = "sandbox1d8dbfc0403a46fe9bf218598dd2fe19.mailgun.org";

		# Make the call to the client.
		return $mgClient->messages()->send($domain, $params);
    }
}