<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class CustomerBankModel extends Eloquent
{
    protected $table = 'customer_bank';

    protected $fillable = [
        'name',
        'account_number',
        'account_name',
        'user_id',
    ];
}