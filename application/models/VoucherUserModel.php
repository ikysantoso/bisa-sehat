<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class VoucherUserModel extends Eloquent
{
    protected $table = 'voucher_user';

    protected $fillable = [
        'voucher_id',
        'voucher_code',
        'user_id',
        'type_broadcast',
    ];

    public function user(){
        return $this->belongsTo( UserModel::class, 'user_id' );
    }

    public function voucher(){
        return $this->belongsTo( VoucherModel::class, 'voucher_id' );
    }

}