<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class BisapayModel extends Eloquent
{
    protected $table = 'bisapay';

    protected $fillable = [
        'user_id',
        'status',
    ];

    public function logs(){
        return $this->hasMany( BisapayLogsModel::class, 'bisapay_id' );
    }

    public function user(){
        return $this->belongsTo( UserModel::class, 'user_id' )->select(['first_name', 'last_name', 'email']);
    }

    public function saldo(){
        return $this->logs->sum(function($item) {
        	$nominal = 0;
        	if ( $item->log_type =='out' || $item->log_type =='lock' ) {
        		$nominal -= $item->nominal;
        	}

        	if ( $item->log_type =='in' && $item->status=='success' ) {
                $unique_number = ($item->topup) ? $item->topup->unique_number : 0;
        		$nominal += $item->nominal + $unique_number;
        	}

        	return $nominal;
        });
    }
}