<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class VoucherModel extends Eloquent
{
    protected $table = 'voucher';

    protected $fillable = [
        'name',
        'nominal',
        'start_date',
        'expired',
        'total_count',
        'quantity',
    ];

    public function voucher(){
        return $this->hasMany( VoucherUserModel::class, 'voucher_id' );
    }

    public function status(){
        if ( date_diff(date_create($this->expired), date_create('now'))->d > 0 ) {
            return 'expired';
        } else {
            return 'avaiable';
        }
    }
}