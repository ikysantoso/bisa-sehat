<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class UserModel extends Eloquent
{
    protected $connection = "default";
    protected $table = 'users';
    public $timestamps = false;
    protected $fillable = [
        'ip_address',
        'username',
        'password',
        'email',
        'activation_selector',
        'activation_code',
        'forgotten_password_selector',
        'forgotten_password_code',
        'forgotten_password_time',
        'remember_selector',
        'remember_code',
        'created_on',
        'last_login',
        'active',
        'first_name',
        'last_name',
        'company',
        'phone',
        'phone_verified',
        'phone_verification_code'
    ];

    public function images(){
        return $this->hasMany( ProductImageModel::class, 'product_id' );
    }

    public function user_metas(){
        return $this->hasMany( UserMetaModel::class, 'user_id' );
    }

    public function user_meta( $meta_key ){
        if ( $this->user_metas()->where('meta_key', $meta_key)->first() ) {
            return $this->user_metas()->where('meta_key', $meta_key)->first()->meta_value;
        } else {
            return null;
        }
    }

    public function medical_records(){
        return $this->hasMany( MedicalRecordModel::class, 'user_id' );
    }

    public function fitnesses(){
        return $this->hasMany( FitnessModel::class, 'user_id' );
    }

    public function age(){
        $datebirth = $this->user_meta('date-birth');
        if ( date_create_from_format('m/d/Y',$datebirth ) ) {
            $date_of_birth = date_format(date_create_from_format('m/d/Y', $datebirth), 'Y-m-d');
            return date_diff(date_create($date_of_birth), date_create('now'))->y;
        } else {
            return '<i style="color:red">Invalid birthdate format ['.$datebirth.']</i><p>Valid Format: MM/DD/YYY</p>';
        }
    }

    public function corporate_member(){
        return $this->hasOne( CorporateMemberModel::class, 'user_id' );
    }

    public function medical_checkups(){
        return $this->hasMany( MedicalCheckupModel::class, 'user_id' );
    }
}