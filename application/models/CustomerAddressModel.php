<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class CustomerAddressModel extends Eloquent
{
    protected $table = 'customer_address';

    protected $fillable = [
        'name',
        'receiver_name',
        'phone',
        'subdistrict_id',
        'village_id',
        'village_text',
        'address',
        'zipcode',
        'user_id',
        'is_primary_address',
        'is_shipping_address'
    ];

    public function user(){
        return $this->belongsTo( UserModel::class, 'user_id' );
    }

    public function kelurahan(){
        $kel=$this->hasOne( WilayahKelurahanModel::class, 'wid', 'village_id' );
        if($kel==null){
        return $this->hasOne( WilayahKelurahanModel::class, 'wid', 'subdistrict_id' );
        }else{
            return $kel;
        }
    }

    public function kecamatan(){
        if($this->kelurahan==null){
            return $this->hasOne( WilayahKecamatanModel::class, 'sdid', 'subdistrict_id' );
        }else{
            return $this->kelurahan->hasOne( WilayahKecamatanModel::class, 'sdid', 'sdid' );
        }

    }

    public function kabupaten(){
        return $this->kecamatan->hasOne( WilayahKabupatenModel::class, 'did', 'did' );
    }

    public function provinsi(){
        return $this->kabupaten->hasOne( WilayahProvinsiModel::class, 'pid', 'pid' );
    }
}