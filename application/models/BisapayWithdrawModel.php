<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class BisapayWithdrawModel extends Eloquent
{
    protected $table = 'bisapay_withdraw';

    protected $fillable = [
        'customer_bank_id',
        'bisapay_log_id',
    ];

    public function bisapay_log(){
        return $this->belongsTo( BisapayLogsModel::class, 'bisapay_log_id' );
    }

    public function customer_bank(){
        return $this->belongsTo( CustomerBankModel::class, 'customer_bank_id' );
    }

}