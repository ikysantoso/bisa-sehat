<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class BisapayLogsModel extends Eloquent
{
    protected $table = 'bisapay_logs';

    protected $fillable = [
        'bisapay_id',
        'nominal',
        'log_type',
        'log_type_description',
        'status',
        'merchant_order_id'
    ];

    public function bisapay(){
        return $this->belongsTo( BisapayModel::class, 'bisapay_id' );
    }

    public function topup(){
		return $this->hasOne( BisapayTopupModel::class, 'bisapay_log_id' );
    }
}