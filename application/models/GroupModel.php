<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class GroupModel extends Eloquent
{
    protected $table = 'groups';
    public $timestamps = false;

    protected $fillable = [
        'name',
        'description',
    ];
}