<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class ProductCategoryModel extends Eloquent
{
    protected $table = 'product_category';

    protected $fillable = [
        'name',
        'image'
    ];

    public function products(){
        return $this->hasMany( ProductModel::class, 'category_id' );
    }
}