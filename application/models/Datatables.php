<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Datatables extends CI_Model {
	var $table = '';
    var $column_order = array();
    var $column_search = array();
    var $order = array();
    var $selected_column = array();
    var $where_clause = array();
    var $group_by = '';
    var $having = '';

    private function _get_datatables_query()
    {   
        $data_column_array = array();
        foreach ($this->selected_column as $key => $data_column) {
            $data_column_array[] = $data_column." as ".$key;
        }

        $this->db->select( implode (", ", $data_column_array) );   

        if ( $this->where_clause ) {
            foreach ($this->where_clause as $key => $value) {
                if ( is_array( $value ) ) {
                    if ( isset($value['method']) && isset($value['data']) ) {
                        if ( $value['method'] == 'where_not_in' ) {
                            $this->db->where_not_in( $key, $value['data'] );
                        }
                    } else {
                        $this->db->where_in( $key, $value );
                    }

                } else {
                    $this->db->where( array( $key => $value ) );
                }
            }
        }

        if ( $this->having != '' ) {
            $this->db->having( $this->having );
        }   

        if ( $this->group_by != '' ) {
            $this->db->group_by( $this->group_by );
        }

        $this->db->from($this->table);

        // print_r($this->db->queries[0]);
 
        $i = 0;
     
        foreach ($this->column_search as $item) // looping awal
        {
            if($_POST['search']['value']) // jika datatable mengirimkan pencarian dengan metode POST
            {
                 
                if($i===0) // looping awal
                {
                    $this->db->group_start(); 
                    $this->db->like( $item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) 
                    $this->db->group_end(); 
            }
            $i++;
        }
         
        if(isset($_POST['order'])) 
        {
            if (isset($_POST['order']['0']['column'])) {
                $key_order = $this->column_order[ $_POST['order']['0']['column'] ];
                if ($key_order) {
                    $this->db->order_by( $this->selected_column[ $key_order ], $_POST['order']['0']['dir'] );
                } else {
                    $order = $this->order;
                    if ( $order ) {
                        $this->db->order_by(key($order), $order[key($order)]);
                    }
                }
            }
        } 
    }
 
    public function get_datatables( 
        $param_table, 
        $param_selected_columns, 
        $param_column_order, 
        $param_column_search, 
        $param_order=null, 
        $param_where_clause=null, 
        $param_custom_column=null, 
        $param_custom_group=null,
        $param_having=null
    )
    {

    	$this->table = $param_table; 
	    $this->column_order = $param_column_order;
	    $this->column_search = $param_column_search; 
        $this->order = $param_order;
	    $this->selected_column = $param_selected_columns;
        $this->where_clause = $param_where_clause;
        $this->group_by = $param_custom_group;
        $this->having = $param_having;

        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        // return $query->result();

        $list = $query->result();
        $data = array();
        $no = $_POST['start'];

        foreach ($list as $field) {
            $no++;
            $row = array();
            $row[] = $no;
            foreach ($param_selected_columns as $key => $value) {
                $row[] = $field->$key;
            }

            if ($param_custom_column) {
                $row[]=$param_custom_column;
            }
 
            $data[] = $row;
        }
 
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->count_all(),
            "recordsFiltered" => $this->count_filtered(),
            "data" => $data,
            'selected_filter' => $this->session->status_filter,
            'query' => $this->db->last_query()
        );
        //output dalam format JSON
        echo json_encode($output);
    }
 
    public function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
}
