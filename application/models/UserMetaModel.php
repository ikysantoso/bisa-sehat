<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class UserMetaModel extends Eloquent
{
    protected $connection = "default";
    protected $table = 'user_meta';

    protected $fillable = [
        'user_id',
        'meta_key',
        'meta_value',
    ];

    public function user(){
        return $this->belongsTo( UserModel::class, 'user_id' );
    }
}