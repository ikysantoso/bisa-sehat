<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class ClaimedVoucher extends Eloquent
{
    protected $table = 'claimed_vouchers';

    protected $fillable = [
        'voucher_id',
        'user_id'
    ];
}