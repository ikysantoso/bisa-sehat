<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class ServiceCategoryModel extends Eloquent
{
    protected $table = 'service_category';

    protected $fillable = [
        'name',
    ];
}