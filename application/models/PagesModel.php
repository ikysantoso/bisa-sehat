<?php 

class PagesModel extends CI_Model{
	function data($number,$offset,$keyword = null){
        if ($keyword) {
            $this->db->like('title', $keyword);
        }
		$this->db->order_by('id', 'DESC');
		return $query = $this->db->get('page',$number,$offset)->result();		
	}
 
	function jumlah_data(){
		return $this->db->get('page')->num_rows();
	}

	function get_id($id){
		$this->db->where('id', $id);
		$query = $this->db->get('page');
		return $query->result(); 
	}

}