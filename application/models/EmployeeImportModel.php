<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class EmployeeImportModel extends Eloquent
{
    protected $table = 'employee_import_logs';

    protected $fillable = [
        'corporate_id',
        'file',
        'status',
    ];

    public function corporate(){
        return $this->belongsTo( CorporateModel::class, 'corporate_id' );
    }

    public function employee_inserted(){
        return $this->hasMany( EmployeeInsertedlog::class, 'imported_log_id' );
    }
}