<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class WishOrderModel extends Eloquent
{
    protected $table = 'wish_order';

    protected $fillable = [
        'user_id',
        'name',
        'price',
        'quantity',
        'unit',
        'description',
        'product_id',
    ];

    public function user(){
        return $this->belongsTo( UserModel::class, 'user_id' );
    }

    public function product(){
        return $this->belongsTo( ProductModel::class, 'product_id' );
    }
}