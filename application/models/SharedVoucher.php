<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class SharedVoucher extends Eloquent
{
    protected $table = 'shared_vouchers';

    protected $fillable = [
        'voucher_id',
        'user_id',
        'quantity'
    ];
}