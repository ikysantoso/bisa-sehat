<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class TransactionModel extends Eloquent
{
    protected $table = 'transaction';

    protected $fillable = [
        'transaction_id',
        'user_id',
        'customer_address_id',
        'customer_address_data',
        'shipping_fee',
        'shipping_courier',
        'transaction_payment_id',
        'payment_method',
        'payment_status',
        'payment_unique_code',
        'is_preorder',
        // 'voucher_id',
        'voucher_name',
        'voucher_nominal',
        'status',
        'tracking_code',
        'payment_image',
        'weight',
        'delivery_service'
    ];

    public function transaction_items(){
        return $this->hasMany( TransactionItemModel::class, 'transaction_id' );
    }

    public function total(){
        return $this->transaction_items->sum(function($item) {
            return $item->quantity * $item->price;
        });
    }

    public function bank(){
        return $this->belongsTo( BankModel::class, 'payment_method' );
    }

    public function user(){
        return $this->belongsTo( UserModel::class, 'user_id' )->select(['username', 'email', 'first_name', 'last_name', 'phone']);
    }

    public function voucher(){
        return $this->belongsTo( VoucherModel::class, 'voucher_id' );
    }

    public function payment(){
        return $this->belongsTo( TransactionPaymentModel::class, 'transaction_payment_id' );
    }

    public function grand_total( $transaction_id ){
        $transaction = $this->find( $transaction_id );

        $total_ongkos_kirim = 0;
        $total_produk = 0;

        foreach (($transaction->payment->status == 'paid') ? [$transaction] : $transaction->payment->transactions as $key => $transaction){

            $total_produk += $transaction->total();
            $total_ongkos_kirim += $transaction->shipping_fee;
        }

        $discount = ($transaction->voucher_nominal) ? ($transaction->voucher_nominal) : 0;

        $total_transaction = $total_produk + $total_ongkos_kirim - $discount;

        if ($total_transaction < 0) {
            $total_transaction = 0;
        }

        return $total_transaction;
    }
}