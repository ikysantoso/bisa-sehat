<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class BankModel extends Eloquent
{
    protected $table = 'bank';

    protected $fillable = [
        'name',
        'account_name',
        'account_number',
        'status',
    ];
}