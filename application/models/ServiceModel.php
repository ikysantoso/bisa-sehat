<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class ServiceModel extends Eloquent
{
    protected $table = 'service';

    protected $fillable = [
        'title',
        'description',
        'category_id',
    ];

    public function images(){
        return $this->hasMany( ServiceImageModel::class, 'service_id' );
    }

    public function category(){
        return $this->belongsTo( ServiceCategoryModel::class, 'category_id' );
    }
}