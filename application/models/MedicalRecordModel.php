<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class MedicalRecordModel extends Eloquent
{
    protected $table = 'medical_record';

    protected $fillable = [
        'user_id',
        'height',
        'weight',
        'sistole',
        'diastole',
        'blood_sugar_level',
        'cholesterol_level',
        'uric_acid_level',
        'uric_acid_more_than_ten_level',
        'attachment',
        'status'
    ];

    public function user(){
        return $this->belongsTo( UserModel::class, 'user_id' );
    }

    public function bmi() {
        $bb = $this->weight;
        $tb = $this->height;

        if ($bb && $tb > 0) {
            $bmi = $bb/(pow(($tb/100),2));

            $min_weight = 18.5 * (pow(($tb/100),2));
            $max_weight = 22.99 * (pow(($tb/100),2));

            $result = array(
                'bmi' => round($bmi,2),
                'min' => ceil($min_weight),
                'max' => round($max_weight),
            );

            return $result;
        }
    }

    public function bmi_scale(){
        $bmi = $this->bmi()['bmi'];

        if($bmi > 0 && $bmi < 18.5){
            $desc = 'Underweight';
            $count = 0;
        }else if($bmi >= 18.5 && $bmi < 23){
            $desc = 'Normal';
            $count = 0;
        }else if($bmi >= 23 && $bmi < 25){
            $desc = 'Overweight';
            $count = 1;
        }else if( $bmi >= 25){
            $desc = 'Obesitas';
            $count = 2;
        }else{
            $desc = 'Invalid';
            $count = 0;
        }

        $result  = array(
            'desc' => $desc,
            'count' => $count,
        );

        return $result;
    }

    public function blood_pressure_scale(){

        $sistole = $this->sistole;
        $diastole = $this->diastole;

        if($sistole > 80 && $sistole < 140){
            if($diastole > 0 && $diastole < 90){
                $desc = 'Normal';
                $count = 0;
            }else if($diastole >= 90 && $diastole < 100) {
                $desc = 'Hipertensi Stage 1';
                $count = 1;
            }else if($diastole >= 100){
                $desc = 'Hipertensi Stage 2';
                $count = 1;
            }else{
                $desc = 'Belum ada data';
                $count = 0;
            }
        }else if($sistole >= 140 && $sistole < 160){
            if($diastole >= 100){
                $desc = 'Hipertensi Stage 2';
                $count = 1;
            }else{
                $desc = 'Hipertensi Stage 1';
                $count = 1;
            }
        }else if( $sistole >= 160 ){
            $desc = 'Hipertensi Stage 2';
            $count = 1;
        }else{
            $desc = 'Belum ada data';
            $count = 0;
        }

        $result = array(
            'desc' => $desc,
            'count' => $count
        );

        return $result;
    }

    public function blood_sugar_level_scale(){

        $gula_darah = $this->blood_sugar_level;
        if( $gula_darah > 70 && $gula_darah < 140 ){
            $desc = 'Normal';
            $count = 0;
        }else if( $gula_darah >= 140 ){
            $desc = 'Hati-hati';
            $count = 1;
        }else if($gula_darah > 0 && $gula_darah < 70 ){
            $desc = 'Hati-hati';
            $count = 2;
        }else{
            $desc = 'Belum ada data';
            $count = 0;
        }

        $result = array(
            'desc' => $desc,
            'count' => $count
        );

        return $result;
    }

    public function cholesterol_level_scale(){

        $kolesterol = $this->cholesterol_level;

        if( $kolesterol > 0 && $kolesterol < 240 ){
            $desc = 'Normal';
            $count = 0;
        }else if( $kolesterol >= 240 ){
            $desc = 'Hati-hati';
            $count = 1;
        }else{
            $desc = 'Belum ada data';
            $count = 0;
        }

        $result = array(
            'desc' => $desc,
            'count' => $count
        );

        return $result;
    }

    public function uric_acid_level_scale(){

        $gender = $this->user->user_meta('gender');
        $asam_urat = $this->uric_acid_level;

        if( $gender == 'Perempuan' ){
                $line = 5.8;
            if( $asam_urat > 0 && $asam_urat <= 5.7 ){
                $desc = 'Normal';
                $count = 0;
            }else if( $asam_urat > 5.7 ){
                $desc = 'Hati-hati';
                $count = 1;
            }else{
                $desc = 'Belum ada data';
                $count = 0;
            }
        } else if( $gender = 'Laki-laki' ){
                $line = 7.1;
            if( $asam_urat > 0 && $asam_urat <= 7 ){
                $desc = 'Normal';
                $count = 0;
            }else if( $asam_urat > 7 && $asam_urat < 15 ){
                $desc = 'Hati-hati';
                $count = 1;
            }else{
                $desc = 'Belum ada data';
                $count = 0;
            }
        }

        $result = array(
            'desc' => $desc,
            'count' => $count,
            'line' => $line
        );
        return $result;
    }
}