<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class BisapayTopupModel extends Eloquent
{
    protected $table = 'bisapay_topup';

    protected $fillable = [
        'bisapay_log_id',
        'unique_number',
        'bank_id',
        'user_id',
    ];

    public function log(){
        return $this->belongsTo( BisapayLogsModel::class, 'bisapay_log_id' );
    }

    public function bank(){
        return $this->belongsTo( BankModel::class, 'bank_id' );
    }

    public function user(){
        return $this->belongsTo( UserModel::class, 'user_id' )->select(['username', 'email']);
    }
}