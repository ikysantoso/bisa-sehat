<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;

class CareResultPatientModel extends Eloquent
{
    protected $table = 'care_result_patient';

    protected $fillable = [
        'crpid',
        'check_date',
        'analysis_doctor',
        'patientid',
        'cmid',
        'result'
    ];
}