<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class CorporateMemberModel extends Eloquent
{
    protected $table = 'corporate_member';
    public $timestamps = false;

    protected $fillable = [
        'corporate_id',
        'user_id',
        'status',
    ];

    public function corporate(){
        return $this->belongsTo( CorporateModel::class, 'corporate_id' );
    }
}