<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class SubDistrictModel extends Eloquent
{
    protected $table = 'wilayah_subdistrict';
}