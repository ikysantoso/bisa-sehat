<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class ProductModel extends Eloquent
{
    protected $table = 'product';

    protected $fillable = [
        'name',
        'price',
        'stock',
        'weight',
        'category_id',
        'description',
        'is_preorder',
        'preorder_time',
        'discount',
        'buyer_count',
        'purchase_price',
        'purchase_vat',
        'margin',
        'sales_vat',
        'start_date_sales',
        'end_date_sales',
        'suggestion_for'
    ];

    public function images()
    {
        return $this->hasMany( ProductImageModel::class, 'product_id' );
    }

    public function stocks()
    {
        return $this->hasMany( ProductStockModel::class, 'product_id' );
    }

    public function stock($warehouse_id)
    {
        return $this->stocks()->where('warehouse_id', $warehouse_id)->first()->stock;
    }

    public function category()
    {
        return $this->belongsTo( ProductCategoryModel::class, 'category_id' );
    }

    public function wishorder()
    {
        return $this->hasOne( WishOrderModel::class, 'product_id' );
    }

    public function cart()
    {
        return $this->hasMany( CartModel::class, 'product_id' );
    }
}