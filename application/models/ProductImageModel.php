<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class ProductImageModel extends Eloquent
{
    protected $table = 'product_images';
    public $timestamps = false;

    protected $fillable = [
        'image',
        'product_id',
    ];

}