<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class WilayahProvinsiModel extends Eloquent
{
    protected $table = 'list_village_name_db.province';
}