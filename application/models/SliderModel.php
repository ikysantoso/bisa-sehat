<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class SliderModel extends Eloquent
{
    protected $table = 'slider';

    protected $fillable = [
        'title1',
        'title2',
        'image',
        'link',
        'start_date',
        'end_date',
        'status',
    ];
}