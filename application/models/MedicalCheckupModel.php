<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class MedicalCheckupModel extends Eloquent
{
    protected $table = 'medical_checkup';

    protected $fillable = [
        'user_id',
        'wee',
        'anamnesis',
        'physical_examination',
        'lab_result',
        'support_examination',
        'fitness_status',
        'occupation',
    ];
}