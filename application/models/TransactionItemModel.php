<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class TransactionItemModel extends Eloquent
{
    protected $table = 'transaction_item';
    public $timestamps = false;

    protected $fillable = [
        'transaction_id',
        'product_id',
        'price',
        'quantity',
        'warehouse_id',
    ];

    public function product(){
        return $this->belongsTo( ProductModel::class, 'product_id' );
    }

    public function transaction(){
        return $this->belongsTo( TransactionModel::class, 'transaction_id' );
    }

    public function preorder_time(){
        return ( $this->transaction->is_preorder == 'yes' ) ? "preorder ".(($this->product->preorder_time) ? $this->product->preorder_time : 7)." hari" : '';
    }
}