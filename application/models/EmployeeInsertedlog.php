<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class EmployeeInsertedlog extends Eloquent
{
    protected $table = 'employee_inserted_logs';

    protected $fillable = [
        'imported_log_id',
        'status',
        'rejected_info',
        'user_id',
    ];

    public function user(){
        return $this->belongsTo( UserModel::class, 'user_id' )->select(['first_name', 'last_name', 'email', 'username']);
    }
}