<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class ServiceImageModel extends Eloquent
{
    protected $table = 'service_images';
    public $timestamps = false;

    protected $fillable = [
        'image',
        'service_id',
    ];

}