<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class TransactionPaymentModel extends Eloquent
{
    protected $table = 'transaction_payment';

    protected $fillable = [
        'unique_code',
        'payment_method',
        'status',
    ];

    public function transactions(){
        return $this->hasMany( TransactionModel::class, 'transaction_payment_id' );
    }

    public function bank(){
        return $this->belongsTo( BankModel::class, 'payment_method' );
    }

    public function voucher_user(){
        return $this->belongsTo( VoucherUserModel::class, 'voucher_user_id' );
    }
}