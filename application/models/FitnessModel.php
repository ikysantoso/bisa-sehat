<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Eloquent\Model as Eloquent;

class FitnessModel extends Eloquent
{
    protected $table = 'fitness';

    protected $fillable = [
        'user_id',
        'height',
        'weight',
        'mileage',
    ];

    public function user()
    {
        return $this->belongsTo( UserModel::class, 'user_id' );
    }

    public function mileage_result()
    {
        $age = date_diff(date_create($this->user->user_meta('date-birth')), date_create('now'))->y;

        $result = $this->mileageCalculation($this->user->user_meta('gender'), $age, $this->height, $this->weight);

        return [
            'ideal'     => ceil($result),
            'status'    => ($this->mileage >= $result) ? 'Anda bugar' : 'Anda tidak bugar',
        ];
    }

    public function mileageCalculation($gender, $age, $height, $weight)
    {
        switch($gender) {
            case 'Laki-Laki':
                $height_counter = 7.57 * $height;
                $age_counter = 5.02 * $age;
                $weight_counter = 1.76 * $weight;

                $result = $height_counter - $age_counter - $weight_counter - 309;

                break;

            case 'Perempuan':
                $height_counter = 2.11 * $height;
                $age_counter = 5.78 * $age;
                $weight_counter = 2.29 * $weight;

                $result = $height_counter - $age_counter - $weight_counter + 667;

                break;

            default:
                return null;
        }

        return $result;
    }
}