<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class UserGroupModel extends Eloquent
{
    protected $connection = "default";
    protected $table = 'users_groups';

    protected $fillable = [
        'user_id',
        'group_id',
    ];
}