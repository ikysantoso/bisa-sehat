<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class WarehouseModel extends Eloquent
{
    protected $table = 'warehouse';
    protected $fillable = [
        'name',
        'village_id',
        'village_text',
        'subdistrict_id',
        'address',
        'zipcode',
        'shipping'
    ];

    public function kecamatan(){
        return $this->hasOne( WilayahKecamatanModel::class, 'subdistrict_id', 'subdistrict_id' );
    }

    public function kabupaten(){
        return $this->kecamatan->hasOne( WilayahKabupatenModel::class, 'city_id', 'city_id' );
    }

    public function provinsi(){
        return $this->kabupaten->hasOne( WilayahProvinsiModel::class, 'province_id', 'province_id' );
    }
}