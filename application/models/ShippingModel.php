<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class ShippingModel extends CI_Model{

	private $rajaongkir_api;

	public function __construct(){
		parent::__construct();
		$this->rajaongkir_api = $this->config->item( 'rajaongkir_api' );
	}

	public function ongkir( $origin, $destination, $weight, $couriers ){

			$data = [];
			foreach ($couriers as $key => $courier) {

				$data[] = json_decode(
								raja_ongkir_api( 
									'cost', 
									$this->rajaongkir_api, 
									"POST", 
									"origin=".$origin."&destination=".$destination."&weight=".$weight."&courier=".$courier
								)
							);
			}
			return $data;
	}

	public function ongkir_by_jasa( $origin, $destination, $weight, $courier, $service ){
		$result = json_decode(
								raja_ongkir_api( 
									'cost', 
									$this->rajaongkir_api, 
									"POST", 
									"origin=".$origin."&destination=".$destination."&weight=".$weight."&courier=".$courier
								)
							);
		$data = null;
		foreach ($result->rajaongkir->results[0]->costs as $key => $item) {
			if ( $item->service == $service ) {
				$data = $item->cost;
			}
		}
		return $data[0];
	}
}
?>