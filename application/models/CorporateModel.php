<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class CorporateModel extends Eloquent
{
    protected $table = 'corporate';

    protected $fillable = [
        'name',
        'office',
        'phone',
        'website',
        'description',
        'status',
    ];

    public function members(){
        return $this->hasMany( CorporateMemberModel::class, 'corporate_id' );
    }
}