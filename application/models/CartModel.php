<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class CartModel extends Eloquent
{
    protected $table = 'cart';

    protected $fillable = [
        'product_id',
        'user_id',
        'quantity',
        'warehouse_id',
        'type',
    ];

    public function product(){
        return $this->belongsTo( ProductModel::class, 'product_id' );
    }

    public function warehouse(){
        return $this->belongsTo( WarehouseModel::class, 'warehouse_id' );
    }

    public function is_preorder( $warehouse_id ){
        return ($this->product->stock( $warehouse_id ) < $this->quantity);
    }

    public function preorder_time( $warehouse_id ){
        return ( $this->is_preorder($warehouse_id) ) ? "preorder ".(($this->product->preorder_time) ? $this->product->preorder_time : 7)." hari" : '';
    }
}