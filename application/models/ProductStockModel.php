<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class ProductStockModel extends Eloquent
{
    protected $table = 'product_stock';

    protected $fillable = [
        'product_id',
        'warehouse_id',
        'stock',
        'unit'
    ];

    public function warehouse(){
        return $this->belongsTo( WarehouseModel::class, 'warehouse_id' );
    }

	public function product(){
        return $this->belongsTo( ProductModel::class, 'product_id' );
    }    
}